
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <sys/sysinfo.h>
#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)




double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, int nk, double rmin, double *x, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
					for (k = 0; k < nk2; k++){

				i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
							for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {

								i02 = I3D(ni2,nj2, i, j,k);
					if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];
						}
					}
				}
			}


			if(x[i0]*somme!=0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void filtre2(int ni, int nj, int nk, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			for (k = 0; k < nk2; k++){

		i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
								for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {
								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
						fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}
				}

						if(somme!=0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void OC (int ni, int nj, int nk, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*nk*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				for (k = 0; k < nk; k++){
				i0 = I3D(ni,nj, i, j,k);
				gamma_old[i0]=gamma0[i0];
			}
		}
	}


	while ((l2-l1)> 1e-4){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
			for (k = 0; k < nk; k++){
			i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
	//printf("ok boucle \n");
				if(gradient[i0]>0) {

					printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);
			}
		}
	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
		for (k = 0; k < nk; k++){
		i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

}
		}
	}
}

volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

void KM_matrix_2D(double delta_x, double delta_y, double *KE, double *ME){
	double N[4];
	double DN[2][4];
	double gxy[3][3];
	double wg[3][3],wxy;
	int i,j,k,i1,j1;
	double a,b,a1,b1;
	double x,y,z,sommeK,sommeM;

	gxy[0][0]=0;
	gxy[0][1]=-0.577350269189626;
	gxy[0][2]=-0.774596669241483;
	gxy[1][0]=0;
	gxy[1][1]=0.577350269189626;
	gxy[1][2]=0;
	gxy[2][0]=0;
	gxy[2][1]=0;
	gxy[2][2]=0.774596669241483;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;

	for (a=1;a<2;a++){
		for (b=1;b<2;b++){
				printf("boucle\n");
				a1=a*0.5*delta_x;
				b1=b*0.5*delta_y;


	for (i1=0;i1<4;i1++){
			for (j1=0;j1<4;j1++){
sommeK=0;
sommeM=0;
					for (i=0;i<3;i++){
						for (j=0;j<3;j++){

								x=a1*gxy[i][2];
								y=b1*gxy[j][2];
								wxy=wg[i][2]*wg[j][2];


								N[0]=(a1-x)*(b1-y)/(2*a1*2*b1);
								N[1]=(a1+x)*(b1-y)/(2*a1*2*b1);
								N[2]=(a1+x)*(b1+y)/(2*a1*2*b1);
								N[3]=(a1-x)*(b1+y)/(2*a1*2*b1);


								DN[0][0]=-(b1-y)/(2*a1*2*b1); DN[1][0]=-(a1-x)/(2*a1*2*b1);
								DN[0][1]=(b1-y)/(2*a1*2*b1);  DN[1][1]=-(a1+x)/(2*a1*2*b1);
								DN[0][2]=(b1+y)/(2*a1*2*b1);  DN[1][2]=(a1+x)/(2*a1*2*b1);
								DN[0][3]=-(b1+y)/(2*a1*2*b1); DN[1][3]=(a1-x)/(2*a1*2*b1);


								sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1])*wxy*a1*b1;
								sommeM=sommeM+(N[i1]*N[j1])*wxy*a1*b1;


						}
					}
					KE[i1+4*j1]=sommeK;
					ME[i1+4*j1]=sommeM;
					printf("%f ",KE[i1+4*j1]);
		}
			printf("\n");
	}


}
}
}

int main(int argc,char **args)
{

	printf("Debut programme \n");




printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,M1,U1,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end,start2,end2,Id,Istart,Iend,nlocal;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew,rnorm,KE1[16],ME1[16],KE2[16],ME2[16],KE3[16],ME3[16],KE4[16],ME4[16],ke[16],me[16],*array;
  PetscScalar y,conv,Tp1;
	char filename[200],u2[200];
	PetscInt indice_d;

	struct sysinfo info;

	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  PetscInt start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int icol,jcol,kcol,id,ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim,freq;
	int cpt1,cpt2,cpt3,cpt4;
	long unsigned int memoire_1,memoire_2,memoire_3,memoire_4,memoire_5,memoire_6,memoire_7;

	ni = 1000;
	nj = 1000;
	ni2=ni+1;

	freq=1;

	ndof=(ni+1)*(nj+1);
	nele=ni*nj;
	opt_it_max=1;
	alpha=1; // coeff pow compliance
	rmin=1.4;
	volfrac=1;
	double hconv=10;
	double delta,somme,somme1,somme2,delta_x1,delta_y1,delta_x2,delta_y2,delta_x3,delta_y3,delta_x4,delta_y4,delta_t,total_time,time;
	delta_x1=(double)0.1; //domaine de 10 mm par 10 mm ...
	delta_y1=(double)0.1;
	delta_x2=2*delta_x1; //domaine de 10 mm par 10 mm ...
	delta_y2=delta_y1;
	delta_x3=delta_x1; //domaine de 10 mm par 10 mm ...
	delta_y3=2*delta_y1;
	delta_x4=2*delta_x1; //domaine de 10 mm par 10 mm ...
	delta_y4=2*delta_y1;
	delta_t=1; // si delta t trop fort oscillations sur compliance
	total_time=1;
	int nbsteps;
	nbsteps=total_time/delta_t;
	double memory_cleared=1;

	KM_matrix_2D(delta_x1,delta_y1, KE1, ME1);
	KM_matrix_2D(delta_x2,delta_y2, KE2, ME2);
	KM_matrix_2D(delta_x3,delta_y3, KE3, ME3);
	KM_matrix_2D(delta_x4,delta_y4, KE4, ME4);

	//Medium properties
	int *boundary;
	boundary=(int *)calloc(nele,sizeof(int));

	double *F;
	F=(double *)calloc(4,sizeof(double));
	int *edofMat;
	edofMat=(int *)calloc(4,sizeof(int));
	domain_optim=(int *)calloc(nele,sizeof(int));
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp2,Tp3,Tp4,Tair;
	cp=(double *)calloc(nele,sizeof(double));
	lambda=(double *)calloc(nele,sizeof(double));
	derivee_c=(double *)calloc(nele,sizeof(double));
	derivee_lambda=(double *)calloc(nele,sizeof(double));
	rho=(double *)calloc(nele,sizeof(double));
	diff=(double *)calloc(nele,sizeof(double));
	c=(double *)calloc(nele,sizeof(double));
	rho_solide=1;
	lambda_solide=150;
	cp_solide=1;
	/*rho_solide=1;
	lambda_solide=1;
	cp_solide=1;*/


	rho_vide=1;
	lambda_vide=0.01;
	cp_vide=1000;
	//rho_vide=1000;
	//lambda_vide=100;
	//cp_vide=500;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	Qvol=0.3;//10;
	T0=0; //Initial temperature
	Tp1=0;//.f/(delta_x1*delta_y1); //Dirichlet temperature
	Tp2=0;///(delta_x1*delta_y1); //Dirichlet temperature
	Tp3=0;///(delta_x1*delta_y1); //Dirichlet temperature
	Tp4=0;///(delta_x1*delta_y1); //Dirichlet temperature
	Qsurf=0*1e6;
	Tair=30;
	penal=3;

		double *x1;
		double *Told;
		double *T_transient, *pos_q1_transient, *T_transient2, *derivee_T_transient,*adjoint_transient,*prod_KU_transient,*second_membre_transient,*derivee_T;
		double *q2,*q3,*q4,*pos_q1,*pos_q2,*pos_q3,*pos_q4, *compliance, *gradient, *lu;
		int *q1, *num_elem, *num_elem1, *num_elem2;
		double *gamma_x, *gamma_h;
		double *fonction_cout;
		fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
	//	solide=(double *)calloc(nele,sizeof(double));
		gamma_h=(double *)calloc(nele,sizeof(double));
		gamma_x=(double *)calloc(nele,sizeof(double));
		x1=(double *)calloc(ndof,sizeof(double));
		Told=(double *)calloc(ndof,sizeof(double));
		T_transient=(double *)calloc(nbsteps,sizeof(double));

	/*	double a1,b1,c1,d1;
		a1=10.f/12;
		b1=2.f/12;
		c1=-5.f/12;
		d1=-7.f/12;

		KE1[0]=2.f/3;
		KE1[1]=-1.f/6;
		KE1[2]=-1.f/3;
		KE1[3]=-1.f/6;

		KE1[4]=-1.f/6;
		KE1[5]=2.f/3;
		KE1[6]=-1.f/6;
		KE1[7]=-1.f/3;

		KE1[8]=-1.f/3;
		KE1[9]=-1.f/6;
		KE1[10]=2.f/3;
		KE1[11]=-1.f/6;

		KE1[12]=-1.f/6;
		KE1[13]=-1.f/3;
		KE1[14]=-1.f/6;
		KE1[15]=2.f/3;

		////////////////////////

		KE2[0]=a1;
		KE2[1]=b1;
		KE2[2]=c1;
		KE2[3]=d1;

		KE2[4]=b1;
		KE2[5]=a1;
		KE2[6]=d1;
		KE2[7]=c1;

		KE2[8]=c1;
		KE2[9]=d1;
		KE2[10]=a1;
		KE2[11]=b1;

		KE2[12]=d1;
		KE2[13]=c1;
		KE2[14]=b1;
		KE2[15]=a1;

	////////////////////////////

	KE3[0]=a1;
	KE3[1]=d1;
	KE3[2]=c1;
	KE3[3]=b1;

	KE3[4]=d1;
	KE3[5]=a1;
	KE3[6]=b1;
	KE3[7]=c1;

	KE3[8]=c1;
	KE3[9]=b1;
	KE3[10]=a1;
	KE3[11]=d1;

	KE3[12]=b1;
	KE3[13]=c1;
	KE3[14]=d1;
	KE3[15]=a1;

	///////////////////////////

	KE4[0]=KE1[0];
	KE4[1]=KE1[1];
	KE4[2]=KE1[2];
	KE4[3]=KE1[3];

	KE4[4]=KE1[4];
	KE4[5]=KE1[5];
	KE4[6]=KE1[6];
	KE4[7]=KE1[7];

	KE4[8]=KE1[8];
	KE4[9]=KE1[9];
	KE4[10]=KE1[10];
	KE4[11]=KE1[11];

	KE4[12]=KE1[12];
	KE4[13]=KE1[13];
	KE4[14]=KE1[14];
	KE4[15]=KE1[15];


	///////////////////////////

		ME1[0]=4.f/9;
		ME1[1]=2.f/9;
		ME1[2]=1.f/9;
		ME1[3]=2.f/9;

		ME1[4]=2.f/9;
		ME1[5]=4.f/9;
		ME1[6]=2.f/9;
		ME1[7]=1.f/9;

		ME1[8]=1.f/9;
		ME1[9]=2.f/9;
		ME1[10]=4.f/9;
		ME1[11]=2.f/9;

		ME1[12]=2.f/9;
		ME1[13]=1.f/9;
		ME1[14]=2.f/9;
		ME1[15]=4.f/9;

		///////////////////////////

			ME2[0]=2*ME1[0];
			ME2[1]=2*ME1[1];
			ME2[2]=2*ME1[2];
			ME2[3]=2*ME1[3];

			ME2[4]=2*ME1[4];
			ME2[5]=2*ME1[5];
			ME2[6]=2*ME1[6];
			ME2[7]=2*ME1[7];

			ME2[8]=2*ME1[8];
			ME2[9]=2*ME1[9];
			ME2[10]=2*ME1[10];
			ME2[11]=2*ME1[11];

			ME2[12]=2*ME1[12];
			ME2[13]=2*ME1[13];
			ME2[14]=2*ME1[14];
			ME2[15]=2*ME1[15];

			///////////////////////////

			ME3[0]=2*ME1[0];
			ME3[1]=2*ME1[1];
			ME3[2]=2*ME1[2];
			ME3[3]=2*ME1[3];

			ME3[4]=2*ME1[4];
			ME3[5]=2*ME1[5];
			ME3[6]=2*ME1[6];
			ME3[7]=2*ME1[7];

			ME3[8]=2*ME1[8];
			ME3[9]=2*ME1[9];
			ME3[10]=2*ME1[10];
			ME3[11]=2*ME1[11];

			ME3[12]=2*ME1[12];
			ME3[13]=2*ME1[13];
			ME3[14]=2*ME1[14];
			ME3[15]=2*ME1[15];

	///////////////////////////////

	ME4[0]=4*ME1[0];
	ME4[1]=4*ME1[1];
	ME4[2]=4*ME1[2];
	ME4[3]=4*ME1[3];

	ME4[4]=4*ME1[4];
	ME4[5]=4*ME1[5];
	ME4[6]=4*ME1[6];
	ME4[7]=4*ME1[7];

	ME4[8]=4*ME1[8];
	ME4[9]=4*ME1[9];
	ME4[10]=4*ME1[10];
	ME4[11]=4*ME1[11];

	ME4[12]=4*ME1[12];
	ME4[13]=4*ME1[13];
	ME4[14]=4*ME1[14];
	ME4[15]=4*ME1[15];*/



	AmgXSolver solver;
	//ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);

	 // ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	int test1;
	FILE *domain;
		domain=fopen("domain2d_140.txt","r");
	double somme_domain;

	double volfrac2;
	cpt=0;
	//for (k=0; k<nk; k++) {
	for (j=0; j<nj; j++) {
	for (i=0; i<ni; i++) {

					i0=I2D(ni,i,j);
				fscanf(domain,"%d\n",&test1);
				domain_optim[i0]=1;//test1;
		//					if(test==0) cpt=cpt+1;
	//			somme_domain=somme_domain+(1-test1);

			}
	}

fclose(domain);

printf("FICHIER OK \n");

	//Definition of boundary conditions


				for (j=0; j<nj; j++) {
	for (i=0; i<ni; i++) {
				i0=I2D(ni,i,j);

				if(i==0 || j==0 || i==(ni-1) || j==(nj-1) ){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;
				if(j==0 ) boundary[i0]=1;
				if(i==0 ) boundary[i0]=2;//2;
				if(j==(nj-1)) boundary[i0]=3;//3;
				if(i==(ni-1)) boundary[i0]=4;//4; }

			}

		}
	}

	for (i=0;i<ndof;i++){
		Told[i]=T0;
	}

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&D);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&Tn);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F2);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&T);CHKERRQ(ierr);
	/*ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
	printf("nlocal=%d\n",nlocal);*/

	ierr = VecSet(D,0);CHKERRQ(ierr);
	ierr = VecSet(Tn,0);CHKERRQ(ierr);
	ierr = VecSet(F1,0);CHKERRQ(ierr);
	ierr = VecSet(F2,0);CHKERRQ(ierr);
	ierr = VecSet(T,0);CHKERRQ(ierr);
		ierr = VecSet(b,0);CHKERRQ(ierr);

	double ratio;

				for (j=0; j<nj; j++) {
					for (i=0; i<ni; i++) {
									i0=I2D(ni,i,j);
											lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;c[i0]=c_solide;
									}
								}

					start = rank*(nele/size) + ((nele%size) < rank ? (nele%size) : rank);
				  end   = start + nele/size + ((nele%size) > rank);

					start1 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
				  end1   = start1 + ndof/size + ((ndof%size) > rank);

					const PetscInt *nindices;

					const PetscInt *nindices1;
					const PetscInt *nindices2;
					const PetscInt *nindices3;
					const PetscInt *nindices4;



					//DIRICHLET BOUNDARY CONDITION
						cpt1=0;
						cpt2=0;
						cpt3=0;
						cpt4=0;


						 for (j=0; j<nj; j++) {
				for (i=0; i<ni; i++) {
				i0=I2D(ni,i,j);

								 if(boundary[i0]==1) cpt1=cpt1+1;
								 if(boundary[i0]==2) cpt2=cpt2+1;
								 if(boundary[i0]==3) cpt3=cpt3+1;
								 if(boundary[i0]==4) cpt4=cpt4+1;


						 }
					 }

					 PetscInt *edofMat1,*edofMat2,*edofMat3,*edofMat4;
					 int nb_BC1,nb_BC2,nb_BC3,nb_BC4;
					 nb_BC1=cpt1;
					 nb_BC2=cpt2;
					 nb_BC3=cpt3;
					 nb_BC4=cpt4;


					PetscMalloc1(4*nb_BC1,&edofMat1);
					PetscMalloc1(4*nb_BC2,&edofMat2);
					PetscMalloc1(4*nb_BC3,&edofMat3);
					PetscMalloc1(4*nb_BC4,&edofMat4);


					IS is1,is2,is3,is4;
					PetscInt n1,n2,n3,n4;
					cpt1=0;
					cpt2=0;
					cpt3=0;
					cpt4=0;

					for (i0=start; i0<end; i0++) {


						jcol=(i0%(nj*ni))/ni;
						icol=	(i0%(nj*ni))%ni;
						id=(ni+1)*jcol+icol;

					edofMat[0]=id;
					edofMat[1]=id+1;
					edofMat[2]=id+1+(ni+1);
					edofMat[3]=id+(ni+1);


							 if(boundary[i0]==1){
								 edofMat1[2*cpt1]=edofMat[0];
								 edofMat1[1+2*cpt1]=edofMat[1];

									cpt1=cpt1+1;
							 }

							 if(boundary[i0]==2){
								 edofMat2[2*cpt2]=edofMat[0];
								 edofMat2[1+2*cpt2]=edofMat[3];
								 cpt2=cpt2+1;
							 }

							 if(boundary[i0]==3){
								 edofMat3[2*cpt3]=edofMat[2];
								 edofMat3[1+2*cpt3]=edofMat[3];
									cpt3=cpt3+1;
							 }

							 if(boundary[i0]==4){
								 edofMat4[2*cpt4]=edofMat[1];
								 edofMat4[1+2*cpt4]=edofMat[2];

								 cpt4=cpt4+1;
							 }

						 }

					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC1,edofMat1,PETSC_COPY_VALUES,&is1);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC2,edofMat2,PETSC_COPY_VALUES,&is2);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC3,edofMat3,PETSC_COPY_VALUES,&is3);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC4,edofMat4,PETSC_COPY_VALUES,&is4);


					ISSortRemoveDups(is1);
					ISSortRemoveDups(is2);
					ISSortRemoveDups(is3);
					ISSortRemoveDups(is4);

			  	ISGetLocalSize(is1,&n1);
					ISGetLocalSize(is2,&n2);
					ISGetLocalSize(is3,&n3);
					ISGetLocalSize(is4,&n4);

					ISGetIndices(is1,&nindices1);
					ISGetIndices(is2,&nindices2);
					ISGetIndices(is3,&nindices3);
					ISGetIndices(is4,&nindices4);


					//DIRICHLET BOUNDARY CONDITION
				/*		cpt=0;

						 for (j=0; j<nj; j++) {
				for (i=0; i<ni; i++) {
						 i0=I2D(ni,i,j);

								 if(boundary[i0]>0){
									 cpt=cpt+1;
								 }

						 }
					 }

					 PetscInt *edofMat2;
					 int nb_BC;
					 nb_BC=cpt;

					PetscMalloc1(4*nb_BC,&edofMat2);

					IS is;
					PetscInt n1,n2;
					cpt=0;
								for (j=0; j<nj; j++) {
					for (i=0; i<ni; i++) {
					 i0=I2D(ni,i,j);

							 if(boundary[i0]>0){
								 value=1;
								 edofMat2[4*cpt]=i+(ni+1)*j;
								 edofMat2[1+4*cpt]=i+1+(ni+1)*j;
								 edofMat2[2+4*cpt]=i+1+(ni+1)*(j+1);
								 edofMat2[3+4*cpt]=i+(ni+1)*(j+1);
									cpt=cpt+1;
							 }
						 }
						}


					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC,edofMat2,PETSC_COPY_VALUES,&is);
					 ISGetLocalSize(is,&n1);
					ISSortRemoveDups(is);
					 ISGetLocalSize(is,&n2);

						ISGetIndices(is,&nindices);

					 printf("n1=%d et n2=%d \n",n1,n2);*/

											 //sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared1=%ld\n",memoire_1);

							 ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/AMG_CLASSICAL_AGGRESSIVE_L1.json" ); CHKERRQ(ierr);
							 //ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/JACOBI.json" ); CHKERRQ(ierr);
					 ierr = MatCreate(PETSC_COMM_WORLD,&K1);CHKERRQ(ierr);
					 ierr = MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
					 ierr = MatSetFromOptions(K1);CHKERRQ(ierr);
					 ierr = MatSetUp(K1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(K1,50,NULL,50,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(K1,50,NULL);CHKERRQ(ierr);
					 //printf("apres malloc K1\n"); getchar();

					 ierr = MatCreate(PETSC_COMM_WORLD,&M1);CHKERRQ(ierr);
					 ierr = MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
					 ierr = MatSetFromOptions(M1);CHKERRQ(ierr);
					 ierr = MatSetUp(M1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(M1,50,NULL,50,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(M1,50,NULL);CHKERRQ(ierr);
					 //printf("apres malloc M1\n"); getchar();

				// sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared2=%ld\n",memoire_1);

					 //printf("Istart=%d, Iend=%d \n",Istart,Iend);

					 // Loop over elements
					 for (i0=start; i0<end; i0++) {

					 kcol=i0/(ni*nj);
					 jcol=(i0%(nj*ni))/ni;
					 //printf("jcol=%d\n",jcol);
					 icol=	(i0%(nj*ni))%ni;
					 id=(ni+1)*jcol+icol;

					 edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);

					 if(domain_optim[i0]==1){
							 for (l=0;l<4*4;l++){
							 ke[l]=KE1[l]*lambda[i0];
							 me[l]=ME1[l]*c[i0]/delta_t;
									 }
						 // Add values to the sparse matrix
						 ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
						 ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
					 //	}
				 		}

						if(domain_optim[i0]==2){
								for (l=0;l<4*4;l++){
								ke[l]=KE2[l]*lambda[i0];
								me[l]=ME2[l]*c[i0]/delta_t;
										}
							// Add values to the sparse matrix
							ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
							ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
						//	}
						 }

						 if(domain_optim[i0]==3){
								 for (l=0;l<4*4;l++){
								 ke[l]=KE3[l]*lambda[i0];
								 me[l]=ME3[l]*c[i0]/delta_t;
										 }
							 // Add values to the sparse matrix
							 ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
							 ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
						 //	}
							}

							if(domain_optim[i0]==4){
									for (l=0;l<4*4;l++){
									ke[l]=KE4[l]*lambda[i0];
									me[l]=ME4[l]*c[i0]/delta_t;
											}
								// Add values to the sparse matrix
								ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
								ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
							//	}
							 }


					 }

					 ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


					 ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

					/* for (i0=start; i0<end; i0++) {

					 kcol=i0/(ni*nj);
					 jcol=(i0%(nj*ni))/ni;
					 //printf("jcol=%d\n",jcol);
					 icol=	(i0%(nj*ni))%ni;
					 id=(ni+1)*jcol+icol;

					 edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);

					 if(boundary[i0]==1) {
						 indice_d=edofMat[0];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
						 indice_d=edofMat[1];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
					 }

					 if(boundary[i0]==2) {
						 indice_d=edofMat[0];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
						 indice_d=edofMat[3];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
					 }

					 if(boundary[i0]==3) {
						 indice_d=edofMat[2];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL);MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
						 indice_d=edofMat[3];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL);MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
					 }

					 if(boundary[i0]==4) {
						 indice_d=edofMat[1];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
						 indice_d=edofMat[2];	 MatZeroRows(K1,1,&indice_d,1,NULL,NULL); MatZeroRows(M1,1,&indice_d,1,NULL,NULL);
					 }

				 }
*/


ierr = MatZeroRows(K1,n1,nindices1,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(M1,n1,nindices1,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(K1,n2,nindices2,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(M1,n2,nindices2,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(K1,n3,nindices3,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(M1,n3,nindices3,1,0,0);CHKERRQ(ierr);
ierr = MatZeroRows(K1,n4,nindices4,1,0,0);CHKERRQ(ierr);

					 ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
					 ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);

					 //ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
					 //ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);


int compteur=0;



	while(compteur<nbsteps ){

	printf("Load \n");

	for (i0=start; i0<end; i0++) {

		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*jcol+icol;

		//if(jcol<10+0.5*nj && jcol>-10+0.5*nj && icol<10+0.5*ni  && icol>-10+0.5*ni ){
	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);

 if(domain_optim[i0]==1){
	 ratio=delta_x1*delta_y1;
		  F[0]=ratio*Qvol;//
			F[1]=ratio*Qvol;//
			F[2]=ratio*Qvol;//
			F[3]=ratio*Qvol;//


	ierr = VecSetValues(b,4,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	}

	if(domain_optim[i0]==2){
		ratio=delta_x2*delta_y2;

		F[0]=ratio*Qvol;//
		F[1]=ratio*Qvol;//
		F[2]=ratio*Qvol;//
		F[3]=ratio*Qvol;//


	 ierr = VecSetValues(b,4,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	 }

	 if(domain_optim[i0]==3){
		 ratio=delta_x3*delta_y3;
		 F[0]=ratio*Qvol;//
		 F[1]=ratio*Qvol;//
		 F[2]=ratio*Qvol;//
		 F[3]=ratio*Qvol;//

		ierr = VecSetValues(b,4,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
		}

		if(domain_optim[i0]==4){
			ratio=delta_x4*delta_y4;
			F[0]=ratio*Qvol;//
			F[1]=ratio*Qvol;//
			F[2]=ratio*Qvol;//
			F[3]=ratio*Qvol;//


	 	 ierr = VecSetValues(b,4,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	 }
//}
	//}
}
	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);


	//Dirichlet condition

	for (i0=start; i0<end; i0++) {

		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*jcol+icol;

	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);


	if(boundary[i0]==4){

		//ierr = VecSetValues(b,1,&edofMat[0],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[1],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[2],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
		//ierr = VecSetValues(b,1,&edofMat[3],&Tp4,INSERT_VALUES);CHKERRQ(ierr);

	}

	if(boundary[i0]==3){

		//ierr = VecSetValues(b,1,&edofMat[0],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
		//ierr = VecSetValues(b,1,&edofMat[1],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[2],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[3],&Tp3,INSERT_VALUES);CHKERRQ(ierr);

	}

				if(boundary[i0]==2){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[1],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(boundary[i0]==1){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}
	}

	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);

	//Prod mat vecteur
//	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);

	//VecAXPY(b,1,F1);

 //Set exact solution; then compute right-hand-side vector.
printf("Solver beginning \n");


ierr = solver.setA(K1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
VecSet(b,0);
compteur++;

} //Fin boucle transient


ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);


start2 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
end2   = start2 + ndof/size + ((ndof%size) > rank);

printf("size=%d et rank=%d\n",size,rank);
printf("start2=%d et end2=%d\n",start2,end2);
FILE *test0_f,*test1_f,*test2_f,*test3_f;

	test0_f =       fopen("temp_2d_100", "w"); memset(filename, 0, sizeof(filename));

	double minT=-1;
	for(i=start2;i<end2;i++) {
		VecGetValues(x,1,&i,&y); fprintf(test0_f,"%f\n",y);
		if(y>minT) minT=y;
	}
	printf("Tmax=%f\n",minT);

MPI_Barrier(PETSC_COMM_WORLD);

solver.finalize();


printf("Fin boucle optim \n");


ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);

//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);

return ierr;
}
