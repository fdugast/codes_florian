#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <cublas.h>
#include <cublas_v2.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define NDim 3
#define MNE 8

#define BLOCKSIZE1 32
#define BLOCKSIZE2 256

#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)
#define I2D(ni,i,j) (((ni)*(j)) + i)

const char *sSDKname = "conjugateGradientMultiDeviceCG";

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

void KM_matrix(double delta_x, double delta_y,double delta_z, double *Ke,double *Me, double *He1, double *He2, double *He3,
  double *He4, double *He5, double *He6, double *Fe1, double *Fe2,double *Fe3, double *Fe4,double *Fe5, double *Fe6, double *Qe1){
//  double N[8];
  double DN[3][8],N[8];
  double gxyz[3][3];
  double wg[3][3],wxyz;
  int i,j,k,i1,j1;
  double a,b,c;
  double x,y,z,sommeK,sommeM,sommeHe1,sommeHe2,sommeHe3,sommeHe4,sommeHe5,sommeHe6, sommeFe1,sommeFe2,sommeFe3,sommeFe4,sommeFe5,sommeFe6;
  double sommeQe1;

  //gxyz[0][0]=0;
  //gxyz[0][1]=-0.577350269189626;
  gxyz[0][2]=-sqrt(3.f/5);
  //gxyz[1][0]=0;
  //gxyz[1][1]=0.577350269189626;
  gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
  gxyz[2][2]=sqrt(3.f/5);

  wg[0][0]=2;
  wg[0][1]=1;
  wg[0][2]=5.f/9;
  wg[1][0]=0;
  wg[1][1]=1;
  wg[1][2]=8.f/9;
  wg[2][0]=0;
  wg[2][1]=0;
  wg[2][2]=5.f/9;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;
        //printf("boucle\n");


  for (i1=0;i1<8;i1++){
      for (j1=0;j1<8;j1++){
sommeK=0;
sommeM=0;
          for (i=0;i<3;i++){
            for (j=0;j<3;j++){
              for (k=0;k<3;k++){

                x=gxyz[i][2];
                y=gxyz[j][2];
                z=gxyz[k][2];
                wxyz=wg[i][2]*wg[j][2]*wg[k][2];

                N[0]=(1-x)*(1-y)*(1-z)/8;
                N[1]=(1+x)*(1-y)*(1-z)/8;
                N[2]=(1+x)*(1+y)*(1-z)/8;
                N[3]=(1-x)*(1+y)*(1-z)/8;

                N[4]=(1-x)*(1-y)*(1+z)/8;
                N[5]=(1+x)*(1-y)*(1+z)/8;
                N[6]=(1+x)*(1+y)*(1+z)/8;
                N[7]=(1-x)*(1+y)*(1+z)/8;

                DN[0][0]=-(1-y)*(1-z)*(1/a)*0.125;   DN[1][0]=-(1-x)*(1-z)*(1/b)*0.125;    DN[2][0]=-(1-x)*(1-y)*(1/c)*0.125;
                DN[0][1]=(1-y)*(1-z)*(1/a)*0.125;    DN[1][1]=-(1+x)*(1-z)*(1/b)*0.125;    DN[2][1]=-(1+x)*(1-y)*(1/c)*0.125;
                DN[0][2]=(1+y)*(1-z)*(1/a)*0.125;    DN[1][2]=(1+x)*(1-z)*(1/b)*0.125;     DN[2][2]=-(1+x)*(1+y)*(1/c)*0.125;
                DN[0][3]=-(1+y)*(1-z)*(1/a)*0.125;   DN[1][3]=(1-x)*(1-z)*(1/b)*0.125;     DN[2][3]=-(1-x)*(1+y)*(1/c)*0.125;
                DN[0][4]=-(1-y)*(1+z)*(1/a)*0.125;   DN[1][4]=-(1-x)*(1+z)*(1/b)*0.125;    DN[2][4]=(1-x)*(1-y)*(1/c)*0.125;
                DN[0][5]=(1-y)*(1+z)*(1/a)*0.125;    DN[1][5]=-(1+x)*(1+z)*(1/b)*0.125;    DN[2][5]=(1+x)*(1-y)*(1/c)*0.125;
                DN[0][6]=(1+y)*(1+z)*(1/a)*0.125;    DN[1][6]=(1+x)*(1+z)*(1/b)*0.125;     DN[2][6]=(1+x)*(1+y)*(1/c)*0.125;
                DN[0][7]=-(1+y)*(1+z)*(1/a)*0.125;   DN[1][7]=(1-x)*(1+z)*(1/b)*0.125;     DN[2][7]=(1-x)*(1+y)*(1/c)*0.125;

                sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1]+DN[2][i1]*DN[2][j1])*wxyz*a*b*c;
                sommeM=sommeM+(N[i1]*N[j1])*wxyz*a*b*c;

              }
            }
          }
          Ke[i1+8*j1]=sommeK;
          Me[i1+8*j1]=sommeM;
          //printf("%lg ",Me[i1+8*j1]);
    }
      //printf("\n");
  }

//printf("\n \n");

//HE1
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe1=sommeHe1+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He1[i1+8*j1]=sommeHe1;
        //printf("%lg ",He1[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE2
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe2=sommeHe2+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He2[i1+8*j1]=sommeHe2;
        //printf("%lg ",He2[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE3
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe3=sommeHe3+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He3[i1+8*j1]=sommeHe3;
        //printf("%lg ",He3[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE4
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe4=sommeHe4+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He4[i1+8*j1]=sommeHe4;
        //printf("%lg ",He4[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");


//HE5
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe5=sommeHe5+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He5[i1+8*j1]=sommeHe5;
        //printf("%lg ",He5[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE6
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe6=sommeHe6+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He6[i1+8*j1]=sommeHe6;
        //printf("%lg ",He6[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe1=sommeFe1+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe1[i1]=sommeFe1;
        //printf("%lg ",Fe1[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE2
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe2=sommeFe2+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe2[i1]=sommeFe2;
        //printf("%lg ",Fe2[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE3
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe3=sommeFe3+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe3[i1]=sommeFe3;
        //printf("%lg ",Fe3[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE4
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe4=sommeFe4+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe4[i1]=sommeFe4;
        //printf("%lg ",Fe4[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE5
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe5=sommeFe5+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe5[i1]=sommeFe5;
        //printf("%lg ",Fe5[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE6
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe6=sommeFe6+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe6[i1]=sommeFe6;
        //printf("%lg ",Fe6[i1]);
  //}
    //printf("\n");
}

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeQe1=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              //wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeQe1=sommeQe1+N[i1]*wxyz*a*b*c;

            }
          }
        }
        Qe1[i1]=sommeQe1;
      //  printf("%lg ",Qe1[i1]);
  //}
    //printf("\n");
}

}

void KM_matrix_2D(double delta_x, double delta_y, double *Ke,double *Me, double *He1, double *He2, double *He3,
  double *He4, double *Fe1, double *Fe2,double *Fe3, double *Fe4, double *Qe1){
//  double N[8];
  double DN[2][4],N[4];
  double gxyz[3][3];
  double wg[3][3],wxyz;
  int i,j,i1,j1;
  double a,b;
  double x,y,sommeK,sommeM,sommeHe1,sommeHe2,sommeHe3,sommeHe4, sommeFe1,sommeFe2,sommeFe3,sommeFe4;
  double sommeQe1;

  //gxyz[0][0]=0;
  //gxyz[0][1]=-0.577350269189626;
  gxyz[0][2]=-sqrt(3.f/5);
  //gxyz[1][0]=0;
  //gxyz[1][1]=0.577350269189626;
  gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
  gxyz[2][2]=sqrt(3.f/5);

  wg[0][0]=2;
  wg[0][1]=1;
  wg[0][2]=5.f/9;
  wg[1][0]=0;
  wg[1][1]=1;
  wg[1][2]=8.f/9;
  wg[2][0]=0;
  wg[2][1]=0;
  wg[2][2]=5.f/9;

a=0.5*delta_x;
b=0.5*delta_y;
//c=0.5*delta_z;
        //printf("boucle\n");


  for (i1=0;i1<4;i1++){
      for (j1=0;j1<4;j1++){
sommeK=0;
sommeM=0;
          for (i=0;i<3;i++){
            for (j=0;j<3;j++){
              //for (k=0;k<3;k++){

                x=gxyz[i][2];
                y=gxyz[j][2];
                //z=gxyz[k][2];
                wxyz=wg[i][2]*wg[j][2];//*wg[k][2];

                N[0]=(1-x)*(1-y)/4;
                N[1]=(1+x)*(1-y)/4;
                N[2]=(1+x)*(1+y)/4;
                N[3]=(1-x)*(1+y)/4;


                DN[0][0]=-(1-y)*(1/a)*0.25;   DN[1][0]=-(1-x)*(1/b)*0.25;
                DN[0][1]=(1-y)*(1/a)*0.25;    DN[1][1]=-(1+x)*(1/b)*0.25;
                DN[0][2]=(1+y)*(1/a)*0.25;    DN[1][2]=(1+x)*(1/b)*0.25;
                DN[0][3]=-(1+y)*(1/a)*0.25;   DN[1][3]=(1-x)*(1/b)*0.25;


                sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1])*wxyz*a*b;
                sommeM=sommeM+(N[i1]*N[j1])*wxyz*a*b;

              //}
            }
          }
          Ke[i1+4*j1]=sommeK;
          Me[i1+4*j1]=sommeM;
          //printf("%lg ",Me[i1+8*j1]);
    }
      //printf("\n");
  }

//printf("\n \n");

//HE1
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;


              sommeHe1=sommeHe1+(N[i1]*N[j1])*wxyz*b;

          //  }
          }
        //}
        He1[i1+4*j1]=sommeHe1;
        //printf("%lg ",He1[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE2
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe2=sommeHe2+(N[i1]*N[j1])*wxyz*b;

            //}
          }
        //}
        He2[i1+4*j1]=sommeHe2;
        //printf("%lg ",He2[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE3
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe3=sommeHe3+(N[i1]*N[j1])*wxyz*a;

          //  }
          }
        //}
        He3[i1+4*j1]=sommeHe3;
        //printf("%lg ",He3[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE4
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe4=sommeHe4+(N[i1]*N[j1])*wxyz*a;

          //  }
          }
        //}
        He4[i1+4*j1]=sommeHe4;
        //printf("%lg ",He4[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//FE1
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
          //  for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
            //  z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe1=sommeFe1+N[i1]*wxyz*b;

            //}
          }
        //}
        Fe1[i1]=sommeFe1;
        //printf("%lg ",Fe1[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE2
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe2=sommeFe2+N[i1]*wxyz*b;

            //}
          }
        //}
        Fe2[i1]=sommeFe2;
        //printf("%lg ",Fe2[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE3
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
            //  z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe3=sommeFe3+N[i1]*wxyz*a;

            //}
          }
        //}
        Fe3[i1]=sommeFe3;
        //printf("%lg ",Fe3[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE4
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe4=sommeFe4+N[i1]*wxyz*a;

            //}
          }
        //}
        Fe4[i1]=sommeFe4;
        //printf("%lg ",Fe4[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");


//FE1
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeQe1=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              wxyz=wg[i][2]*wg[j][2];//*wg[k][2];
              //wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeQe1=sommeQe1+N[i1]*wxyz*a*b;

            //}
          }
        }
        Qe1[i1]=sommeQe1;
        printf("%lg ",Qe1[i1]);
  //}
    //printf("\n");
}

}

void domain_definition (int ni,int nj, int nk,int base_height, int *map_elements, int *domain)
{
  int i,j,k,i0;
for (k=0; k<nk; k++) {
    for (j=0; j<nj; j++) {
        for (i=0; i<ni; i++) {

        i0=I3D(ni,nj,i,j,k);
        //printf("i0=%d\n",i0);
        domain[i0]=1;
       if(k<base_height ) {
          domain[i0]=0;
          map_elements[i0]=i0;
        }
        else{
              //if(i<(100+10) && i>=(100-10) && j<(100+10) && j>=(100-10) && k<80){
          /*if(j==2 && (i==1 || i==4 ))      domain[i0]=0;
          if(j==3 && (i==1 || i==2 || i==3 || i==4))      domain[i0]=0;
          if(j==4 && (i==2 || i==3 ))      domain[i0]=0;*/

          //if(j<3 && j>=2){
            if(i<35 && j<35 && i>=15 && j>=15 ) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
        //  }
          /*if(j<4 && j>=3){
            //if(i<2 && i>=1) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
            if(i<5 && i>=1) domain[i0]=0;

          }
          if(j<5 && j>=4){
            //if(i<2 && i>=1) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
            if(i<4 && i>=2) domain[i0]=0;

          }*/


          }
//domain_optim[i0]=0;

    }
  }
}
}

  // Set residual to zero
//void SetResZero(double *r, int numNodes)
//{
//	memset(r,0.0,sizeof(numNodes));
//}

void active_domain(int ni, int nj, int numElems, int base_height, int layer_height, int *domain, int *domain_optim, int *pos_heat)
{
  //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int k; int e=0;

  while (e<numElems){

    k=e/(ni*nj);
    //i=e%ni;

    if(k<base_height) {
       domain_optim[e]=0;
     }
     else{
           //if(i<(100+10) && i>=(100-10) && j<(100+10) && j>=(100-10) && k<80){
       if(k<=layer_height){
         domain_optim[e]=domain[e];
       }
       else{
         domain_optim[e]=1;
       }
     }
     if(k==layer_height && domain[e]==0) pos_heat[e]=1;
      e++;
  }
}

void Boundary_Conditions1(int ni, int nj, int nk, int numElems, int base_height,int layer_height, int *domain_optim,int *convnodes_x,int *convnodes_y, int *convnodes_z,
  double *hconv1,double *hconv2,double *hconv3,double *hconv4, double *hconv5,double *hconv6){

    //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
    int e=0;
    int i,j,k,id,i1,edofMat[MNE];

    while (e<numElems){

      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id=(ni+1)*(nj+1)*k+(ni+1)*j+i;

      edofMat[0]=id;
      edofMat[1]=id+1;
      edofMat[2]=id+1+(ni+1);
      edofMat[3]=id+(ni+1);
      edofMat[4]=id + (ni+1)*(nj+1);
      edofMat[5]=id+1 + (ni+1)*(nj+1);
      edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
      edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

      if(i==0 && domain_optim[e]==0)       {hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1;   convnodes_x[edofMat[4]]=1;    convnodes_x[edofMat[7]]=1; }
      if(i==(ni-1) && domain_optim[e]==0)  {hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2;   convnodes_x[edofMat[5]]=2;    convnodes_x[edofMat[6]]=2;}
      if(j==0 && domain_optim[e]==0)       {hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   convnodes_y[edofMat[4]]=3;    convnodes_y[edofMat[5]]=3; }
      if(j==(nj-1) && domain_optim[e]==0)  {hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   convnodes_y[edofMat[6]]=4;    convnodes_y[edofMat[7]]=4;}
      if(k==0 && domain_optim[e]==0)       {hconv5[e]=5;    convnodes_z[edofMat[0]]=5;      convnodes_z[edofMat[1]]=5;   convnodes_z[edofMat[2]]=5;    convnodes_z[edofMat[3]]=5;}
      //if(k==(nk-1) && domain_optim[e]==0)  {hconv6[e]=50;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
      i1=e-1;
      if(i1>0 && i!=0 && j!=0){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1; convnodes_x[edofMat[4]]=1;    convnodes_x[edofMat[7]]=1;
        }
      }
      i1=e+1;
      if(i1<ni*nj*nk && i!=(ni-1) && j!=(nj-1)){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2; convnodes_x[edofMat[5]]=2;    convnodes_x[edofMat[6]]=2;
        }
      }
      i1=e-ni;
      if(i1>0 && i!=0 && j!=0){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   convnodes_y[edofMat[4]]=3;    convnodes_y[edofMat[5]]=3;
        }
      }
      i1=e+ni;
      if(i1<ni*nj*nk && i!=(ni-1) && j!=(nj-1)){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   convnodes_y[edofMat[6]]=4;    convnodes_y[edofMat[7]]=4;
        }
      }
      i1=e+ni*nj;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          if (k==(base_height-1)) {hconv6[e]=5;    convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;   }
          if (k==layer_height) {hconv6[e]=50;    convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;  }
        }
      }
      else{
        if(domain_optim[e]==0 ) {
          hconv6[e]=50;    convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;
        }
      }

      /*i1=e+ni*nj;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          if(k<base_height)       {hconv6[e]=5;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
          if(k>=base_height)       {hconv6[e]=55;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
        }
      }*/
      e++;
    }
  }


  void Boundary_Conditions2(int ni, int nj, int numElems, int *domain_optim,int *convnodes_x,int *convnodes_y, int *convnodes_z,
    double *hconv1,double *hconv2,double *hconv3,double *hconv4, double *hconv5, double *hconv6, double Tinf, double Tinf2, double *b1,
    double *Fe1,double *Fe2,double *Fe3,double *Fe4,double *Fe5,double *Fe6,double *Fe1_b,double *Fe2_b,double *Fe3_b,double *Fe4_b,double *Fe5_b,double *Fe6_b, int base_height ){

      //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
      int e=0;
      int i,j,k,l,id,edofMat[MNE];

      while (e<numElems){

        k=e/(ni*nj);
        j=(e%(nj*ni))/ni;
        i=(e%(nj*ni))%ni;

        id=(ni+1)*(nj+1)*k+(ni+1)*j+i;

        edofMat[0]=id;
        edofMat[1]=id+1;
        edofMat[2]=id+1+(ni+1);
        edofMat[3]=id+(ni+1);
        edofMat[4]=id + (ni+1)*(nj+1);
        edofMat[5]=id+1 + (ni+1)*(nj+1);
        edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
        edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

        for(l=0;l<MNE;l++){
                if(convnodes_x[edofMat[l]]==1)  {
                //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                  if(e<ni*nj*base_height){
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv1[e]*Tinf*Fe1_b[l];
                  }
                  else{
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv1[e]*Tinf*Fe1[l];
                  }
                  //atomicAdd(&b1[edofMat[l]], hconv1[e]*Tinf*Fe1[l]);

                }
                if(convnodes_x[edofMat[l]]==2) {
                  //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                  if(e<ni*nj*base_height){
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv2[e]*Tinf*Fe2_b[l];
                 }
                 else{
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv2[e]*Tinf*Fe2[l];
                 }
                    //atomicAdd(&b1[edofMat[l]], hconv2[e]*Tinf*Fe2[l]);
                 }

                 if(convnodes_y[edofMat[l]]==3)  {
                 //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                 if(e<ni*nj*base_height){
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv3[e]*Tinf*Fe3_b[l];
                 }
                 else{
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv3[e]*Tinf*Fe3[l];
                   //atomicAdd(&b1[edofMat[l]], hconv3[e]*Tinf*Fe3[l]);
                 }
               }

                 if(convnodes_y[edofMat[l]]==4) {
                   //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                   if(e<ni*nj*base_height){
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv4[e]*Tinf*Fe4_b[l];
                  }
                  else{
                      b1[edofMat[l]]=b1[edofMat[l]]+hconv4[e]*Tinf*Fe4[l];
                    //atomicAdd(&b1[edofMat[l]], hconv4[e]*Tinf*Fe4[l]);
                  }
                }

                  if(convnodes_z[edofMat[l]]==5)  {
                  //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                  if(e<ni*nj*base_height){
                    if(k==0){
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv5[e]*Tinf2*Fe5_b[l];
                  }
                  else{
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv5[e]*Tinf*Fe5_b[l];
                  }
                  }
                  else{
                      b1[edofMat[l]]=b1[edofMat[l]]+hconv5[e]*Tinf*Fe5[l];
                  }
                    //atomicAdd(&b1[edofMat[l]], hconv5[e]*Tinf*Fe5[l]);

                  }
                  if(convnodes_z[edofMat[l]]==6) {
                    //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                    if(e<ni*nj*base_height){
                     b1[edofMat[l]]=b1[edofMat[l]]+hconv6[e]*Tinf*Fe6_b[l];
                   }
                   else{
                     b1[edofMat[l]]=b1[edofMat[l]]+hconv6[e]*Tinf*Fe6[l];
                   }
                    // atomicAdd(&b1[edofMat[l]], hconv6[e]*Tinf*Fe6[l]);
                   }
            }
            e++;
      }
    }

  int new_elements_counter(int ni, int nj, int nk, int layer_height, int base_height,int *domain_optim)
    {
    int l1,l2,i,j,k,i0;
    if(layer_height==base_height){
      l1=layer_height;
      l2=layer_height;
    }
    else{
      l1=layer_height-1;
      l2=layer_height;
    }
    //printf("l1=%d et l2=%d\n",l1,l2);
    //New element counter
    int new_elements=0;
    for (k=0; k<nk; k++) {
      for (j=0; j<nj; j++) {
        for (i=0; i<ni; i++) {

          if(k>=l1 && k<=l2){
              i0=I3D(ni,nj,i,j,k);
        if(domain_optim[i0]==0){
          new_elements++;
        }
      }
    }
  }
}
  return new_elements;
}

void ele_id_calculation(int ni, int nj, int nk, int base_height, int layer_height, int *domain_optim, int *ele_id)
{
  int i,j,k,i0,l1,l2;
  int cpt=0;

  if(layer_height==base_height){
    l1=layer_height;
    l2=layer_height;
  }
  else{
    l1=layer_height-1;
    l2=layer_height;
  }
  for (k=0; k<nk; k++) {
    for (j=0; j<nj; j++) {
      for (i=0; i<ni; i++) {
          if(k>=l1 && k<=l2){
            i0=I3D(ni,nj,i,j,k);
      if(domain_optim[i0]==0){
      ele_id[cpt]=i0;
      //printf("ele_id[%d]=%d\n",cpt,ele_id[cpt]);
      cpt++;
      }
  }
}
}
}
}


__global__ void Mat_Properties(double *U, int ni, int nj, double *kcond, double *rho, double *c, int *edof, int numElems,  int base_height)
{

  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,id1,edofMat2[MNE];
  double Te;

  if (e<numElems){

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
  }

    Te=0.125*(U[edofMat2[0]]+U[edofMat2[1]]+U[edofMat2[2]]+U[edofMat2[3]]+U[edofMat2[4]]+U[edofMat2[5]]+U[edofMat2[6]]+U[edofMat2[7]]);

      if(Te<=20) c[e]=421;
      if(Te>20 && Te<=100)  c[e] =(double)421+(442-421)*(Te-20)/(100-20);
      if(Te>100 && Te<=200) c[e] =(double)442+(453-442)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) c[e] =(double)453+(472-453)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) c[e] =(double)472+(481-472)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) c[e] =(double)481+(502-481)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) c[e] =(double)502+(527-502)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) c[e] =(double)527+(562-527)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) c[e] =(double)562+(606-562)*(Te-700)/(800-700);
      if(Te>800 && Te<=850) c[e] =(double)606+(628-606)*(Te-800)/(850-800);
      if(Te>850 && Te<=900) c[e] =(double)628+(636-628)*(Te-850)/(900-850);
      if(Te>900 && Te<=1000)  c[e]=(double)636+(647-636)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) c[e]=(double)647+(651-647)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1500) c[e]=(double)651+(652-651)*(Te-1100)/(1500-1100);
      if(Te>1500) c[e]=652;

      if(Te<=22) kcond[e]=11.9;
      if(Te>22 && Te<=233)  kcond[e] =(double)11.9+(13.7-11.9)*(Te-22)/(233-22);
      if(Te>233 && Te<=448) kcond[e] =(double)13.7+(16.9-13.7)*(Te-233)/(448-233);
      if(Te>448 && Te<=657) kcond[e] =(double)16.9+(21.7-16.9)*(Te-448)/(657-448);
      if(Te>657 && Te<=866) kcond[e] =(double)21.7+(25.6-21.7)*(Te-657)/(866-657);
      if(Te>866 && Te<=1079) kcond[e] =(double)25.6+(22.9-25.6)*(Te-866)/(1079-866);
      if(Te>1079 && Te<=1289) kcond[e] =(double)22.9+(19.1-22.9)*(Te-1079)/(1289-1079);
      if(Te>1289 && Te<=1500) kcond[e] =(double)19.1+(17.7-19.1)*(Te-1289)/(1500-1289);
      if(Te>1500) kcond[e]=17.7;


      if(Te<=20) rho[e]=8220;
      if(Te>20 && Te<=227)     rho[e] =(double)8220+(8121-8220)*(Te-20)/(227-20);
      if(Te>227 && Te<=427)    rho[e] =(double)8121+(8121-8048)*(Te-227)/(427-227);
      if(Te>427 && Te<=727)    rho[e] =(double)8048+(8121-8048)*(Te-427)/(727-427);
      if(Te>727 && Te<=927)    rho[e] =(double)7961+(8121-8048)*(Te-727)/(927-727);
      if(Te>927 && Te<=1127)   rho[e] =(double)7875+(8121-8048)*(Te-927)/(1127-927);
      if(Te>1127 && Te<=1260)  rho[e] =(double)7787+(8121-8048)*(Te-1127)/(1260-1127);
      if(Te>1260 && Te<=1344)  rho[e] =(double)7733+(8121-8048)*(Te-1260)/(1344-1260);
      if(Te>1344 && Te<=1450)  rho[e] =(double)7579+(8121-8048)*(Te-1344)/(1450-1344);
      if(Te>1450 && Te<=1527)  rho[e] =(double)7488+(8121-8048)*(Te-1450)/(1527-1450);
      if(Te>1527 && Te<=1827)  rho[e] =(double)7488+(8121-8048)*(Te-1527)/(1827-1527);
      if(Te>1827) rho[e]=7341;
    }

}

__global__ void Mat_Properties_Qian(double *U, int ni, int nj, double *kcond, double *rho, double *c, int *edof, int numElems,  int base_height)
{

  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,id1,edofMat2[MNE];
  double Te;

  if (e<numElems){

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
  }

    Te=0.125*(U[edofMat2[0]]+U[edofMat2[1]]+U[edofMat2[2]]+U[edofMat2[3]]+U[edofMat2[4]]+U[edofMat2[5]]+U[edofMat2[6]]+U[edofMat2[7]]);


      if(Te<=25) c[e]=425;
      if(Te>25 && Te<=100)  c[e] =(double)425+(441.5-425)*(Te-25)/(100-25);
      if(Te>100 && Te<=200) c[e] =(double)441.5+(459.5-441.5)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) c[e] =(double)459.5+(476-459.5)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) c[e] =(double)476+(492-476)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) c[e] =(double)492+(508.5-492)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) c[e] =(double)508.5+(604-508.5)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) c[e] =(double)604+(653-604)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) c[e] =(double)653+(707-653)*(Te-700)/(800-700);
      if(Te>800 && Te<=900) c[e] =(double)707+(625-707)*(Te-800)/(900-800);
      if(Te>900 && Te<=1000)  c[e]=(double)625+(597-625)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) c[e]=(double)597+(615-597)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1200) c[e]=(double)615+(636-615)*(Te-1100)/(1200-1100);
      if(Te>1200 && Te<=1300) c[e]=(double)636+(725-636)*(Te-1200)/(1300-1200);
      if(Te>1300 && Te<=3000) c[e]=(double)725+(725-725)*(Te-1300)/(3000-1300);
      if(Te>3000) c[e]=725;

        if(e<base_height*ni*nj){
      if(Te<=25) kcond[e]=7.98;
      if(Te>25 && Te<=100)  kcond[e] =(double)7.98+(8.75-7.98)*(Te-25)/(100-25);
      if(Te>100 && Te<=200) kcond[e] =(double)8.75+(9.275-8.75)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) kcond[e] =(double)9.275+(9.8-9.275)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) kcond[e] =(double)9.8+(14.75-9.8)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) kcond[e] =(double)14.75+(15.5-14.75)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) kcond[e] =(double)15.5+(18.5-15.5)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) kcond[e] =(double)18.5+(21.5-18.5)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) kcond[e] =(double)21.5+(27-21.5)*(Te-700)/(800-700);
      if(Te>800 && Te<=900) kcond[e] =(double)27+(26-27)*(Te-800)/(900-800);
      if(Te>900 && Te<=1000) kcond[e] =(double)26+(27-26)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) kcond[e] =(double)27+(28.5-27)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1200) kcond[e] =(double)28.5+(29.5-28.5)*(Te-1100)/(1200-1100);
      if(Te>1200 && Te<=1300) kcond[e] =(double)29.5+(29.5-29.5)*(Te-1200)/(1300-1200);
      if(Te>1300 && Te<=3000) kcond[e] =(double)29.5+(29.5-29.5)*(Te-1300)/(3000-1300);
      if(Te>3000) kcond[e]=29.5;
    }
    else{
      if(Te<=25) kcond[e]=17.1;
      if(Te>25 && Te<=100)  kcond[e] =(double)17.1+(18.75-17.1)*(Te-25)/(100-25);
      if(Te>100 && Te<=200) kcond[e] =(double)18.75+(19.875-18.75)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) kcond[e] =(double)19.875+(21-19.875)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) kcond[e] =(double)21+(22.125-21)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) kcond[e] =(double)22.125+(23.25-22.125)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) kcond[e] =(double)23.25+(27.75-23.25)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) kcond[e] =(double)27.75+(21.5-27.75)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) kcond[e] =(double)21.5+(27-21.5)*(Te-700)/(800-700);
      if(Te>800 && Te<=900) kcond[e] =(double)27+(26-27)*(Te-800)/(900-800);
      if(Te>900 && Te<=1000) kcond[e] =(double)26+(27-26)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) kcond[e] =(double)27+(28.5-27)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1200) kcond[e] =(double)28.5+(29.5-28.5)*(Te-1100)/(1200-1100);
      if(Te>1200 && Te<=1300) kcond[e] =(double)29.5+(29.5-29.5)*(Te-1200)/(1300-1200);
      if(Te>1300 && Te<=3000) kcond[e] =(double)29.5+(29.5-29.5)*(Te-1300)/(3000-1300);
      if(Te>3000) kcond[e]=29.5;
    }

 rho[e]=8220;

    }

}



__global__ void Mat_Properties_Wen(double *U, int ni, int nj, int size_list, double *x_list, double *kcond_list, double *rho_list, double *c_list, double *kcond, double *rho, double *c, int *edof, int numElems,  int base_height)
{

  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,l,id1,edofMat2[MNE];
  double Te;

  if (e<numElems){

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
  }

    Te=0.125*(U[edofMat2[0]]+U[edofMat2[1]]+U[edofMat2[2]]+U[edofMat2[3]]+U[edofMat2[4]]+U[edofMat2[5]]+U[edofMat2[6]]+U[edofMat2[7]]);

    //kcond[e]=7;//0.0121*Te+6.4675;
    //rho[e]=546;//-0.1397*Te+4422.2;
    //c[e]=4420;//0.2212*Te+540.06;

    if(Te<=x_list[0]){
    kcond[e]=kcond_list[0];
    rho[e]=rho_list[0];
    c[e]=c_list[0];
    }
    else{
      if(Te>x_list[size_list-1]){
        kcond[e]=kcond_list[size_list-1];
        rho[e]=rho_list[size_list-1];
        c[e]=c_list[size_list-1];
      }
      else{
        for(l=0;l<size_list-1;l++){
          if(Te>x_list[l] && Te<=x_list[l+1]){
            kcond[e]=kcond_list[l]+(kcond_list[l+1]-kcond_list[l])*(Te-x_list[l])/(x_list[l+1]-x_list[l]);
            rho[e]=rho_list[l]+(rho_list[l+1]-rho_list[l])*(Te-x_list[l])/(x_list[l+1]-x_list[l]);
            c[e]=c_list[l]+(c_list[l+1]-c_list[l])*(Te-x_list[l])/(x_list[l+1]-x_list[l]);
          }
        }
      }
    }

    }

}

__global__ void ResetRes(double *r, int numNodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
     r[n] = 0.0;
  }
}

__global__ void GPUMatVec_Uold(int ni, int nj, double *rho, double *c, double *Me, double *Me_b, double *U, double *r, size_t numElems, int *edof, int base_height)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au[MNE],sum;
  bool vrai;
size_t e = blockIdx.x * blockDim.x + threadIdx.x;

  if (e<numElems){
    //printf("Mat vec old : Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
    vrai=1;
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
    vrai=0;
  }

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
        Au[j]= rho[e]*c[e]*(Me_b[i*MNE+j]*vrai+Me[i*MNE+j]*(1-vrai))*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }
}


__global__ void GPUMatVec(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, double *hconv5, double *hconv6, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *He5, double *He6,double *Me_b, double *Ke_b,
    double *He1_b, double *He2_b, double *He3_b, double *He4_b, double *He5_b, double *He6_b, double *U, double *r, size_t numElems, int *edof, int base_height)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  bool vrai;

  if (e<numElems){

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
    vrai=1;
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
    vrai=0;
  }

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
        Au[j]= (rho[e]*c[e]*(vrai*Me_b[i*MNE+j]+(1-vrai)*Me[i*MNE+j])+kcond[e]*(vrai*Ke_b[i*MNE+j]+(1-vrai)*Ke[i*MNE+j])
        +hconv1[e]*(vrai*He1_b[i*MNE+j]+(1-vrai)*He1[i*MNE+j])+hconv2[e]*(vrai*He2_b[i*MNE+j]+(1-vrai)*He2[i*MNE+j])+hconv3[e]*(vrai*He3_b[i*MNE+j]+(1-vrai)*He3[i*MNE+j])+
          hconv4[e]*(vrai*He4_b[i*MNE+j]+(1-vrai)*He4[i*MNE+j])+hconv5[e]*(vrai*He5_b[i*MNE+j]+(1-vrai)*He5[i*MNE+j])+hconv6[e]*(vrai*He6_b[i*MNE+j]+(1-vrai)*He6[i*MNE+j]))*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    //r[edofMat2[i]]=r[edofMat2[i]]+sum;
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }

}


__global__ void GPUMatVec_Precond(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, double *hconv5, double *hconv6, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *He5, double *He6,double *Me_b, double *Ke_b,
    double *He1_b, double *He2_b, double *He3_b, double *He4_b, double *He5_b, double *He6_b,  double *r, size_t numElems, int *edof, int base_height)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  bool vrai;

  if (e<numElems){

    if(e<base_height*ni*nj){
      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1+(ni+1)*(nj+1);
    edofMat2[5]=id1+1+(ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
    vrai=1;
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
    edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
    edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
    edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
    edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
    edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
    edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
    edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
    vrai=0;
  }


	for (int id=0; id<MNE; id++){
        for (int j=0; j<MNE; j++){
        //  Au[j]=0;
        if(j==id){
      //  Au= rho[e]*c[e]*Me[id*MNE+j]+kcond[e]*Ke[id*MNE+j]+hconv1[e]*He1[id*MNE+j]+hconv2[e]*He2[id*MNE+j]+hconv3[e]*He3[id*MNE+j]+
                  //hconv4[e]*He4[id*MNE+j]+hconv5[e]*He5[id*MNE+j]+hconv6[e]*He6[id*MNE+j];

                  Au= rho[e]*c[e]*(vrai*Me_b[id*MNE+j]+(1-vrai)*Me[id*MNE+j])+kcond[e]*(vrai*Ke_b[id*MNE+j]+(1-vrai)*Ke[id*MNE+j])
                  +hconv1[e]*(vrai*He1_b[id*MNE+j]+(1-vrai)*He1[id*MNE+j])+hconv2[e]*(vrai*He2_b[id*MNE+j]+(1-vrai)*He2[id*MNE+j])+hconv3[e]*(vrai*He3_b[id*MNE+j]+(1-vrai)*He3[id*MNE+j])+
                    hconv4[e]*(vrai*He4_b[id*MNE+j]+(1-vrai)*He4[id*MNE+j])+hconv5[e]*(vrai*He5_b[id*MNE+j]+(1-vrai)*He5[id*MNE+j])+hconv6[e]*(vrai*He6_b[id*MNE+j]+(1-vrai)*He6[id*MNE+j]);
                }
                                  }


      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[id]], Au);
                         }
  }

}

__global__ void rzp_calculation(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
    M_pre[n]= 1.f/M_pre[n];
      //printf("M[%d]=%f\n",n,M_pre[n]);
      r[n] = f[n] - prod[n];
      z[n] = M_pre[n]*r[n];
      p[n] = z[n];
    }

  }

}


/*__global__ void heatsource_position(int ni, int nj, int *domain_optim, int base_height,int layer_height, int *pos_heat, int numElems)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int edofMat2[MNE];

  int i,j,id1;

  if (e<numElems)
  {

    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

      if(domain_optim[e]==0){
        edofMat2[0]=id1;
        edofMat2[1]=id1+1;
        edofMat2[2]=id1+1+(ni+1);
        edofMat2[3]=id1+(ni+1);



    //if(height_k[compteur]>1 && k>=0+height_k[compteur] && k<layer_thickness+height_k[compteur]){
    //if(k>=base_height && k<=layer_height){
    if(j==layer_height){
      pos_heat[e]=1;


   }
  //if(k<=height_k[compteur]){



    activenodes[edofMat2[0]]=1;
    activenodes[edofMat2[1]]=1;
    activenodes[edofMat2[2]]=1;
    activenodes[edofMat2[3]]=1;


    //  }
  }


  }

}*/

/*__global__ void heatsource_position2(double *pos_heat, int numNodes, int *symnodes)
{

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
                if (symnodes[n]==1 && pos_heat[n]==1) pos_heat[n] = 0.5;
                if (symnodes[n]==2 && pos_heat[n]==1) pos_heat[n] = 0.25;
                if (symnodes[n]==3 && pos_heat[n]==1) pos_heat[n] = 0.125;
    }


}*/

__global__ void heatsource_term(int ni, int nj, int *pos_heat, double *b, int numElems, int heating,double Qvol, double *Qe1, int *edof, int base_height)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,id1,edofMat2[MNE];

  if (e<numElems){

    if(pos_heat[e]==1){

      if(e<base_height*ni*nj){
        k=e/(ni*nj);
        j=(e%(nj*ni))/ni;
        i=(e%(nj*ni))%ni;

        id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

      edofMat2[0]=id1;
      edofMat2[1]=id1+1;
      edofMat2[2]=id1+1+(ni+1);
      edofMat2[3]=id1+(ni+1);
      edofMat2[4]=id1+(ni+1)*(nj+1);
      edofMat2[5]=id1+1+(ni+1)*(nj+1);
      edofMat2[6]=id1+1+(ni+1)+(ni+1)*(nj+1);
      edofMat2[7]=id1+(ni+1)+(ni+1)*(nj+1);
    }
    else{
      edofMat2[0]=edof[(e-base_height*ni*nj)*MNE+0];
      edofMat2[1]=edof[(e-base_height*ni*nj)*MNE+1];
      edofMat2[2]=edof[(e-base_height*ni*nj)*MNE+2];
      edofMat2[3]=edof[(e-base_height*ni*nj)*MNE+3];
      edofMat2[4]=edof[(e-base_height*ni*nj)*MNE+4];
      edofMat2[5]=edof[(e-base_height*ni*nj)*MNE+5];
      edofMat2[6]=edof[(e-base_height*ni*nj)*MNE+6];
      edofMat2[7]=edof[(e-base_height*ni*nj)*MNE+7];
    }
        atomicAdd(&b[edofMat2[0]], heating*Qvol*Qe1[0]);
        atomicAdd(&b[edofMat2[1]], heating*Qvol*Qe1[1]);
        atomicAdd(&b[edofMat2[2]], heating*Qvol*Qe1[2]);
        atomicAdd(&b[edofMat2[3]], heating*Qvol*Qe1[3]);
        atomicAdd(&b[edofMat2[4]], heating*Qvol*Qe1[4]);
        atomicAdd(&b[edofMat2[5]], heating*Qvol*Qe1[5]);
        atomicAdd(&b[edofMat2[6]], heating*Qvol*Qe1[6]);
        atomicAdd(&b[edofMat2[7]], heating*Qvol*Qe1[7]);
      }
    }

}

__global__ void force_transient(double *f, double *b, double *b1, int numNodes2)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes2){
        f[n] =  f[n]+b[n] + b1[n];
        //printf("force transient : Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);
        //f[n]=b[n];
    }
}

__global__ void UpdateVec(double *p, double *r, double beta, int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void Norm(double *r, int numNodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){

      result[n]=r[n]*r[n];
      //atomicAdd(&result[0], r[n]*r[n]);

    }
}

__global__ void Prod_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  ){
      atomicAdd(&result[0], r[n]*z[n]);
    }
    }
}
__global__ void Update_UR(double *U, double *r, double *p, double *Ap, double alpha,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
    }
    }
}

__global__ void zCalculation(double *M_pre, double *r, double *z,  int numNodes, int *fixednodes)
    {
      size_t n = blockIdx.x * blockDim.x + threadIdx.x;

      if (n<numNodes){
        if(fixednodes[n]==0 ){
          z[n] = M_pre[n]*r[n];
        }
        }
    }


int main(int argc, char **argv)
{

  //Check out my device properties
  int nDevices;
  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
           prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
           prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
           2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  }


  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

  cudaSetDevice(1);

int i,j,k,i0,i1,iter,layer_height,step,heating;
double t,dt;
size_t free_memory, total_memory;

//size of the domain
double domain_x=0.050;
double domain_y=0.050;

//number of elements in each direction
int ni=50;
int nj=50;
int ni2=ni+1;
int nj2=nj+1;

FILE *solution;

int build_height=1;
int base_height=4;
const int nk=build_height+base_height;
int total_steps=290;
double domain_z=0.5e-3*build_height+1e-3*base_height;

double Tinf=20;
double Tinf2=20;
double Tini=20.00;

//space steps
const double delta_x=(double)domain_x/ni;
const double delta_y=(double)domain_y/nj;
const double delta_z=0.5e-3;
const double delta_z1=1e-3;


//volume multiplier
double Qvol=1.44e9;

//Material Properties
int size_list=21;
double *x_list=(double *)calloc(size_list,sizeof(double));
double *kcond_list=(double *)calloc(size_list,sizeof(double));
double *rho_list=(double *)calloc(size_list,sizeof(double));
double *c_list=(double *)calloc(size_list,sizeof(double));

//316L Material Properties
/*x_list[0]= 20;    c_list[0]=467;  kcond_list[0]=13.31;  rho_list[0]=7948;
x_list[1]= 104;   c_list[1]=486;  kcond_list[1]=15.37;  rho_list[1]=7915;
x_list[2]= 202;   c_list[2]=519;  kcond_list[2]=17.60;  rho_list[2]=7881;
x_list[3]= 304;   c_list[3]=539;  kcond_list[3]=19.37;  rho_list[3]=7826;
x_list[4]= 405;   c_list[4]=559;  kcond_list[4]=21.71;  rho_list[4]=7781;
x_list[5]= 504;   c_list[5]=569;  kcond_list[5]=23.23;  rho_list[5]=7731;
x_list[6]= 604;   c_list[6]=590;  kcond_list[6]=24.38;  rho_list[6]=7670;
x_list[7]= 703;   c_list[7]=601;  kcond_list[7]=24.95;  rho_list[7]=7631;
x_list[8]= 803;   c_list[8]=620;  kcond_list[8]=26.93;  rho_list[8]=7575;
x_list[9]= 903;   c_list[9]=640;  kcond_list[9]=27.70;  rho_list[9]=7514;
x_list[10]= 1002; c_list[10]=656; kcond_list[10]=28.85; rho_list[10]=7459;
x_list[11]= 1103; c_list[11]=666; kcond_list[11]=29.00; rho_list[11]=7392;
x_list[12]= 1203; c_list[12]=699; kcond_list[12]=30.69; rho_list[12]=7353;
x_list[13]= 1304; c_list[13]=708; kcond_list[13]=30.93; rho_list[13]=7308;
x_list[14]= 1385; c_list[14]=717; kcond_list[14]=31.87; rho_list[14]=7264;
x_list[15]= 1452; c_list[15]=828; kcond_list[15]=28.26; rho_list[15]=6867;
x_list[16]= 1502; c_list[16]=828; kcond_list[16]=29.25; rho_list[16]=6845;
x_list[17]= 1600; c_list[17]=828; kcond_list[17]=30.15; rho_list[17]=6762;
x_list[18]= 2881; c_list[18]=828; kcond_list[18]=42.92; rho_list[18]=5709;*/

//Ti 64 Material properties
x_list[0]= 25;    c_list[0]=546;  kcond_list[0]=7.00;  rho_list[0]=4420;
x_list[1]= 100;   c_list[1]=562;  kcond_list[1]=7.45;  rho_list[1]=4406;
x_list[2]= 200;   c_list[2]=584;  kcond_list[2]=8.75;  rho_list[2]=4395;
x_list[3]= 300;   c_list[3]=606;  kcond_list[3]=10.15;  rho_list[3]=4381;
x_list[4]= 400;   c_list[4]=629;  kcond_list[4]=11.35;  rho_list[4]=4366;
x_list[5]= 500;   c_list[5]=651;  kcond_list[5]=12.60;  rho_list[5]=4350;
x_list[6]= 600;   c_list[6]=673;  kcond_list[6]=14.20;  rho_list[6]=4336;
x_list[7]= 700;   c_list[7]=694;  kcond_list[7]=15.50;  rho_list[7]=4324;
x_list[8]= 800;   c_list[8]=714;  kcond_list[8]=17.80;  rho_list[8]=4309;
x_list[9]= 900;   c_list[9]=734;  kcond_list[9]=20.20;  rho_list[9]=4294;
x_list[10]= 995; c_list[10]=753; kcond_list[10]=22.70; rho_list[10]=4282;
x_list[11]= 1000; c_list[11]=641; kcond_list[11]=19.30; rho_list[11]=4282;
x_list[12]= 1100; c_list[12]=660; kcond_list[12]=21.00; rho_list[12]=4267;
x_list[13]= 1200; c_list[13]=678; kcond_list[13]=22.90; rho_list[13]=4252;
x_list[14]= 1300; c_list[14]=696; kcond_list[14]=23.70; rho_list[14]=4240;
x_list[15]= 1400; c_list[15]=714; kcond_list[15]=24.60; rho_list[15]=4225;
x_list[16]= 1500; c_list[16]=732; kcond_list[16]=25.80; rho_list[16]=4205;
x_list[17]= 1600; c_list[17]=750; kcond_list[17]=27.00; rho_list[17]=4198;
x_list[18]= 1655; c_list[18]=759; kcond_list[18]=28.40; rho_list[18]=4189;
x_list[19]= 1660; c_list[19]=831; kcond_list[19]=33.40; rho_list[19]=3920;
x_list[20]= 1700; c_list[20]=831; kcond_list[20]=34.60; rho_list[20]=3886;

double *x_list_d, *kcond_list_d, *rho_list_d, *c_list_d;

cudaMalloc((void **) &x_list_d, size_list * sizeof(double));
cudaMalloc((void **) &kcond_list_d, size_list * sizeof(double));
cudaMalloc((void **) &rho_list_d, size_list * sizeof(double));
cudaMalloc((void **) &c_list_d, size_list * sizeof(double));

cudaMemcpy(x_list_d, x_list, size_list * sizeof(double),cudaMemcpyHostToDevice);
cudaMemcpy(kcond_list_d, kcond_list, size_list * sizeof(double),cudaMemcpyHostToDevice);
cudaMemcpy(rho_list_d, rho_list, size_list * sizeof(double),cudaMemcpyHostToDevice);
cudaMemcpy(c_list_d, c_list, size_list * sizeof(double),cudaMemcpyHostToDevice);

//Total number of elements & nodes for the 3D structured uniform grid
int numElems=ni*nj*nk;
int numNodes=(ni+1)*(nj+1)*(nk+1);
//const int slice=(ni+1)*(nj+1);

int *map_nodes = (int *)calloc(numNodes, sizeof(int));
int *map_elements = (int *)calloc(numElems, sizeof(int));


    double *U_red,*hconv1_red,*hconv2_red,*hconv3_red,*hconv4_red,*hconv5_red,*hconv6_red,*b1_red;
    //double *f_red,*p_red,*z_red,*Ap_red;
    double *U_red_d,*b_red_d,*hconv1_red_d,*hconv2_red_d,*hconv3_red_d,*hconv4_red_d,*hconv5_red_d,*hconv6_red_d,*r_red_d,*r1_red_d,*prod_red_d,*M_red_d,*b1,*b1_red_d;
    double *kcond_red_d,*rho_red_d,*c_red_d;
    double *f_red_d,*p_red_d,*z_red_d,*Ap_red_d;

    int *pos_heat_red;
    int *pos_heat_red_d,*edofMat_all_d,*edofMat_all2_d,*fixednodes_red_d,*fixednodes_red;
    int edofMat[MNE];
    int id;

    //Remplissage base_plate nodes
    for(i=0;i<(nj+1)*(ni+1)*(base_height+1);i++) map_nodes[i]=i;

    int e,e1,id1,new_elements,new_ndof;
    new_elements=0;

    int old_elements=0;
    int old_node=(base_height+1)*(ni+1)*(nj+1);
    int old_node2=(base_height+1)*(ni+1)*(nj+1);
    int numNodes2=(base_height+1)*(ni+1)*(nj+1);
    int numElems2=base_height*ni*nj;
    int *edofMat_all = (int *)calloc(MNE*old_elements, sizeof(int));
    //int *edofMat_all2 = (int *)calloc(4*numElems2, sizeof(int));

    hconv1_red = (double *)calloc(numElems2 , sizeof(double));
    hconv2_red = (double *)calloc(numElems2 , sizeof(double));
    hconv3_red = (double *)calloc(numElems2 , sizeof(double));
    hconv4_red = (double *)calloc(numElems2 , sizeof(double));
    hconv5_red = (double *)calloc(numElems2 , sizeof(double));
    hconv6_red = (double *)calloc(numElems2 , sizeof(double));


    b1_red = (double *) calloc(numNodes2 , sizeof(double));
    U_red = (double *) calloc(numNodes2 , sizeof(double));
    fixednodes_red = (int *)calloc(numNodes2 ,sizeof(int));
    pos_heat_red = (int *)calloc(numElems2 ,sizeof(int));
    int *activenodes = (int *)calloc(numNodes ,sizeof(int));

    b1 = (double *) calloc(numNodes , sizeof(double));


//GET GPU DEVICE
printf("Starting [%s]...\n", sSDKname);


    //create handle for Ddot on gpu
    cublasHandle_t handle;
    cublasCreate(&handle);

//////////////////////////////////////// Solve Poisson ////////////////////////////////////////
/*------let's let the NVIDIA to take care allocation of matrices using the Unified Memory API----------*/

  //int *fixednodes_d, *activenodes_d, *convnodes_x_d, *convnodes_y_d,*convnodes_z_d; //*ndofMat_d;
  //int *domain_optim_d, *domain_d;
  int cpt;
  //int *pos_heat_d;
  //double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double  *Ke_d, *Me_d, *Qe1_d, *He1_d, *He2_d, *He3_d, *He4_d,*He5_d, *He6_d; //z_d
  double *Fe1_d, *Fe2_d, *Fe3_d, *Fe4_d,*Fe5_d, *Fe6_d;
  double  *Ke_b_d, *Me_b_d, *Qe1_b_d, *He1_b_d, *He2_b_d, *He3_b_d, *He4_b_d,*He5_b_d, *He6_b_d; //z_d
  double *Fe1_b_d, *Fe2_b_d, *Fe3_b_d, *Fe4_b_d,*Fe5_b_d, *Fe6_b_d;
  double *pAp_d;
  double *rznew_d;
  double *rNorm_d,*fNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  double *fNorm = (double *) calloc(1,sizeof(double));
  double norme;
  char filename[500], u2[500];
//  double *Au;

  //Allocate CPU memory using MallocManaged
  double *Ke = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me1 = (double *) malloc(MNE*MNE * sizeof(double));
  double *He1 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He2 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He3 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He4 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He5 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He6 = (double *)calloc(MNE*MNE, sizeof(double));
  double *Fe1 = (double *)calloc(MNE, sizeof(double));
  double *Fe2 = (double *)calloc(MNE, sizeof(double));
  double *Fe3 = (double *)calloc(MNE, sizeof(double));
  double *Fe4 = (double *)calloc(MNE, sizeof(double));
  double *Fe5 = (double *)calloc(MNE, sizeof(double));
  double *Fe6 = (double *)calloc(MNE, sizeof(double));
  double *Qe1 = (double *)calloc(MNE, sizeof(double));

  double *Ke_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me1_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *He1_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He2_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He3_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He4_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He5_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He6_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *Fe1_b = (double *)calloc(MNE, sizeof(double));
  double *Fe2_b = (double *)calloc(MNE, sizeof(double));
  double *Fe3_b = (double *)calloc(MNE, sizeof(double));
  double *Fe4_b = (double *)calloc(MNE, sizeof(double));
  double *Fe5_b = (double *)calloc(MNE, sizeof(double));
  double *Fe6_b = (double *)calloc(MNE, sizeof(double));
  double *Qe1_b = (double *)calloc(MNE, sizeof(double));

  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_x = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_y = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_z = (int *)calloc(numNodes, sizeof(int));

  double *time_probe_tab = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe1 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe2 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe3 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe4 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe5 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe6 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe7 = (double *)calloc(build_height*total_steps, sizeof(double));

  int *pos_heat = (int *)calloc(numElems, sizeof(int));

  int *domain_optim = (int *)calloc(numElems, sizeof(int));
  int *domain = (int *)calloc(numElems, sizeof(int));
  double *kcond = (double *)calloc(numElems, sizeof(double));
  double *rho = (double *)calloc(numElems, sizeof(double));
  double *c = (double *)calloc(numElems, sizeof(double));
  double *hconv1 = (double *)calloc(numElems, sizeof(double));
  double *hconv2 = (double *)calloc(numElems, sizeof(double));
  double *hconv3 = (double *)calloc(numElems, sizeof(double));
  double *hconv4 = (double *)calloc(numElems, sizeof(double));
  double *hconv5 = (double *)calloc(numElems, sizeof(double));
  double *hconv6 = (double *)calloc(numElems, sizeof(double));
  //int *ndofMat = (int *) malloc(MNE*numElems * sizeof(int));

  double *U = (double *) calloc(numNodes , sizeof(double));
  double *U1 = (double *) calloc(numNodes , sizeof(double));
  double *U2 = (double *) calloc(numNodes , sizeof(double));
  double *U3 = (double *) calloc(numNodes , sizeof(double));
  double *U4 = (double *) calloc(numNodes , sizeof(double));
  double *U5 = (double *) calloc(numNodes , sizeof(double));
  double *U6 = (double *) calloc(numNodes , sizeof(double));
  double *U7 = (double *) calloc(numNodes , sizeof(double));
  double *U8 = (double *) calloc(numNodes , sizeof(double));
  double *U9 = (double *) calloc(numNodes , sizeof(double));
  double *U10 = (double *) calloc(numNodes , sizeof(double));
  double *U11 = (double *) calloc(numNodes , sizeof(double));
  double *U12 = (double *) calloc(numNodes , sizeof(double));
  double *U13 = (double *) calloc(numNodes , sizeof(double));
  double *U14 = (double *) calloc(numNodes , sizeof(double));
  double *U15 = (double *) calloc(numNodes , sizeof(double));
  double *U16 = (double *) calloc(numNodes , sizeof(double));
  double *U17 = (double *) calloc(numNodes , sizeof(double));
  double *U18 = (double *) calloc(numNodes , sizeof(double));
  double *U19 = (double *) calloc(numNodes , sizeof(double));
  double *U20 = (double *) calloc(numNodes , sizeof(double));
  double *U21 = (double *) calloc(numNodes , sizeof(double));
  double *U22 = (double *) calloc(numNodes , sizeof(double));
  double *U23 = (double *) calloc(numNodes , sizeof(double));
  double *U24 = (double *) calloc(numNodes , sizeof(double));
  double *U25 = (double *) calloc(numNodes , sizeof(double));

  for (i=0;i<numNodes;i++) {
    //if(i<(ni+1)*(nj+1)*(base_height+1)){
    U[i]=Tini;
  //}
  }
  for (i=0;i<numNodes2;i++) {
  //if(i<(ni+1)*(nj+1)*(base_height+1)){
    U_red[i]=Tini;
  //}
  }


  // Allocate memory
  cudaMalloc((void **) &Ke_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Me_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He4_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He5_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe4_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe5_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Qe1_d, MNE * sizeof(double));

  cudaMalloc((void **) &Ke_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Me_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He1_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He2_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He3_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He4_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He5_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He6_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe1_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe2_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe3_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe4_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe5_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe6_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Qe1_b_d, MNE * sizeof(double));

  //additional variable for reduction operations
  cudaMalloc((void **) &pAp_d, 1 * sizeof(double));
  cudaMalloc((void **) &rznew_d, 1 * sizeof(double));
  cudaMalloc((void **) &rNorm_d, 1 * sizeof(double));
  cudaMalloc((void **) &fNorm_d, 1 * sizeof(double));

  cudaMemcpy(pAp_d, pAp, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rznew_d, rznew, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rNorm_d, rNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(fNorm_d, fNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);

  //Calculation of the element matrix
  KM_matrix(delta_x,delta_y,delta_z,Ke,Me1,He1,He2,He3,He4,He5,He6,Fe1,Fe2,Fe3,Fe4,Fe5,Fe6,Qe1);
  KM_matrix(delta_x,delta_y,delta_z1,Ke_b,Me1_b,He1_b,He2_b,He3_b,He4_b,He5_b,He6_b,Fe1_b,Fe2_b,Fe3_b,Fe4_b,Fe5_b,Fe6_b,Qe1_b);
  //KM_matrix_2D(delta_x,delta_y,Ke,Me1,He1,He2,He3,He4,Fe1,Fe2,Fe3,Fe4,Qe1);

  //Domain definition
  domain_definition(ni,nj,nk,base_height,map_elements,domain);


    cudaMemcpy(Ke_d, Ke, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He1_d, He1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He2_d, He2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He3_d, He3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He4_d, He4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He5_d, He5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He6_d, He6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe1_d, Fe1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe2_d, Fe2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe3_d, Fe3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe4_d, Fe4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe5_d, Fe5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe6_d, Fe6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Qe1_d, Qe1, MNE * sizeof(double),cudaMemcpyHostToDevice);

    cudaMemcpy(Ke_b_d, Ke_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He1_b_d, He1_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He2_b_d, He2_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He3_b_d, He3_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He4_b_d, He4_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He5_b_d, He5_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He6_b_d, He6_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe1_b_d, Fe1_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe2_b_d, Fe2_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe3_b_d, Fe3_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe4_b_d, Fe4_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe5_b_d, Fe5_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe6_b_d, Fe6_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Qe1_b_d, Qe1_b, MNE * sizeof(double),cudaMemcpyHostToDevice);

  const double TOL = 1.0e-6;

   cpt=0;
   //printf("beginning transient loop \n");
    //Beginning of the transient loop
    int cpt1=0;

    double time_probe=0;

    for (layer_height=base_height;layer_height<build_height+base_height;layer_height++){

    for (i=0;i<numElems;i++) pos_heat[i]=0;//memset(pos_heat, 0.0, numElems*sizeof(int));
    active_domain (ni,nj,numElems,base_height,layer_height,domain, domain_optim,pos_heat);

    //Reinitialization
    for (i=0;i<numElems;i++) hconv1[i]=0;
    for (i=0;i<numElems;i++) hconv2[i]=0;
    for (i=0;i<numElems;i++) hconv3[i]=0;
    for (i=0;i<numElems;i++) hconv4[i]=0;
    for (i=0;i<numElems;i++) hconv5[i]=0;
    for (i=0;i<numElems;i++) hconv6[i]=0;
    for (i=0;i<numNodes;i++) convnodes_x[i]=0;
    for (i=0;i<numNodes;i++) convnodes_y[i]=0;
    for (i=0;i<numNodes;i++) convnodes_z[i]=0;

    //if(layer_height==0*base_height){
    Boundary_Conditions1 (ni,nj,nk,numElems,base_height,layer_height,domain_optim,convnodes_x,convnodes_y,convnodes_z,hconv1,hconv2, hconv3, hconv4,hconv5,hconv6);

    for (k=0; k<nk; k++) {
        for (j=0; j<nj; j++) {
            for (i=0; i<ni; i++) {

            i0=I3D(ni,nj,i,j,k);
            //if(hconv6[i0]>0) printf("i=%d, j=%d, k=%d, hconv=%lg\n",i,j,k, hconv6[i0]);
          }
        }
      }

    for(i=0;i<numNodes;i++) b1[i]=0;//memset(b1, 0.0, numNodes*sizeof(double));
    Boundary_Conditions2 (ni,nj,numElems,domain_optim,convnodes_x,convnodes_y,convnodes_z,hconv1,hconv2, hconv3, hconv4,hconv5,hconv6,Tinf,Tinf2,b1, Fe1,Fe2,Fe3,Fe4,Fe5,Fe6, Fe1_b,Fe2_b,Fe3_b,Fe4_b,Fe5_b,Fe6_b,base_height);
  //}
      /////////////////////////////////////////////////////////////////////
      //Mapping u

      for(i=0;i<numNodes;i++){
        U[0]=U_red[0];
        if(map_nodes[i]>0) {
          U[i]=U_red[map_nodes[i]];
        }
      }

      new_elements=new_elements_counter(ni,nj,nk,layer_height,base_height,domain_optim);

      new_ndof=MNE*new_elements;

      int *ele_id = (int *)calloc(new_elements, sizeof(int));
      int *ele2_id = (int *)calloc(new_elements, sizeof(int));
      int *node_id = (int *)calloc(new_ndof, sizeof(int));
      int *node_id2 = (int *)calloc(new_ndof, sizeof(int));
      int *ele_id_node = (int *)calloc(new_ndof, sizeof(int));
      int *pos_id_node = (int *)calloc(new_ndof, sizeof(int));
      int *edofMat_new = (int *)calloc(new_ndof, sizeof(int));

      //ele_id calculation
      ele_id_calculation(ni,nj,nk,base_height,layer_height,domain_optim,ele_id);

      //Update map_elements
      numElems2=base_height*ni*nj+old_elements;
      for (i = 0; i < new_elements; i++){
      ele2_id[i]=numElems2;
      map_elements[ele_id[i]]=ele2_id[i];
      numElems2++;
        }

      //node_id array of new elements
      cpt=0;
      for (e1=0;e1<new_elements;e1++){
          e=ele_id[e1];
          k=e/(ni*nj);
          j=(e%(nj*ni))/ni;
          i=(e%(nj*ni))%ni;
          id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;
          node_id[cpt]=id1;                               ele_id_node[cpt]=e; pos_id_node[cpt]=1; cpt++;
          node_id[cpt]=id1+1;                             ele_id_node[cpt]=e; pos_id_node[cpt]=2; cpt++;
          node_id[cpt]=id1+1+(ni+1);                      ele_id_node[cpt]=e; pos_id_node[cpt]=3; cpt++;
          node_id[cpt]=id1+(ni+1);                        ele_id_node[cpt]=e; pos_id_node[cpt]=4; cpt++;
          node_id[cpt]=id1+ (ni+1)*(nj+1);                ele_id_node[cpt]=e; pos_id_node[cpt]=5; cpt++;
          node_id[cpt]=id1+1+ (ni+1)*(nj+1);              ele_id_node[cpt]=e; pos_id_node[cpt]=6; cpt++;
          node_id[cpt]=id1+1+(ni+1)+ (ni+1)*(nj+1);       ele_id_node[cpt]=e; pos_id_node[cpt]=7; cpt++;
          node_id[cpt]=id1+(ni+1)+ (ni+1)*(nj+1);         ele_id_node[cpt]=e; pos_id_node[cpt]=8; cpt++;
        }

      //sort of node id
      int a;
      for (i = 0; i < new_ndof; ++i)  {
            for (j = i + 1; j < new_ndof; ++j)  {
                if (node_id[i] > node_id[j])
                {
                    a =  node_id[i];
                    node_id[i] = node_id[j];
                    node_id[j] = a;

                    a =  ele_id_node[i];
                    ele_id_node[i] = ele_id_node[j];
                    ele_id_node[j] = a;

                    a =  pos_id_node[i];
                    pos_id_node[i] = pos_id_node[j];
                    pos_id_node[j] = a;
                }
            }
        }


        // Node id removal

        int min_node=old_node;
        int min_node2=old_node2;


        for (i = 0; i < new_ndof; ++i){
          if(node_id[i]<min_node2){
            node_id2[i]=map_nodes[node_id[i]];

          }
          else{
            if(node_id[i]==node_id[i-1]){
              node_id2[i]=node_id2[i-1];

            }
            else{
              node_id2[i]=min_node;

              min_node=min_node+1;
            }
          }

        }
        numNodes2=min_node;

        //Update map_nodes
        for (i=0;i<new_ndof;i++) {
          map_nodes[node_id[i]]=node_id2[i];

        }

        //Sorting elements
        for (i = 0; i < new_ndof; ++i)
          {
              for (j = i + 1; j < new_ndof; ++j)
              {
                  if (ele_id_node[i] > ele_id_node[j])
                  {
                      a =  node_id2[i];
                      node_id2[i] = node_id2[j];
                      node_id2[j] = a;

                      a =  node_id[i];
                      node_id[i] = node_id[j];
                      node_id[j] = a;

                      a =  ele_id_node[i];
                      ele_id_node[i] = ele_id_node[j];
                      ele_id_node[j] = a;

                      a =  pos_id_node[i];
                      pos_id_node[i] = pos_id_node[j];
                      pos_id_node[j] = a;

                  }
              }
          }

          //Creation edofMat new
          for (i=0;i<new_elements;i++){
            for (j=0;j<MNE;j++){
            edofMat_new[MNE*i+pos_id_node[MNE*i+j]-1]=node_id2[MNE*i+j];
                  }
                }

          //printf("numElems2=%d and numNodes2=%d\n",numElems2,numNodes2);
          //Auxilliary fields
          hconv1_red = (double *)realloc(hconv1_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv1_red[i]=0;
          hconv2_red = (double *)realloc(hconv2_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv2_red[i]=0;
          hconv3_red = (double *)realloc(hconv3_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv3_red[i]=0;
          hconv4_red = (double *)realloc(hconv4_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv4_red[i]=0;
          hconv5_red = (double *)realloc(hconv5_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv5_red[i]=0;
          hconv6_red = (double *)realloc(hconv6_red, numElems2 * sizeof(double)); for(i=0;i<numElems2;i++) hconv6_red[i]=0;

          U_red = (double *)realloc(U_red,numNodes2*sizeof(double));


          edofMat_all = (int *)realloc(edofMat_all,MNE*(old_elements+new_elements)*sizeof(int));
          for (i=0;i<new_ndof;i++) edofMat_all[MNE*old_elements+i]=edofMat_new[i];

          b1_red = (double *)realloc(b1_red, numNodes2 * sizeof(double)); for(i=0;i<numNodes2;i++) b1_red[i]=0;
          fixednodes_red = (int *)realloc(fixednodes_red, numNodes2 * sizeof(int)); for(i=0;i<numNodes2;i++) fixednodes_red[i]=0;
          pos_heat_red = (int *)realloc(pos_heat_red, numElems2 * sizeof(int)); for(i=0;i<numElems2;i++) pos_heat_red[i]=0;

          hconv1_red[0]=hconv1[0];
          hconv2_red[0]=hconv2[0];
          hconv3_red[0]=hconv3[0];
          hconv4_red[0]=hconv4[0];
          hconv5_red[0]=hconv5[0];
          hconv6_red[0]=hconv6[0];
          pos_heat_red[0]=pos_heat[0];

          for(i=0;i<numElems;i++){
            if(map_elements[i]>0) {
              hconv1_red[map_elements[i]]=hconv1[i];
              hconv2_red[map_elements[i]]=hconv2[i];
              hconv3_red[map_elements[i]]=hconv3[i];
              hconv4_red[map_elements[i]]=hconv4[i];
              hconv5_red[map_elements[i]]=hconv5[i];
              hconv6_red[map_elements[i]]=hconv6[i];
              pos_heat_red[map_elements[i]]=pos_heat[i];
            }
          }

          for(i=0;i<numNodes;i++){
              b1_red[0]=b1[0];
              U_red[0]=U[0];
              fixednodes_red[0]=fixednodes[0];
            if(map_nodes[i]>0) {
              U_red[map_nodes[i]]=U[i];
              b1_red[map_nodes[i]]=b1[i];
              fixednodes_red[map_nodes[i]]=fixednodes[i];
            }
          }

            //for (i=old_node;i<numNodes2;i++) U_red[i]=Tini;
          //for(i=0;i<numNodes2;i++) printf("Ured[%d]=%f\n",i,U_red[i]);

        //cudaMalloc
        checkCudaErrors(cudaMalloc((void **) &hconv1_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv2_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv3_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv4_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv5_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv6_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &kcond_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &rho_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &c_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &U_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &f_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &b_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &b1_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &M_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &r_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &r1_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &prod_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &p_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &z_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &Ap_red_d, numNodes2 * sizeof(double)));

        checkCudaErrors(cudaMalloc((void **) &fixednodes_red_d, numNodes2 * sizeof(int)));
        checkCudaErrors(cudaMalloc((void **) &edofMat_all_d, MNE*(old_elements+new_elements) * sizeof(int)));
        //checkCudaErrors(cudaMalloc((void **) &edofMat_all2_d, 4*numElems2 * sizeof(int)));
        checkCudaErrors(cudaMalloc((void **) &pos_heat_red_d, numElems2 * sizeof(int)));

        //Transfer to GPU
        checkCudaErrors(cudaMemcpy(hconv1_red_d, hconv1_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv2_red_d, hconv2_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv3_red_d, hconv3_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv4_red_d, hconv4_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv5_red_d, hconv5_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv6_red_d, hconv6_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(U_red_d, U_red, numNodes2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(b1_red_d, b1_red, numNodes2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(fixednodes_red_d, fixednodes_red, numNodes2 * sizeof(int),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(pos_heat_red_d, pos_heat_red, numElems2 * sizeof(int),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(edofMat_all_d, edofMat_all, MNE*(old_elements+new_elements) * sizeof(int),cudaMemcpyHostToDevice));
        //checkCudaErrors(cudaMemcpy(edofMat_all2_d, edofMat_all2, 4*numElems2* sizeof(int),cudaMemcpyHostToDevice));

        if(layer_height>base_height){
          int node_complet=(ni+1)*(nj+1)*(layer_height+1);
          int elem_complet=ni*nj*layer_height;
          int max=0;
          for(i=0;i<node_complet;i++){
            if(map_nodes[i]>=max) max=map_nodes[i];
          }
          old_node=max+1;
          old_node2=node_complet;
          max=0;
          for(i=0;i<elem_complet;i++){
            if(map_elements[i]>=max) max=map_elements[i];
          }
          old_elements=max-ni*nj*base_height+1;
        //printf("layer_height=%d, old_node=%d, old_node2=%d, old_elements=%d\n",layer_height,old_node,old_node2,old_elements);
        }

        int  NBLOCKS   = (numElems2+BLOCKSIZE1-1) / BLOCKSIZE1; //round up if n is not a multiple of blocksize
      //  printf("Number of Blocks: %d\n",NBLOCKS);
        //dim3 dimBlock(BLOCKSIZE, BLOCKSIZE);
        //printf("numNodes: %d\n", numNodes2);
        //printf("numElems: %d\n", numElems2);
        int  NBLOCKS2 = (numNodes2+BLOCKSIZE2-1) / BLOCKSIZE2; //round up if n is not a multiple of blocksize
        //printf("nblock2=%d and blocksize=%d\n",NBLOCKS2,BLOCKSIZE);

      /////////////////////////////////////////////////////////////////////

      for (step=0;step<290;step++){


        if(step<100) {
          dt=(double)3.216/100; heating=1;
        }
      if(step>=100 && step<200) {

          dt=0.1; heating=0;
        }
        if(step>=200 && step<290) {

            dt=1; heating=0;
          }

        time_probe=time_probe+dt;

        /*if(step<1) {
          dt=0.9; heating=1;
        }
      if(step>=1 && step<11) {

          dt=0.11; heating=0;
        }
      if(step>=11 && step<21) {

            dt=0.8; heating=0;
          }*/



         /*if(step<9) {
              dt=0.1; heating=1;
            }
            else{
              dt=0.1; heating=0;
            }*/

        for(size_t n=0; n<MNE*MNE; n++) {
          Me[n]=(double)Me1[n]/dt;
          Me_b[n]=(double) Me1_b[n]/dt;
        }

       checkCudaErrors(cudaMemcpy(Me_d, Me, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));
       checkCudaErrors(cudaMemcpy(Me_b_d, Me_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));

  //Uold _matvec
  //MatVec_Uold(ni,nj, Me, U, f, numElems, numNodes);
  //Mat_Properties_Qian <<<NBLOCKS,BLOCKSIZE1>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);
    Mat_Properties_Wen <<<NBLOCKS,BLOCKSIZE1>>>(U_red_d,ni,nj,size_list,x_list_d,kcond_list_d,rho_list_d,c_list_d,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);
  //getchar();
  checkCudaErrors(cudaMemset(f_red_d, 0.0, numNodes2*sizeof(double)));
  //Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,numElems2,edofMat_all2_d,base_height);
  GPUMatVec_Uold<<<NBLOCKS,BLOCKSIZE1>>>(ni,nj, rho_red_d,c_red_d, Me_d, Me_b_d,U_red_d, f_red_d, numElems2,edofMat_all_d,base_height);

  //cudaMemset(pos_heat_d, 0.0, numElems*sizeof(double));
  //heatsource_position<<<NBLOCKS,BLOCKSIZE>>>(ni,nj, domain_optim_d,base_height,layer_height,pos_heat_d,activenodes_d,numElems);


  checkCudaErrors(cudaMemset(b_red_d, 0.0, numNodes2*sizeof(double)));
  heatsource_term<<<NBLOCKS,BLOCKSIZE1>>>(ni,nj,pos_heat_red_d,b_red_d,numElems2,heating,Qvol,Qe1_d,edofMat_all_d,base_height);

  force_transient<<<NBLOCKS2,BLOCKSIZE2>>>(f_red_d, b_red_d, b1_red_d,numNodes2);

  //define misc vars
  //double rNorm = 0.0;
  //double bNorm = 0.0;
  double rzold = 0.0;
  //double rznew = 0.0;
  double alpha = 0.0;
  double beta  = 0.0;
  //double malpha = -alpha;


  //Initialize MatVec on the CPU
  //MatVec(kcond,rhoc, hconv1,hconv2, ni,nj, Ke, Me, He1, He2, U, r, numElems, numNodes);
  //checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes2*sizeof(double)));
  checkCudaErrors(cudaMemset(prod_red_d, 0.0, numNodes2*sizeof(double)));
  GPUMatVec<<<NBLOCKS,BLOCKSIZE1>>>(kcond_red_d, rho_red_d,c_red_d, hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,hconv5_red_d, hconv6_red_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d,He5_d, He6_d,Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d,He5_b_d, He6_b_d, U_red_d, prod_red_d, numElems2,edofMat_all_d,base_height);

  //preconditioner
  //Precond(kcond,rhoc,hconv1,hconv2,ni,nj,Ke, He1, He2, M_pre, numElems, numNodes);
    checkCudaErrors(cudaMemset(M_red_d, 0.0, numNodes2*sizeof(double)));
  GPUMatVec_Precond<<<NBLOCKS,BLOCKSIZE1>>>(kcond_red_d, rho_red_d,c_red_d, hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,hconv5_red_d, hconv6_red_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, He5_d, He6_d,Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d,He5_b_d, He6_b_d, M_red_d, numElems2,edofMat_all_d,base_height);

    checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes2*sizeof(double)));
  rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_red_d, M_red_d, r_red_d,prod_red_d, z_red_d, p_red_d, numNodes2, fixednodes_red_d);

  //Reset pAp value
  //cudaMemset(pAp_d, 0.0, 1*sizeof(double));
  //Apply Ddot
  pAp[0]=0;
  cublasDdot(handle, numNodes2, p_red_d, 1, r_red_d, 1, pAp);
  //Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_red_d, r_red_d,numNodes2,fixednodes_red_d,pAp_d);
  //cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);


    // control phi
  rzold = pAp[0];
  double rzold1=rzold;
  //printf("rzold=%lg\n",rzold);

  const int Max_iters = 5000;
  //double pAp = 0.0;
  //int node;


//  r_usage.ru_maxrss = 0.0; //set memory to zero
  //t = get_time(); // get time stamp


    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

      //Mat_Properties_Qian <<<NBLOCKS,BLOCKSIZE1>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);
        Mat_Properties_Wen <<<NBLOCKS,BLOCKSIZE1>>>(U_red_d,ni,nj,size_list,x_list_d,kcond_list_d,rho_list_d,c_list_d,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);

    checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes2*sizeof(double)));

    //Apply Matrix-Free MatVec kernel [Loop over elements]
    GPUMatVec<<<NBLOCKS,BLOCKSIZE1>>>(kcond_red_d, rho_red_d,c_red_d, hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,hconv5_red_d, hconv6_red_d,
      ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d,He5_d, He6_d,Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d,He5_b_d, He6_b_d, p_red_d, Ap_red_d, numElems2,edofMat_all_d,base_height);

    //Reset pAp value
    //checkCudaErrors(cudaMemset(pAp_d, 0.0, 1*sizeof(double)));
    //Apply Ddot
    pAp[0]=0;
    checkCudaErrors(cublasDdot(handle, numNodes2, p_red_d, 1, Ap_red_d, 1, pAp));
    //Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_red_d, Ap_red_d,numNodes2,fixednodes_red_d,pAp_d);
    //checkCudaErrors(cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost));
    //printf("pAp_d[0]=%f\n",pAp_d[0]);

    // control alpha
    alpha = rzold/pAp[0];
    //printf("rzold = %1.12f pAp = %1.12f   alpha = %1.12f\n",rzold,pAp_d[0], alpha);

    Update_UR<<<NBLOCKS2,BLOCKSIZE2>>>(U_red_d, r_red_d, p_red_d, Ap_red_d,alpha, numNodes2,fixednodes_red_d);

      //Reset Norm
    //checkCudaErrors(cudaMemset(rNorm_d, 0.0, 1*sizeof(double)));
    rNorm[0]=0;
    //cudaMemset(fNorm_d, 0.0, 1*sizeof(double));
    //Apply Dnorm
    //checkCudaErrors(cublasDnrm2(handle, numNodes2, r_red_d, 1, rNorm));

    Norm<<<NBLOCKS2,BLOCKSIZE2>>>(r_red_d, numNodes2,r1_red_d);
    cublasDasum(handle,numNodes2,r1_red_d,1,rNorm);
    norme=sqrt(rNorm[0]);
      if(norme <= TOL) break;

    //Norm<<<NBLOCKS2,BLOCKSIZE>>>(f_d, numNodes,fixednodes_d,activenodes_d,fNorm_d);
    //checkCudaErrors(cudaMemcpy(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost));
    //cudaMemcpy(fNorm, fNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);

    // converge if reached Tolerance 1e-6


    //Calculation for preconditioner
    zCalculation<<<NBLOCKS2,BLOCKSIZE2>>>(M_red_d, r_red_d, z_red_d, numNodes2,fixednodes_red_d);

    //Update rznew
      //cudaMemset(rznew_d, 0.0, 1*sizeof(double));
        rznew[0]=0;
    cublasDdot(handle, numNodes2, z_red_d, 1, r_red_d, 1, rznew);
    //Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(r_d, z_d,numNodes,fixednodes_d,activenodes_d,rznew_d);
    //cudaMemcpy(rznew, rznew_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);

    //Check residual norm and my alpha
     //printf("iter = %i, alpha=%f, beta=%f, rNorm = %lg \n", iter, alpha, beta, norme);

    // control beta
    beta = rznew[0]/rzold;

    //update vector [p] for next iters...
    UpdateVec<<<NBLOCKS2,BLOCKSIZE2>>>(p_red_d, z_red_d, beta, numNodes2,fixednodes_red_d);


    //update rzold for next iters...
    rzold = rznew[0];

//    if (iter==0) break;

}


checkCudaErrors(cudaMemcpy(U_red, U_red_d, numNodes2 * sizeof(double), cudaMemcpyDeviceToHost));

//for(i=0;i<numNodes2;i++) printf("Fin Boucle : U_red[%d]=%f \n",i,U_red[i]);

  for(i=0;i<numNodes;i++){
    U[0]=U_red[0];
    if(map_nodes[i]>0) {
      U[i]=U_red[map_nodes[i]];
    }
  }


      /*if(layer_height==30 && step==0){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        //cudaMemcpy(pos_heat,pos_heat_d, numElems * sizeof(double),cudaMemcpyDeviceToHost);

        for (int n=0; n<numNodes; n++) U1[n]=U[n];
      }
      if(layer_height==130 && step==0){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U2[n]=U[n];
      }
      if(layer_height==230 && step==0){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U3[n]=U[n];
      }
      if(layer_height==230 && step==1){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U4[n]=U[n];

      }
      if(layer_height==230 && step==2){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U5[n]=U[n];
      }
      if(layer_height==230 && step==3){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U6[n]=U[n];
      }
      if(layer_height==230 && step==4){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U7[n]=U[n];
      }
      if(layer_height==230 && step==5){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U8[n]=U[n];
      }
      if(layer_height==230 && step==6){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U9[n]=U[n];
      }
      if(layer_height==230 && step==7){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U10[n]=U[n];
      }
      if(layer_height==230 && step==8){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        //cudaMemcpy(activenodes,activenodes_d, numNodes * sizeof(int),cudaMemcpyDeviceToHost);
        //cudaMemcpy(domain_optim,domain_optim_d, numElems * sizeof(int),cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U11[n]=U[n];
      }
      if(layer_height==230 && step==9){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U12[n]=U[n];
      }
      if(layer_height==230 && step==10){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U13[n]=U[n];
      }
      if(layer_height==230 && step==11){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U14[n]=U[n];
      }
      if(layer_height==230 && step==12){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U15[n]=U[n];
      }
      if(layer_height==230 && step==13){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U16[n]=U[n];
      }
      if(layer_height==230 && step==14){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U17[n]=U[n];
      }
      if(layer_height==230 && step==15){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U18[n]=U[n];
      }
      if(layer_height==230 && step==16){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U19[n]=U[n];
      }
      if(layer_height==230 && step==17){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U20[n]=U[n];
      }
      if(layer_height==230 && step==18){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U21[n]=U[n];
      }
      if(layer_height==230 && step==19){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U22[n]=U[n];
      }
      if(layer_height==230 && step==20){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U23[n]=U[n];
      }
      if(layer_height==730 && step==0){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), //cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U24[n]=U[n];

      }
      if(layer_height==1130 && step==0){
        //cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
        for (int n=0; n<numNodes; n++) U25[n]=U[n];

      }*/

  for (k=0; k<(nk+1); k++) {
    for (j=0; j<(nj+1); j++) {
      for (i1=0; i1<(ni+1); i1++) {
          i0=I3D(ni2,nj2,i1,j,k);
                  //fprintf(solution,"%d,%d,%d,%f,%d,%d\n",i,j,k, pos_heat[i0],activenodes[i0],convnodes[i0]);
                  //fprintf(solution,"%f\n",b[i0]);
                  if(k==0 && i1==25 && j==25) probe1[cpt1]=U[i0];
                  if(k==5 && i1==25 && j==25) probe2[cpt1]=U[i0];
                  if(k==6 && i1==25 && j==25) probe3[cpt1]=U[i0];
                  /*if(k==180 && i1==0 && j==0) probe4[cpt1]=U[i0];
                  if(k==430 && i1==0 && j==0) probe5[cpt1]=U[i0];
                  if(k==730 && i1==0 && j==0) probe6[cpt1]=U[i0];
                  if(k==1180 && i1==0 && j==0) probe7[cpt1]=U[i0];*/
                }
            }
          }
          time_probe_tab[cpt1]=time_probe;

  cpt1=cpt1+1;
  printf("Layer %d/%d, Step %d/%d  it_PCG=%d, residual=%lg\n",layer_height-base_height+1,build_height,step,total_steps,iter,norme);
}



checkCudaErrors(cudaFree(hconv1_red_d));
checkCudaErrors(cudaFree(hconv2_red_d));
checkCudaErrors(cudaFree(hconv3_red_d));
checkCudaErrors(cudaFree(hconv4_red_d));
checkCudaErrors(cudaFree(hconv5_red_d));
checkCudaErrors(cudaFree(hconv6_red_d));
checkCudaErrors(cudaFree(kcond_red_d));
checkCudaErrors(cudaFree(rho_red_d));
checkCudaErrors(cudaFree(c_red_d));
checkCudaErrors(cudaFree(U_red_d));
checkCudaErrors(cudaFree(f_red_d));
checkCudaErrors(cudaFree(b_red_d));
checkCudaErrors(cudaFree(b1_red_d));
checkCudaErrors(cudaFree(M_red_d));
checkCudaErrors(cudaFree(r_red_d));
checkCudaErrors(cudaFree(r1_red_d));
checkCudaErrors(cudaFree(prod_red_d));
checkCudaErrors(cudaFree(p_red_d));
checkCudaErrors(cudaFree(z_red_d));
checkCudaErrors(cudaFree(Ap_red_d));
checkCudaErrors(cudaFree(fixednodes_red_d));
checkCudaErrors(cudaFree(edofMat_all_d));
checkCudaErrors(cudaFree(pos_heat_red_d));

}
    //kill handle
    cublasDestroy(handle);


  // StdOut prints
  printf("\n");
  printf("*** PCG Solver Converged after %d total iterations ***\n", iter+1);
  printf("\n");
  printf("Residual Reduction: [%1.4e] <== Tolerance [%1.4e]\n",norme,TOL);
  printf("\n");
  printf("Solver Statistics [Wall Time, Memory Usage]\n");
  // Compute Elapsed Time for accumulative iters
  /*printf("-------------------------------------------\n");
  printf("|   Elapsed Time: [%g sec]                 \n",get_time()-t);
  printf("|   Elapsed Time: [%g sec/iter]            \n",(get_time()-t)/(iter+1));
  printf("-------------------------------------------\n");
  printf("|  Memory Usage: [%lf MB]                  \n", r_usage.ru_maxrss*0.001);
  printf("-------------------------------------------\n");*/

  //cudaMemcpy(kcond,kcond_d, numElems * sizeof(double),cudaMemcpyDeviceToHost);
  //cudaMemcpy(activenodes, activenodes_d, numNodes * sizeof(int), cudaMemcpyDeviceToHost);

    //cudaMemcpy(M, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  //for (int n=0; n<numNodes; n++){
    //printf("M[%d]=%f\n",n,M[n]);
    // if (U[n] < 0) U[n] = 0.0;
  //}
  // Post-Processing

  FILE *fichier_probe;
  fichier_probe= fopen("probes.txt","w");

  for (i=0;i<build_height*total_steps;i++) {
    //fprintf(fichier_probe,"%lg %lg %lg %lg %lg %lg %lg\n",probe1[i],probe2[i],probe3[i],probe4[i],probe5[i],probe6[i],probe7[i]);
    fprintf(fichier_probe,"%lg %lg %lg %lg\n",time_probe_tab[i],probe1[i],probe2[i],probe3[i]);
  }

  fclose(fichier_probe);

    for (e=0;e<numElems;e++){
      if(domain[e]==0){
  k=e/(ni*nj);
  j=(e%(nj*ni))/ni;
  i=(e%(nj*ni))%ni;

  id=(ni+1)*(nj+1)*k+(ni+1)*j+i;

  edofMat[0]=id;
  edofMat[1]=id+1;
  edofMat[2]=id+1+(ni+1);
  edofMat[3]=id+(ni+1);
  edofMat[4]=id + (ni+1)*(nj+1);
  edofMat[5]=id+1 + (ni+1)*(nj+1);
  edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
  edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

  activenodes[edofMat[0]]=1;
  activenodes[edofMat[1]]=1;
  activenodes[edofMat[2]]=1;
  activenodes[edofMat[3]]=1;
  activenodes[edofMat[4]]=1;
  activenodes[edofMat[5]]=1;
  activenodes[edofMat[6]]=1;
  activenodes[edofMat[7]]=1;
}
}
solution = fopen("MatFree_Temp3D_Wen.txt", "w");
//fprintf(solution, "i,j,k,a1,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15,u16,u17,u18,u19,u20,u21,u22,u23,u24,u25\n");
fprintf(solution, "i,j,k,a1,u1\n");

  double max=-1;
  double min=1e6;
  double lz;
  for (k=0; k<(nk+1); k++) {
    for (j=0; j<(nj+1); j++) {
      for (i=0; i<(ni+1); i++) {
        if(k<=base_height) {
          lz=k*delta_z1;
        }
          else{
            lz=base_height*delta_z1+(k-base_height)*delta_z;
          }
          i0=I3D(ni2,nj2,i,j,k);

          //if(hconv6[i0]==50) printf("i=%d,j=%d,k=%d\n",i,j,k);
        //fprintf(solution,"%lg,%lg,%lg,%d,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n",i*delta_x,j*delta_y,lz, activenodes[i0],U1[i0],U2[i0],U3[i0],U4[i0],U5[i0],U6[i0],U7[i0],U8[i0],U9[i0],U10[i0],U11[i0],U12[i0],U13[i0],U14[i0],U15[i0],U16[i0],U17[i0],U18[i0],U19[i0],U20[i0],U21[i0],U22[i0],U23[i0],U24[i0],U25[i0]);
        fprintf(solution,"%lg,%lg,%lg,%d,%lg\n",i*delta_x,j*delta_y,lz, activenodes[i0],U[i0]);
        /*if(i==25 && j==25) printf("lz=%lg\n",lz);
        if(k==(nk-1)) {
          lz=lz+delta_z;
          if(i==25 && j==25) printf("lz=%lg\n",lz);
          fprintf(solution,"%lg,%lg,%lg,%d,%lg\n",i*delta_x,j*delta_y,lz+delta_z, domain_optim[i0],hconv6[i0]);
        }*/

          //fprintf(solution,"%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n",i*delta_x,j*delta_y,lz,hconv1[i0],hconv2[i0],hconv3[i0],hconv4[i0],hconv5[i0],hconv6[i0]);
          //fprintf(solution,"%d,%d,%d,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n",i,j,k, U1[i0],U2[i0],U3[i0],U4[i0],U5[i0],U6[i0],U7[i0],U8[i0],U9[i0],U10[i0],U11[i0],U12[i0],U13[i0],U14[i0]);
          //fprintf(solution,"%d,%d,%d,%lg,%d\n",i,j,k,U11[i0],activenodes[i0]);
          if(U[i0]>max) max=U[i0];
          if(U[i0]<min && U[i0]>Tini) min=U[i0];
              }
            }
          }
    printf("Tmin=%lg and Tmax=%lg\n",min,max);
  fclose(solution);


  //free gpu memory
  /*cudaFree(U_d);   cudaFree(f_d);   cudaFree(b_d);   cudaFree(b1_d);  cudaFree(M_d);   cudaFree(prod_d);  cudaFree(fixednodes_d);
  cudaFree(kcond_d);   cudaFree(rho_d); cudaFree(c_d); cudaFree(hconv1_d); cudaFree(hconv2_d); cudaFree(hconv3_d); cudaFree(hconv4_d);
  cudaFree(r_d);
  cudaFree(p_d);
  cudaFree(Ap_d);
  cudaFree(z_d);*/
  checkCudaErrors(cudaFree(Ke_d));   checkCudaErrors(cudaFree(Me_d)); checkCudaErrors(cudaFree(Qe1_d));
  checkCudaErrors(cudaFree(He1_d)); checkCudaErrors(cudaFree(He2_d)); checkCudaErrors(cudaFree(He3_d)); checkCudaErrors(cudaFree(He4_d)); checkCudaErrors(cudaFree(He5_d)); checkCudaErrors(cudaFree(He6_d));
  checkCudaErrors(cudaFree(Fe1_d)); checkCudaErrors(cudaFree(Fe2_d)); checkCudaErrors(cudaFree(Fe3_d)); checkCudaErrors(cudaFree(Fe4_d)); checkCudaErrors(cudaFree(Fe5_d)); checkCudaErrors(cudaFree(Fe6_d));
  //cudaFree(pos_heat_d); cudaFree(domain_optim_d); cudaFree(activenodes_d); cudaFree(convnodes_x_d); cudaFree(convnodes_y_d);
  //cudaFree(face_d);
  //cudaFree(edofMat_d);
  checkCudaErrors(cudaFree(pAp_d));
  checkCudaErrors(cudaFree(rznew_d));
  checkCudaErrors(cudaFree(rNorm_d));
  checkCudaErrors(cudaFree(fNorm_d));

    checkCudaErrors(cudaFree(x_list_d));
    checkCudaErrors(cudaFree(kcond_list_d));
    checkCudaErrors(cudaFree(rho_list_d));
    checkCudaErrors(cudaFree(c_list_d));


  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
