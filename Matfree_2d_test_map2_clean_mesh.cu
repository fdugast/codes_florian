#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <cublas.h>
#include <cublas_v2.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define NDim 2
#define MNE 4

#define BLOCKSIZE 32

#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)
#define I2D(ni,i,j) (((ni)*(j)) + i)

const char *sSDKname = "conjugateGradientMultiDeviceCG";

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

void KM_matrix(double delta_x, double delta_y,double delta_z, double *Ke,double *Me, double *He1, double *He2, double *He3,
  double *He4, double *He5, double *He6, double *Fe1, double *Fe2,double *Fe3, double *Fe4,double *Fe5, double *Fe6, double *Qe1){
//  double N[8];
  double DN[3][8],N[8];
  double gxyz[3][3];
  double wg[3][3],wxyz;
  int i,j,k,i1,j1;
  double a,b,c;
  double x,y,z,sommeK,sommeM,sommeHe1,sommeHe2,sommeHe3,sommeHe4,sommeHe5,sommeHe6, sommeFe1,sommeFe2,sommeFe3,sommeFe4,sommeFe5,sommeFe6;
  double sommeQe1;

  //gxyz[0][0]=0;
  //gxyz[0][1]=-0.577350269189626;
  gxyz[0][2]=-sqrt(3.f/5);
  //gxyz[1][0]=0;
  //gxyz[1][1]=0.577350269189626;
  gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
  gxyz[2][2]=sqrt(3.f/5);

  wg[0][0]=2;
  wg[0][1]=1;
  wg[0][2]=5.f/9;
  wg[1][0]=0;
  wg[1][1]=1;
  wg[1][2]=8.f/9;
  wg[2][0]=0;
  wg[2][1]=0;
  wg[2][2]=5.f/9;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;
        //printf("boucle\n");


  for (i1=0;i1<8;i1++){
      for (j1=0;j1<8;j1++){
sommeK=0;
sommeM=0;
          for (i=0;i<3;i++){
            for (j=0;j<3;j++){
              for (k=0;k<3;k++){

                x=gxyz[i][2];
                y=gxyz[j][2];
                z=gxyz[k][2];
                wxyz=wg[i][2]*wg[j][2]*wg[k][2];

                N[0]=(1-x)*(1-y)*(1-z)/8;
                N[1]=(1+x)*(1-y)*(1-z)/8;
                N[2]=(1+x)*(1+y)*(1-z)/8;
                N[3]=(1-x)*(1+y)*(1-z)/8;

                N[4]=(1-x)*(1-y)*(1+z)/8;
                N[5]=(1+x)*(1-y)*(1+z)/8;
                N[6]=(1+x)*(1+y)*(1+z)/8;
                N[7]=(1-x)*(1+y)*(1+z)/8;

                DN[0][0]=-(1-y)*(1-z)*(1/a)*0.125;   DN[1][0]=-(1-x)*(1-z)*(1/b)*0.125;    DN[2][0]=-(1-x)*(1-y)*(1/c)*0.125;
                DN[0][1]=(1-y)*(1-z)*(1/a)*0.125;    DN[1][1]=-(1+x)*(1-z)*(1/b)*0.125;    DN[2][1]=-(1+x)*(1-y)*(1/c)*0.125;
                DN[0][2]=(1+y)*(1-z)*(1/a)*0.125;    DN[1][2]=(1+x)*(1-z)*(1/b)*0.125;     DN[2][2]=-(1+x)*(1+y)*(1/c)*0.125;
                DN[0][3]=-(1+y)*(1-z)*(1/a)*0.125;   DN[1][3]=(1-x)*(1-z)*(1/b)*0.125;     DN[2][3]=-(1-x)*(1+y)*(1/c)*0.125;
                DN[0][4]=-(1-y)*(1+z)*(1/a)*0.125;   DN[1][4]=-(1-x)*(1+z)*(1/b)*0.125;    DN[2][4]=(1-x)*(1-y)*(1/c)*0.125;
                DN[0][5]=(1-y)*(1+z)*(1/a)*0.125;    DN[1][5]=-(1+x)*(1+z)*(1/b)*0.125;    DN[2][5]=(1+x)*(1-y)*(1/c)*0.125;
                DN[0][6]=(1+y)*(1+z)*(1/a)*0.125;    DN[1][6]=(1+x)*(1+z)*(1/b)*0.125;     DN[2][6]=(1+x)*(1+y)*(1/c)*0.125;
                DN[0][7]=-(1+y)*(1+z)*(1/a)*0.125;   DN[1][7]=(1-x)*(1+z)*(1/b)*0.125;     DN[2][7]=(1-x)*(1+y)*(1/c)*0.125;

                sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1]+DN[2][i1]*DN[2][j1])*wxyz*a*b*c;
                sommeM=sommeM+(N[i1]*N[j1])*wxyz*a*b*c;

              }
            }
          }
          Ke[i1+8*j1]=sommeK;
          Me[i1+8*j1]=sommeM;
          //printf("%lg ",Me[i1+8*j1]);
    }
      //printf("\n");
  }

//printf("\n \n");

//HE1
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe1=sommeHe1+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He1[i1+8*j1]=sommeHe1;
        //printf("%lg ",He1[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE2
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe2=sommeHe2+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He2[i1+8*j1]=sommeHe2;
        //printf("%lg ",He2[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE3
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe3=sommeHe3+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He3[i1+8*j1]=sommeHe3;
        //printf("%lg ",He3[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE4
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe4=sommeHe4+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He4[i1+8*j1]=sommeHe4;
        //printf("%lg ",He4[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");


//HE5
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe5=sommeHe5+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He5[i1+8*j1]=sommeHe5;
        //printf("%lg ",He5[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE6
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe6=sommeHe6+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He6[i1+8*j1]=sommeHe6;
        //printf("%lg ",He6[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe1=sommeFe1+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe1[i1]=sommeFe1;
        //printf("%lg ",Fe1[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE2
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe2=sommeFe2+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe2[i1]=sommeFe2;
        //printf("%lg ",Fe2[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE3
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe3=sommeFe3+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe3[i1]=sommeFe3;
        //printf("%lg ",Fe3[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE4
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe4=sommeFe4+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe4[i1]=sommeFe4;
        //printf("%lg ",Fe4[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE5
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe5=sommeFe5+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe5[i1]=sommeFe5;
        //printf("%lg ",Fe5[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE6
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe6=sommeFe6+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe6[i1]=sommeFe6;
        //printf("%lg ",Fe6[i1]);
  //}
    //printf("\n");
}

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeQe1=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              //wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeQe1=sommeQe1+N[i1]*wxyz*a*b*c;

            }
          }
        }
        Qe1[i1]=sommeQe1;
        printf("%lg ",Qe1[i1]);
  //}
    //printf("\n");
}

}

void KM_matrix_2D(double delta_x, double delta_y, double *Ke,double *Me, double *He1, double *He2, double *He3,
  double *He4, double *Fe1, double *Fe2,double *Fe3, double *Fe4, double *Qe1){
//  double N[8];
  double DN[2][4],N[4];
  double gxyz[3][3];
  double wg[3][3],wxyz;
  int i,j,i1,j1;
  double a,b;
  double x,y,sommeK,sommeM,sommeHe1,sommeHe2,sommeHe3,sommeHe4, sommeFe1,sommeFe2,sommeFe3,sommeFe4;
  double sommeQe1;

  //gxyz[0][0]=0;
  //gxyz[0][1]=-0.577350269189626;
  gxyz[0][2]=-sqrt(3.f/5);
  //gxyz[1][0]=0;
  //gxyz[1][1]=0.577350269189626;
  gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
  gxyz[2][2]=sqrt(3.f/5);

  wg[0][0]=2;
  wg[0][1]=1;
  wg[0][2]=5.f/9;
  wg[1][0]=0;
  wg[1][1]=1;
  wg[1][2]=8.f/9;
  wg[2][0]=0;
  wg[2][1]=0;
  wg[2][2]=5.f/9;

a=0.5*delta_x;
b=0.5*delta_y;
//c=0.5*delta_z;
        //printf("boucle\n");


  for (i1=0;i1<4;i1++){
      for (j1=0;j1<4;j1++){
sommeK=0;
sommeM=0;
          for (i=0;i<3;i++){
            for (j=0;j<3;j++){
              //for (k=0;k<3;k++){

                x=gxyz[i][2];
                y=gxyz[j][2];
                //z=gxyz[k][2];
                wxyz=wg[i][2]*wg[j][2];//*wg[k][2];

                N[0]=(1-x)*(1-y)/4;
                N[1]=(1+x)*(1-y)/4;
                N[2]=(1+x)*(1+y)/4;
                N[3]=(1-x)*(1+y)/4;


                DN[0][0]=-(1-y)*(1/a)*0.25;   DN[1][0]=-(1-x)*(1/b)*0.25;
                DN[0][1]=(1-y)*(1/a)*0.25;    DN[1][1]=-(1+x)*(1/b)*0.25;
                DN[0][2]=(1+y)*(1/a)*0.25;    DN[1][2]=(1+x)*(1/b)*0.25;
                DN[0][3]=-(1+y)*(1/a)*0.25;   DN[1][3]=(1-x)*(1/b)*0.25;


                sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1])*wxyz*a*b;
                sommeM=sommeM+(N[i1]*N[j1])*wxyz*a*b;

              //}
            }
          }
          Ke[i1+4*j1]=sommeK;
          Me[i1+4*j1]=sommeM;
          //printf("%lg ",Me[i1+8*j1]);
    }
      //printf("\n");
  }

//printf("\n \n");

//HE1
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;


              sommeHe1=sommeHe1+(N[i1]*N[j1])*wxyz*b;

          //  }
          }
        //}
        He1[i1+4*j1]=sommeHe1;
        //printf("%lg ",He1[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE2
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe2=sommeHe2+(N[i1]*N[j1])*wxyz*b;

            //}
          }
        //}
        He2[i1+4*j1]=sommeHe2;
        //printf("%lg ",He2[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE3
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe3=sommeHe3+(N[i1]*N[j1])*wxyz*a;

          //  }
          }
        //}
        He3[i1+4*j1]=sommeHe3;
        //printf("%lg ",He3[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE4
for (i1=0;i1<4;i1++){
    for (j1=0;j1<4;j1++){
        sommeHe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeHe4=sommeHe4+(N[i1]*N[j1])*wxyz*a;

          //  }
          }
        //}
        He4[i1+4*j1]=sommeHe4;
        //printf("%lg ",He4[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//FE1
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
          //  for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
            //  z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe1=sommeFe1+N[i1]*wxyz*b;

            //}
          }
        //}
        Fe1[i1]=sommeFe1;
        //printf("%lg ",Fe1[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE2
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe2=sommeFe2+N[i1]*wxyz*b;

            //}
          }
        //}
        Fe2[i1]=sommeFe2;
        //printf("%lg ",Fe2[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE3
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
            //  z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe3=sommeFe3+N[i1]*wxyz*a;

            //}
          }
        //}
        Fe3[i1]=sommeFe3;
        //printf("%lg ",Fe3[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE4
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              //z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2];//*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeFe4=sommeFe4+N[i1]*wxyz*a;

            //}
          }
        //}
        Fe4[i1]=sommeFe4;
        //printf("%lg ",Fe4[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");


//FE1
for (i1=0;i1<4;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeQe1=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              //z=gxyz[k][2];
              wxyz=wg[i][2]*wg[j][2];//*wg[k][2];
              //wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)/4;
              N[1]=(1+x)*(1-y)/4;
              N[2]=(1+x)*(1+y)/4;
              N[3]=(1-x)*(1+y)/4;

              sommeQe1=sommeQe1+N[i1]*wxyz*a*b;

            //}
          }
        }
        Qe1[i1]=sommeQe1;
      //  printf("%lg ",Qe1[i1]);
  //}
    //printf("\n");
}

}

void domain_definition (int ni,int nj1,int *domain_coarse)
{
  int i,j,i0;

    for (j=0; j<nj1; j++) {
        for (i=0; i<ni; i++) {

        i0=I2D(ni,i,j);
        //printf("i0=%d\n",i0);
        domain_coarse[i0]=1;

              //if(i<(100+10) && i>=(100-10) && j<(100+10) && j>=(100-10) && k<80){
          /*if(j==2 && (i==1 || i==4 ))      domain[i0]=0;
          if(j==3 && (i==1 || i==2 || i==3 || i==4))      domain[i0]=0;
          if(j==4 && (i==2 || i==3 ))      domain[i0]=0;*/

          if(j<1 && j>=0){
            if(i<2 && i>=1) domain_coarse[i0]=0;
            if(i<5 && i>=4) domain_coarse[i0]=0;
          }
          if(j<2 && j>=1){
            //if(i<2 && i>=1) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
            if(i<5 && i>=1) domain_coarse[i0]=0;

          }
          if(j<3 && j>=2){
            //if(i<2 && i>=1) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
            if(i<4 && i>=2) domain_coarse[i0]=0;

          }
          if( j>=3){
            //if(i<2 && i>=1) domain[i0]=0;
            //if(i<5 && i>=4) domain[i0]=0;
            if(i<4 && i>=2) domain_coarse[i0]=0;

          }



//domain_optim[i0]=0;
  //printf("i=%d,j=%d,domain_coarse[%d]=%d\n",i,j,i0,domain_coarse[i0]);
    }
  }
}

void domain_update (int ni,int nj1, int nj, int base_height, int *map_elements, int *domain_coarse, int *domain_fine, int resolution, int coarse_layers)
{

int i,j,j2,i0,i02;

for (j=0; j<nj; j++) {
    for (i=0; i<ni; i++) {

    i0=I2D(ni,i,j);
    //printf("i0=%d\n",i0);
    domain_fine[i0]=1;

  }
}

for (j=0; j<base_height; j++) {
    for (i=0; i<ni; i++) {

    i0=I2D(ni,i,j);
    //printf("i0=%d\n",i0);
    domain_fine[i0]=0;
    //printf("domain_fine_1[%d]=%d\n",i0,domain_fine[i0]);
    map_elements[i0]=i0;

  }
}

//for (j=0; j<nj1; j++) {
for (j=0; j<coarse_layers; j++) {
    j2=base_height+j;
    for (i=0; i<ni; i++) {

    i0=I2D(ni,i,j);
    i02=I2D(ni,i,j2);
    domain_fine[i02]=domain_coarse[i0];
    //printf("domain_fine_2[%d]=%d\n",i02,domain_fine[i02]);

  }
}

for (j=coarse_layers; j<nj1; j++) {
  for (j2=base_height+coarse_layers+resolution*(j-coarse_layers); j2<base_height+coarse_layers+resolution*(j+1-coarse_layers); j2++) {
    for (i=0; i<ni; i++) {
      i02=I2D(ni,i,j2);
    i0=I2D(ni,i,j);
    domain_fine[i02]=domain_coarse[i0];
    //printf("domain_fine_3[%d]=%d\n",i02,domain_fine[i02]);
  }
}
}

/*for (j=0; j<nj; j++) {
    for (i=0; i<ni; i++) {

    i0=I2D(ni,i,j);
    printf("i=%d,j=%d,domain_fine[%d]=%d\n",i,j,i0,domain_fine[i0]);


  }
}*/

}

int domain_coarse_elements(int ni, int nj, int base_height, int *domain_fine, int coarse_layers){
  int i,j,i0;
  int coarse_elements=0;
  for (j=0; j<nj; j++) {
      for (i=0; i<ni; i++) {

      i0=I2D(ni,i,j);

      if(domain_fine[i0]==0 && j<base_height+coarse_layers) coarse_elements++;

    }
  }
  return coarse_elements;
}

  // Set residual to zero
//void SetResZero(double *r, int numNodes)
//{
//	memset(r,0.0,sizeof(numNodes));
//}

void active_domain(int ni, int nj, int numElems, int base_height, int layer_height, int *domain, int *domain_optim, int *pos_heat)
{
  //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int j; int e=0;

  while (e<numElems){

    j=e/ni;
    //i=e%ni;

    if(j<base_height) {
       domain_optim[e]=0;
     }
     else{
           //if(i<(100+10) && i>=(100-10) && j<(100+10) && j>=(100-10) && k<80){
       if(j<=layer_height){
         domain_optim[e]=domain[e];
       }
       else{
         domain_optim[e]=1;
       }
     }
     if(j==layer_height && domain[e]==0) pos_heat[e]=1;
      e++;
  }
}

void Boundary_Conditions1(int ni, int nj, int numElems, int base_height,int *domain_optim,int *convnodes_x,int *convnodes_y,
  double *hconv1,double *hconv2,double *hconv3,double *hconv4){

    //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
    int e=0;
    int i,j,id,i1,edofMat[MNE];

    while (e<numElems){

      j=e/ni;
      i=e%ni;
      id=(ni+1)*j+i;

      edofMat[0]=id;
      edofMat[1]=id+1;
      edofMat[2]=id+1+(ni+1);
      edofMat[3]=id+(ni+1);

      if(i==0 && domain_optim[e]==0)       {hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1;   }
      if(i==(ni-1) && domain_optim[e]==0)  {hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2;   }
      if(j==0 && domain_optim[e]==0)       {hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   }
      //if(j==(nj-1) && domain_optim[e]==0)  {hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   convnodes_y[edofMat[6]]=4;    convnodes_y[edofMat[7]]=4;}
      //if(k==0 && domain_optim[e]==0)       {hconv5[e]=5;    convnodes_z[edofMat[0]]=5;      convnodes_z[edofMat[1]]=5;   convnodes_z[edofMat[2]]=5;    convnodes_z[edofMat[3]]=5;}
      //if(k==(nk-1) && domain_optim[e]==0)  {hconv6[e]=50;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
      i1=e-1;
      if(i1>0 ){
        if(domain_optim[e]==0 && domain_optim[i1]==1 && i>0) {
          hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1;
        }
      }
      i1=e+1;
      if(i1<ni*nj){
        if(domain_optim[e]==0 && domain_optim[i1]==1 && i<(ni-1)) {
          hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2;
        }
      }
      /*i1=e-ni;
      if(i1>0){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   convnodes_y[edofMat[4]]=3;    convnodes_y[edofMat[5]]=3;
        }
      }*/
      i1=e+ni;
      if(i1<ni*nj){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          if (j<base_height) {hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   }
          if (j>=base_height) {hconv4[e]=55;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   }
        }
      }
      /*i1=e+ni*nj;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          if(k<base_height)       {hconv6[e]=5;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
          if(k>=base_height)       {hconv6[e]=55;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
        }
      }*/
      e++;
    }
  }


  void Boundary_Conditions2(int ni, int nj, int numElems, int *domain_optim,int *convnodes_x,int *convnodes_y,
    double *hconv1,double *hconv2,double *hconv3,double *hconv4, double Tinf, double *b1,
    double *Fe1,double *Fe2,double *Fe3,double *Fe4,double *Fe1_b,double *Fe2_b,double *Fe3_b,double *Fe4_b, int base_height ){

      //size_t e = blockIdx.x * blockDim.x + threadIdx.x;
      int e=0;
      int i,j,l,id,edofMat[MNE];

      while (e<numElems){

        j=e/ni;
        i=e%ni;
        id=(ni+1)*j+i;

        edofMat[0]=id;
        edofMat[1]=id+1;
        edofMat[2]=id+1+(ni+1);
        edofMat[3]=id+(ni+1);
        /*edofMat[4]=id + (ni+1)*(nj+1);
        edofMat[5]=id+1 + (ni+1)*(nj+1);
        edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
        edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);*/

        for(l=0;l<MNE;l++){
                if(convnodes_x[edofMat[l]]==1)  {
                //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                if(e<ni*base_height){
                  b1[edofMat[l]]=b1[edofMat[l]]+hconv1[e]*Tinf*Fe1_b[l];
                }
                else{
                  b1[edofMat[l]]=b1[edofMat[l]]+hconv1[e]*Tinf*Fe1[l];
                }
                  //atomicAdd(&b1[edofMat[l]], hconv1[e]*Tinf*Fe1[l]);

                }
                if(convnodes_x[edofMat[l]]==2) {
                  //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                  if(e<ni*base_height){
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv2[e]*Tinf*Fe2_b[l];
                 }
                 else{
                  b1[edofMat[l]]=b1[edofMat[l]]+hconv2[e]*Tinf*Fe2[l];
                 }
                    //atomicAdd(&b1[edofMat[l]], hconv2[e]*Tinf*Fe2[l]);
                 }

                 if(convnodes_y[edofMat[l]]==3)  {
                 //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                   if(e<ni*base_height){
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv3[e]*Tinf*Fe3_b[l];
                 }
                 else{
                   b1[edofMat[l]]=b1[edofMat[l]]+hconv3[e]*Tinf*Fe3[l];
                 }
                   //atomicAdd(&b1[edofMat[l]], hconv3[e]*Tinf*Fe3[l]);

                 }
                 if(convnodes_y[edofMat[l]]==4) {
                   //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                    if(e<ni*base_height){
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv4[e]*Tinf*Fe4_b[l];
                  }
                  else{
                    b1[edofMat[l]]=b1[edofMat[l]]+hconv4[e]*Tinf*Fe4[l];
                  }
                    //atomicAdd(&b1[edofMat[l]], hconv4[e]*Tinf*Fe4[l]);
                  }

                  /*if(convnodes_z[edofMat[l]]==5)  {
                  //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                    //b1[edofMat[l]]=b1[edofMat[l]]+hconv5[e]*Tinf*Fe5[l];
                    atomicAdd(&b1[edofMat[l]], hconv5[e]*Tinf*Fe5[l]);

                  }
                  if(convnodes_z[edofMat[l]]==6) {
                    //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                     //b1[edofMat[l]]=b1[edofMat[l]]+hconv6[e]*Tinf*Fe6[l];
                     atomicAdd(&b1[edofMat[l]], hconv6[e]*Tinf*Fe6[l]);
                   }*/
            }
            e++;
      }
    }

    int new_elements_counter(int ni, int nj, int layer_height, int base_height,int *domain_optim, bool init)
    {
    int l1,l2,i,j,i0;
    if(layer_height==base_height){
      l1=layer_height;
      l2=layer_height;
    }
    else{
      l1=layer_height-1;
      l2=layer_height;
    }
    //printf("l1=%d et l2=%d\n",l1,l2);
    //New element counter
    int new_elements=0;
    for (i=0; i<ni; i++) {
        for (j=0; j<nj; j++) {
          if(j>=l1 && j<=l2){
        i0=I2D(ni,i,j);
        if(domain_optim[i0]==0){
          new_elements++;
        }
      }
    }
  }
  return new_elements;
}

int map_elements_calculation(int ni, int base_height, int old_elements, int new_elements, int *ele_id, int *map_elements){
  int   numElems2=base_height*ni+old_elements;
  int *ele2_id = (int *)calloc(new_elements, sizeof(int));
  for (int i = 0; i < new_elements; i++){
  ele2_id[i]=numElems2;
  map_elements[ele_id[i]]=ele2_id[i];
  numElems2++;
    }
    return numElems2;

}

int map_nodes_calculation(int ni, int new_elements, int new_ndof, int old_node, int old_node2, int *node_id, int *ele_id,int *ele_id_node, int *pos_id_node, int *map_nodes, int *node_id2)
{
  int cpt=0;
  int e1,e,i,j,id1;

  //node_id array of new elements
  cpt=0;
  for (e1=0;e1<new_elements;e1++){
      e=ele_id[e1];
      j=e/ni;
      i=e%ni;
      id1=(ni+1)*j+i;
      node_id[cpt]=id1; ele_id_node[cpt]=e; pos_id_node[cpt]=1; cpt++;
      node_id[cpt]=id1+1; ele_id_node[cpt]=e; pos_id_node[cpt]=2; cpt++;
      node_id[cpt]=id1+1+(ni+1); ele_id_node[cpt]=e; pos_id_node[cpt]=3; cpt++;
      node_id[cpt]=id1+(ni+1); ele_id_node[cpt]=e; pos_id_node[cpt]=4; cpt++;
    }
    //sort of node id
    int a;
    for (i = 0; i < new_ndof; ++i)  {
          for (j = i + 1; j < new_ndof; ++j)  {
              if (node_id[i] > node_id[j])
              {
                  a =  node_id[i];
                  node_id[i] = node_id[j];
                  node_id[j] = a;

                  a =  ele_id_node[i];
                  ele_id_node[i] = ele_id_node[j];
                  ele_id_node[j] = a;

                  a =  pos_id_node[i];
                  pos_id_node[i] = pos_id_node[j];
                  pos_id_node[j] = a;
              }
          }
      }


      // Node id removal

      int min_node=old_node;
      int min_node2=old_node2;


      for (i = 0; i < new_ndof; ++i){
        if(node_id[i]<min_node2){
          node_id2[i]=map_nodes[node_id[i]];

        }
        else{
          if(node_id[i]==node_id[i-1]){
            node_id2[i]=node_id2[i-1];

          }
          else{
            node_id2[i]=min_node;

            min_node=min_node+1;
          }
        }

      }
      int numNodes2=min_node;

      //Update map_nodes
      for (i=0;i<new_ndof;i++) {
        map_nodes[node_id[i]]=node_id2[i];

      }

      //Sorting elements
      for (i = 0; i < new_ndof; ++i)
        {
            for (j = i + 1; j < new_ndof; ++j)
            {
                if (ele_id_node[i] > ele_id_node[j])
                {
                    a =  node_id2[i];
                    node_id2[i] = node_id2[j];
                    node_id2[j] = a;

                    a =  node_id[i];
                    node_id[i] = node_id[j];
                    node_id[j] = a;

                    a =  ele_id_node[i];
                    ele_id_node[i] = ele_id_node[j];
                    ele_id_node[j] = a;

                    a =  pos_id_node[i];
                    pos_id_node[i] = pos_id_node[j];
                    pos_id_node[j] = a;

                }
            }
        }
      return numNodes2;
}

void ele_id_calculation(int ni, int nj, int base_height, int layer_height, int *domain_optim, int *ele_id, bool init)
{
  int i,j,i0,l1,l2;
  int cpt=0;

  if(layer_height==base_height){
    l1=layer_height;
    l2=layer_height;
  }
  else{
    l1=layer_height-1;
    l2=layer_height;
  }

    for (j=0; j<nj; j++) {
      for (i=0; i<ni; i++) {
    if(j>=l1 && j<=l2){
    i0=I2D(ni,i,j);
      if(domain_optim[i0]==0){
      ele_id[cpt]=i0;
      //printf("ele_id[%d]=%d\n",cpt,ele_id[cpt]);
      cpt++;
      }
  }
}
}
}


__global__ void Mat_Properties(double *U, int ni, int nj, double *kcond, double *rho, double *c, int *edof, int numElems,  int base_height)
{

  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,id1,edofMat2[MNE];
  double Te;

  if (e<numElems){

    if(e<base_height*ni){
    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
  }
  else{
    edofMat2[0]=edof[(e-base_height*ni)*4+0];
    edofMat2[1]=edof[(e-base_height*ni)*4+1];
    edofMat2[2]=edof[(e-base_height*ni)*4+2];
    edofMat2[3]=edof[(e-base_height*ni)*4+3];
  }
//printf("e=%d, Mat Properties : Hello from block %d, thread %d\n", e,blockIdx.x, threadIdx.x);

  /*edofMat2[0]=edof[4*e+0];
  edofMat2[1]=edof[4*e+1];
  edofMat2[2]=edof[4*e+2];
  edofMat2[3]=edof[4*e+3];*/

    //printf("thread=%d : \n",threadIdx.x,edofMat_all[4*e+0],edofMat_all[4*e+1],edofMat_all[4*e+2],edofMat_all[4*e+3]);

    /*edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);*/

    Te=0.25*(U[edofMat2[0]]+U[edofMat2[1]]+U[edofMat2[2]]+U[edofMat2[3]]);

    //id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;


    /*  c[e]=421+0.1827*Te;
      kcond[e]=11.9+0.01543*Te;
      rho[e]=8220-0.3663*Te;*/
            //if(k<=height_k[compteur]){

      if(Te<=20) c[e]=421;
      if(Te>20 && Te<=100)  c[e] =(double)421+(442-421)*(Te-20)/(100-20);
      if(Te>100 && Te<=200) c[e] =(double)442+(453-442)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) c[e] =(double)453+(472-453)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) c[e] =(double)472+(481-472)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) c[e] =(double)481+(502-481)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) c[e] =(double)502+(527-502)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) c[e] =(double)527+(562-527)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) c[e] =(double)562+(606-562)*(Te-700)/(800-700);
      if(Te>800 && Te<=850) c[e] =(double)606+(628-606)*(Te-800)/(850-800);
      if(Te>850 && Te<=900) c[e] =(double)628+(636-628)*(Te-850)/(900-850);
      if(Te>900 && Te<=1000)  c[e]=(double)636+(647-636)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) c[e]=(double)647+(651-647)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1500) c[e]=(double)651+(652-651)*(Te-1100)/(1500-1100);
      if(Te>1500) c[e]=652;

      if(Te<=22) kcond[e]=11.9;
      if(Te>22 && Te<=233)  kcond[e] =(double)11.9+(13.7-11.9)*(Te-22)/(233-22);
      if(Te>233 && Te<=448) kcond[e] =(double)13.7+(16.9-13.7)*(Te-233)/(448-233);
      if(Te>448 && Te<=657) kcond[e] =(double)16.9+(21.7-16.9)*(Te-448)/(657-448);
      if(Te>657 && Te<=866) kcond[e] =(double)21.7+(25.6-21.7)*(Te-657)/(866-657);
      if(Te>866 && Te<=1079) kcond[e] =(double)25.6+(22.9-25.6)*(Te-866)/(1079-866);
      if(Te>1079 && Te<=1289) kcond[e] =(double)22.9+(19.1-22.9)*(Te-1079)/(1289-1079);
      if(Te>1289 && Te<=1500) kcond[e] =(double)19.1+(17.7-19.1)*(Te-1289)/(1500-1289);
      if(Te>1500) kcond[e]=17.7;


      if(Te<=20) rho[e]=8220;
      if(Te>20 && Te<=227)     rho[e] =(double)8220+(8121-8220)*(Te-20)/(227-20);
      if(Te>227 && Te<=427)    rho[e] =(double)8121+(8121-8048)*(Te-227)/(427-227);
      if(Te>427 && Te<=727)    rho[e] =(double)8048+(8121-8048)*(Te-427)/(727-427);
      if(Te>727 && Te<=927)    rho[e] =(double)7961+(8121-8048)*(Te-727)/(927-727);
      if(Te>927 && Te<=1127)   rho[e] =(double)7875+(8121-8048)*(Te-927)/(1127-927);
      if(Te>1127 && Te<=1260)  rho[e] =(double)7787+(8121-8048)*(Te-1127)/(1260-1127);
      if(Te>1260 && Te<=1344)  rho[e] =(double)7733+(8121-8048)*(Te-1260)/(1344-1260);
      if(Te>1344 && Te<=1450)  rho[e] =(double)7579+(8121-8048)*(Te-1344)/(1450-1344);
      if(Te>1450 && Te<=1527)  rho[e] =(double)7488+(8121-8048)*(Te-1450)/(1527-1450);
      if(Te>1527 && Te<=1827)  rho[e] =(double)7488+(8121-8048)*(Te-1527)/(1827-1527);
      if(Te>1827) rho[e]=7341;
      //}
        //if(k>height_k[compteur]){
          //kcond[e]=lambda_vide;
          //rhoc[e]=rho_vide*cp_vide;
        //}


  }

}

__global__ void ResetRes(double *r, int numNodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
     r[n] = 0.0;
  }
}

__global__ void GPUMatVec_Uold(int ni, int nj, double *rho, double *c, double *Me, double *Me_b, double *U, double *r, size_t numElems, int *edofMat_all, int base_height, int coarse_elements)

{
  //global gpu thread index
	//double Au;

  int i,j,id1,edofMat2[MNE];
  double Au[MNE],sum;
  bool vrai;
size_t e = blockIdx.x * blockDim.x + threadIdx.x;

  if (e<numElems){
    //printf("Mat vec old : Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);

    if(e<base_height*ni){
    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);

  }
  else{
    edofMat2[0]=edofMat_all[(e-base_height*ni)*4+0];
    edofMat2[1]=edofMat_all[(e-base_height*ni)*4+1];
    edofMat2[2]=edofMat_all[(e-base_height*ni)*4+2];
    edofMat2[3]=edofMat_all[(e-base_height*ni)*4+3];

  }

  if(e<coarse_elements){
    vrai=1;
  }
  else{
    vrai=0;
  }

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
        Au[j]= rho[e]*c[e]*(Me_b[i*MNE+j]*vrai+Me[i*MNE+j]*(1-vrai))*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }
}


__global__ void GPUMatVec(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *Me_b, double *Ke_b,
    double *He1_b, double *He2_b, double *He3_b, double *He4_b, double *U, double *r, size_t numElems, int *edofMat_all, int base_height, int coarse_elements)

{
  //global gpu thread index
	//double Au;

  int i,j,id1,edofMat2[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  bool vrai;

  if (e<numElems){

    if(e<base_height*ni){
    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    //vrai=1;
  }
  else{
    edofMat2[0]=edofMat_all[(e-base_height*ni)*4+0];
    edofMat2[1]=edofMat_all[(e-base_height*ni)*4+1];
    edofMat2[2]=edofMat_all[(e-base_height*ni)*4+2];
    edofMat2[3]=edofMat_all[(e-base_height*ni)*4+3];

  }

  if(e<coarse_elements){
    vrai=1;
  }
  else{
    vrai=0;
  }

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
          Au[j]= (rho[e]*c[e]*(vrai*Me_b[i*MNE+j]+(1-vrai)*Me[i*MNE+j])+kcond[e]*(vrai*Ke_b[i*MNE+j]+(1-vrai)*Ke[i*MNE+j])
          +hconv1[e]*(vrai*He1_b[i*MNE+j]+(1-vrai)*He1[i*MNE+j])+hconv2[e]*(vrai*He2_b[i*MNE+j]+(1-vrai)*He2[i*MNE+j])+hconv3[e]*(vrai*He3_b[i*MNE+j]+(1-vrai)*He3[i*MNE+j])+
            hconv4[e]*(vrai*He4_b[i*MNE+j]+(1-vrai)*He4[i*MNE+j]))*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }

}


__global__ void GPUMatVec_Precond(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *Me_b, double *Ke_b,
    double *He1_b, double *He2_b, double *He3_b, double *He4_b,double *r, size_t numElems, int *edofMat_all, int base_height, int coarse_elements)

{
  //global gpu thread index
	//double Au;

  int i,j,id1,edofMat2[MNE];
  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  bool vrai;

  if (e<numElems){

    if(e<base_height*ni){
    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);

  }
  else{
    edofMat2[0]=edofMat_all[(e-base_height*ni)*4+0];
    edofMat2[1]=edofMat_all[(e-base_height*ni)*4+1];
    edofMat2[2]=edofMat_all[(e-base_height*ni)*4+2];
    edofMat2[3]=edofMat_all[(e-base_height*ni)*4+3];

  }

  if(e<coarse_elements){
    vrai=1;
  }
  else{
    vrai=0;
  }


	for (int id=0; id<MNE; id++){
        for (int j=0; j<MNE; j++){
        //  Au[j]=0;
        if(j==id){
        Au= rho[e]*c[e]*(vrai*Me_b[id*MNE+j]+(1-vrai)*Me[id*MNE+j])+kcond[e]*(vrai*Ke_b[id*MNE+j]+(1-vrai)*Ke[id*MNE+j])
              +hconv1[e]*(vrai*He1_b[id*MNE+j]+(1-vrai)*He1[id*MNE+j])
              +hconv2[e]*(vrai*He2_b[id*MNE+j]+(1-vrai)*He2[id*MNE+j])
              +hconv3[e]*(vrai*He3_b[id*MNE+j]+(1-vrai)*He3[id*MNE+j])
             + hconv4[e]*(vrai*He4_b[id*MNE+j]+(1-vrai)*He4[id*MNE+j]);
                }
                                  }


      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[id]], Au);
                         }
  }

}

__global__ void rzp_calculation(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
    M_pre[n]= 1;//1.f/M_pre[n];
      //printf("M[%d]=%f\n",n,M_pre[n]);
      r[n] = f[n] - prod[n];
      z[n] = M_pre[n]*r[n];
      p[n] = z[n];
    }

  }

}


/*__global__ void heatsource_position(int ni, int nj, int *domain_optim, int base_height,int layer_height, int *pos_heat, int numElems)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int edofMat2[MNE];

  int i,j,id1;

  if (e<numElems)
  {

    j=e/ni;
    i=e%ni;
    id1=(ni+1)*j+i;

      if(domain_optim[e]==0){
        edofMat2[0]=id1;
        edofMat2[1]=id1+1;
        edofMat2[2]=id1+1+(ni+1);
        edofMat2[3]=id1+(ni+1);



    //if(height_k[compteur]>1 && k>=0+height_k[compteur] && k<layer_thickness+height_k[compteur]){
    //if(k>=base_height && k<=layer_height){
    if(j==layer_height){
      pos_heat[e]=1;


   }
  //if(k<=height_k[compteur]){



    activenodes[edofMat2[0]]=1;
    activenodes[edofMat2[1]]=1;
    activenodes[edofMat2[2]]=1;
    activenodes[edofMat2[3]]=1;


    //  }
  }


  }

}*/

/*__global__ void heatsource_position2(double *pos_heat, int numNodes, int *symnodes)
{

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
                if (symnodes[n]==1 && pos_heat[n]==1) pos_heat[n] = 0.5;
                if (symnodes[n]==2 && pos_heat[n]==1) pos_heat[n] = 0.25;
                if (symnodes[n]==3 && pos_heat[n]==1) pos_heat[n] = 0.125;
    }


}*/

__global__ void heatsource_term(int ni, int nj, int *pos_heat, double *b, int numElems, int heating,double Qvol, double *Qe1, int *edofMat_all, int base_height)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,id1,edofMat2[MNE];

  if (e<numElems){

    if(pos_heat[e]==1){

      if(e<base_height*ni){
      j=e/ni;
      i=e%ni;
      id1=(ni+1)*j+i;

      edofMat2[0]=id1;
      edofMat2[1]=id1+1;
      edofMat2[2]=id1+1+(ni+1);
      edofMat2[3]=id1+(ni+1);
    }
    else{
      edofMat2[0]=edofMat_all[(e-base_height*ni)*4+0];
      edofMat2[1]=edofMat_all[(e-base_height*ni)*4+1];
      edofMat2[2]=edofMat_all[(e-base_height*ni)*4+2];
      edofMat2[3]=edofMat_all[(e-base_height*ni)*4+3];
    }

    /*if(e==0) {
      printf("edofMat2[0]=%d, edofMat2[1]=%d, edofMat2[2]=%d, edofMat2[3]=%d\n",edofMat2[0],edofMat2[1],edofMat2[2],edofMat2[3]);
    }*/
    //printf("heatsource : Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);
        /*edofMat2[4]=id1 + (ni+1)*(nj+1);
        edofMat2[5]=id1+1 + (ni+1)*(nj+1);
        edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
        edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);*/

        /*b[edofMat2[0]]=e;
        b[edofMat2[1]]=e;
        b[edofMat2[2]]=e;
        b[edofMat2[3]]=e;*/

        atomicAdd(&b[edofMat2[0]], heating*Qvol*Qe1[0]);
        atomicAdd(&b[edofMat2[1]], heating*Qvol*Qe1[1]);
        atomicAdd(&b[edofMat2[2]], heating*Qvol*Qe1[2]);
        atomicAdd(&b[edofMat2[3]], heating*Qvol*Qe1[3]);
      /*  atomicAdd(&b[edofMat2[4]], heating*Qvol*Qe1[4]);
        atomicAdd(&b[edofMat2[5]], heating*Qvol*Qe1[5]);
        atomicAdd(&b[edofMat2[6]], heating*Qvol*Qe1[6]);
        atomicAdd(&b[edofMat2[7]], heating*Qvol*Qe1[7]);*/
      }
    }

        //f(flux_elems>0) b[n] = pos_heat[n]*Qvol/flux_elems;
      //b[n] = b[n]+pos_heat[n]*Qvol;
      //if(convnodes_x[n]==0 && convnodes_y[n]==0 && convnodes_z[n]==0) b[n]=pos_heat[n]*Qvol;

}

__global__ void force_transient(double *f, double *b, double *b1, int numNodes2)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes2){
        f[n] =  f[n]+b[n] + b1[n];
        //printf("force transient : Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);
        //f[n]=b[n];
    }
}

__global__ void UpdateVec(double *p, double *r, double beta, int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void Norm(double *r, int numNodes, int *fixednodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
      atomicAdd(&result[0], r[n]*r[n]);
    }
    }
}

__global__ void Prod_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  ){
      atomicAdd(&result[0], r[n]*z[n]);
    }
    }
}
__global__ void Update_UR(double *U, double *r, double *p, double *Ap, double alpha,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
    }
    }
}

__global__ void zCalculation(double *M_pre, double *r, double *z,  int numNodes, int *fixednodes)
    {
      size_t n = blockIdx.x * blockDim.x + threadIdx.x;

      if (n<numNodes){
        if(fixednodes[n]==0 ){
          z[n] = M_pre[n]*r[n];
        }
        }
    }


int main(int argc, char **argv)
{

  //Check out my device properties
  int nDevices;
  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
           prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
           prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
           2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  }


  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

  cudaSetDevice(0);

int i,j,j1,i0,i1,i01,i02,iter,layer_height,step,heating,node,id1,old_elements2;
size_t temp=1;
double dt;
//size_t free_memory, total_memory; double t;

//size of the domain
double domain_x=0.006;
//double domain_y=0.05;

//number of elements in each direction
int ni=6;
int nj1=6;
//int nj=20;
int ni2=ni+1;
//int nj2=nj+1;

FILE *solution;

int resolution=4;
int build_height=nj1*resolution;
int base_height=2;
int nj=build_height+base_height;
int total_steps=21;
double domain_y=nj1*1e-3+1e-3*base_height;//(double)1e-3*nj/4;
int coarse_layers=0;
double Tinf=35;
int coarse_elements=ni*base_height;

//space steps
const double delta_x=(double)domain_x/ni;
const double delta_y=(double)1e-3/resolution;
const double delta_y1=(double)1e-3;
//const double delta_y1=1e-3;//(double)domain_y/nj;
//const double delta_z=(double)domain_z/nk;

//volume multiplier
double Qvol=1.31e10;


//Total number of elements & nodes for the 3D structured uniform grid
int numElems=ni*nj;
printf("numElems=%d\n",numElems);
int numNodes=(ni+1)*(nj+1);
//const int slice=(ni+1)*(nj+1);
int *map_nodes2;
int *map_nodes = (int *)calloc(numNodes, sizeof(int));
int *map_elements = (int *)calloc(numElems, sizeof(int));

int *ele_id;
int *node_id;
int *node_id2 ;
int *ele_id_node;
int *pos_id_node;
int *edofMat_new ;
int *edofMat_all;

    int  NBLOCKS,NBLOCKS2;
    double *U_red,*hconv1_red,*hconv2_red,*hconv3_red,*hconv4_red,*b1_red;
    double *f_red;//,*p_red,*z_red,*Ap_red;
    double *U_red_d,*b_red_d,*hconv1_red_d,*hconv2_red_d,*hconv3_red_d,*hconv4_red_d,*r_red_d,*prod_red_d,*M_red_d,*b1,*b1_red_d;
    double *kcond_red_d,*rho_red_d,*c_red_d;
    double *f_red_d,*p_red_d,*z_red_d,*Ap_red_d;

    int *pos_heat_red;
    int *pos_heat_red_d,*edofMat_all_d,*fixednodes_red_d,*fixednodes_red;

    //Remplissage base_plate nodes
    for(i=0;i<(ni+1)*(base_height+1);i++) map_nodes[i]=i;

    int new_elements,new_ndof;
    new_elements=0;

    int old_elements=0;
    int old_node=(base_height+1)*(ni+1);
    int old_node2=(base_height+1)*(ni+1);
    int numNodes2=(base_height+1)*(ni+1);
    int numElems2=base_height*ni;
    edofMat_all = (int *)calloc(4, sizeof(int));
    //int *edofMat_all2 = (int *)calloc(4*numElems2, sizeof(int));

    hconv1_red = (double *)calloc(numElems2 , sizeof(double));
    hconv2_red = (double *)calloc(numElems2 , sizeof(double));
    hconv3_red = (double *)calloc(numElems2 , sizeof(double));
    hconv4_red = (double *)calloc(numElems2 , sizeof(double));


    b1_red = (double *) calloc(numNodes2 , sizeof(double));
    f_red = (double *) calloc(numNodes2 , sizeof(double));
    U_red = (double *) calloc(numNodes2 , sizeof(double));
    fixednodes_red = (int *)calloc(numNodes2 ,sizeof(int));
    pos_heat_red = (int *)calloc(numElems2 ,sizeof(int));

    b1 = (double *) calloc(numNodes , sizeof(double));


//GET GPU DEVICE
printf("Starting [%s]...\n", sSDKname);


    //create handle for Ddot on gpu
    cublasHandle_t handle;
    cublasCreate(&handle);

//////////////////////////////////////// Solve Poisson ////////////////////////////////////////
/*------let's let the NVIDIA to take care allocation of matrices using the Unified Memory API----------*/

  //int *fixednodes_d, *activenodes_d, *convnodes_x_d, *convnodes_y_d,*convnodes_z_d; //*ndofMat_d;
  //int *domain_optim_d, *domain_d;
  int cpt;
  bool init=0;
  //int *pos_heat_d;
  //double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double  *Ke_d, *Me_d, *Qe1_d, *He1_d, *He2_d, *He3_d, *He4_d; //z_d
  double *Fe1_d, *Fe2_d, *Fe3_d, *Fe4_d;
  double  *Ke_b_d, *Me_b_d, *Qe1_b_d, *He1_b_d, *He2_b_d, *He3_b_d, *He4_b_d; //z_d
  double *Fe1_b_d, *Fe2_b_d, *Fe3_b_d, *Fe4_b_d;
  double *pAp_d;
  double *rznew_d;
  double *rNorm_d,*fNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  double *fNorm = (double *) calloc(1,sizeof(double));
  double norme;
  char filename[500], u2[500];
//  double *Au;

  //Allocate CPU memory using MallocManaged
  double *Ke = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me1 = (double *) malloc(MNE*MNE * sizeof(double));
  double *He1 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He2 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He3 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He4 = (double *)calloc(MNE*MNE, sizeof(double));
  //double *He5 = (double *)calloc(MNE*MNE, sizeof(double));
  //double *He6 = (double *)calloc(MNE*MNE, sizeof(double));
  double *Fe1 = (double *)calloc(MNE, sizeof(double));
  double *Fe2 = (double *)calloc(MNE, sizeof(double));
  double *Fe3 = (double *)calloc(MNE, sizeof(double));
  double *Fe4 = (double *)calloc(MNE, sizeof(double));
  //double *Fe5 = (double *)calloc(MNE, sizeof(double));
  //double *Fe6 = (double *)calloc(MNE, sizeof(double));
  double *Qe1 = (double *)calloc(MNE, sizeof(double));

  double *Ke_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me1_b = (double *) malloc(MNE*MNE * sizeof(double));
  double *He1_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He2_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He3_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *He4_b = (double *)calloc(MNE*MNE, sizeof(double));
  //double *He5_b = (double *)calloc(MNE*MNE, sizeof(double));
  //double *He6_b = (double *)calloc(MNE*MNE, sizeof(double));
  double *Fe1_b = (double *)calloc(MNE, sizeof(double));
  double *Fe2_b = (double *)calloc(MNE, sizeof(double));
  double *Fe3_b = (double *)calloc(MNE, sizeof(double));
  double *Fe4_b = (double *)calloc(MNE, sizeof(double));
  //double *Fe5_b = (double *)calloc(MNE, sizeof(double));
  //double *Fe6_b = (double *)calloc(MNE, sizeof(double));
  double *Qe1_b = (double *)calloc(MNE, sizeof(double));


  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_x = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_y = (int *)calloc(numNodes, sizeof(int));

  int *pos_heat = (int *)calloc(numElems, sizeof(int));

  int *domain_optim = (int *)calloc(numElems, sizeof(int));
  int *domain_coarse = (int *)calloc(ni*nj1, sizeof(int));
  int *domain_fine = (int *)calloc(numElems, sizeof(int));
  double *kcond = (double *)calloc(numElems, sizeof(double));
  double *rho = (double *)calloc(numElems, sizeof(double));
  double *c = (double *)calloc(numElems, sizeof(double));
  double *hconv1 = (double *)calloc(numElems, sizeof(double));
  double *hconv2 = (double *)calloc(numElems, sizeof(double));
  double *hconv3 = (double *)calloc(numElems, sizeof(double));
  double *hconv4 = (double *)calloc(numElems, sizeof(double));
  //double *hconv5 = (double *)calloc(numElems, sizeof(double));
  //double *hconv6 = (double *)calloc(numElems, sizeof(double));
  //int *ndofMat = (int *) malloc(MNE*numElems * sizeof(int));

  double *U = (double *) calloc(numNodes , sizeof(double));

  // Allocate memory
  cudaMalloc((void **) &Ke_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Me_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He4_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &He5_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &He6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe4_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &Fe5_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &Fe6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Qe1_d, MNE * sizeof(double));

  cudaMalloc((void **) &Ke_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Me_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He1_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He2_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He3_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He4_b_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &He5_b_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &He6_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe1_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe2_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe3_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe4_b_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &Fe5_b_d, MNE*MNE * sizeof(double));
  //cudaMalloc((void **) &Fe6_b_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Qe1_b_d, MNE * sizeof(double));

  //additional variable for reduction operations
  cudaMalloc((void **) &pAp_d, 1 * sizeof(double));
  cudaMalloc((void **) &rznew_d, 1 * sizeof(double));
  cudaMalloc((void **) &rNorm_d, 1 * sizeof(double));
  cudaMalloc((void **) &fNorm_d, 1 * sizeof(double));

  cudaMemcpy(pAp_d, pAp, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rznew_d, rznew, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rNorm_d, rNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(fNorm_d, fNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);

  //Calculation of the element matrix
  //KM_matrix(delta_x,delta_y,delta_z,Ke,Me1,He1,He2,He3,He4,He5,He6,Fe1,Fe2,Fe3,Fe4,Fe5,Fe6,Qe1);
  KM_matrix_2D(delta_x,delta_y,Ke,Me1,He1,He2,He3,He4,Fe1,Fe2,Fe3,Fe4,Qe1);
  KM_matrix_2D(delta_x,delta_y1,Ke_b,Me1_b,He1_b,He2_b,He3_b,He4_b,Fe1_b,Fe2_b,Fe3_b,Fe4_b,Qe1_b);

  //Domain definition
  domain_definition(ni,nj1,domain_coarse);

  domain_update(ni,nj1,nj,base_height,map_elements,domain_coarse,domain_fine,resolution,coarse_layers);


    cudaMemcpy(Ke_d, Ke, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He1_d, He1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He2_d, He2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He3_d, He3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He4_d, He4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(He5_d, He5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(He6_d, He6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe1_d, Fe1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe2_d, Fe2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe3_d, Fe3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe4_d, Fe4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(Fe5_d, Fe5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(Fe6_d, Fe6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Qe1_d, Qe1, MNE * sizeof(double),cudaMemcpyHostToDevice);

    cudaMemcpy(Ke_b_d, Ke_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He1_b_d, He1_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He2_b_d, He2_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He3_b_d, He3_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He4_b_d, He4_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(He5_b_d, He5_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(He6_b_d, He6_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe1_b_d, Fe1_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe2_b_d, Fe2_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe3_b_d, Fe3_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe4_b_d, Fe4_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(Fe5_b_d, Fe5_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    //cudaMemcpy(Fe6_b_d, Fe6_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Qe1_b_d, Qe1_b, MNE * sizeof(double),cudaMemcpyHostToDevice);

  const double TOL = 1.0e-6;

   cpt=0;
   //printf("beginning transient loop \n");
    //Beginning of the transient loop
    int cpt1=0;

    int layer_height1=0;
    layer_height=base_height;

    int trigger=2*resolution;

    while(layer_height1<build_height){

    //  printf("layer_height1=%d,layer_height=%d, old_node=%d, old_node2=%d, old_elements=%d\n",layer_height1,layer_height,old_node,old_node2,old_elements);
    //printf("Avant trigger : U[14]=%f and U[20]=%f\n",U[14],U[20]);

      if(layer_height1==trigger){

        //MAP NODES 2

        map_nodes2 = (int *)calloc(numNodes, sizeof(int)); //free ok


        //map nodes2
        cpt=0;
        for (j=0; j<(base_height+coarse_layers+1); j++) {
            for (i=0; i<(ni+1); i++) {
            i0=I2D(ni2,i,j);
            map_nodes2[cpt]=i0;
            cpt++;
        }
      }
        i0=(ni+1)*(base_height+coarse_layers+1);
       for (j=(base_height+coarse_layers+1); j<base_height+coarse_layers+1+resolution; j++) {
        if(j<base_height+coarse_layers+resolution){
          for (i=0; i<(ni+1); i++) {
            //map_nodes2[cpt]=i0;
            cpt++;
            //i0++;
        }
        }
      if(j==base_height+coarse_layers+resolution){
        for (i=0; i<(ni+1); i++) {
        map_nodes2[cpt]=i0;
        cpt++;
        i0++;
      }
      }
    }

        //i0=(ni+1)*(base_height+coarse_layers+1);
        for (j=base_height+coarse_layers+resolution+1; j<(nj+1); j++) {
          for (i=0; i<(ni+1); i++) {
          map_nodes2[cpt]=i0;
          cpt++;
          i0++;
        }
      }
      //printf("MAP NODE 2 ok \n");
      coarse_layers++;

      layer_height=base_height+layer_height1-(resolution-1)*coarse_layers;

      //printf("layer_height1=%d et layer_height=%d\n",layer_height1,layer_height);
      nj=base_height+build_height-(resolution-1)*coarse_layers;
      //temporary arrays
      numElems=ni*nj;

      double *U_temp = (double *) calloc(i0 , sizeof(double)); //free ok
      int *map_nodes_temp = (int *) calloc(i0 , sizeof(int));
      int *map_nodes_old = (int *) calloc(numNodes , sizeof(int));
      int *map_elements_temp = (int *) calloc(numElems , sizeof(int));

      for (i=0;i<numNodes;i++) {
        map_nodes_old[i]=map_nodes[i];
        //printf("map_nodes_old[%d]=%d\n",i,map_nodes_old[i]);
      }

      //map nodes
      cpt=0;
      for (j=0;j<base_height+coarse_layers;j++){
        for (i=0;i<(ni+1);i++){
          if(map_nodes[i+j*(ni+1)]>0){
        map_nodes_temp[i+j*(ni+1)]=map_nodes[i+j*(ni+1)];
        cpt++;
        //printf("map_nodes1[%d]=%d and cpt=%d\n",i+j*(ni+1),map_nodes_temp[i+j*(ni+1)],cpt);
      }
      }
    }
    for (j=base_height+coarse_layers;j<(nj+1);j++){
      for (i=0;i<(ni+1);i++){
        i01=i+(ni+1)*(j+resolution-1);
        i02=i+j*(ni+1);
        if(map_nodes[i01]>0){
          cpt++;
          map_nodes_temp[i02]=cpt;
        //  printf("old map_nodes[%d]=%d >0 : map_nodes_temp[%d]=%d\n",i01,map_nodes[i01],i0,map_nodes_temp[i0]);

          //  printf("map_nodes1[%d]=%d and cpt=%d\n",i0,map_nodes_temp[i0],cpt);
        }
      }
    }
    map_nodes = (int *)realloc(map_nodes,i0*sizeof(int));


    /*for(i=0;i<numNodes;i++) {
      //map_nodes[i]=map_nodes_temp[i];
      printf("old map_nodes[%d]=%d\n",i,map_nodes2[i]);
    }*/
    for(i=0;i<i0;i++) {
      map_nodes[i]=map_nodes_temp[i];
      //printf("map_nodes[%d]=%d\n",i,map_nodes[i]);
    }

    //for(i=0;i<numElems;i++) printf("old map_elements[%d]=%d\n",i,map_elements[i]);
    //map elements
    cpt=0;
    for (j=0;j<base_height+coarse_layers;j++){
      for (i=0;i<ni;i++){
        if(map_elements[i+ni*j]>0){
      map_elements_temp[i+j*ni]=map_elements[i+j*ni];

      cpt++;
    }
      //printf("map_elements1[%d]=%d\n",i+j*ni,map_elements_temp[i+j*ni]);
    }
  }
  for (j=base_height+coarse_layers;j<nj;j++){
    for (i=0;i<ni;i++){
      if(map_elements[i+ni*(j+resolution-1)]>0){
        cpt++;
        map_elements_temp[i+j*ni]=cpt;

      }
      //printf("map_elements2[%d]=%d\n",i+j*ni,map_elements_temp[i+j*ni]);
    }
  }
  map_elements = (int *)realloc(map_elements,numElems*sizeof(int));

  for(i=0;i<numElems;i++) {
    map_elements[i]=map_elements_temp[i];
    //printf("map_elements[%d]=%d\n",i,map_elements[i]);
    }


  //Recalcul de old node, old node 2, old element
  /*printf("EdofMat_old\n");
  for (j=0;j<old_elements;j++) {
    printf("Element %d : ",j);
    for (i=0;i<MNE;i++){
      printf("%d ",edofMat_all[MNE*j+i]);
    }
    printf("\n");
  }*/

    int node_complet=(ni+1)*(layer_height+1);
    int elem_complet=ni*layer_height; //printf("elem_complet=%d\n",elem_complet);
    int elem_complet2=ni*(layer_height-1);
    int max1=0;
    for(i=0;i<node_complet;i++){
      if(map_nodes[i]>=max1) max1=map_nodes[i];
    }
    old_node=max1+1;
    old_node2=node_complet;
    max1=0;
    for(i=0;i<elem_complet2;i++){
      if(map_elements[i]>=max1) max1=map_elements[i];
    }
    old_elements=max1-ni*base_height+1;

    temp=(unsigned int)4*old_elements;

    //Reconstruction of edofMat_all
    edofMat_all = (int *)realloc(edofMat_all,temp * sizeof(int));

    cpt=0;
    for(i=ni*base_height;i<elem_complet2;i++){
      if(map_elements[i]>0){
        j1=i/ni;
        i1=i%ni;
        id1=(ni+1)*j1+i1;
        node=id1;           edofMat_all[MNE*cpt+0]=map_nodes[node];
        node=id1+1;         edofMat_all[MNE*cpt+1]=map_nodes[node];
        node=id1+1+(ni+1);  edofMat_all[MNE*cpt+2]=map_nodes[node];
        node=id1+(ni+1);    edofMat_all[MNE*cpt+3]=map_nodes[node];
        cpt++;
      }
    }

    /*printf("EdofMat_new\n");
    for (j=0;j<old_elements;j++) {
      printf("Element %d : ",j);
      for (i=0;i<MNE;i++){
        printf("%d ",edofMat_all[MNE*j+i]);
      }
      printf("\n");
    }*/

        //U TRANSFER
        for(i=0;i<numNodes;i++){
          U_temp[0]=U_red[0];
          if(map_nodes_old[i]>0){
          U_temp[map_nodes2[i]]=U_red[map_nodes_old[i]];
        }
        }

        //for (i=0;i<numNodes;i++) printf("map_nodes2[%d]=%d, U_red[%d]=%lg\n",i,map_nodes2[i],i,U_red[i]);
          //for (i=0;i<i0;i++) printf("map_nodes[%d]=%d\n",i,map_nodes[i]);

        numNodes=i0;
        U = (double *)realloc(U, numNodes * sizeof(double));
        for(i=0;i<numNodes;i++) {U[i]=U_temp[i]; }

        fixednodes = (int *)realloc(fixednodes,numNodes*sizeof(int));
        convnodes_x = (int *)realloc(convnodes_x,numNodes*sizeof(int));
        convnodes_y = (int *)realloc(convnodes_y,numNodes*sizeof(int));
        b1 = (double *)realloc(b1,numNodes*sizeof(double));

        pos_heat = (int *)realloc(pos_heat,numElems*sizeof(int));
        domain_optim = (int *)realloc(domain_optim,numElems*sizeof(int));

        domain_fine = (int *)realloc(domain_fine,numElems*sizeof(int));
        kcond = (double *)realloc(kcond,numElems*sizeof(double));
        rho = (double *)realloc(rho,numElems*sizeof(double));
        c = (double *)realloc(c,numElems*sizeof(double));
        hconv1 = (double *)realloc(hconv1,numElems*sizeof(double));
        hconv2 = (double *)realloc(hconv2,numElems*sizeof(double));
        hconv3 = (double *)realloc(hconv3,numElems*sizeof(double));
        hconv4 = (double *)realloc(hconv4,numElems*sizeof(double));

        domain_update(ni,nj1,nj,base_height,map_elements,domain_coarse,domain_fine,resolution,coarse_layers);

        coarse_elements=domain_coarse_elements(ni,nj,base_height,domain_fine, coarse_layers);
        //printf("coarse_elements=%d\n",coarse_elements);

          //for(i=0;i<numElems;i++) {
          //map_elements[i]=map_elements_temp[i];
          //printf("domain_fine[%d]=%d\n",i,domain_fine[i]);
        //}

        trigger=trigger+resolution;
        printf("trigger=%d\n",trigger);
        init=1;
          free(map_nodes2);
          free(U_temp);
          free(map_nodes_temp);
          free(map_nodes_old);
          free(map_elements_temp);
      }

      //for (i=0;i<numNodes;i++){
      //printf("Apres trigger : U[14]=%f et U[20]=%f\n",U[14],U[20]);
      //}

    //for (layer_height=base_height;layer_height<build_height+base_height;layer_height++){


    for (i=0;i<numElems;i++) pos_heat[i]=0;//memset(pos_heat, 0.0, numElems*sizeof(int));
    active_domain (ni,nj,numElems,base_height,layer_height,domain_fine, domain_optim,pos_heat);
    //for (i=0;i<numElems;i++) printf("pos_heat[%d]=%d\n",i,pos_heat[i]);



    /*for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I2D(ni,i,j);
        printf("i=%d, j=%d, domain_optim[%d]=%d\n",i,j,i0,domain_optim[i0]);
      }
    }*/

    //Reinitialization

    for (i=0;i<numElems;i++) hconv1[i]=0;//memset(hconv1, 0.0, numElems*sizeof(double));
    for (i=0;i<numElems;i++) hconv2[i]=0;//memset(hconv2, 0.0, numElems*sizeof(double));
    for (i=0;i<numElems;i++) hconv3[i]=0;//memset(hconv3, 0.0, numElems*sizeof(double));
    for (i=0;i<numElems;i++) hconv4[i]=0;//memset(hconv4, 0.0, numElems*sizeof(double));
    for (i=0;i<numNodes;i++) convnodes_x[i]=0;  //memset(convnodes_x, 0.0, numNodes*sizeof(int));
    for (i=0;i<numNodes;i++) convnodes_y[i]=0; //memset(convnodes_y, 0.0, numNodes*sizeof(int));


    Boundary_Conditions1 (ni,nj,numElems,base_height,domain_optim,convnodes_x,convnodes_y,hconv1,hconv2, hconv3, hconv4);

    for(i=0;i<numNodes;i++) b1[i]=0;//memset(b1, 0.0, numNodes*sizeof(double));
    Boundary_Conditions2 (ni,nj,numElems,domain_optim,convnodes_x,convnodes_y,hconv1,hconv2, hconv3, hconv4,Tinf,b1, Fe1,Fe2,Fe3,Fe4,Fe1_b,Fe2_b,Fe3_b,Fe4_b,base_height);


    //for (i=0;i<numNodes;i++) printf("b1[%d]=%f\n",i,b1[i]);

      /////////////////////////////////////////////////////////////////////
      //Mapping u

      for(i=0;i<numNodes;i++){
        if(init==0){
        U[0]=U_red[0];
        if(map_nodes[i]>0) {
          U[i]=U_red[map_nodes[i]];
        }
      }
      }

      new_elements=new_elements_counter(ni,nj,layer_height,base_height,domain_optim,init);

      new_ndof=MNE*new_elements;

      ele_id = (int *)calloc(new_elements, sizeof(int));

      node_id = (int *)calloc(new_ndof, sizeof(int));
      node_id2 = (int *)calloc(new_ndof, sizeof(int));
      ele_id_node = (int *)calloc(new_ndof, sizeof(int));
      pos_id_node = (int *)calloc(new_ndof, sizeof(int));
      edofMat_new = (int *)calloc(new_ndof, sizeof(int));

      //ele_id calculation

      ele_id_calculation(ni,nj,base_height,layer_height,domain_optim,ele_id,init);

        init=0;

      //Update map_elements

      numElems2=map_elements_calculation(ni, base_height, old_elements, new_elements, ele_id,map_elements);
      numNodes2=map_nodes_calculation(ni,new_elements,new_ndof,old_node,old_node2,node_id,ele_id,ele_id_node,pos_id_node,map_nodes,node_id2);

            //Creation edofMat new
          for (i=0;i<new_elements;i++){
            for (j=0;j<MNE;j++){
            edofMat_new[MNE*i+pos_id_node[MNE*i+j]-1]=node_id2[MNE*i+j];
                  }
                }


          //printf("numElems2=%d and numNodes2=%d\n",numElems2,numNodes2);
          //Auxilliary fields

          hconv1_red = (double *)realloc(hconv1_red, numElems2 * sizeof(double));
          hconv2_red = (double *)realloc(hconv2_red, numElems2 * sizeof(double));
          hconv3_red = (double *)realloc(hconv3_red, numElems2 * sizeof(double));
          hconv4_red = (double *)realloc(hconv4_red, numElems2 * sizeof(double));



          U_red = (double *)realloc(U_red,numNodes2*sizeof(double));
          for (i=old_node;i<numNodes2;i++) {U_red[i]=0; }

          edofMat_all = (int *)realloc(edofMat_all,MNE*(old_elements+new_elements)*sizeof(int));
          for (i=0;i<new_ndof;i++) edofMat_all[MNE*old_elements+i]=edofMat_new[i];

          b1_red = (double *)realloc(b1_red, numNodes2 * sizeof(double));
          f_red = (double *)realloc(f_red, numNodes2 * sizeof(double));
          fixednodes_red = (int *)realloc(fixednodes_red, numNodes2 * sizeof(int));
          pos_heat_red = (int *)realloc(pos_heat_red, numElems2 * sizeof(int));

          hconv1_red[0]=hconv1[0];
          hconv2_red[0]=hconv2[0];
          hconv3_red[0]=hconv3[0];
          hconv4_red[0]=hconv4[0];
          pos_heat_red[0]=pos_heat[0];

          for(i=0;i<numElems;i++){
            if(map_elements[i]>0) {
              hconv1_red[map_elements[i]]=hconv1[i];
              hconv2_red[map_elements[i]]=hconv2[i];
              hconv3_red[map_elements[i]]=hconv3[i];
              hconv4_red[map_elements[i]]=hconv4[i];
              pos_heat_red[map_elements[i]]=pos_heat[i];
            }
          }

          for(i=0;i<numNodes;i++){
              b1_red[0]=b1[0];
              U_red[0]=U[0];
              fixednodes_red[0]=fixednodes[0];
            if(map_nodes[i]>0) {
              U_red[map_nodes[i]]=U[i];
              b1_red[map_nodes[i]]=b1[i];
              fixednodes_red[map_nodes[i]]=fixednodes[i];
            }
          }

          /*if(trigger==1000){
            for(i=0;i<numNodes;i++) printf("U[%d]=%lg, b1[%d]=%lg\n",i,U[i],i,b1[i]);
            for(i=0;i<numNodes2;i++) printf("U_red[%d]=%lg, b1_red[%d]=%lg\n",i,U_red[i],i,b1_red[i]);
            for(i=0;i<numElems;i++) printf("pos_heat[%d]=%d\n", i,pos_heat[i]);
            for(i=0;i<numElems2;i++) printf("pos_heat_red[%d]=%d\n", i,pos_heat_red[i]);

          }*/

        //cudaMalloc
        checkCudaErrors(cudaMalloc((void **) &hconv1_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv2_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv3_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &hconv4_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &kcond_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &rho_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &c_red_d, numElems2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &U_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &f_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &b_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &b1_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &M_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &r_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &prod_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &p_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &z_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &Ap_red_d, numNodes2 * sizeof(double)));
        checkCudaErrors(cudaMalloc((void **) &fixednodes_red_d, numNodes2 * sizeof(int)));
        checkCudaErrors(cudaMalloc((void **) &edofMat_all_d, MNE*(old_elements+new_elements) * sizeof(int)));
        //checkCudaErrors(cudaMalloc((void **) &edofMat_all2_d, 4*numElems2 * sizeof(int)));
        checkCudaErrors(cudaMalloc((void **) &pos_heat_red_d, numElems2 * sizeof(int)));

        //Transfer to GPU
        checkCudaErrors(cudaMemcpy(hconv1_red_d, hconv1_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv2_red_d, hconv2_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv3_red_d, hconv3_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(hconv4_red_d, hconv4_red, numElems2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(U_red_d, U_red, numNodes2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(b1_red_d, b1_red, numNodes2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(f_red_d, f_red, numNodes2 * sizeof(double),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(fixednodes_red_d, fixednodes_red, numNodes2 * sizeof(int),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(pos_heat_red_d, pos_heat_red, numElems2 * sizeof(int),cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(edofMat_all_d, edofMat_all, MNE*(old_elements+new_elements) * sizeof(int),cudaMemcpyHostToDevice));
        //checkCudaErrors(cudaMemcpy(edofMat_all2_d, edofMat_all2, 4*numElems2* sizeof(int),cudaMemcpyHostToDevice));



        if(layer_height>base_height){
          int node_complet=(ni+1)*(layer_height+1);
          int elem_complet=ni*layer_height; //printf("elem_complet=%d\n",elem_complet);
          int max1=0;
          for(i=0;i<node_complet;i++){
            if(map_nodes[i]>=max1) max1=map_nodes[i];
          }
          old_node=max1+1;
          old_node2=node_complet;
          max1=0;
          for(i=0;i<elem_complet;i++){
            if(map_elements[i]>=max1) max1=map_elements[i];
          }
          old_elements=max1-ni*base_height+1;

        }

        NBLOCKS   = (numElems2+BLOCKSIZE-1) / BLOCKSIZE; //round up if n is not a multiple of blocksize

      //  printf("Number of Blocks: %d\n",NBLOCKS);
        //dim3 dimBlock(BLOCKSIZE, BLOCKSIZE);
        //printf("numNodes: %d\n", numNodes2);
        //printf("numElems: %d\n", numElems2);
        NBLOCKS2 = (numNodes2+BLOCKSIZE-1) / BLOCKSIZE; //round up if n is not a multiple of blocksize
        //printf("nblock2=%d and blocksize=%d\n",NBLOCKS2,BLOCKSIZE);

      /////////////////////////////////////////////////////////////////////

      for (step=0;step<21;step++){

        if(step<1) {
          dt=0.1; heating=1;
        }
      if(step>=1 && step<11) {

          dt=0.11; heating=0;
        }
      if(step>=11 && step<21) {

            dt=0.8; heating=0;
          }

        /*  if(step<9) {
              dt=0.1; heating=1;
            }
            else{
              dt=0.1; heating=0;
            }*/


        for(size_t n=0; n<MNE*MNE; n++) {
          Me[n]=Me1[n]/dt;
            Me_b[n]=Me1_b[n]/dt;
        }

       checkCudaErrors(cudaMemcpy(Me_d, Me, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));
       checkCudaErrors(cudaMemcpy(Me_b_d, Me_b, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));

  //Uold _matvec
  //MatVec_Uold(ni,nj, Me, U, f, numElems, numNodes);
  Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);
  //getchar();
  checkCudaErrors(cudaMemset(f_red_d, 0.0, numNodes2*sizeof(double)));
  //Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,numElems2,edofMat_all2_d,base_height);
  GPUMatVec_Uold<<<NBLOCKS,BLOCKSIZE>>>(ni,nj, rho_red_d,c_red_d, Me_d, Me_b_d,U_red_d, f_red_d, numElems2,edofMat_all_d,base_height,coarse_elements);

  //cudaMemset(pos_heat_d, 0.0, numElems*sizeof(double));
  //heatsource_position<<<NBLOCKS,BLOCKSIZE>>>(ni,nj, domain_optim_d,base_height,layer_height,pos_heat_d,activenodes_d,numElems);


  checkCudaErrors(cudaMemset(b_red_d, 0.0, numNodes2*sizeof(double)));
  heatsource_term<<<NBLOCKS,BLOCKSIZE>>>(ni,nj,pos_heat_red_d,b_red_d,numElems2,heating,Qvol,Qe1_d,edofMat_all_d,base_height);

  force_transient<<<NBLOCKS2,BLOCKSIZE>>>(f_red_d, b_red_d, b1_red_d,numNodes2);

  //checkCudaErrors(cudaMemcpy(f_red, f_red_d, numNodes2 * sizeof(double),cudaMemcpyDeviceToHost));

  //for (i=0;i<numNodes2;i++) printf("f_red[%d]=%lg\n",i,f_red[i]);

  //define misc vars
  //double rNorm = 0.0;
  //double bNorm = 0.0;
  double rzold = 0.0;
  //double rznew = 0.0;
  double alpha = 0.0;
  double beta  = 0.0;
  //double malpha = -alpha;


  //Initialize MatVec on the CPU
  //MatVec(kcond,rhoc, hconv1,hconv2, ni,nj, Ke, Me, He1, He2, U, r, numElems, numNodes);
  //checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes2*sizeof(double)));
  checkCudaErrors(cudaMemset(prod_red_d, 0.0, numNodes2*sizeof(double)));
  GPUMatVec<<<NBLOCKS,BLOCKSIZE>>>(kcond_red_d, rho_red_d,c_red_d, hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d,U_red_d, prod_red_d, numElems2,edofMat_all_d,base_height,coarse_elements);

  //preconditioner
  //Precond(kcond,rhoc,hconv1,hconv2,ni,nj,Ke, He1, He2, M_pre, numElems, numNodes);
    checkCudaErrors(cudaMemset(M_red_d, 0.0, numNodes2*sizeof(double)));
  GPUMatVec_Precond<<<NBLOCKS,BLOCKSIZE>>>(kcond_red_d, rho_red_d,c_red_d, hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d,M_red_d, numElems2,edofMat_all_d,base_height,coarse_elements);

    checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes2*sizeof(double)));
  rzp_calculation<<<NBLOCKS2,BLOCKSIZE>>>(f_red_d, M_red_d, r_red_d,prod_red_d, z_red_d, p_red_d, numNodes2, fixednodes_red_d);

  //Reset pAp value
  cudaMemset(pAp_d, 0.0, 1*sizeof(double));
  //Apply Ddot
  //cublasDdot(handle, numNodes2, p_red_d, 1, r_red_d, 1, pAp_d);
  Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_red_d, r_red_d,numNodes2,fixednodes_red_d,pAp_d);
  cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);


    // control phi
  rzold = pAp[0];

  //printf("rzold=%lg\n",rzold);

  const int Max_iters = 5000;
  //double pAp = 0.0;
  //int node;


//  r_usage.ru_maxrss = 0.0; //set memory to zero
  //t = get_time(); // get time stamp


    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

      Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_red_d,ni,nj,kcond_red_d,rho_red_d,c_red_d,edofMat_all_d,numElems2,base_height);

    checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes2*sizeof(double)));

    //Apply Matrix-Free MatVec kernel [Loop over elements]
    GPUMatVec<<<NBLOCKS,BLOCKSIZE>>>(kcond_red_d, rho_red_d, c_red_d,  hconv1_red_d, hconv2_red_d, hconv3_red_d, hconv4_red_d,
       ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d,  Me_b_d,Ke_b_d, He1_b_d, He2_b_d, He3_b_d, He4_b_d, p_red_d, Ap_red_d,numElems2,edofMat_all_d,base_height,coarse_elements);

    //Reset pAp value
    checkCudaErrors(cudaMemset(pAp_d, 0.0, 1*sizeof(double)));
    //Apply Ddot
    //checkCudaErrors(cublasDdot(handle, numNodes2, p_red_d, 1, Ap_red_d, 1, pAp_d));
    Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_red_d, Ap_red_d,numNodes2,fixednodes_red_d,pAp_d);
    checkCudaErrors(cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost));
    //printf("pAp_d[0]=%f\n",pAp_d[0]);

    // control alpha
    alpha = rzold/pAp[0];
    //printf("rzold = %1.12f pAp = %1.12f   alpha = %1.12f\n",rzold,pAp_d[0], alpha);

    Update_UR<<<NBLOCKS2,BLOCKSIZE>>>(U_red_d, r_red_d, p_red_d, Ap_red_d,alpha, numNodes2,fixednodes_red_d);

      //Reset Norm
    //checkCudaErrors(cudaMemset(rNorm_d, 0.0, 1*sizeof(double)));
    rNorm[0]=0;
    //cudaMemset(fNorm_d, 0.0, 1*sizeof(double));
    //Apply Dnorm
    checkCudaErrors(cublasDnrm2(handle, numNodes2, r_red_d, 1, rNorm));
    //Norm<<<NBLOCKS2,BLOCKSIZE>>>(r_d, numNodes,fixednodes_d,activenodes_d,rNorm_d);
    //Norm<<<NBLOCKS2,BLOCKSIZE>>>(f_d, numNodes,fixednodes_d,activenodes_d,fNorm_d);
    //checkCudaErrors(cudaMemcpy(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost));
    //cudaMemcpy(fNorm, fNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);
    norme=rNorm[0];
    // converge if reached Tolerance 1e-6
    if(norme <= TOL) break;

    //Calculation for preconditioner
    zCalculation<<<NBLOCKS2,BLOCKSIZE>>>(M_red_d, r_red_d, z_red_d, numNodes2,fixednodes_red_d);

    //Update rznew
      //cudaMemset(rznew_d, 0.0, 1*sizeof(double));
        rznew[0]=0;
    cublasDdot(handle, numNodes2, z_red_d, 1, r_red_d, 1, rznew);
    //Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(r_d, z_d,numNodes,fixednodes_d,activenodes_d,rznew_d);
    //cudaMemcpy(rznew, rznew_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);

    //Check residual norm and my alpha
  //  if(trigger==16) printf("Step=%d , iter = %i, rNorm = %lg \n", step, iter, norme);

    // control beta
    beta = rznew[0]/rzold;

    //update vector [p] for next iters...
    UpdateVec<<<NBLOCKS2,BLOCKSIZE>>>(p_red_d, z_red_d, beta, numNodes2,fixednodes_red_d);


    //update rzold for next iters...
    rzold = rznew[0];

//    if (iter==0) break;

}


checkCudaErrors(cudaMemcpy(U_red, U_red_d, numNodes2 * sizeof(double), cudaMemcpyDeviceToHost));

//for(i=0;i<numNodes2;i++) printf("Fin Boucle : U_red[%d]=%f \n",i,U_red[i]);

  for(i=0;i<numNodes;i++){
    U[0]=U_red[0];
    if(map_nodes[i]>0) {
      U[i]=U_red[map_nodes[i]];
    }
  }



  strcpy(filename,"temp2d_map3_");	sprintf(u2,"%d",cpt1);	strcat(filename,u2);
  solution=fopen(filename,"w"); memset(filename, 0, sizeof(filename));

  double max=-1;
  double min=1e6;
  double ly;

  for (j=0; j<(nj+1); j++) {
    for (i1=0; i1<(ni+1); i1++) {
        i0=I2D(ni2,i1,j);
        if(j<=base_height+coarse_layers){
          ly=j*delta_y1;
        }
        else{
          ly=(base_height+coarse_layers)*delta_y1+(j-base_height-coarse_layers)*delta_y;
        }

        fprintf(solution,"%lg %lg %lg \n",i1*delta_x,ly,U[i0]);
        //if(layer_height1==7 && step==20) printf("U[%d]=%lg\n",i0,U[i0]);
        //if(layer_height1==8 && step==0) printf("U[%d]=%lg\n",i0,U[i0]);
        if(U[i0]>max) max=U[i0];
        if(U[i0]<min) min=U[i0];
      }
    }
    fclose(solution);

  printf("Layer %d/%d, Step %d/%d  it_PCG=%d, residual=%lg\n",layer_height1,build_height-1,step,total_steps,iter,norme);


cpt1=cpt1+1;
}
/*for (i=0;i<numNodes;i++){
printf("Fin layer : U[%d]=%f \n",i,U[i]);
}*/

checkCudaErrors(cudaFree(hconv1_red_d));
checkCudaErrors(cudaFree(hconv2_red_d));
checkCudaErrors(cudaFree(hconv3_red_d));
checkCudaErrors(cudaFree(hconv4_red_d));
checkCudaErrors(cudaFree(kcond_red_d));
checkCudaErrors(cudaFree(rho_red_d));
checkCudaErrors(cudaFree(c_red_d));
checkCudaErrors(cudaFree(U_red_d));
checkCudaErrors(cudaFree(f_red_d));
checkCudaErrors(cudaFree(b_red_d));
checkCudaErrors(cudaFree(b1_red_d));
checkCudaErrors(cudaFree(M_red_d));
checkCudaErrors(cudaFree(r_red_d));
checkCudaErrors(cudaFree(prod_red_d));
checkCudaErrors(cudaFree(p_red_d));
checkCudaErrors(cudaFree(z_red_d));
checkCudaErrors(cudaFree(Ap_red_d));
checkCudaErrors(cudaFree(fixednodes_red_d));
checkCudaErrors(cudaFree(edofMat_all_d));
checkCudaErrors(cudaFree(pos_heat_red_d));



layer_height1++;
layer_height++;


free(ele_id);
free(node_id);
free(node_id2);
free(ele_id_node);
free(pos_id_node);
free(edofMat_new);

}
    //kill handle
    cublasDestroy(handle);


  // StdOut prints
  printf("\n");
  printf("*** PCG Solver Converged after %d total iterations ***\n", iter+1);
  printf("\n");
  printf("Residual Reduction: [%1.4e] <== Tolerance [%1.4e]\n",norme,TOL);
  printf("\n");
  printf("Solver Statistics [Wall Time, Memory Usage]\n");
  // Compute Elapsed Time for accumulative iters
  /*printf("-------------------------------------------\n");
  printf("|   Elapsed Time: [%g sec]                 \n",get_time()-t);
  printf("|   Elapsed Time: [%g sec/iter]            \n",(get_time()-t)/(iter+1));
  printf("-------------------------------------------\n");
  printf("|  Memory Usage: [%lf MB]                  \n", r_usage.ru_maxrss*0.001);
  printf("-------------------------------------------\n");*/

  //cudaMemcpy(kcond,kcond_d, numElems * sizeof(double),cudaMemcpyDeviceToHost);
  //cudaMemcpy(activenodes, activenodes_d, numNodes * sizeof(int), cudaMemcpyDeviceToHost);

    //cudaMemcpy(M, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  //for (int n=0; n<numNodes; n++){
    //printf("M[%d]=%f\n",n,M[n]);
    // if (U[n] < 0) U[n] = 0.0;
  //}
  // Post-Processing


  //free gpu memory
  /*cudaFree(U_d);   cudaFree(f_d);   cudaFree(b_d);   cudaFree(b1_d);  cudaFree(M_d);   cudaFree(prod_d);  cudaFree(fixednodes_d);
  cudaFree(kcond_d);   cudaFree(rho_d); cudaFree(c_d); cudaFree(hconv1_d); cudaFree(hconv2_d); cudaFree(hconv3_d); cudaFree(hconv4_d);
  cudaFree(r_d);
  cudaFree(p_d);
  cudaFree(Ap_d);
  cudaFree(z_d);*/
  checkCudaErrors(cudaFree(Ke_d));   checkCudaErrors(cudaFree(Me_d)); checkCudaErrors(cudaFree(Qe1_d));
  checkCudaErrors(cudaFree(He1_d)); checkCudaErrors(cudaFree(He2_d)); checkCudaErrors(cudaFree(He3_d)); checkCudaErrors(cudaFree(He4_d));
  checkCudaErrors(cudaFree(Fe1_d)); checkCudaErrors(cudaFree(Fe2_d)); checkCudaErrors(cudaFree(Fe3_d)); checkCudaErrors(cudaFree(Fe4_d));
  //cudaFree(pos_heat_d); cudaFree(domain_optim_d); cudaFree(activenodes_d); cudaFree(convnodes_x_d); cudaFree(convnodes_y_d);
  //cudaFree(face_d);
  //cudaFree(edofMat_d);
  checkCudaErrors(cudaFree(pAp_d));
  checkCudaErrors(cudaFree(rznew_d));
  checkCudaErrors(cudaFree(rNorm_d));
  checkCudaErrors(cudaFree(fNorm_d));

  free(c);
  free(rho);
  free(kcond);
  free(hconv1);
  free(hconv2);
  free(hconv3);
  free(hconv4);
  free(domain_fine);
  free(domain_optim);
  free(domain_coarse);
  free(pos_heat);
  free(b1);
  free(convnodes_x);
  free(convnodes_y);
  free(fixednodes);
  free(U);
  free(map_elements);
  free(map_nodes);

  free(hconv1_red);
  free(hconv2_red);
  free(hconv3_red);
  free(hconv4_red);
  free(edofMat_all);
  free(b1_red);
  free(f_red);
  free(U_red);
  free(fixednodes_red);
  free(pos_heat_red);


  free(pAp);
  free(rznew);
  free(rNorm);
  free(fNorm);
  free(Ke);
  free(Me);
  free(Me1);
  free(He1);
  free(He2);
  free(He3);
  free(He4);
  free(Fe1);
  free(Fe2);
  free(Fe3);
  free(Fe4);
  free(Qe1);
  free(Ke_b);
  free(Me_b);
  free(Me1_b);
  free(He1_b);
  free(He2_b);
  free(He3_b);
  free(He4_b);
  free(Fe1_b);
  free(Fe2_b);
  free(Fe3_b);
  free(Fe4_b);
  free(Qe1_b);



  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
