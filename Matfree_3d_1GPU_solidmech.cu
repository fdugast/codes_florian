#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <omp.h>

#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define NDim 3
#define MNE 24

#define BLOCKSIZE1 32
#define BLOCKSIZE2 32

//#define MAX(x, y) (((x) > (y)) ? (x) : (y))

#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)
#define I2D(ni,i,j) (((ni)*(j)) + i)

const char *sSDKname = "conjugateGradientMultiDeviceCG";

int *array;

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

void BMat_matrix(double delta_x, double delta_y,double delta_z,double B[6][24][8]){

	double DN[3][8];
	double gxyz[3][3];

	int i,j,k,i3,cpt;
	double a,b,c;
	double x,y,z;

	gxyz[0][0]=0;
	gxyz[0][1]=-0.577350269189626;
	gxyz[0][2]=-0.774596669241483;
	gxyz[1][0]=0;
	gxyz[1][1]=0.577350269189626;
	gxyz[1][2]=0;
	gxyz[2][0]=0;
	gxyz[2][1]=0;
	gxyz[2][2]=0.774596669241483;



a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;

				cpt=0;
				for (i=0;i<2;i++){
					for (j=0;j<2;j++){
						for (k=0;k<2;k++){

							x=gxyz[i][1];
							y=gxyz[j][1];
							z=gxyz[k][1];


							DN[0][0]=-(1-y)*(1-z)*(1/a)/(2*2*2); DN[1][0]=-(1-x)*(1-z)*(1/b)/(2*2*2);  DN[2][0]=-(1-x)*(1-y)*(1/c)/(2*2*2);
							DN[0][1]=(1-y)*(1-z)*(1/a)/(2*2*2);  DN[1][1]=-(1+x)*(1-z)*(1/b)/(2*2*2);  DN[2][1]=-(1+x)*(1-y)*(1/c)/(2*2*2);
							DN[0][2]=(1+y)*(1-z)*(1/a)/(2*2*2);  DN[1][2]=(1+x)*(1-z)*(1/b)/(2*2*2);   DN[2][2]=-(1+x)*(1+y)*(1/c)/(2*2*2);
							DN[0][3]=-(1+y)*(1-z)*(1/a)/(2*2*2); DN[1][3]=(1-x)*(1-z)*(1/b)/(2*2*2);   DN[2][3]=-(1-x)*(1+y)*(1/c)/(2*2*2);
							DN[0][4]=-(1-y)*(1+z)*(1/a)/(2*2*2); DN[1][4]=-(1-x)*(1+z)*(1/b)/(2*2*2);  DN[2][4]=(1-x)*(1-y)*(1/c)/(2*2*2);
							DN[0][5]=(1-y)*(1+z)*(1/a)/(2*2*2);  DN[1][5]=-(1+x)*(1+z)*(1/b)/(2*2*2);  DN[2][5]=(1+x)*(1-y)*(1/c)/(2*2*2);
							DN[0][6]=(1+y)*(1+z)*(1/a)/(2*2*2);  DN[1][6]=(1+x)*(1+z)*(1/b)/(2*2*2);   DN[2][6]=(1+x)*(1+y)*(1/c)/(2*2*2);
							DN[0][7]=-(1+y)*(1+z)*(1/a)/(2*2*2); DN[1][7]=(1-x)*(1+z)*(1/b)/(2*2*2);   DN[2][7]=(1-x)*(1+y)*(1/c)/(2*2*2);



							for(i3=0;i3<8;i3++){

										B[0][0+3*i3][cpt]=DN[0][i3];  B[0][1+3*i3][cpt]=0; 				 B[0][2+3*i3][cpt]=0;
										B[1][0+3*i3][cpt]=0; 			  	B[1][1+3*i3][cpt]=DN[1][i3]; B[1][2+3*i3][cpt]=0;
										B[2][0+3*i3][cpt]=0; 				  B[2][1+3*i3][cpt]=0; 				 B[2][2+3*i3][cpt]=DN[2][i3];
										B[3][0+3*i3][cpt]=DN[1][i3];	B[3][1+3*i3][cpt]=DN[0][i3]; B[3][2+3*i3][cpt]=0;
										B[4][0+3*i3][cpt]=0;					B[4][1+3*i3][cpt]=DN[2][i3]; B[4][2+3*i3][cpt]=DN[1][i3];
										B[5][0+3*i3][cpt]=DN[2][i3];	B[5][1+3*i3][cpt]=0; 				 B[5][2+3*i3][cpt]=DN[0][i3];
								}
								cpt++;
							}
						}
					}


}

void KM_matrix(double delta_x, double delta_y,double delta_z, double *KE, double E, double nu, double *C, double Bmat[6][24][8]){

	double wg[3][3],wxyz;
	int i,j,k,i1,j1,k3,cpt,l;
	double a,b,c;
	double sommeK,somme_k,somme_l;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;
				//printf("boucle\n");


	for (i1=0;i1<24;i1++){
			for (j1=0;j1<24;j1++){

sommeK=0;
cpt=0;
for (i=0;i<2;i++){
	for (j=0;j<2;j++){
		for (k=0;k<2;k++){

			//x=a1*gxyz[i][1];
			//y=b1*gxyz[j][1];
			//z=c1*gxyz[k][1];
			wxyz=wg[i][1]*wg[j][1]*wg[k][1];

										somme_k=0;
										for(k3=0;k3<6;k3++){
											somme_l=0;
											for(l=0;l<6;l++){
												somme_l=somme_l+Bmat[l][i1][cpt]*C[l+6*k3];
											}
											somme_k=somme_k+somme_l*Bmat[k3][j1][cpt];
										}


								sommeK=sommeK+somme_k*wxyz*a*b*c;


								//printf("sommeK=%f\n",sommeK);
								cpt++;
							}
						}
					}
					KE[i1+24*j1]=sommeK;
				//	printf("%.2lg ",KE[i1+24*j1]);
}

//printf("\n");

}

//printf("j1=%d\n",j1);
}

void Be_matrix(double delta_x, double delta_y,double delta_z, double *BE){

	double DN[3][8];
	double gxyz[3][3];

	double wg[3][3],wxyz;
	int i,j,k,i1,j1,i3;
	double a,b,c,a1,b1,c1;
	double x,y,z,sommeK;

	//gxyz[0][0]=0;
	//gxyz[0][1]=-0.577350269189626;
	gxyz[0][2]=-0.774596669241483;
	//gxyz[1][0]=0;
	//gxyz[1][1]=0.577350269189626;
	gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
	gxyz[2][2]=0.774596669241483;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;

a=0.5;
b=0.5;
c=0.5;
				//printf("boucle\n");
				a1=a*delta_x;
				b1=b*delta_y;
				c1=c*delta_z;

		for (j1=0;j1<24;j1++){
	for (i1=0;i1<6;i1++){


sommeK=0;

					for (i=0;i<2;i++){
						for (j=0;j<2;j++){
							for (k=0;k<2;k++){

								x=a1*gxyz[i][1];
								y=b1*gxyz[j][1];
								z=c1*gxyz[k][1];
								wxyz=wg[i][1]*wg[j][1]*wg[k][1];

								DN[0][0]=-(b1-y)*(c1-z)/(2*a1*2*b1*2*c1); DN[1][0]=-(a1-x)*(c1-z)/(2*a1*2*b1*2*c1);  DN[2][0]=-(a1-x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][1]=(b1-y)*(c1-z)/(2*a1*2*b1*2*c1);  DN[1][1]=-(a1+x)*(c1-z)/(2*a1*2*b1*2*c1);  DN[2][1]=-(a1+x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][2]=(b1+y)*(c1-z)/(2*a1*2*b1*2*c1);  DN[1][2]=(a1+x)*(c1-z)/(2*a1*2*b1*2*c1);   DN[2][2]=-(a1+x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][3]=-(b1+y)*(c1-z)/(2*a1*2*b1*2*c1); DN[1][3]=(a1-x)*(c1-z)/(2*a1*2*b1*2*c1);   DN[2][3]=-(a1-x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][4]=-(b1-y)*(c1+z)/(2*a1*2*b1*2*c1); DN[1][4]=-(a1-x)*(c1+z)/(2*a1*2*b1*2*c1);   DN[2][4]=(a1-x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][5]=(b1-y)*(c1+z)/(2*a1*2*b1*2*c1);  DN[1][5]=-(a1+x)*(c1+z)/(2*a1*2*b1*2*c1);   DN[2][5]=(a1+x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][6]=(b1+y)*(c1+z)/(2*a1*2*b1*2*c1);  DN[1][6]=(a1+x)*(c1+z)/(2*a1*2*b1*2*c1);  DN[2][6]=(a1+x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][7]=-(b1+y)*(c1+z)/(2*a1*2*b1*2*c1); DN[1][7]=(a1-x)*(c1+z)/(2*a1*2*b1*2*c1);  DN[2][7]=(a1-x)*(b1+y)/(2*a1*2*b1*2*c1);

								double B[6][24]={0};

								for(i3=0;i3<8;i3++){

											B[0][0+3*i3]=DN[0][i3]; B[0][1+3*i3]=0; 				B[0][2+3*i3]=0;
											B[1][0+3*i3]=0; 				B[1][1+3*i3]=DN[1][i3]; B[1][2+3*i3]=0;
											B[2][0+3*i3]=0; 				B[2][1+3*i3]=0; 				B[2][2+3*i3]=DN[2][i3];
											B[3][0+3*i3]=DN[1][i3];	B[3][1+3*i3]=DN[0][i3]; B[3][2+3*i3]=0;
											B[4][0+3*i3]=0;					B[4][1+3*i3]=DN[2][i3]; B[4][2+3*i3]=DN[1][i3];
											B[5][0+3*i3]=DN[2][i3];	B[5][1+3*i3]=0; 				B[5][2+3*i3]=DN[0][i3];

											/*Bmat[0][0+3*i3][cpt]=DN[0][i3]; 	Bmat[0][1+3*i3][cpt]=0; 				 B[0][2+3*i3][cpt]=0;
											Bmat[1][0+3*i3][cpt]=0; 					Bmat[1][1+3*i3][cpt]=DN[1][i3];  B[1][2+3*i3][cpt]=0;
											Bmat[2][0+3*i3][cpt]=0; 				  Bmat[2][1+3*i3][cpt]=0; 				 B[2][2+3*i3][cpt]=DN[2][i3];
											Bmat[3][0+3*i3][cpt]=DN[1][i3];		Bmat[3][1+3*i3][cpt]=DN[0][i3];  B[3][2+3*i3][cpt]=0;
											Bmat[4][0+3*i3][cpt]=0;						Bmat[4][1+3*i3][cpt]=DN[2][i3];  B[4][2+3*i3][cpt]=DN[1][i3];
											Bmat[5][0+3*i3][cpt]=DN[2][i3];		Bmat[5][1+3*i3][cpt]=0; 			   B[5][2+3*i3][cpt]=DN[0][i3];*/
									}


										sommeK=sommeK+B[i1][j1]*wxyz*a1*b1*c1;
										//	if(i1==0 && j1==0) printf("sommeK=%f\n",sommeK);
										//cpt++;
							}
						}
					}

					BE[i1+6*j1]=sommeK;
						//printf("%lg ",BE[i1+6*j1]);

}

//printf("\n");

}


}

void BE_matrix2(double delta_x, double delta_y,double delta_z,
	double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8, double E, double nu){

	double DN[3][8];
	int i1,j1,i3;
	double a,b,c;
	double x,y,z;
	int cpt;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;

x=-1;

cpt=1;

	while (x<2){
		y=-1;
				while (y<2){
						z=-1;
					while (z<2){

	for (i1=0;i1<24;i1++){
			for (j1=0;j1<6;j1++){

								DN[0][0]=-(1-y)*(1-z)*(1/a)/(2*2*2); DN[1][0]=-(1-x)*(1-z)*(1/b)/(2*2*2);  DN[2][0]=-(1-x)*(1-y)*(1/c)/(2*2*2);
								DN[0][1]=(1-y)*(1-z)*(1/a)/(2*2*2);  DN[1][1]=-(1+x)*(1-z)*(1/b)/(2*2*2);  DN[2][1]=-(1+x)*(1-y)*(1/c)/(2*2*2);
								DN[0][2]=(1+y)*(1-z)*(1/a)/(2*2*2);  DN[1][2]=(1+x)*(1-z)*(1/b)/(2*2*2);   DN[2][2]=-(1+x)*(1+y)*(1/c)/(2*2*2);
								DN[0][3]=-(1+y)*(1-z)*(1/a)/(2*2*2); DN[1][3]=(1-x)*(1-z)*(1/b)/(2*2*2);   DN[2][3]=-(1-x)*(1+y)*(1/c)/(2*2*2);
								DN[0][4]=-(1-y)*(1+z)*(1/a)/(2*2*2); DN[1][4]=-(1-x)*(1+z)*(1/b)/(2*2*2);  DN[2][4]=(1-x)*(1-y)*(1/c)/(2*2*2);
								DN[0][5]=(1-y)*(1+z)*(1/a)/(2*2*2);  DN[1][5]=-(1+x)*(1+z)*(1/b)/(2*2*2);  DN[2][5]=(1+x)*(1-y)*(1/c)/(2*2*2);
								DN[0][6]=(1+y)*(1+z)*(1/a)/(2*2*2);  DN[1][6]=(1+x)*(1+z)*(1/b)/(2*2*2);   DN[2][6]=(1+x)*(1+y)*(1/c)/(2*2*2);
								DN[0][7]=-(1+y)*(1+z)*(1/a)/(2*2*2); DN[1][7]=(1-x)*(1+z)*(1/b)/(2*2*2);   DN[2][7]=(1-x)*(1+y)*(1/c)/(2*2*2);


								double B[6][24]={0};

								for(i3=0;i3<8;i3++){

											B[0][0+3*i3]=DN[0][i3]; B[0][1+3*i3]=0; 				B[0][2+3*i3]=0;
											B[1][0+3*i3]=0; 				B[1][1+3*i3]=DN[1][i3]; B[1][2+3*i3]=0;
											B[2][0+3*i3]=0; 				B[2][1+3*i3]=0; 				B[2][2+3*i3]=DN[2][i3];
											B[3][0+3*i3]=DN[1][i3];	B[3][1+3*i3]=DN[0][i3]; B[3][2+3*i3]=0;
											B[4][0+3*i3]=0;					B[4][1+3*i3]=DN[2][i3]; B[4][2+3*i3]=DN[1][i3];
											B[5][0+3*i3]=DN[2][i3];	B[5][1+3*i3]=0; 				B[5][2+3*i3]=DN[0][i3];
									}

					if(cpt==1) BE1[j1+6*i1]=B[j1][i1];
					if(cpt==2) BE2[j1+6*i1]=B[j1][i1];
					if(cpt==3) BE3[j1+6*i1]=B[j1][i1];
					if(cpt==4) BE4[j1+6*i1]=B[j1][i1];
					if(cpt==5) BE5[j1+6*i1]=B[j1][i1];
					if(cpt==6) BE6[j1+6*i1]=B[j1][i1];
					if(cpt==7) BE7[j1+6*i1]=B[j1][i1];
					if(cpt==8) BE8[j1+6*i1]=B[j1][i1];



					//printf("%f ",BE[j1+6*i1]);
}

//printf("\n");

}
cpt=cpt+1;

//printf("j1=%d\n",j1);
z=z+2;
}
y=y+2;
}
x=x+2;
}

}

void matC(double E,double nu,double *C){
	double den;
	den=(double)E/((1+nu)*(1-2*nu));

	C[0]=den*(1-nu);
	C[1]=den*nu;
	C[2]=den*nu;
	C[0+6*1]=den*nu;
	C[1+6*1]=den*(1-nu);
	C[2+6*1]=den*nu;
	C[0+6*2]=den*nu;
	C[1+6*2]=den*nu;
	C[2+6*2]=den*(1-nu);
	C[3+6*3]=den*0.5*(1-2*nu);
	C[4+6*4]=den*0.5*(1-2*nu);
	C[5+6*5]=den*0.5*(1-2*nu);


}

void prodMatMat(int i1, int j1, int i2, int j2, double *Mat1, double *Mat2, double *product){
int i,j,k;
double somme=0;
//product[0][0]=1;
if(i1!=j2) printf("DIMENSIONS PROBLEM : the matrix product can't be done\n");
for (i=0;i<i2;i++){
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat1[k+i1*j]*Mat2[i+k*i2];
		}
		product[i+i2*j]=somme;
		// printf("%.2f ",product[i][j]);
}
//printf("\n");
}
//product[0][0]=2.3;
}

void transMat(int i1, int j1, double *original, double *trans){
	//printf("BE \n");
	int i,j;
	for(j=0;j<i1;j++){
		for(i=0;i<j1;i++){
			trans[i+j1*j]=original[j+i*i1];
			//printf("%f ",original[i+i1*j]);
		}
		//printf("\n");
	}
}

void prodMatVec(int i1, int j1, int j2, double *Mat, double *Vec, double *product){
int j,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[k+i1*j]*Vec[k];
		}
		product[j]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

void edofMat_calculation(int id, int ni, int nj, int *edofMat){
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//z1
  edofMat[3]=id+3;//x2
  edofMat[4]=id+4;//y2
  edofMat[5]=id+5;//z2
  edofMat[6]=id+3+3*(ni+1); //x3
  edofMat[7]=id+4+3*(ni+1); //y3
  edofMat[8]=id+5+3*(ni+1); //z3
  edofMat[9]=id+3*(ni+1); //x4
  edofMat[10]=id+1+3*(ni+1); //y4
  edofMat[11]=id+2+3*(ni+1); //z4
  edofMat[12]=id+3*(ni+1)*(nj+1);//x5
  edofMat[13]=id+1+3*(ni+1)*(nj+1);//y5
  edofMat[14]=id+2+3*(ni+1)*(nj+1);//z5
  edofMat[15]=id+3+3*(ni+1)*(nj+1);//x6
  edofMat[16]=id+4+3*(ni+1)*(nj+1);//y6
  edofMat[17]=id+5+3*(ni+1)*(nj+1);//z6
  edofMat[18]=id+3+3*(ni+1)+3*(ni+1)*(nj+1); //x7
  edofMat[19]=id+4+3*(ni+1)+3*(ni+1)*(nj+1); //y7
  edofMat[20]=id+5+3*(ni+1)+3*(ni+1)*(nj+1); //z7
  edofMat[21]=id+3*(ni+1)+3*(ni+1)*(nj+1); //x8
  edofMat[22]=id+1+3*(ni+1)+3*(ni+1)*(nj+1); //y8
  edofMat[23]=id+2+3*(ni+1)+3*(ni+1)*(nj+1); //z8
}

__global__ void GPUMatVec(int ni, int nj, size_t numElems, double *Ke,  double *U, double *r)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id,edofMat[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id=3*((ni+1)*(nj+1)*k+(ni+1)*j+i);

      edofMat[0]=id;//x1
      edofMat[1]=id+1;//y1
      edofMat[2]=id+2;//z1
      edofMat[3]=id+3;//x2
      edofMat[4]=id+4;//y2
      edofMat[5]=id+5;//z2
      edofMat[6]=id+3+3*(ni+1); //x3
      edofMat[7]=id+4+3*(ni+1); //y3
      edofMat[8]=id+5+3*(ni+1); //z3
      edofMat[9]=id+3*(ni+1); //x4
      edofMat[10]=id+1+3*(ni+1); //y4
      edofMat[11]=id+2+3*(ni+1); //z4
      edofMat[12]=id+3*(ni+1)*(nj+1);//x5
      edofMat[13]=id+1+3*(ni+1)*(nj+1);//y5
      edofMat[14]=id+2+3*(ni+1)*(nj+1);//z5
      edofMat[15]=id+3+3*(ni+1)*(nj+1);//x6
      edofMat[16]=id+4+3*(ni+1)*(nj+1);//y6
      edofMat[17]=id+5+3*(ni+1)*(nj+1);//z6
      edofMat[18]=id+3+3*(ni+1)+3*(ni+1)*(nj+1); //x7
      edofMat[19]=id+4+3*(ni+1)+3*(ni+1)*(nj+1); //y7
      edofMat[20]=id+5+3*(ni+1)+3*(ni+1)*(nj+1); //z7
      edofMat[21]=id+3*(ni+1)+3*(ni+1)*(nj+1); //x8
      edofMat[22]=id+1+3*(ni+1)+3*(ni+1)*(nj+1); //y8
      edofMat[23]=id+2+3*(ni+1)+3*(ni+1)*(nj+1); //z8



	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= Ke[i*MNE+j]*U[edofMat[j]];
                                  }
      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7]+
          Au[8]+Au[9]+Au[10]+Au[11]+Au[12]+Au[13]+Au[14]+Au[15]+
          Au[16]+Au[17]+Au[18]+Au[19]+Au[20]+Au[21]+Au[22]+Au[23];

      atomicAdd(&r[edofMat[i]], sum);
                         }
  }

}


__global__ void rzp_calculation(double *f, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
      //printf("M[%d]=%f\n",n,M_pre[n]);
      r[n] = f[n] - prod[n];
      z[n]=r[n];
      p[n] = z[n];

      //if(n==0) printf("M_pre[21056]=%lg, r[21056]=%lg, p[21056]=%lg, z[21056]=%lg\n",M_pre[21056],
    //r[21056],p[21056],z[21056]);
    }

  }

}


__global__ void UpdateVec(double *p, double *r, int numNodes, int *fixednodes, double *rzold, double *rznew, double *Ap)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  double beta = rznew[0]/rzold[0];
  //if(n==0) printf("rzold=%lg, rznew=%lg and beta=%f\n",rzold[0],rznew[0],beta);
  if (n<numNodes){
    Ap[n]=0;
    if(fixednodes[n]==0 ){

      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void switch_r(double *rzold, double *rznew, double *pAp){
  //size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  if(threadIdx.x==0){
    rzold[0]=rznew[0];
    rznew[0]=0;
    pAp[0]=0;
    //norme[0]=0;
  }
}


__global__ void Norm(double *r, int numNodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
  result[n]=r[n]*r[n];
    }
}

__global__ void Prod_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  ){
      atomicAdd(&result[0], r[n]*z[n]);
    }
    }
}

__global__ void Prod2_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
   }

}

__global__ void Prod3_rz(double *r, double*z, int numNodes, int *fixednodes, double *result, double *norme)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
     norme[0]=0;
   }

}

__global__ void Update_UR(double *U, double *r, double *z, double *p, double *Ap, int numNodes, int *fixednodes,
  double *rzold, double *pAp, double *result1, double *result2)
{

  /*size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = M_pre[n]*r[n];

    }

  }*/

  __shared__ double cache1[BLOCKSIZE2];
  __shared__ double cache2[BLOCKSIZE2];

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp1=0.0;
  double temp2=0.0;


  while (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
        //if(n==0) printf("rzold=%lg, pAp=%lg and alpha=%f\n",rzold[0],pAp[0],alpha);
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = r[n];
      temp1=temp1+ r[n]*z[n];
      temp2=temp2+ r[n]*r[n];
    }
    n=n+stride;
    }

    cache1[threadIdx.x]=temp1;
    cache2[threadIdx.x]=temp2;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache1[threadIdx.x]+=cache1[threadIdx.x+i];
        cache2[threadIdx.x]+=cache2[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }

    if (threadIdx.x ==0){
     atomicAdd(&result1[0],cache1[0]);
     atomicAdd(&result2[0],cache2[0]);

   }

}

__global__ void display(double *vector, int size){
  if(threadIdx.x==0){
    for (int i=0;i<size;i++){
      printf("vector[%d]=%lg\n",i,vector[i]);
    }
  }
}


int main(int argc, char **argv)
{

  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

  cudaSetDevice(0);
  cudaDeviceReset();

int i,j,k,l,i0,iter,e,id;
int edofMat[MNE];

//size_t free_memory, total_memory; double t;

//size of the domain
//double domain_x=0.01;
//double domain_y=0.1;
//double domain_z=0.050;

//number of elements in each direction
int ni=10;
int nj=100;
int nk=40;
//int nj=20;
int ni2=ni+1;
int nj2=nj+1;

double delta_x=(double)0.001; //domaine de 10 mm par 10 mm ...
double delta_y=(double)0.001;
double delta_z=(double)0.001;

int numElems=ni*nj*nk;
int numNodes=3*(ni+1)*(nj+1)*(nk+1);

double *ux= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *uy= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *uz= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));

int *load=(int *)calloc(numElems,sizeof(int));

for(k=0;k<nk;k++){
  for(j=0;j<nj;j++){
    for(i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      if(k>=20 && k<30) load[i0]=1;
    }
  }
}

double *vms=(double *)calloc(numElems,sizeof(double));
double *vms1=(double *)calloc(numElems,sizeof(double));
double *vms2=(double *)calloc(numElems,sizeof(double));
double *vms3=(double *)calloc(numElems,sizeof(double));
double *vms4=(double *)calloc(numElems,sizeof(double));
double *vms5=(double *)calloc(numElems,sizeof(double));
double *vms6=(double *)calloc(numElems,sizeof(double));
double *vms7=(double *)calloc(numElems,sizeof(double));
double *vms8=(double *)calloc(numElems,sizeof(double));

double E=200e9;
double nu=0.33;
double inh_strain[6]={-0.015,-0.015,0.02,0,0,0};
double BE[144]={0},BE_t[144],BE1[144],BE1_t[144],BE2[144],BE2_t[144], BE3[144],
BE3_t[144], BE4[144],BE4_t[144], BE5[144],BE5_t[144],BE6[144],BE6_t[144], BE7[144],BE7_t[144], BE8[144],BE8_t[144];
double prod1[144]={0};
double fe[24]={0};
double Bmat[6][24][8]={0};
double C[36]={0};
double epsilon1[6]={0}; 	double epsilon2[6]={0}; double epsilon3[6]={0}; 	double epsilon4[6]={0};  double epsilon5[6]={0}; 	double epsilon6[6]={0};  double epsilon7[6]={0}; 	double epsilon8[6]={0};
double epsilon_bis1[6]={0}; double epsilon_bis2[6]={0}; double epsilon_bis3[6]={0}; double epsilon_bis4[6]={0}; double epsilon_bis5[6]={0}; double epsilon_bis6[6]={0}; double epsilon_bis7[6]={0}; double epsilon_bis8[6]={0};
double Ue[24]={0};
double *Ke = (double *) malloc(MNE*MNE * sizeof(double));
//Strain displacement matrix with gauss quadrature
Be_matrix(delta_x,delta_y,delta_z,BE);
//Strain displacement matrix without gauss quadrature for all nodes
BMat_matrix(delta_x,delta_y,delta_z,Bmat);
//Strain displacement matrix without gauss quadrature
BE_matrix2(delta_x,delta_y,delta_z,BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,E,nu);
//Material properties
matC(E,nu,C);
//Stiffness matrix
KM_matrix(delta_x,delta_y,delta_z,Ke,E,nu,C,Bmat);


//int *edofMat; edofMat=(int *)calloc(MNE,sizeof(int));

cudaStream_t *streams;
streams = (cudaStream_t *)malloc(2 * sizeof(cudaStream_t));
checkCudaErrors(cudaStreamCreate(&(streams[0])));
checkCudaErrors(cudaStreamCreate(&(streams[1])));

double *U_d,*r_d,*prod_d,*f_d,*p_d,*z_d,*Ap_d;
int *fixednodes_d;


  //int *fixednodes_d, *activenodes_d, *convnodes_x_d, *convnodes_y_d,*convnodes_z_d; //*ndofMat_d;
  //int *domain_optim_d, *domain_d;
  int cpt;

  //int *pos_heat_d;
  //double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double  *Ke_d; //z_d
  double *pAp_d;
  double *rznew_d,*rzold_d;
  double *rNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  //double *fNorm = (double *) calloc(1,sizeof(double));
  double norme;
  char filename[500];

  //Fixed displacement
  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  k=0;
  cpt=0;
   for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
      i0=I3D(ni2,nj2,i,j,k);
      fixednodes[3*i0]=1; // x-direction
      fixednodes[3*i0+1]=1; // y-direction
      fixednodes[3*i0+2]=1; // z-direction
    }
  }

  //Nodal force
  double *f =(double *)calloc(numNodes,sizeof(double));

  /*//k=0;
  j=nj;
    for(k=0;k<nk+1;k++){
  for(i=0;i<ni+1;i++){
    i0=I3D(ni2,nj2,i,j,k);
    f[3*i0+1]=(double)1e5/((ni+1)*(nk+1));
  }
}*/

for (e=0;e<numElems;e++){
  k=e/(ni*nj);
  j=(e%(nj*ni))/ni;
  i=	(e%(nj*ni))%ni;
  i0=I3D(ni,nj,i,j,k);
  id=3*((ni+1)*(nj+1)*k+(ni+1)*j+i);

  if(load[i0]==1){

    edofMat_calculation(id,ni,nj,edofMat);
    prodMatMat(6,24,6,6,BE,C,prod1);
    prodMatVec(6,24,6,prod1,inh_strain,fe);
    for (l=0;l<MNE;l++) f[edofMat[l]]=f[edofMat[l]]+fe[l];

  }
}

  double *U = (double *) calloc(numNodes , sizeof(double));

  // Allocate memory
  cudaMalloc((void **) &Ke_d, MNE*MNE * sizeof(double));

  //additional variable for reduction operations
  cudaMalloc((void **) &pAp_d, 1 * sizeof(double));
  cudaMalloc((void **) &rznew_d, 1 * sizeof(double));
  cudaMalloc((void **) &rzold_d, 1 * sizeof(double));
  cudaMalloc((void **) &rNorm_d, 1 * sizeof(double));

  //Calculation of the element matrix
  cudaMemcpy(Ke_d, Ke, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);

  checkCudaErrors(cudaMalloc((void **) &U_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &f_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &r_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &p_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &z_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &fixednodes_d, numNodes * sizeof(int)));

  //Transfer to GPU
  checkCudaErrors(cudaMemcpy(U_d, U, numNodes * sizeof(double),cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(f_d, f, numNodes * sizeof(double),cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(fixednodes_d, fixednodes, numNodes * sizeof(int),cudaMemcpyHostToDevice));

  int NBLOCKS   = (numElems+BLOCKSIZE1-1) / BLOCKSIZE1; //round up if n is not a multiple of blocksize
  int NBLOCKS2 = (numNodes+BLOCKSIZE2-1) / BLOCKSIZE2; //round up if n is not a multiple of blocksize

  /////////////////////////////////////////////////////////////////////

  double tol = 1e-6;//1.0e-8*numNodes2;

  checkCudaErrors(cudaMemset(prod_d, 0.0, numNodes*sizeof(double)));
  GPUMatVec<<<NBLOCKS,BLOCKSIZE1>>>(ni,nj,numElems,Ke_d,U_d,prod_d);

  checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
  rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_d, r_d,prod_d, z_d, p_d, numNodes, fixednodes_d);

  cudaMemset(rzold_d, 0.0, 1*sizeof(double));
  Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_d, r_d,numNodes,fixednodes_d,rzold_d);

  checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
  //printf("Init : rzold=%lg  \n",rznew[0]);
  int Max_iters=5000;
  rNorm[0]=10000;

    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

    GPUMatVec<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni,nj, numElems, Ke_d,p_d, Ap_d);

    Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, Ap_d,numNodes,fixednodes_d,pAp_d,rNorm_d);

    Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(U_d, r_d, z_d, p_d, Ap_d,numNodes,fixednodes_d, rzold_d,pAp_d,rznew_d,rNorm_d);

    UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, z_d, numNodes,fixednodes_d,rzold_d,rznew_d,Ap_d);

    switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

    checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

    norme=sqrt(rNorm[0]);
    //printf("iter=%d : norme=%lg\n",iter,norme);
    checkCudaErrors(cudaStreamSynchronize(streams[0]));

    if(norme <= tol) break;

}

if(iter==Max_iters){
  printf("Residual error : end of program \n");
  return(0);
}

printf("Solution obtained in %d iterations : residual=%lg\n",iter,norme);

cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);

double min_ux=1e12;
double max_ux=-1e12;
double min_uy=1e12;
double max_uy=-1e12;
double min_uz=1e12;
double max_uz=-1e12;
cpt=0;
 for (k=0;k<nk+1;k++){
 for (j=0;j<nj+1;j++){
for (i=0;i<ni+1;i++){
  i0=I3D(ni2,nj2,i,j,k);
	 ux[i0]=U[cpt];   cpt=cpt+1;
	 uy[i0]=U[cpt];   cpt=cpt+1;
	 uz[i0]=U[cpt];   cpt=cpt+1;
   if(ux[i0]<min_ux) min_ux=ux[i0];
   if(ux[i0]>max_ux) max_ux=ux[i0];
   if(uy[i0]<min_uy) min_uy=uy[i0];
   if(uy[i0]>max_uy) max_uy=uy[i0];
   if(uz[i0]<min_uz) min_uz=uz[i0];
   if(uz[i0]>max_uz) max_uz=uz[i0];
  // if(uz[i0]>0.1) printf("1- i=%d, j=%d, k=%d, uz=%lg\n",i,j,k,uz[i0]);
}
}
}

printf("min_ux=%lg and max_ux=%lg \n",min_ux,max_ux);
printf("min_uy=%lg and max_uy=%lg \n",min_uy,max_uy);
printf("min_uz=%lg and max_uz=%lg \n",min_uz,max_uz);

double min_vms=1e13;
double max_vms=-1;
//Stress CALCULATION
//Calculation of elastic strain


for (e=0;e<numElems;e++){
  k=e/(ni*nj);
  j=(e%(nj*ni))/ni;
  i=	(e%(nj*ni))%ni;
  i0=I3D(ni,nj,i,j,k);
  id=3*((ni+1)*(nj+1)*k+(ni+1)*j+i);

  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//z1
  edofMat[3]=id+3;//x2
  edofMat[4]=id+4;//y2
  edofMat[5]=id+5;//z2
  edofMat[6]=id+3+3*(ni+1); //x3
  edofMat[7]=id+4+3*(ni+1); //y3
  edofMat[8]=id+5+3*(ni+1); //z3
  edofMat[9]=id+3*(ni+1); //x4
  edofMat[10]=id+1+3*(ni+1); //y4
  edofMat[11]=id+2+3*(ni+1); //z4
  edofMat[12]=id+3*(ni+1)*(nj+1);//x5
  edofMat[13]=id+1+3*(ni+1)*(nj+1);//y5
  edofMat[14]=id+2+3*(ni+1)*(nj+1);//z5
  edofMat[15]=id+3+3*(ni+1)*(nj+1);//x6
  edofMat[16]=id+4+3*(ni+1)*(nj+1);//y6
  edofMat[17]=id+5+3*(ni+1)*(nj+1);//z6
  edofMat[18]=id+3+3*(ni+1)+3*(ni+1)*(nj+1); //x7
  edofMat[19]=id+4+3*(ni+1)+3*(ni+1)*(nj+1); //y7
  edofMat[20]=id+5+3*(ni+1)+3*(ni+1)*(nj+1); //z7
  edofMat[21]=id+3*(ni+1)+3*(ni+1)*(nj+1); //x8
  edofMat[22]=id+1+3*(ni+1)+3*(ni+1)*(nj+1); //y8
  edofMat[23]=id+2+3*(ni+1)+3*(ni+1)*(nj+1); //z8

  transMat(6,24,BE,BE_t);
  transMat(6,24,BE1,BE1_t); transMat(6,24,BE2,BE2_t); transMat(6,24,BE3,BE3_t); transMat(6,24,BE4,BE4_t);
  transMat(6,24,BE5,BE5_t); transMat(6,24,BE6,BE6_t); transMat(6,24,BE7,BE7_t); transMat(6,24,BE8,BE8_t);

  for (int l=0;l<24;l++) {
    Ue[l]=U[edofMat[l]];
  }
  prodMatVec(24,6,24,BE1_t,Ue,epsilon1);
  prodMatVec(24,6,24,BE2_t,Ue,epsilon2);
  prodMatVec(24,6,24,BE3_t,Ue,epsilon3);
  prodMatVec(24,6,24,BE4_t,Ue,epsilon4);
  prodMatVec(24,6,24,BE5_t,Ue,epsilon5);
  prodMatVec(24,6,24,BE6_t,Ue,epsilon6);
  prodMatVec(24,6,24,BE7_t,Ue,epsilon7);
  prodMatVec(24,6,24,BE8_t,Ue,epsilon8);

  /*for (l=0;l<6;l++) epsilon1[l]=epsilon1[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon2[l]=epsilon2[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon3[l]=epsilon3[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon4[l]=epsilon4[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon5[l]=epsilon5[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon6[l]=epsilon6[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon7[l]=epsilon7[l];//-inh_strain_solide[l];
  for (l=0;l<6;l++) epsilon8[l]=epsilon8[l];//-inh_strain_solide[l];*/

  prodMatVec(6,6,6,C,epsilon1,epsilon_bis1);
  prodMatVec(6,6,6,C,epsilon2,epsilon_bis2);
  prodMatVec(6,6,6,C,epsilon3,epsilon_bis3);
  prodMatVec(6,6,6,C,epsilon4,epsilon_bis4);
  prodMatVec(6,6,6,C,epsilon5,epsilon_bis5);
  prodMatVec(6,6,6,C,epsilon6,epsilon_bis6);
  prodMatVec(6,6,6,C,epsilon7,epsilon_bis7);
  prodMatVec(6,6,6,C,epsilon8,epsilon_bis8);

  vms1[i0]=sqrt(epsilon_bis1[0]*epsilon_bis1[0]+epsilon_bis1[1]*epsilon_bis1[1]+epsilon_bis1[2]*epsilon_bis1[2]-epsilon_bis1[0]*epsilon_bis1[1]-epsilon_bis1[1]*epsilon_bis1[2]-epsilon_bis1[0]*epsilon_bis1[2]
    +3*epsilon_bis1[3]*epsilon_bis1[3]+3*epsilon_bis1[4]*epsilon_bis1[4]+3*epsilon_bis1[5]*epsilon_bis1[5]);

  vms2[i0]=sqrt(epsilon_bis2[0]*epsilon_bis2[0]+epsilon_bis2[1]*epsilon_bis2[1]+epsilon_bis2[2]*epsilon_bis2[2]-epsilon_bis2[0]*epsilon_bis2[1]-epsilon_bis2[1]*epsilon_bis2[2]-epsilon_bis2[0]*epsilon_bis2[2]
    +3*epsilon_bis2[3]*epsilon_bis2[3]+3*epsilon_bis2[4]*epsilon_bis2[4]+3*epsilon_bis2[5]*epsilon_bis2[5]);

  vms3[i0]=sqrt(epsilon_bis3[0]*epsilon_bis3[0]+epsilon_bis3[1]*epsilon_bis3[1]+epsilon_bis3[2]*epsilon_bis3[2]-epsilon_bis3[0]*epsilon_bis3[1]-epsilon_bis3[1]*epsilon_bis3[2]-epsilon_bis3[0]*epsilon_bis3[2]
    +3*epsilon_bis3[3]*epsilon_bis3[3]+3*epsilon_bis3[4]*epsilon_bis3[4]+3*epsilon_bis3[5]*epsilon_bis3[5]);

  vms4[i0]=sqrt(epsilon_bis4[0]*epsilon_bis4[0]+epsilon_bis4[1]*epsilon_bis4[1]+epsilon_bis4[2]*epsilon_bis4[2]-epsilon_bis4[0]*epsilon_bis4[1]-epsilon_bis4[1]*epsilon_bis4[2]-epsilon_bis4[0]*epsilon_bis4[2]
    +3*epsilon_bis4[3]*epsilon_bis4[3]+3*epsilon_bis4[4]*epsilon_bis4[4]+3*epsilon_bis4[5]*epsilon_bis4[5]);

  vms5[i0]=sqrt(epsilon_bis5[0]*epsilon_bis5[0]+epsilon_bis5[1]*epsilon_bis5[1]+epsilon_bis5[2]*epsilon_bis5[2]-epsilon_bis5[0]*epsilon_bis5[1]-epsilon_bis5[1]*epsilon_bis5[2]-epsilon_bis5[0]*epsilon_bis5[2]
    +3*epsilon_bis5[3]*epsilon_bis5[3]+3*epsilon_bis5[4]*epsilon_bis5[4]+3*epsilon_bis5[5]*epsilon_bis5[5]);

  vms6[i0]=sqrt(epsilon_bis6[0]*epsilon_bis6[0]+epsilon_bis6[1]*epsilon_bis6[1]+epsilon_bis6[2]*epsilon_bis6[2]-epsilon_bis6[0]*epsilon_bis6[1]-epsilon_bis6[1]*epsilon_bis6[2]-epsilon_bis6[0]*epsilon_bis6[2]
    +3*epsilon_bis6[3]*epsilon_bis6[3]+3*epsilon_bis6[4]*epsilon_bis6[4]+3*epsilon_bis6[5]*epsilon_bis6[5]);

  vms7[i0]=sqrt(epsilon_bis7[0]*epsilon_bis7[0]+epsilon_bis7[1]*epsilon_bis7[1]+epsilon_bis7[2]*epsilon_bis7[2]-epsilon_bis7[0]*epsilon_bis7[1]-epsilon_bis7[1]*epsilon_bis7[2]-epsilon_bis7[0]*epsilon_bis7[2]
    +3*epsilon_bis7[3]*epsilon_bis7[3]+3*epsilon_bis7[4]*epsilon_bis7[4]+3*epsilon_bis7[5]*epsilon_bis7[5]);

  vms8[i0]=sqrt(epsilon_bis8[0]*epsilon_bis8[0]+epsilon_bis8[1]*epsilon_bis8[1]+epsilon_bis8[2]*epsilon_bis8[2]-epsilon_bis8[0]*epsilon_bis8[1]-epsilon_bis8[1]*epsilon_bis8[2]-epsilon_bis8[0]*epsilon_bis8[2]
    +3*epsilon_bis8[3]*epsilon_bis8[3]+3*epsilon_bis8[4]*epsilon_bis8[4]+3*epsilon_bis8[5]*epsilon_bis8[5]);

  //vms[i0]=sqrt(vms1[i0]*vms1[i0]+vms2[i0]*vms2[i0]+vms3[i0]*vms3[i0]+vms4[i0]*vms4[i0]+vms5[i0]*vms5[i0]+
  //vms6[i0]*vms6[i0]+vms7[i0]*vms7[i0]+vms8[i0]*vms8[i0]);
  vms[i0]=MAX(vms1[i0],MAX(vms2[i0],MAX(vms3[i0],MAX(vms4[i0],MAX(vms5[i0],MAX(vms6[i0],MAX(vms7[i0],vms8[i0])))))));
  //(double)(vms1[i0]+vms2[i0]+vms3[i0]+vms4[i0]+vms5[i0]+vms6[i0]+vms7[i0]+vms8[i0])/8;
  if(vms[i0]<min_vms) min_vms=vms[i0];
  if(vms[i0]>max_vms) max_vms=vms[i0];
}

printf("min_vms=%lg and max_vms=%lg\n",min_vms,max_vms);


size_t free_memory, total_memory;
checkCudaErrors(cudaMemGetInfo(&free_memory,&total_memory));
printf("GPU memory usage : %.3f GB\n",(double) (total_memory-free_memory)/1e9);


FILE *fichier;
strcpy(filename,"disp.vtk"); fichier=fopen(filename,"w");
fprintf(fichier, "# vtk DataFile Version 2.0\n");
fprintf(fichier, "VTK from C\n");
fprintf(fichier,"ASCII\n");
fprintf(fichier,"DATASET STRUCTURED_GRID\n");
fprintf(fichier,"DIMENSIONS %d %d %d\n",ni+1,nj+1,nk+1);
fprintf(fichier,"POINTS %d float\n",(ni+1)*(nj+1)*(nk+1));
for (k=0;k<nk+1;k++){
  for (j=0;j<nj+1;j++){
      for (i=0;i<ni+1;i++){
        i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%d %d %d ",i,j,k);
    }
  }
}
fprintf(fichier,"\nPOINT_DATA %d\n",(ni+1)*(nj+1)*(nk+1));
fprintf(fichier,"SCALARS ux float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk+1;k++){
  for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
      i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%f ",ux[i0]);
    }
  }
}
fprintf(fichier,"\nSCALARS uy float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk+1;k++){
  for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
      i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%f ",uy[i0]);
    }
  }
}
fprintf(fichier,"\nSCALARS uz_2 float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk+1;k++){
  for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
      i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%f ",uz[i0]);
    }
  }
}
fclose(fichier);

strcpy(filename,"vms.vtk"); fichier=fopen(filename,"w");
fprintf(fichier, "# vtk DataFile Version 2.0\n");
fprintf(fichier, "VTK from C\n");
fprintf(fichier,"ASCII\n");
fprintf(fichier,"DATASET STRUCTURED_GRID\n");
fprintf(fichier,"DIMENSIONS %d %d %d\n",ni,nj,nk);
fprintf(fichier,"POINTS %d float\n",numElems);
for (k=0;k<nk;k++){
  for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      fprintf(fichier,"%d %d %d ",i,j,k);
    }
  }
}
fprintf(fichier,"\nPOINT_DATA %d\n",numElems);
fprintf(fichier,"SCALARS vms_2 float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
  for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      fprintf(fichier,"%f ",vms[i0]);
     }
    }
  }
  fprintf(fichier,"\nSCALARS vms1 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms1[i0]);
      }
    }
  }
  fprintf(fichier,"\nSCALARS vms2 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      fprintf(fichier,"%f ",vms2[i0]);
     }
   }
  }
  fprintf(fichier,"\nSCALARS vms3 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms3[i0]);
       }
     }
   }
  fprintf(fichier,"\nSCALARS vms4 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms4[i0]);
       }
     }
   }
  fprintf(fichier,"\nSCALARS vms5 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms5[i0]);
       }
     }
   }
  fprintf(fichier,"\nSCALARS vms6 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms6[i0]);
      }
    }
  }
  fprintf(fichier,"\nSCALARS vms7 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms7[i0]);
      }
    }
  }
  fprintf(fichier,"\nSCALARS vms8 float\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (k=0;k<nk;k++){
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
        i0=I3D(ni,nj,i,j,k);
        fprintf(fichier,"%f ",vms8[i0]);
      }
    }
  }

fclose(fichier);

/*FILE *fichier;
fichier=fopen("uz.txt","w");
for (k=0;k<nk+1;k++){
  for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
      i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%d %d %d %f\n",i,j,k,uz[i0]);
      if(uz[i0]>0.1) printf("2 - i=%d, j=%d, k=%d, uz=%lg\n",i,j,k,uz[i0]);
    }
  }
}
fclose(fichier);*/





  //free gpu memory

  checkCudaErrors(cudaFree(Ke_d));
  checkCudaErrors(cudaFree(pAp_d));
  checkCudaErrors(cudaFree(rznew_d));
  checkCudaErrors(cudaFree(rzold_d));
  checkCudaErrors(cudaFree(rNorm_d));
  checkCudaErrors(cudaFree(fixednodes_d));
  checkCudaErrors(cudaFree(U_d));
  checkCudaErrors(cudaFree(r_d));
  checkCudaErrors(cudaFree(p_d));
  checkCudaErrors(cudaFree(z_d));
  checkCudaErrors(cudaFree(f_d));
  checkCudaErrors(cudaFree(prod_d));
  checkCudaErrors(cudaFree(Ap_d));


  free(U);
  free(ux);
  free(uy);
  free(uz);
  free(vms); free(vms1); free(vms2); free(vms3); free(vms4); free(vms5); free(vms6); free(vms7); free(vms8);
  free(pAp);
  free(rznew);
  free(rNorm);
  free(Ke);
  free(fixednodes);


  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
