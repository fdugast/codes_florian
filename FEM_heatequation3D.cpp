
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)


double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, int nk, double rmin, double *x, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
					for (k = 0; k < nk2; k++){

				i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
							for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {

								i02 = I3D(ni2,nj2, i, j,k);
					if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];
						}
					}
				}
			}


			if(x[i0]*somme!=0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void filtre2(int ni, int nj, int nk, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			for (k = 0; k < nk2; k++){

		i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
								for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {
								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
						fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}
				}

						if(somme!=0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void OC (int ni, int nj, int nk, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*nk*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				for (k = 0; k < nk; k++){
				i0 = I3D(ni,nj, i, j,k);
				gamma_old[i0]=gamma0[i0];
			}
		}
	}


	while ((l2-l1)> 1e-4){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
			for (k = 0; k < nk; k++){
			i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
	//printf("ok boucle \n");
				if(gradient[i0]>0) {

					printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);
			}
		}
	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
		for (k = 0; k < nk; k++){
		i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

}
		}
	}
}

volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

int main(int argc,char **args)
{

	printf("Debut programme \n");



printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,K2,M1,M2,U1,U2,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew;
  PetscScalar y;
	char filename[200],u2[200];

	double Xmin = 0.0001;
	double Xmax = 1;
	double movlim = 0.05;



	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
	double KE[64],ME[64],HE1[64],HE2[64],HE3[64],HE4[64],HE5[64],HE6[64],ke[64],me[64];
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  clock_t start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim;

	ni = 200;
	nj = 200;
	nk = 200;
	ni2=201;
	nj2=201;

	int N=ni*nj*nk;
	double *xmma = new double[N];
	double *xold = new double[N];
	double *f = new double[1];
	double *df = new double[N];
	double *df2 = new double[N];
	double *g = new double[1];
	double *dg = new double[N];
	double *xmin = new double[N];
	double *xmax = new double[N];

	double *af = new double[N];
	double *bf = new double[N];
	// Initialize MMA
	MMASolver *mma = new MMASolver(N,1);

	ndof=(ni+1)*(nj+1)*(nk+1);
	nele=ni*nj*nk;
	opt_it_max=1;
	alpha=1; // coeff pow compliance
	rmin=1.4;
	volfrac=1;
	float hconv=1000;
	double delta,somme,somme1,somme2,delta_x, delta_y,delta_z,delta_t,total_time,time;
	delta_x=(double)0.2/(ni-1); //domaine de 10 mm par 10 mm ...
	delta_y=(double)0.2/(nj-1);
	delta_z=(double)0.2/(nk-1);
	delta_t=0.1; // si delta t trop fort oscillations sur compliance
	total_time=0.5;
	int nbsteps;
	nbsteps=total_time/delta_t;

	//Medium properties
	int *edofMat;
	edofMat=(int *)calloc(8,sizeof(int));
	domain_optim=(int *)calloc(nele,sizeof(int));
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp1,Tp2,Tair;
	cp=(double *)calloc(nele,sizeof(double));
	lambda=(double *)calloc(nele,sizeof(double));
	derivee_c=(double *)calloc(nele,sizeof(double));
	derivee_lambda=(double *)calloc(nele,sizeof(double));
	rho=(double *)calloc(nele,sizeof(double));
	diff=(double *)calloc(nele,sizeof(double));
	c=(double *)calloc(nele,sizeof(double));
	rho_solide=1000;
	lambda_solide=100;
	cp_solide=500;
	rho_vide=1000;
	lambda_vide=100;
	cp_vide=500;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	Qvol=0;//10;
	T0=0; //Initial temperature
	Tp1=0; //Dirichlet temperature
	Tp2=0.f/(delta_x*delta_y); //Dirichlet temperature
	Qsurf=0*1e6;
	Tair=0;
	penal=3;

	FILE *fichier_K,*fichier_M, *fichier_H1,*fichier_H2, *fichier_H3,*fichier_H4,*fichier_H5,*fichier_H6;
	fichier_K=fopen("Ke_3d.txt","r");
	fichier_M=fopen("Me_3d.txt","r");
	fichier_H1=fopen("HE_3D_1.txt","r");
	fichier_H2=fopen("HE_3D_2.txt","r");
	fichier_H3=fopen("HE_3D_3.txt","r");
	fichier_H4=fopen("HE_3D_4.txt","r");
	fichier_H5=fopen("HE_3D_5.txt","r");
	fichier_H6=fopen("HE_3D_6.txt","r");
	float test;
	for (i=0;i<64;i++){
		fscanf(fichier_K,"%f\n",&test);		KE[i]=test;
		fscanf(fichier_M,"%f\n",&test);		ME[i]=test;
		fscanf(fichier_H1,"%f\n",&test);		HE1[i]=test;
		fscanf(fichier_H2,"%f\n",&test);		HE2[i]=test;
		fscanf(fichier_H3,"%f\n",&test);		HE3[i]=test;
		fscanf(fichier_H4,"%f\n",&test);		HE4[i]=test;
		fscanf(fichier_H5,"%f\n",&test);		HE5[i]=test;
		fscanf(fichier_H6,"%f\n",&test);		HE6[i]=test;
	}

	fclose(fichier_K);
	fclose(fichier_M);
	fclose(fichier_H1);
	fclose(fichier_H2);
	fclose(fichier_H3);
	fclose(fichier_H4);
	fclose(fichier_H5);
	fclose(fichier_H6);

	AmgXSolver solver;
	ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);

	int *boundary;
	boundary=(int *)calloc(nele,sizeof(int));

	double *F;
	F=(double *)calloc(ndof,sizeof(double));

	double *x1;
	double *Told;
	double *T_transient, *pos_q1_transient, *T_transient2, *derivee_T_transient,*adjoint_transient,*prod_KU_transient,*second_membre_transient,*derivee_T;
	double *q2,*q3,*q4,*pos_q1,*pos_q2,*pos_q3,*pos_q4, *compliance, *gradient, *lu;
	int *q1, *num_elem;
	double *gamma_x, *gamma_h;
	double *fonction_cout;
	fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
//	solide=(double *)calloc(nele,sizeof(double));
	gamma_h=(double *)calloc(nele,sizeof(double));
	gamma_x=(double *)calloc(nele,sizeof(double));
	x1=(double *)calloc(ndof,sizeof(double));
	Told=(double *)calloc(ndof,sizeof(double));
	T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	pos_q1_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	T_transient2=(double *)calloc(nbsteps*ndof,sizeof(double));
	derivee_T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	adjoint_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	prod_KU_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	second_membre_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	derivee_T=(double *)calloc(nbsteps*ndof,sizeof(double));
	q1=(int *)calloc(nbsteps,sizeof(int));
	num_elem=(int *)calloc(nbsteps,sizeof(int));
	q2=(double *)calloc(nbsteps,sizeof(double));
	q3=(double *)calloc(nbsteps,sizeof(double));
	q4=(double *)calloc(nbsteps,sizeof(double));
	pos_q1=(double *)calloc(ndof,sizeof(double));
	pos_q2=(double *)calloc(ndof,sizeof(double));
	pos_q3=(double *)calloc(ndof,sizeof(double));
	pos_q4=(double *)calloc(ndof,sizeof(double));
	lu=(double *)calloc(ndof,sizeof(double));
	compliance=(double *)calloc(nbsteps,sizeof(double));
	gradient=(double *)calloc(nele,sizeof(double));

/*	int test;
	FILE *domain;
		domain=fopen("domain2_triangle2.txt","r");
	double somme_domain;

	double volfrac2;
	cpt=0;
	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				i0=I2D(ni,i,j);
				fscanf(domain,"%d\n",&test);
				domain_optim[i0]=test;
							if(test==0) cpt=cpt+1;
				somme_domain=somme_domain+(1-test);
			//	printf("domain optim=%d\n",domain_optim[i0]);
				//if(j>20 && i<150 && i>50) domain_optim[i0]=0;
			}
		}
		volfrac2=(double)(nele*volfrac-cpt)/(nele-cpt);
printf("volfrac_shape=%f\n",somme_domain/(nele));
		//for (i=0; i<nele; i++) fprintf(domain,"%d\n",domain_optim[i]);
fclose(domain);
//getchar();*/

	FILE *init;
//	init=fopen("init2.txt","r");

	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);
				//fscanf(init,"%f",&test);
				if(domain_optim[i0]==1){
				gamma_h[i0]=volfrac;
			//	gamma_x[i0]=volfrac;
				xmma[i0]=gamma_h[i0];
				xold[i0]=gamma_h[i0];
			}
			if(domain_optim[i0]==0){
				gamma_h[i0]=1;
				gamma_x[i0]=1;//volfrac;
				xmma[i0]=volfrac;//gamma_h[i0];
				xold[i0]=volfrac;//gamma_h[i0];
			}
			}
		}
	}

	//Definition of boundary conditions

	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
					for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);

				if(i==0 || j==0 || i==(ni-1) || j==(nj-1) || k==0 || k==(nk-1)){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;
				if(j==0 ) boundary[i0]=1;
				if(i==0 ) boundary[i0]=2;//2;
				if(j==(nj-1)) boundary[i0]=3;//3;
				if(i==(ni-1)) boundary[i0]=4;//4; }
				if(k==0) boundary[i0]=5;
				if(k==(nk-1)) boundary[i0]=6;//2;

			}

			}
		}
	}


		/*FILE *fichier;
		fichier=fopen("boundary.txt","w");
		for (i=0;i<ndof;i++) fprintf(fichier,"%d\n",boundary[i]);
		fclose(fichier);*/

	double temps;
	temps=0;

	for (i=0;i<nbsteps;i++){
		//if(temps<=1) q1[i]=0;
		if(temps >1 && temps <=2) q1[i]=25;
		if(temps >2 && temps <=3) q1[i]=30;
		if(temps >3 && temps <=4) q1[i]=35;
		if(temps >4 && temps <=5) q1[i]=40;
		if(temps >5 && temps <=6) q1[i]=45;
		if(temps >6 && temps <=7) q1[i]=50;
		if(temps >7 && temps <=8) q1[i]=55;
		if(temps >8 && temps <=9) q1[i]=60;
		if(temps >9 && temps <=10) q1[i]=65;
		if(temps >10 && temps <=11) q1[i]=70;
		if(temps >11 && temps <=12) q1[i]=75;
		if(temps >12 && temps <=13) q1[i]=80;
		if(temps >13 && temps <=14) q1[i]=85;
		if(temps >14 && temps <=15) q1[i]=90;
		if(temps >15 && temps <=16) q1[i]=95;
		if(temps >16 && temps <=17) q1[i]=100;
		if(temps >17 && temps <=18) q1[i]=105;
		if(temps >18 && temps <=19) q1[i]=110;
		if(temps >19 && temps <=20) q1[i]=115;
		if(temps >20 && temps <=21) q1[i]=120;
		if(temps >21 && temps <=22) q1[i]=125;
		if(temps >22 && temps <=23) q1[i]=130;
		if(temps >23 && temps <=24) q1[i]=135;
		if(temps >24 && temps <=25) q1[i]=140;
		if(temps >25 && temps <=26) q1[i]=145;
		if(temps >26 && temps <=27) q1[i]=150;
		if(temps >27 && temps <=28) q1[i]=155;
		if(temps >28 && temps <=29) q1[i]=160;
		if(temps >29 && temps <=30) q1[i]=165;
		if(temps >30 && temps <=31) q1[i]=170;
		if(temps >31 && temps <=32) q1[i]=175;
		if(temps >32 && temps <=33) q1[i]=180;
		if(temps >33 && temps <=34) q1[i]=185;


		//q1[i]=0.3*1e-2*sin(PI*temps*(temps<=1));
		//q2[i]=0.3*1e-2*sin(PI*temps*(temps<=1));
		//q3[i]=0.3*1e-2*sin(PI*temps*(temps<=1))+0.3*1e-2*sin(PI*temps*(temps>1));
		//q4[i]=0.3*1e-2*sin(PI*temps*(temps<=1))+0.3*1e-2*sin(PI*temps*(temps>1));
		temps=temps+delta_t;
	}

	for (i=0;i<ndof;i++){
		Told[i]=T0;
	}

  nnz=(int *)calloc(ndof,sizeof(int));
	nnz2=(int *)calloc(ndof,sizeof(int));


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Create vectors.  Note that we form 1 vector from scratch and
     then duplicate as needed.
  */

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&D);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&Tn);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F2);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&T);CHKERRQ(ierr);

	ierr = VecSet(D,0);CHKERRQ(ierr);
	ierr = VecSet(Tn,0);CHKERRQ(ierr);
	ierr = VecSet(F1,0);CHKERRQ(ierr);
	ierr = VecSet(F2,0);CHKERRQ(ierr);
	ierr = VecSet(T,0);CHKERRQ(ierr);

	double ratio;

	nz=ndof;
	nz2=ndof;

	for(i=0;i<ndof;i++){
		nnz[i]=0.000025*ndof;
		nnz2[i]=0.00025*ndof;
	}
					/* printf("Definition solide \n");

					for (i=0; i<ni; i++) {
					for (j=0+q1[compteur]; j<5+q1[compteur]; j++) {
						 i0=I2D(ni,i,j);
						 if(domain_optim[i0]==0) gamma_h[i0]=1;

					}
					}*/

	for (k2_it=0;k2_it<opt_it_max;k2_it++){

		start1=clock();


		MatCreate(PETSC_COMM_WORLD,&K1);
		MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
		MatSetFromOptions(K1);
		MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&K1);
		MatSetOption(K1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

		MatCreate(PETSC_COMM_WORLD,&K2);
		MatSetSizes(K2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
		MatSetFromOptions(K2);
		MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&K2);
		MatSetOption(K2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&M1);
			MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(M1);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&M1);
			MatSetOption(M1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&M2);
			MatSetSizes(M2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(M2);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&M2);
			MatSetOption(M2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);


			MatCreate(PETSC_COMM_WORLD,&U1);
			MatSetSizes(U1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(U1);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&U1);
			MatSetOption(U1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&U2);
			MatSetSizes(U2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(U2);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&U2);
			MatSetOption(U2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

		for (i=0; i<ni; i++) {
				for (j=0; j<nj; j++) {
					for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);

				/*	if(domain_optim[i0]==0 && j>21){
						lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;
					}
					if(domain_optim[i0]==0 && j<=21){
						lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;
					}
					if(domain_optim[i0]==1){*/
					lambda[i0]=lambda_vide+PetscPowScalar(gamma_h[i0],penal)*(lambda_solide-lambda_vide);
					rho[i0]=rho_vide+PetscPowScalar(gamma_h[i0],penal)*(rho_solide-rho_vide);
					cp[i0]=cp_vide+PetscPowScalar(gamma_h[i0],penal)*(cp_solide-cp_vide);
					diff[i0]=lambda[i0]/(rho[i0]*cp[i0]);
					c[i0]=c_vide+PetscPowScalar(gamma_h[i0],penal)*(c_solide-c_vide);
			//	}
				}
			}
		}

		// Loop over elements
		for (i=0; i<ni; i++) {
				for (j=0; j<nj; j++) {
					for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);
				//	if(domain_optim[i0]==0){lambda[i0]=lambda_vide; rho[i0]=rho_vide;cp[i0]=cp_vide;}
					edofMat_f(i,j,k,ni,nj,nk,edofMat);

					for (l=0;l<8*8;l++){
					ke[l]=KE[l]*(double)lambda[i0]/(delta_y*delta_x*delta_z);
					me[l]=ME[l]*(double)c[i0]/delta_t;
							}
				// Add values to the sparse matrix
				ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
				ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
				ierr = MatSetValues(K2,8,edofMat,8,edofMat,ke,ADD_VALUES);
				ierr = MatSetValues(M2,8,edofMat,8,edofMat,me,ADD_VALUES);
			//	}

				}
			}
		}

			ierr=MatAssemblyBegin(K2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(K2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyBegin(M2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(M2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


			// Loop over elements
			for (i=0; i<ni; i++) {
					for (j=0; j<nj; j++) {
						for (k=0; k<nk; k++) {
					i0=I3D(ni,nj,i,j,k);

						/*if(boundary[i0]==1){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE1[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
					}*/

						if(boundary[i0]==2){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE2[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==3){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE3[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==4){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE4[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==5){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++) ke[l]=HE5[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==6){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE6[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}
				}
			}
		}
			ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

		//DIRICHLET BOUNDARY CONDITION
			cpt=0;
			for (i=0; i<ni; i++) {
			 for (j=0; j<nj; j++) {
				 for (k=0; k<nk; k++) {
			 i0=I3D(ni,nj,i,j,k);

					 if(boundary[i0]==1){
						 cpt=cpt+1;
					 }
				 }
			 }
		 }

		 PetscInt *edofMat2;
		 int nb_BC;
		 nb_BC=cpt;
		PetscMalloc1(8*nb_BC,&edofMat2);
		IS is;
		PetscInt n1,n2;
		cpt=0;
		for (i=0; i<ni; i++) {
		 for (j=0; j<nj; j++) {
			 for (k=0; k<nk; k++) {
		 i0=I3D(ni,nj,i,j,k);

				 if(boundary[i0]==1){
					 value=1;
					 edofMat2[8*cpt]=i+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat2[1+8*cpt]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat2[2+8*cpt]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat2[3+8*cpt]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat2[4+8*cpt]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat2[5+8*cpt]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat2[6+8*cpt]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
					 edofMat2[7+8*cpt]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
					 	cpt=cpt+1;
				 }
			 }
			}
		}

		ISCreateGeneral(PETSC_COMM_SELF,8*nb_BC,edofMat2,PETSC_COPY_VALUES,&is);
		 ISGetLocalSize(is,&n1);
		ISSortRemoveDups(is);
		 ISGetLocalSize(is,&n2);

		  const PetscInt *nindices;
			ISGetIndices(is,&nindices);

		 printf("n1=%d et n2=%d \n",n1,n2);

		ierr = MatZeroRowsColumns(K1,n2,nindices,1,0,0);
		ierr = MatZeroRowsColumns(M1,n2,nindices,1,0,0);


			//Addition de M et K
			ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
			ierr=MatDuplicate(M2,MAT_COPY_VALUES,&U2);CHKERRQ(ierr);
			ierr=MatAXPY(U1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
			ierr=MatAXPY(U2,-1,K2,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);


	printf("OPTIMIZATION PROBLEM - BEGINNING \n ");

int compteur=0;
FILE *fichier_compliance;
fichier_compliance=fopen("compliance_T.txt","w");
int jc;
jc=0;
double lambda1;

	while(compteur<nbsteps){


/////////// Load //////////////

for (i=0; i<(ni+1); i++) {
		for (j=0; j<(nj+1); j++) {
			for (k=0; k<nk; k++) {
		i0=I3D(ni2,nj2,i,j,k);
		pos_q1[i0]=0;

		}
	}
}

cpt=0;
	for (i=0; i<ni; i++) {
			for (j=0+q1[compteur]; j<5+q1[compteur]; j++) {
				for (k=0; k<nk; k++) {
			i0=I3D(ni,nj,i,j,k);
				/*lambda1=0.1;
				if(j<(5+q1[compteur])) lambda1=1;
				if(j>(10+q1[compteur])) lambda1=0.01;*/

				if(domain_optim[i0]==0){
			edofMat_f(i,j,k,ni,nj,nk,edofMat);
			pos_q1[edofMat[0]]=1;
			pos_q1[edofMat[1]]=1;
			pos_q1[edofMat[2]]=1;
			pos_q1[edofMat[3]]=1;
			pos_q1[edofMat[4]]=1;
			pos_q1[edofMat[5]]=1;
			pos_q1[edofMat[6]]=1;
			pos_q1[edofMat[7]]=1;
			cpt=cpt+1;
		/*	for (k=0;k<4*4;k++){
			ke[k]=KE[k]*lambda_solide;
			me[k]=ME[k]*c_solide*delta_x*delta_y/delta_t;
							}
		// Add values to the sparse matrix
		ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
		ierr = MatSetValues(K1,4,edofMat,4,edofMat,me,ADD_VALUES);*/

		}
			}
	}
}
	num_elem[compteur]=cpt;
	printf("num_elem=%d\n",num_elem[compteur]);

/*	ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);*/

	//ierr=MatAXPY(M1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

	//Prod mat vecteur
	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);


printf("Load \n");

for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		for (k=0; k<nk; k++) {
	i0=I3D(ni,nj,i,j,k);
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

	/*	if(num_elem[compteur]>0){
			F[edofMat[0]]=pos_q1[edofMat[0]]*1000.f/(num_elem[compteur]*delta_x*delta_y);//
			F[edofMat[1]]=pos_q1[edofMat[1]]*1000.f/(num_elem[compteur]*delta_x*delta_y);//
			F[edofMat[2]]=pos_q1[edofMat[2]]*1000.f/(num_elem[compteur]*delta_x*delta_y);//
			F[edofMat[3]]=pos_q1[edofMat[3]]*1000.f/(num_elem[compteur]*delta_x*delta_y);//
		}
		else{
			F[edofMat[0]]=0;//
			F[edofMat[1]]=0;//
			F[edofMat[2]]=0;//
			F[edofMat[3]]=0;//
		}*/
		F[edofMat[0]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[1]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[2]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[3]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[4]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[5]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[6]]=(double)10/(delta_x*delta_y*delta_z);//
		F[edofMat[7]]=(double)10/(delta_x*delta_y*delta_z);//

}
}
}

printf("Complete load vector \n");

//Normal node
for (i=0; i<ndof; i++) {
	value=F[i];
	ierr = VecSetValues(b,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);

}
//VecView(b,PETSC_VIEWER_STDOUT_SELF);
//getchar();

double conv;
//Dirichlet condition

for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		for (k=0; k<nk; k++) {
	i0=I3D(ni,nj,i,j,k);

/*	if(boundary[i0]==1){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

		ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}*/

	if(boundary[i0]==2){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

		ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}

	if(boundary[i0]==3){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

		ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}

	if(boundary[i0]==4){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

		ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}

	if(boundary[i0]==5){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

		ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}

	if(boundary[i0]==6){
	edofMat_f(i,j,k,ni,nj,nk,edofMat);

		conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

		ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

	}

			if(boundary[i0]==1){
				edofMat_f(i,j,k,ni,nj,nk,edofMat);
				ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
			}
}
}
}

VecAXPY(b,1,F1);

end1=clock();


 //Set exact solution; then compute right-hand-side vector.
printf("Solver beginning \n");
//ierr = VecSet(x,0);CHKERRQ(ierr);

ierr = solver.setA(U1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
int         iters;



for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	Told[i]=y;
}



cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
T_transient[i]=x1[cpt];
pos_q1_transient[i]=pos_q1[cpt];
cpt=cpt+1;
}

somme=0;
//Fonction cout
for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		for (k=0; k<nk; k++) {
	i0=I3D(ni,nj,i,j,k);
edofMat_f(i,j,k,ni,nj,nk,edofMat);
PetscScalar uKu=0.0;
for (PetscInt k=0;k<8;k++){
for (PetscInt h=0;h<8;h++){
	uKu += x1[edofMat[k]]*KE[k*8+h]*x1[edofMat[h]];
}
}
somme=somme+lambda[i0]*uKu;
//gradient[i0]=-penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu;
}
}
}

compliance[compteur]=somme;
fprintf(fichier_compliance,"%f\n",somme);
//printf("fonction cout=%f\n",fonction_cout[k2_it]);

/*if(compteur%100==0){//*nbsteps/100){
	FILE *fichier_T1;
	char filename[200],u2[200];
	strcpy(filename,"T");	sprintf(u2,"%d",cpt4);	strcat(filename,u2); 	fichier_T1=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
	for (i=0; i<ndof; i++) {
	fprintf(fichier_T1,"%f\n",x1[i]);
	}
fclose(fichier_T1);
cpt4=cpt4+1;
printf("cpt4=%d\n",cpt4);
}*/

compteur=compteur+1;
printf("compteur=%d\n",compteur);
} //Fin boucle transient



//double diff_time;




fclose(fichier_compliance);

//Fonction cout

somme=0;
somme2=0;
for (i=0;i<nbsteps;i++) {
	somme=somme+compliance[i]*pow(alpha,compliance[i]);
	somme2=somme2+pow(alpha,compliance[i]);
}

fonction_cout[k2_it]=somme/somme2;

//Probleme ADJOINT
//ierr = MatDestroy(&K);CHKERRQ(ierr);

double C10,C11,C12,*ksi;
ksi=(double *)calloc(nbsteps,sizeof(double));

somme1=0;
somme2=0;
for (i=0;i<nbsteps;i++){
	somme1=somme1+pow(alpha,compliance[i]);
	somme2=somme2+compliance[i]*pow(alpha,compliance[i]);
}
C10=1.f/(somme1*somme1);
C11=somme1;
C12=somme2;

for (i=0;i<nbsteps;i++){
ksi[i]=C10*(C11*(1+compliance[i]*log(alpha))-C12*log(alpha))*pow(alpha,compliance[i]);
printf("ksi[%d]=%f\n",i,ksi[i]);
}
//getchar();
ierr = VecSet(x,0);CHKERRQ(ierr);
compteur=10000;
while(compteur<nbsteps){

for(i=0;i<ndof;i++){
	value=T_transient[(nbsteps-1-compteur)*ndof+i];
	ierr=VecSetValues(T,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);
}

ierr=MatMult(K2,T,b);CHKERRQ(ierr);
ierr=VecScale(b,2*ksi[nbsteps-1-compteur]); CHKERRQ(ierr);

ierr=MatMult(M2,x,F2);CHKERRQ(ierr);

//Dirichlet condition
/*for(i=0;i<nb_dirichlet;i++){
	j=dirichlet[i]; //printf("j=%d\n",j);
	edofMat[0]=j;
	//edofMat[1]=i0+ni+1;
	//edofMat[2]=i0+ni+2;
	//edofMat[3]=i0+1;
	ierr = VecSetValues(b,1,edofMat,&Tp,INSERT_VALUES);CHKERRQ(ierr);
}*/

ierr=VecAXPY(b,1,F2);CHKERRQ(ierr);

/*for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		i0=I2D(ni,i,j);
if(boundary[i0]>0){
edofMat_f(i,j,k,ni,nj,nk,edofMat);
	ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
	ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
	ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
	ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
}
}
}*/

printf("Solver beginning \n");
//ierr = VecSet(x,0);CHKERRQ(ierr);
ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);
ierr = solver.setA(U2); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
int         iters;
solver.finalize();

for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	}

cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
adjoint_transient[i]=x1[cpt];
cpt=cpt+1;
}

	compteur=compteur+1;
	printf("compteur=%d\n",compteur);
	} //Fin boucle transient


//printf("nbsteps=%d\n",nbsteps);
for(i=0;i<ndof;i++){
	for(j=0;j<nbsteps;j++){
		T_transient2[j*ndof+i]=ksi[j]*T_transient[j*ndof+i]+adjoint_transient[(nbsteps-1-j)*ndof+i];
		//if(i==5+(101*5)) printf("T_transient2=%lg\n",T_transient2[j*ndof+i]);
		if(j==0) derivee_T_transient[j*ndof+i]=0;
		if(j>0) derivee_T_transient[j*ndof+i]=(T_transient[j*ndof+i]-T_transient[(j-1)*ndof+i]);///delta_t;
		//printf("j=%d\n",j);
	}
}

FILE *somme_gradient,*somme_uKu1,*somme_uKu2;
somme_gradient=fopen("somme_gradient.txt","w");
somme_uKu1=fopen("uKu1.txt","w");
somme_uKu2=fopen("uKu2.txt","w");


	//gradient
	printf("gradient\n");
	for (i=0; i<ni; i++) {
		for (j=0; j<nj; j++) {
			for (k=0; k<nk; k++) {
		i0=I3D(ni,nj,i,j,k);
			if(domain_optim[i0]==1){
edofMat_f(i,j,k,ni,nj,nk,edofMat);
			compteur=0;
			somme=0;
			while(compteur<nbsteps){
		PetscScalar uKu1=0.0;
		PetscScalar uKu2=0.0;
	for (PetscInt k=0;k<8;k++){
	for (PetscInt h=0;h<8;h++){
		uKu1 += T_transient2[compteur*ndof+edofMat[k]]*KE[k*4+h]*T_transient[compteur*ndof+edofMat[h]];
		uKu2 += adjoint_transient[(nbsteps-1-compteur)*ndof+edofMat[k]]*ME[k*4+h]*(delta_x*delta_y/delta_t)*derivee_T_transient[compteur*ndof+edofMat[h]];
	}
	}
	somme=somme+penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu1
	+penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu2;

	//if(i==5 && j==5){
//	if(compteur<120 && compteur >80){
		//printf("compteur=%d, somme gradient=%lg, uKu1=%lg, uKu2=%lg, derivee_transient=%lg, adjoint_transient=%lg, ksi=%f\n",compteur,penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu1
		//+penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu2,uKu1,uKu2,derivee_T_transient[compteur*ndof+i+(ni+1)*j],adjoint_transient[(nbsteps-1-compteur)*ndof+i+(ni+1)*j],ksi[compteur]);
		//fprintf(somme_gradient,"%lg\n",penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu1
		//+penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu2);
	//	fprintf(somme_uKu1,"%lg\n",penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu1);
		//fprintf(somme_uKu2,"%lg\n",penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu2);

	//}
//}
	compteur=compteur+1;
}
	gradient[i0]=-somme/nele;
//printf("somme=%f\n",somme);

}

}
	}
}


//getchar();


fclose(somme_gradient);
fclose(somme_uKu1);
fclose(somme_uKu2);




//Fonction cout

/*somme=0;
somme2=0;
for (i=0;i<nbsteps;i++) {
	somme=somme+compliance[i]*pow(alpha,compliance[i]);
	somme2=somme2+pow(alpha,compliance[i]);
}*/



//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

/*for (i=0;i<nele;i++){
	Told[i]=T0;
}

//Probleme ADJOINT
ierr = MatDestroy(&K);CHKERRQ(ierr);

double C10,C11,C12,*ksi;
ksi=(double *)calloc(nbsteps,sizeof(double));

somme1=0;
somme2=0;
for (i=0;i<nbsteps;i++){
	somme1=somme1+pow(alpha,compliance[i]);
	somme2=somme2+compliance[i]*pow(alpha,compliance[i]);
}
C10=1.f/(somme1*somme1);
C11=somme1;
C12=somme2;

for (i=0;i<nbsteps;i++){
ksi[i]=C10*(C11*(1+compliance[i]*log(alpha))-C12*log(alpha))*pow(alpha,compliance[i]);
printf("ksi[%d]=%f\n",i,ksi[i]);
}

MatCreate(PETSC_COMM_WORLD,&K);
MatSetSizes(K,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
MatSetFromOptions(K);

MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&K);
MatSetOption(K,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

// Loop over elements
for (i=1; i<ni-1; i++) {
		for (j=1; j<nj-1; j++) {
			i0=I2D(ni,i,j);


			coeff_center_adjoint=-2*delta_t*diff[i0]/(delta_x*delta_x)-2*delta_t*diff[i0]/(delta_y*delta_y);
			coeff_center2_adjoint=1;//1.f/delta_t;//+2*diff[i0]/(delta_x*delta_x)+2*diff[i0]/(delta_y*delta_y);
			coeff_left_adjoint=delta_t*diff[i0]/(delta_x*delta_x);
			coeff_right_adjoint=delta_t*diff[i0]/(delta_x*delta_x);
			coeff_top_adjoint=delta_t*diff[i0]/(delta_y*delta_y);
			coeff_bottom_adjoint=delta_t*diff[i0]/(delta_y*delta_y);

			//printf("coeff_center=%f, coeff_left=%f, coeff_right=%f, coeff_top=%f, coeff_bottom=%f\n",coeff_center,coeff_left,coeff_right,coeff_top,coeff_bottom);

			i0_left=i0-1;
			i0_right=i0+1;
			i0_bottom=i0-ni;
			i0_top=i0+ni;

			ierr = MatSetValues(K,1,&i0,1,&i0,&coeff_center_adjoint,INSERT_VALUES);	CHKERRQ(ierr);
			ierr = MatSetValues(K,1,&i0,1,&i0_left,&coeff_left_adjoint,INSERT_VALUES);	CHKERRQ(ierr);
			ierr = MatSetValues(K,1,&i0,1,&i0_right,&coeff_right_adjoint,INSERT_VALUES);	CHKERRQ(ierr);
			ierr = MatSetValues(K,1,&i0,1,&i0_bottom,&coeff_bottom_adjoint,INSERT_VALUES);	CHKERRQ(ierr);
			ierr = MatSetValues(K,1,&i0,1,&i0_top,&coeff_top_adjoint,INSERT_VALUES);	CHKERRQ(ierr);
		}
	}

	cpt1=0;
	cpt2=0;
	cpt3=0;

		for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				i0=I2D(ni,i,j);

					if(boundary[i0]==1){
					dirichlet[cpt1]=i0;
					value=1; //est ce au'il faut mettre -1, dirichlet est a zero donc on s'en fiche peut etre
					ierr=VecSetValues(D,1,&i0,&value,INSERT_VALUES);CHKERRQ(ierr);
					cpt1=cpt1+1;
				}

				if(boundary[i0]==2){
					convection[cpt2]=i0;
					i0_left=i0-1;
					i0_right=i0+1;
					i0_bottom=i0-ni;
					i0_top=i0+ni;
					if(i==0)    {i0_side=i0_right; convection_delta[cpt2]=delta_x;}
					if(i==ni-1) {i0_side=i0_left; convection_delta[cpt2]=delta_x;}
					if(j==0)    {i0_side=i0_top; convection_delta[cpt2]=delta_y;}
					if(j==nj-1) {i0_side=i0_bottom; convection_delta[cpt2]=delta_y;}
					//printf("cpt2=%d, convection_delta[cpt2]=%f\n",cpt2,convection_delta[cpt2]);
					coeff_conv_center=(1+h*convection_delta[cpt2]/lambda[i0]);
					coeff_conv_side=-1;
					//printf("coeff_conv_center=%f\n",coeff_conv_center);
					//printf("coeff_conv_side=%f\n",coeff_conv_side);
					ierr = MatSetValues(K,1,&i0,1,&i0,&coeff_conv_center,INSERT_VALUES);	CHKERRQ(ierr);
					ierr = MatSetValues(K,1,&i0,1,&i0_side,&coeff_conv_side,INSERT_VALUES);	CHKERRQ(ierr);
					cpt2=cpt2+1;
				}

				if(boundary[i0]==3){
					heatflux[cpt3]=i0;
					i0_left=i0-1;
					i0_right=i0+1;
					i0_bottom=i0-ni;
					i0_top=i0+ni;
					//printf("i0_left=%d, i0_right=%d, i0_top=%d, i0_bottom=%d \n",i0_left,i0_right,i0_top,i0_bottom);
					if(i==0)    {i0_side=i0_right; heatflux_delta[cpt3]=delta_x;}
					if(i==ni-1) {i0_side=i0_left; heatflux_delta[cpt3]=delta_x;}
					if(j==0)    {i0_side=i0_top; heatflux_delta[cpt3]=delta_y;}
					if(j==nj-1) {i0_side=i0_bottom; heatflux_delta[cpt3]=delta_y;}
					//printf("i=%d, j=%d, i0=%d, i0_side=%d\n",i,j,i0,i0_side);
					coeff_flux_center=2*delta_t*diff[i0]/(delta_x*delta_x);//-2*delta_t*diff[i0]/(delta_y*delta_y);
					coeff_flux_side=-2*delta_t*diff[i0]/(delta_x*delta_x);//+2*delta_t*diff[i0]/(delta_y*delta_y);
					ierr = MatSetValues(K,1,&i0,1,&i0,&coeff_flux_center,INSERT_VALUES);	CHKERRQ(ierr);
					ierr = MatSetValues(K,1,&i0,1,&i0_side,&coeff_flux_side,INSERT_VALUES);	CHKERRQ(ierr);
					cpt3=cpt3+1;
				}

			}
		}

	ierr=MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

ierr=MatDiagonalSet(K,D,ADD_VALUES);


compteur=0;

	while(compteur<nbsteps){

printf("Load adjoint \n");


double *prod_KU;
prod_KU=(double *)calloc(ndof,sizeof(double));

for (i=1; i<(ni-1); i++) {
	for (j=1; j<(nj-1); j++) {
		i0=I2D(ni,i,j);
		i0_left=i0-1;
		i0_right=i0+1;
		i0_bottom=i0-ni;
		i0_top=i0+ni;

		//prod_KU[i0]=T_transient[compteur*ndof+i0]*(lambda[i0]+lambda[i0-1]+lambda[i0+1]+lambda[i0-ni]+lambda[i0-ni]);
		prod_KU[i0]=+(lambda[i0]/(delta_x*delta_x)+lambda[i0]/(delta_y*delta_y))*
		(T_transient[(nbsteps-1-compteur)*ndof+i0]+
		 T_transient[(nbsteps-1-compteur)*ndof+i0_bottom]+
		 T_transient[(nbsteps-1-compteur)*ndof+i0_top]+
		 T_transient[(nbsteps-1-compteur)*ndof+i0_left]+
		 T_transient[(nbsteps-1-compteur)*ndof+i0_right]);//-T_transient[(nbsteps-1-compteur)*ndof+i0]*(lambda[i0]+lambda[i0-1]+lambda[i0+1]+lambda[i0-ni]+lambda[i0-ni])/(delta_x*delta_x);

		}
	}

for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		i0=I2D(ni,i,j);
		i0_left=i0-1;
		i0_right=i0+1;
		i0_bottom=i0-ni;
		i0_top=i0+ni;

			if(boundary[i0]==1) prod_KU[i0]=+(lambda[i0]/(delta_x*delta_x)+lambda[i0]/(delta_y*delta_y))*T_transient[(nbsteps-1-compteur)*ndof+i0];//coeff_center_adjoint*T_transient[compteur*ndof+i0];
			if(boundary[i0]==2 || boundary[i0]==3){
				if(i==0)    {i0_side=i0_right; delta=delta_x;}
				if(i==ni-1) {i0_side=i0_left; delta=delta_x;}
				if(j==0)    {i0_side=i0_top; delta=delta_y;}
				if(j==nj-1) {i0_side=i0_bottom; delta=delta_y;}
				//coeff_side_adjoint=diff[i0_side]/(delta*delta);
				prod_KU[i0]=+(lambda[i0]/(delta_x*delta_x)+lambda[i0]/(delta_y*delta_y))*(T_transient[(nbsteps-1-compteur)*ndof+i0]+T_transient[(nbsteps-1-compteur)*ndof+i0_side]);
			}
		}
	}

for (i=0; i<ni; i++) {
		for (j=0; j<nj; j++) {
			i0=I2D(ni,i,j);

			F[i0]=Told[i0]+2*delta_t*ksi[compteur]*prod_KU[i0]/c[i0];
			//printf("prod_KU[i0]=%f and ksi[compteur]=%f\n",prod_KU[i0],ksi[compteur]);
}
}

printf("Complete load vector \n");

//Normal node
for (i=0; i<nele; i++) {
	value=F[i];
	ierr = VecSetValues(b,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);

}

ierr = VecSet(x,0);CHKERRQ(ierr);
ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);
ierr = solver.setA(K); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
int         iters;
solver.finalize();

//VecView(x,PETSC_VIEWER_STDOUT_SELF);

for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	Told[i]=y;
}



int cpt;
cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
adjoint_transient[i]=x1[cpt];
prod_KU_transient[i]=2*ksi[compteur]*prod_KU[cpt]/c[cpt];
second_membre_transient[i]=coeff_center2_adjoint*Told[cpt];
cpt=cpt+1;
}



compteur=compteur+1;
printf("compteur=%d\n",compteur);
}*/

/*printf("Ecriture des fichiers transients \n");

FILE *fichier_adjoint;
fichier_adjoint=fopen("adjoint_transient.txt","w");
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_adjoint,"%f\n",adjoint_transient[i]);
fclose(fichier_adjoint);


FILE *fichier_second_membre;
fichier_second_membre=fopen("T_transient.txt","w");
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_second_membre,"%f\n",T_transient[i]);
fclose(fichier_second_membre);


fichier_second_membre=fopen("pos_q1_transient.txt","w");
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_second_membre,"%f\n",pos_q1_transient[i]);
fclose(fichier_second_membre);*/

//Calcul du gradient
printf("derivees \n");

/*compteur=1; // a 1 pour la derivee temporelle
for (PetscInt k=0;k<ndof;k++){
		derivee_lambda[k]=penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1));
		derivee_c[k]=penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1));
}*/

/*printf("Calcul du gradient \n");
int k;
for (i=1;i<(ni-1);i++){
	for (j=1;j<(nj-1);j++){
		k=I2D(ni,i,j);
	PetscScalar uKu=0.0;
	PetscScalar uKu2=0.0;
	//compteur=1;
	compteur=0;
	somme=0;*/
/*	while(compteur<nbsteps){
		lu[k]=ksi[compteur]*T_transient[compteur*ndof+k]+adjoint_transient[compteur*ndof+k];
		derivee_T[compteur*ndof+k]=0;//(T_transient[compteur*ndof+k]-T_transient[(compteur-1)*ndof+k])/delta_t;

	uKu += lu[k]*derivee_lambda[k]*T_transient[compteur*ndof+k]
				+lu[k]*derivee_lambda[k-1]*T_transient[compteur*ndof+k-1]
				+lu[k]*derivee_lambda[k+1]*T_transient[compteur*ndof+k+1]
				+lu[k]*derivee_lambda[k-ni]*T_transient[compteur*ndof+k-ni]
				+lu[k]*derivee_lambda[k+ni]*T_transient[compteur*ndof+k+ni];

	uKu2 += adjoint_transient[compteur*ndof+k]*derivee_c[k]*derivee_T[compteur*ndof+k]
				+ adjoint_transient[compteur*ndof+k]*derivee_c[k-1]*derivee_T[compteur*ndof+k-1]
				+ adjoint_transient[compteur*ndof+k]*derivee_c[k+1]*derivee_T[compteur*ndof+k+1]
				+ adjoint_transient[compteur*ndof+k]*derivee_c[k-ni]*derivee_T[compteur*ndof+k-ni]
				+ adjoint_transient[compteur*ndof+k]*derivee_c[k+ni]*derivee_T[compteur*ndof+k+ni];

	//printf("uku=%f et uKu2=%f \n",uKu,uKu2);

	somme=somme+uKu+uKu2;
	compteur++;
}*/

//uKu=(4*T_transient[k]-T_transient[k-1]-T_transient[k-ni]-T_transient[k+1]-T_transient[k+ni])*T_transient[k];//-T_transient[k-ni]*T_transient[k-ni]-T_transient[k-1]*T_transient[k-1]-T_transient[k+1]*T_transient[k+1]-T_transient[k+ni]*T_transient[k+ni];

 //gradient[k]=uKu;//penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[k],(penal-1))*uKu;//somme;

 /*if(i==50 && j==20){
	 printf("i=20 et j=50\n");
	 printf("uKu=%f et T_transient[k]=%f , T_transient[k-ni]=%f , T_transient[k-1]=%f , T_transient[k+1]=%f , T_transient[k+ni]=%f \n",uKu, T_transient[k],T_transient[k-ni],T_transient[k-1],T_transient[k+1],T_transient[k+ni]);
	 //printf("somme=%lg\n",somme);
 }

 if(i==50 && j==80){
	 printf("i=80 et j=50\n");
		 printf("uKu=%f et T_transient[k]=%f , T_transient[k-ni]=%f , T_transient[k-1]=%f , T_transient[k+1]=%f , T_transient[k+ni]=%f \n",uKu, T_transient[k],T_transient[k-ni],T_transient[k-1],T_transient[k+1],T_transient[k+ni]);
	 printf("somme=%lg\n",somme);
 }*/

//}
//}

/*FILE *fichier_gradient;
fichier_gradient=fopen("gradient","w");
for (i=0; i<nele; i++) {
fprintf(fichier_gradient,"%lg\n",gradient[i]);
//printf("%lg\n",gradient[i]);
}
fclose(fichier_gradient);*/

FILE *fichier_T;
strcpy(filename,"h012_T_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_T=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) fprintf(fichier_T,"%lg\n",T_transient[(int)(0.5*(nbsteps-1))*ndof+i]);
fclose(fichier_T);

strcpy(filename,"h012_adjoint_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_T=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) fprintf(fichier_T,"%lg\n",adjoint_transient[(int)(0.5*(nbsteps-1))*ndof+i]);
fclose(fichier_T);

// Set outer move limits
printf("MMA limits \n");

for (i=0;i<nele;i++) {
	xmax[i] = Min(Xmax, xmma[i] + movlim); //printf("xmax=%f\n", xmax[i]);
	xmin[i] = Max(Xmin, xmma[i] - movlim); //printf("xmin=%f\n", xmin[i]);
}

somme=0;
for (i=0; i<nele; i++) {
	df[i]=-gradient[i];
	dg[i]=1;
	somme=somme+gamma_h[i];
}

g[0]=volfrac-somme/(nele);
printf("g0=%f\n",g[0]);

printf("Spatial filter \n");
filtre(ni,nj,nk,rmin,gamma_h,df,df2,domain_optim);


/*for (i=0; i<nele; i++) {
	if(domain_optim[i]==0) df2[i]=mingrad;
	df2[i]=(double) -df2[i]/mingrad;
}*/



printf("MMA update \n");
OC(ni, nj, nk, df2, gamma_h, movlim, volfrac,domain_optim);

filtre2(ni,nj,nk,rmin,gamma_h,gamma_x,domain_optim);

for (i=0; i<nele; i++) {
	gamma_h[i]=gamma_x[i];
}

//mma->Update(xmma,df,g,dg,xmin,xmax);

//printf("minigrad=%f\n",mingrad);

/*for (i=0;i<nele;i++) {
	xold[i] = xmma[i];
	gamma_h[i]=xmma[i];
}*/

FILE *temp_ijk_0, *temp_ijk_05, *temp_ijk_1;

temp_ijk_0=fopen("temp_ijk_0.txt","w");
temp_ijk_05=fopen("temp_ijk_05.txt","w");
temp_ijk_1=fopen("temp_ijk_1.txt","w");

for (i=0; i<(ni+1); i++) {
	for (j=0; j<(nj+1); j++) {
		for (k=0; k<(nk+1); k++) {
	i0=I3D(ni2,nj2,i,j,k);
	fprintf(temp_ijk_0,"%d %d %d %f %f \n",i,j,k,T_transient[i0],T_transient[i0]);
	fprintf(temp_ijk_05,"%d %d %d %f %f \n",i,j,k,T_transient[(int)(0.5*(nbsteps-1))*ndof+i0],T_transient[(int)(0.5*(nbsteps-1))*ndof+i0]);
	fprintf(temp_ijk_1,"%d %d %d %f %f \n",i,j,k,T_transient[(int)((nbsteps-1))*ndof+i0],T_transient[(int)((nbsteps-1))*ndof+i0]);
}
}
}

fclose(temp_ijk_0);
fclose(temp_ijk_05);
fclose(temp_ijk_1);

FILE *fichier_gamma;

strcpy(filename,"h012_gamma_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_gamma=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<nele;i++) fprintf(fichier_gamma,"%f\n",gamma_h[i]);
fclose(fichier_gamma);

strcpy(filename,"h012_gradient_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_gamma=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<nele;i++) fprintf(fichier_gamma,"%lg\n",df2[i]);
fclose(fichier_gamma);

FILE *fichier_fonction_cout;
strcpy(filename,"h012_fonction_cout_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_fonction_cout=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
//fichier_fonction_cout=fopen("fonction_cout","w");

for (i=0;i<=k2_it;i++) fprintf(fichier_fonction_cout,"%f\n",fonction_cout[i]);

fclose(fichier_fonction_cout);


ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);
ierr = MatDestroy(&K2);CHKERRQ(ierr);
ierr = MatDestroy(&M2);CHKERRQ(ierr);
} //Fin boucle optim

printf("Fin boucle optim \n");

printf("assembly time=%ld\n",(end1-start1)/CLOCKS_PER_SEC);

solver.finalize();

ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);
//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);
return ierr;
}
