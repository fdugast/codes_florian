#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <omp.h>

#include <MMASolver.h>
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define MNE 8 //8 degrees of freedom per element : hexahedral 4 node element with two degrees of freedom per node

#define BLOCKSIZE1 32
#define BLOCKSIZE2 32

//#define MAX(x, y) (((x) > (y)) ? (x) : (y))

#define I2D(ni,i,j) (((ni)*(j)) + i)

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

//Elastic contribution in the element stiffness matrix
void KE_elasticity(double delta_x, double delta_y, double *KE, double E, double nu, double *C){

	double wg[3][3],wxyz;
	int i,j,i1,j1,k3,cpt,l,i3;
	double x,y,a,b;
	double sommeK,somme_k,somme_l;

  double DN[2][4];
  double B[3][8];
  double gxyz[3][3];

  //1 point
  gxyz[0][0]=0;
  	wg[0][0]=2;

  //2 points
  gxyz[0][1]=-0.577350269189626;
  gxyz[1][1]=0.577350269189626;
  	wg[0][1]=1;
    wg[1][1]=1;

  //3pts
  gxyz[0][2]=-0.774596669241483;
  gxyz[1][2]=0;
  gxyz[2][2]=0.774596669241483;

	wg[0][2]=0.555555555555556;
	wg[1][2]=0.888888888888889;
	wg[2][2]=0.555555555555556;

a=0.5*delta_x;
b=0.5*delta_y;


	for (i1=0;i1<MNE;i1++){
			for (j1=0;j1<MNE;j1++){

sommeK=0;
cpt=0;
for (i=0;i<2;i++){
	for (j=0;j<2;j++){


			//x=a1*gxyz[i][1];
			//y=b1*gxyz[j][1];
			//z=c1*gxyz[k][1];
			wxyz=wg[i][1]*wg[j][1];

      x=gxyz[i][1];
      y=gxyz[j][1];


      DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
      DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
      DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
      DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);


      for(i3=0;i3<4;i3++){
      B[0][0+2*i3]=DN[0][i3];   B[0][1+2*i3]=0;
      B[1][0+2*i3]=0; 			  	B[1][1+2*i3]=DN[1][i3];
      B[2][0+2*i3]=DN[1][i3]; 	B[2][1+2*i3]=DN[0][i3];

    }

										somme_k=0;
										for(k3=0;k3<3;k3++){
											somme_l=0;
											for(l=0;l<3;l++){
												//somme_l=somme_l+B[l][i1]*C[l+3*k3];
                        somme_l=somme_l+B[l][i1]*C[l+3*k3];
											}
											somme_k=somme_k+somme_l*B[k3][j1];
										}


								sommeK=sommeK+somme_k*wxyz*a*b;


								//printf("sommeK=%f\n",sommeK);
								cpt++;
							}

					}
					KE[i1+MNE*j1]=sommeK;
				//	printf("%.2lg ",KE[i1+24*j1]);
}

//printf("\n");

}

}


void KE_plasticity2(double delta_x, double delta_y,int numElems, double *rho,  double penal, double *C, double *D,
    double *BE1, double *BE2, double *BE3, double *BE4,
  bool *plasticity_elements,double *KE_full, int *map_plastic)
  {

	int e,m,i1,j1,k3,l;
	double a,b;
	double sommeK;

  double somme_l1,somme_l2,somme_l3,somme_l4;
  double somme_k1,somme_k2,somme_k3,somme_k4;

  double KE[8*8];

  a=0.5*delta_x;
  b=0.5*delta_y;

  double wg[3][3],wxyz1,wxyz2,wxyz3,wxyz4;

  wg[0][1]=1;
  wg[1][1]=1;

  wxyz1=wg[0][1]*wg[0][1];
  wxyz2=wg[1][1]*wg[0][1];
  wxyz3=wg[1][1]*wg[1][1];
  wxyz4=wg[0][1]*wg[1][1];


  int cpt1=0;

for (e=0;e<numElems;e++){

  for(m=0;m<8*8;m++) KE[m]=0;

  if(plasticity_elements[e]==1){

	for (i1=0;i1<8;i1++){
			for (j1=0;j1<8;j1++){

        somme_k1=0;   somme_k2=0;   somme_k3=0;   somme_k4=0;

  			for(k3=0;k3<3;k3++){

        somme_l1=0;   somme_l2=0;   somme_l3=0;   somme_l4=0;


				for(l=0;l<3;l++){
					somme_l1=somme_l1+BE1[8*l+i1]*(D[4*9*e+9*0+l+3*k3]-pow(rho[e],penal)*C[l+3*k3]);
          somme_l2=somme_l2+BE2[8*l+i1]*(D[4*9*e+9*1+l+3*k3]-pow(rho[e],penal)*C[l+3*k3]);
          somme_l3=somme_l3+BE3[8*l+i1]*(D[4*9*e+9*2+l+3*k3]-pow(rho[e],penal)*C[l+3*k3]);
          somme_l4=somme_l4+BE4[8*l+i1]*(D[4*9*e+9*3+l+3*k3]-pow(rho[e],penal)*C[l+3*k3]);
          								}

					somme_k1=somme_k1+somme_l1*BE1[8*k3+j1];
          somme_k2=somme_k2+somme_l2*BE2[8*k3+j1];
          somme_k3=somme_k3+somme_l3*BE3[8*k3+j1];
          somme_k4=somme_k4+somme_l4*BE4[8*k3+j1];
										}

					sommeK=a*b*(somme_k1*wxyz1+somme_k2*wxyz2+somme_k3*wxyz3+somme_k4*wxyz4);

					KE[i1+8*j1]=sommeK;

}

}

for (m=0;m<8*8;m++) KE_full[8*8*cpt1+m]=KE[m];
map_plastic[e]=cpt1;
cpt1++;
}
}
}

void KE_plasticity3(double delta_x, double delta_y,int numElems,  double *D,
    double *BE1, double *BE2, double *BE3, double *BE4,  double *KE_full)
  {

	int e,m,i1,j1,k3,l;
	double a,b;
	double sommeK;

  double somme_l1,somme_l2,somme_l3,somme_l4;
  double somme_k1,somme_k2,somme_k3,somme_k4;

  double KE[8*8];

  a=0.5*delta_x;
  b=0.5*delta_y;

  double wg[3][3],wxyz1,wxyz2,wxyz3,wxyz4;

  wg[0][1]=1;
  wg[1][1]=1;

  wxyz1=wg[0][1]*wg[0][1];
  wxyz2=wg[1][1]*wg[0][1];
  wxyz3=wg[1][1]*wg[1][1];
  wxyz4=wg[0][1]*wg[1][1];


  int cpt1=0;

for (e=0;e<numElems;e++){

  for(m=0;m<8*8;m++) KE[m]=0;


	for (i1=0;i1<8;i1++){
			for (j1=0;j1<8;j1++){

        somme_k1=0;   somme_k2=0;   somme_k3=0;   somme_k4=0;

  			for(k3=0;k3<3;k3++){

        somme_l1=0;   somme_l2=0;   somme_l3=0;   somme_l4=0;


				for(l=0;l<3;l++){
					somme_l1=somme_l1+BE1[8*l+i1]*D[4*9*e+9*0+l+3*k3];
          somme_l2=somme_l2+BE2[8*l+i1]*D[4*9*e+9*1+l+3*k3];
          somme_l3=somme_l3+BE3[8*l+i1]*D[4*9*e+9*2+l+3*k3];
          somme_l4=somme_l4+BE4[8*l+i1]*D[4*9*e+9*3+l+3*k3];
          								}

					somme_k1=somme_k1+somme_l1*BE1[8*k3+j1];
          somme_k2=somme_k2+somme_l2*BE2[8*k3+j1];
          somme_k3=somme_k3+somme_l3*BE3[8*k3+j1];
          somme_k4=somme_k4+somme_l4*BE4[8*k3+j1];
										}

					sommeK=a*b*(somme_k1*wxyz1+somme_k2*wxyz2+somme_k3*wxyz3+somme_k4*wxyz4);

					KE[i1+8*j1]=sommeK;

}

}

for (m=0;m<8*8;m++) KE_full[8*8*cpt1+m]=KE[m];

cpt1++;

}
}

//Strain displacement matrix on the different quadrature points
void BE_matrix2(double delta_x, double delta_y,
	double *BE1, double *BE2, double *BE3, double *BE4,
  double E, double nu){

	double DN[2][4];
	int i1,j1,i3,i,j;
	double a,b;
	double x,y;
	int cpt;

  double gxyz[2];

  //location for 2 quadrature points
  //One can use only 2 quadrature points because the shape functions are linear
  gxyz[0]=-0.577350269189626;
  gxyz[1]=0.577350269189626;


  a=0.5*delta_x;
  b=0.5*delta_y;


cpt=1;
for (i=0;i<2;i++){
  for (j=0;j<2;j++){

                    x=gxyz[i];
                    y=gxyz[j];


                    for (i1=0;i1<MNE;i1++){
                        for (j1=0;j1<3;j1++){

                          //Shape functions
                          //N[0]=(1-x)*(1-y)*(1-z)/8;
                          //N[1]=(1+x)*(1-y)*(1-z)/8;
                          //N[2]=(1+x)*(1+y)*(1-z)/8;
                          //N[3]=(1-x)*(1+y)*(1-z)/8;
                          //N[4]=(1-x)*(1-y)*(1+z)/8;
                          //N[5]=(1+x)*(1-y)*(1+z)/8;
                          //N[6]=(1+x)*(1+y)*(1+z)/8;
                          //N[7]=(1-x)*(1+y)*(1+z)/8;

                //Derivatives of shape functions
                //DN[i][0] derivative of shape functions N[i] with respect to x
                //DN[i][1] derivative of shape functions N[i] with respect to y
                //DN[i][2] derivative of shape functions N[i] with respect to z

                DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
                DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
                DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
                DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

                double B[8][3]={0};

                for(i3=0;i3<4;i3++){
                B[0+2*i3][0]=DN[0][i3];   B[1+2*i3][0]=0;
                B[0+2*i3][1]=0; 			  	B[1+2*i3][1]=DN[1][i3];
                B[0+2*i3][2]=DN[1][i3]; 	B[1+2*i3][2]=DN[0][i3];

              }


          //In the loops
          //for (i=0;i<2;i++){
          //  for (j=0;j<2;j++){
          //    for (k=0;k<2;k++){
          //      ...
          //    }
          //  }
          //}
          // the order is (i,j,k)= (0,0,0) -- (0,0,1) -- (0,1,0) -- (0,1,1) -- (1,0,0) -- ... and so on
          //It does not correspond to order that has been chosen to calculate the quadrature points.
          //For example (i,j,k) = (1,0,0) correspond to our second quadrature points but
          //it only appears at the fifth position in the loop, that is why we have something like
          // 	"if(cpt==5) BE2[24*j1+i1]=B[i1][j1];" to get that BE1 corresponds to the first quadrature point,
          // BE2 corresponds to the second quadrature points and so on

					if(cpt==1) BE1[MNE*j1+i1]=B[i1][j1];
					if(cpt==2) BE4[MNE*j1+i1]=B[i1][j1];
					if(cpt==3) BE2[MNE*j1+i1]=B[i1][j1];
					if(cpt==4) BE3[MNE*j1+i1]=B[i1][j1];

}

//printf("\n");

}
cpt=cpt+1;

//printf("j1=%d\n",j1);

}

}


}


//Calculation of the constitutuve elastic matrix
void matC(double E,double nu,double *C){
	double den;
/*	den=(double)E/((1+nu)*(1-2*nu));

	C[0]=den*(1-nu);
	C[1]=den*nu;
	C[0+3*1]=den*nu;
	C[1+3*1]=den*(1-nu);
	C[2+3*2]=den*0.5*(1-2*nu);*/

  	den=(double)E/(1-nu*nu);

  	C[0]=den;
  	C[1]=den*nu;
  	C[0+3*1]=den*nu;
  	C[1+3*1]=den;
  	C[2+3*2]=den*0.5*(1-nu);


}

//Matrix transpose
void transMat(int i1, int j1, double *original, double *trans){
	//printf("BE \n");
	int i,j;
	for(j=0;j<i1;j++){
		for(i=0;i<j1;i++){
			trans[i+j1*j]=original[j+i*i1];
			//printf("%f ",original[i+i1*j]);
		}
		//printf("\n");
	}
}

//Matrix vector product on CPU
void prodMatVec(int i1, int j1, int j2, double *Mat, double *Vec, double *product){
int j,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[k+i1*j]*Vec[k];
		}
		product[j]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

void prodVecMat(int i1, int i2, int j2, double *Vec, double *Mat, double *product){
int i,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (i=0;i<i2;i++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[i+i2*k]*Vec[k];
      		}
		product[i]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

void prodVecMat1(int i1, int i2, int j2, double *Vec, double *Mat, double *product, int e, int q){
int i,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (i=0;i<i2;i++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[i+i2*k]*Vec[k];
      if(i==0 && e==0 && q==0) printf("Mat=%lg, Vec=%lg,somme=%lg\n",Mat[i+i1*k],Vec[k],somme);
      		}
		product[i]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}


void prodMatMat(int i1, int j1, int i2, int j2, double *Mat1, double *Mat2, double *product){
int j,k,l;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
for (l=0;l<i2;l++){
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat1[k+i1*j]*Mat2[i2*k+l];
		}
		product[j*i2+l]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}
}

//Matrix vector product on CPU
void prodMatVec_e(int i1, int j1, int j2, double *Mat, double rho, double penal,double *Vec, double *product){
int j,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[k+i1*j]*pow(rho,penal)*Vec[k];
		}
		product[j]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

//Relation between node number and degrees of freedom for a voxel mesh
void edofMat_calculation(int id, int ni, int *edofMat){
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//x2
  edofMat[3]=id+3;//y2
  edofMat[4]=id+2+2*(ni+1);//x3
  edofMat[5]=id+3+2*(ni+1);//y3
  edofMat[6]=id+0+2*(ni+1); //x4
  edofMat[7]=id+1+2*(ni+1); //y4
}


int Min(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Max(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Mini(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Maxi(double d1,double d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filter(int ni, int nj, double rmin, double *x, double *df, double *df2)
{

	int i,j,i0,i02,i2,j2;
	double somme,somme2,fac;


	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){

			i02=I2D(ni,i,j);
				somme=0;
				somme2=0;
				for (i2 = Max(i-floor(rmin),0); i2 <= Min(i+floor(rmin),(ni-1)); i2++) {
					for (j2 = Max(j-floor(rmin),0); j2 <= Min(j+floor(rmin),(nj-1)); j2++) {

								i0=I2D(ni,i2,j2);
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Maxi(0,fac);
								somme2=somme2+Maxi(0,fac)*x[i0]*df[i0];

					}
				}


			if(x[i02]*somme>0) {
				df2[i02]=somme2/(x[i02]*somme);
			}
			else {
				df2[i02]=df[i02];
			}

		}
		}

}

void FE_internal_forces2(double delta_x, double delta_y,
  double *BE1, double *BE2, double *BE3, double *BE4,
  int numElems,  double *stress,double *FE_int){

	int e,i1,l;
	double a,b;
	double somme_l1,somme_l2,somme_l3,somme_l4;

a=0.5*delta_x;
b=0.5*delta_y;



for (e=0;e<numElems;e++){

  //if(active_elements[e]==1){

for (i1=0;i1<8;i1++){

          somme_l1=0;   somme_l2=0;   somme_l3=0;   somme_l4=0;


            for (l=0;l<3;l++){
              somme_l1=somme_l1+BE1[3*i1+l]*stress[4*3*e+3*0+l];
              somme_l2=somme_l2+BE2[3*i1+l]*stress[4*3*e+3*1+l];
              somme_l3=somme_l3+BE3[3*i1+l]*stress[4*3*e+3*2+l];
              somme_l4=somme_l4+BE4[3*i1+l]*stress[4*3*e+3*3+l];
            }

        FE_int[8*e+i1]=(somme_l1+somme_l2+somme_l3+ somme_l4)*a*b;

}

//}
}

}

void inversion(int m, double *original, double *inverse, int e){

int i,j,k,j1;
double max1,pivot,temp;
double *reference=(double *)calloc(m*m,sizeof(double));
double *original1=(double *)calloc(m*m,sizeof(double));

int r=0;

for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    inverse[j+m*i]=0;
    reference[j+m*i]=0;
    original1[j+m*i]=original[j+m*i];
    if(i==j) {
      inverse[j+m*i]=1;
      reference[j+m*i]=1;
    }
  }
}

for (j=0;j<m;j++){
  //recherche max
  max1=-1;
  for (i=r;i<m;i++){
    if(abs(original[j+m*i])>max1) { max1=abs(original[j+m*i]); k=i;}
  }

  if(original[j+m*k]!=0){
    pivot=original[j+m*k];

  for (j1=0;j1<m;j1++){
    original[j1+m*k]=(double)original[j1+m*k]/pivot;
    inverse[j1+m*k]=(double)inverse[j1+m*k]/pivot;
  }

  if(k!=r){
    for (j1=0;j1<m;j1++){
      temp=original[j1+m*k];  original[j1+m*k]=original[j1+m*r];  original[j1+m*r]=temp;
      temp=inverse[j1+m*k];   inverse[j1+m*k]=inverse[j1+m*r];  inverse[j1+m*r]=temp;
    }
  }


  for (i=0;i<m;i++){
    if(i!=r){
      pivot=original[j+m*i];
      //printf("pivot=%lg\n",pivot);
      for (j1=0;j1<m;j1++){
        original[j1+m*i]=original[j1+m*i]-original[j1+m*r]*pivot;
        inverse[j1+m*i]=inverse[j1+m*i]-inverse[j1+m*r]*pivot;
      }
    }
  }

  r++;

  }

}

//Verification
double difference=0;
for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    difference=difference+abs(original[m*i+j]-reference[m*i+j]);
  }
}


for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    original[m*i+j]=original1[m*i+j];
  }
}

if(difference>1e-6) {
  printf("issue with inversion : element %d : diff=%lg\n",e,difference);
  printf("original\n");
  for (i=0;i<m;i++){
    for (j=0;j<m;j++){
      printf("%lg ",original[m*i+j]);
    }
    printf("\n");
  }
  printf("inverse\n");
  for (i=0;i<m;i++){
    for (j=0;j<m;j++){
      printf("%lg ",inverse[m*i+j]);
    }
    printf("\n");
  }
}

}

//Adjoint and gradient functions
void dH_due(int qpoints, int e, int numElems, int l, double *C1, double *yphys, double *BE1, double *rho, double penal, double *dH_due_q1)
{

  int i;
  double mat1[24]={0};
  prodMatMat(3,3,8,3,C1,BE1,mat1);

  for(i=0;i<3*2*qpoints;i++){
  dH_due_q1[i]=pow(rho[e],penal)*yphys[l*numElems+e]*mat1[i];
  }
}

void dR_dwe(int l, int numElems, int q, int qpoints, int e, double *ID_mat, double *dn_ds,
  double hard_constant_e, double Ge, double weight1, double delta_x, double delta_y, double *BE1_t, double *dR_dwe_q1,
bool *plasticity_nodes)
{
  int i,m1,m2;
  double *dR_dw1e_q1=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dw2e_q1=(double *)calloc(3*3,sizeof(double));

  double frac=2*Ge/(2*Ge+hard_constant_e);

  for(i=0;i<24;i++)   dR_dw1e_q1[i]=-BE1_t[i]*weight1*0.5*delta_x*0.5*delta_y;

  for (m1=0;m1<3;m1++) {
    for (m2=0;m2<3;m2++) {

      dR_dw2e_q1[3*m1+m2]=ID_mat[3*m1+m2]-plasticity_nodes[qpoints*numElems*l+qpoints*e+q]*dn_ds[9*qpoints*numElems*l+9*qpoints*e+9*q+3*m1+m2]*frac;
    }
  }


  prodMatMat(3,8,3,3,dR_dw1e_q1,dR_dw2e_q1,dR_dwe_q1);


    free(dR_dw1e_q1);
    free(dR_dw2e_q1);


}

void dS_dY_f1(int l, int numElems, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
    double *dS_dYe_q1, bool *plasticity_nodes)
{
  int m1;

  double frac=2*Ge/(2*Ge+hard_constant_e);

  for (m1=0;m1<3;m1++) {
        dS_dYe_q1[m1]=plasticity_nodes[l*numElems*qpoints+qpoints*e+q]*n_trial_stress[l*numElems*qpoints*3+qpoints*3*e+3*q+m1]*frac;
    }
}

void dS_dG_f1(int l, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,
  double Ge, double hard_constant_e, double yield_stress_e, double *dS1_dG_q1,bool *plasticity_nodes)
{
  int m1;

  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac=-2*hard_constant_e/denom2;

  for (m1=0;m1<3;m1++) {
        dS1_dG_q1[m1]=plasticity_nodes[l*numElems*qpoints+qpoints*e+q]*
        (norm_dev_stress[l*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[l*numElems*qpoints*3+qpoints*3*e+3*q+m1]*frac;
    }

}

void dS_da_f1(int l, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,
  double Ge, double hard_constant_e, double yield_stress_e, double *dS_da_q1,bool *plasticity_nodes)
{
  int m1;

  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac=2*Ge/denom2;

  for (m1=0;m1<3;m1++) {
        dS_da_q1[m1]=plasticity_nodes[l*numElems*qpoints+qpoints*e+q]*
        (norm_dev_stress[l*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[l*numElems*qpoints*3+qpoints*3*e+3*q+m1]*frac;
    }
}

void assembly_lambda(double *dR_dwe_q1, double *dR_dwe_q2, double *dR_dwe_q3, double *dR_dwe_q4,
double *dH_due_q1, double *dH_due_q2, double *dH_due_q3, double *dH_due_q4, double *Ke2)
{

int i;

//Tangent matrix 2
double Ke_q1[64]={0};
double Ke_q2[64]={0};
double Ke_q3[64]={0};
double Ke_q4[64]={0};

prodMatMat(3,8,8,3,dR_dwe_q1,dH_due_q1,Ke_q1);
prodMatMat(3,8,8,3,dR_dwe_q2,dH_due_q2,Ke_q2);
prodMatMat(3,8,8,3,dR_dwe_q3,dH_due_q3,Ke_q3);
prodMatMat(3,8,8,3,dR_dwe_q4,dH_due_q4,Ke_q4);

for(i=0;i<MNE*MNE;i++){
  Ke2[i]=Ke_q1[i]+Ke_q2[i]+Ke_q3[i]+Ke_q4[i];
}
}


void deta_du_f1(int l, int e, int numElems, double *rho, double penal, double *yphys, double *C,
  double *DEV, double *BE, double *deta_du)
{
int i;
double prod1[MNE*3];
double prod2[MNE*3];

//ProdMat 1
prodMatMat(3,3,MNE,3,C,BE,prod1);
for (i=0;i<MNE*3;i++) prod1[i]=pow(rho[e],penal)*yphys[l*numElems+e]*prod1[i];

//ProdMat 2
prodMatMat(3,3,MNE,3,DEV,prod1,prod2);
for (i=0;i<MNE*3;i++) deta_du[MNE*3*l+i]=prod2[i];
}

void dep_du_f(int q, int qpoints, int e, int numElems, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_du, double *dep_du)
{
  int i;
  double dep_deta[9];
  double prod[8*3];
  double mat1[8*3];

  double frac1=1.f/(2*Ge+hard_constant_e);

  for (i=0;i<3*3;i++) {
    if(i<6) dep_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
    if(i>=6) dep_deta[i]=2*plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
    //if(e==0) printf("dep_deta=%lg\n",dep_deta[i]);
  }

  for (i=0;i<8*3;i++) mat1[i]=deta_du[8*3*(l2-1)+i];

  prodMatMat(3,3,8,3,dep_deta,mat1,prod);

  for (i=0;i<8*3;i++) dep_du[8*3*l2+i]=prod[i]+dep_du[8*3*(l2-1)+i];
}

void dbeta_du_f(int q, int qpoints, int e, int numElems, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_du, double *dbeta_du)
{
  int i;
  double dbeta_deta[9];
  double prod[8*3];
  double mat1[8*3];

  double frac1=hard_constant_e/(2*Ge+hard_constant_e);

  for (i=0;i<3*3;i++) {
  dbeta_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
  }

  for (i=0;i<8*3;i++) mat1[i]=deta_du[8*3*(l2-1)+i];

  prodMatMat(3,3,8,3,dbeta_deta,mat1,prod);

  for (i=0;i<8*3;i++) dbeta_du[8*3*l2+i]=prod[i]+dbeta_du[8*3*(l2-1)+i];
}

void deta_du_f2(int l2, int e, int numElems, double *rho, double penal, double *yphys, double *C,
  double *DEV, double *dep_du, double *dbeta_du, double *deta_du)
{
int i;
double prod1[3*3];
double mat1[MNE*3];
double prod2[MNE*3];

//ProdMat 1
prodMatMat(3,3,3,3,DEV,C,prod1);
for (i=0;i<3*3;i++) prod1[i]=-pow(rho[e],penal)*yphys[l2*numElems+e]*prod1[i];

for (i=0;i<MNE*3;i++) {
  mat1[i]=dep_du[8*3*l2+i];
}

//ProdMat 2
prodMatMat(3,3,MNE,3,prod1,mat1,prod2);
for (i=0;i<MNE*3;i++) deta_du[MNE*3*l2+i]=prod2[i]-dbeta_du[8*3*l2+i];
}

void deta_du_f3(int l, int *initial_elements, int e, int numElems, double *rho, double penal, double *yphys, double *C,
  double *DEV,  double *BE, int l1, double *deta_du)
{
int i;
double prod1[3*3];
double mat1[MNE*3];
double prod2[MNE*3];

//ProdMat 1
prodMatMat(3,3,3,3,DEV,C,prod1);
for (i=0;i<3*3;i++) prod1[i]=-pow(rho[e],penal)*yphys[l1*numElems+e]*prod1[i];

for (i=0;i<MNE*3;i++) {
  mat1[i]=initial_elements[(l+1)*numElems+e]*BE[i];
}

//ProdMat 2
prodMatMat(3,3,MNE,3,prod1,mat1,prod2);
for (i=0;i<MNE*3;i++) deta_du[MNE*3*l1+i]=deta_du[MNE*3*l1+i]+prod2[i];
}

void deini_du_f(int l, int e, int numElems, double *BE, int *initial_elements, double *deini_du)
{
int i;
  for(i=0;i<24;i++) deini_du[i]=initial_elements[(l+1)*numElems+e]*BE[i];
}

void Ke_rhs_adjoint1(int l1, int loadsteps, int numElems, double delta_x, double delta_y, int q,int qpoints, int e,
   double *BE_t, double weight, double *dvm_dwe, double *rho, double penal, double *yphys, double *C,
   double *dep_du, double *Ke_rhs1, double *dc_rhs1)
{

  int i,m1;
  double dR_dw1e_q1[24];
  double dc_dH[3];
  double dH_dep[9];

  //First derivative R
  for(i=0;i<24;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y;

  //First derivative C
  for(i=0;i<3;i++)   {
    dc_dH[i]=dvm_dwe[3*e+i];
  }

  //Second derivative
  for (m1=0;m1<3*3;m1++) dH_dep[m1]=-pow(rho[e],penal)*yphys[l1*numElems+e]*C[m1];

  //Third derivative
  double dep_du1[24]={0};
  for(i=0;i<8*3;i++)  dep_du1[i]=dep_du[8*3*l1+i];

  //Matproduct 1
  double mat1[8*3]={0};
  prodMatMat(3,3,8,3,dH_dep,dep_du1,mat1);

  //Matproduct 2 R
  prodMatMat(3,8,8,3,dR_dw1e_q1,mat1,Ke_rhs1);

  //Matproduct 2 c
  prodVecMat(3,8,3,dc_dH,mat1,dc_rhs1);
}


void Ke_rhs_adjoint2(int l1, int loadsteps, int numElems, double delta_x, double delta_y,
  int q,int qpoints, int e, double *BE_t, double weight, double Ge, double hard_constant_e, double *dvm_dwe,
  double *dH_deta, double *deta_du, double *Ke_rhs2,  double *dc_rhs2, bool *plasticity_nodes)
{

  int i,m1;
  double dR_dw1e_q1[24];
  double dH_deta1[9];
  double dc_dH[3];

  double frac1=2*Ge/(2*Ge+hard_constant_e);

  //First derivative R
  for(i=0;i<24;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y;

  //First derivative C
  for(i=0;i<3;i++)   dc_dH[i]=dvm_dwe[3*e+i];

  //Second derivative
  for (m1=0;m1<3*3;m1++) {
      dH_deta1[m1]=-plasticity_nodes[qpoints*numElems*l1+qpoints*e+q]*dH_deta[9*qpoints*numElems*l1+9*qpoints*e+9*q+m1]*frac1;
  }

  //Third derivative
  double deta_du1[24]={0};
  for(i=0;i<8*3;i++)  deta_du1[i]=deta_du[8*3*l1+i];

  //Matproduct 1
  double mat1[8*3]={0};
  prodMatMat(3,3,8,3,dH_deta1,deta_du1,mat1);

  //Matproduct2 R
  prodMatMat(3,8,8,3,dR_dw1e_q1,mat1,Ke_rhs2);

  //Matproduct2 c
  prodVecMat(3,8,3,dc_dH,mat1,dc_rhs2);
}

void Ke_rhs_adjoint3(int l1, int loadsteps, int numElems, double delta_x, double delta_y, int q,int qpoints, int e,
   double *BE_t, double weight, double *dvm_dwe, double *rho, double penal, double *yphys, double *C,
   double *deini_du, double *Ke_rhs1, double *dc_rhs1)
{

  int i,m1;
  double dR_dw1e_q1[24];
  double dH_deini[9];
  double dc_dH[3];

  //First derivative R
  for(i=0;i<24;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y;

  //First derivative c
  for(i=0;i<3;i++)   dc_dH[i]=dvm_dwe[3*e+i];

  //Second derivative
  for (m1=0;m1<3*3;m1++)  dH_deini[m1]=-pow(rho[e],penal)*yphys[l1*numElems+e]*C[m1];

  //Matproduct 1
  double mat1[8*3]={0};
  prodMatMat(3,3,8,3,dH_deini,deini_du,mat1);

  //Matproduct2 R
  prodMatMat(3,8,8,3,dR_dw1e_q1,mat1,Ke_rhs1);

  //Matproduct2 c
  prodVecMat(3,8,3,dc_dH,mat1,dc_rhs1);

}

void deta_dC_f1(int ni, int l2, int numElems, int numNodes,int q, int qpoints, int e, double *rho, double penal, double *yphys, double *C,
  double *DEV_mat, double *BE, double *U_l, double *inh_strain2_l, double *old_plastic_strain_l, double *strain_ini, double *deta_dC)
{
  int i,j,id,m;
  int edofMat[8]={0};
  double Ue[8]={0};
  double strain[3]={0};
  double deta_dC1[3]={0};
  double dC_dx[9]={0};
  double prod1[9]={0};
  double penal1=penal-1;

  j=e/ni;
  i=e%ni;
  id=2*((ni+1)*j+i);
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//x2
  edofMat[3]=id+3;//y2
  edofMat[4]=id+2+2*(ni+1);//x3
  edofMat[5]=id+3+2*(ni+1);//y3
  edofMat[6]=id+0+2*(ni+1); //x4
  edofMat[7]=id+1+2*(ni+1); //y4

  for (i=0;i<3*3;i++) dC_dx[i]=penal*pow(rho[e],penal1)*yphys[l2*numElems+e]*C[i];

  prodMatMat(3,3,3,3,DEV_mat,dC_dx,prod1);

  for (m=0;m<8;m++) {
    Ue[m]=U_l[numNodes*l2+edofMat[m]];
        }

  prodMatVec(2*qpoints,3,2*qpoints,BE,Ue,strain);

  for (m=0;m<3;m++) {
    strain[m]=strain[m]-inh_strain2_l[3*l2*numElems+3*e+m]-
    old_plastic_strain_l[l2*numElems*3*qpoints+3*qpoints*e+3*q+m]-strain_ini[l2*numElems*3*qpoints+3*qpoints*e+3*q+m];
    //if(e==0 && q==0 && l2==0) printf("new code : strain1=%lg\n",strain[m]);
  }

  prodMatVec(3,3,3,prod1,strain,deta_dC1);

  for (m=0;m<3;m++) deta_dC[3*l2+m]=deta_dC1[m];
}

void dep_dC_f(int q, int qpoints, int e, int numElems, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_dC, double *dep_dC)
{
  int i;
  double dep_deta[9];
  double prod[3];
  double mat1[3];

  double frac1=1.f/(2*Ge+hard_constant_e);

  for (i=0;i<3*3;i++) {
    if(i<6) dep_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
    if(i>=6) dep_deta[i]=2*plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
      }

  for (i=0;i<3;i++) {
    mat1[i]=deta_dC[3*(l2-1)+i];
  }

  prodMatVec(3,3,3,dep_deta,mat1,prod);

  for (i=0;i<3;i++) dep_dC[3*l2+i]=prod[i]+dep_dC[3*(l2-1)+i];
}

void dbeta_dC_f(int q, int qpoints, int e, int numElems, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_dC, double *dbeta_dC)
{
  int i;
  double dbeta_deta[9];
  double prod[3];
  double mat1[3];

  double frac1=hard_constant_e/(2*Ge+hard_constant_e);

  for (i=0;i<3*3;i++) {
  dbeta_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
  }

  for (i=0;i<3;i++) mat1[i]=deta_dC[3*(l2-1)+i];

  prodMatVec(3,3,3,dbeta_deta,mat1,prod);

  for (i=0;i<3;i++) dbeta_dC[3*l2+i]=prod[i]+dbeta_dC[3*(l2-1)+i];
}

void dep_dY_f(int l2, int numElems, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
                double *dH_deta, double *deta_dY, double *dep_dY, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double dep_dY1[3];
  double dep_dY2[3];
  double dep_dY3[3];
  double dep_deta[9];
  double deta_dY1[3];

  for (i=0;i<3;i++){
    if(i<2){
      dep_dY1[i]=-plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac1;
          }
          else{
      dep_dY1[i]=-2*plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac1;
          }

      dep_dY2[i]=dep_dY[3*(l2-1)+i];
                }

  for (i=0;i<3*3;i++) {
      if(i<6){
      dep_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
              }
      else{
      dep_deta[i]=2*plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
          }
                     }

  for (i=0;i<3;i++) deta_dY1[i]=deta_dY[3*(l2-1)+i];

  prodMatVec(3,3,3,dep_deta,deta_dY1,dep_dY3);

  for (i=0;i<3;i++) {
    dep_dY[3*l2+i]=dep_dY1[i]+dep_dY2[i]+dep_dY3[i];
    //if(e==0 && q==0) printf("l2=%d, dep_dY=%lg, [%lg %lg %lg]\n",l2,dep_dY[i],dep_dY1[i],dep_dY2[i],dep_dY3[i]);
  }

}

void dbeta_dY_f(int l2, int numElems, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
                double *dH_deta, double *deta_dY, double *dbeta_dY, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double dbeta_dY1[3];
  double dbeta_dY2[3];
  double dbeta_dY3[3];
  double dbeta_deta[9];
  double deta_dY1[3];

  for (i=0;i<3;i++){
      dbeta_dY1[i]=-plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac1;
      dbeta_dY2[i]=dbeta_dY[3*(l2-1)+i];
                }

  for (i=0;i<3*3;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
                }

  for (i=0;i<3;i++) deta_dY1[i]=deta_dY[3*(l2-1)+i];

  prodMatVec(3,3,3,dbeta_deta,deta_dY1,dbeta_dY3);

  for (i=0;i<3;i++) dbeta_dY[3*l2+i]=dbeta_dY1[i]+dbeta_dY2[i]+dbeta_dY3[i];

}

void dep_dG_f(int l2, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,  double Ge, double yield_stress_e,
                double hard_constant_e, double *dH_deta, double *deta_dG, double *dep_dG, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2.f/denom2;
  double dep_dG1[3];
  double dep_dG2[3];
  double dep_dG3[3];
  double dep_deta[9];
  double deta_dG1[3];

  for (i=0;i<3;i++){
    if(i<2){
      dep_dG1[i]=-plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
          }
          else{
      dep_dG1[i]=-2*plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
            (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
          }

      dep_dG2[i]=dep_dG[3*(l2-1)+i];
                }

  for (i=0;i<3*3;i++) {
      if(i<6){
      dep_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
              }
      else{
      dep_deta[i]=2*plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
          }
                     }

  for (i=0;i<3;i++) deta_dG1[i]=deta_dG[3*(l2-1)+i];

  prodMatVec(3,3,3,dep_deta,deta_dG1,dep_dG3);

  for (i=0;i<3;i++) {
    dep_dG[3*l2+i]=dep_dG1[i]+dep_dG2[i]+dep_dG3[i];
    //if(e==0 && q==0) printf("dep_dG=[%lg %lg %lg]\n",dep_dG1[i],dep_dG2[i],dep_dG3[i]);
  }

}

void dbeta_dG_f(int l2, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                 double hard_constant_e, double *dH_deta, double *deta_dG, double *dbeta_dG, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2*hard_constant_e/denom2;
  double dbeta_dG1[3];
  double dbeta_dG2[3];
  double dbeta_dG3[3];
  double dbeta_deta[9];
  double deta_dG1[3];

  for (i=0;i<3;i++){
      dbeta_dG1[i]=-plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
      dbeta_dG2[i]=dbeta_dG[3*(l2-1)+i];
                }

  for (i=0;i<3*3;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
                }

  for (i=0;i<3;i++) deta_dG1[i]=deta_dG[3*(l2-1)+i];

  prodMatVec(3,3,3,dbeta_deta,deta_dG1,dbeta_dG3);

  for (i=0;i<3;i++) dbeta_dG[3*l2+i]=dbeta_dG1[i]+dbeta_dG2[i]+dbeta_dG3[i];

}

void dep_da_f(int l2, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                double hard_constant_e, double *dH_deta, double *deta_da, double *dep_da, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=1.f/denom2;
  double dep_da1[3];
  double dep_da2[3];
  double dep_da3[3];
  double dep_deta[9];
  double deta_da1[3];

  for (i=0;i<3;i++){
    if(i<2){
      dep_da1[i]=-plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
        (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
      }
      else{
        dep_da1[i]=-2*plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
        (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
      }

      dep_da2[i]=dep_da[3*(l2-1)+i];

                }

  for (i=0;i<3*3;i++) {
      dep_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
                     }

  for (i=0;i<3;i++) deta_da1[i]=deta_da[3*(l2-1)+i];

  prodMatVec(3,3,3,dep_deta,deta_da1,dep_da3);

  for (i=0;i<3;i++) {
    dep_da[3*l2+i]=dep_da1[i]+dep_da2[i]+dep_da3[i];
      //if(e==0 && q==0)  printf("dep_da=[%lg %lg %lg]\n",dep_da1[i],dep_da2[i],dep_da3[i]);
    }

}

void dbeta_da_f(int l2, int numElems, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                 double hard_constant_e, double *dH_deta, double *deta_da, double *dbeta_da, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2*Ge/denom2;
  double dbeta_da1[3];
  double dbeta_da2[3];
  double dbeta_da3[3];
  double dbeta_deta[9];
  double deta_da1[3];

  for (i=0;i<3;i++){
      dbeta_da1[i]=plasticity_nodes[(l2-1)*numElems*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems*qpoints*3+qpoints*3*e+3*q+i]*frac2;
      dbeta_da2[i]=dbeta_da[3*(l2-1)+i];
                }

  for (i=0;i<3*3;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems*(l2-1)+qpoints*e+q]*dH_deta[9*qpoints*numElems*(l2-1)+9*qpoints*e+9*q+i]*frac1;
                }

  for (i=0;i<3;i++) deta_da1[i]=deta_da[3*(l2-1)+i];

  prodMatVec(3,3,3,dbeta_deta,deta_da1,dbeta_da3);

  for (i=0;i<3;i++) {
    dbeta_da[3*l2+i]=dbeta_da1[i]+dbeta_da2[i]+dbeta_da3[i];
    //if(e==0 && q==0)  printf("dbeta_da=[%lg %lg %lg]\n",dbeta_da1[i],dbeta_da2[i],dbeta_da3[i]);
  }

}

void deta_dCYGa_f2(int l2, int e, int q, int numElems, double *rho, double penal, double *yphys, double *C,
  double *DEV_mat, double *dep_dC, double *dbeta_dC, double *deta_dC)
{
int i;
double prod1[3*3];
double mat1[3];
double prod2[3];

//ProdMat 1
prodMatMat(3,3,3,3,DEV_mat,C,prod1);
for (i=0;i<3*3;i++) prod1[i]=-pow(rho[e],penal)*yphys[l2*numElems+e]*prod1[i];

for (i=0;i<3;i++) {
  mat1[i]=dep_dC[3*l2+i];
  //if(e==0 && q==0) printf("l2=%d, mat1=%lg\n",l2,mat1[i]);
}

//ProdMat 2
prodMatVec(3,3,3,prod1,mat1,prod2);
for (i=0;i<3;i++) {
  deta_dC[3*l2+i]=deta_dC[3*l2+i]+prod2[i]-dbeta_dC[3*l2+i];
  //if(e==0 && q==0) printf("l2=%d : deta_dC=%lg, prod2=%lg, dbeta_dC=%lg\n",l2,deta_dC[3*l2+i],prod2[i],dbeta_dC[3*l2+i]);
}
}

void dS_dC_f1_bis(int ni, int l, int numElems, int numNodes, int q, int qpoints, int e, double *BE, double *U_l,
  double *rho, double penal, double *yphys, double *C,
double *inh_strain2_l, double *old_plastic_strain_l, double *strain_ini, double *ID_mat, double *dep_dC, double *dS1_dC)
{
  int i,j,id,m;
  int edofMat[8]={0};
  double Ue[8]={0};
  double strain[3]={0}; double strain_C[3]={0};
  double mat1[9]={0};
  double mat2[3]={0};
  double prod1[3]={0};
  double ds_tr_dC[3]={0};
  double dC_dx[9]={0};
  double penal1=penal-1;

  j=e/ni;
  i=e%ni;
  id=2*((ni+1)*j+i);
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//x2
  edofMat[3]=id+3;//y2
  edofMat[4]=id+2+2*(ni+1);//x3
  edofMat[5]=id+3+2*(ni+1);//y3
  edofMat[6]=id+0+2*(ni+1); //x4
  edofMat[7]=id+1+2*(ni+1); //y4

  for (m=0;m<8;m++) {
    Ue[m]=U_l[numNodes*l+edofMat[m]];
        }

  prodMatVec(2*qpoints,3,2*qpoints,BE,Ue,strain);

  for (m=0;m<3;m++) {
    strain[m]=strain[m]-inh_strain2_l[3*l*numElems+3*e+m]-
    old_plastic_strain_l[l*numElems*3*qpoints+3*qpoints*e+3*q+m]-strain_ini[l*numElems*3*qpoints+3*qpoints*e+3*q+m];
  }


  for (i=0;i<3*3;i++) {
    mat1[i]=pow(rho[e],penal)*yphys[l*numElems+e]*C[i];
  }

  for (i=0;i<3;i++) {
    mat2[i]=dep_dC[3*l+i];
  }

  prodMatVec(3,3,3,mat1,mat2,prod1);

  for (i=0;i<3*3;i++) dC_dx[i]=penal*pow(rho[e],penal1)*yphys[l*numElems+e]*C[i];
  prodMatVec(3,3,3,dC_dx,strain,strain_C);

  for (i=0;i<3;i++) {
    ds_tr_dC[i]=strain_C[i]-prod1[i];
  }
  prodMatVec(3,3,3,ID_mat,ds_tr_dC,dS1_dC);
}

void dS_dCYGa_f2_bis(int l, int loadsteps, int numElems, int q,int qpoints, int e, double Ge, double hard_constant_e,
  bool *plasticity_nodes,double *dH_deta, double *deta_dC, double *dS2_dC)
{

  int i,m1;
  double dH_deta1[9];

  double frac1=2*Ge/(2*Ge+hard_constant_e);


  //Second derivative
  for (m1=0;m1<3*3;m1++) {
      dH_deta1[m1]=-plasticity_nodes[qpoints*numElems*l+qpoints*e+q]*dH_deta[9*qpoints*numElems*l+9*qpoints*e+9*q+m1]*frac1;
  }

  //Third derivative
  double deta_dC1[3]={0};

  for(i=0;i<3;i++){
  deta_dC1[i]=deta_dC[3*l+i];
  //if(e==0 && q==0) printf("deta_dC/dY=%lg\n",deta_dC1[i]);
  }

  //Matproduct1
  prodMatVec(3,3,3,dH_deta1,deta_dC1,dS2_dC);


}

void dS_dYGa_f3_bis(int l, int numElems, int numNodes, int q, int qpoints, int e,
double *rho, double penal, double *yphys, double *C, double *ID_mat,
double *dep_dY, double *dS3_dY)
{
  int i;
  double dep_dY1[3]={0};
  double prod1[3]={0};

  for (i=0;i<3;i++) dep_dY1[i]=dep_dY[3*l+i];

  prodMatVec(3,3,3,C,dep_dY1,prod1);

  for (i=0;i<3;i++) prod1[i]=-pow(rho[e],penal)*yphys[l*numElems+e]*prod1[i];

  prodMatVec(3,3,3,ID_mat,prod1,dS3_dY);

}


void adjoint_K(int l, int ni, int numElems, int qpoints, double *rho, double penal, double G, double yield_stress, double hard_constant,
double *ID_mat, double *dn_ds_l, double *yphys_l, double *C1, double delta_x, double delta_y, double weight1, double weight2, double weight3, double weight4,
double *BE1, double *BE2, double *BE3, double *BE4, double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t, bool *plasticity_nodes_l, double nodal_force,
double *dc_dvm, double *dvm_dwe, double dc_dvm1, double *von_mises_stress, double *stress_xx_e, double *stress_yy_e, double *stress_xy_e, double pnorm, double *dc_due,
bool *plasticity_nodes, double *Ke2_e)
{
  int e,q,m,m1,m2;


  double *Ke2=(double *)calloc(MNE*MNE,sizeof(double));
  double *dvm_dwe_short=(double *)calloc(3,sizeof(double));
  double *vm_q1=(double *)calloc(3,sizeof(double));
  double *vm_q2=(double *)calloc(3,sizeof(double));
  double *vm_q3=(double *)calloc(3,sizeof(double));
  double *vm_q4=(double *)calloc(3,sizeof(double));
  double *r2_q1=(double *)calloc(8,sizeof(double));
  double *r2_q2=(double *)calloc(8,sizeof(double));
  double *r2_q3=(double *)calloc(8,sizeof(double));
  double *r2_q4=(double *)calloc(8,sizeof(double));

  double *dn_ds_q1=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q2=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q3=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q4=(double *)calloc(3*3,sizeof(double));

  double *dn_ds_q1_t=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q2_t=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q3_t=(double *)calloc(3*3,sizeof(double));
  double *dn_ds_q4_t=(double *)calloc(3*3,sizeof(double));

  double *dR_dwe_q1=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dwe_q2=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dwe_q3=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dwe_q4=(double *)calloc(3*2*qpoints,sizeof(double));

  double *dH_due_q1=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q2=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q3=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q4=(double *)calloc(3*2*qpoints,sizeof(double));

  double *dH_due_q1_t=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q2_t=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q3_t=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dH_due_q4_t=(double *)calloc(3*2*qpoints,sizeof(double));

  double pnorm2=pnorm-1;

    for(e=0;e<numElems;e++){

    double Ge=pow(rho[e],penal)*G;
    double hard_constant_e=pow(rho[e],penal)*hard_constant;
    double yield_stress_e=pow(rho[e],penal)*yield_stress;

    double frac=(double)2*Ge/(2*Ge+hard_constant_e);

    q=0 ; dR_dwe(l,numElems,q,qpoints,e,ID_mat,dn_ds_l,
      hard_constant_e,Ge, weight1,delta_x,delta_y,BE1_t,dR_dwe_q1,plasticity_nodes_l);
    q=1 ; dR_dwe(l,numElems,q,qpoints,e,ID_mat,dn_ds_l,
      hard_constant_e,Ge, weight2,delta_x,delta_y,BE2_t,dR_dwe_q2,plasticity_nodes_l);
    q=2;  dR_dwe(l,numElems,q,qpoints,e,ID_mat,dn_ds_l,
      hard_constant_e,Ge, weight3,delta_x,delta_y,BE3_t,dR_dwe_q3,plasticity_nodes_l);
    q=3;  dR_dwe(l,numElems,q,qpoints,e,ID_mat,dn_ds_l,
      hard_constant_e,Ge, weight4,delta_x,delta_y,BE4_t,dR_dwe_q4,plasticity_nodes_l);

      dH_due(qpoints,e,numElems,l,C1,yphys_l,BE1,rho,penal,dH_due_q1);
      dH_due(qpoints,e,numElems,l,C1,yphys_l,BE2,rho,penal,dH_due_q2);
      dH_due(qpoints,e,numElems,l,C1,yphys_l,BE3,rho,penal,dH_due_q3);
      dH_due(qpoints,e,numElems,l,C1,yphys_l,BE4,rho,penal,dH_due_q4);

      transMat(8,3,dH_due_q1,dH_due_q1_t);
      transMat(8,3,dH_due_q2,dH_due_q2_t);
      transMat(8,3,dH_due_q3,dH_due_q3_t);
      transMat(8,3,dH_due_q4,dH_due_q4_t);


      assembly_lambda(dR_dwe_q1, dR_dwe_q2, dR_dwe_q3, dR_dwe_q4, dH_due_q1,
        dH_due_q2, dH_due_q3, dH_due_q4, Ke2);

      for(m=0;m<MNE*MNE;m++)   Ke2_e[MNE*MNE*e+m]=Ke2[m];

      //dc_due *********************************

      if(nodal_force==0){
        if(von_mises_stress[l*numElems+e]!=0){
        dc_dvm[e]=(double)pow(von_mises_stress[l*numElems+e],pnorm2)*dc_dvm1/numElems;
        dvm_dwe[3*e+0]=(double)(2*stress_xx_e[l*numElems+e]-stress_yy_e[l*numElems+e])/(2*von_mises_stress[l*numElems+e]);
        dvm_dwe[3*e+1]=(double)(2*stress_yy_e[l*numElems+e]-stress_xx_e[l*numElems+e])/(2*von_mises_stress[l*numElems+e]);
        dvm_dwe[3*e+2]=(double)3*stress_xy_e[l*numElems+e]/von_mises_stress[l*numElems+e];
      }
      else{
        dc_dvm[e]=0;
        dvm_dwe[3*e+0]=0;
        dvm_dwe[3*e+1]=0;
        dvm_dwe[3*e+2]=0;
      }

        for (m1=0;m1<3;m1++) {
          for (m2=0;m2<3;m2++) {
        dn_ds_q1[3*m1+m2]=ID_mat[3*m1+m2]-plasticity_nodes[qpoints*e+0]*dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*0+3*m1+m2]*frac;
        dn_ds_q2[3*m1+m2]=ID_mat[3*m1+m2]-plasticity_nodes[qpoints*e+1]*dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*1+3*m1+m2]*frac;
        dn_ds_q3[3*m1+m2]=ID_mat[3*m1+m2]-plasticity_nodes[qpoints*e+2]*dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*2+3*m1+m2]*frac;
        dn_ds_q4[3*m1+m2]=ID_mat[3*m1+m2]-plasticity_nodes[qpoints*e+3]*dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*3+3*m1+m2]*frac;

      }

        dvm_dwe_short[m1]=dvm_dwe[3*e+m1];

      }
        transMat(3,3,dn_ds_q1,dn_ds_q1_t);
        transMat(3,3,dn_ds_q2,dn_ds_q2_t);
        transMat(3,3,dn_ds_q3,dn_ds_q3_t);
        transMat(3,3,dn_ds_q4,dn_ds_q4_t);

        prodMatVec(3,3,3,dn_ds_q1_t,dvm_dwe_short,vm_q1);
        prodMatVec(3,3,3,dn_ds_q2_t,dvm_dwe_short,vm_q2);
        prodMatVec(3,3,3,dn_ds_q3_t,dvm_dwe_short,vm_q3);
        prodMatVec(3,3,3,dn_ds_q4_t,dvm_dwe_short,vm_q4);

        prodMatVec(3,8,3,dH_due_q1_t,vm_q1,r2_q1);
        prodMatVec(3,8,3,dH_due_q2_t,vm_q2,r2_q2);
        prodMatVec(3,8,3,dH_due_q3_t,vm_q3,r2_q3);
        prodMatVec(3,8,3,dH_due_q4_t,vm_q4,r2_q4);


        for(m=0;m<8;m++){
        dc_due[8*numElems*l+8*e+m]=dc_dvm[e]*0.25*(r2_q1[m]+r2_q2[m]+r2_q3[m]+r2_q4[m]);
        }

      }

  }//end element loop

  free(dR_dwe_q1); free(dR_dwe_q2); free(dR_dwe_q3); free(dR_dwe_q4);
  free(dH_due_q1); free(dH_due_q2); free(dH_due_q3); free(dH_due_q4);
  free(dH_due_q1_t); free(dH_due_q2_t); free(dH_due_q3_t); free(dH_due_q4_t);
  free(dn_ds_q1); free(dn_ds_q2); free(dn_ds_q3); free(dn_ds_q4);
  free(dn_ds_q1_t); free(dn_ds_q2_t); free(dn_ds_q3_t); free(dn_ds_q4_t);
  free(vm_q1); free(vm_q2); free(vm_q3); free(vm_q4);
  free(r2_q1); free(r2_q2); free(r2_q3); free(r2_q4);
  free(Ke2); free(dvm_dwe_short);
}

void adjoint_rhs(int qpoints, double delta_x, double delta_y, int l, int load1, int loadsteps1, int loadsteps, double nodal_force, int ni, int numElems, int numNodes,
  double penal, double hard_constant, double yield_stress, double *yphys_l, double *DEV_mat, double *C1, double G, double *rho, double *BE1, double *BE2, double *BE3, double *BE4,
double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t, double weight1, double weight2, double weight3, double weight4,
double *von_mises_stress, double *stress_xx_e, double *stress_yy_e, double *stress_xy_e, double pnorm, double *dH_deta_l, bool *plasticity_nodes_l, int *initial_elements_l,
double *lambda_l,double *rhs, double *rhs_j, double *dc_dvm, double *dvm_dwe, double *dc_dvm2, bool *old_pstrain)
{
  int i,j,e,id,l1,l2,q,m;
  int edofMat[MNE];

  double Ke_rhs[MNE*MNE]; double Ke_rhs_t[MNE*MNE];
  double Ke_rhs1_q1[MNE*MNE],Ke_rhs1_q2[MNE*MNE],Ke_rhs1_q3[MNE*MNE],Ke_rhs1_q4[MNE*MNE];
  double Ke_rhs2_q1[MNE*MNE],Ke_rhs2_q2[MNE*MNE],Ke_rhs2_q3[MNE*MNE],Ke_rhs2_q4[MNE*MNE];
  double Ke_rhs3_q1[MNE*MNE],Ke_rhs3_q2[MNE*MNE],Ke_rhs3_q3[MNE*MNE],Ke_rhs3_q4[MNE*MNE];

  double dc_rhs[MNE];
  double dc_rhs1_q1[MNE],dc_rhs1_q2[MNE],dc_rhs1_q3[MNE],dc_rhs1_q4[MNE];
  double dc_rhs2_q1[MNE],dc_rhs2_q2[MNE],dc_rhs2_q3[MNE],dc_rhs2_q4[MNE];
  double dc_rhs3_q1[MNE],dc_rhs3_q2[MNE],dc_rhs3_q3[MNE],dc_rhs3_q4[MNE];

  double *deta_du_q1=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *deta_du_q2=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *deta_du_q3=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *deta_du_q4=(double *)calloc(8*3*loadsteps,sizeof(double));

  double *dep_du_q1=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dep_du_q2=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dep_du_q3=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dep_du_q4=(double *)calloc(8*3*loadsteps,sizeof(double));

  double *dbeta_du_q1=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dbeta_du_q2=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dbeta_du_q3=(double *)calloc(8*3*loadsteps,sizeof(double));
  double *dbeta_du_q4=(double *)calloc(8*3*loadsteps,sizeof(double));

  double *deini_du_q1=(double *)calloc(8*3,sizeof(double));
  double *deini_du_q2=(double *)calloc(8*3,sizeof(double));
  double *deini_du_q3=(double *)calloc(8*3,sizeof(double));
  double *deini_du_q4=(double *)calloc(8*3,sizeof(double));

  double lambda_e[8]; double r2[8];

  double pnorm2=pnorm-1;

  for (e=0;e<numElems;e++){
  j=e/ni;
  i=e%ni;
  id=2*((ni+1)*j+i);

  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//x2
  edofMat[3]=id+3;//y2
  edofMat[4]=id+2+2*(ni+1);//x3
  edofMat[5]=id+3+2*(ni+1);//y3
  edofMat[6]=id+0+2*(ni+1); //x4
  edofMat[7]=id+1+2*(ni+1); //y4

  double Ge=pow(rho[e],penal)*G;
  double hard_constant_e=pow(rho[e],penal)*hard_constant;
  double yield_stress_e=pow(rho[e],penal)*yield_stress;

  for (i=0;i<MNE*MNE;i++){
    Ke_rhs1_q1[i]=0;   Ke_rhs2_q1[i]=0; Ke_rhs3_q1[i]=0;
    Ke_rhs1_q2[i]=0;   Ke_rhs2_q2[i]=0; Ke_rhs3_q2[i]=0;
    Ke_rhs1_q3[i]=0;   Ke_rhs2_q3[i]=0; Ke_rhs3_q3[i]=0;
    Ke_rhs1_q4[i]=0;   Ke_rhs2_q4[i]=0; Ke_rhs3_q4[i]=0;
  }

  for (i=0;i<MNE;i++){
    dc_rhs1_q1[i]=0;   dc_rhs2_q1[i]=0; dc_rhs3_q1[i]=0;
    dc_rhs1_q2[i]=0;   dc_rhs2_q2[i]=0; dc_rhs3_q2[i]=0;
    dc_rhs1_q3[i]=0;   dc_rhs2_q3[i]=0; dc_rhs3_q3[i]=0;
    dc_rhs1_q4[i]=0;   dc_rhs2_q4[i]=0; dc_rhs3_q4[i]=0;
  }

  for (l1=l+1;l1<loadsteps;l1++){

    if(2*von_mises_stress[l1*numElems+e]!=0){
    dc_dvm[e]=(double)pow(von_mises_stress[l1*numElems+e],pnorm2)*dc_dvm2[l1]/numElems;
    dvm_dwe[3*e+0]=(double)(2*stress_xx_e[l1*numElems+e]-stress_yy_e[l1*numElems+e])/(2*von_mises_stress[l1*numElems+e]);
    dvm_dwe[3*e+1]=(double)(2*stress_yy_e[l1*numElems+e]-stress_xx_e[l1*numElems+e])/(2*von_mises_stress[l1*numElems+e]);
    dvm_dwe[3*e+2]=(double)3*stress_xy_e[l1*numElems+e]/von_mises_stress[l1*numElems+e];
    }
    else{
      dc_dvm[e]=0;
      dvm_dwe[3*e+0]=0;
      dvm_dwe[3*e+1]=0;
      dvm_dwe[3*e+2]=0;
    }


    //Calculation of dep_du, dbeta_du and deta_du
    for (i=0;i<8*3*loadsteps;i++){
    deta_du_q1[i]=0; dep_du_q1[i]=0; dbeta_du_q1[i]=0;
    deta_du_q2[i]=0; dep_du_q2[i]=0; dbeta_du_q2[i]=0;
    deta_du_q3[i]=0; dep_du_q3[i]=0; dbeta_du_q3[i]=0;
    deta_du_q4[i]=0; dep_du_q4[i]=0; dbeta_du_q4[i]=0;
    }

    for (i=0;i<8*3;i++){
    deini_du_q1[i]=0;
    deini_du_q2[i]=0;
    deini_du_q3[i]=0;
    deini_du_q4[i]=0;
    }

    if(plasticity_nodes_l[qpoints*numElems*l1+qpoints*e+0]==1 ||
       plasticity_nodes_l[qpoints*numElems*l1+qpoints*e+1]==1 ||
       plasticity_nodes_l[qpoints*numElems*l1+qpoints*e+2]==1 ||
       plasticity_nodes_l[qpoints*numElems*l1+qpoints*e+3]==1){
  //  if(old_pstrain[l1]==1 && old_pstrain[l+1]==1){

          for(l2=l;l2<=l1;l2++){
            if(l2==l){
              q=0; deta_du_f1(l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE1,deta_du_q1);
              q=1; deta_du_f1(l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE2,deta_du_q2);
              q=2; deta_du_f1(l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE3,deta_du_q3);
              q=3; deta_du_f1(l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE4,deta_du_q4);
            }
            else{
              if(plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+0]==1 ||
                 plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+1]==1 ||
                 plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+2]==1 ||
                 plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+3]==1){

                   //time=get_time();
              q=0; dep_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q1,dep_du_q1);
              q=1; dep_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q2,dep_du_q2);
              q=2; dep_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q3,dep_du_q3);
              q=3; dep_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q4,dep_du_q4);
                  //printf("time dep_du_f=%f s\n",get_time()-time);

                  //time=get_time();
              q=0; dbeta_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q1,dbeta_du_q1);
              q=1; dbeta_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q2,dbeta_du_q2);
              q=2; dbeta_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q3,dbeta_du_q3);
              q=3; dbeta_du_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q4,dbeta_du_q4);
                //printf("time dbeta_du_f=%f s\n",get_time()-time);

                //time=get_time();
              q=0; deta_du_f2(l2,e,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_du_q1,dbeta_du_q1,deta_du_q1);
              q=1; deta_du_f2(l2,e,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_du_q2,dbeta_du_q2,deta_du_q2);
              q=2; deta_du_f2(l2,e,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_du_q3,dbeta_du_q3,deta_du_q3);
              q=3; deta_du_f2(l2,e,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_du_q4,dbeta_du_q4,deta_du_q4);
              //printf("time deta_du_f=%f s\n",get_time()-time);
            }
            }
          }//end l2 loop

          //Plastic strain contribution
          q=0; Ke_rhs_adjoint1(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE1_t,weight1,
              dvm_dwe,rho,penal,yphys_l,C1, dep_du_q1,Ke_rhs1_q1,dc_rhs1_q1);
          q=1; Ke_rhs_adjoint1(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE2_t,weight2,
              dvm_dwe,rho,penal,yphys_l,C1, dep_du_q2,Ke_rhs1_q2,dc_rhs1_q2);
          q=2; Ke_rhs_adjoint1(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE3_t,weight3,
              dvm_dwe,rho,penal,yphys_l,C1, dep_du_q3,Ke_rhs1_q3,dc_rhs1_q3);
          q=3; Ke_rhs_adjoint1(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE4_t,weight4,
              dvm_dwe,rho,penal,yphys_l,C1, dep_du_q4,Ke_rhs1_q4,dc_rhs1_q4);


             }//end plasticity loop


          //Shifted stress contribution
            //if(e==0) time=get_time();

          if(load1==loadsteps1-1 ){
            q=0; deta_du_f3(l,initial_elements_l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE1,l1,deta_du_q1);
            q=0; deta_du_f3(l,initial_elements_l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE2,l1,deta_du_q2);
            q=0; deta_du_f3(l,initial_elements_l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE3,l1,deta_du_q3);
            q=0; deta_du_f3(l,initial_elements_l,e,numElems,rho,penal,yphys_l,C1,DEV_mat,BE4,l1,deta_du_q4);

          }

          q=0; Ke_rhs_adjoint2(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE1_t,weight1,
                   Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q1,Ke_rhs2_q1,dc_rhs2_q1,plasticity_nodes_l);
          q=1; Ke_rhs_adjoint2(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE2_t,weight2,
                   Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q2,Ke_rhs2_q2,dc_rhs2_q2,plasticity_nodes_l);
          q=2; Ke_rhs_adjoint2(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE3_t,weight3,
                   Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q3,Ke_rhs2_q3,dc_rhs2_q3,plasticity_nodes_l);
          q=3; Ke_rhs_adjoint2(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE4_t,weight4,
                   Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q4,Ke_rhs2_q4,dc_rhs2_q4,plasticity_nodes_l);


       //Activation strain contribution
       if(load1==loadsteps1-1 ){
         //if(e==0) printf("activation strain : l=%d, l1=%d\n",l,l1);
         //if(e==0) time=get_time();
         q=0; deini_du_f(l,e,numElems,BE1,initial_elements_l,deini_du_q1);
         q=1; deini_du_f(l,e,numElems,BE2,initial_elements_l,deini_du_q2);
         q=2; deini_du_f(l,e,numElems,BE3,initial_elements_l,deini_du_q3);
         q=3; deini_du_f(l,e,numElems,BE4,initial_elements_l,deini_du_q4);
         //if(e==0) printf("time deini_du_f=%f s\n",get_time()-time);
       }

       if(deini_du_q1[0]!=0 || deini_du_q2[0]!=0 || deini_du_q3[0]!=0 || deini_du_q4[0]!=0){
        q=0; Ke_rhs_adjoint3(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE1_t,weight1,
                 dvm_dwe,rho,penal,yphys_l,C1, deini_du_q1,Ke_rhs3_q1,dc_rhs3_q1);
        q=1; Ke_rhs_adjoint3(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE2_t,weight2,
                 dvm_dwe,rho,penal,yphys_l,C1, deini_du_q2,Ke_rhs3_q2,dc_rhs3_q2);
        q=2; Ke_rhs_adjoint3(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE3_t,weight3,
                 dvm_dwe,rho,penal,yphys_l,C1, deini_du_q3,Ke_rhs3_q3,dc_rhs3_q3);
        q=3; Ke_rhs_adjoint3(l1,loadsteps,numElems,delta_x,delta_y,q,qpoints,e,BE4_t,weight4,
                 dvm_dwe,rho,penal,yphys_l,C1, deini_du_q4,Ke_rhs3_q4,dc_rhs3_q4);
       }

       if(Ke_rhs1_q1[0]!=0 || Ke_rhs1_q2[0]!=0 || Ke_rhs1_q3[0]!=0 || Ke_rhs1_q4[0]!=0 ||
          Ke_rhs2_q1[0]!=0 || Ke_rhs2_q2[0]!=0 || Ke_rhs2_q3[0]!=0 || Ke_rhs2_q4[0]!=0 ||
          Ke_rhs3_q1[0]!=0 || Ke_rhs3_q2[0]!=0 || Ke_rhs3_q3[0]!=0 || Ke_rhs3_q4[0]!=0){

       for(i=0;i<MNE;i++){
         for(j=0;j<MNE;j++){
           Ke_rhs[MNE*i+j]=Ke_rhs1_q1[MNE*i+j]+Ke_rhs1_q2[MNE*i+j]+Ke_rhs1_q3[MNE*i+j]+Ke_rhs1_q4[MNE*i+j]+
                           Ke_rhs2_q1[MNE*i+j]+Ke_rhs2_q2[MNE*i+j]+Ke_rhs2_q3[MNE*i+j]+Ke_rhs2_q4[MNE*i+j]+
                           Ke_rhs3_q1[MNE*i+j]+Ke_rhs3_q2[MNE*i+j]+Ke_rhs3_q3[MNE*i+j]+Ke_rhs3_q4[MNE*i+j];

       }
          dc_rhs[i]=dc_rhs1_q1[i]+dc_rhs1_q2[i]+dc_rhs1_q3[i]+dc_rhs1_q4[i]+
                    dc_rhs2_q1[i]+dc_rhs2_q2[i]+dc_rhs2_q3[i]+dc_rhs2_q4[i]+
                    dc_rhs3_q1[i]+dc_rhs3_q2[i]+dc_rhs3_q3[i]+dc_rhs3_q4[i];
     }


    for (m=0;m<8;m++) {
      lambda_e[m]=lambda_l[numNodes*l1+edofMat[m]];
      }

      transMat(8,8,Ke_rhs,Ke_rhs_t);
      prodMatVec(8,8,8,Ke_rhs_t,lambda_e,r2);

    for (m=0;m<8;m++)    {
      rhs[8*e+m]=rhs[8*e+m]+r2[m];
      if(nodal_force==0) rhs_j[8*e+m]=rhs_j[8*e+m]+dc_dvm[e]*0.25*dc_rhs[m];
    }
   }

  }//end l1 loop
} //end element loop

free(deta_du_q1); free(deta_du_q2); free(deta_du_q3); free(deta_du_q4);
free(dep_du_q1); free(dep_du_q2); free(dep_du_q3); free(dep_du_q4);
free(dbeta_du_q1); free(dbeta_du_q2); free(dbeta_du_q3); free(dbeta_du_q4);
free(deini_du_q1); free(deini_du_q2); free(deini_du_q3); free(deini_du_q4);
}

void x_derivatives(int l, int loadsteps, int numNodes, int numElems, int qpoints, double *U_l, double *lambda_l, int ni, double *rho, double penal,
  double yield_stress, double G, double hard_constant, double *yphys_l, double *C1, double *ID_mat, double *DEV_mat, double *inh_strain2_l,
  double *old_plastic_strain_l, double *strain_ini, double delta_x, double delta_y, double weight1, double weight2, double weight3, double weight4,
double *BE1, double *BE2, double *BE3, double *BE4, double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t, bool *old_pstrain,
bool *plasticity_nodes_l, double *dH_deta_l, double *norm_dev_stress_l, double *n_trial_stress_l,
double *von_mises_stress, double *stress_xx_e, double *stress_yy_e, double *stress_xy_e,
double dc_dvm1, double pnorm, double *dc_dvm, double *dvm_dwe, double nodal_force, double *dc_dxe,
double *gradient2_l, double *gradient2_map)
{
  int i,j,e,id,m,l2,q;
  int edofMat[8];
  double somme1, somme2, somme3, somme4;
  double somme1_dc, somme2_dc, somme3_dc, somme4_dc;

  double lambda_e[8];
  double penal1=penal-1;
  double pnorm2=pnorm-1;

  double *dR_dC_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dC_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dC_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dC_q4=(double *)calloc(2*qpoints,sizeof(double));

  double *dR_dxe_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dxe_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dxe_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dxe_q4=(double *)calloc(2*qpoints,sizeof(double));

  double *dR_dS_q1=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dS_q2=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dS_q3=(double *)calloc(3*2*qpoints,sizeof(double));
  double *dR_dS_q4=(double *)calloc(3*2*qpoints,sizeof(double));

  double *dR_dY_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dY_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dY_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dY_q4=(double *)calloc(2*qpoints,sizeof(double));

  double *dR_dG_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dG_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dG_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_dG_q4=(double *)calloc(2*qpoints,sizeof(double));

  double *dR_da_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_da_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_da_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dR_da_q4=(double *)calloc(2*qpoints,sizeof(double));



//////////////////////////////////////////


  double *dS_dC_q1=(double *)calloc(3,sizeof(double));
  double *dS_dC_q2=(double *)calloc(3,sizeof(double));
  double *dS_dC_q3=(double *)calloc(3,sizeof(double));
  double *dS_dC_q4=(double *)calloc(3,sizeof(double));

  double *dS1_dC_q1=(double *)calloc(3,sizeof(double));
  double *dS1_dC_q2=(double *)calloc(3,sizeof(double));
  double *dS1_dC_q3=(double *)calloc(3,sizeof(double));
  double *dS1_dC_q4=(double *)calloc(3,sizeof(double));

  double *dS2_dC_q1=(double *)calloc(3,sizeof(double));
  double *dS2_dC_q2=(double *)calloc(3,sizeof(double));
  double *dS2_dC_q3=(double *)calloc(3,sizeof(double));
  double *dS2_dC_q4=(double *)calloc(3,sizeof(double));

  double *dS_dY_q1=(double *)calloc(2*qpoints,sizeof(double));
  double *dS_dY_q2=(double *)calloc(2*qpoints,sizeof(double));
  double *dS_dY_q3=(double *)calloc(2*qpoints,sizeof(double));
  double *dS_dY_q4=(double *)calloc(2*qpoints,sizeof(double));

  double *dS1_dY_q1=(double *)calloc(3,sizeof(double));
  double *dS1_dY_q2=(double *)calloc(3,sizeof(double));
  double *dS1_dY_q3=(double *)calloc(3,sizeof(double));
  double *dS1_dY_q4=(double *)calloc(3,sizeof(double));

  double *dS2_dY_q1=(double *)calloc(3,sizeof(double));
  double *dS2_dY_q2=(double *)calloc(3,sizeof(double));
  double *dS2_dY_q3=(double *)calloc(3,sizeof(double));
  double *dS2_dY_q4=(double *)calloc(3,sizeof(double));

  double *dS3_dY_q1=(double *)calloc(3,sizeof(double));
  double *dS3_dY_q2=(double *)calloc(3,sizeof(double));
  double *dS3_dY_q3=(double *)calloc(3,sizeof(double));
  double *dS3_dY_q4=(double *)calloc(3,sizeof(double));

  double *dS_dG_q1=(double *)calloc(3,sizeof(double));
  double *dS_dG_q2=(double *)calloc(3,sizeof(double));
  double *dS_dG_q3=(double *)calloc(3,sizeof(double));
  double *dS_dG_q4=(double *)calloc(3,sizeof(double));

  double *dS1_dG_q1=(double *)calloc(3,sizeof(double));
  double *dS1_dG_q2=(double *)calloc(3,sizeof(double));
  double *dS1_dG_q3=(double *)calloc(3,sizeof(double));
  double *dS1_dG_q4=(double *)calloc(3,sizeof(double));

  double *dS2_dG_q1=(double *)calloc(3,sizeof(double));
  double *dS2_dG_q2=(double *)calloc(3,sizeof(double));
  double *dS2_dG_q3=(double *)calloc(3,sizeof(double));
  double *dS2_dG_q4=(double *)calloc(3,sizeof(double));

  double *dS3_dG_q1=(double *)calloc(3,sizeof(double));
  double *dS3_dG_q2=(double *)calloc(3,sizeof(double));
  double *dS3_dG_q3=(double *)calloc(3,sizeof(double));
  double *dS3_dG_q4=(double *)calloc(3,sizeof(double));

  double *dS_da_q1=(double *)calloc(3,sizeof(double));
  double *dS_da_q2=(double *)calloc(3,sizeof(double));
  double *dS_da_q3=(double *)calloc(3,sizeof(double));
  double *dS_da_q4=(double *)calloc(3,sizeof(double));

  double *dS1_da_q1=(double *)calloc(3,sizeof(double));
  double *dS1_da_q2=(double *)calloc(3,sizeof(double));
  double *dS1_da_q3=(double *)calloc(3,sizeof(double));
  double *dS1_da_q4=(double *)calloc(3,sizeof(double));

  double *dS2_da_q1=(double *)calloc(3,sizeof(double));
  double *dS2_da_q2=(double *)calloc(3,sizeof(double));
  double *dS2_da_q3=(double *)calloc(3,sizeof(double));
  double *dS2_da_q4=(double *)calloc(3,sizeof(double));

  double *dS3_da_q1=(double *)calloc(3,sizeof(double));
  double *dS3_da_q2=(double *)calloc(3,sizeof(double));
  double *dS3_da_q3=(double *)calloc(3,sizeof(double));
  double *dS3_da_q4=(double *)calloc(3,sizeof(double));


  ////////////////////////////////////////////////

  //C
  double *deta_dC_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dC_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dC_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dC_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dep_dC_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dC_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dC_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dC_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dbeta_dC_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dC_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dC_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dC_q4=(double *)calloc(3*loadsteps,sizeof(double));

  //Y
  double *deta_dY_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dY_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dY_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dY_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dep_dY_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dY_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dY_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dY_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dbeta_dY_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dY_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dY_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dY_q4=(double *)calloc(3*loadsteps,sizeof(double));

  //G
  double *deta_dG_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dG_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dG_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_dG_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dep_dG_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dG_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dG_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_dG_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dbeta_dG_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dG_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dG_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_dG_q4=(double *)calloc(3*loadsteps,sizeof(double));

  //a
  double *deta_da_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_da_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_da_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *deta_da_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dep_da_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_da_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_da_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dep_da_q4=(double *)calloc(3*loadsteps,sizeof(double));

  double *dbeta_da_q1=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_da_q2=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_da_q3=(double *)calloc(3*loadsteps,sizeof(double));
  double *dbeta_da_q4=(double *)calloc(3*loadsteps,sizeof(double));


for(e=0;e<numElems;e++){

  j=e/ni;
  i=e%ni;

  double Ge=pow(rho[e],penal)*G;
  double dGe=penal*pow(rho[e],penal1)*G;
  double yield_stress_e=pow(rho[e],penal)*yield_stress;

  double d_yield_stress_e=penal*pow(rho[e],penal1)*yield_stress;
  double hard_constant_e=pow(rho[e],penal)*hard_constant;
  double d_hard_constant_e=penal*pow(rho[e],penal1)*hard_constant;

  id=2*((ni+1)*j+i);

  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//x2
  edofMat[3]=id+3;//y2
  edofMat[4]=id+2+2*(ni+1);//x3
  edofMat[5]=id+3+2*(ni+1);//y3
  edofMat[6]=id+0+2*(ni+1); //x4
  edofMat[7]=id+1+2*(ni+1); //y4


  for (m=0;m<8;m++) {
    lambda_e[m]=lambda_l[numNodes*l+edofMat[m]];
    //printf("l=%d, e=%d lambda_e=%lg\n",l,e,lambda_e[m]);
    }

    for(i=0;i<24;i++){
      dR_dS_q1[i]=-BE1_t[i]*weight1*0.5*delta_x*0.5*delta_y;
      dR_dS_q2[i]=-BE2_t[i]*weight2*0.5*delta_x*0.5*delta_y;
      dR_dS_q3[i]=-BE3_t[i]*weight3*0.5*delta_x*0.5*delta_y;
      dR_dS_q4[i]=-BE4_t[i]*weight4*0.5*delta_x*0.5*delta_y;
      //printf("dR_dS_q1=%lg\n",dR_dS_q1[i]);
    }

    for(i=0;i<3;i++) {
    dS2_dC_q1[i]=0;  dS2_dY_q1[i]=0; dS3_dY_q1[i]=0; dS2_dG_q1[i]=0; dS3_dG_q1[i]=0; dS2_da_q1[i]=0; dS3_da_q1[i]=0;
    dS2_dC_q2[i]=0;  dS2_dY_q2[i]=0; dS3_dY_q2[i]=0; dS2_dG_q2[i]=0; dS3_dG_q2[i]=0; dS2_da_q2[i]=0; dS3_da_q2[i]=0;
    dS2_dC_q3[i]=0;  dS2_dY_q3[i]=0; dS3_dY_q3[i]=0; dS2_dG_q3[i]=0; dS3_dG_q3[i]=0; dS2_da_q3[i]=0; dS3_da_q3[i]=0;
    dS2_dC_q4[i]=0;  dS2_dY_q4[i]=0; dS3_dY_q4[i]=0; dS2_dG_q4[i]=0; dS3_dG_q4[i]=0; dS2_da_q4[i]=0; dS3_da_q4[i]=0;
    }

    //Derivative with respect to C
    for (i=0;i<3*loadsteps;i++){
    deta_dC_q1[i]=0; dep_dC_q1[i]=0; dbeta_dC_q1[i]=0;
    deta_dC_q2[i]=0; dep_dC_q2[i]=0; dbeta_dC_q2[i]=0;
    deta_dC_q3[i]=0; dep_dC_q3[i]=0; dbeta_dC_q3[i]=0;
    deta_dC_q4[i]=0; dep_dC_q4[i]=0; dbeta_dC_q4[i]=0;
  }


  for(l2=0;l2<=l;l2++){
    q=0; deta_dC_f1(ni,l2,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,DEV_mat,
      BE1,U_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q1);
    q=1; deta_dC_f1(ni,l2,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,DEV_mat,
      BE2,U_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q2);
    q=2; deta_dC_f1(ni,l2,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,DEV_mat,
      BE3,U_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q3);
    q=3; deta_dC_f1(ni,l2,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,DEV_mat,
      BE4,U_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q4);

      if(old_pstrain[l2]==1){
      if(plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+0]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+1]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+2]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+3]==1){


    q=0; dep_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q1,dep_dC_q1);
    q=1; dep_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q2,dep_dC_q2);
    q=2; dep_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q3,dep_dC_q3);
    q=3; dep_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q4,dep_dC_q4);

    q=0; dbeta_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q1,dbeta_dC_q1);
    q=1; dbeta_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q2,dbeta_dC_q2);
    q=2; dbeta_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q3,dbeta_dC_q3);
    q=3; dbeta_dC_f(q,qpoints,e,numElems,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q4,dbeta_dC_q4);

    q=0; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dC_q1,dbeta_dC_q1,deta_dC_q1);
    q=1; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dC_q2,dbeta_dC_q2,deta_dC_q2);
    q=2; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dC_q3,dbeta_dC_q3,deta_dC_q3);
    q=3; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dC_q4,dbeta_dC_q4,deta_dC_q4);

  }
}

  }//end l2 loop

  q=0; dS_dC_f1_bis(ni,l,numElems,numNodes,q,qpoints,e,BE1,U_l,rho,penal,yphys_l,C1,
                    inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q1,dS1_dC_q1);
  q=1; dS_dC_f1_bis(ni,l,numElems,numNodes,q,qpoints,e,BE2,U_l,rho,penal,yphys_l,C1,
                    inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q2,dS1_dC_q2);
  q=2; dS_dC_f1_bis(ni,l,numElems,numNodes,q,qpoints,e,BE3,U_l,rho,penal,yphys_l,C1,
                    inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q3,dS1_dC_q3);
  q=3; dS_dC_f1_bis(ni,l,numElems,numNodes,q,qpoints,e,BE4,U_l,rho,penal,yphys_l,C1,
                    inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q4,dS1_dC_q4);

  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
                      deta_dC_q1,dS2_dC_q1);
  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
                      deta_dC_q2,dS2_dC_q2);
  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
                      deta_dC_q3,dS2_dC_q3);
  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
                      deta_dC_q4,dS2_dC_q4);

    for (m=0;m<3;m++) {
        dS_dC_q1[m]=dS1_dC_q1[m]+dS2_dC_q1[m];
        dS_dC_q2[m]=dS1_dC_q2[m]+dS2_dC_q2[m];
        dS_dC_q3[m]=dS1_dC_q3[m]+dS2_dC_q3[m];
        dS_dC_q4[m]=dS1_dC_q4[m]+dS2_dC_q4[m];
                  }

  prodMatVec(3,8,3,dR_dS_q1,dS_dC_q1,dR_dC_q1);
  prodMatVec(3,8,3,dR_dS_q2,dS_dC_q2,dR_dC_q2);
  prodMatVec(3,8,3,dR_dS_q3,dS_dC_q3,dR_dC_q3);
  prodMatVec(3,8,3,dR_dS_q4,dS_dC_q4,dR_dC_q4);

    //Derivative with respect to Y,G,a

  for (i=0;i<3*loadsteps;i++){
      //yield stress
      deta_dY_q1[i]=0; dep_dY_q1[i]=0; dbeta_dY_q1[i]=0;
      deta_dY_q2[i]=0; dep_dY_q2[i]=0; dbeta_dY_q2[i]=0;
      deta_dY_q3[i]=0; dep_dY_q3[i]=0; dbeta_dY_q3[i]=0;
      deta_dY_q4[i]=0; dep_dY_q4[i]=0; dbeta_dY_q4[i]=0;

      //shear modulus
      deta_dG_q1[i]=0; dep_dG_q1[i]=0; dbeta_dG_q1[i]=0;
      deta_dG_q2[i]=0; dep_dG_q2[i]=0; dbeta_dG_q2[i]=0;
      deta_dG_q3[i]=0; dep_dG_q3[i]=0; dbeta_dG_q3[i]=0;
      deta_dG_q4[i]=0; dep_dG_q4[i]=0; dbeta_dG_q4[i]=0;

      //hardening constant
      deta_da_q1[i]=0; dep_da_q1[i]=0; dbeta_da_q1[i]=0;
      deta_da_q2[i]=0; dep_da_q2[i]=0; dbeta_da_q2[i]=0;
      deta_da_q3[i]=0; dep_da_q3[i]=0; dbeta_da_q3[i]=0;
      deta_da_q4[i]=0; dep_da_q4[i]=0; dbeta_da_q4[i]=0;
    }

    for(l2=0;l2<=l;l2++){

      if(old_pstrain[l2]==1){
      if(plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+0]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+1]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+2]==1 ||
         plasticity_nodes_l[qpoints*numElems*(l2-1)+qpoints*e+3]==1){

        //yield stress
      //  if(e==0) printf("yield stress\n");
        q=0; dep_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q1,dep_dY_q1,plasticity_nodes_l);
        q=1; dep_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q2,dep_dY_q2,plasticity_nodes_l);
        q=2; dep_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q3,dep_dY_q3,plasticity_nodes_l);
        q=3; dep_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q4,dep_dY_q4,plasticity_nodes_l);

        q=0; dbeta_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q1,dbeta_dY_q1,plasticity_nodes_l);
        q=1; dbeta_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q2,dbeta_dY_q2,plasticity_nodes_l);
        q=2; dbeta_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q3,dbeta_dY_q3,plasticity_nodes_l);
        q=3; dbeta_dY_f(l2,numElems,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q4,dbeta_dY_q4,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dY_q1,dbeta_dY_q1,deta_dY_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dY_q2,dbeta_dY_q2,deta_dY_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dY_q3,dbeta_dY_q3,deta_dY_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dY_q4,dbeta_dY_q4,deta_dY_q4);

        //shear modulus
        //if(e==0) printf("shear modulus\n");
        q=0; dep_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q1,dep_dG_q1,plasticity_nodes_l);
        q=1; dep_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q2,dep_dG_q2,plasticity_nodes_l);
        q=2; dep_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q3,dep_dG_q3,plasticity_nodes_l);
        q=3; dep_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q4,dep_dG_q4,plasticity_nodes_l);

        q=0; dbeta_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q1,dbeta_dG_q1,plasticity_nodes_l);
        q=1; dbeta_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q2,dbeta_dG_q2,plasticity_nodes_l);
        q=2; dbeta_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q3,dbeta_dG_q3,plasticity_nodes_l);
        q=3; dbeta_dG_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q4,dbeta_dG_q4,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dG_q1,dbeta_dG_q1,deta_dG_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dG_q2,dbeta_dG_q2,deta_dG_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dG_q3,dbeta_dG_q3,deta_dG_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_dG_q4,dbeta_dG_q4,deta_dG_q4);

        //hardening constant
        //if(e==0) printf("hardening constant\n");
        q=0; dep_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q1,dep_da_q1,plasticity_nodes_l);
        q=1; dep_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q2,dep_da_q2,plasticity_nodes_l);
        q=2; dep_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q3,dep_da_q3,plasticity_nodes_l);
        q=3; dep_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q4,dep_da_q4,plasticity_nodes_l);

        q=0; dbeta_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q1,dbeta_da_q1,plasticity_nodes_l);
        q=1; dbeta_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q2,dbeta_da_q2,plasticity_nodes_l);
        q=2; dbeta_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q3,dbeta_da_q3,plasticity_nodes_l);
        q=3; dbeta_da_f(l2,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q4,dbeta_da_q4,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_da_q1,dbeta_da_q1,deta_da_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_da_q2,dbeta_da_q2,deta_da_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_da_q3,dbeta_da_q3,deta_da_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems,rho,penal,yphys_l,C1,DEV_mat,dep_da_q4,dbeta_da_q4,deta_da_q4);
      }
    }
  }//end l2 loop

  ///////////////////// Y /////////////////////////////
  //S
  q=0; dS_dY_f1(l,numElems,q,qpoints,e,n_trial_stress_l,Ge,
      hard_constant_e,dS1_dY_q1,plasticity_nodes_l);

  q=1; dS_dY_f1(l,numElems,q,qpoints,e,n_trial_stress_l,Ge,
      hard_constant_e,dS1_dY_q2,plasticity_nodes_l);

  q=2; dS_dY_f1(l,numElems,q,qpoints,e,n_trial_stress_l,Ge,
      hard_constant_e,dS1_dY_q3,plasticity_nodes_l);

  q=3; dS_dY_f1(l,numElems,q,qpoints,e,n_trial_stress_l,Ge,
      hard_constant_e,dS1_dY_q4,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
        deta_dY_q1,dS2_dY_q1);

  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
        deta_dY_q2,dS2_dY_q2);

  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
        deta_dY_q3,dS2_dY_q3);

  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,
        deta_dY_q4,dS2_dY_q4);

  //ds_tr

  q=0; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dY_q1,dS3_dY_q1);

  q=1; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dY_q2,dS3_dY_q2);

  q=2; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dY_q3,dS3_dY_q3);

  q=3; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dY_q4,dS3_dY_q4);


  ///////////////////// G /////////////////////////////

  //S
  q=0; dS_dG_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_dG_q1,plasticity_nodes_l);

  q=1; dS_dG_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_dG_q2,plasticity_nodes_l);

  q=2; dS_dG_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_dG_q3,plasticity_nodes_l);

  q=3; dS_dG_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_dG_q4,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_dG_q1,dS2_dG_q1);

  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_dG_q2,dS2_dG_q2);

  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_dG_q3,dS2_dG_q3);

  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_dG_q4,dS2_dG_q4);

  //ds_tr
  q=0; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dG_q1,dS3_dG_q1);

  q=1; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dG_q2,dS3_dG_q2);

  q=2; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dG_q3,dS3_dG_q3);

  q=3; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_dG_q4,dS3_dG_q4);

  ///////////////////// a /////////////////////////////

  //S
  q=0; dS_da_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_da_q1,plasticity_nodes_l);

  q=1; dS_da_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_da_q2,plasticity_nodes_l);

  q=2; dS_da_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_da_q3,plasticity_nodes_l);

  q=3; dS_da_f1(l,numElems,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,
      hard_constant_e,yield_stress_e,dS1_da_q4,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_da_q1,dS2_da_q1);

  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_da_q2,dS2_da_q2);

  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_da_q3,dS2_da_q3);

  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems,q,qpoints,e,Ge,hard_constant_e,
    plasticity_nodes_l,dH_deta_l,deta_da_q4,dS2_da_q4);


  //ds_tr
  q=0; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_da_q1,dS3_da_q1);

  q=1; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_da_q2,dS3_da_q2);

  q=2; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_da_q3,dS3_da_q3);

  q=3; dS_dYGa_f3_bis(l,numElems,numNodes,q,qpoints,e,rho,penal,yphys_l,C1,
  ID_mat,dep_da_q4,dS3_da_q4);

  //////////////////////////////////////////////////////////////////////

  for (m=0;m<3;m++) {
      dS_dY_q1[m]=dS1_dY_q1[m]+dS2_dY_q1[m]+dS3_dY_q1[m];
      dS_dY_q2[m]=dS1_dY_q2[m]+dS2_dY_q2[m]+dS3_dY_q2[m];
      dS_dY_q3[m]=dS1_dY_q3[m]+dS2_dY_q3[m]+dS3_dY_q3[m];
      dS_dY_q4[m]=dS1_dY_q4[m]+dS2_dY_q4[m]+dS3_dY_q4[m];

      dS_dG_q1[m]=dS1_dG_q1[m]+dS2_dG_q1[m]+dS3_dG_q1[m];
      dS_dG_q2[m]=dS1_dG_q2[m]+dS2_dG_q2[m]+dS3_dG_q2[m];
      dS_dG_q3[m]=dS1_dG_q3[m]+dS2_dG_q3[m]+dS3_dG_q3[m];
      dS_dG_q4[m]=dS1_dG_q4[m]+dS2_dG_q4[m]+dS3_dG_q4[m];

      dS_da_q1[m]=dS1_da_q1[m]+dS2_da_q1[m]+dS3_da_q1[m];
      dS_da_q2[m]=dS1_da_q2[m]+dS2_da_q2[m]+dS3_da_q2[m];
      dS_da_q3[m]=dS1_da_q3[m]+dS2_da_q3[m]+dS3_da_q3[m];
      dS_da_q4[m]=dS1_da_q4[m]+dS2_da_q4[m]+dS3_da_q4[m];

      }

prodMatVec(3,8,3,dR_dS_q1,dS_dY_q1,dR_dY_q1);
prodMatVec(3,8,3,dR_dS_q2,dS_dY_q2,dR_dY_q2);
prodMatVec(3,8,3,dR_dS_q3,dS_dY_q3,dR_dY_q3);
prodMatVec(3,8,3,dR_dS_q4,dS_dY_q4,dR_dY_q4);

prodMatVec(3,8,3,dR_dS_q1,dS_dG_q1,dR_dG_q1);
prodMatVec(3,8,3,dR_dS_q2,dS_dG_q2,dR_dG_q2);
prodMatVec(3,8,3,dR_dS_q3,dS_dG_q3,dR_dG_q3);
prodMatVec(3,8,3,dR_dS_q4,dS_dG_q4,dR_dG_q4);

prodMatVec(3,8,3,dR_dS_q1,dS_da_q1,dR_da_q1);
prodMatVec(3,8,3,dR_dS_q2,dS_da_q2,dR_da_q2);
prodMatVec(3,8,3,dR_dS_q3,dS_da_q3,dR_da_q3);
prodMatVec(3,8,3,dR_dS_q4,dS_da_q4,dR_da_q4);

//////////////////////////////////////////
for (m=0;m<8;m++) {

  dR_dY_q1[m]=dR_dY_q1[m]*d_yield_stress_e;
  dR_dY_q2[m]=dR_dY_q2[m]*d_yield_stress_e;
  dR_dY_q3[m]=dR_dY_q3[m]*d_yield_stress_e;
  dR_dY_q4[m]=dR_dY_q4[m]*d_yield_stress_e;

  dR_dG_q1[m]=dR_dG_q1[m]*dGe;
  dR_dG_q2[m]=dR_dG_q2[m]*dGe;
  dR_dG_q3[m]=dR_dG_q3[m]*dGe;
  dR_dG_q4[m]=dR_dG_q4[m]*dGe;

  dR_da_q1[m]=dR_da_q1[m]*d_hard_constant_e;
  dR_da_q2[m]=dR_da_q2[m]*d_hard_constant_e;
  dR_da_q3[m]=dR_da_q3[m]*d_hard_constant_e;
  dR_da_q4[m]=dR_da_q4[m]*d_hard_constant_e;

  dR_dxe_q1[m]=dR_dC_q1[m]+dR_dY_q1[m]+dR_dG_q1[m]+dR_da_q1[m];
  dR_dxe_q2[m]=dR_dC_q2[m]+dR_dY_q2[m]+dR_dG_q2[m]+dR_da_q2[m];
  dR_dxe_q3[m]=dR_dC_q3[m]+dR_dY_q3[m]+dR_dG_q3[m]+dR_da_q3[m];
  dR_dxe_q4[m]=dR_dC_q4[m]+dR_dY_q4[m]+dR_dG_q4[m]+dR_da_q4[m];

}

somme1=0;
somme2=0;
somme3=0;
somme4=0;

for (m=0;m<8;m++){
  somme1=somme1+lambda_e[m]*dR_dxe_q1[m];
  somme2=somme2+lambda_e[m]*dR_dxe_q2[m];
  somme3=somme3+lambda_e[m]*dR_dxe_q3[m];
  somme4=somme4+lambda_e[m]*dR_dxe_q4[m];

  }

  if(von_mises_stress[l*numElems+e]!=0){
  dc_dvm[e]=(double)pow(von_mises_stress[l*numElems+e],pnorm2)*dc_dvm1/numElems;
  dvm_dwe[3*e+0]=(double)(2*stress_xx_e[l*numElems+e]-stress_yy_e[l*numElems+e])/(2*von_mises_stress[l*numElems+e]);
  dvm_dwe[3*e+1]=(double)(2*stress_yy_e[l*numElems+e]-stress_xx_e[l*numElems+e])/(2*von_mises_stress[l*numElems+e]);
  dvm_dwe[3*e+2]=(double)3*stress_xy_e[l*numElems+e]/von_mises_stress[l*numElems+e];
}
else{
  dc_dvm[e]=0;
  dvm_dwe[3*e+0]=0;
  dvm_dwe[3*e+1]=0;
  dvm_dwe[3*e+2]=0;
}


somme1_dc=0;
somme2_dc=0;
somme3_dc=0;
somme4_dc=0;

for (m=0;m<3;m++){
    somme1_dc=somme1_dc+dvm_dwe[3*e+m]*(dS_dC_q1[m]+dS_dY_q1[m]*d_yield_stress_e+dS_dG_q1[m]*dGe+dS_da_q1[m]*d_hard_constant_e);
    somme2_dc=somme2_dc+dvm_dwe[3*e+m]*(dS_dC_q2[m]+dS_dY_q2[m]*d_yield_stress_e+dS_dG_q2[m]*dGe+dS_da_q2[m]*d_hard_constant_e);
    somme3_dc=somme3_dc+dvm_dwe[3*e+m]*(dS_dC_q3[m]+dS_dY_q3[m]*d_yield_stress_e+dS_dG_q3[m]*dGe+dS_da_q3[m]*d_hard_constant_e);
    somme4_dc=somme4_dc+dvm_dwe[3*e+m]*(dS_dC_q4[m]+dS_dY_q4[m]*d_yield_stress_e+dS_dG_q4[m]*dGe+dS_da_q4[m]*d_hard_constant_e);
}

dc_dxe[e]=dc_dvm[e]*0.25*(somme1_dc+somme2_dc+somme3_dc+somme4_dc);

gradient2_l[l*numElems+e]=(nodal_force==0)*dc_dxe[e]+somme1+somme2+somme3+somme4;

gradient2_map[e]=gradient2_map[e]+gradient2_l[l*numElems+e];
//if(e==0 || e==1 || e==58  || e==30 || e==59 || e==600 || e==601)  if(l==loadsteps-1) printf("e=%d :  dc_dxe=%lg, gradient2=%.8lg \n", e,dc_dxe[e],gradient2_map[e]);


}//end element loop

free(dR_dS_q1); free(dR_dS_q2); free(dR_dS_q3); free(dR_dS_q4);
free(dS_dC_q1); free(dS_dC_q2); free(dS_dC_q3); free(dS_dC_q4);
free(dS1_dC_q1); free(dS1_dC_q2); free(dS1_dC_q3); free(dS1_dC_q4);
free(dS2_dC_q1); free(dS2_dC_q2); free(dS2_dC_q3); free(dS2_dC_q4);
free(deta_dC_q1); free(deta_dC_q2); free(deta_dC_q3); free(deta_dC_q4);
free(dep_dC_q1); free(dep_dC_q2); free(dep_dC_q3); free(dep_dC_q4);
free(dbeta_dC_q1); free(dbeta_dC_q2); free(dbeta_dC_q3); free(dbeta_dC_q4);
free(deta_dY_q1); free(deta_dY_q2); free(deta_dY_q3); free(deta_dY_q4);
free(dep_dY_q1); free(dep_dY_q2); free(dep_dY_q3); free(dep_dY_q4);
free(dbeta_dY_q1); free(dbeta_dY_q2); free(dbeta_dY_q3); free(dbeta_dY_q4);
free(deta_dG_q1); free(deta_dG_q2); free(deta_dG_q3); free(deta_dG_q4);
free(dep_dG_q1); free(dep_dG_q2); free(dep_dG_q3); free(dep_dG_q4);
free(dbeta_dG_q1); free(dbeta_dG_q2); free(dbeta_dG_q3); free(dbeta_dG_q4);
free(deta_da_q1); free(deta_da_q2); free(deta_da_q3); free(deta_da_q4);
free(dep_da_q1); free(dep_da_q2); free(dep_da_q3); free(dep_da_q4);
free(dbeta_da_q1); free(dbeta_da_q2); free(dbeta_da_q3); free(dbeta_da_q4);
free(dR_dY_q1); free(dR_dY_q2);  free(dR_dY_q3); free(dR_dY_q4);
free(dR_dG_q1); free(dR_dG_q2);  free(dR_dG_q3); free(dR_dG_q4);
free(dS_dY_q1); free(dS_dY_q2); free(dS_dY_q3); free(dS_dY_q4);
free(dS1_dY_q1); free(dS1_dY_q2); free(dS1_dY_q3); free(dS1_dY_q4);
free(dS2_dY_q1); free(dS2_dY_q2); free(dS2_dY_q3); free(dS2_dY_q4);
free(dS3_dY_q1); free(dS3_dY_q2); free(dS3_dY_q3); free(dS3_dY_q4);
free(dR_da_q1); free(dR_da_q2); free(dR_da_q3); free(dR_da_q4);
free(dS_dG_q1);  free(dS_dG_q2); free(dS_dG_q3); free(dS_dG_q4);
free(dS1_dG_q1);  free(dS1_dG_q2); free(dS1_dG_q3); free(dS1_dG_q4);
free(dS2_dG_q1);  free(dS2_dG_q2); free(dS2_dG_q3); free(dS2_dG_q4);
free(dS3_dG_q1);  free(dS3_dG_q2); free(dS3_dG_q3); free(dS3_dG_q4);
free(dS_da_q1);  free(dS_da_q2); free(dS_da_q3); free(dS_da_q4);
free(dS1_da_q1);  free(dS1_da_q2); free(dS1_da_q3); free(dS1_da_q4);
free(dS2_da_q1);  free(dS2_da_q2); free(dS2_da_q3); free(dS2_da_q4);
free(dS3_da_q1);  free(dS3_da_q2); free(dS3_da_q3); free(dS3_da_q4);
free(dR_dC_q1); free(dR_dC_q2); free(dR_dC_q3); free(dR_dC_q4);
free(dR_dxe_q1); free(dR_dxe_q2); free(dR_dxe_q3); free(dR_dxe_q4);
}

////////////

void rho_definition(int ni, int nj, int nj1, double *rho, double volfrac, int *map_gradient)
  {
    int i,j,e;

    for (j=0;j<nj;j++) {
      for (i=0;i<ni;i++) {
        e=I2D(ni,i,j);
        if(j<nj1){

      rho[e]=volfrac;

      //if((i-40)*(i-40)+(j-10)*(j-10)<=5*5) rho[e]=0.8;
      //if((i-20)*(i-20)+(j-10)*(j-10)<=5*5) rho[e]=0.8;

      map_gradient[e]=e;
          }
       else{
         rho[e]=1;
         map_gradient[e]=-1;
          }
    }
    }
  }


//Matrix-free vector product : elastic contribution
__global__ void GPUMatVec_elastic(int ni, size_t numElems, double *rho,  double penal,
  double *yphys, double *Ke,  double *U, double *r)
{
  //global gpu thread index
	//double Au;

  int i,j,id,edofMat[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

    j=e/ni;
    i=e%ni;

    id=2*((ni+1)*j+i);

    edofMat[0]=id;//x1
    edofMat[1]=id+1;//y1
    edofMat[2]=id+2;//x2
    edofMat[3]=id+3;//y2
    edofMat[4]=id+2+2*(ni+1);//x3
    edofMat[5]=id+3+2*(ni+1);//y3
    edofMat[6]=id+0+2*(ni+1); //x4
    edofMat[7]=id+1+2*(ni+1); //y4



	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= pow(rho[e],penal)*yphys[e]*Ke[i*MNE+j]*U[edofMat[j]];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+j]*U[edofMat[j]];
                                  }
      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];

      atomicAdd(&r[edofMat[i]], sum);
                         }
  }

}

__global__ void GPUMatVec_2(int ni, size_t numElems,  double *Ke_total,  double *U, double *r)
{
  //global gpu thread index
	//double Au;

  int i,j,l,id,edofMat[MNE];
  double Au[MNE],sum;
  double Ke[MNE*MNE];
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

    for(l=0;l<MNE*MNE;l++) Ke[l]=Ke_total[MNE*MNE*e+l];

    j=e/ni;
    i=e%ni;

    id=2*((ni+1)*j+i);

    edofMat[0]=id;//x1
    edofMat[1]=id+1;//y1
    edofMat[2]=id+2;//x2
    edofMat[3]=id+3;//y2
    edofMat[4]=id+2+2*(ni+1);//x3
    edofMat[5]=id+3+2*(ni+1);//y3
    edofMat[6]=id+0+2*(ni+1); //x4
    edofMat[7]=id+1+2*(ni+1); //y4



	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= Ke[i*MNE+j]*U[edofMat[j]];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+j]*U[edofMat[j]];
                                  }
      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];

      atomicAdd(&r[edofMat[i]], sum);
                         }
  }

}

//Preconditionning for the conjugate gradient algorithm : Jacobi preconditionner = diagonal scaling
__global__ void GPUMatVec_precond(int ni, size_t numElems, double *rho, double *yphys, double penal,
  double *Ke,  double *r)
{
  //global gpu thread index
	//double Au;

  int i,j,id,edofMat[MNE];
  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

      j=e/ni;
      i=e%ni;

      id=2*((ni+1)*j+i);

      edofMat[0]=id;//x1
      edofMat[1]=id+1;//y1
      edofMat[2]=id+2;//x2
      edofMat[3]=id+3;//y2
      edofMat[4]=id+2+2*(ni+1);//x3
      edofMat[5]=id+3+2*(ni+1);//y3
      edofMat[6]=id+0+2*(ni+1); //x4
      edofMat[7]=id+1+2*(ni+1); //y4


	for (int i=0; i<MNE; i++){

      Au= pow(rho[e],penal)*yphys[e]*Ke[i*MNE+i];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+i];
      atomicAdd(&r[edofMat[i]], Au);
                         }
  }

}

__global__ void GPUMatVec_precond2(int ni, size_t numElems,  double *Ke,  double *r)
{
  //global gpu thread index
	//double Au;

  int i,j,id,edofMat[MNE];
  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

      j=e/ni;
      i=e%ni;

      id=2*((ni+1)*j+i);

      edofMat[0]=id;//x1
      edofMat[1]=id+1;//y1
      edofMat[2]=id+2;//x2
      edofMat[3]=id+3;//y2
      edofMat[4]=id+2+2*(ni+1);//x3
      edofMat[5]=id+3+2*(ni+1);//y3
      edofMat[6]=id+0+2*(ni+1); //x4
      edofMat[7]=id+1+2*(ni+1); //y4


	for (int i=0; i<MNE; i++){

      Au= Ke[MNE*MNE*e+i*MNE+i];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+i];
      atomicAdd(&r[edofMat[i]], Au);
                         }
  }

}

__global__ void GPUMatVec_plastic(int ni, int nj, size_t numElems, bool *plasticity_elements,int *map_plastic, double *Ke_plastic,
  double *U, double *r)
{
  //global gpu thread index
	//double Au;

  int i,j,l,id,e1,edofMat[MNE];
  double Au[MNE],sum;
  double Ke[MNE*MNE];
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){
    //if(e==0) printf("device=%d : Matrix-free plastic vector product\n",device);
    if(plasticity_elements[e]==1){
      e1=map_plastic[e];
      for(l=0;l<MNE*MNE;l++) Ke[l]=Ke_plastic[MNE*MNE*e1+l];

          j=e/ni;
          i=e%ni;

          id=2*((ni+1)*j+i);

          edofMat[0]=id;//x1
          edofMat[1]=id+1;//y1
          edofMat[2]=id+2;//x2
          edofMat[3]=id+3;//y2
          edofMat[4]=id+2+2*(ni+1);//x3
          edofMat[5]=id+3+2*(ni+1);//y3
          edofMat[6]=id+0+2*(ni+1); //x4
          edofMat[7]=id+1+2*(ni+1); //y4



	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= Ke[i*MNE+j]*U[edofMat[j]];
                                  }
      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];

      atomicAdd(&r[edofMat[i]], sum);
                         }
  }
}
}

__global__ void MatVec_sum(int numNodes, double *prod_elastic, double *prod_plastic, double *prod)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    //if(n==0) printf("device=%d : Summation elastic-plastic\n",device);
      //if(active_nodes[n]==1){
        prod[n] = prod_elastic[n] + prod_plastic[n];
        //if(prod_elastic[n]!=0) printf("prod_elastic[%d]=%lg and prod_plastic[%d]=%lg\n",n,prod_elastic[n],n,prod_plastic[n]);
      //}
      //else{
      //  prod[n]=0;
      //}
  }

}


//Conjugate gradient Procedures
__global__ void rzp_calculation(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
      //printf("M[%d]=%f\n",n,M_pre[n]);
      M_pre[n]= 1.f/M_pre[n];
      r[n] = f[n] - prod[n];
      z[n]=M_pre[n]*r[n];
      p[n] = z[n];

      //if(n==0) printf("M_pre[21056]=%lg, r[21056]=%lg, p[21056]=%lg, z[21056]=%lg\n",M_pre[21056],
    //r[21056],p[21056],z[21056]);
    }

  }

}

__global__ void rzp_calculation2(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
      //printf("M[%d]=%f\n",n,M_pre[n]);
      M_pre[n]= 1.f/M_pre[n];
      r[n] = f[n] - prod[n];
      z[n]=M_pre[n]*r[n];
      p[n] = z[n];

      //if(n==0) printf("M_pre[21056]=%lg, r[21056]=%lg, p[21056]=%lg, z[21056]=%lg\n",M_pre[21056],
    //r[21056],p[21056],z[21056]);
    }

  }

}

__global__ void UpdateVec(double *p, double *r, int numNodes, int *fixednodes, double *rzold, double *rznew,
  double *Ap_elastic, double *Ap_plastic)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  double beta = rznew[0]/rzold[0];
  //if(n==0) printf("rzold=%lg, rznew=%lg and beta=%f\n",rzold[0],rznew[0],beta);
  if (n<numNodes){
    Ap_elastic[n]=0;
    Ap_plastic[n]=0;
    if(fixednodes[n]==0 ){

      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void switch_r(double *rzold, double *rznew, double *pAp){
  //size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  if(threadIdx.x==0){
    rzold[0]=rznew[0];
    rznew[0]=0;
    pAp[0]=0;
    //norme[0]=0;
  }
}


__global__ void Norm(double *r, int numNodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes ){
  result[n]=r[n]*r[n];
    }
}

__global__ void Prod2_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0 ){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
   }

}

__global__ void Prod3_rz(double *r, double*z, int numNodes, int *fixednodes, double *result, double *norme)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0 ){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
     norme[0]=0;
   }

}

__global__ void Update_UR(double *M_pre, double *U, double *r, double *z, double *p, double *Ap, int numNodes, int *fixednodes,
  double *rzold, double *pAp, double *result1, double *result2)
{

  /*size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = M_pre[n]*r[n];

    }

  }*/

  __shared__ double cache1[BLOCKSIZE2];
  __shared__ double cache2[BLOCKSIZE2];

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp1=0.0;
  double temp2=0.0;


  while (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
        //if(n==numNodes-1) printf("rzold=%lg, pAp=%lg and alpha=%lg\n",rzold[0],pAp[0],alpha);
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = M_pre[n]*r[n];
      temp1=temp1+ r[n]*z[n];
      temp2=temp2+ r[n]*r[n];
    }
    n=n+stride;
    }

    cache1[threadIdx.x]=temp1;
    cache2[threadIdx.x]=temp2;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache1[threadIdx.x]+=cache1[threadIdx.x+i];
        cache2[threadIdx.x]+=cache2[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }

    if (threadIdx.x ==0){
     atomicAdd(&result1[0],cache1[0]);
     atomicAdd(&result2[0],cache2[0]);

   }

}

__global__ void Update_Newton(int numNodes, double *delta_U, double *U)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
      U[n] = U[n]+delta_U[n];
  }

}

__global__ void display(double *vector, int size){
  if(threadIdx.x==0){
    for (int i=0;i<size;i++){
      printf("vector[%d]=%lg\n",i,vector[i]);
    }
  }
}


int main(int argc, char **argv)
{
printf("Program beginning\n");
  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

  cudaSetDevice(2);
  cudaDeviceReset();

  double time_program = get_time();

  int i,j,i1,j1,l,l1,q,m,m1,m2,n,e,id,id2,i0,i02,cpt,iter,nb_plastic,nb_plastic_nodes;
  int layer, load1;
  bool plasticity;
int edofMat[MNE];
FILE *fichier;
double weight1,weight2,weight3,weight4,cost_function1, cost_function2,
 sommeV,n1,n2,n3,pnorm1, somme,
dp1_ds1,dp1_ds2,dp1_ds3,dp2_ds2,dp2_ds1,dp2_ds3,dp3_ds1,dp3_ds2,dp3_ds3,
ds1_dstr1,ds2_dstr1,ds1_dstr2,ds2_dstr2,ds3_dstr3;
double dc_dvm1,min_stress,ratio,ratio2;
weight1=1; weight2=1; weight3=1; weight4=1;
double time;


//number of elements in each direction
int ni=60;
int nj1=20;
int nj=30;
int qpoints=4;
//Note that if you want to change this value (qpoints), you will have also to create
//new BE arrays
double volfrac=0.5;
double rmin=2.1;
double penal=3;
double penal1=penal-1;
double pnorm=4;
int opt_steps=150;
int Max_iters=50000;
double tol=1e-6;
int Max_iters_newton=200;
double tol_newton=1e-6;
double nodal_force=-0*300000;
//number of nodes in x and y
int ni2=ni+1;


double delta_x=(double)0.001;
double delta_y=(double)0.001;

int numElems=ni*nj;
int numNodes=2*(ni+1)*(nj+1);

int numElems_opt=ni*nj1;

int nb_layers=5;
int layer_height=nj/nb_layers;
printf("layer_height=%d\n",layer_height);
int loadsteps1=2;
int loadsteps=nb_layers*loadsteps1;

double E=180E9;
double Emin=1e-6;
double nu=0.30;
double inh_strain[3]={0.002,-0.004,0};
double inh_strain2[3];
double *inh_strain2_l = (double *) malloc(3*loadsteps * numElems*sizeof(double));
double hard_constant=1e-2*E;//1e-4;//0.1*E;
double hard_constant_e;
double yield_stress=800e6*sqrt(2.f/3);
double yield_stress_e, Ge;
double G=(double)E/(2*(1+nu));

bool finite_difference=0;
bool forward_problem=1;
bool gradient_calculation=1;

double *ux= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
double *uy= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
double *lambdax= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
double *lambday= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
double *rhs_x= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
double *rhs_y= (double *)calloc(loadsteps*(ni+1)*(nj+1),sizeof(double));
bool *old_pstrain= (bool *)calloc(loadsteps,sizeof(bool));

char str1[10]=".vtk"; char str2[10];
char u2[10];

int *edofMat2= (int *)calloc(4*numElems,sizeof(int));

for (e=0;e<numElems;e++){
  j=e/ni;
  i=e%ni;

  id2=(ni+1)*j+i;

  edofMat2[4*e+0]=id2;
  edofMat2[4*e+1]=id2+1;
  edofMat2[4*e+2]=id2+1+(ni+1);
  edofMat2[4*e+3]=id2+(ni+1);

}

//MMA initialization
double Xmin = 0.2;
double Xmax = 0.9;
double movlim = 0.01;
double *rho= (double *)calloc(numElems,sizeof(double));
double *rho_opt= (double *)calloc(numElems_opt,sizeof(double));
double *rho_old_opt= (double *)calloc(numElems_opt,sizeof(double));
double *g= (double *)calloc(1,sizeof(double));
double *dg= (double *)calloc(numElems_opt,sizeof(double));
double *xmin= (double *)calloc(numElems_opt,sizeof(double));
double *xmax= (double *)calloc(numElems_opt,sizeof(double));
double *af= (double *)calloc(numElems_opt,sizeof(double));
double *bf= (double *)calloc(numElems_opt,sizeof(double));
MMASolver *mma = new MMASolver(numElems_opt,1,0,1e3,0);

int *map_gradient= (int *)calloc(numElems,sizeof(int));
double *gradient_map= (double *)calloc(numElems,sizeof(double));
double *gradient= (double *)calloc(numElems_opt,sizeof(double));
double *gradient1= (double *)calloc(numElems_opt,sizeof(double));
double *gradient2=(double *)calloc(numElems_opt,sizeof(double));
double *gradient_FD=(double *)calloc(numElems_opt,sizeof(double));
double *gradient2_map=(double *)calloc(numElems,sizeof(double));
double *gradient2_l=(double *)calloc(loadsteps*numElems,sizeof(double));
double *gradient_FD_map=(double *)calloc(numElems,sizeof(double));

double *cost_function=(double *)calloc(opt_steps,sizeof(double));
double *constraint=(double *)calloc(opt_steps,sizeof(double));

  rho_definition(ni,nj,nj1,rho,volfrac,map_gradient);


for (e=0;e<numElems;e++){
  if(map_gradient[e]>-1) rho_opt[map_gradient[e]]=rho[e];
}

double *yphys_d;
double *yphys= (double *)calloc(numElems,sizeof(double));
double *yphys_l= (double *)calloc(loadsteps*numElems,sizeof(double));
int *initial_elements= (int *)calloc(numElems,sizeof(int));
int *initial_elements_l= (int *)calloc(loadsteps*numElems,sizeof(int));
double *vms=(double *)calloc(numElems,sizeof(double));
double *von_mises_stress=(double *)calloc(loadsteps*numElems,sizeof(double));
double *dev_von_mises_stress=(double *)calloc(numElems,sizeof(double));
double *stress_xx_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_yy_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_xy_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *dev_stress_xx_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_yy_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_xy_e= (double *)calloc(numElems,sizeof(double));

double *n_trial_stress_xx_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_yy_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_xy_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_eq =(double *)calloc(numElems,sizeof(double));

double *dR_due=(double *)calloc(loadsteps*3*2*qpoints*numElems,sizeof(double));
double *dc_dvm=(double *)calloc(numElems,sizeof(double));
double *dc_dvm2=(double *)calloc(loadsteps,sizeof(double));
double *dvm_dwe=(double *)calloc(3*numElems,sizeof(double));

double *dc_due=(double *)calloc(loadsteps*2*qpoints*numElems,sizeof(double));
double *rhs=(double *)calloc(2*qpoints*numElems,sizeof(double));
double *rhs_j=(double *)calloc(2*qpoints*numElems,sizeof(double));
double *rhs_l=(double *)calloc(loadsteps*numNodes,sizeof(double));
double *dc_dxe=(double *)calloc(numElems,sizeof(double));


double C[9]={0}; double C1[9]={0};
double n_trial_stress_matrix[9]={0};
double Ue[MNE]={0}; double Ue_ini[MNE]={0};

double *Ke_elastic = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2 = (double *) malloc(MNE*MNE * sizeof(double));

double *Ke2_t = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_plastic = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_plastic_t = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_e = (double *) calloc(MNE*MNE * numElems,sizeof(double));
double *Ke_plastic = (double *) calloc(MNE*MNE * numElems,sizeof(double));
double *Ke_plastic_d;
checkCudaErrors(cudaMalloc((void **) &Ke_plastic_d, MNE*MNE * numElems* sizeof(double)));

//Gathering of all strain-displacement matrix for the different quadrature points for lighter coding

double BE[24],BE1[24],BE1_t[24],BE2[24],BE2_t[24], BE3[24],BE3_t[24], BE4[24],BE4_t[24];
double strain1[3]={0}; 	double strain1_ini[3]={0}; 	double diff_strain1[3]={0};
double trial_stress1[3]={0}; 	double n_trial_stress1[3]={0}; double dev_stress1[3]={0};
double norm_dev_stress1;

BE_matrix2(delta_x,delta_y,BE1,BE2,BE3,BE4,E,nu);
transMat(8,3,BE1,BE1_t); transMat(8,3,BE2,BE2_t); transMat(8,3,BE3,BE3_t); transMat(8,3,BE4,BE4_t);


double *BE_total = (double *) malloc(qpoints*3*2*qpoints * sizeof(double));

  for (i=0;i<3*2*qpoints;i++){
    BE_total[0*3*2*qpoints+i]=BE1[i];
    BE_total[1*3*2*qpoints+i]=BE2[i];
    BE_total[2*3*2*qpoints+i]=BE3[i];
    BE_total[3*3*2*qpoints+i]=BE4[i];

  }

//Material properties
matC(E,nu,C);
matC(1,nu,C1);

//Stiffness matrix
KE_elasticity(delta_x,delta_y,Ke_elastic,E,nu,C1);

//Deviatoric stress matrix
double DEV[9]={0};
DEV[0*3+0]=1.f/2;  DEV[0*3+1]=-1.f/2;
DEV[1*3+0]=-1.f/2; DEV[1*3+1]=1.f/2;
DEV[2*3+2]=1.f/2;

double ID_mat[9]={0};
ID_mat[0]=1; ID_mat[4]=1; ID_mat[8]=1;

double DEV_mat[9]={0};
DEV_mat[0*3+0]=1.f/2;  DEV_mat[0*3+1]=-1.f/2;
DEV_mat[1*3+0]=-1.f/2; DEV_mat[1*3+1]=1.f/2;
DEV_mat[2*3+2]=1;

cudaStream_t *streams;
streams = (cudaStream_t *)malloc(2 * sizeof(cudaStream_t));
checkCudaErrors(cudaStreamCreate(&(streams[0])));
checkCudaErrors(cudaStreamCreate(&(streams[1])));

double *rho_d,*U_d,*delta_U_d,*delta_lambda_d,*lambda_d, *norm_delta_U_d,*norm_Uold_d,*norm_Unew_d,
*norm_delta_lambda_d,*norm_lambda_old_d,*norm_lambda_new_d,
*r_d,*M_d, *prod_d, *prod_elastic_d, *prod_plastic_d, *f_d,*p_d,*z_d,*Ap_d, *Ap_elastic_d, *Ap_plastic_d;
int *fixednodes_d;

  //int *pos_heat_d;
  //double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double  *Ke_elastic_d, *Ke2_d; //z_d
  double *pAp_d;
  double *rznew_d,*rzold_d;
  double *rNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  double norme, norme1_forward,norme2_forward,norme3_forward,norme_newton_forward;
  char filename[500];

  double *norm_delta_U =(double *)calloc(numNodes,sizeof(double));
  double *norm_Unew =(double *)calloc(numNodes,sizeof(double));
  double *norm_Uold =(double *)calloc(numNodes,sizeof(double));

  double *norm_delta_lambda =(double *)calloc(numNodes,sizeof(double));
  double *norm_lambda_new =(double *)calloc(numNodes,sizeof(double));
  double *norm_lambda_old =(double *)calloc(numNodes,sizeof(double));

  //Fixed displacement
  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  int *fixednodes_x = (int *)calloc((ni+1)*(nj+1), sizeof(int));
  int *fixednodes_y = (int *)calloc((ni+1)*(nj+1), sizeof(int));

  if(nodal_force==0){
    j=0;//Fixed displacement at y=0;

      for (i=0;i<ni+1;i++){
        i0=I2D(ni2,i,j);
        fixednodes[2*i0]=1; // x-direction
        fixednodes[2*i0+1]=1; // y-direction
      }
  }
  else{
    i=0;//Fixed displacement at x=0;

      for (j=0;j<nj+1;j++){
        i0=I2D(ni2,i,j);
        fixednodes[2*i0]=1; // x-direction
        fixednodes[2*i0+1]=1; // y-direction
      }
  }



  //Nodal force
  double *fext =(double *)calloc(numNodes,sizeof(double));
  double *fint =(double *)calloc(numNodes,sizeof(double));
  double *f =(double *)calloc(numNodes,sizeof(double));

  double *U = (double *) calloc(numNodes , sizeof(double));
  double *U_l = (double *) calloc(loadsteps*numNodes , sizeof(double));
  double *Uini = (double *) calloc(numNodes , sizeof(double));
  double *Uold = (double *) calloc(numNodes , sizeof(double));
  double *delta_U = (double *) calloc(numNodes , sizeof(double));
  double *lambda = (double *) calloc(numNodes , sizeof(double));
  double *lambda_l = (double *) calloc(loadsteps*numNodes , sizeof(double));
  double *delta_lambda = (double *) calloc(numNodes , sizeof(double));
  double *lambda_old = (double *) calloc(numNodes , sizeof(double));
  double *FE_load=(double *)calloc(8*numElems,sizeof(double));

  //Nodal force at the bottom right corner
  for (j=8;j<=12;j++){
  i=ni-1;i0=I2D(ni,i,j);
  FE_load[8*i0+3]=nodal_force;
  }

  // Allocate memory
  cudaMalloc((void **) &Ke_elastic_d, MNE*MNE * sizeof(double));

  //additional variable for reduction operations
  cudaMalloc((void **) &pAp_d, 1 * sizeof(double));
  cudaMalloc((void **) &rznew_d, 1 * sizeof(double));
  cudaMalloc((void **) &rzold_d, 1 * sizeof(double));
  cudaMalloc((void **) &rNorm_d, 1 * sizeof(double));

  //Calculation of the element matrix
  cudaMemcpy(Ke_elastic_d, Ke_elastic, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);

  //Elastic strain predictor
  double *dn_ds= (double *)calloc(numElems*9*qpoints,sizeof(double));
  double *dn_ds_l= (double *)calloc(loadsteps*numElems*9*qpoints,sizeof(double));
  double *dH_deta_l= (double *)calloc(loadsteps*numElems*9*qpoints,sizeof(double));
  double *n_trial_stress= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *n_trial_stress_l= (double *)calloc(loadsteps*numElems*3*qpoints,sizeof(double));
  double *dgamma= (double *)calloc(numElems*qpoints,sizeof(double));
  double *strain= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *strain_ini= (double *)calloc(loadsteps*numElems*3*qpoints,sizeof(double));
  double *plastic_strain= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *hardening= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *old_plastic_strain= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *old_plastic_strain_l= (double *)calloc(loadsteps*numElems*3*qpoints,sizeof(double));
  double *old_hardening= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *stress= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *deviatoric_stress= (double *)calloc(numElems*3*qpoints,sizeof(double));
  double *consistent_matrix= (double *)calloc(numElems*9*qpoints,sizeof(double));
  bool *plasticity_nodes=(bool *)calloc(numElems*qpoints,sizeof(bool));
  bool *plasticity_nodes_l=(bool *)calloc(loadsteps*numElems*qpoints,sizeof(bool));
  double *norm_dev_stress_l=(double *)calloc(loadsteps*numElems*qpoints,sizeof(double));
  double *criteria_vms=(double *)calloc(numElems*qpoints,sizeof(double));
  bool *plasticity_elements=(bool *)calloc(numElems,sizeof(bool));
  bool *plasticity_elements_d;  cudaMalloc((void **) &plasticity_elements_d, numElems * sizeof(bool));
  int *map_plastic=(int *)calloc(numElems,sizeof(int));
  int *map_plastic_d;  cudaMalloc((void **) &map_plastic_d, numElems * sizeof(int));
  double *FE_int=(double *)calloc(2*qpoints*numElems,sizeof(double));

  checkCudaErrors(cudaMalloc((void **) &yphys_d, numElems * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &rho_d, numElems * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &delta_lambda_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &lambda_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &delta_U_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_delta_U_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_Unew_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_Uold_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_delta_lambda_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_lambda_new_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_lambda_old_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &U_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &f_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &M_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &r_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_elastic_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_plastic_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &p_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &z_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_elastic_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_plastic_d, numNodes * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &fixednodes_d, numNodes * sizeof(int)));
  checkCudaErrors(cudaMalloc((void **) &Ke2_d, numElems*MNE*MNE * sizeof(double)));

    int NBLOCKS   = (numElems+BLOCKSIZE1-1) / BLOCKSIZE1; //round up if n is not a multiple of blocksize
    int NBLOCKS2 = (numNodes+BLOCKSIZE2-1) / BLOCKSIZE2; //round up if n is not a multiple of blocksize


if (finite_difference==1){


    double eps=1e-6;
    i1=0;
    while(i1<ni){
      j1=0;

      while (j1<nj1){
          i0=I2D(ni,i1,j1);
          //if(i0>=0){
           if(i0==0 || i0==1 || i0==58 || i0==59 || i0==600 || i0==601 ){
        cpt=1;
              while(cpt<=2){

                for (e=0;e<numElems_opt;e++){
                  i0=I2D(ni,i1,j1);
                  rho[e]=volfrac;
                  if(cpt==1 && e==i0) rho[e]=volfrac+eps;
                  if(cpt==2 && e==i0) rho[e]=volfrac-eps;
                }

                //CONJUGATE GRADIENT BEGINNING

                //Transfer to GPU

                checkCudaErrors(cudaMemcpy(rho_d, rho, numElems * sizeof(double),cudaMemcpyHostToDevice));
                checkCudaErrors(cudaMemset(U_d, 0.0, numNodes*sizeof(double)));

                for(m=0;m<3*qpoints*numElems;m++){
                old_hardening[m]=0;
                old_plastic_strain[m]=0;
                stress[m]=0;
                }


                for(m=0;m<3*qpoints*numElems*loadsteps;m++) strain_ini[m]=0;

                for(m=0;m<3*numElems*loadsteps;m++) inh_strain2_l[m]=0;

                for (n=0;n<numNodes;n++) {
                  U[n]=0;
                }

                /////////////////////////////////////////////////////////////////////
                min_stress=0;

                for(e=0;e<numElems;e++) yphys[e]=E*Emin;

                for(layer=0;layer<nb_layers;layer++){

                  for(e=0;e<numElems;e++) initial_elements[e]=0;
                  for(i=0;i<numNodes;i++) Uini[i]=U[i];

                  printf("active jmax=%d, cpt=%d\n",layer_height*(layer+1),cpt);

                  for(j=layer_height*layer;j<layer_height*(layer+1);j++){
                    for(i=0;i<ni;i++){
                      i02=I2D(ni,i,j);
                      yphys[i02]=E;
                      initial_elements[i02]=1;
                    }
                  }

                  checkCudaErrors(cudaMemcpy(yphys_d, yphys, numElems * sizeof(double), cudaMemcpyHostToDevice));

                for(load1=0;load1<loadsteps1;load1++){

                  l=loadsteps1*layer+load1;

                  printf("\n\n\n Simulation %d/%d : layer %d ;load step =%d \n\n\n",1+j1+nj1*i1,ni*nj1,layer,load1);

                  double ratio=(double)(load1+1)/loadsteps1;
                  double ratio2=ratio;

                  for (m=0;m<3;m++) inh_strain2[m]=ratio2*inh_strain[m];

                  int iter_newton;

                  for(iter_newton=0; iter_newton<Max_iters_newton; iter_newton++)
                  {

                    for (n=0;n<numNodes;n++) fext[n]=0;

                    for(e=0;e<numElems;e++){

                      initial_elements_l[l*numElems+e]=initial_elements[e];

                        Ge=pow(rho[e],penal)*G;
                        yield_stress_e=pow(rho[e],penal)*yield_stress;
                        hard_constant_e=pow(rho[e],penal)*hard_constant;

                      j=e/ni;
                      i=e%ni;

                      id=2*((ni+1)*j+i);
                      id2=(ni+1)*j+i;

                      if(j<layer_height*(layer+1)){

                      edofMat[0]=id;//x1
                      edofMat[1]=id+1;//y1
                      edofMat[2]=id+2;//x2
                      edofMat[3]=id+3;//y2
                      edofMat[4]=id+2+2*(ni+1);//x3
                      edofMat[5]=id+3+2*(ni+1);//y3
                      edofMat[6]=id+0+2*(ni+1); //x4
                      edofMat[7]=id+1+2*(ni+1); //y4

                      for (m=0;m<8;m++) {
                        Ue[m]=U[edofMat[m]];
                        Ue_ini[m]=Uini[edofMat[m]];
                        fext[edofMat[m]]=fext[edofMat[m]]+ratio*FE_load[8*e+m];
                        }

                      for (q=0;q<qpoints;q++){

                        for (i=0;i<3*2*qpoints;i++){
                          BE[i]=BE_total[q*3*2*qpoints+i];
                        }

                        prodMatVec(2*qpoints,3,2*qpoints,BE,Ue,strain1);
                        prodMatVec(2*qpoints,3,2*qpoints,BE,Ue_ini,strain1_ini);


                    for (m=0;m<3;m++) {
                      if(layer>0){
                      if(initial_elements[e]==1 && load1==0)    {
                        strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m]=strain_ini[3*qpoints*numElems*(l-1)+qpoints*3*e+3*q+m]+strain1_ini[m];
                      }
                      else{
                        strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m]=strain_ini[3*qpoints*numElems*(l-1)+qpoints*3*e+3*q+m];
                      }
                      }
                      if(layer==0){
                        inh_strain2_l[3*numElems*l+3*e+m]=inh_strain2[m];
                      }
                      else{
                        inh_strain2_l[3*numElems*l+3*e+m]=(j<layer_height*layer)*inh_strain[m]+(j>=layer_height*layer)*inh_strain2[m];
                      }


                      diff_strain1[m]=strain1[m]-old_plastic_strain[qpoints*3*e+3*q+m]-inh_strain2_l[3*numElems*l+3*e+m]
                      -strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m];
                    }

                    //Stress trial with previous strain
                    prodMatVec_e(3,3,3,C,rho[e],penal,diff_strain1,trial_stress1);


                      //Deviatoric stress : from trial stress
                      dev_stress1[0]=trial_stress1[0]-0.5*(trial_stress1[0]+trial_stress1[1]);
                    dev_stress1[1]=trial_stress1[1]-0.5*(trial_stress1[0]+trial_stress1[1]);
                    dev_stress1[2]=trial_stress1[2];

                    //Deviatoric stress : Substract hardening Variable
                    for (m=0;m<3;m++) {
                      dev_stress1[m]=dev_stress1[m]-old_hardening[qpoints*3*e+3*q+m];
                    }

                    norm_dev_stress1=sqrt(dev_stress1[0]*dev_stress1[0]+dev_stress1[1]*dev_stress1[1]
                    +2*(dev_stress1[2]*dev_stress1[2]));


                    //Stress update
                    if(norm_dev_stress1-yield_stress_e<=0){
                        plasticity_nodes[qpoints*e+q]=0;
                      for (m=0;m<3;m++)   {
                        stress[qpoints*3*e+3*q+m]=trial_stress1[m];
                        plastic_strain[qpoints*3*e+3*q+m]=old_plastic_strain[qpoints*3*e+3*q+m];
                        hardening[qpoints*3*e+3*q+m]=old_hardening[qpoints*3*e+3*q+m];
                      }
                      for (m=0;m<9;m++)  consistent_matrix[qpoints*9*e+9*q+m]=pow(rho[e],penal)*C[m];
                      for (m=0;m<3;m++)  deviatoric_stress[qpoints*3*e+3*q+m]=dev_stress1[m];
                    }
                    else{
                        plasticity_nodes[qpoints*e+q]=1;
                    for (m=0;m<3;m++) {
                      n_trial_stress1[m]=dev_stress1[m]/norm_dev_stress1;
                      n_trial_stress[qpoints*3*e+3*q+m]=n_trial_stress1[m];

                      stress[qpoints*3*e+3*q+m]=trial_stress1[m]-(double)(2*Ge*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

                      hardening[qpoints*3*e+3*q+m]=old_hardening[qpoints*3*e+3*q+m]
                      +(double)(hard_constant_e*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
                    plastic_strain[qpoints*3*e+3*q+m]=old_plastic_strain[qpoints*3*e+3*q+m]
                      +(1*(m<=1)+2*(m>=2))*(double)((norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
                      deviatoric_stress[qpoints*3*e+3*q+m]=dev_stress1[m];
                    }
                    for (m1=0;m1<3;m1++) {
                      for (m2=0;m2<3;m2++) {
                        n_trial_stress_matrix[m2+3*m1]=n_trial_stress1[m1]*n_trial_stress1[m2];
                      }
                  }
                for (m=0;m<9;m++)   {
                  consistent_matrix[qpoints*9*e+9*q+m]=pow(rho[e],penal)*C[m]-(double)4*Ge*Ge*DEV[m]/(2*Ge+hard_constant_e)+
                (double)4*Ge*Ge*(DEV[m]-n_trial_stress_matrix[m])*(double)(yield_stress_e/norm_dev_stress1)/(2*Ge+hard_constant_e);
              }
                }
              }
            }
            else{

              edofMat[0]=id;//x1
              edofMat[1]=id+1;//y1
              edofMat[2]=id+2;//x2
              edofMat[3]=id+3;//y2
              edofMat[4]=id+2+2*(ni+1);//x3
              edofMat[5]=id+3+2*(ni+1);//y3
              edofMat[6]=id+0+2*(ni+1); //x4
              edofMat[7]=id+1+2*(ni+1); //y4

              for (m=0;m<8;m++) {
                Ue[m]=U[edofMat[m]];
              }

                for (q=0;q<qpoints;q++){

              for (i=0;i<3*2*qpoints;i++){
                  BE[i]=BE_total[q*3*2*qpoints+i];
                }

              prodMatVec(2*qpoints,3,2*qpoints,BE,Ue,strain1);

              prodMatVec_e(3,3,3,C1,rho[e],penal,diff_strain1,trial_stress1);

              for (m=0;m<3;m++) diff_strain1[m]=strain1[m];


              for (m=0;m<9;m++)  {
                consistent_matrix[qpoints*9*e+9*q+m]=yphys[e]*pow(rho[e],penal)*C1[m];
              }
              for (m=0;m<3;m++) stress[qpoints*3*e+3*q+m]=0*yphys[e]*trial_stress1[m];
            }

            }
              }


                    nb_plastic=0;
                    nb_plastic_nodes=0;
                    for(e=0;e<numElems;e++){
                      plasticity=0;
                      plasticity_elements[e]=0;
                      for(m=0;m<qpoints;m++){
                      if(plasticity_nodes[qpoints*e+m]==1){
                          //  printf("Element %d  : norm_dev_stress1=%lg, yield_stress=%lg\n",e,norm_dev_stress1,yield_stress);
                        plasticity=1;
                        nb_plastic_nodes++;
                      }
                      }
                      if(plasticity==1) {
                        plasticity_elements[e]=1;
                        nb_plastic++;
                      }

                    }

                  printf("Plasticity : %d elements and %d nodes\n",nb_plastic,nb_plastic_nodes);

                  KE_plasticity3(delta_x,delta_y,numElems,consistent_matrix,
                    BE1,BE2,BE3,BE4,Ke_plastic);

                  checkCudaErrors(cudaMemcpy(Ke_plastic_d, Ke_plastic, MNE*MNE * numElems *sizeof(double),cudaMemcpyHostToDevice));
                  checkCudaErrors(cudaMemcpy(plasticity_elements_d, plasticity_elements, numElems *sizeof(bool),cudaMemcpyHostToDevice));



                  if(forward_problem==1){

                    //FORWARD PROBLEM
                  FE_internal_forces2(delta_x,delta_y,BE1_t,BE2_t,BE3_t,BE4_t,numElems,stress,FE_int);

                  for(n=0;n<numNodes;n++) fint[n]=0;

                  for(e=0;e<numElems;e++){
                    j=e/ni;
                    i=e%ni;

                    id=2*((ni+1)*j+i);
                    id2=(ni+1)*j+i;

                    edofMat[0]=id;//x1
                    edofMat[1]=id+1;//y1
                    edofMat[2]=id+2;//x2
                    edofMat[3]=id+3;//y2
                    edofMat[4]=id+2+2*(ni+1);//x3
                    edofMat[5]=id+3+2*(ni+1);//y3
                    edofMat[6]=id+0+2*(ni+1); //x4
                    edofMat[7]=id+1+2*(ni+1); //y4

                    for(m=0;m<8;m++){
                      fint[edofMat[m]]=fint[edofMat[m]]+FE_int[8*e+m];
                      }
                  }

                  for (n=0;n<numNodes;n++) {
                    f[n]=fext[n]-fint[n];
                          }

                  checkCudaErrors(cudaMemcpy(f_d, f, numNodes * sizeof(double),cudaMemcpyHostToDevice));
                  checkCudaErrors(cudaMemcpy(fixednodes_d, fixednodes, numNodes * sizeof(int),cudaMemcpyHostToDevice));

                checkCudaErrors(cudaMemset(delta_U_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemset(prod_elastic_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemset(prod_plastic_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
                GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1>>>(ni,numElems,Ke_plastic_d,delta_U_d,prod_d);

                cudaMemset(M_d, 0.0, numNodes*sizeof(double));
                GPUMatVec_precond<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems,rho_d,yphys_d,penal,Ke_elastic_d, M_d);

                checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
                rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_d, M_d,r_d,prod_d, z_d, p_d, numNodes, fixednodes_d);

                cudaMemset(rzold_d, 0.0, 1*sizeof(double));
                Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_d, r_d,numNodes,fixednodes_d,rzold_d);

                checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemset(Ap_elastic_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemset(Ap_plastic_d, 0.0, numNodes*sizeof(double)));
                checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
                printf("Init : rzold=%lg  \n",rznew[0]);

                rNorm[0]=10000;

                  //==============PCG Iterations Start Here=========================//
                for(iter=0; iter<Max_iters; iter++)
                {

                  checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
                  GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, Ke_plastic_d,p_d, Ap_d);

                  Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, Ap_d,numNodes,fixednodes_d,pAp_d,rNorm_d);

                  Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_d,delta_U_d, r_d, z_d, p_d, Ap_d,numNodes,fixednodes_d, rzold_d,pAp_d,rznew_d,rNorm_d);

                  UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, z_d, numNodes,fixednodes_d,rzold_d,rznew_d,Ap_elastic_d, Ap_plastic_d);

                  switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

                  checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

                  norme=sqrt(rNorm[0]);
                  //if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
                  checkCudaErrors(cudaStreamSynchronize(streams[0]));

                  if(norme <= tol) break;

              }

              if(iter==Max_iters){
                printf("Residual error in PCG: norme=%lg : end of program \n",norme);
                return(0);
              }

              checkCudaErrors(cudaMemset(norm_delta_U_d, 0.0, numNodes*sizeof(double)));
              checkCudaErrors(cudaMemset(norm_Uold_d, 0.0, numNodes*sizeof(double)));
              checkCudaErrors(cudaMemset(norm_Unew_d, 0.0, numNodes*sizeof(double)));

              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d,penal, yphys_d,Ke_elastic_d,delta_U_d, norm_delta_U_d);
              checkCudaErrors(cudaMemcpy(delta_U, delta_U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_delta_U, norm_delta_U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d, penal, yphys_d,Ke_elastic_d,U_d, norm_Uold_d);
              checkCudaErrors(cudaMemcpy(Uold, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_Uold, norm_Uold_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
              Update_Newton<<<NBLOCKS2,BLOCKSIZE2>>>(numNodes,delta_U_d,U_d);
              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d, penal, yphys_d,Ke_elastic_d,U_d, norm_Unew_d);
              checkCudaErrors(cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_Unew, norm_Unew_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));

              norme1_forward=0;
              norme2_forward=0;
              norme3_forward=0;
              for(m=0;m<numNodes;m++) {
                norme1_forward=norme1_forward+delta_U[m]*norm_delta_U[m];
                norme2_forward=norme2_forward+Uold[m]*norm_Uold[m];
                norme3_forward=norme3_forward+U[m]*norm_Unew[m];
              }

              norme1_forward=sqrt(norme1_forward);
              norme2_forward=sqrt(norme2_forward);
              norme3_forward=sqrt(norme3_forward);

              norme_newton_forward=(double) norme1_forward/(norme2_forward+norme3_forward);
              printf("FORWARD solution : Newton steps : %d , Newton residual=%lg, PCG steps : %d, PCG residual=%lg\n",iter_newton,norme_newton_forward, iter,norme);

              //END OF FORWARD PROBLEM
              }


              if(norme_newton_forward <= tol_newton  || norme3_forward<=tol_newton ) break;

              }//END NEWTON

              if(iter_newton==Max_iters_newton){
                printf("Residual error in Newton algorithm: end of program \n");
                return(0);
              }

            for (n=0;n<numNodes;n++) U_l[numNodes*l+n]=U[n];
            int cpt2=0;
          for (i=0;i<(ni+1)*(nj+1);i++){
          ux[(ni+1)*(nj+1)*l+i]=U_l[numNodes*l+cpt2];  cpt2++;
          uy[(ni+1)*(nj+1)*l+i]=U_l[numNodes*l+cpt2];   cpt2++;
          }
          printf("l=%d\n",l);
              for(m=0;m<3*qpoints*numElems;m++){
              old_hardening[m]=hardening[m];
              old_plastic_strain[m]=plastic_strain[m];
              }

              //Gradient Calculation

              somme=0;

              for(e=0;e<numElems;e++){
              stress_xx_e[numElems*l+e]=0;
              stress_yy_e[numElems*l+e]=0;
              stress_xy_e[numElems*l+e]=0;


                for (m=0;m<4;m++){
                stress_xx_e[numElems*l+e]=stress_xx_e[numElems*l+e]+0.25*stress[4*3*e+3*m+0];
                stress_yy_e[numElems*l+e]=stress_yy_e[numElems*l+e]+0.25*stress[4*3*e+3*m+1];
                stress_xy_e[numElems*l+e]=stress_xy_e[numElems*l+e]+0.25*stress[4*3*e+3*m+2];
              }
              von_mises_stress[numElems*l+e]=sqrt(stress_xx_e[numElems*l+e]*stress_xx_e[numElems*l+e]+stress_yy_e[numElems*l+e]*stress_yy_e[numElems*l+e]
              -stress_xx_e[numElems*l+e]*stress_yy_e[numElems*l+e]+3*stress_xy_e[numElems*l+e]*stress_xy_e[numElems*l+e]);

                somme=somme+pow(von_mises_stress[numElems*l+e],pnorm);
              }


                if(nodal_force!=0){

                  for (n=0;n<numNodes;n++){
                    min_stress=min_stress+(double)fext[n]*U[n]/loadsteps;
                  }
                }


              }//END LOADSTEPS

            }//END LAYER

            if(nodal_force==0){
            pnorm1=1.f/pnorm;
            somme=(double)somme/numElems;
            min_stress=pow(somme,pnorm1);
            printf("min_stress=%lg\n",min_stress);
          }

    if (cpt==1) cost_function1=min_stress;
    if (cpt==2) cost_function2=min_stress;

    cpt++;
    }

        }
    gradient_FD[i0]=(double)(cost_function1-cost_function2)/(2*eps);
    j1++;
    }
    i1++;
    }

    printf("gradient_FD[0]=%.8lg\n",gradient_FD[0]);
    printf("gradient_FD[1]=%.8lg\n",gradient_FD[1]);
    printf("gradient_FD[58]=%.8lg\n",gradient_FD[58]);
    printf("gradient_FD[59]=%.8lg\n",gradient_FD[59]);
    printf("gradient_FD[600]=%.8lg\n",gradient_FD[600]);
    printf("gradient_FD[601]=%.8lg\n",gradient_FD[601]);

    fichier=fopen("gradient_FD.vtk","w"); memset(filename, 0, sizeof(filename));
    fprintf(fichier, "# vtk DataFile Version 3.0\n");
    fprintf(fichier, "VTK from C\n");
    fprintf(fichier,"ASCII\n");
    fprintf(fichier,"DATASET UNSTRUCTURED_GRID\n");
    fprintf(fichier,"POINTS %d float\n",(nj+1)*(ni+1));
    for (j=0;j<nj+1;j++){
        for (i=0;i<ni+1;i++){
        fprintf(fichier,"%f %f %f\n",i*0.01,j*0.01,1.f);
      }
    }
    fprintf(fichier,"CELLS %d %d\n",ni*nj,5*ni*nj); //5 for 4 (node index) +1 (number of nodes per cell)
    for (i=0;i<ni*nj;i++){
      fprintf(fichier,"4 %d %d %d %d\n",edofMat2[4*i+0],edofMat2[4*i+1],edofMat2[4*i+3],edofMat2[4*i+2]);
    }
    fprintf(fichier,"CELL_TYPES %d\n",ni*nj);
    for (i=0;i<numElems;i++){
    fprintf(fichier,"8\n"); // 8 for pixel (2D) https://www.kitware.com/products/books/VTKUsersGuide.pdf
    }
    fprintf(fichier,"CELL_DATA %d\n",numElems);
    fprintf(fichier,"SCALARS gradient_FD double\n");
    fprintf(fichier,"LOOKUP_TABLE default\n");
      for (j=0;j<nj;j++){
        for (i=0;i<ni;i++){
              i0=I2D(ni,i,j);
          fprintf(fichier,"%lg ",gradient_FD[i0]);
        }
      }
      fprintf(fichier,"POINT_DATA %d\n",(ni+1)*(nj1+1));
      fprintf(fichier,"SCALARS ux double\n");
      fprintf(fichier,"LOOKUP_TABLE default\n");
        for (j=0;j<nj1+1;j++){
          for (i=0;i<ni+1;i++){
                i0=I2D(ni2,i,j);
            fprintf(fichier,"%lg ",ux[i0]);
          }
        }
        fprintf(fichier,"SCALARS uy double\n");
        fprintf(fichier,"LOOKUP_TABLE default\n");
          for (j=0;j<nj1+1;j++){
            for (i=0;i<ni+1;i++){
                  i0=I2D(ni2,i,j);
              fprintf(fichier,"%lg ",uy[i0]);
            }
          }
    fclose(fichier);

    checkCudaErrors(cudaMemset(U_d, 0.0, numNodes*sizeof(double)));

    rho_definition(ni,nj,nj1,rho,volfrac,map_gradient);

}


for (int opt=0;opt<opt_steps;opt++){

  printf("\n********************************************\n");
  printf("\n          OPTIMIZATION STEP %d              \n",opt);
  printf("\n********************************************\n");

  //CONJUGATE GRADIENT BEGINNING

  //Transfer to GPU

  checkCudaErrors(cudaMemcpy(rho_d, rho, numElems * sizeof(double),cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemset(U_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(lambda_d, 0.0, numNodes*sizeof(double)));

  for(m=0;m<3*qpoints*numElems;m++){
  old_hardening[m]=0;
  old_plastic_strain[m]=0;
  hardening[m]=0;
  plastic_strain[m]=0;
  stress[m]=0;
  }

  for(m=0;m<qpoints*numElems;m++) plasticity_nodes[m]=0;
  for(m=0;m<9*qpoints*numElems;m++) consistent_matrix[m]=0;
  for(m=0;m<3*qpoints*numElems*loadsteps;m++) strain_ini[m]=0;
  for(m=0;m<3*qpoints*numElems*loadsteps;m++) old_plastic_strain_l[m]=0;
  for(m=0;m<3*numElems*loadsteps;m++) inh_strain2_l[m]=0;
  for(m=0;m<numElems*loadsteps;m++) initial_elements_l[m]=0;
  for(m=0;m<9*qpoints*numElems*loadsteps;m++) dn_ds_l[m]=0;
  for(m=0;m<9*qpoints*numElems*loadsteps;m++) dH_deta_l[m]=0;
  for(m=0;m<qpoints*numElems*loadsteps;m++) plasticity_nodes_l[m]=0;

  for (n=0;n<numNodes;n++) {
    U[n]=0;
  }

  /////////////////////////////////////////////////////////////////////

  for(e=0;e<numElems;e++) yphys[e]=E*Emin;

  for(layer=0;layer<nb_layers;layer++){

    for(e=0;e<numElems;e++) initial_elements[e]=0;
    for(i=0;i<numNodes;i++) Uini[i]=U[i];

    for(j=layer_height*layer;j<layer_height*(layer+1);j++){
      for(i=0;i<ni;i++){
        i02=I2D(ni,i,j);
        yphys[i02]=E;
        initial_elements[i02]=1;
      }
    }

    checkCudaErrors(cudaMemcpy(yphys_d, yphys, numElems * sizeof(double), cudaMemcpyHostToDevice));

  for(load1=0;load1<loadsteps1;load1++){

    l=loadsteps1*layer+load1;

    for(e=0;e<numElems;e++) yphys_l[l*numElems+e]=yphys[e];

    printf("\n\n\n layer %d, load step =%d \n\n\n",layer,load1);

     ratio=(double)(load1+1)/loadsteps1;
     ratio2=ratio;

    for (m=0;m<3;m++) {
      inh_strain2[m]=ratio2*inh_strain[m];

    }

    int iter_newton;

    for(iter_newton=0; iter_newton<Max_iters_newton; iter_newton++)
    {

      for (n=0;n<numNodes;n++) fext[n]=0;

      for(e=0;e<numElems;e++){

        initial_elements_l[l*numElems+e]=initial_elements[e];

          Ge=pow(rho[e],penal)*G;
          yield_stress_e=pow(rho[e],penal)*yield_stress;
          hard_constant_e=pow(rho[e],penal)*hard_constant;

        j=e/ni;
        i=e%ni;

        id=2*((ni+1)*j+i);
        id2=(ni+1)*j+i;

        if(j<layer_height*(layer+1)){

        edofMat[0]=id;//x1
        edofMat[1]=id+1;//y1
        edofMat[2]=id+2;//x2
        edofMat[3]=id+3;//y2
        edofMat[4]=id+2+2*(ni+1);//x3
        edofMat[5]=id+3+2*(ni+1);//y3
        edofMat[6]=id+0+2*(ni+1); //x4
        edofMat[7]=id+1+2*(ni+1); //y4

        for (m=0;m<8;m++) {
          Ue[m]=U[edofMat[m]];
          Ue_ini[m]=Uini[edofMat[m]];
          fext[edofMat[m]]=fext[edofMat[m]]+ratio*FE_load[8*e+m];
          }

        for (q=0;q<qpoints;q++){

          for (i=0;i<3*2*qpoints;i++){
            BE[i]=BE_total[q*3*2*qpoints+i];
          }

          prodMatVec(2*qpoints,3,2*qpoints,BE,Ue,strain1);
          prodMatVec(2*qpoints,3,2*qpoints,BE,Ue_ini,strain1_ini);


      for (m=0;m<3;m++) {
        if(layer>0){
        if(initial_elements[e]==1 && load1==0)    {
          strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m]=strain_ini[3*qpoints*numElems*(l-1)+qpoints*3*e+3*q+m]+strain1_ini[m];
        }
        else{
          strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m]=strain_ini[3*qpoints*numElems*(l-1)+qpoints*3*e+3*q+m];
        }
        }

        if(layer==0){
          inh_strain2_l[3*numElems*l+3*e+m]=inh_strain2[m];
        }
        else{
          inh_strain2_l[3*numElems*l+3*e+m]=(j<layer_height*layer)*inh_strain[m]+(j>=layer_height*layer)*inh_strain2[m];
        }

        diff_strain1[m]=strain1[m]-old_plastic_strain[qpoints*3*e+3*q+m]-inh_strain2_l[3*numElems*l+3*e+m]
        -strain_ini[3*qpoints*numElems*l+qpoints*3*e+3*q+m];
      }

      //Stress trial with previous strain
      prodMatVec_e(3,3,3,C,rho[e],penal,diff_strain1,trial_stress1);

      //Deviatoric stress : from trial stress

      dev_stress1[0]=trial_stress1[0]-0.5*(trial_stress1[0]+trial_stress1[1]);
      dev_stress1[1]=trial_stress1[1]-0.5*(trial_stress1[0]+trial_stress1[1]);
      dev_stress1[2]=trial_stress1[2];


      //Deviatoric stress : Substract hardening Variable
      for (m=0;m<3;m++) {
        dev_stress1[m]=dev_stress1[m]-old_hardening[qpoints*3*e+3*q+m];
      }

      norm_dev_stress1=sqrt(dev_stress1[0]*dev_stress1[0]+dev_stress1[1]*dev_stress1[1]
      +2*(dev_stress1[2]*dev_stress1[2]));

      norm_dev_stress_l[l*qpoints*numElems+qpoints*e+q]=norm_dev_stress1;

      //if(e==63 || e==64) printf("q=%d, norm_dev_stress1=%lg\n",q,norm_dev_stress1);

      //Stress update
      if(norm_dev_stress1-yield_stress_e<=0){
        dgamma[qpoints*e+q]=0;
          plasticity_nodes[qpoints*e+q]=0;
          plasticity_nodes_l[numElems*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
        for (m=0;m<3;m++)   {
          stress[qpoints*3*e+3*q+m]=trial_stress1[m];
          hardening[qpoints*3*e+3*q+m]=old_hardening[qpoints*3*e+3*q+m];
          plastic_strain[qpoints*3*e+3*q+m]=old_plastic_strain[qpoints*3*e+3*q+m];
        }
        for (m=0;m<9;m++)  consistent_matrix[qpoints*9*e+9*q+m]=pow(rho[e],penal)*C[m];
        for (m=0;m<3;m++)  deviatoric_stress[qpoints*3*e+3*q+m]=dev_stress1[m];
      }
      else{
          plasticity_nodes[qpoints*e+q]=1;
          plasticity_nodes_l[numElems*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
          //dgamma[qpoints*e+q]=(norm_dev_stress1-yield_stress_e)/(2*Ge+hard_constant_e);
          //if(e==1) printf("q=%d, dgamma=%lg\n",q,dgamma[qpoints*e+q]);
      for (m=0;m<3;m++) {
        n_trial_stress1[m]=dev_stress1[m]/norm_dev_stress1;
        n_trial_stress[qpoints*3*e+3*q+m]=n_trial_stress1[m];
        n_trial_stress_l[l*numElems*qpoints*3+qpoints*3*e+3*q+m]=n_trial_stress[qpoints*3*e+3*q+m];

        stress[qpoints*3*e+3*q+m]=trial_stress1[m]-(double)(2*Ge*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

        deviatoric_stress[qpoints*3*e+3*q+m]=dev_stress1[m];
        hardening[qpoints*3*e+3*q+m]=old_hardening[qpoints*3*e+3*q+m]
        +(double)(hard_constant_e*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
        plastic_strain[qpoints*3*e+3*q+m]=old_plastic_strain[qpoints*3*e+3*q+m]
        +(1*(m<=1)+2*(m>=2))*(double)((norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

      }
      n1=n_trial_stress[qpoints*3*e+3*q+0];
      n2=n_trial_stress[qpoints*3*e+3*q+1];
      n3=n_trial_stress[qpoints*3*e+3*q+2];

      dp1_ds1=1-yield_stress_e*(1-n1*n1)/norm_dev_stress1;
      dp1_ds2=yield_stress_e*n1*n2/norm_dev_stress1;
      dp1_ds3=2*yield_stress_e*n1*n3/norm_dev_stress1;

      dp2_ds2=1-yield_stress_e*(1-n2*n2)/norm_dev_stress1;
      dp2_ds1=yield_stress_e*n1*n2/norm_dev_stress1;
      dp2_ds3=2*yield_stress_e*n2*n3/norm_dev_stress1;

      dp3_ds1=yield_stress_e*n1*n3/norm_dev_stress1;
      dp3_ds2=yield_stress_e*n2*n3/norm_dev_stress1;
      dp3_ds3=1-yield_stress_e*((1-2*n3*n3)/norm_dev_stress1);

      ds1_dstr1=0.5;  ds2_dstr1=-0.5;
      ds1_dstr2=-0.5; ds2_dstr2=0.5;
      ds3_dstr3=1;

      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+0]=dp1_ds1*ds1_dstr1+dp1_ds2*ds2_dstr1;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+1]=dp1_ds1*ds1_dstr2+dp1_ds2*ds2_dstr2;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+2]=dp1_ds3*ds3_dstr3;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+0]=dp2_ds1*ds1_dstr1+dp2_ds2*ds2_dstr1;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+1]=dp2_ds1*ds1_dstr2+dp2_ds2*ds2_dstr2;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+2]=dp2_ds3*ds3_dstr3;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+0]=dp3_ds1*ds1_dstr1+dp3_ds2*ds2_dstr1;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+1]=dp3_ds1*ds1_dstr2+dp3_ds2*ds2_dstr2;
      dn_ds_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+2]=dp3_ds3*ds3_dstr3;

      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+0]=dp1_ds1;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+1]=dp1_ds2;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*0+2]=dp1_ds3;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+0]=dp2_ds1;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+1]=dp2_ds2;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*1+2]=dp2_ds3;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+0]=dp3_ds1;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+1]=dp3_ds2;
      dH_deta_l[9*qpoints*numElems*l+9*qpoints*e+9*q+3*2+2]=dp3_ds3;

    for (m1=0;m1<3;m1++) {
        for (m2=0;m2<3;m2++) {
          n_trial_stress_matrix[m2+3*m1]=n_trial_stress1[m1]*n_trial_stress1[m2];
      }
    }
  for (m=0;m<9;m++)   {
    consistent_matrix[qpoints*9*e+9*q+m]=pow(rho[e],penal)*C[m]-(double)4*Ge*Ge*DEV[m]/(2*Ge+hard_constant_e)+
  (double)4*Ge*Ge*(DEV[m]-n_trial_stress_matrix[m])*(double)(yield_stress_e/norm_dev_stress1)/(2*Ge+hard_constant_e);
}
  }
}
      }
      else{
          for (q=0;q<qpoints;q++){
            for (m=0;m<9;m++)    consistent_matrix[qpoints*9*e+9*q+m]=pow(rho[e],penal)*yphys[e]*C1[m];
            plasticity_nodes[qpoints*e+q]=0;
            plasticity_nodes_l[numElems*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
          }
      }
    }

      nb_plastic=0;
      nb_plastic_nodes=0;
      for(e=0;e<numElems;e++){
        plasticity=0;
        plasticity_elements[e]=0;
        for(m=0;m<qpoints;m++){
        if(plasticity_nodes[qpoints*e+m]==1){
            //  printf("Element %d  : norm_dev_stress1=%lg, yield_stress=%lg\n",e,norm_dev_stress1,yield_stress);
          plasticity=1;
          nb_plastic_nodes++;
        }
        }
        if(plasticity==1) {
          plasticity_elements[e]=1;
          nb_plastic++;
        }

      }

    printf("Plasticity : %d elements and %d nodes\n",nb_plastic,nb_plastic_nodes);


    KE_plasticity3(delta_x,delta_y,numElems,consistent_matrix, BE1,BE2,BE3,BE4,Ke_plastic);

    checkCudaErrors(cudaMemcpy(Ke_plastic_d, Ke_plastic, MNE*MNE * numElems *sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(plasticity_elements_d, plasticity_elements, numElems *sizeof(bool),cudaMemcpyHostToDevice));


    if(forward_problem==1){

      //FORWARD PROBLEM
    FE_internal_forces2(delta_x,delta_y,BE1_t,BE2_t,BE3_t,BE4_t,numElems,stress,FE_int);

    for(n=0;n<numNodes;n++) fint[n]=0;

    for(e=0;e<numElems;e++){
      j=e/ni;
      i=e%ni;

      id=2*((ni+1)*j+i);
      id2=(ni+1)*j+i;

      edofMat[0]=id;//x1
      edofMat[1]=id+1;//y1
      edofMat[2]=id+2;//x2
      edofMat[3]=id+3;//y2
      edofMat[4]=id+2+2*(ni+1);//x3
      edofMat[5]=id+3+2*(ni+1);//y3
      edofMat[6]=id+0+2*(ni+1); //x4
      edofMat[7]=id+1+2*(ni+1); //y4

      for(m=0;m<8;m++){
        fint[edofMat[m]]=fint[edofMat[m]]+FE_int[8*e+m];
        }
    }

    for (n=0;n<numNodes;n++) {
      f[n]=fext[n]-fint[n];
    }


    checkCudaErrors(cudaMemcpy(f_d, f, numNodes * sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(fixednodes_d, fixednodes, numNodes * sizeof(int),cudaMemcpyHostToDevice));

  checkCudaErrors(cudaMemset(delta_U_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(prod_elastic_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(prod_plastic_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
  GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1>>>(ni,numElems,Ke_plastic_d,delta_U_d,prod_d);

  cudaMemset(M_d, 0.0, numNodes*sizeof(double));
  GPUMatVec_precond<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems,rho_d,yphys_d,penal,Ke_elastic_d, M_d);

  checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
  rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_d, M_d,r_d,prod_d, z_d, p_d, numNodes, fixednodes_d);

  cudaMemset(rzold_d, 0.0, 1*sizeof(double));
  Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_d, r_d,numNodes,fixednodes_d,rzold_d);

  checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(Ap_elastic_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemset(Ap_plastic_d, 0.0, numNodes*sizeof(double)));
  checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
  printf("Init : rzold=%lg  \n",rznew[0]);

  rNorm[0]=10000;

    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

    checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));

    GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, Ke_plastic_d,p_d, Ap_d);

    Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, Ap_d,numNodes,fixednodes_d,pAp_d,rNorm_d);

    Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_d,delta_U_d, r_d, z_d, p_d, Ap_d,numNodes,fixednodes_d, rzold_d,pAp_d,rznew_d,rNorm_d);

    UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, z_d, numNodes,fixednodes_d,rzold_d,rznew_d,Ap_elastic_d, Ap_plastic_d);

    switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

    checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

    norme=sqrt(rNorm[0]);
    if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
    checkCudaErrors(cudaStreamSynchronize(streams[0]));

    if(norme <= tol) break;

}

if(iter==Max_iters){
  printf("Residual error in PCG: norme=%lg : end of program \n",norme);
  return(0);
}

checkCudaErrors(cudaMemset(norm_delta_U_d, 0.0, numNodes*sizeof(double)));
checkCudaErrors(cudaMemset(norm_Uold_d, 0.0, numNodes*sizeof(double)));
checkCudaErrors(cudaMemset(norm_Unew_d, 0.0, numNodes*sizeof(double)));

GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d,penal, yphys_d,Ke_elastic_d,delta_U_d, norm_delta_U_d);
checkCudaErrors(cudaMemcpy(delta_U, delta_U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_delta_U, norm_delta_U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d, penal, yphys_d,Ke_elastic_d,U_d, norm_Uold_d);
checkCudaErrors(cudaMemcpy(Uold, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_Uold, norm_Uold_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
Update_Newton<<<NBLOCKS2,BLOCKSIZE2>>>(numNodes,delta_U_d,U_d);
GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, rho_d, penal, yphys_d,Ke_elastic_d,U_d, norm_Unew_d);
checkCudaErrors(cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_Unew, norm_Unew_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));

norme1_forward=0;
norme2_forward=0;
norme3_forward=0;
for(m=0;m<numNodes;m++) {
  norme1_forward=norme1_forward+delta_U[m]*norm_delta_U[m];
  norme2_forward=norme2_forward+Uold[m]*norm_Uold[m];
  norme3_forward=norme3_forward+U[m]*norm_Unew[m];
}

norme1_forward=sqrt(norme1_forward);
norme2_forward=sqrt(norme2_forward);
norme3_forward=sqrt(norme3_forward);

norme_newton_forward=(double) norme1_forward/(norme2_forward+norme3_forward);
printf("norme_newton=%lg : norme du=%lg : norme Uold=%lg : norme Unew=%lg\n",norme_newton_forward,norme1_forward,norme2_forward,norme3_forward);
printf("FORWARD solution : Newton steps : %d , Newton residual=%lg, PCG steps : %d, PCG residual=%lg\n",iter_newton,norme_newton_forward, iter,norme);


//END OF FORWARD PROBLEM
}


if(norme_newton_forward <= tol_newton  || norme3_forward<=tol_newton ) break;

}//END NEWTON

if(iter_newton==Max_iters_newton){
  printf("Residual error in Newton algorithm: end of program \n");
  return(0);
}

for (n=0;n<numNodes;n++) U_l[numNodes*l+n]=U[n];
printf("n=%d,U[2]=%lg, U[2442]=%lg, U[3]=%lg, U[2443]=%lg\n",numNodes,U[2],U[2442],U[3],U[2443]);

double min_vms=1e15;
double max_vms=-1e-15;

for(e=0;e<numElems;e++){
stress_xx_e[l*numElems+e]=0;
stress_yy_e[l*numElems+e]=0;
stress_xy_e[l*numElems+e]=0;
dev_stress_xx_e[e]=0;
dev_stress_yy_e[e]=0;
dev_stress_xy_e[e]=0;


  for (m=0;m<4;m++){
  stress_xx_e[l*numElems+e]=stress_xx_e[l*numElems+e]+0.25*stress[4*3*e+3*m+0];
  stress_yy_e[l*numElems+e]=stress_yy_e[l*numElems+e]+0.25*stress[4*3*e+3*m+1];
  stress_xy_e[l*numElems+e]=stress_xy_e[l*numElems+e]+0.25*stress[4*3*e+3*m+2];
  dev_stress_xx_e[e]=dev_stress_xx_e[e]+0.25*deviatoric_stress[4*3*e+3*m+0];
  dev_stress_yy_e[e]=dev_stress_yy_e[e]+0.25*deviatoric_stress[4*3*e+3*m+1];
  dev_stress_xy_e[e]=dev_stress_xy_e[e]+0.25*deviatoric_stress[4*3*e+3*m+2];
}

von_mises_stress[l*numElems+e]=sqrt(stress_xx_e[l*numElems+e]*stress_xx_e[l*numElems+e]+stress_yy_e[l*numElems+e]*stress_yy_e[l*numElems+e]
-stress_xx_e[l*numElems+e]*stress_yy_e[l*numElems+e]+3*stress_xy_e[l*numElems+e]*stress_xy_e[l*numElems+e]);

dev_von_mises_stress[e]=sqrt(dev_stress_xx_e[e]*dev_stress_xx_e[e]+dev_stress_yy_e[e]*dev_stress_yy_e[e]
-dev_stress_xx_e[e]*dev_stress_yy_e[e]+3*dev_stress_xy_e[e]*dev_stress_xy_e[e]);

if(von_mises_stress[l*numElems+e]<min_vms) min_vms=von_mises_stress[l*numElems+e];
if(von_mises_stress[l*numElems+e]>max_vms) max_vms=von_mises_stress[l*numElems+e];
}
  printf("min_vms=%lg and max_vms=%lg\n",min_vms,max_vms);

for(m=0;m<3*qpoints*numElems;m++){
  old_plastic_strain_l[numElems*3*qpoints*l+m]=old_plastic_strain[m];
}

  for(m=0;m<3*qpoints*numElems;m++){
  old_hardening[m]=hardening[m];
  old_plastic_strain[m]=plastic_strain[m];
  }

  }//END LOADSTEPS

  }

double somme=0;
printf("l=%d\n",l);

for (e=0;e<numElems;e++)  gradient2_map[e]=0;

for (e=0;e<numElems_opt;e++) {
  if(map_gradient[e]>-1)  somme=somme+pow(von_mises_stress[map_gradient[e]],pnorm);

}


if(nodal_force==0){
pnorm1=1.f/pnorm;
somme=(double)somme/numElems_opt;
min_stress=pow(somme,pnorm1);
}
else{
min_stress=0;
for (n=0;n<numNodes;n++){
  min_stress=min_stress+fext[n]*U[n];
}
}

cost_function[opt]=min_stress;

  //Check old plastic strain value, no derivative if old plastic strain=0
  for(l=0;l<loadsteps;l++){
    old_pstrain[l]=0;
      for(m=0;m<3*qpoints*numElems;m++){
          if(old_plastic_strain_l[3*qpoints*numElems*l+m]!=0 ) old_pstrain[l]=1;
        }
      printf("old_pstrain[%d]=%d\n",l,old_pstrain[l]);
  }

  if(gradient_calculation==1){

  for(layer=nb_layers-1;layer>=0;layer--){

  for(load1=loadsteps1-1;load1>=0;load1--)
  {
    l=loadsteps1*layer+load1;

    ratio=(double)(load1+1)/loadsteps1;

    //Gradient calculation NEWTON

  //ADJOINT STIFFNESS Matrix

        somme=0;
        if(l==loadsteps-1){
        for(e=0;e<numElems;e++) somme=somme+pow(von_mises_stress[l*numElems+e],pnorm);
        pnorm1=1.f/pnorm-1;
        somme=somme/numElems;
      }

        if(somme!=0) dc_dvm1=pow(somme,pnorm1);
        if(somme==0) dc_dvm1=0;


  for(e=0;e<8*numElems;e++) {
    rhs[e]=0;
    rhs_j[e]=0;
  }

  if(abs(nodal_force)>0){
    for(i=0;i<8*numElems;i++){
    dc_due[8*numElems*l+i]=(double)ratio*FE_load[i]/loadsteps;
    }
  }

double l1_loop_time=get_time();

double first_loop_time=get_time();

adjoint_K(l,ni,numElems,qpoints,rho,penal,G,yield_stress,hard_constant,ID_mat,dn_ds_l,yphys_l,C1,delta_x,delta_y,weight1,weight2,weight3,weight4,
BE1,BE2,BE3,BE4,BE1_t,BE2_t,BE3_t,BE4_t,plasticity_nodes_l,nodal_force,dc_dvm, dvm_dwe, dc_dvm1, von_mises_stress, stress_xx_e,
stress_yy_e, stress_xy_e,pnorm, dc_due,plasticity_nodes,Ke2_e);


printf("first loop time =%f s \n",get_time()-first_loop_time);

//dc_dvm2 calculation
for (l1=l+1;l1<loadsteps;l1++){

  somme=0;
  if(l1==loadsteps-1){
  for(int e1=0;e1<numElems;e1++) somme=somme+pow(von_mises_stress[l1*numElems+e1],pnorm);
  pnorm1=1.f/pnorm-1;
  somme=somme/numElems;
  }

  if(somme!=0) dc_dvm2[l1]=pow(somme,pnorm1);
  if(somme==0) dc_dvm2[l1]=0;
}

double second_loop_time=get_time();

adjoint_rhs(qpoints,delta_x,delta_y,l,load1,loadsteps1,loadsteps,nodal_force,ni,numElems,numNodes,penal,hard_constant,yield_stress,yphys_l,DEV_mat,
C1,G,rho,BE1,BE2,BE3,BE4,BE1_t,BE2_t,BE3_t,BE4_t,weight1,weight2,weight3,weight4,von_mises_stress,stress_xx_e,stress_yy_e,stress_xy_e, pnorm,
dH_deta_l,plasticity_nodes_l, initial_elements_l, lambda_l, rhs,rhs_j,dc_dvm,dvm_dwe,dc_dvm2,old_pstrain);

printf("second loop time =%f s \n",get_time()-second_loop_time);

double adjoint_time=get_time();

checkCudaErrors(cudaMemcpy(Ke2_d, Ke2_e, numElems*MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));

  //ADJOINT PROBLEM

    //lambda
    for(n=0;n<numNodes;n++) f[n]=0;


    for(e=0;e<numElems;e++){
      j=e/ni;
      i=e%ni;

      id=2*((ni+1)*j+i);
      id2=(ni+1)*j+i;

      edofMat[0]=id;//x1
      edofMat[1]=id+1;//y1
      edofMat[2]=id+2;//x2
      edofMat[3]=id+3;//y2
      edofMat[4]=id+2+2*(ni+1);//x3
      edofMat[5]=id+3+2*(ni+1);//y3
      edofMat[6]=id+0+2*(ni+1); //x4
      edofMat[7]=id+1+2*(ni+1); //y4

      for(m=0;m<8;m++){
        f[edofMat[m]]=f[edofMat[m]]-dc_due[8*numElems*l+8*e+m]-rhs[8*e+m]-rhs_j[8*e+m];
        rhs_l[numNodes*l+edofMat[m]]=rhs_l[numNodes*l+edofMat[m]]+rhs[8*e+m]+rhs_j[8*e+m];
        }
    }

    checkCudaErrors(cudaMemcpy(f_d, f, numNodes * sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemset(delta_lambda_d, 0.0, numNodes*sizeof(double)));

    bool conjugate_gradient=0;
    for(n=0;n<numNodes;n++){
      if(f[n]!=0) conjugate_gradient=1;
    }

    if(conjugate_gradient==1){
    checkCudaErrors(cudaMemset(prod_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(prod_elastic_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(prod_plastic_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(r_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(p_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(z_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(rznew_d, 0.0, sizeof(double)));
    checkCudaErrors(cudaMemset(rNorm_d, 0.0, sizeof(double)));
    checkCudaErrors(cudaMemset(pAp_d, 0.0, sizeof(double)));

    GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni,numElems,Ke2_d,delta_lambda_d,prod_d);

    cudaMemset(M_d, 0.0, numNodes*sizeof(double));
    GPUMatVec_precond2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, Ke2_d, M_d);

    rzp_calculation<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(f_d, M_d, r_d,prod_d, z_d, p_d, numNodes, fixednodes_d);

    cudaMemset(rzold_d, 0.0, 1*sizeof(double));
    Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_d, r_d,numNodes,fixednodes_d,rzold_d);

    checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(Ap_elastic_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemset(Ap_plastic_d, 0.0, numNodes*sizeof(double)));
    checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
    //printf("rzold=%lg\n",rznew[0]);
    //rNorm[0]=10000;

    for(iter=0; iter<Max_iters; iter++)
    {
      checkCudaErrors(cudaMemset(Ap_d, 0.0, numNodes*sizeof(double)));
      GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(ni, numElems, Ke2_d,p_d, Ap_d);

      Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, Ap_d,numNodes,fixednodes_d,pAp_d,rNorm_d);

      Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_d,delta_lambda_d, r_d, z_d, p_d, Ap_d,numNodes,fixednodes_d, rzold_d,pAp_d,rznew_d,rNorm_d);

      UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_d, z_d, numNodes,fixednodes_d,rzold_d,rznew_d,Ap_elastic_d, Ap_plastic_d);

      switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

      checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

      norme=sqrt(rNorm[0]);
      if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
      checkCudaErrors(cudaStreamSynchronize(streams[0]));

      if(norme <= tol) break;

    }

    if(iter==Max_iters){
    printf("Residual error in PCG adjoint: norme=%lg : end of program \n",norme);
    return(0);
    }

    printf("ADJOINT solution : loadstep=%d, PCG steps : %d, PCG residual=%lg\n", l,iter,norme);

    }
    else{
      printf("WARNING : Null right hand side : the adjoint conjugate gradient loop has been skipped : normal behavior when : objective function not depending on this step, 1st layer, no plastic strain\n");
    }
  //END OF ADJOINT PROBLEM

checkCudaErrors(cudaMemcpy(lambda, delta_lambda_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost));

for (n=0;n<numNodes;n++) lambda_l[numNodes*l+n]=lambda[n];

printf("adjoint time =%f s \n",get_time()-adjoint_time);

  }//END LOADSTEPS

}

  for(l=0;l<loadsteps;l++)
  {

    printf("gradient calculation : l=%d/%d\n",l+1,loadsteps);

    somme=0;
    if(l==loadsteps-1){
    for(e=0;e<numElems;e++) somme=somme+pow(von_mises_stress[l*numElems+e],pnorm);


    pnorm1=1.f/pnorm-1;
    somme=somme/numElems;
  }

    if(somme!=0) dc_dvm1=pow(somme,pnorm1);
    if(somme==0) dc_dvm1=0;


x_derivatives(l,loadsteps,numNodes,numElems,qpoints,U_l,lambda_l,ni,rho,penal,yield_stress,G,hard_constant,yphys_l,C1,ID_mat,DEV_mat,
inh_strain2_l,old_plastic_strain_l,strain_ini,delta_x,delta_y,weight1,weight2,weight3,weight4,
BE1,BE2,BE3,BE4,BE1_t,BE2_t,BE3_t,BE4_t,old_pstrain,plasticity_nodes_l,dH_deta_l, norm_dev_stress_l, n_trial_stress_l,
von_mises_stress,stress_xx_e,stress_yy_e,stress_xy_e,dc_dvm1,pnorm,dc_dvm,dvm_dwe,nodal_force,dc_dxe,gradient2_l, gradient2_map);


  }//END LOADSTEPS

  }

//Filtering

for (e=0;e<numElems;e++) {
  if(map_gradient[e]>-1){
    gradient2[map_gradient[e]]=gradient2_map[e];

  }
}

filter(ni, nj1, rmin, rho_opt, gradient2, gradient);

//Density Update
sommeV=0;

double max_gradient=-1e5;
double min_gradient=1e5;

// Set outer move limits
for (e=0;e<numElems_opt;e++) {
	xmax[e] = Mini(Xmax, rho_opt[e] + movlim);
	xmin[e] = Maxi(Xmin, rho_opt[e] - movlim);
  sommeV=sommeV+rho_opt[e];
  dg[map_gradient[e]]=1.f/numElems_opt;
  if(gradient[e]<min_gradient) min_gradient=gradient[e];
  if(gradient[e]>max_gradient) max_gradient=gradient[e];
}

for (e=0;e<numElems_opt;e++) gradient[e]=(double)gradient[e]/max_gradient;

printf("gradient=[%lg %lg]\n",min_gradient,max_gradient);

g[0]=(double) sommeV/numElems_opt-volfrac;
constraint[opt]=(double) sommeV/numElems_opt;

mma->Update(rho_opt,gradient,g,dg,xmin,xmax);


for (e=0;e<numElems_opt;e++) {
  rho_old_opt[e]=rho_opt[e];
}

for (e=0;e<numElems;e++) {
  if(map_gradient[e]>-1){
    rho[e]=rho_opt[map_gradient[e]];
    gradient_FD_map[e]=gradient_FD[map_gradient[e]];
    gradient2_map[e]=gradient2[map_gradient[e]];
  }
  else{
    gradient_FD_map[e]=0;
    gradient2_map[e]=0;
  }
}

printf("Optimization step %d : PCG steps : %d, PCG residual=%lg, cost function=%lg, volume fraction=%lg\n",
opt,iter,norme,cost_function[opt],constraint[opt]);


//POST PROCESSING

for (l=0;l<loadsteps;l++){
  int cpt=0;
for (i=0;i<(ni+1)*(nj+1);i++){
ux[(ni+1)*(nj+1)*l+i]=U_l[numNodes*l+cpt];  lambdax[(ni+1)*(nj+1)*l+i]=lambda_l[numNodes*l+cpt]; rhs_x[(ni+1)*(nj+1)*l+i]=rhs_l[numNodes*l+cpt]; cpt++;
uy[(ni+1)*(nj+1)*l+i]=U_l[numNodes*l+cpt];  lambday[(ni+1)*(nj+1)*l+i]=lambda_l[numNodes*l+cpt]; rhs_y[(ni+1)*(nj+1)*l+i]=rhs_l[numNodes*l+cpt]; cpt++;
}
}

l=loadsteps-1;

strcpy(filename,"optim_newton_elastic_");  sprintf(u2,"%d",opt);	strcat(filename,u2);
 strcpy(str2,str1);   strcat(filename,str2);
fichier=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
fprintf(fichier, "# vtk DataFile Version 3.0\n");
fprintf(fichier, "VTK from C\n");
fprintf(fichier,"ASCII\n");
fprintf(fichier,"DATASET UNSTRUCTURED_GRID\n");
fprintf(fichier,"POINTS %d float\n",(nj+1)*(ni+1));
for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
    fprintf(fichier,"%f %f %f\n",i*0.001,j*0.001,1.f);
  }
}
fprintf(fichier,"CELLS %d %d\n",ni*nj,5*ni*nj); //5 for 4 (node index) +1 (number of nodes per cell)
for (i=0;i<ni*nj;i++){
  fprintf(fichier,"4 %d %d %d %d\n",edofMat2[4*i+0],edofMat2[4*i+1],edofMat2[4*i+3],edofMat2[4*i+2]);
}
fprintf(fichier,"CELL_TYPES %d\n",ni*nj);
for (i=0;i<numElems;i++){
fprintf(fichier,"8\n"); // 8 for pixel (2D) https://www.kitware.com/products/books/VTKUsersGuide.pdf
}
fprintf(fichier,"CELL_DATA %d\n",numElems);
fprintf(fichier,"SCALARS vms float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
    fprintf(fichier,"%lg ",von_mises_stress[l*numElems+i0]);
}
}
for (l=loadsteps-1;l<loadsteps;l++){
  fprintf(fichier,"SCALARS strain_ini_l%d float\n",l);
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
        i0=I2D(ni,i,j);
      fprintf(fichier,"%lg ",strain_ini[3*qpoints*numElems*l+qpoints*3*i0+3*0+1]);
  }
  }
  fprintf(fichier,"SCALARS initial_elements_l%d float\n",l);
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
        i0=I2D(ni,i,j);
      fprintf(fichier,"%d ",initial_elements_l[numElems*l+i0]);
  }
  }
  fprintf(fichier,"SCALARS yphys_l%d float\n",l);
  fprintf(fichier,"LOOKUP_TABLE default\n");
  for (j=0;j<nj;j++){
    for (i=0;i<ni;i++){
        i0=I2D(ni,i,j);
      fprintf(fichier,"%lg ",yphys_l[numElems*l+i0]);
  }
  }
}


fprintf(fichier,"SCALARS map_gradient float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
    fprintf(fichier,"%d ",map_gradient[i0]);
}
}
fprintf(fichier,"SCALARS plasticity float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
    fprintf(fichier,"%d ",plasticity_elements[i0]);
}
}
fprintf(fichier,"\nSCALARS density float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
    fprintf(fichier,"%lg ",rho[i0]);
}
}


if(finite_difference==1){
fprintf(fichier,"\nSCALARS diff_FD float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
      if(gradient_FD_map[i0]!=0){
    fprintf(fichier,"%lg ",abs(gradient_FD_map[i0]-gradient2_map[i0])/abs(gradient_FD_map[i0]));
  }
  else{
    fprintf(fichier,"%lg ",0.0);
  }
}
}
fprintf(fichier,"\nSCALARS gradient_FD float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I2D(ni,i,j);
      fprintf(fichier,"%lg ",gradient_FD_map[i0]);
    }
  }
}
  fprintf(fichier,"\nSCALARS gradient2 double\n");
  fprintf(fichier,"LOOKUP_TABLE default\n");
    for (j=0;j<nj;j++){
      for (i=0;i<ni;i++){
            i0=I2D(ni,i,j);
        fprintf(fichier,"%lg ",gradient2_map[i0]);
      }
    }
    fprintf(fichier,"\nSCALARS stress_xx double\n");
    fprintf(fichier,"LOOKUP_TABLE default\n");
      for (j=0;j<nj;j++){
        for (i=0;i<ni;i++){
              i0=I2D(ni,i,j);
          fprintf(fichier,"%lg ",stress_xx_e[i0]);
        }
      }
      fprintf(fichier,"\nSCALARS stress_yy double\n");
      fprintf(fichier,"LOOKUP_TABLE default\n");
        for (j=0;j<nj;j++){
          for (i=0;i<ni;i++){
                i0=I2D(ni,i,j);
            fprintf(fichier,"%lg ",stress_yy_e[i0]);
          }
        }
        fprintf(fichier,"\nSCALARS stress_xy double\n");
        fprintf(fichier,"LOOKUP_TABLE default\n");
          for (j=0;j<nj;j++){
            for (i=0;i<ni;i++){
                  i0=I2D(ni,i,j);
              fprintf(fichier,"%lg ",stress_xy_e[i0]);
            }
          }
fprintf(fichier,"\nPOINT_DATA %d\n",(ni+1)*(nj+1));
for (l=0;l<loadsteps;l++){
fprintf(fichier,"SCALARS u_l%d double\n",l);
fprintf(fichier,"LOOKUP_TABLE default\n");
  for (j=0;j<(nj+1);j++){
    for (i=0;i<(ni+1);i++){
        i0=I2D(ni2,i,j);
      fprintf(fichier,"%lg ",sqrt(ux[(ni+1)*(nj+1)*l+i0]*ux[(ni+1)*(nj+1)*l+i0]+uy[(ni+1)*(nj+1)*l+i0]*uy[(ni+1)*(nj+1)*l+i0]));
    }
  }
  fprintf(fichier,"SCALARS rhs_l%d double\n",l);
  fprintf(fichier,"LOOKUP_TABLE default\n");
    for (j=0;j<(nj+1);j++){
      for (i=0;i<(ni+1);i++){
          i0=I2D(ni2,i,j);
        fprintf(fichier,"%lg ",rhs_x[(ni+1)*(nj+1)*l+i0]);//sqrt(rhs_x[(ni+1)*(nj+1)*l+i0]*rhs_x[(ni+1)*(nj+1)*l+i0]+rhs_y[(ni+1)*(nj+1)*l+i0]*rhs_y[(ni+1)*(nj+1)*l+i0]));
      }
    }
  fprintf(fichier,"SCALARS lambda_l%d double\n",l);
  fprintf(fichier,"LOOKUP_TABLE default\n");
    for (j=0;j<(nj+1);j++){
      for (i=0;i<(ni+1);i++){
          i0=I2D(ni2,i,j);
        fprintf(fichier,"%lg ",sqrt(lambdax[(ni+1)*(nj+1)*l+i0]*lambdax[(ni+1)*(nj+1)*l+i0]+lambday[(ni+1)*(nj+1)*l+i0]*lambday[(ni+1)*(nj+1)*l+i0]));
      }
    }
    fprintf(fichier,"SCALARS f_l%d double\n",l);
    fprintf(fichier,"LOOKUP_TABLE default\n");
      for (j=0;j<(nj+1);j++){
        for (i=0;i<(ni+1);i++){
            i0=I2D(ni2,i,j);
          fprintf(fichier,"%lg ",f[i0]);
        }
      }
}
fclose(fichier);

}


FILE *cost_function_file;

cost_function_file=fopen("min_stress.txt","w");

for (i=0;i<opt_steps;i++){
fprintf(cost_function_file,"%lg %lg\n",cost_function[i],constraint[i]);
}

fclose(cost_function_file);

size_t free_memory, total_memory;
checkCudaErrors(cudaMemGetInfo(&free_memory,&total_memory));
printf("GPU memory usage : %.3f GB\n",(double) (total_memory-free_memory)/1e9);


  //free gpu memory

  checkCudaErrors(cudaFree(Ke_elastic_d));
  checkCudaErrors(cudaFree(pAp_d));
  checkCudaErrors(cudaFree(rznew_d));
  checkCudaErrors(cudaFree(rzold_d));
  checkCudaErrors(cudaFree(rNorm_d));
  checkCudaErrors(cudaFree(fixednodes_d));
  checkCudaErrors(cudaFree(U_d));
  checkCudaErrors(cudaFree(M_d));
  checkCudaErrors(cudaFree(r_d));
  checkCudaErrors(cudaFree(p_d));
  checkCudaErrors(cudaFree(z_d));
  checkCudaErrors(cudaFree(f_d));
  checkCudaErrors(cudaFree(prod_d));
  checkCudaErrors(cudaFree(Ap_d));
  cudaFree(Ke_plastic_d);

  free(BE_total);
  free(inh_strain2_l);
  free(f);
  free(U);
  free(ux);
  free(uy);
  free(lambda);
  free(lambdax);
  free(lambday);
  free(pAp);
  free(rznew);
  free(rNorm);
  free(fixednodes);

  free(rhs_x);
  free(rhs_y);
  free(old_pstrain);
  free(edofMat2);
  free(rho);
  free(rho_opt);
  free(rho_old_opt);
  free(g);
  free(dg);
  free(xmin);
  free(xmax);
  free(af);
  free(bf);
  free(map_gradient);
  free(gradient_map);
  free(gradient);
  free(gradient1);
  free(gradient2);
  free(gradient_FD);
  free(gradient2_map);
  free(gradient2_l);
  free(gradient_FD_map);
  free(cost_function);
  free(constraint);

  free(vms);
  free(von_mises_stress);
  free(dev_von_mises_stress);
  free(stress_xx_e);
  free(stress_yy_e);
  free(stress_xy_e);
  free(dev_stress_xx_e);
  free(dev_stress_yy_e);
  free(dev_stress_xy_e);
  free(n_trial_stress_xx_e);
  free(n_trial_stress_yy_e);
  free(n_trial_stress_xy_e);
  free(n_trial_stress_eq);

  free(initial_elements);
  free(initial_elements_l);

  free(dR_due);
  free(dc_dvm);
  free(dvm_dwe);

  free(dc_due);
  free(rhs);
  free(rhs_j);
  free(rhs_l);
  free(dc_dxe);

  free(Ke_elastic);
  free(Ke2);

  free(Ke2_t);
  free(Ke2_plastic);
  free(Ke2_plastic_t);
  free(Ke2_e);
  free(Ke_plastic);


  free(norm_delta_U);
  free(norm_Unew);
  free(norm_Uold);
  free(norm_delta_lambda);
  free(norm_lambda_new);
  free(norm_lambda_old);
  free(fixednodes_x);
  free(fixednodes_y);

  free(fext);
  free(fint);
  free(U_l);
  free(Uini);

  free(Uold);
  free(delta_U);
  free(lambda_l);
  free(delta_lambda);
  free(lambda_old);
  free(FE_load);

  free(dn_ds);
  free(dn_ds_l);
  free(n_trial_stress);
  free(n_trial_stress_l);
  free(dgamma);
  free(strain);
  free(strain_ini);
  free(plastic_strain);
  free(hardening);
  free(old_plastic_strain);
  free(old_plastic_strain_l);
  free(old_hardening);
  free(stress);
  free(deviatoric_stress);
  free(consistent_matrix);
  free(plasticity_nodes);
  free(plasticity_nodes_l);
  free(norm_dev_stress_l);
  free(criteria_vms);
  free(plasticity_elements);
  free(map_plastic);
  free(FE_int);

  double final_time=get_time()-time_program;
  int hours=final_time/3600;
  int minutes1=final_time-hours*3600;
  int minutes=minutes1/60;
  int seconds=minutes1%60;
  printf("Program finished in %d hours %d minutes and %d seconds \n",hours,minutes,seconds);

  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
