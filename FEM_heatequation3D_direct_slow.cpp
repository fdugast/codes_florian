
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)


double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, int nk, double rmin, double *x, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
					for (k = 0; k < nk2; k++){

				i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
							for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {

								i02 = I3D(ni2,nj2, i, j,k);
					if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];
						}
					}
				}
			}


			if(x[i0]*somme!=0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void filtre2(int ni, int nj, int nk, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			for (k = 0; k < nk2; k++){

		i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
								for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {
								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
						fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}
				}

						if(somme!=0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void OC (int ni, int nj, int nk, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*nk*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				for (k = 0; k < nk; k++){
				i0 = I3D(ni,nj, i, j,k);
				gamma_old[i0]=gamma0[i0];
			}
		}
	}


	while ((l2-l1)> 1e-4){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
			for (k = 0; k < nk; k++){
			i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
	//printf("ok boucle \n");
				if(gradient[i0]>0) {

					printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);
			}
		}
	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
		for (k = 0; k < nk; k++){
		i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

}
		}
	}
}

volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

int main(int argc,char **args)
{

	printf("Debut programme \n");



printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,M1,U1,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end,start2,end2,Id,Istart,Iend,nlocal;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew,rnorm,KE[64],ME[64],HE1[64],HE2[64],HE3[64],HE4[64],HE5[64],HE6[64],ke[64],me[64],*array;
  PetscScalar y,conv,Tp1;
	char filename[200],u2[200];

	double Xmin = 0.0001;
	double Xmax = 1;
	double movlim = 0.05;



	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  clock_t start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int icol,jcol,kcol,id,ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim;

	ni = 250;
	nj = ni;
	nk = ni;
	ni2=ni+1;
	nj2=nj+1;

	ndof=(ni+1)*(nj+1)*(nk+1);
	nele=ni*nj*nk;
	opt_it_max=1;
	alpha=1; // coeff pow compliance
	rmin=1.4;
	volfrac=1;
	double hconv=10;
	double delta,somme,somme1,somme2,delta_x, delta_y,delta_z,delta_t,total_time,time;
	delta_x=(double)0.250/(ni-1); //domaine de 10 mm par 10 mm ...
	delta_y=(double)0.250/(nj-1);
	delta_z=(double)0.250/(nk-1);
	delta_t=1; // si delta t trop fort oscillations sur compliance
	total_time=5;
	int nbsteps;
	nbsteps=total_time/delta_t;

	//Medium properties
	int *edofMat;
	edofMat=(int *)calloc(8,sizeof(int));
	domain_optim=(int *)calloc(nele,sizeof(int));
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp2,Tair;
	cp=(double *)calloc(nele,sizeof(double));
	lambda=(double *)calloc(nele,sizeof(double));
	derivee_c=(double *)calloc(nele,sizeof(double));
	derivee_lambda=(double *)calloc(nele,sizeof(double));
	rho=(double *)calloc(nele,sizeof(double));
	diff=(double *)calloc(nele,sizeof(double));
	c=(double *)calloc(nele,sizeof(double));
	rho_solide=1000;
	lambda_solide=100;
	cp_solide=500;
	rho_vide=1;
	lambda_vide=0.01;
	cp_vide=1000;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	Qvol=0;//10;
	T0=0; //Initial temperature
	Tp1=0; //Dirichlet temperature
	Tp2=0.f/(delta_x*delta_y); //Dirichlet temperature
	Qsurf=0*1e6;
	Tair=30;
	penal=3;

	FILE *fichier_K,*fichier_M, *fichier_H1,*fichier_H2, *fichier_H3,*fichier_H4,*fichier_H5,*fichier_H6;
	fichier_K=fopen("Ke_3d.txt","r");
	fichier_M=fopen("Me_3d.txt","r");
	fichier_H1=fopen("HE_3D_1.txt","r");
	fichier_H2=fopen("HE_3D_2.txt","r");
	fichier_H3=fopen("HE_3D_3.txt","r");
	fichier_H4=fopen("HE_3D_4.txt","r");
	fichier_H5=fopen("HE_3D_5.txt","r");
	fichier_H6=fopen("HE_3D_6.txt","r");
float test;
	for (i=0;i<64;i++){
		fscanf(fichier_K,"%f\n",&test);		KE[i]=test;
		fscanf(fichier_M,"%f\n",&test);		ME[i]=test;
		fscanf(fichier_H1,"%f\n",&test);		HE1[i]=test;
		fscanf(fichier_H2,"%f\n",&test);		HE2[i]=test;
		fscanf(fichier_H3,"%f\n",&test);		HE3[i]=test;
		fscanf(fichier_H4,"%f\n",&test);		HE4[i]=test;
		fscanf(fichier_H5,"%f\n",&test);		HE5[i]=test;
		fscanf(fichier_H6,"%f\n",&test);		HE6[i]=test;
	}

	fclose(fichier_K);
	fclose(fichier_M);
	fclose(fichier_H1);
	fclose(fichier_H2);
	fclose(fichier_H3);
	fclose(fichier_H4);
	fclose(fichier_H5);
	fclose(fichier_H6);

	AmgXSolver solver;
	//ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);
	ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/AMG_CLASSICAL_AGGRESSIVE_L1.json" ); CHKERRQ(ierr);
	 // ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);


	int *boundary;
	boundary=(int *)calloc(nele,sizeof(int));

	double *F;
	F=(double *)calloc(ndof,sizeof(double));

	double *x1;
	double *Told;
	double *T_transient, *pos_q1_transient, *T_transient2, *derivee_T_transient,*adjoint_transient,*prod_KU_transient,*second_membre_transient,*derivee_T;
	double *q2,*q3,*q4,*pos_q1,*pos_q2,*pos_q3,*pos_q4, *compliance, *gradient, *lu;
	int *q1, *num_elem;
	double *gamma_x, *gamma_h;
	double *fonction_cout;
	fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
//	solide=(double *)calloc(nele,sizeof(double));
	gamma_h=(double *)calloc(nele,sizeof(double));
	gamma_x=(double *)calloc(nele,sizeof(double));
	x1=(double *)calloc(ndof,sizeof(double));
	Told=(double *)calloc(ndof,sizeof(double));
	T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//pos_q1_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//T_transient2=(double *)calloc(nbsteps*ndof,sizeof(double));
	//derivee_T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//adjoint_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//prod_KU_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//second_membre_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	//derivee_T=(double *)calloc(nbsteps*ndof,sizeof(double));
	q1=(int *)calloc(nbsteps,sizeof(int));
	num_elem=(int *)calloc(nbsteps,sizeof(int));
	q2=(double *)calloc(nbsteps,sizeof(double));
	q3=(double *)calloc(nbsteps,sizeof(double));
	q4=(double *)calloc(nbsteps,sizeof(double));
	pos_q1=(double *)calloc(ndof,sizeof(double));
	pos_q2=(double *)calloc(ndof,sizeof(double));
	pos_q3=(double *)calloc(ndof,sizeof(double));
	pos_q4=(double *)calloc(ndof,sizeof(double));
	//lu=(double *)calloc(ndof,sizeof(double));
	//compliance=(double *)calloc(nbsteps,sizeof(double));
	//gradient=(double *)calloc(nele,sizeof(double));

	int test1;
	FILE *domain;
		domain=fopen("domain_3d_250","r");
	double somme_domain;

	double volfrac2;
	cpt=0;
	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);
				fscanf(domain,"%d\n",&test1);
				domain_optim[i0]=1-test1;
		//					if(test==0) cpt=cpt+1;
	//			somme_domain=somme_domain+(1-test1);

			}
		}
	}
		//volfrac2=(double)(nele*volfrac-cpt)/(nele-cpt);
//printf("volfrac_shape=%f\n",somme_domain/(nele));
		//for (i=0; i<nele; i++) fprintf(domain,"%d\n",domain_optim[i]);
fclose(domain);
//getchar();

	FILE *init;
//	init=fopen("init2.txt","r");

	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);
				//fscanf(init,"%f",&test);
				if(domain_optim[i0]==1){
				gamma_h[i0]=0;

			}
			if(domain_optim[i0]==0){
				gamma_h[i0]=1;
				gamma_x[i0]=1;//volfrac;
			}
			}
		}
	}

	//Definition of boundary conditions

	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
					for (k=0; k<nk; k++) {
				i0=I3D(ni,nj,i,j,k);

				if(i==0 || j==0 || i==(ni-1) || j==(nj-1) || k==0 || k==(nk-1)){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;
				if(j==0 ) boundary[i0]=1;
				if(i==0 ) boundary[i0]=2;//2;
				if(j==(nj-1)) boundary[i0]=3;//3;
				if(i==(ni-1)) boundary[i0]=4;//4; }
				if(k==0) boundary[i0]=5;
				if(k==(nk-1)) boundary[i0]=6;//2;

			}

			}
		}
	}


		/*FILE *fichier;
		fichier=fopen("boundary.txt","w");
		for (i=0;i<ndof;i++) fprintf(fichier,"%d\n",boundary[i]);
		fclose(fichier);*/

	double temps;
	temps=0;

	for (i=0;i<nbsteps;i++){
		//if(temps<=1) q1[i]=0;
	 q1[i]=21+i;


		//q1[i]=0.3*1e-2*sin(PI*temps*(temps<=1));
		//q2[i]=0.3*1e-2*sin(PI*temps*(temps<=1));
		//q3[i]=0.3*1e-2*sin(PI*temps*(temps<=1))+0.3*1e-2*sin(PI*temps*(temps>1));
		//q4[i]=0.3*1e-2*sin(PI*temps*(temps<=1))+0.3*1e-2*sin(PI*temps*(temps>1));
		//temps=temps+delta_t;
	}

	for (i=0;i<ndof;i++){
		Told[i]=T0;
	}

  nnz=(int *)calloc(ndof,sizeof(int));
	nnz2=(int *)calloc(ndof,sizeof(int));


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Create vectors.  Note that we form 1 vector from scratch and
     then duplicate as needed.
  */

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&D);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&Tn);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F2);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&T);CHKERRQ(ierr);
	ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
	printf("nlocal=%d\n",nlocal);

	ierr = VecSet(D,0);CHKERRQ(ierr);
	ierr = VecSet(Tn,0);CHKERRQ(ierr);
	ierr = VecSet(F1,0);CHKERRQ(ierr);
	ierr = VecSet(F2,0);CHKERRQ(ierr);
	ierr = VecSet(T,0);CHKERRQ(ierr);

	double ratio;

	nz=ndof;
	nz2=ndof;

	for(i=0;i<ndof;i++){
		nnz[i]=0.02*ndof;
		}
					/* printf("Definition solide \n");

					for (i=0; i<ni; i++) {
					for (j=0+q1[compteur]; j<5+q1[compteur]; j++) {
						 i0=I2D(ni,i,j);
						 if(domain_optim[i0]==0) gamma_h[i0]=1;

					}
					}*/


								for (i=0; i<ni; i++) {
									for (j=0; j<nj; j++) {
										for (k=0; k<nk; k++) {
									i0=I3D(ni,nj,i,j,k);

										if(domain_optim[i0]==0 && j>10){
											lambda[i0]=lambda_vide; rho[i0]=rho_vide;cp[i0]=cp_vide; c[i0]=c_vide;
										}
										if(domain_optim[i0]==0 && j<=10){
											lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;c[i0]=c_solide;
										}
										if(domain_optim[i0]==1){
										lambda[i0]=lambda_vide+PetscPowScalar(gamma_h[i0],penal)*(lambda_solide-lambda_vide);
										rho[i0]=rho_vide+PetscPowScalar(gamma_h[i0],penal)*(rho_solide-rho_vide);
										cp[i0]=cp_vide+PetscPowScalar(gamma_h[i0],penal)*(cp_solide-cp_vide);
										diff[i0]=lambda[i0]/(rho[i0]*cp[i0]);
										c[i0]=c_vide+PetscPowScalar(gamma_h[i0],penal)*(c_solide-c_vide);
									}
									}
								}
							}


						//	ierr = VecGetOwnershipRange(x,&rstart,&rend);CHKERRQ(ierr);


					start = rank*(nele/size) + ((nele%size) < rank ? (nele%size) : rank);
				  end   = start + nele/size + ((nele%size) > rank);

				  printf("size=%d et rank=%d\n",size,rank);
				  printf("start=%d et end=%d\n",start,end);

					const PetscInt *nindices;


					//DIRICHLET BOUNDARY CONDITION
						cpt=0;
						for (i=0; i<ni; i++) {
						 for (j=0; j<nj; j++) {
							 for (k=0; k<nk; k++) {
						 i0=I3D(ni,nj,i,j,k);

								 if(boundary[i0]==1){
									 cpt=cpt+1;
								 }
							 }
						 }
					 }

					 PetscInt *edofMat2;
					 int nb_BC;
					 nb_BC=cpt;
					PetscMalloc1(8*nb_BC,&edofMat2);
					IS is;
					PetscInt n1,n2;
					cpt=0;
					for (i=0; i<ni; i++) {
					 for (j=0; j<nj; j++) {
						 for (k=0; k<nk; k++) {
					 i0=I3D(ni,nj,i,j,k);

							 if(boundary[i0]==1){
								 value=1;
								 edofMat2[8*cpt]=i+(ni+1)*j+k*(ni+1)*(nj+1);
								 edofMat2[1+8*cpt]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
								 edofMat2[2+8*cpt]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
								 edofMat2[3+8*cpt]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
								 edofMat2[4+8*cpt]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
								 edofMat2[5+8*cpt]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
								 edofMat2[6+8*cpt]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
								 edofMat2[7+8*cpt]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
									cpt=cpt+1;
							 }
						 }
						}
					}

					ISCreateGeneral(PETSC_COMM_SELF,8*nb_BC,edofMat2,PETSC_COPY_VALUES,&is);
					 ISGetLocalSize(is,&n1);
					ISSortRemoveDups(is);
					 ISGetLocalSize(is,&n2);

						ISGetIndices(is,&nindices);

					 printf("n1=%d et n2=%d \n",n1,n2);

	for (k2_it=0;k2_it<opt_it_max;k2_it++){


	printf("OPTIMIZATION PROBLEM - BEGINNING \n ");

int compteur=0;
/*FILE *fichier_compliance;
fichier_compliance=fopen("compliance_T.txt","w");*/
int jc;
jc=0;
//double lambda1;

//VecView(b,PETSC_VIEWER_STDOUT_SELF);
//getchar();


	while(compteur<nbsteps){

		ierr = MatCreate(PETSC_COMM_WORLD,&K1);CHKERRQ(ierr);
		ierr = MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
		ierr = MatSetFromOptions(K1);CHKERRQ(ierr);
		ierr = MatSetUp(K1);CHKERRQ(ierr);
		if(size>1) ierr=MatMPIAIJSetPreallocation(K1,50,NULL,50,NULL);CHKERRQ(ierr);
		if(size==1) ierr=MatSeqAIJSetPreallocation(K1,50,NULL);CHKERRQ(ierr);

		ierr = MatCreate(PETSC_COMM_WORLD,&M1);CHKERRQ(ierr);
		ierr = MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
		ierr = MatSetFromOptions(M1);CHKERRQ(ierr);
		ierr = MatSetUp(M1);CHKERRQ(ierr);
		if(size>1) ierr=MatMPIAIJSetPreallocation(M1,50,NULL,50,NULL);CHKERRQ(ierr);
		if(size==1) ierr=MatSeqAIJSetPreallocation(M1,50,NULL);CHKERRQ(ierr);

		ierr = MatCreate(PETSC_COMM_WORLD,&U1);CHKERRQ(ierr);
		ierr = MatSetSizes(U1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
		ierr = MatSetFromOptions(U1);CHKERRQ(ierr);
		ierr = MatSetUp(U1);CHKERRQ(ierr);
		if(size>1) ierr=MatMPIAIJSetPreallocation(U1,50,NULL,50,NULL);CHKERRQ(ierr);
		if(size==1) ierr=MatSeqAIJSetPreallocation(U1,50,NULL);CHKERRQ(ierr);


		// Loop over elements
		for (i0=start; i0<end; i0++) {

		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%nj;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

		edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
		edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
		edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
		edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
		edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
		edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
		edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
		edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

					for (l=0;l<8*8;l++){
					ke[l]=KE[l]*lambda[i0]/(delta_y*delta_x*delta_z);
					me[l]=ME[l]*c[i0]/delta_t;
							}
				// Add values to the sparse matrix
				ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
				ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
			//	}
		}

			ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

			// Loop over elements
			for (i0=start; i0<end; i0++) {

				kcol=i0/(ni*nj);
				jcol=(i0%(nj*ni))/ni;
				icol=	(i0%(nj*ni))%nj;
				id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

			edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
			edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
			edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
			edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
			edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
			edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
			edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
			edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

						/*if(boundary[i0]==1){
						edofMat_f(i,j,k,ni,nj,nk,edofMat);
						for (l=0;l<8*8;l++)	ke[l]=HE1[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
					}*/

						if(boundary[i0]==2){
						for (l=0;l<8*8;l++)	ke[l]=HE2[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==3){
						for (l=0;l<8*8;l++)	ke[l]=HE3[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==4){
						for (l=0;l<8*8;l++)	ke[l]=HE4[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==5){
						for (l=0;l<8*8;l++) ke[l]=HE5[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}

						if(boundary[i0]==6){
						for (l=0;l<8*8;l++)	ke[l]=HE6[l]*hconv/(delta_z*delta_x);
						ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						}
		}


			ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

		ierr = MatZeroRows(K1,n2,nindices,1,0,0);CHKERRQ(ierr);
		ierr = MatZeroRows(M1,n2,nindices,1,0,0);CHKERRQ(ierr);


/////////// Load //////////////

for (i=0; i<(ni+1); i++) {
		for (j=0; j<(nj+1); j++) {
			for (k=0; k<nk; k++) {
		i0=I3D(ni2,nj2,i,j,k);
		pos_q1[i0]=0;

		}
	}
}

cpt=0;
	for (i=0; i<ni; i++) {
			for (j=0+q1[compteur]; j<1+q1[compteur]; j++) {
				for (k=0; k<nk; k++) {
			i0=I3D(ni,nj,i,j,k);


				if(domain_optim[i0]==0){
					kcol=i0/(ni*nj);
					jcol=(i0%(nj*ni))/ni;
					icol=	(i0%(nj*ni))%nj;
					id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

				edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
				edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
				edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
				edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
				edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
				edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
				edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
				edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

			pos_q1[edofMat[0]]=1;
			pos_q1[edofMat[1]]=1;
			pos_q1[edofMat[2]]=1;
			pos_q1[edofMat[3]]=1;
			pos_q1[edofMat[4]]=1;
			pos_q1[edofMat[5]]=1;
			pos_q1[edofMat[6]]=1;
			pos_q1[edofMat[7]]=1;
			cpt=cpt+1;

			for (l=0;l<8*8;l++){
			ke[l]=KE[l]*(lambda_solide-lambda_vide)/(delta_x*delta_y*delta_z);
			me[l]=ME[l]*(c_solide-c_vide)/delta_t;
							}
		// Add values to the sparse matrix
		ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
		ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);


		}
			}
	}
}
	num_elem[compteur]=cpt;
	printf("num_elem=%d\n",num_elem[compteur]);

	ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	//ierr=MatAXPY(M1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

	//Addition de M et K
	ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
	ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);

	printf("Load \n");

	for (i0=start; i0<end; i0++) {

		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%nj;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

	if(num_elem[compteur]>0){
		F[edofMat[0]]=pos_q1[edofMat[0]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[1]]=pos_q1[edofMat[1]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[2]]=pos_q1[edofMat[2]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[3]]=pos_q1[edofMat[3]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[4]]=pos_q1[edofMat[4]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[5]]=pos_q1[edofMat[5]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[6]]=pos_q1[edofMat[6]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[edofMat[7]]=pos_q1[edofMat[7]]*0.05/(num_elem[compteur]*delta_x*delta_y*delta_z);//
	}
	else{
		F[edofMat[0]]=0;//
		F[edofMat[1]]=0;//
		F[edofMat[2]]=0;//
		F[edofMat[3]]=0;//
		F[edofMat[4]]=0;//
		F[edofMat[5]]=0;//
		F[edofMat[6]]=0;//
		F[edofMat[7]]=0;//
	}

		/*	F[edofMat[0]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[1]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[2]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[3]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[4]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[5]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[6]]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[edofMat[7]]=(double)0.01/(delta_x*delta_y*delta_z);//*/

	}

	printf("Complete load vector \n");

	//Normal node
	for (i=0; i<ndof; i++) {
		value=F[i];
		ierr = VecSetValues(b,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);

	}

	//Dirichlet condition

	for (i0=start; i0<end; i0++) {

		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%nj;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

	/*	if(boundary[i0]==1){
		edofMat_f(i,j,k,ni,nj,nk,edofMat);

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}*/

		if(boundary[i0]==2){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==3){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==4){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==5){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==6){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

				if(boundary[i0]==1){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}
	}


	//Prod mat vecteur
	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);

	VecAXPY(b,1,F1);

 //Set exact solution; then compute right-hand-side vector.
printf("Solver beginning \n");
//ierr = VecSet(x,0);CHKERRQ(ierr);
 /*ierr = KSPSetOperators(ksp,U1,U1);CHKERRQ(ierr);
	ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
	ierr = PCSetType(pc,PCJACOBI);CHKERRQ(ierr);
	ierr = KSPSetTolerances(ksp,1e-5,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
	ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);
	 ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	 ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);
	 ierr = KSPGetResidualNorm(ksp,&rnorm);CHKERRQ(ierr);*/

ierr = solver.setA(U1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);

/*for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	Told[i]=y;
}

cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
T_transient[i]=x1[cpt];
pos_q1_transient[i]=pos_q1[cpt];
cpt=cpt+1;
}

somme=0;
//Fonction cout
for (i=0; i<ni; i++) {
	for (j=0; j<nj; j++) {
		for (k=0; k<nk; k++) {
	i0=I3D(ni,nj,i,j,k);
edofMat_f(i,j,k,ni,nj,nk,edofMat);
PetscScalar uKu=0.0;
for (PetscInt k=0;k<8;k++){
for (PetscInt h=0;h<8;h++){
	uKu += x1[edofMat[k]]*KE[k*8+h]*x1[edofMat[h]];
}
}
somme=somme+lambda[i0]*uKu;
//gradient[i0]=-penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu;
}
}
}

compliance[compteur]=somme;
fprintf(fichier_compliance,"%f\n",somme);
//printf("fonction cout=%f\n",fonction_cout[k2_it]);*/

compteur=compteur+1;
printf("compteur=%d\n",compteur);

ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);
ierr = MatDestroy(&U1);CHKERRQ(ierr);

} //Fin boucle transient

//double diff_time;
//fclose(fichier_compliance);

//Fonction cout
/*
somme=0;
somme2=0;
for (i=0;i<nbsteps;i++) {
	somme=somme+compliance[i]*pow(alpha,compliance[i]);
	somme2=somme2+pow(alpha,compliance[i]);
}

fonction_cout[k2_it]=somme/somme2;*/


/*FILE *temp_ijk_0, *temp_ijk_05, *temp_ijk_1;

temp_ijk_0=fopen("temp_ijk_0.txt","w");
temp_ijk_05=fopen("temp_ijk_05.txt","w");
temp_ijk_1=fopen("temp_ijk_1.txt","w");

for (i=0; i<(ni+1); i++) {
	for (j=0; j<(nj+1); j++) {
		for (k=0; k<(nk+1); k++) {
	i0=I3D(ni2,nj2,i,j,k);
	fprintf(temp_ijk_0,"%d %d %d %f %f \n",i,j,k,T_transient[i0],T_transient[i0]);
	fprintf(temp_ijk_05,"%d %d %d %f %f \n",i,j,k,T_transient[(int)(0.5*(nbsteps-1))*ndof+i0],T_transient[(int)(0.5*(nbsteps-1))*ndof+i0]);
	fprintf(temp_ijk_1,"%d %d %d %f %f \n",i,j,k,T_transient[(int)((nbsteps-1))*ndof+i0],T_transient[(int)((nbsteps-1))*ndof+i0]);
}
}
}

fclose(temp_ijk_0);
fclose(temp_ijk_05);
fclose(temp_ijk_1);*/


/*FILE *fichier_fonction_cout;
strcpy(filename,"h012_fonction_cout_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_fonction_cout=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
//fichier_fonction_cout=fopen("fonction_cout","w");

for (i=0;i<=k2_it;i++) fprintf(fichier_fonction_cout,"%f\n",fonction_cout[i]);

fclose(fichier_fonction_cout);*/



} //Fin boucle optim

solver.finalize();

printf("Fin boucle optim \n");

start2 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
end2   = start2 + ndof/size + ((ndof%size) > rank);

printf("size=%d et rank=%d\n",size,rank);
printf("start2=%d et end2=%d\n",start2,end2);
FILE *test0_f,*test1_f,*test2_f,*test3_f;

if(rank==0) test0_f=fopen("test0.txt","w");
if(rank==1) test1_f=fopen("test1.txt","w");
if(rank==2) test2_f=fopen("test2.txt","w");
if(rank==3) test3_f=fopen("test3.txt","w");

for(i=start2;i<end2;i++) {
if(rank==0){	VecGetValues(x,1,&i,&y); fprintf(test0_f,"%f\n",y);}
if(rank==1) {	VecGetValues(x,1,&i,&y); fprintf(test1_f,"%f\n",y);}
if(rank==2) {	VecGetValues(x,1,&i,&y); fprintf(test2_f,"%f\n",y);}
if(rank==3) {	VecGetValues(x,1,&i,&y); fprintf(test3_f,"%f\n",y);}

}

MPI_Barrier(PETSC_COMM_WORLD);
if(rank==0) fclose(test0_f);
if(rank==1) fclose(test1_f);
if(rank==2) fclose(test2_f);
if(rank==3) fclose(test3_f);
/*
FILE *test2;
ierr =PetscFOpen(PETSC_COMM_WORLD,"temperature.txt","w",&test2);

ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
ierr = VecGetArray(x,&array);CHKERRQ(ierr);

for(i=0;i<nlocal;i++) {
PetscSynchronizedFPrintf(PETSC_COMM_WORLD,test2,"%f\n",array[i]);
PetscSynchronizedFlush(PETSC_COMM_WORLD,test2);
//printf("i=%d\n",i);
}


//ierr = VecRestoreArray(x,&array);CHKERRQ(ierr);
//ierr =PetscBarrier(NULL);CHKERRQ(ierr);
ierr =PetscFClose(PETSC_COMM_WORLD,test2);CHKERRQ(ierr);*/



ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);
//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);
return ierr;
}
