#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <cublas.h>
#include <cublas_v2.h>
#include <sys/time.h>
#include <sys/resource.h>


#define NDim 3
#define MNE 8

#define BLOCKSIZE 32

#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)
#define I2D(ni,i,j) (((ni)*(j)) + i)

const char *sSDKname = "conjugateGradientMultiDeviceCG";

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

void KM_matrix(double delta_x, double delta_y,double delta_z, double *Ke,double *Me, double *He1, double *He2, double *He3,
  double *He4, double *He5, double *He6, double *Fe1, double *Fe2,double *Fe3, double *Fe4,double *Fe5, double *Fe6, double *Qe1){
//  double N[8];
  double DN[3][8],N[8];
  double gxyz[3][3];
  double wg[3][3],wxyz;
  int i,j,k,i1,j1;
  double a,b,c;
  double x,y,z,sommeK,sommeM,sommeHe1,sommeHe2,sommeHe3,sommeHe4,sommeHe5,sommeHe6, sommeFe1,sommeFe2,sommeFe3,sommeFe4,sommeFe5,sommeFe6;
  double sommeQe1;

  //gxyz[0][0]=0;
  //gxyz[0][1]=-0.577350269189626;
  gxyz[0][2]=-sqrt(3.f/5);
  //gxyz[1][0]=0;
  //gxyz[1][1]=0.577350269189626;
  gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
  gxyz[2][2]=sqrt(3.f/5);

  wg[0][0]=2;
  wg[0][1]=1;
  wg[0][2]=5.f/9;
  wg[1][0]=0;
  wg[1][1]=1;
  wg[1][2]=8.f/9;
  wg[2][0]=0;
  wg[2][1]=0;
  wg[2][2]=5.f/9;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;
        //printf("boucle\n");


  for (i1=0;i1<8;i1++){
      for (j1=0;j1<8;j1++){
sommeK=0;
sommeM=0;
          for (i=0;i<3;i++){
            for (j=0;j<3;j++){
              for (k=0;k<3;k++){

                x=gxyz[i][2];
                y=gxyz[j][2];
                z=gxyz[k][2];
                wxyz=wg[i][2]*wg[j][2]*wg[k][2];

                N[0]=(1-x)*(1-y)*(1-z)/8;
                N[1]=(1+x)*(1-y)*(1-z)/8;
                N[2]=(1+x)*(1+y)*(1-z)/8;
                N[3]=(1-x)*(1+y)*(1-z)/8;

                N[4]=(1-x)*(1-y)*(1+z)/8;
                N[5]=(1+x)*(1-y)*(1+z)/8;
                N[6]=(1+x)*(1+y)*(1+z)/8;
                N[7]=(1-x)*(1+y)*(1+z)/8;

                DN[0][0]=-(1-y)*(1-z)*(1/a)*0.125;   DN[1][0]=-(1-x)*(1-z)*(1/b)*0.125;    DN[2][0]=-(1-x)*(1-y)*(1/c)*0.125;
                DN[0][1]=(1-y)*(1-z)*(1/a)*0.125;    DN[1][1]=-(1+x)*(1-z)*(1/b)*0.125;    DN[2][1]=-(1+x)*(1-y)*(1/c)*0.125;
                DN[0][2]=(1+y)*(1-z)*(1/a)*0.125;    DN[1][2]=(1+x)*(1-z)*(1/b)*0.125;     DN[2][2]=-(1+x)*(1+y)*(1/c)*0.125;
                DN[0][3]=-(1+y)*(1-z)*(1/a)*0.125;   DN[1][3]=(1-x)*(1-z)*(1/b)*0.125;     DN[2][3]=-(1-x)*(1+y)*(1/c)*0.125;
                DN[0][4]=-(1-y)*(1+z)*(1/a)*0.125;   DN[1][4]=-(1-x)*(1+z)*(1/b)*0.125;    DN[2][4]=(1-x)*(1-y)*(1/c)*0.125;
                DN[0][5]=(1-y)*(1+z)*(1/a)*0.125;    DN[1][5]=-(1+x)*(1+z)*(1/b)*0.125;    DN[2][5]=(1+x)*(1-y)*(1/c)*0.125;
                DN[0][6]=(1+y)*(1+z)*(1/a)*0.125;    DN[1][6]=(1+x)*(1+z)*(1/b)*0.125;     DN[2][6]=(1+x)*(1+y)*(1/c)*0.125;
                DN[0][7]=-(1+y)*(1+z)*(1/a)*0.125;   DN[1][7]=(1-x)*(1+z)*(1/b)*0.125;     DN[2][7]=(1-x)*(1+y)*(1/c)*0.125;

                sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1]+DN[2][i1]*DN[2][j1])*wxyz*a*b*c;
                sommeM=sommeM+(N[i1]*N[j1])*wxyz*a*b*c;

              }
            }
          }
          Ke[i1+8*j1]=sommeK;
          Me[i1+8*j1]=sommeM;
          //printf("%lg ",Me[i1+8*j1]);
    }
      //printf("\n");
  }

//printf("\n \n");

//HE1
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe1=sommeHe1+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He1[i1+8*j1]=sommeHe1;
        //printf("%lg ",He1[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE2
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe2=sommeHe2+(N[i1]*N[j1])*wxyz*b*c;

            }
          }
        //}
        He2[i1+8*j1]=sommeHe2;
        //printf("%lg ",He2[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE3
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe3=sommeHe3+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He3[i1+8*j1]=sommeHe3;
        //printf("%lg ",He3[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE4
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe4=sommeHe4+(N[i1]*N[j1])*wxyz*a*c;

            }
          }
        //}
        He4[i1+8*j1]=sommeHe4;
        //printf("%lg ",He4[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");


//HE5
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe5=sommeHe5+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He5[i1+8*j1]=sommeHe5;
        //printf("%lg ",He5[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//HE6
for (i1=0;i1<8;i1++){
    for (j1=0;j1<8;j1++){
        sommeHe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeHe6=sommeHe6+(N[i1]*N[j1])*wxyz*a*b;

            }
          }
        //}
        He6[i1+8*j1]=sommeHe6;
        //printf("%lg ",He6[i1+8*j1]);
  }
    //printf("\n");
}

//printf("\n \n");

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe1=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=-1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe1=sommeFe1+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe1[i1]=sommeFe1;
        //printf("%lg ",Fe1[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE2
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe2=0;
        //for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=1;//gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe2=sommeFe2+N[i1]*wxyz*b*c;

            }
          }
        //}
        Fe2[i1]=sommeFe2;
        //printf("%lg ",Fe2[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE3
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe3=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=-1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe3=sommeFe3+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe3[i1]=sommeFe3;
        //printf("%lg ",Fe3[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE4
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe4=0;
        for (i=0;i<3;i++){
          //for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=1;//gxyz[j][2];
              z=gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe4=sommeFe4+N[i1]*wxyz*a*c;

            }
          }
        //}
        Fe4[i1]=sommeFe4;
        //printf("%lg ",Fe4[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE5
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe5=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=-1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe5=sommeFe5+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe5[i1]=sommeFe5;
        //printf("%lg ",Fe5[i1]);
  //}
    //printf("\n");
}

//printf("\n \n");

//FE6
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeFe6=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            //for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=1;//gxyz[k][2];
              //wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              wxyz=wg[i][2]*wg[j][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeFe6=sommeFe6+N[i1]*wxyz*a*b;

            }
          }
        //}
        Fe6[i1]=sommeFe6;
        //printf("%lg ",Fe6[i1]);
  //}
    //printf("\n");
}

//FE1
for (i1=0;i1<8;i1++){
  //  for (j1=0;j1<8;j1++){
        sommeQe1=0;
        for (i=0;i<3;i++){
          for (j=0;j<3;j++){
            for (k=0;k<3;k++){

              x=gxyz[i][2];
              y=gxyz[j][2];
              z=gxyz[k][2];
              wxyz=wg[i][2]*wg[j][2]*wg[k][2];
              //wxyz=wg[j][2]*wg[k][2];

              N[0]=(1-x)*(1-y)*(1-z)/8;
              N[1]=(1+x)*(1-y)*(1-z)/8;
              N[2]=(1+x)*(1+y)*(1-z)/8;
              N[3]=(1-x)*(1+y)*(1-z)/8;

              N[4]=(1-x)*(1-y)*(1+z)/8;
              N[5]=(1+x)*(1-y)*(1+z)/8;
              N[6]=(1+x)*(1+y)*(1+z)/8;
              N[7]=(1-x)*(1+y)*(1+z)/8;

              sommeQe1=sommeQe1+N[i1]*wxyz*a*b*c;

            }
          }
        }
        Qe1[i1]=sommeQe1;
        printf("%lg ",Qe1[i1]);
  //}
    //printf("\n");
}

}

  // Set residual to zero
//void SetResZero(double *r, int numNodes)
//{
//	memset(r,0.0,sizeof(numNodes));
//}

__global__ void active_domain(int ni, int nj, int numElems, int base_height, int layer_height, int *domain_optim)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k;

  if (e<numElems){

    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;

    if(k<base_height) {
       domain_optim[e]=0;
     }
     else{
           //if(i<(100+10) && i>=(100-10) && j<(100+10) && j>=(100-10) && k<80){
       if(i<2 && j<2 && k<=layer_height){
         domain_optim[e]=0;
       }
       else{
         domain_optim[e]=1;
       }
     }

  }
}

__global__ void Boundary_Conditions1(int ni, int nj, int nk, int numElems, int base_height,int *domain_optim,int *convnodes_x,int *convnodes_y, int *convnodes_z,
  double *hconv1,double *hconv2,double *hconv3,double *hconv4,double *hconv5, double *hconv6){

    size_t e = blockIdx.x * blockDim.x + threadIdx.x;
    int i,j,k,id,i1,edofMat[MNE];

    if (e<numElems){

      k=e/(ni*nj);
      j=(e%(nj*ni))/ni;
      i=(e%(nj*ni))%ni;

      id=(ni+1)*(nj+1)*k+(ni+1)*j+i;

      edofMat[0]=id;
      edofMat[1]=id+1;
      edofMat[2]=id+1+(ni+1);
      edofMat[3]=id+(ni+1);
      edofMat[4]=id + (ni+1)*(nj+1);
      edofMat[5]=id+1 + (ni+1)*(nj+1);
      edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
      edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

      //if(i==0 && domain_optim[e]==0)       {hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1;   convnodes_x[edofMat[4]]=1;    convnodes_x[edofMat[7]]=1;}
      if(i==(ni-1) && domain_optim[e]==0)  {hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2;   convnodes_x[edofMat[5]]=2;    convnodes_x[edofMat[6]]=2;}
      //if(j==0 && domain_optim[e]==0)       {hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   convnodes_y[edofMat[4]]=3;    convnodes_y[edofMat[5]]=3;}
      if(j==(nj-1) && domain_optim[e]==0)  {hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   convnodes_y[edofMat[6]]=4;    convnodes_y[edofMat[7]]=4;}
      if(k==0 && domain_optim[e]==0)       {hconv5[e]=5;    convnodes_z[edofMat[0]]=5;      convnodes_z[edofMat[1]]=5;   convnodes_z[edofMat[2]]=5;    convnodes_z[edofMat[3]]=5;}
      //if(k==(nk-1) && domain_optim[e]==0)  {hconv6[e]=50;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
      /*/i1=e-1;
      if(i1>0){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv1[e]=5;    convnodes_x[edofMat[0]]=1;      convnodes_x[edofMat[3]]=1;   convnodes_x[edofMat[4]]=1;    convnodes_x[edofMat[7]]=1;
        }
      }*/
      i1=e+1;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv2[e]=5;    convnodes_x[edofMat[1]]=2;      convnodes_x[edofMat[2]]=2;   convnodes_x[edofMat[5]]=2;    convnodes_x[edofMat[6]]=2;
        }
      }
      /*i1=e-ni;
      if(i1>0){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv3[e]=5;    convnodes_y[edofMat[0]]=3;      convnodes_y[edofMat[1]]=3;   convnodes_y[edofMat[4]]=3;    convnodes_y[edofMat[5]]=3;
        }
      }*/
      i1=e+ni;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          hconv4[e]=5;    convnodes_y[edofMat[2]]=4;      convnodes_y[edofMat[3]]=4;   convnodes_y[edofMat[6]]=4;    convnodes_y[edofMat[7]]=4;
        }
      }
      i1=e+ni*nj;
      if(i1<ni*nj*nk){
        if(domain_optim[e]==0 && domain_optim[i1]==1) {
          if(k<base_height)       {hconv6[e]=5;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
          if(k>=base_height)       {hconv6[e]=55;     convnodes_z[edofMat[4]]=6;      convnodes_z[edofMat[5]]=6;   convnodes_z[edofMat[6]]=6;    convnodes_z[edofMat[7]]=6;}
        }
      }

    }
  }


  __global__ void Boundary_Conditions2(int ni, int nj, int numElems, int *domain_optim,int *convnodes_x,int *convnodes_y, int *convnodes_z,
    double *hconv1,double *hconv2,double *hconv3,double *hconv4,double *hconv5, double *hconv6, double Tinf, double *b1,
    double *Fe1,double *Fe2,double *Fe3,double *Fe4,double *Fe5,double *Fe6 ){

      size_t e = blockIdx.x * blockDim.x + threadIdx.x;
      int i,j,k,l,id,edofMat[MNE];

      if (e<numElems){

        k=e/(ni*nj);
        j=(e%(nj*ni))/ni;
        i=(e%(nj*ni))%ni;

        id=(ni+1)*(nj+1)*k+(ni+1)*j+i;

        edofMat[0]=id;
        edofMat[1]=id+1;
        edofMat[2]=id+1+(ni+1);
        edofMat[3]=id+(ni+1);
        edofMat[4]=id + (ni+1)*(nj+1);
        edofMat[5]=id+1 + (ni+1)*(nj+1);
        edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
        edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

        for(l=0;l<MNE;l++){
                if(convnodes_x[edofMat[l]]==1)  {
                //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                  //b1[edofMat[l]]=b1[edofMat[l]]+hconv1[e]*Tinf*Fe1[l];
                  atomicAdd(&b1[edofMat[l]], hconv1[e]*Tinf*Fe1[l]);

                }
                if(convnodes_x[edofMat[l]]==2) {
                  //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                   //b1[edofMat[l]]=b1[edofMat[l]]+hconv2[e]*Tinf*Fe2[l];
                    atomicAdd(&b1[edofMat[l]], hconv2[e]*Tinf*Fe2[l]);
                 }

                 if(convnodes_y[edofMat[l]]==3)  {
                 //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                   //b1[edofMat[l]]=b1[edofMat[l]]+hconv3[e]*Tinf*Fe3[l];
                   atomicAdd(&b1[edofMat[l]], hconv3[e]*Tinf*Fe3[l]);

                 }
                 if(convnodes_y[edofMat[l]]==4) {
                   //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                    //b1[edofMat[l]]=b1[edofMat[l]]+hconv4[e]*Tinf*Fe4[l];
                    atomicAdd(&b1[edofMat[l]], hconv4[e]*Tinf*Fe4[l]);
                  }

                  if(convnodes_z[edofMat[l]]==5)  {
                  //  printf("element=%d, convnodes=1 : edof=%d, Fe1[l]=%f\n",e,edofMat[l], Fe1[l]);
                    //b1[edofMat[l]]=b1[edofMat[l]]+hconv5[e]*Tinf*Fe5[l];
                    atomicAdd(&b1[edofMat[l]], hconv5[e]*Tinf*Fe5[l]);

                  }
                  if(convnodes_z[edofMat[l]]==6) {
                    //printf("element=%d, convnodes=2 : edof=%d, Fe2[l]=%f\n",e,edofMat[l], Fe2[l]);
                     //b1[edofMat[l]]=b1[edofMat[l]]+hconv6[e]*Tinf*Fe6[l];
                     atomicAdd(&b1[edofMat[l]], hconv6[e]*Tinf*Fe6[l]);
                   }
            }

      }
    }


__global__ void Mat_Properties(double *U, int ni, int nj, int *domain_optim, double *kcond, double *rho, double *c,int numElems)
{

  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,id1,edofMat2[MNE];
  double Te;

  if (e<numElems){

    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;
    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);

    Te=0.125*(U[edofMat2[0]]+U[edofMat2[1]]+U[edofMat2[2]]+U[edofMat2[3]]
      +U[edofMat2[4]]+U[edofMat2[5]]+U[edofMat2[6]]+U[edofMat2[7]]);

    //id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    if(domain_optim[e]==0){

    /*  c[e]=421+0.1827*Te;
      kcond[e]=11.9+0.01543*Te;
      rho[e]=8220-0.3663*Te;*/
            //if(k<=height_k[compteur]){

      if(Te<=20) c[e]=421;
      if(Te>20 && Te<=100)  c[e] =(double)421+(442-421)*(Te-20)/(100-20);
      if(Te>100 && Te<=200) c[e] =(double)442+(453-442)*(Te-100)/(200-100);
      if(Te>200 && Te<=300) c[e] =(double)453+(472-453)*(Te-200)/(300-200);
      if(Te>300 && Te<=400) c[e] =(double)472+(481-472)*(Te-300)/(400-300);
      if(Te>400 && Te<=500) c[e] =(double)481+(502-481)*(Te-400)/(500-400);
      if(Te>500 && Te<=600) c[e] =(double)502+(527-502)*(Te-500)/(600-500);
      if(Te>600 && Te<=700) c[e] =(double)527+(562-527)*(Te-600)/(700-600);
      if(Te>700 && Te<=800) c[e] =(double)562+(606-562)*(Te-700)/(800-700);
      if(Te>800 && Te<=850) c[e] =(double)606+(628-606)*(Te-800)/(850-800);
      if(Te>850 && Te<=900) c[e] =(double)628+(636-628)*(Te-850)/(900-850);
      if(Te>900 && Te<=1000)  c[e]=(double)636+(647-636)*(Te-900)/(1000-900);
      if(Te>1000 && Te<=1100) c[e]=(double)647+(651-647)*(Te-1000)/(1100-1000);
      if(Te>1100 && Te<=1500) c[e]=(double)651+(652-651)*(Te-1100)/(1500-1100);
      if(Te>1500) c[e]=652;

      if(Te<=22) kcond[e]=11.9;
      if(Te>22 && Te<=233)  kcond[e] =(double)11.9+(13.7-11.9)*(Te-22)/(233-22);
      if(Te>233 && Te<=448) kcond[e] =(double)13.7+(16.9-13.7)*(Te-233)/(448-233);
      if(Te>448 && Te<=657) kcond[e] =(double)16.9+(21.7-16.9)*(Te-448)/(657-448);
      if(Te>657 && Te<=866) kcond[e] =(double)21.7+(25.6-21.7)*(Te-657)/(866-657);
      if(Te>866 && Te<=1079) kcond[e] =(double)25.6+(22.9-25.6)*(Te-866)/(1079-866);
      if(Te>1079 && Te<=1289) kcond[e] =(double)22.9+(19.1-22.9)*(Te-1079)/(1289-1079);
      if(Te>1289 && Te<=1500) kcond[e] =(double)19.1+(17.7-19.1)*(Te-1289)/(1500-1289);
      if(Te>1500) kcond[e]=17.7;


      if(Te<=20) rho[e]=8220;
      if(Te>20 && Te<=227)     rho[e] =(double)8220+(8121-8220)*(Te-20)/(227-20);
      if(Te>227 && Te<=427)    rho[e] =(double)8121+(8121-8048)*(Te-227)/(427-227);
      if(Te>427 && Te<=727)    rho[e] =(double)8048+(8121-8048)*(Te-427)/(727-427);
      if(Te>727 && Te<=927)    rho[e] =(double)7961+(8121-8048)*(Te-727)/(927-727);
      if(Te>927 && Te<=1127)   rho[e] =(double)7875+(8121-8048)*(Te-927)/(1127-927);
      if(Te>1127 && Te<=1260)  rho[e] =(double)7787+(8121-8048)*(Te-1127)/(1260-1127);
      if(Te>1260 && Te<=1344)  rho[e] =(double)7733+(8121-8048)*(Te-1260)/(1344-1260);
      if(Te>1344 && Te<=1450)  rho[e] =(double)7579+(8121-8048)*(Te-1344)/(1450-1344);
      if(Te>1450 && Te<=1527)  rho[e] =(double)7488+(8121-8048)*(Te-1450)/(1527-1450);
      if(Te>1527 && Te<=1827)  rho[e] =(double)7488+(8121-8048)*(Te-1527)/(1827-1527);
      if(Te>1827) rho[e]=7341;
      //}
        //if(k>height_k[compteur]){
          //kcond[e]=lambda_vide;
          //rhoc[e]=rho_vide*cp_vide;
        //}
    }

  }

}

__global__ void ResetRes(double *r, int numNodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
     r[n] = 0.0;
  }
}

__global__ void GPUMatVec_Uold(int ni, int nj, double *rho, double *c, double *Me, double *U, double *r, size_t numElems)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * BLOCKSIZE + threadIdx.x;


  if (e<numElems)
  {

    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;
    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);


  /*  k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;

    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    //local DOFs
    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);*/

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
        Au[j]= rho[e]*c[e]*Me[i*MNE+j]*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }
}


__global__ void GPUMatVec(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, double *hconv5, double *hconv6, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *He5, double *He6, double *U, double *r, size_t numElems, int *domain_optim)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au[MNE],sum;
  size_t e = blockIdx.x * BLOCKSIZE + threadIdx.x;


  if (e<numElems)
  {
    if(domain_optim[e]==0 ){
    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;
    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);

  /*  k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;

    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    //local DOFs
    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);*/

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
        Au[j]= (rho[e]*c[e]*Me[i*MNE+j]+kcond[e]*Ke[i*MNE+j]+hconv1[e]*He1[i*MNE+j]+hconv2[e]*He2[i*MNE+j]+hconv3[e]*He3[i*MNE+j]+
          hconv4[e]*He4[i*MNE+j]+hconv5[e]*He5[i*MNE+j]+hconv6[e]*He6[i*MNE+j])*U[edofMat2[j]];
                                  }

      sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7];
      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[i]], sum);
                         }
  }
}
}


__global__ void GPUMatVec_Precond(double *kcond, double *rho, double *c, double *hconv1, double *hconv2, double *hconv3, double *hconv4, double *hconv5, double *hconv6, int ni, int nj, double *Me, double *Ke,
  double *He1, double *He2, double *He3, double *He4, double *He5, double *He6, double *r, size_t numElems,int *domain_optim)

{
  //global gpu thread index
	//double Au;

  int i,j,k,id1,edofMat2[MNE];
  double Au;
  size_t e = blockIdx.x * BLOCKSIZE + threadIdx.x;


  if (e<numElems)
  {
if(domain_optim[e]==0 ){
    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;
    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

    edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);


	for (int id=0; id<MNE; id++){
        for (int j=0; j<MNE; j++){
        //  Au[j]=0;
        if(j==id){
        Au= rho[e]*c[e]*Me[id*MNE+j]+kcond[e]*Ke[id*MNE+j]+hconv1[e]*He1[id*MNE+j]+hconv2[e]*He2[id*MNE+j]+hconv3[e]*He3[id*MNE+j]+
                  hconv4[e]*He4[id*MNE+j]+hconv5[e]*He5[id*MNE+j]+hconv6[e]*He6[id*MNE+j];
                }
                                  }


      //sum=Au[0]+Au[1]+Au[2]+Au[3];
    //  if(fixednodes[edofMat2[i]]==0)
    atomicAdd(&r[edofMat2[id]], Au);
                         }
  }
}
}

__global__ void rzp_calculation(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes, int *activenodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 || activenodes[n]==0){
      r[n]=0;
    }
    else{
    M_pre[n]= 1.f/M_pre[n];
      //printf("M[%d]=%f\n",n,M_pre[n]);
      r[n] = f[n] - prod[n];
      z[n] = M_pre[n]*r[n];
      p[n] = z[n];
    }

  }

}


__global__ void heatsource_position(int ni, int nj, int *domain_optim, int base_height,int layer_height, double *pos_heat, int *activenodes, int numElems)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int edofMat2[MNE];

  int i,j,k,id1;

  if (e<numElems)
  {

    k=e/(ni*nj);
    j=(e%(nj*ni))/ni;
    i=(e%(nj*ni))%ni;
    id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;
      if(domain_optim[e]==0){
        edofMat2[0]=id1;
        edofMat2[1]=id1+1;
        edofMat2[2]=id1+1+(ni+1);
        edofMat2[3]=id1+(ni+1);
        edofMat2[4]=id1 + (ni+1)*(nj+1);
        edofMat2[5]=id1+1 + (ni+1)*(nj+1);
        edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
        edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);


    //if(height_k[compteur]>1 && k>=0+height_k[compteur] && k<layer_thickness+height_k[compteur]){
    //if(k>=base_height && k<=layer_height){
    if(k==layer_height){
      pos_heat[e]=1;

    /*pos_heat[edofMat2[0]]=1;
    pos_heat[edofMat2[1]]=1;
    pos_heat[edofMat2[2]]=1;
    pos_heat[edofMat2[3]]=1;
    pos_heat[edofMat2[4]]=1;
    pos_heat[edofMat2[5]]=1;
    pos_heat[edofMat2[6]]=1;
    pos_heat[edofMat2[7]]=1;*/
   }
  //if(k<=height_k[compteur]){

  /*  edofMat2[0]=id1;
    edofMat2[1]=id1+1;
    edofMat2[2]=id1+1+(ni+1);
    edofMat2[3]=id1+(ni+1);
    edofMat2[4]=id1 + (ni+1)*(nj+1);
    edofMat2[5]=id1+1 + (ni+1)*(nj+1);
    edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
    edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);*/

    activenodes[edofMat2[0]]=1;
    activenodes[edofMat2[1]]=1;
    activenodes[edofMat2[2]]=1;
    activenodes[edofMat2[3]]=1;
    activenodes[edofMat2[4]]=1;
    activenodes[edofMat2[5]]=1;
    activenodes[edofMat2[6]]=1;
    activenodes[edofMat2[7]]=1;

    //  }
  }


  }

}

/*__global__ void heatsource_position2(double *pos_heat, int numNodes, int *symnodes)
{

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
                if (symnodes[n]==1 && pos_heat[n]==1) pos_heat[n] = 0.5;
                if (symnodes[n]==2 && pos_heat[n]==1) pos_heat[n] = 0.25;
                if (symnodes[n]==3 && pos_heat[n]==1) pos_heat[n] = 0.125;
    }


}*/

__global__ void heatsource_term(int ni, int nj, int *convnodes_x,int *convnodes_y, int *convnodes_z, double *pos_heat, double *b, int numElems, int heating,double Qvol, double *Qe1)
{
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;
  int i,j,k,id1,edofMat2[MNE];

  if (e<numElems){

      if(pos_heat[e]==1){
        k=e/(ni*nj);
        j=(e%(nj*ni))/ni;
        i=(e%(nj*ni))%ni;
        id1=(ni+1)*(nj+1)*k+(ni+1)*j+i;

        edofMat2[0]=id1;
        edofMat2[1]=id1+1;
        edofMat2[2]=id1+1+(ni+1);
        edofMat2[3]=id1+(ni+1);
        edofMat2[4]=id1 + (ni+1)*(nj+1);
        edofMat2[5]=id1+1 + (ni+1)*(nj+1);
        edofMat2[6]=id1+1+(ni+1) + (ni+1)*(nj+1);
        edofMat2[7]=id1+(ni+1) + (ni+1)*(nj+1);

        atomicAdd(&b[edofMat2[0]], heating*Qvol*Qe1[0]);
        atomicAdd(&b[edofMat2[1]], heating*Qvol*Qe1[1]);
        atomicAdd(&b[edofMat2[2]], heating*Qvol*Qe1[2]);
        atomicAdd(&b[edofMat2[3]], heating*Qvol*Qe1[3]);
        atomicAdd(&b[edofMat2[4]], heating*Qvol*Qe1[4]);
        atomicAdd(&b[edofMat2[5]], heating*Qvol*Qe1[5]);
        atomicAdd(&b[edofMat2[6]], heating*Qvol*Qe1[6]);
        atomicAdd(&b[edofMat2[7]], heating*Qvol*Qe1[7]);
      }

        //f(flux_elems>0) b[n] = pos_heat[n]*Qvol/flux_elems;
      //b[n] = b[n]+pos_heat[n]*Qvol;
      //if(convnodes_x[n]==0 && convnodes_y[n]==0 && convnodes_z[n]==0) b[n]=pos_heat[n]*Qvol;
    }
}

__global__ void force_transient(double *f, double *b, double *b1, int numNodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
        f[n] = f[n] + b[n] + b1[n];
        //f[n]=b[n];
    }
}

__global__ void UpdateVec(double *p, double *r, double beta, int numNodes, int *fixednodes,int *activenodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  && activenodes[n]==1){
      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void Norm(double *r, int numNodes, int *fixednodes,int *activenodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  && activenodes[n]==1){
      atomicAdd(&result[0], r[n]*r[n]);
    }
    }
}

__global__ void Prod_rz(double *r, double*z, int numNodes, int *fixednodes,int *activenodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0  && activenodes[n]==1){
      atomicAdd(&result[0], r[n]*z[n]);
    }
    }
}
__global__ void Update_UR(double *U, double *r, double *p, double *Ap, double alpha,  int numNodes, int *fixednodes, int *activenodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 && activenodes[n]==1){
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
    }
    }
}

__global__ void zCalculation(double *M_pre, double *r, double *z,  int numNodes, int *fixednodes,int *activenodes)
    {
      size_t n = blockIdx.x * blockDim.x + threadIdx.x;

      if (n<numNodes){
        if(fixednodes[n]==0 && activenodes[n]==1){
          z[n] = M_pre[n]*r[n];
        }
        }
    }


int main(int argc, char **argv)
{

  //Check out my device properties
  int nDevices;
  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
           prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
           prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
           2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  }


  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

  cudaSetDevice(2);

int i,j,k,i0,i1,iter,layer_height,step,heating;
double t,dt;
size_t free_memory, total_memory;

//size of the domain
double domain_x=0.05;
double domain_y=0.05;


//number of elements in each direction
const int ni=20;
const int nj=20;


int build_height=1250;
int base_height=80;
const int nk=build_height+base_height;
int total_steps=21;
double domain_z=40e-6*nk;

double Tinf=35;

//space steps
const double delta_x=(double)domain_x/ni;
const double delta_y=(double)domain_y/nj;
const double delta_z=(double)domain_z/nk;

//volume multiplier
double Qvol=3.9343e10;


//Total number of elements & nodes for the 3D structured uniform grid
const int numElems=ni*nj*nk;
const int numNodes=(ni+1)*(nj+1)*(nk+1);
//const int slice=(ni+1)*(nj+1);


//GET GPU DEVICE
printf("Starting [%s]...\n", sSDKname);


    //create handle for Ddot on gpu
    cublasHandle_t handle;
    cublasCreate(&handle);

//////////////////////////////////////// Solve Poisson ////////////////////////////////////////
/*------let's let the NVIDIA to take care allocation of matrices using the Unified Memory API----------*/

  int *fixednodes_d, *activenodes_d, *convnodes_x_d, *convnodes_y_d,*convnodes_z_d; //*ndofMat_d;
  int *domain_optim_d;
  double *pos_heat_d;
  double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double *Ap_d, *Ke_d, *Me_d, *Qe1_d, *He1_d, *He2_d, *He3_d, *He4_d, *He5_d, *He6_d, *kcond_d, *hconv1_d, *hconv2_d, *hconv3_d, *hconv4_d, *hconv5_d, *hconv6_d, *rho_d, *c_d; //z_d
  double *Fe1_d, *Fe2_d, *Fe3_d, *Fe4_d, *Fe5_d, *Fe6_d;
  double *pAp_d;
  double *rznew_d;
  double *rNorm_d,*fNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  double *fNorm = (double *) calloc(1,sizeof(double));
  double norme;
  char filename[500], u2[500];
//  double *Au;

  //Allocate CPU memory using MallocManaged
  double *Ke = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me = (double *) malloc(MNE*MNE * sizeof(double));
  double *Me1 = (double *) malloc(MNE*MNE * sizeof(double));
  double *He1 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He2 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He3 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He4 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He5 = (double *)calloc(MNE*MNE, sizeof(double));
  double *He6 = (double *)calloc(MNE*MNE, sizeof(double));
  double *Fe1 = (double *)calloc(MNE, sizeof(double));
  double *Fe2 = (double *)calloc(MNE, sizeof(double));
  double *Fe3 = (double *)calloc(MNE, sizeof(double));
  double *Fe4 = (double *)calloc(MNE, sizeof(double));
  double *Fe5 = (double *)calloc(MNE, sizeof(double));
  double *Fe6 = (double *)calloc(MNE, sizeof(double));
  double *Qe1 = (double *)calloc(MNE, sizeof(double));

  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  int *activenodes = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_x = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_y = (int *)calloc(numNodes, sizeof(int));
  int *convnodes_z = (int *)calloc(numNodes, sizeof(int));
  double *pos_heat = (double *)calloc(numElems, sizeof(double));
  double *probe1 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe2 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe3 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe4 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe5 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe6 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe7 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe8 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe9 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe10 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe11 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe12 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe13 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe14 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe15 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe16 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe17 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe18 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe19 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe20 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe21 = (double *)calloc(build_height*total_steps, sizeof(double));
  double *probe22 = (double *)calloc(build_height*total_steps, sizeof(double));
  int *domain_optim = (int *)calloc(numElems, sizeof(int));
  double *kcond = (double *)calloc(numElems, sizeof(double));
  double *rho = (double *)calloc(numElems, sizeof(double));
  double *c = (double *)calloc(numElems, sizeof(double));
  double *hconv1 = (double *)calloc(numElems, sizeof(double));
  double *hconv2 = (double *)calloc(numElems, sizeof(double));
  double *hconv3 = (double *)calloc(numElems, sizeof(double));
  double *hconv4 = (double *)calloc(numElems, sizeof(double));
  double *hconv5 = (double *)calloc(numElems, sizeof(double));
  double *hconv6 = (double *)calloc(numElems, sizeof(double));
  //int *ndofMat = (int *) malloc(MNE*numElems * sizeof(int));

  double *U = (double *) calloc(numNodes , sizeof(double));
  double *U1 = (double *) calloc(numNodes , sizeof(double));
  double *U2 = (double *) calloc(numNodes , sizeof(double));
  double *U3 = (double *) calloc(numNodes , sizeof(double));
  double *U4 = (double *) calloc(numNodes , sizeof(double));
  double *U5 = (double *) calloc(numNodes , sizeof(double));
  double *U6 = (double *) calloc(numNodes , sizeof(double));
  double *U7 = (double *) calloc(numNodes , sizeof(double));
  double *U8 = (double *) calloc(numNodes , sizeof(double));
  double *U9 = (double *) calloc(numNodes , sizeof(double));
  double *U10 = (double *) calloc(numNodes , sizeof(double));
  double *U11 = (double *) calloc(numNodes , sizeof(double));
  double *U12 = (double *) calloc(numNodes , sizeof(double));
  double *U13 = (double *) calloc(numNodes , sizeof(double));
  double *U14 = (double *) calloc(numNodes , sizeof(double));
  double *U15 = (double *) calloc(numNodes , sizeof(double));
  double *U16 = (double *) calloc(numNodes , sizeof(double));
  double *U17 = (double *) calloc(numNodes , sizeof(double));
  double *U18 = (double *) calloc(numNodes , sizeof(double));
  double *U19 = (double *) calloc(numNodes , sizeof(double));
  double *U20 = (double *) calloc(numNodes , sizeof(double));
  double *U21 = (double *) calloc(numNodes , sizeof(double));
  double *U22 = (double *) calloc(numNodes , sizeof(double));
  double *U23 = (double *) calloc(numNodes , sizeof(double));
  double *U24 = (double *) calloc(numNodes , sizeof(double));
  double *U25 = (double *) calloc(numNodes , sizeof(double));
  double *r = (double *) calloc(numNodes , sizeof(double));
  double *prod = (double *) calloc(numNodes , sizeof(double));
  double *M_pre = (double *) calloc(numNodes , sizeof(double));
  double *b = (double *) calloc(numNodes , sizeof(double));
  double *b1 = (double *) calloc(numNodes , sizeof(double));
  double *f = (double *) calloc(numNodes , sizeof(double));
  double *p = (double *) calloc(numNodes , sizeof(double));
  double *z = (double *) calloc(numNodes , sizeof(double));
  double *Ap = (double *) calloc(numNodes , sizeof(double));


  // Allocate memory
  cudaMalloc((void **) &Ke_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Me_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He4_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He5_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &He6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe1_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe2_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe3_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe4_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe5_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Fe6_d, MNE*MNE * sizeof(double));
  cudaMalloc((void **) &Qe1_d, MNE * sizeof(double));
  cudaMalloc((void **) &pos_heat_d, numElems * sizeof(double));
  cudaMalloc((void **) &domain_optim_d, numElems * sizeof(int));
  cudaMalloc((void **) &activenodes_d, numNodes * sizeof(int));
  cudaMalloc((void **) &convnodes_x_d, numNodes * sizeof(int));
  cudaMalloc((void **) &convnodes_y_d, numNodes * sizeof(int));
  cudaMalloc((void **) &convnodes_z_d, numNodes * sizeof(int));

  cudaMallocManaged((void **) &U_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &f_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &b_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &b1_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &M_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &r_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &prod_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &p_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &z_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &Ap_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &fixednodes_d, numNodes * sizeof(int));
  cudaMallocManaged((void **) &kcond_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &rho_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &c_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv1_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv2_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv3_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv4_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv5_d, numNodes * sizeof(double));
  cudaMallocManaged((void **) &hconv6_d, numNodes * sizeof(double));


  //additional variable for reduction operations
  cudaMallocManaged((void **) &pAp_d, 1 * sizeof(double));
  cudaMallocManaged((void **) &rznew_d, 1 * sizeof(double));
  cudaMallocManaged((void **) &rNorm_d, 1 * sizeof(double));
  cudaMallocManaged((void **) &fNorm_d, 1 * sizeof(double));

  cudaMemcpy(pAp_d, pAp, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rznew_d, rznew, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(rNorm_d, rNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);
  cudaMemcpy(fNorm_d, fNorm, 1 * sizeof(double),cudaMemcpyHostToDevice);

  //Calculation of the element matrix
  KM_matrix(delta_x,delta_y,delta_z,Ke,Me1,He1,He2,He3,He4,He5,He6,Fe1,Fe2,Fe3,Fe4,Fe5,Fe6,Qe1);

  //Calculation of the element transient matrix : implicit scheme : Ue=Ke+Me
      for(size_t n=0; n<numNodes; n++) {
        k=n/((ni+1)*(nj+1));

        //if(k==0) {fixednodes[n]=6; U[n]=80;}
      }


    int ni2=ni+1;
    int nj2=nj+1;

    FILE *solution;

    cudaMemcpy(Ke_d, Ke, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He1_d, He1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He2_d, He2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He3_d, He3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He4_d, He4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He5_d, He5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(He6_d, He6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe1_d, Fe1, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe2_d, Fe2, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe3_d, Fe3, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe4_d, Fe4, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe5_d, Fe5, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Fe6_d, Fe6, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Qe1_d, Qe1, MNE * sizeof(double),cudaMemcpyHostToDevice);

    cudaMemcpy(b_d, b, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(b1_d, b1, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(fixednodes_d, fixednodes, numNodes * sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(activenodes_d, activenodes, numNodes * sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(convnodes_x_d, convnodes_x, numNodes * sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(convnodes_y_d, convnodes_y, numNodes * sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(convnodes_z_d, convnodes_z, numNodes * sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(kcond_d, kcond, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv1_d, hconv1, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv2_d, hconv2, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv3_d, hconv3, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv4_d, hconv4, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv5_d, hconv5, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(hconv6_d, hconv6, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(rho_d, rho, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(c_d, c, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(pos_heat_d, pos_heat, numElems * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(domain_optim_d, domain_optim, numElems * sizeof(int),cudaMemcpyHostToDevice);

    cudaMemcpy(U_d, U, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(prod_d, prod, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(r_d, r, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(p_d, p, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(f_d, f, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(Ap_d, Ap, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(z_d, z, numNodes * sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(M_d, M_pre, numNodes * sizeof(double),cudaMemcpyHostToDevice);


  int  NBLOCKS   = (numElems+BLOCKSIZE-1) / BLOCKSIZE; //round up if n is not a multiple of blocksize
  printf("Number of Blocks: %d\n",NBLOCKS);
  //dim3 dimBlock(BLOCKSIZE, BLOCKSIZE);
  printf("numNodes: %d\n", numNodes);
  printf("numElems: %d\n", numElems);
  int  NBLOCKS2 = (numNodes+BLOCKSIZE-1) / BLOCKSIZE; //round up if n is not a multiple of blocksize
  const double TOL = 1.0e-6;

  int cpt=0;

    //Beginning of the transient loop
    for (layer_height=base_height;layer_height<build_height+base_height;layer_height++){

    active_domain <<<NBLOCKS,BLOCKSIZE>>> (ni,nj,numElems,base_height,layer_height,domain_optim_d);
    cudaMemset(hconv1_d, 0.0, numElems*sizeof(double));
    cudaMemset(hconv2_d, 0.0, numElems*sizeof(double));
    cudaMemset(hconv3_d, 0.0, numElems*sizeof(double));
    cudaMemset(hconv4_d, 0.0, numElems*sizeof(double));
    cudaMemset(hconv5_d, 0.0, numElems*sizeof(double));
    cudaMemset(hconv6_d, 0.0, numElems*sizeof(double));
    Boundary_Conditions1 <<<NBLOCKS,BLOCKSIZE>>>(ni,nj,nk,numElems,base_height,domain_optim_d,convnodes_x_d,convnodes_y_d,convnodes_z_d,hconv1_d,hconv2_d, hconv3_d, hconv4_d,hconv5_d, hconv6_d);

      cudaMemset(b1_d, 0.0, numNodes*sizeof(double));
    Boundary_Conditions2 <<<NBLOCKS,BLOCKSIZE>>>(ni,nj,numElems,domain_optim_d,convnodes_x_d,convnodes_y_d,convnodes_z_d,
      hconv1_d,hconv2_d, hconv3_d, hconv4_d,hconv5_d, hconv6_d,Tinf,b1_d, Fe1_d,Fe2_d,Fe3_d,Fe4_d,Fe5_d,Fe6_d );

      for (step=0;step<21;step++){

        if(step<1) {
          dt=0.9; heating=1;
        }
      if(step>=1 && step<11) {

          dt=0.11; heating=0;
        }
      if(step>=11 && step<21) {

            dt=0.8; heating=0;
          }

        /*  if(step<9) {
              dt=0.1; heating=1;
            }
            else{
              dt=0.1; heating=0;
            }*/

        for(size_t n=0; n<MNE*MNE; n++) {
          Me[n]=Me1[n]/dt;
        }

       cudaMemcpy(Me_d, Me, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);

  //Uold _matvec
  //MatVec_Uold(ni,nj, Me, U, f, numElems, numNodes);
  Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_d,ni,nj,domain_optim_d,kcond_d,rho_d,c_d,numElems);

  cudaMemset(f_d, 0.0, numNodes*sizeof(double));
  GPUMatVec_Uold<<<NBLOCKS,BLOCKSIZE>>>(ni,nj, rho_d,c_d, Me_d, U_d, f_d, numElems);

  cudaMemset(pos_heat_d, 0.0, numElems*sizeof(double));
  heatsource_position<<<NBLOCKS,BLOCKSIZE>>>(ni,nj, domain_optim_d,base_height,layer_height,pos_heat_d,activenodes_d,numElems);


  cudaMemset(b_d, 0.0, numNodes*sizeof(double));
  heatsource_term<<<NBLOCKS,BLOCKSIZE>>>(ni,nj,convnodes_x_d,convnodes_y_d,convnodes_z_d,pos_heat_d,b_d,numElems,heating,Qvol,Qe1_d);

  force_transient<<<NBLOCKS2,BLOCKSIZE>>>(f_d, b_d, b1_d,numNodes);

  //define misc vars
  //double rNorm = 0.0;
  //double bNorm = 0.0;
  double rzold = 0.0;
  //double rznew = 0.0;
  double alpha = 0.0;
  double beta  = 0.0;
  //double malpha = -alpha;


  //Initialize MatVec on the CPU
  //MatVec(kcond,rhoc, hconv1,hconv2, ni,nj, Ke, Me, He1, He2, U, r, numElems, numNodes);
  cudaMemset(r_d, 0.0, numNodes*sizeof(double));
  cudaMemset(prod_d, 0.0, numNodes*sizeof(double));
  GPUMatVec<<<NBLOCKS,BLOCKSIZE>>>(kcond_d, rho_d,c_d, hconv1_d, hconv2_d, hconv3_d, hconv4_d, hconv5_d, hconv6_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, He5_d, He6_d, U_d, prod_d, numElems,domain_optim_d);


//  cudaMemcpy(r, r_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
    //cudaMemcpy(M, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  //for (int n=0; n<numNodes; n++){
    //printf("r[%d]=%f \n",n,r[n]);
    // if (U[n] < 0) U[n] = 0.0;
  //}

  //preconditioner
  //Precond(kcond,rhoc,hconv1,hconv2,ni,nj,Ke, He1, He2, M_pre, numElems, numNodes);
    cudaMemset(M_d, 0.0, numNodes*sizeof(double));
  GPUMatVec_Precond<<<NBLOCKS,BLOCKSIZE>>>(kcond_d, rho_d,c_d, hconv1_d, hconv2_d, hconv3_d, hconv4_d, hconv5_d, hconv6_d,
    ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, He5_d, He6_d, M_d, numElems,domain_optim_d);


  rzp_calculation<<<NBLOCKS2,BLOCKSIZE>>>(f_d, M_d, r_d,prod_d, z_d, p_d, numNodes, fixednodes_d,activenodes_d);


  //cudaMemcpy(M_pre, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  //cudaMemcpy(p, p_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  //cudaMemcpy(z, z_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
    //cudaMemcpy(M, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  for (int n=0; n<numNodes; n++){
    //printf("M_pre=%f \n",M_pre[n]);
    // if (U[n] < 0) U[n] = 0.0;
  }

  //Reset pAp value
  cudaMemset(pAp_d, 0.0, 1*sizeof(double));
  //Apply Ddot
  //cublasDdot(handle, numNodes, p_d, 1, r_d, 1, pAp_d);
  Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_d, r_d,numNodes,fixednodes_d,activenodes_d,pAp_d);
  cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);


    // control phi
  rzold = pAp[0];

//  printf("rzold=%lg\n",rzold);


  //define some values

  const int Max_iters = 5000;
  //double pAp = 0.0;
  //int node;


  r_usage.ru_maxrss = 0.0; //set memory to zero
  t = get_time(); // get time stamp


    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

    Mat_Properties <<<NBLOCKS,BLOCKSIZE>>>(U_d,ni,nj,domain_optim_d,kcond_d,rho_d,c_d,numElems);

    //reset result [Ap_d==0]
    //ResetRes<<<NBLOCKS,BLOCKSIZE>>>(Ap_d,numElems*MNE);
    //cudaDeviceSynchronize();
    cudaMemset(Ap_d, 0.0, numNodes*sizeof(double));

    //Apply Matrix-Free MatVec kernel [Loop over elements]
    GPUMatVec<<<NBLOCKS,BLOCKSIZE>>>(kcond_d, rho_d, c_d,  hconv1_d, hconv2_d, hconv3_d, hconv4_d, hconv5_d, hconv6_d,
       ni,nj, Me_d,Ke_d, He1_d, He2_d, He3_d, He4_d, He5_d, He6_d, p_d, Ap_d, numElems,domain_optim_d);



    //Apply Diriclhlet BC [U==r==0] on faces [Loop over elements]
    //DirichletBC<<<NBLOCKS,BLOCKSIZE>>>(p_d, Ap_d, numElems, edofMat_d, face_d);
    //cudaDeviceSynchronize();


    //Reset pAp value
    cudaMemset(pAp_d, 0.0, 1*sizeof(double));
    //Apply Ddot
    //cublasDdot(handle, numNodes, p_d, 1, Ap_d, 1, pAp_d);
    Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(p_d, Ap_d,numNodes,fixednodes_d,activenodes_d,pAp_d);
    cudaMemcpy(pAp, pAp_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);
    //printf("pAp_d[0]=%f\n",pAp_d[0]);

    // control alpha
    alpha = rzold/pAp[0];
    //printf("rzold = %1.12f pAp = %1.12f   alpha = %1.12f\n",rzold,pAp_d[0], alpha);

    Update_UR<<<NBLOCKS2,BLOCKSIZE>>>(U_d, r_d, p_d, Ap_d,alpha, numNodes,fixednodes_d,activenodes_d);


    //Update solution vectors[U,r]
    //daxpy (+alpha)
    //cublasDaxpy(handle, numNodes, &( alpha), p_d, 1, U_d, 1); //y = ax + y
    //daxpy (-alpha)
    //malpha = -alpha;
    //cublasDaxpy(handle, numNodes, &(malpha), Ap_d, 1, r_d, 1); //y = -ax + y


    //Reset Norm
    cudaMemset(rNorm_d, 0.0, 1*sizeof(double));
    //cudaMemset(fNorm_d, 0.0, 1*sizeof(double));
    //Apply Dnorm
    //cublasDnrm2(handle, numNodes, r_d, 1, rNorm_d);
    Norm<<<NBLOCKS2,BLOCKSIZE>>>(r_d, numNodes,fixednodes_d,activenodes_d,rNorm_d);
    //Norm<<<NBLOCKS2,BLOCKSIZE>>>(f_d, numNodes,fixednodes_d,activenodes_d,fNorm_d);
    cudaMemcpy(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);
    //cudaMemcpy(fNorm, fNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);
    norme=rNorm[0];
    // converge if reached Tolerance 1e-6
    if(norme <= TOL) break;

    //Calculation for preconditioner
    zCalculation<<<NBLOCKS2,BLOCKSIZE>>>(M_d, r_d, z_d, numNodes,fixednodes_d,activenodes_d);


    //Identity Copy [z==r]
    //cublasDcopy(handle, numNodes, r_d, 1, z_d, 1);
    //cudaDeviceSynchronize();


    //Update rznew
      cudaMemset(rznew_d, 0.0, 1*sizeof(double));
    //cublasDdot(handle, numNodes, z_d, 1, r_d, 1, rznew_d);
    Prod_rz<<<NBLOCKS2,BLOCKSIZE>>>(r_d, z_d,numNodes,fixednodes_d,activenodes_d,rznew_d);
    cudaMemcpy(rznew, rznew_d, 1 * sizeof(double),cudaMemcpyDeviceToHost);

    //Check residual norm and my alpha
    //printf("iter = %i, rNorm = %lg \n", iter, rNorm_d[0]);

    // control beta
    beta = rznew[0]/rzold;

    //update vector [p] for next iters...
    UpdateVec<<<NBLOCKS2,BLOCKSIZE>>>(p_d, z_d, beta, numNodes,fixednodes_d,activenodes_d);


    //update rzold for next iters...
    rzold = rznew[0];

//    if (iter==0) break;

  }
  printf("Layer %d/%d, Step %d/%d  it_PCG=%d and residual=%lg\n",layer_height-base_height,build_height,step,total_steps,iter,rNorm_d[0]);

  cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);

  /*strcpy(filename,"temp2d_");	sprintf(u2,"%d",cpt);	strcat(filename,u2);
  solution=fopen(filename,"w"); memset(filename, 0, sizeof(filename));

  j=0;
  for (k=0; k<(nk+1); k++) {
    for (i1=0; i1<(ni+1); i1++) {
        i0=I3D(ni2,nj2,i1,j,k);
        fprintf(solution,"%lg\n",U[i0]);
      }
    }

  fclose(solution);*/

  for (k=0; k<(nk+1); k++) {
    for (j=0; j<(nj+1); j++) {
      for (i1=0; i1<(ni+1); i1++) {
          i0=I3D(ni2,nj2,i1,j,k);
                  //fprintf(solution,"%d,%d,%d,%f,%d,%d\n",i,j,k, pos_heat[i0],activenodes[i0],convnodes[i0]);
                  //fprintf(solution,"%f\n",b[i0]);
                  if(k==0 && i1==0 && j==0) probe1[cpt]=U[i0];
                  if(k==40 && i1==0 && j==0) probe2[cpt]=U[i0];
                  if(k==85 && i1==0 && j==0) probe3[cpt]=U[i0];
                  if(k==250 && i1==0 && j==0) probe4[cpt]=U[i0];
                  if(k==500 && i1==0 && j==0) probe5[cpt]=U[i0];
                  if(k==800 && i1==0 && j==0) probe6[cpt]=U[i0];
                  if(k==1250 && i1==0 && j==0) probe7[cpt]=U[i0];

                  if(k==0 && i1==5 && j==0) probe8[cpt]=U[i0];
                  if(k==0 && i1==10 && j==0) probe9[cpt]=U[i0];
                  if(k==0 && i1==15 && j==0) probe10[cpt]=U[i0];

                  if(k==0 && i1==5 && j==5) probe11[cpt]=U[i0];
                  if(k==0 && i1==10 && j==10) probe12[cpt]=U[i0];
                  if(k==0 && i1==15 && j==15) probe13[cpt]=U[i0];
                  if(k==0 && i1==20 && j==20) probe14[cpt]=U[i0];

                  if(k==40 && i1==5 && j==5) probe15[cpt]=U[i0];
                  if(k==40 && i1==10 && j==10) probe16[cpt]=U[i0];
                  if(k==40 && i1==15 && j==15) probe17[cpt]=U[i0];
                  if(k==40 && i1==20 && j==20) probe18[cpt]=U[i0];

                  if(k==80 && i1==5 && j==5) probe19[cpt]=U[i0];
                  if(k==80 && i1==10 && j==10) probe20[cpt]=U[i0];
                  if(k==80 && i1==15 && j==15) probe21[cpt]=U[i0];
                  if(k==80 && i1==20 && j==20) probe22[cpt]=U[i0];


                }
            }
          }


    if(layer_height==100 && step==0){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      cudaMemcpy(pos_heat,pos_heat_d, numElems * sizeof(double),cudaMemcpyDeviceToHost);

      for (int n=0; n<numNodes; n++) U1[n]=U[n];
    }
    if(layer_height==200 && step==0){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U2[n]=U[n];
    }
    if(layer_height==300 && step==0){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U3[n]=U[n];
    }
    if(layer_height==300 && step==1){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U4[n]=U[n];

    }
    if(layer_height==300 && step==2){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U5[n]=U[n];
    }
    if(layer_height==300 && step==3){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U6[n]=U[n];
    }
    if(layer_height==300 && step==4){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U7[n]=U[n];
    }
    if(layer_height==300 && step==5){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U8[n]=U[n];
    }
    if(layer_height==300 && step==6){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U9[n]=U[n];
    }
    if(layer_height==300 && step==7){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U10[n]=U[n];
    }
    if(layer_height==300 && step==8){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      cudaMemcpy(activenodes,activenodes_d, numNodes * sizeof(int),cudaMemcpyDeviceToHost);
      cudaMemcpy(domain_optim,domain_optim_d, numElems * sizeof(int),cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U11[n]=U[n];
    }
    if(layer_height==300 && step==9){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U12[n]=U[n];
    }
    if(layer_height==300 && step==10){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U13[n]=U[n];
    }
    if(layer_height==300 && step==11){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U14[n]=U[n];
    }
    if(layer_height==300 && step==12){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U15[n]=U[n];
    }
    if(layer_height==300 && step==13){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U16[n]=U[n];
    }
    if(layer_height==300 && step==14){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U17[n]=U[n];
    }
    if(layer_height==300 && step==15){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U18[n]=U[n];
    }
    if(layer_height==300 && step==16){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U19[n]=U[n];
    }
    if(layer_height==300 && step==17){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U20[n]=U[n];
    }
    if(layer_height==300 && step==18){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U21[n]=U[n];
    }
    if(layer_height==300 && step==19){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U22[n]=U[n];
    }
    if(layer_height==300 && step==20){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U23[n]=U[n];
    }
    if(layer_height==800 && step==0){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U24[n]=U[n];

    }
    if(layer_height==1200 && step==0){
      cudaMemcpy(U, U_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
      for (int n=0; n<numNodes; n++) U25[n]=U[n];

    }
cpt=cpt+1;
}

}
    //kill handle
    cublasDestroy(handle);

  //Send solution [U] back to host


  // get memory usage
  getrusage(RUSAGE_SELF, &r_usage);

  cudaMemGetInfo(&free_memory,&total_memory);
printf("GPU total memory=%.2f and GPU free memory=%.2f\n",total_memory/1e9, free_memory/1e9);

  // StdOut prints
  printf("\n");
  printf("*** PCG Solver Converged after %d total iterations ***\n", iter+1);
  printf("\n");
  printf("Residual Reduction: [%1.4e] <== Tolerance [%1.4e]\n",rNorm_d[0],TOL);
  printf("\n");
  printf("Solver Statistics [Wall Time, Memory Usage]\n");
  // Compute Elapsed Time for accumulative iters
  printf("-------------------------------------------\n");
  printf("|   Elapsed Time: [%g sec]                 \n",get_time()-t);
  printf("|   Elapsed Time: [%g sec/iter]            \n",(get_time()-t)/(iter+1));
  printf("-------------------------------------------\n");
  printf("|  Memory Usage: [%lf MB]                  \n", r_usage.ru_maxrss*0.001);
  printf("-------------------------------------------\n");

  cudaMemcpy(kcond,kcond_d, numElems * sizeof(double),cudaMemcpyDeviceToHost);
  cudaMemcpy(activenodes, activenodes_d, numNodes * sizeof(int), cudaMemcpyDeviceToHost);

    //cudaMemcpy(M, M_d, numNodes * sizeof(double), cudaMemcpyDeviceToHost);
  for (int n=0; n<numNodes; n++){
    //printf("M[%d]=%f\n",n,M[n]);
    // if (U[n] < 0) U[n] = 0.0;
  }
  // Post-Processing

  solution = fopen("solutions1_transient_sym_AM.txt", "w");
  fprintf(solution, "i,j,k,a1,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15,u16,u17,u18,u19,u20,u21,u22,u23,u24,u25\n");
  //fprintf(solution, "i,j,k,u11,a11\n");


  double max=-1;
  double min=1e6;
  for (k=0; k<(nk+1); k++) {
    for (j=0; j<(nj+1); j++) {
      for (i=0; i<(ni+1); i++) {
          i0=I3D(ni2,nj2,i,j,k);
          fprintf(solution,"%lg,%lg,%lg,%d,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n",i*delta_x,j*delta_y,k*delta_z, activenodes[i0],U1[i0],U2[i0],U3[i0],U4[i0],U5[i0],U6[i0],U7[i0],U8[i0],U9[i0],U10[i0],U11[i0],U12[i0],U13[i0],U14[i0],U15[i0],U16[i0],U17[i0],U18[i0],U19[i0],U20[i0],U21[i0],U22[i0],U23[i0],U24[i0],U25[i0]);
          //fprintf(solution,"%d,%d,%d,%lg,%d\n",i,j,k,U11[i0],activenodes[i0]);
          //fprintf(solution,"%d,%d,%d,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n",i,j,k, U1[i0],U2[i0],U3[i0],U4[i0],U5[i0],U6[i0],U7[i0],U8[i0],U9[i0],U10[i0],U11[i0],U12[i0],U13[i0],U14[i0]);
          //fprintf(solution,"%d,%d,%d,%lg,%d\n",i,j,k,U11[i0],activenodes[i0]);
          if(U25[i0]>max) max=U25[i0];
          if(U25[i0]<min && U25[i0]>0) min=U25[i0];
              }
            }
          }
    printf("Tmin=%lg and Tmax=%lg\n",min,max);
  fclose(solution);

  solution = fopen("solutions2.txt", "w");
  fprintf(solution, "i,j,k,l,d\n");

  for (k=0; k<nk; k++) {
    for (j=0; j<nj; j++) {
      for (i=0; i<ni; i++) {
          i0=I3D(ni,nj,i,j,k);
                  fprintf(solution,"%d,%d,%d,%f,%d\n",i,j,k, pos_heat[i0],domain_optim[i0]);
                  //fprintf(solution,"%lg\n",U[i0]);
                  //if(U[i0]>max) max=U[i0];
              }
            }
          }
    //printf("Tmax=%lg\n",max);
  fclose(solution);

  solution = fopen("solutions3.txt", "w");
  //fprintf(solution, "i,j,k,pos,act,conv\n");
int max1=-1;
  for (k=0; k<(nk+1); k++) {
    for (j=0; j<(nj+1); j++) {
      for (i=0; i<(ni+1); i++) {
          i0=I3D(ni2,nj2,i,j,k);
                  //fprintf(solution,"%d,%d,%d,%f,%d,%d\n",i,j,k, pos_heat[i0],activenodes[i0],convnodes[i0]);
                  fprintf(solution,"%d %d %d %d \n",activenodes[i0],convnodes_x[i0],convnodes_y[i0],convnodes_z[i0]);
                  //if(pos_heat[i0]>max1) max1=pos_heat[i0];
              }
            }
          }
    printf("pos_max=%d\n",max1);
  fclose(solution);

  FILE *fichier_probe;
  fichier_probe= fopen("probes.txt","w");

  for (i=0;i<build_height*total_steps;i++) fprintf(fichier_probe,"%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg\n"
  ,probe1[i],probe2[i],probe3[i],probe4[i],probe5[i],probe6[i],probe7[i],probe8[i],probe9[i],probe10[i],probe11[i],probe12[i],probe13[i],probe14[i],probe15[i]
,probe16[i],probe17[i],probe18[i],probe19[i],probe20[i],probe21[i],probe22[i]);

  fclose(fichier_probe);

   //fprintf(solution, "i,j,u\n");
/*  int ni2=ni+1;
  double max=-1;
    for (j=0; j<(nj+1); j++) {
      for (i=0; i<(ni+1); i++) {
          i0=I2D(ni2,i,j);
                  //fprintf(solution,"%d,%d,%lg\n",i,j,U[i0]);
                  fprintf(solution,"%lg\n",U[i0]);
                  if(U[i0]>max) max=U[i0];
              }
            }
    printf("Tmax=%lg\n",max);
  fclose(solution);*/


  //free gpu memory
  cudaFree(U_d);   cudaFree(f_d);   cudaFree(b_d);   cudaFree(b1_d);  cudaFree(M_d);   cudaFree(prod_d);  cudaFree(fixednodes_d);
  cudaFree(kcond_d);   cudaFree(rho_d); cudaFree(c_d); cudaFree(hconv1_d); cudaFree(hconv2_d); cudaFree(hconv3_d); cudaFree(hconv4_d); cudaFree(hconv5_d); cudaFree(hconv6_d);
  cudaFree(r_d);
  cudaFree(p_d);
  cudaFree(Ap_d);
  cudaFree(z_d);
  cudaFree(Ke_d);   cudaFree(Me_d); cudaFree(Qe1_d);
  cudaFree(He1_d); cudaFree(He2_d); cudaFree(He3_d); cudaFree(He4_d); cudaFree(He5_d); cudaFree(He6_d);
  cudaFree(Fe1_d); cudaFree(Fe2_d); cudaFree(Fe3_d); cudaFree(Fe4_d); cudaFree(Fe5_d); cudaFree(Fe6_d);
  cudaFree(pos_heat_d); cudaFree(domain_optim_d); cudaFree(activenodes_d); cudaFree(convnodes_x_d); cudaFree(convnodes_y_d); cudaFree(convnodes_z_d);
  //cudaFree(face_d);
  //cudaFree(edofMat_d);
  cudaFree(pAp_d);
  cudaFree(rznew_d);
  cudaFree(rNorm_d);
  cudaFree(fNorm_d);

  //cudaFree(Au);


  //free cpu memory
  free(U);  free(U1);  free(U2);  free(U3);  free(U4);  free(U5);  free(U6); free(U7);  free(U8);  free(U9);  free(U10);  free(U11);  free(U12);  free(U13);
  free(U14);  free(U15);  free(U16);  free(U17);  free(U18);  free(U19);  free(U20); free(U21);  free(U22);  free(U23);  free(U24);  free(U25);
  free(probe1); free(probe2); free(probe3); free(probe4); free(probe5); free(probe6); free(probe7); free(probe8); free(probe9); free(probe10); free(probe11); free(probe12);
  free(probe13); free(probe14); free(probe15); free(probe16); free(probe17); free(probe18); free(probe19); free(probe20); free(probe21); free(probe22);
  free(domain_optim); free(kcond); free(rho); free(c); free(hconv1); free(hconv2); free(hconv3); free(hconv4); free(hconv5); free(hconv6);
  free(pos_heat); free(convnodes_x); free(convnodes_y); free(convnodes_z); free(activenodes); free(fixednodes);
  free(b); free(b1); free(f);
  free(r);
  free(Ke); free(Me); free(Me1); free(Qe1);
  free(He1); free(He2); free(He3); free(He4); free(He5); free(He6);
  free(Fe1); free(Fe2); free(Fe3); free(Fe4); free(Fe5); free(Fe6);
  free(p);
  free(Ap);
  free(z);
  //free(face);
  //free(edofMat);
  //free(ndofMat);

  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
