
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)


double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, double rmin, double *x, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,i0,i02,i2,j2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){

				i0 = I2D(ni2, i, j);

				somme=0;
				somme2=0;
						if(domain_optim[i0]==1){
				for (i2 = Maxi(i-floor(rmin),0); i2 < Mini(i+floor(rmin),ni2); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 < Mini(j+floor(rmin),nj2); j2++) {

								i02 = I2D(ni2,i2,j2);
					if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];
						}
					}
				}


			if(x[i0]*somme>0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}
		}
		else{
			df2[i0]=df[i0];

		}
			if(i==80 && j==75) printf("domain_optim[i0]=%d, somme2=%f, df2[81,76]=%f\n",domain_optim[i0],somme2,df2[i0]);
			if(i==119 && j==75) printf("domain_optim[i0]=%d, somme2=%f, df2[120,76]=%f\n",domain_optim[i0],somme2,df2[i0]);
		}
		}

}

void filtre2(int ni, int nj, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,i0,i02,i2,j2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){

				i0 = I2D(ni2, i, j);

				somme=0;
				somme2=0;

						if(domain_optim[i0]==1){

				for (i2 = Maxi(i-floor(rmin),0); i2 < Mini(i+floor(rmin),ni2); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 < Mini(j+floor(rmin),nj2); j2++) {

								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}

						if(somme>0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}
		}
		else {
			df2[i0]=df[i0];
		}

		}
		}

}

void OC (int ni, int nj, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				i0 = I2D(ni, i, j);
				gamma_old[i0]=gamma0[i0];
			}
		}


	while ((l2-l1)> 1e-8){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				i0 = I2D(ni, i, j);
if(domain_optim[i0]==1){

				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*pow(Be,1.f/2);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);
			}
		}
	}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
if(domain_optim[i0]==1){
					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

}
		}
	}


volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void KM_matrix_2D(double delta_x, double delta_y, double *KE, double *ME, double *HE1, double *HE2, double *HE3, double *HE4, double *FE1, double *FE2, double *FE3, double *FE4){
	double N[4];
	double DN[2][4];
	double gxy[3][3];
	double wg[3][3],wxy;
	int i,j,k,i1,j1;
	double a,b,a1,b1;
	double x,y,z,sommeK,sommeM;

	gxy[0][0]=0;
	gxy[0][1]=-0.577350269189626;
	gxy[0][2]=-0.774596669241483;
	gxy[1][0]=0;
	gxy[1][1]=0.577350269189626;
	gxy[1][2]=0;
	gxy[2][0]=0;
	gxy[2][1]=0;
	gxy[2][2]=0.774596669241483;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;


				printf("boucle\n");
				a=0.5*delta_x;
				b=0.5*delta_y;


	for (i1=0;i1<4;i1++){
			for (j1=0;j1<4;j1++){
sommeK=0;
sommeM=0;
					for (i=0;i<3;i++){
						for (j=0;j<3;j++){

								x=gxy[i][2];
								y=gxy[j][2];
								wxy=wg[i][2]*wg[j][2];


								N[0]=(1-x)*(1-y)/(2*2);
								N[1]=(1+x)*(1-y)/(2*2);
								N[2]=(1+x)*(1+y)/(2*2);
								N[3]=(1-x)*(1+y)/(2*2);


								DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
								DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
								DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
								DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);


								sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1])*wxy*a*b;
								sommeM=sommeM+(N[i1]*N[j1])*wxy*a*b;


						}
					}
					KE[i1+4*j1]=sommeK;
					ME[i1+4*j1]=sommeM;
					printf("%f ",ME[i1+4*j1]);
		}
			printf("\n");
	}

	//HE1
 printf("HE1\n");

	for (i1=0;i1<4;i1++){
			for (j1=0;j1<4;j1++){
				sommeM=0;

	for (i=0;i<3;i++){

				x=gxy[i][2];
				y=-1;//gxy[j][2];
				wxy=wg[i][2];


				N[0]=(1-x)*(1-y)/(2*2);
				N[1]=(1+x)*(1-y)/(2*2);
				N[2]=(1+x)*(1+y)/(2*2);
				N[3]=(1-x)*(1+y)/(2*2);


				DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
				DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
				DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
				DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

				sommeM=sommeM+(N[i1]*N[j1])*wxy*a;

	}

	HE1[i1+4*j1]=sommeM;
	printf("%f ",HE1[i1+4*j1]);
}
printf("\n");
}

//HE2
 printf("HE2\n");

for (i1=0;i1<4;i1++){
	for (j1=0;j1<4;j1++){
		sommeM=0;

for (j=0;j<3;j++){

		x=1;//gxy[i][2];
		y=gxy[j][2];
		wxy=wg[j][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+(N[i1]*N[j1])*wxy*b;

}

HE2[i1+4*j1]=sommeM;
printf("%f ",HE2[i1+4*j1]);
}
printf("\n");
}

//HE3
 printf("HE3\n");
for (i1=0;i1<4;i1++){
	for (j1=0;j1<4;j1++){
		sommeM=0;

for (i=0;i<3;i++){

		x=gxy[i][2];
		y=1;//gxy[j][2];
		wxy=wg[i][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+(N[i1]*N[j1])*wxy*a;

}

HE3[i1+4*j1]=sommeM;
printf("%f ",HE3[i1+4*j1]);
}
printf("\n");
}

//HE4
 printf("HE4\n");
for (i1=0;i1<4;i1++){
for (j1=0;j1<4;j1++){
sommeM=0;

for (j=0;j<3;j++){

x=-1;//gxy[i][2];
y=gxy[j][2];
wxy=wg[j][2];


N[0]=(1-x)*(1-y)/(2*2);
N[1]=(1+x)*(1-y)/(2*2);
N[2]=(1+x)*(1+y)/(2*2);
N[3]=(1-x)*(1+y)/(2*2);


DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

sommeM=sommeM+(N[i1]*N[j1])*wxy*b;

}

HE4[i1+4*j1]=sommeM;
printf("%f ",HE4[i1+4*j1]);
}
printf("\n");
}

//FE1

for (i1=0;i1<4;i1++){
		sommeM=0;

for (i=0;i<3;i++){

		x=gxy[i][2];
		y=-1;//gxy[j][2];
		wxy=wg[i][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+N[i1]*wxy*a;

}

FE1[i1]=sommeM;
printf("%f ",FE1[i1]);

}

//FE2

for (j1=0;j1<4;j1++){
		sommeM=0;

for (j=0;j<3;j++){

		x=1;//gxy[i][2];
		y=gxy[j][2];
		wxy=wg[j][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+N[j1]*wxy*b;

}

FE2[j1]=sommeM;
printf("%f ",FE2[j1]);

}

//FE3

for (i1=0;i1<4;i1++){
		sommeM=0;

for (i=0;i<3;i++){

		x=gxy[i][2];
		y=1;//gxy[j][2];
		wxy=wg[i][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+N[i1]*wxy*a;

}

FE3[i1]=sommeM;
printf("%f ",FE3[i1]);

}

//FE4

for (j1=0;j1<4;j1++){
		sommeM=0;

for (j=0;j<3;j++){

		x=-1;//gxy[i][2];
		y=gxy[j][2];
		wxy=wg[j][2];


		N[0]=(1-x)*(1-y)/(2*2);
		N[1]=(1+x)*(1-y)/(2*2);
		N[2]=(1+x)*(1+y)/(2*2);
		N[3]=(1-x)*(1+y)/(2*2);


		DN[0][0]=-(1-y)*(1/a)/(2*2); DN[1][0]=-(1-x)*(1/b)/(2*2);
		DN[0][1]=(1-y)*(1/a)/(2*2);  DN[1][1]=-(1+x)*(1/b)/(2*2);
		DN[0][2]=(1+y)*(1/a)/(2*2);  DN[1][2]=(1+x)*(1/b)/(2*2);
		DN[0][3]=-(1+y)*(1/a)/(2*2); DN[1][3]=(1-x)*(1/b)/(2*2);

		sommeM=sommeM+N[j1]*wxy*b;

}

FE4[j1]=sommeM;
printf("%f ",FE4[j1]);

}

}

int main(int argc,char **args)
{

	printf("Debut programme \n");



printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,K2,M1,M2,U1,U2,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end;
  PetscMPIInt    rank,size;
  PetscScalar    value,value0,value1,value2,value3,xnew;
  PetscScalar y;
	char filename[200],u2[200];

	double Xmin = 0.0001;
	double Xmax = 1;
	double movlim = 0.05;

	double *xmma = new double[40000];
	double *xold = new double[40000];
	double *f = new double[1];
	double *df = new double[40000];
	double *df2 = new double[40000];
	double *g = new double[1];
	double *dg = new double[40000];
	double *xmin = new double[40000];
	double *xmax = new double[40000];

	double *af = new double[40000];
	double *bf = new double[40000];
	// Initialize MMA
	MMASolver *mma = new MMASolver(40000,1);

	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
	double KE[16],ME[16],HE1[16],HE2[16],HE3[16],HE4[16],FE1[4],FE2[4],FE3[4],FE4[4],ke[16],me[16],fe[4];
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  clock_t start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int ni,nj,ni2, i,j,k,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim;

	ni = 200;
	nj = 200;
	ni2=201;

	ndof=(ni+1)*(nj+1);
	opt_it_max=30;
	alpha=1; // coeff pow compliance
	rmin=2;
	volfrac=0.3;
	float hconv=0;
	double delta,somme,somme1,somme2,delta_x, delta_y,delta_t,total_time,time;
	delta_x=(double)0.2/ni; //domaine de 10 mm par 10 mm ...
	delta_y=(double)0.2/nj;
	delta_t=0.1; // si delta t trop fort oscillations sur compliance
	total_time=150;
	int nbsteps;
	nbsteps=total_time/delta_t;

	//Medium properties
	int *edofMat;
	edofMat=(int *)calloc(4,sizeof(int));
	domain_optim=(int *)calloc(ni*nj,sizeof(int));
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp1,Tp2,Tp3,Tp4,Tair=20;
	cp=(double *)calloc(ni*nj,sizeof(double));
	lambda=(double *)calloc(ni*nj,sizeof(double));
	derivee_c=(double *)calloc(ni*nj,sizeof(double));
	derivee_lambda=(double *)calloc(ni*nj,sizeof(double));
	rho=(double *)calloc(ni*nj,sizeof(double));
	diff=(double *)calloc(ni*nj,sizeof(double));
	c=(double *)calloc(ni*nj,sizeof(double));
	rho_solide=5000;
	lambda_solide=50;
	cp_solide=1000;
	rho_vide=1;
	lambda_vide=2e-2;
	cp_vide=1000;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	Qvol=0;//10;
	T0=0; //Initial temperature
	Tp1=0; //Dirichlet temperature
	Tp2=0; //Dirichlet temperature
	Tp4=0;
	Qsurf=0*1e6;
	Tair=0;
	penal=3;


	AmgXSolver solver;

	int *boundary;
	boundary=(int *)calloc(ni*nj,sizeof(int));

	double *F;
	F=(double *)calloc(ndof,sizeof(double));

	double *x1;
	double *Told;
	double *T_transient, *pos_q1_transient, *T_transient2, *derivee_T_transient,*adjoint_transient,*prod_KU_transient,*second_membre_transient,*derivee_T;
	double *q2,*q3,*q4,*pos_q1,*pos_q2,*pos_q3,*pos_q4, *compliance, *gradient, *lu;
	int *q1, *num_elem;
	double *gamma_x, *gamma_h;
	double *fonction_cout;
	fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
//	solide=(double *)calloc(ni*nj,sizeof(double));
	gamma_h=(double *)calloc(ni*nj,sizeof(double));
	gamma_x=(double *)calloc(ni*nj,sizeof(double));
	x1=(double *)calloc(ndof,sizeof(double));
	Told=(double *)calloc(ndof,sizeof(double));
	T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	pos_q1_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	T_transient2=(double *)calloc(nbsteps*ndof,sizeof(double));
	derivee_T_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	adjoint_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	prod_KU_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	second_membre_transient=(double *)calloc(nbsteps*ndof,sizeof(double));
	derivee_T=(double *)calloc(nbsteps*ndof,sizeof(double));
	q1=(int *)calloc(nbsteps,sizeof(int));
	num_elem=(int *)calloc(nbsteps,sizeof(int));
	q2=(double *)calloc(nbsteps,sizeof(double));
	q3=(double *)calloc(nbsteps,sizeof(double));
	q4=(double *)calloc(nbsteps,sizeof(double));
	pos_q1=(double *)calloc(ndof,sizeof(double));
	pos_q2=(double *)calloc(ndof,sizeof(double));
	pos_q3=(double *)calloc(ndof,sizeof(double));
	pos_q4=(double *)calloc(ndof,sizeof(double));
	lu=(double *)calloc(ndof,sizeof(double));
	compliance=(double *)calloc(nbsteps,sizeof(double));
	gradient=(double *)calloc(ni*nj,sizeof(double));

	int test;
	FILE *domain;
		domain=fopen("domain2_triangle2.txt","r");
	double somme_domain;

	double volfrac2;
	cpt=0;
	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				i0=I2D(ni,i,j);
				df2[i0]=0;
				fscanf(domain,"%d\n",&test);
				domain_optim[i0]=test;
							if(test==0) cpt=cpt+1;
				somme_domain=somme_domain+(1-test);
			//	printf("domain optim=%d\n",domain_optim[i0]);
				//if(j>20 && i<150 && i>50) domain_optim[i0]=0;
			}
		}
		volfrac2=(double)(ni*nj*volfrac-cpt)/(ni*nj-cpt);
printf("volfrac_shape=%f\n",somme_domain/(ni*nj));
		//for (i=0; i<ni*nj; i++) fprintf(domain,"%d\n",domain_optim[i]);
fclose(domain);
int nele1=cpt;
//getchar();
printf("nele1=%d\n",nele1);

PetscInt *nodes_domainT;
PetscInt size_domainT;

const PetscInt *nindices1;

PetscMalloc1(4*nele1,&nodes_domainT);
IS is1;
k=0;
for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {

i0=I2D(ni,i,j);
	if(domain_optim[i0]==0){
edofMat[0]=i+(ni+1)*j;
edofMat[3]=i+(ni+1)*(j+1);
edofMat[2]=i+1+(ni+1)*(j+1);
edofMat[1]=i+1+(ni+1)*j;
nodes_domainT[k]=edofMat[0];
nodes_domainT[k+1]=edofMat[3];
nodes_domainT[k+2]=edofMat[2];
nodes_domainT[k+3]=edofMat[1];

k=k+4;

}
//printf("k=%d\n",k);
}
}



ierr=ISCreateGeneral(PETSC_COMM_SELF,4*nele1,nodes_domainT,PETSC_COPY_VALUES,&is1);CHKERRQ(ierr);

//ISView(is1,PETSC_VIEWER_STDOUT_SELF);

ISSortRemoveDups(is1);
ierr=ISGetSize(is1,&size_domainT);CHKERRQ(ierr);
//size_domainT=2;
printf("size_domain_T=%d\n",size_domainT);
ISGetIndices(is1,&nindices1);
//for (i=0;i<size_domainT;i++) printf("nindices1[%d]=%d\n",i,nindices1[i]);
//
//getchar();

KM_matrix_2D(delta_x,delta_y,KE,ME, HE1, HE2, HE3, HE4, FE1, FE2, FE3, FE4);

	FILE *init;
 //init=fopen("init2.txt","r");
 float *test1;

	for (i=0; i<ni; i++) {
			for (j=0; j<nj; j++) {
				i0=I2D(ni,i,j);
				//fscanf(init,"%f",&test);
				if(domain_optim[i0]==1){
				gamma_h[i0]=volfrac;
			//	gamma_x[i0]=volfrac;
				xmma[i0]=gamma_h[i0];
				xold[i0]=gamma_h[i0];
			}
			if(domain_optim[i0]==0){
				gamma_h[i0]=1;
				gamma_x[i0]=1;//volfrac;
				xmma[i0]=volfrac;//gamma_h[i0];
				xold[i0]=volfrac;//gamma_h[i0];
			}
			}
		}

		/*	for (i=0; i<ni; i++) {
					for (j=0; j<nj; j++) {
						i0=I2D(ni,i,j);
						fscanf(init,"%f",&test1);
						domain_optim[i0]=1;
						if(test1==1) domain_optim[i0]=0;
						gamma_h[i0]=test1;
						gamma_x[i0]=test1;
						xmma[i0]=gamma_h[i0];
						xold[i0]=gamma_h[i0];
					}
				}*/

	//Definition of boundary conditions
		for (j=0; j<nj; j++) {
	for (i=0; i<ni; i++) {

				i0=I2D(ni,i,j);

				if(i==0 || j==0 || i==(ni-1) || j==(nj-1)){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;

				if(j==(nj-1)) boundary[i0]=3;//3;
				if(i==0 ) boundary[i0]=4;//2;
				if(i==(ni-1)) boundary[i0]=2;//4; }
				if(j==0) boundary[i0]=1;

			}

			}
		}


		/*FILE *fichier;
		fichier=fopen("boundary.txt","w");
		for (i=0;i<ndof;i++) fprintf(fichier,"%d\n",boundary[i]);
		fclose(fichier);*/

	double temps;
	double dt1;
	dt1=1.f/delta_t;
	temps=0;

	for (i=0;i<nbsteps;i++){
		temps=i/dt1;
		j=0;
		while(j<(nbsteps*delta_t)){
			if(temps>j && temps <=j+1-0.5) q1[i]=30+j;
			j++;
		}
		/*if(temps<=1) q1[i]=30;
		if(temps >1 && temps <=2) q1[i]=30;
		if(temps >2 && temps <=3) q1[i]=30;
		if(temps >3 && temps <=4) q1[i]=35;
		if(temps >4 && temps <=5) q1[i]=40;
		if(temps >5 && temps <=6) q1[i]=45;
		if(temps >6 && temps <=7) q1[i]=50;
		if(temps >7 && temps <=8) q1[i]=55;
		if(temps >8 && temps <=9) q1[i]=60;
		if(temps >9 && temps <=10) q1[i]=65;
		if(temps >10 && temps <=11) q1[i]=70;
		if(temps >11 && temps <=12) q1[i]=75;
		if(temps >12 && temps <=13) q1[i]=80;
		if(temps >13 && temps <=14) q1[i]=85;
		if(temps >14 && temps <=15) q1[i]=90;
		if(temps >15 && temps <=16) q1[i]=95;
		if(temps >16 && temps <=17) q1[i]=100;
		if(temps >17 && temps <=18) q1[i]=105;
		if(temps >18 && temps <=19) q1[i]=110;
		if(temps >19 && temps <=20) q1[i]=115;
		if(temps >20 && temps <=21) q1[i]=120;
		if(temps >21 && temps <=22) q1[i]=125;
		if(temps >22 && temps <=23) q1[i]=130;
		if(temps >23 && temps <=24) q1[i]=135;
		if(temps >24 && temps <=25) q1[i]=140;
		if(temps >25 && temps <=26) q1[i]=145;
		if(temps >26 && temps <=27) q1[i]=150;
		if(temps >27 && temps <=28) q1[i]=155;
		if(temps >28 && temps <=29) q1[i]=160;
		if(temps >29 && temps <=30) q1[i]=165;
		if(temps >30 && temps <=31) q1[i]=170;
		if(temps >31 && temps <=32) q1[i]=175;
		if(temps >32 && temps <=33) q1[i]=180;
		if(temps >33 && temps <=34) q1[i]=185;*/




	/*	q1[i]=0.3*sin(PI*temps*(temps<=1));
		q2[i]=0.3*sin(PI*temps*(temps<=1));
		q3[i]=0.3*sin(PI*temps*(temps<=1))+0.3*sin(PI*temps*(temps>1));
		q4[i]=0.3*sin(PI*temps*(temps<=1))+0.3*sin(PI*temps*(temps>1));*/
	//	temps=temps+delta_t;
	}

	for (i=0;i<ndof;i++){
		Told[i]=T0;
	}

  nnz=(int *)calloc(ndof,sizeof(int));
	nnz2=(int *)calloc(ndof,sizeof(int));


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Create vectors.  Note that we form 1 vector from scratch and
     then duplicate as needed.
  */

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&D);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&Tn);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F2);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&T);CHKERRQ(ierr);

	ierr = VecSet(D,0);CHKERRQ(ierr);
	ierr = VecSet(Tn,0);CHKERRQ(ierr);
	ierr = VecSet(F1,0);CHKERRQ(ierr);
	ierr = VecSet(F2,0);CHKERRQ(ierr);
	ierr = VecSet(T,0);CHKERRQ(ierr);

	double ratio;

	nz=ndof;
	nz2=ndof;

	for(i=0;i<ndof;i++){
		nnz[i]=0.001*ndof;
		nnz2[i]=0.001*ndof;
	}

					/* printf("Definition solide \n");

					for (i=0; i<ni; i++) {
					for (j=0+q1[compteur]; j<5+q1[compteur]; j++) {
						 i0=I2D(ni,i,j);
						 if(domain_optim[i0]==0) gamma_h[i0]=1;

					}
					}*/


	for (k2_it=0;k2_it<opt_it_max;k2_it++){

		MatCreate(PETSC_COMM_WORLD,&K1);
		MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
		MatSetFromOptions(K1);
		MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&K1);
		MatSetOption(K1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

		MatCreate(PETSC_COMM_WORLD,&K2);
		MatSetSizes(K2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
		MatSetFromOptions(K2);
		MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&K2);
		MatSetOption(K2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&M1);
			MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(M1);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&M1);
			MatSetOption(M1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&M2);
			MatSetSizes(M2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(M2);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&M2);
			MatSetOption(M2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);


			MatCreate(PETSC_COMM_WORLD,&U1);
			MatSetSizes(U1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(U1);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&U1);
			MatSetOption(U1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

			MatCreate(PETSC_COMM_WORLD,&U2);
			MatSetSizes(U2,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);
			MatSetFromOptions(U2);
			MatCreateSeqAIJ(PETSC_COMM_SELF,ndof,ndof,nz,nnz,&U2);
			MatSetOption(U2,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

		for (j=0; j<nj; j++) {
		for (i=0; i<ni; i++) {

					i0=I2D(ni,i,j);

					if(domain_optim[i0]==0 && j>21){
						lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;
					}
					if(domain_optim[i0]==0 && j<=21){
						lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;
					}
					if(domain_optim[i0]==1){
					lambda[i0]=lambda_vide+PetscPowScalar(gamma_h[i0],penal)*(lambda_solide-lambda_vide);
					rho[i0]=rho_vide+PetscPowScalar(gamma_h[i0],penal)*(rho_solide-rho_vide);
					cp[i0]=cp_vide+PetscPowScalar(gamma_h[i0],penal)*(cp_solide-cp_vide);
					diff[i0]=lambda[i0]/(rho[i0]*cp[i0]);
					c[i0]=c_vide+PetscPowScalar(gamma_h[i0],penal)*(c_solide-c_vide);
				}
				}
			}

		// Loop over elements
					for (j=0; j<nj; j++) {
		for (i=0; i<ni; i++) {

					i0=I2D(ni,i,j);
				//	if(domain_optim[i0]==0){lambda[i0]=lambda_vide; rho[i0]=rho_vide;cp[i0]=cp_vide;}
					edofMat[0]=i+(ni+1)*j;
					edofMat[3]=i+(ni+1)*(j+1);
					edofMat[2]=i+1+(ni+1)*(j+1);
					edofMat[1]=i+1+(ni+1)*j;

					for (k=0;k<4*4;k++){
					ke[k]=KE[k]*lambda[i0];
					me[k]=ME[k]*(double)rho[i0]*cp[i0]/delta_t;
							}
				// Add values to the sparse matrix
				ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
				ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
				ierr = MatSetValues(K2,4,edofMat,4,edofMat,ke,ADD_VALUES);
				ierr = MatSetValues(M2,4,edofMat,4,edofMat,me,ADD_VALUES);
			//	}

				}
			}

			ierr=MatAssemblyBegin(K2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(K2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyBegin(M2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(M2, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


			// Loop over elements
		for (j=0; j<nj; j++) {
			for (i=0; i<ni; i++) {

						i0=I2D(ni,i,j);

					/*	if(boundary[i0]==2){
						edofMat[0]=i+(ni+1)*j;
						edofMat[3]=i+(ni+1)*(j+1);
						edofMat[2]=i+1+(ni+1)*(j+1);
						edofMat[1]=i+1+(ni+1)*j;

						for (k=0;k<4*4;k++){
						ke[k]=HE2[k]*hconv;
						//me[k]=ME[k]*0*rho[i0]*cp[i0]*1e-8/delta_t;
								}
					// Add values to the sparse matrix
					ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
					//ierr = MatSetValues(M,4,edofMat,4,edofMat,me,ADD_VALUES);
				//	}
			}

						if(boundary[i0]==3){
						edofMat[0]=i+(ni+1)*j;
						edofMat[3]=i+(ni+1)*(j+1);
						edofMat[2]=i+1+(ni+1)*(j+1);
						edofMat[1]=i+1+(ni+1)*j;

						for (k=0;k<4*4;k++){
						ke[k]=HE3[k]*hconv;
						//me[k]=ME[k]*0*rho[i0]*cp[i0]*1e-8/delta_t;
								}
					// Add values to the sparse matrix
					ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
					//ierr = MatSetValues(M,4,edofMat,4,edofMat,me,ADD_VALUES);
				//	}
			}

			if(boundary[i0]==4){
			edofMat[0]=i+(ni+1)*j;
			edofMat[3]=i+(ni+1)*(j+1);
			edofMat[2]=i+1+(ni+1)*(j+1);
			edofMat[1]=i+1+(ni+1)*j;

			for (k=0;k<4*4;k++){
			ke[k]=HE4[k]*hconv;
			//me[k]=ME[k]*0*rho[i0]*cp[i0]*1e-8/delta_t;
					}
					// Add values to the sparse matrix
			ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
			//ierr = MatSetValues(M,4,edofMat,4,edofMat,me,ADD_VALUES);
			//	}
		}*/

				}
			}
			ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
			ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

			 for (j=0; j<nj; j++) {
			for (i=0; i<ni; i++) {

				 i0=I2D(ni,i,j);

					 if(boundary[i0]==1){
					 value=1;
					 edofMat[0]=i+(ni+1)*j;
					 edofMat[3]=i+(ni+1)*(j+1);
					 edofMat[2]=i+1+(ni+1)*(j+1);
					 edofMat[1]=i+1+(ni+1)*j;
					 ierr = MatZeroRows(K1,1,&edofMat[0],1,0,0);
					 ierr = MatZeroRows(K1,1,&edofMat[1],1,0,0);
					 ierr = MatZeroRows(M1,1,&edofMat[0],1,0,0);
					 ierr = MatZeroRows(M1,1,&edofMat[1],1,0,0);
					 ierr = MatZeroRows(K2,1,&edofMat[0],1,0,0);
					 ierr = MatZeroRows(K2,1,&edofMat[1],1,0,0);
					 ierr = MatZeroRows(M2,1,&edofMat[0],1,0,0);
					 ierr = MatZeroRows(M2,1,&edofMat[1],1,0,0);
				 }

				 /*if(boundary[i0]==2){
				 value=1;
				 edofMat[0]=i+(ni+1)*j;
				 edofMat[3]=i+(ni+1)*(j+1);
				 edofMat[2]=i+1+(ni+1)*(j+1);
				 edofMat[1]=i+1+(ni+1)*j;
				 ierr = MatZeroRows(K1,1,&edofMat[2],1,0,0);
				 ierr = MatZeroRows(K1,1,&edofMat[1],1,0,0);
				 ierr = MatZeroRows(M1,1,&edofMat[2],1,0,0);
				 ierr = MatZeroRows(M1,1,&edofMat[1],1,0,0);
			 }

			 if(boundary[i0]==3){
			 value=1;
			 edofMat[0]=i+(ni+1)*j;
			 edofMat[3]=i+(ni+1)*(j+1);
			 edofMat[2]=i+1+(ni+1)*(j+1);
			 edofMat[1]=i+1+(ni+1)*j;
			 ierr = MatZeroRows(K1,1,&edofMat[2],1,0,0);
			 ierr = MatZeroRows(K1,1,&edofMat[3],1,0,0);
			 ierr = MatZeroRows(M1,1,&edofMat[2],1,0,0);
			 ierr = MatZeroRows(M1,1,&edofMat[3],1,0,0);
		 }

		 if(boundary[i0]==4){
		 value=1;
		 edofMat[0]=i+(ni+1)*j;
		 edofMat[3]=i+(ni+1)*(j+1);
		 edofMat[2]=i+1+(ni+1)*(j+1);
		 edofMat[1]=i+1+(ni+1)*j;
		 ierr = MatZeroRows(K1,1,&edofMat[0],1,0,0);
		 ierr = MatZeroRows(K1,1,&edofMat[3],1,0,0);
		 ierr = MatZeroRows(M1,1,&edofMat[0],1,0,0);
		 ierr = MatZeroRows(M1,1,&edofMat[3],1,0,0);
	 }*/

			 }
			}

			//Addition de M et K
			ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
			ierr=MatDuplicate(M2,MAT_COPY_VALUES,&U2);CHKERRQ(ierr);
			ierr=MatAXPY(U1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
			ierr=MatAXPY(U2,1,K2,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

		//Creation of K in KT=F


		/*
		PetscInt row[441];
		PetscInt col[441];

		 for (i=0;i<ndof;i++){
			 row[i]=i;
			 col[i]=i;
		 }

		 MatGetValues(K,ndof,row,ndof,col,v);

		 FILE *matrice;
		 matrice=fopen("matrice_direct.txt","w");

			 for (i=0;i<ndof*ndof;i++){
				 fprintf(matrice,"%f\n",v[i]);
			 }

			 fclose(matrice);*/



	printf("OPTIMIZATION PROBLEM - BEGINNING \n ");

int compteur=0;
FILE *fichier_compliance;
fichier_compliance=fopen("compliance_T.txt","w");
int jc;
jc=0;
double lambda1;

	while(compteur<nbsteps){


/////////// Load //////////////


		/*for (j=0; j<(nj+1); j++) {
			for (i=0; i<(ni+1); i++) {
			i0=I2D(ni2,i,j);
			if (i==0.25*ni && j==0.5*nj) pos_q1[i0]=1;
			if (i==0.75*ni && j==0.5*nj) pos_q2[i0]=1;
			if (i==0.5*ni && j==0.25*nj) pos_q3[i0]=1;
			if (i==0.5*ni && j==0.75*nj) pos_q4[i0]=1;

		}
	}*/

	for (i=0; i<(ni+1); i++) {
			for (j=0; j<(nj+1); j++) {
				i0=I2D(ni2,i,j);
		pos_q1[i0]=0;

			}
		}

cpt=0;
			for (j=q1[compteur]; j<q1[compteur]+1; j++) {
	for (i=0; i<ni; i++) {
		if(q1[compteur]>0){
				/*lambda1=0.1;
				if(j<(5+q1[compteur])) lambda1=1;
				if(j>(10+q1[compteur])) lambda1=0.01;*/

			i0=I2D(ni,i,j);
			if(domain_optim[i0]==0){
			edofMat[0]=i+(ni+1)*j;
			edofMat[3]=i+(ni+1)*(j+1);
			edofMat[2]=i+1+(ni+1)*(j+1);
			edofMat[1]=i+1+(ni+1)*j;

			pos_q1[edofMat[0]]=1;
			pos_q1[edofMat[3]]=1;
			pos_q1[edofMat[2]]=1;
			pos_q1[edofMat[1]]=1;
			cpt=cpt+1;
		/*	for (k=0;k<4*4;k++){
			ke[k]=KE[k]*lambda_solide;
			me[k]=ME[k]*c_solide*delta_x*delta_y/delta_t;
							}
		// Add values to the sparse matrix
		ierr = MatSetValues(K1,4,edofMat,4,edofMat,ke,ADD_VALUES);
		ierr = MatSetValues(K1,4,edofMat,4,edofMat,me,ADD_VALUES);*/

		}
	}
			}
	}
	num_elem[compteur]=cpt;
	printf("num_elem=%d\n",num_elem[compteur]);

/*	ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);*/

	//ierr=MatAXPY(M1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

	//Prod mat vecteur
	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);


printf("Load \n");
	for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {

		i0=I2D(ni,i,j);
		edofMat[0]=i+(ni+1)*j;
		edofMat[3]=i+(ni+1)*(j+1);
		edofMat[2]=i+1+(ni+1)*(j+1);
		edofMat[1]=i+1+(ni+1)*j;

		if(num_elem[compteur]>0){
			F[edofMat[0]]=pos_q1[edofMat[0]]*0.75e4;///(num_elem[compteur]);//
			F[edofMat[1]]=pos_q1[edofMat[1]]*0.75e4;///(num_elem[compteur]);//
			F[edofMat[2]]=pos_q1[edofMat[2]]*0.75e4;///(num_elem[compteur]);//
			F[edofMat[3]]=pos_q1[edofMat[3]]*0.75e4;//(num_elem[compteur]);//
		}
		else{
			F[edofMat[0]]=0;//
			F[edofMat[1]]=0;//
			F[edofMat[2]]=0;//
			F[edofMat[3]]=0;//
		}
	/*	F[edofMat[0]]=(double)1;//
		F[edofMat[1]]=(double)1;//
		F[edofMat[2]]=(double)1;//
		F[edofMat[3]]=(double)1;//*/

		/*F[edofMat[0]]=(pos_q1[edofMat[0]]*q2[compteur]+pos_q2[edofMat[0]]*q2[compteur]+pos_q3[edofMat[0]]*q3[compteur]+pos_q4[edofMat[0]]*q4[compteur]);///(rho[i0]*cp[i0]);
		F[edofMat[1]]=(pos_q1[edofMat[1]]*q2[compteur]+pos_q2[edofMat[1]]*q2[compteur]+pos_q3[edofMat[1]]*q3[compteur]+pos_q4[edofMat[1]]*q4[compteur]);///(rho[i0]*cp[i0]);
		F[edofMat[2]]=(pos_q1[edofMat[2]]*q2[compteur]+pos_q2[edofMat[2]]*q2[compteur]+pos_q3[edofMat[2]]*q3[compteur]+pos_q4[edofMat[2]]*q4[compteur]);///(rho[i0]*cp[i0]);
		F[edofMat[3]]=(pos_q1[edofMat[3]]*q2[compteur]+pos_q2[edofMat[3]]*q2[compteur]+pos_q3[edofMat[3]]*q3[compteur]+pos_q4[edofMat[3]]*q4[compteur]);///(rho[i0]*cp[i0]);*/

}
}

printf("Complete load vector \n");

//Normal node
for (i=0; i<ndof; i++) {
	value=F[i];
	ierr = VecSetValues(b,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);

}

VecAXPY(b,1,F1);

//VecView(b,PETSC_VIEWER_STDOUT_SELF);
//getchar();

double conv[4];
//Dirichlet condition
	for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {

		i0=I2D(ni,i,j);

			/*if(boundary[i0]==4){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE4[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);

			}

			if(boundary[i0]==3){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE3[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);
			}

			if(boundary[i0]==2){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE2[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);
			*/



			if(boundary[i0]==1){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

		/*	if(boundary[i0]==2){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

			if(boundary[i0]==3){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

			if(boundary[i0]==4){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}*/



	}
}


 //Set exact solution; then compute right-hand-side vector.
printf("Solver beginning \n");
//ierr = VecSet(x,0);CHKERRQ(ierr);
ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);
ierr = solver.setA(U1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
int         iters;
solver.finalize();

for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	Told[i]=y;
}


cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
T_transient[i]=x1[cpt];
pos_q1_transient[i]=pos_q1[cpt];
cpt=cpt+1;
}

somme=0;
//Fonction cout
/*	for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {
		i0=I2D(ni,i,j);
		edofMat[0]=i+(ni+1)*j;
		edofMat[3]=i+(ni+1)*(j+1);
		edofMat[2]=i+1+(ni+1)*(j+1);
		edofMat[1]=i+1+(ni+1)*j;*/

/*PetscScalar uKu=0.0;
for (PetscInt k=0;k<4;k++){
for (PetscInt h=0;h<4;h++){
	uKu += x1[edofMat[k]]*KE[k*4+h]*x1[edofMat[h]];
}
*/
//if(compteur==20 && i==5 && j==5) printf("uKu avant=%lg\n",uKu);
/*somme=somme+lambda[i0]*uKu;
//gradient[i0]=-penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu;
}
}*/

for (i=0;i<size_domainT;i++) somme=somme+x1[nindices1[i]];
compliance[compteur]=somme/size_domainT;
printf("size_domainT=%d\n",size_domainT);
printf("compliance=%f\n",compliance[compteur]);
//getchar();
fprintf(fichier_compliance,"%f\n",somme);
//printf("fonction cout=%f\n",fonction_cout[k2_it]);

compteur=compteur+1;
printf("compteur=%d\n",compteur);
} //Fin boucle transient


fclose(fichier_compliance);


//Fonction cout

somme=0;
somme2=0;
for (i=0;i<nbsteps;i++) {
//	somme=somme+compliance[i]*pow(alpha,compliance[i]);
//	somme2=somme2+pow(alpha,compliance[i]);
somme=somme+compliance[i];
}

fonction_cout[k2_it]=somme/nbsteps;
printf("fonction cout=%f\n",fonction_cout[k2_it]);

//Probleme ADJOINT
//ierr = MatDestroy(&K);CHKERRQ(ierr);

double C10,C11,C12,*ksi;
ksi=(double *)calloc(nbsteps,sizeof(double));

somme1=0;
somme2=0;
for (i=0;i<nbsteps;i++){
	somme1=somme1+pow(alpha,compliance[i]);
	somme2=somme2+compliance[i]*pow(alpha,compliance[i]);
}
C10=1.f/(somme1*somme1);
C11=somme1;
C12=somme2;

for (i=0;i<nbsteps;i++){
ksi[i]=C10*(C11*(1+compliance[i]*log(alpha))-C12*log(alpha))*pow(alpha,compliance[i]);
printf("compliance[%d]=%f and ksi[%d]=%f\n",i,compliance[i],i,ksi[i]);
}
//getchar();
ierr = VecSet(x,0);CHKERRQ(ierr);
compteur=0;
while(compteur<nbsteps){
ierr = VecSet(b,0);CHKERRQ(ierr);
/*for(i=0;i<ndof;i++){
	value=T_transient[(nbsteps-1-compteur)*ndof+i];
	ierr=VecSetValues(T,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);
}

ierr=MatMult(K2,T,b);CHKERRQ(ierr);
ierr=VecScale(b,-2*ksi[nbsteps-1-compteur]); CHKERRQ(ierr);

ierr=MatMult(M2,x,F2);CHKERRQ(ierr);

ierr=VecAXPY(b,1,F2);CHKERRQ(ierr);*/

ierr=MatMult(M2,x,F2);CHKERRQ(ierr);


for (i=0;i<size_domainT;i++){
	value=-(double)T_transient[(nbsteps-1-compteur)*ndof+nindices1[i]]/size_domainT;
	ierr = VecSetValues(b,1,&nindices1[i],&value,INSERT_VALUES);CHKERRQ(ierr);
}

FILE *fichier_b;
strcpy(filename,"KM_b_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_b=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++){
	VecGetValues(b,1,&i,&y);
	fprintf(fichier_b,"%lg\n",y);
}
fclose(fichier_b);

ierr=VecAXPY(b,1,F2);CHKERRQ(ierr);

//Dirichlet condition
	for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {

		i0=I2D(ni,i,j);

			/*if(boundary[i0]==4){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE4[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);

			}

			if(boundary[i0]==3){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE3[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);
			}

			if(boundary[i0]==2){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;

				for (k=0;k<4;k++) fe[k]=FE2[k]*hconv*Tair;

				VecSetValues(b,4,edofMat,fe,ADD_VALUES); CHKERRQ(ierr);
			*/

			if(boundary[i0]==1){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

		/*	if(boundary[i0]==2){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

			if(boundary[i0]==3){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}

			if(boundary[i0]==4){
				edofMat[0]=i+(ni+1)*j;
				edofMat[3]=i+(ni+1)*(j+1);
				edofMat[2]=i+1+(ni+1)*(j+1);
				edofMat[1]=i+1+(ni+1)*j;
				ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[2],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
				//ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
			}*/



	}
}


//ierr=VecScale(b,1); CHKERRQ(ierr);

printf("Solver beginning \n");
//ierr = VecSet(x,0);CHKERRQ(ierr);
ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);
ierr = solver.setA(U2); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
int         iters;
solver.finalize();

for (i=0; i<ndof; i++) {
	VecGetValues(x,1,&i,&y);
	x1[i]=y;
	}

cpt=0;
for (i=0+compteur*ndof; i<(ndof*(compteur+1)); i++) {
adjoint_transient[i]=x1[cpt];
cpt=cpt+1;
}

	compteur=compteur+1;
	printf("compteur=%d\n",compteur);
	} //Fin boucle transient


//printf("nbsteps=%d\n",nbsteps);
	for(j=0;j<nbsteps;j++){
for(i=0;i<ndof;i++){

		//T_transient2[j*ndof+i]=ksi[j]*T_transient[j*ndof+i]+adjoint_transient[(nbsteps-1-j)*ndof+i];
		T_transient2[j*ndof+i]=adjoint_transient[(nbsteps-1-j)*ndof+i];
		//if(i==5+(101*5)) printf("T_transient2=%lg\n",T_transient2[j*ndof+i]);
		if(j==0) derivee_T_transient[j*ndof+i]=0;
		if(j>0) derivee_T_transient[j*ndof+i]=(T_transient[j*ndof+i]-T_transient[(j-1)*ndof+i]);///delta_t;
		//printf("j=%d\n",j);
	}
}

FILE *somme_gradient,*somme_uKu1,*somme_uKu2;
somme_gradient=fopen("somme_gradient.txt","w");
somme_uKu1=fopen("uKu1.txt","w");
somme_uKu2=fopen("uKu2.txt","w");
PetscScalar uKu1;
PetscScalar uKu2;

	//gradient
	printf("gradient\n");
			for (j=0; j<nj; j++) {
			for (i=0; i<ni; i++) {
			i0=I2D(ni,i,j);
			if(domain_optim[i0]==1){
			edofMat[0]=i+(ni+1)*j;
			edofMat[3]=i+(ni+1)*(j+1);
			edofMat[2]=i+1+(ni+1)*(j+1);
			edofMat[1]=i+1+(ni+1)*j;
			compteur=0;
			somme=0;
			while(compteur<nbsteps){
				uKu1=0.0;
				uKu2=0.0;
	for (PetscInt k=0;k<4;k++){
	for (PetscInt h=0;h<4;h++){
		uKu1 += T_transient2[compteur*ndof+edofMat[k]]*KE[k*4+h]*T_transient[compteur*ndof+edofMat[h]];
		uKu2 += adjoint_transient[(nbsteps-1-compteur)*ndof+edofMat[k]]*ME[k*4+h]*derivee_T_transient[compteur*ndof+edofMat[h]];

	}
	}

		//if(i==190 && j==50) printf("somme=%f\n",somme);
	somme=somme+penal*(lambda_solide-lambda_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu1	+penal*(c_solide-c_vide)*PetscPowScalar(gamma_h[i0],(penal-1))*uKu2;
	if(i==190 && j==50) printf("compteur=%d, uKu1=%f, somme=%f \n",compteur,uKu1,somme);

	compteur=compteur+1;
}
	gradient[i0]=-somme;


}
	//if(i==190 && j==50) printf("Final uKu1=%f\n",uKu1);
	}
}


//getchar();


fclose(somme_gradient);
fclose(somme_uKu1);
fclose(somme_uKu2);


printf("Ecriture des fichiers transients \n");

/*FILE *fichier_adjoint;
fichier_adjoint=fopen("KM_adjoint_transient.txt","w");
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_adjoint,"%f\n",adjoint_transient[i]);
fclose(fichier_adjoint);*/
FILE *fichier_second_membre;

if(k2_it==0 || k2_it==(opt_it_max-1)){

strcpy(filename,"KM_transient_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_second_membre=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_second_membre,"%f\n",T_transient[i]);
fclose(fichier_second_membre);
}

/*
fichier_second_membre=fopen("KM_pos_q1_transient.txt","w");
for (i=0;i<nbsteps*ndof;i++) fprintf(fichier_second_membre,"%f\n",pos_q1_transient[i]);
fclose(fichier_second_membre);*/

//Calcul du gradient
printf("derivees \n");

FILE *fichier_T;
strcpy(filename,"KM_T_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_T=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) fprintf(fichier_T,"%lg\n",T_transient[(int)(0.5*(nbsteps-1))*ndof+i]);
fclose(fichier_T);



strcpy(filename,"KM_adjoint_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_T=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) fprintf(fichier_T,"%lg\n",adjoint_transient[(int)(0.5*(nbsteps-1))*ndof+i]);
fclose(fichier_T);

// Set outer move limits
printf("MMA limits \n");

for (i=0;i<ni*nj;i++) {
	xmax[i] = Min(Xmax, xmma[i] + movlim); //printf("xmax=%f\n", xmax[i]);
	xmin[i] = Max(Xmin, xmma[i] - movlim); //printf("xmin=%f\n", xmin[i]);
}

somme=0;
for (i=0; i<ni*nj; i++) {
	df[i]=-gradient[i];
	dg[i]=1;
	somme=somme+gamma_h[i];
}

g[0]=volfrac-somme/(ni*nj);
printf("g0=%f\n",g[0]);

printf("Spatial filter \n");
filtre(ni,nj,rmin,gamma_h,df,df2,domain_optim);

printf("MMA update \n");
//OC(ni, nj, df, gamma_h, movlim, volfrac,domain_optim);
mma->Update(xmma,df2,g,dg,xmin,xmax);


for (i=0;i<ni*nj;i++) {
	if(domain_optim[i]==1){
	xold[i] = xmma[i];
	gamma_h[i]=xmma[i];
}
}


filtre2(ni,nj,rmin,gamma_h,gamma_x,domain_optim);

for (i=0; i<ni*nj; i++) {
	gamma_h[i]=gamma_x[i];
}

FILE *fichier_gamma;

strcpy(filename,"KM_gamma_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_gamma=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ni*nj;i++) fprintf(fichier_gamma,"%f\n",gamma_h[i]);
fclose(fichier_gamma);

strcpy(filename,"h012_gradient_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_gamma=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ni*nj;i++) fprintf(fichier_gamma,"%lg\n",df2[i]);
fclose(fichier_gamma);

FILE *fichier_fonction_cout;
strcpy(filename,"h012_fonction_cout_");	sprintf(u2,"%d",k2_it);	strcat(filename,u2); 	fichier_fonction_cout=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
//fichier_fonction_cout=fopen("fonction_cout","w");

for (i=0;i<=k2_it;i++) fprintf(fichier_fonction_cout,"%f\n",fonction_cout[i]);

fclose(fichier_fonction_cout);


ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);
ierr = MatDestroy(&K2);CHKERRQ(ierr);
ierr = MatDestroy(&M2);CHKERRQ(ierr);
} //Fin boucle optim

printf("Fin boucle optim \n");


ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);
//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);
return ierr;
}
