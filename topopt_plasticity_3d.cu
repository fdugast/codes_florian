#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <omp.h>

#include <MMASolver.h>
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define MNE 24 //8 degrees of freedom per element : hexahedral 4 node element with two degrees of freedom per node

#define BLOCKSIZE1 32
#define BLOCKSIZE2 32

//#define MAX(x, y) (((x) > (y)) ? (x) : (y))

#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)

// get time structure
double get_time()
{
  struct timeval timeval_time;
  gettimeofday(&timeval_time,NULL);
  return (double)timeval_time.tv_sec + (double)timeval_time.tv_usec*1e-6;
}

// get RAM structure
struct rusage r_usage;

//Elastic contribution in the element stiffness matrix
void KE_elasticity(double delta_x, double delta_y, double delta_z, double *KE, double E, double nu, double *C){

	double wg[3][3],wxyz;
	int i,j,k,i1,j1,k3,cpt,l,i3;
	double x,y,z,a,b,c;
	double sommeK,somme_k,somme_l;

  double DN[3][8];
  double B[6][24];
  double gxyz[3][3];

  //1 point
  gxyz[0][0]=0;
  	wg[0][0]=2;

  //2 points
  gxyz[0][1]=-0.577350269189626;
  gxyz[1][1]=0.577350269189626;
  	wg[0][1]=1;
    wg[1][1]=1;

  //3pts
  gxyz[0][2]=-0.774596669241483;
  gxyz[1][2]=0;
  gxyz[2][2]=0.774596669241483;

	wg[0][2]=0.555555555555556;
	wg[1][2]=0.888888888888889;
	wg[2][2]=0.555555555555556;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;


	for (i1=0;i1<MNE;i1++){
			for (j1=0;j1<MNE;j1++){

sommeK=0;
cpt=0;
for (i=0;i<2;i++){
	for (j=0;j<2;j++){
    for (k=0;k<2;k++){


			//x=a1*gxyz[i][1];
			//y=b1*gxyz[j][1];
			//z=c1*gxyz[k][1];
			wxyz=wg[i][1]*wg[j][1]*wg[k][1];

      x=gxyz[i][1];
      y=gxyz[j][1];
      z=gxyz[k][1];


      DN[0][0]=-(1-y)*(1-z)*(1/a)/(2*2*2); DN[1][0]=-(1-x)*(1-z)*(1/b)/(2*2*2);  DN[2][0]=-(1-x)*(1-y)*(1/c)/(2*2*2);
      DN[0][1]=(1-y)*(1-z)*(1/a)/(2*2*2);  DN[1][1]=-(1+x)*(1-z)*(1/b)/(2*2*2);  DN[2][1]=-(1+x)*(1-y)*(1/c)/(2*2*2);
      DN[0][2]=(1+y)*(1-z)*(1/a)/(2*2*2);  DN[1][2]=(1+x)*(1-z)*(1/b)/(2*2*2);   DN[2][2]=-(1+x)*(1+y)*(1/c)/(2*2*2);
      DN[0][3]=-(1+y)*(1-z)*(1/a)/(2*2*2); DN[1][3]=(1-x)*(1-z)*(1/b)/(2*2*2);   DN[2][3]=-(1-x)*(1+y)*(1/c)/(2*2*2);
      DN[0][4]=-(1-y)*(1+z)*(1/a)/(2*2*2); DN[1][4]=-(1-x)*(1+z)*(1/b)/(2*2*2);  DN[2][4]=(1-x)*(1-y)*(1/c)/(2*2*2);
      DN[0][5]=(1-y)*(1+z)*(1/a)/(2*2*2);  DN[1][5]=-(1+x)*(1+z)*(1/b)/(2*2*2);  DN[2][5]=(1+x)*(1-y)*(1/c)/(2*2*2);
      DN[0][6]=(1+y)*(1+z)*(1/a)/(2*2*2);  DN[1][6]=(1+x)*(1+z)*(1/b)/(2*2*2);   DN[2][6]=(1+x)*(1+y)*(1/c)/(2*2*2);
      DN[0][7]=-(1+y)*(1+z)*(1/a)/(2*2*2); DN[1][7]=(1-x)*(1+z)*(1/b)/(2*2*2);   DN[2][7]=(1-x)*(1+y)*(1/c)/(2*2*2);


      for(i3=0;i3<8;i3++){
      B[0][0+3*i3]=DN[0][i3];   B[0][1+3*i3]=0; 				  B[0][2+3*i3]=0;
      B[1][0+3*i3]=0; 			  	B[1][1+3*i3]=DN[1][i3];   B[1][2+3*i3]=0;
      B[2][0+3*i3]=0; 				  B[2][1+3*i3]=0; 				  B[2][2+3*i3]=DN[2][i3];
      B[3][0+3*i3]=DN[1][i3];	  B[3][1+3*i3]=DN[0][i3];   B[3][2+3*i3]=0;
      B[4][0+3*i3]=0;					  B[4][1+3*i3]=DN[2][i3];   B[4][2+3*i3]=DN[1][i3];
      B[5][0+3*i3]=DN[2][i3];	  B[5][1+3*i3]=0; 				  B[5][2+3*i3]=DN[0][i3];
    }

										somme_k=0;
										for(k3=0;k3<6;k3++){
											somme_l=0;
											for(l=0;l<6;l++){
												//somme_l=somme_l+B[l][i1]*C[l+3*k3];
                        somme_l=somme_l+B[l][i1]*C[l+6*k3];
											}
											somme_k=somme_k+somme_l*B[k3][j1];
										}


								sommeK=sommeK+somme_k*wxyz*a*b*c;


								//printf("sommeK=%f\n",sommeK);
								cpt++;
							}
            }
					}
					KE[i1+MNE*j1]=sommeK;
				//	printf("%.2lg ",KE[i1+24*j1]);
}

//printf("\n");

}

}



void KE_plasticity3(int qpoints, double delta_x, double delta_y, double delta_z, int numElems_red,  double *D,
    double *BE1, double *BE2, double *BE3, double *BE4,
    double *BE5, double *BE6, double *BE7, double *BE8,  double *KE_full)
  {

	int e,m,i1,j1,k3,l;
	double a,b,c;
	double sommeK;

  double somme_l1,somme_l2,somme_l3,somme_l4;
  double somme_l5,somme_l6,somme_l7,somme_l8;
  double somme_k1,somme_k2,somme_k3,somme_k4;
  double somme_k5,somme_k6,somme_k7,somme_k8;

  double KE[MNE*MNE];

  a=0.5*delta_x;
  b=0.5*delta_y;
  c=0.5*delta_z;

  double wg[3][3],wxyz1,wxyz2,wxyz3,wxyz4,wxyz5,wxyz6,wxyz7,wxyz8;

  wg[0][1]=1;
  wg[1][1]=1;

  wxyz1=wg[0][1]*wg[0][1]*wg[0][1];
  wxyz2=wg[1][1]*wg[0][1]*wg[0][1];
  wxyz3=wg[1][1]*wg[1][1]*wg[0][1];
  wxyz4=wg[0][1]*wg[1][1]*wg[0][1];
  wxyz5=wg[0][1]*wg[0][1]*wg[1][1];
  wxyz6=wg[1][1]*wg[0][1]*wg[1][1];
  wxyz7=wg[1][1]*wg[1][1]*wg[1][1];
  wxyz8=wg[0][1]*wg[1][1]*wg[1][1];


  int cpt1=0;

for (e=0;e<numElems_red;e++){

  for(m=0;m<MNE*MNE;m++) KE[m]=0;


	for (i1=0;i1<MNE;i1++){
			for (j1=0;j1<MNE;j1++){

        somme_k1=0;   somme_k2=0;   somme_k3=0;   somme_k4=0;
        somme_k5=0;   somme_k6=0;   somme_k7=0;   somme_k8=0;

  			for(k3=0;k3<6;k3++){

        somme_l1=0;   somme_l2=0;   somme_l3=0;   somme_l4=0;
        somme_l5=0;   somme_l6=0;   somme_l7=0;   somme_l8=0;


				for(l=0;l<6;l++){
					somme_l1=somme_l1+BE1[3*qpoints*l+i1]*D[qpoints*36*e+36*0+l+6*k3];
          somme_l2=somme_l2+BE2[3*qpoints*l+i1]*D[qpoints*36*e+36*1+l+6*k3];
          somme_l3=somme_l3+BE3[3*qpoints*l+i1]*D[qpoints*36*e+36*2+l+6*k3];
          somme_l4=somme_l4+BE4[3*qpoints*l+i1]*D[qpoints*36*e+36*3+l+6*k3];
          somme_l5=somme_l5+BE5[3*qpoints*l+i1]*D[qpoints*36*e+36*4+l+6*k3];
          somme_l6=somme_l6+BE6[3*qpoints*l+i1]*D[qpoints*36*e+36*5+l+6*k3];
          somme_l7=somme_l7+BE7[3*qpoints*l+i1]*D[qpoints*36*e+36*6+l+6*k3];
          somme_l8=somme_l8+BE8[3*qpoints*l+i1]*D[qpoints*36*e+36*7+l+6*k3];
          								}

					somme_k1=somme_k1+somme_l1*BE1[3*qpoints*k3+j1];
          somme_k2=somme_k2+somme_l2*BE2[3*qpoints*k3+j1];
          somme_k3=somme_k3+somme_l3*BE3[3*qpoints*k3+j1];
          somme_k4=somme_k4+somme_l4*BE4[3*qpoints*k3+j1];
          somme_k5=somme_k5+somme_l5*BE5[3*qpoints*k3+j1];
          somme_k6=somme_k6+somme_l6*BE6[3*qpoints*k3+j1];
          somme_k7=somme_k7+somme_l7*BE7[3*qpoints*k3+j1];
          somme_k8=somme_k8+somme_l8*BE8[3*qpoints*k3+j1];
										}

					sommeK=a*b*c*(somme_k1*wxyz1+somme_k2*wxyz2+somme_k3*wxyz3+somme_k4*wxyz4+
          somme_k5*wxyz5+somme_k6*wxyz6+somme_k7*wxyz7+somme_k8*wxyz8);

					KE[i1+MNE*j1]=sommeK;

}

}

for (m=0;m<MNE*MNE;m++) {
  KE_full[MNE*MNE*cpt1+m]=KE[m];
}

/*if(e==0 || e==49){
  printf("e=%d\n",e);
  for (i1=0;i1<MNE;i1++){
      for (j1=0;j1<MNE;j1++){
        printf("%lg ",KE[MNE*i1+j1]);
      }
      printf("\n");
    }
}*/

cpt1++;

}
}

//Strain displacement matrix on the different quadrature points
void BE_matrix2(double delta_x, double delta_y, double delta_z,
	double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8,
  double E, double nu){

	double DN[3][8];
	int i1,j1,i3,i,j,k;
	double a,b,c;
	double x,y,z;
	int cpt;

  double gxyz[2];

  //location for 2 quadrature points
  //One can use only 2 quadrature points because the shape functions are linear
  gxyz[0]=-0.577350269189626;
  gxyz[1]=0.577350269189626;


  a=0.5*delta_x;
  b=0.5*delta_y;
  c=0.5*delta_z;


cpt=1;
for (i=0;i<2;i++){
  for (j=0;j<2;j++){
    for (k=0;k<2;k++){

                    x=gxyz[i];
                    y=gxyz[j];
                    z=gxyz[k];


                    for (i1=0;i1<MNE;i1++){
                        for (j1=0;j1<6;j1++){

                          //Shape functions
                          //N[0]=(1-x)*(1-y)*(1-z)/8;
                          //N[1]=(1+x)*(1-y)*(1-z)/8;
                          //N[2]=(1+x)*(1+y)*(1-z)/8;
                          //N[3]=(1-x)*(1+y)*(1-z)/8;
                          //N[4]=(1-x)*(1-y)*(1+z)/8;
                          //N[5]=(1+x)*(1-y)*(1+z)/8;
                          //N[6]=(1+x)*(1+y)*(1+z)/8;
                          //N[7]=(1-x)*(1+y)*(1+z)/8;

                //Derivatives of shape functions
                //DN[i][0] derivative of shape functions N[i] with respect to x
                //DN[i][1] derivative of shape functions N[i] with respect to y
                //DN[i][2] derivative of shape functions N[i] with respect to z

                DN[0][0]=-(1-y)*(1-z)*(1/a)/(2*2*2); DN[1][0]=-(1-x)*(1-z)*(1/b)/(2*2*2);  DN[2][0]=-(1-x)*(1-y)*(1/c)/(2*2*2);
								DN[0][1]=(1-y)*(1-z)*(1/a)/(2*2*2);  DN[1][1]=-(1+x)*(1-z)*(1/b)/(2*2*2);  DN[2][1]=-(1+x)*(1-y)*(1/c)/(2*2*2);
								DN[0][2]=(1+y)*(1-z)*(1/a)/(2*2*2);  DN[1][2]=(1+x)*(1-z)*(1/b)/(2*2*2);   DN[2][2]=-(1+x)*(1+y)*(1/c)/(2*2*2);
								DN[0][3]=-(1+y)*(1-z)*(1/a)/(2*2*2); DN[1][3]=(1-x)*(1-z)*(1/b)/(2*2*2);   DN[2][3]=-(1-x)*(1+y)*(1/c)/(2*2*2);
								DN[0][4]=-(1-y)*(1+z)*(1/a)/(2*2*2); DN[1][4]=-(1-x)*(1+z)*(1/b)/(2*2*2);  DN[2][4]=(1-x)*(1-y)*(1/c)/(2*2*2);
								DN[0][5]=(1-y)*(1+z)*(1/a)/(2*2*2);  DN[1][5]=-(1+x)*(1+z)*(1/b)/(2*2*2);  DN[2][5]=(1+x)*(1-y)*(1/c)/(2*2*2);
								DN[0][6]=(1+y)*(1+z)*(1/a)/(2*2*2);  DN[1][6]=(1+x)*(1+z)*(1/b)/(2*2*2);   DN[2][6]=(1+x)*(1+y)*(1/c)/(2*2*2);
								DN[0][7]=-(1+y)*(1+z)*(1/a)/(2*2*2); DN[1][7]=(1-x)*(1+z)*(1/b)/(2*2*2);   DN[2][7]=(1-x)*(1+y)*(1/c)/(2*2*2);

                double B[24][6]={0};

                for(i3=0;i3<8;i3++){
                  B[0+3*i3][0]=DN[0][i3]; B[1+3*i3][0]=0; 				B[2+3*i3][0]=0;
                  B[0+3*i3][1]=0; 				B[1+3*i3][1]=DN[1][i3]; B[2+3*i3][1]=0;
                  B[0+3*i3][2]=0; 				B[1+3*i3][2]=0; 				B[2+3*i3][2]=DN[2][i3];
                  B[0+3*i3][3]=DN[1][i3];	B[1+3*i3][3]=DN[0][i3]; B[2+3*i3][3]=0;
                  B[0+3*i3][4]=0;					B[1+3*i3][4]=DN[2][i3]; B[2+3*i3][4]=DN[1][i3];
                  B[0+3*i3][5]=DN[2][i3];	B[1+3*i3][5]=0; 				B[2+3*i3][5]=DN[0][i3];

              }


          //In the loops
          //for (i=0;i<2;i++){
          //  for (j=0;j<2;j++){
          //    for (k=0;k<2;k++){
          //      ...
          //    }
          //  }
          //}
          // the order is (i,j,k)= (0,0,0) -- (0,0,1) -- (0,1,0) -- (0,1,1) -- (1,0,0) -- ... and so on
          //It does not correspond to order that has been chosen to calculate the quadrature points.
          //For example (i,j,k) = (1,0,0) correspond to our second quadrature points but
          //it only appears at the fifth position in the loop, that is why we have something like
          // 	"if(cpt==5) BE[24*j1+i1]=B[i1][j1];" to get that BE corresponds to the first quadrature point,
          // BE corresponds to the second quadrature points and so on

          if(cpt==1) BE1[MNE*j1+i1]=B[i1][j1];
					if(cpt==2) BE5[MNE*j1+i1]=B[i1][j1];
					if(cpt==3) BE4[MNE*j1+i1]=B[i1][j1];
					if(cpt==4) BE8[MNE*j1+i1]=B[i1][j1];
					if(cpt==5) BE2[MNE*j1+i1]=B[i1][j1];
					if(cpt==6) BE6[MNE*j1+i1]=B[i1][j1];
					if(cpt==7) BE3[MNE*j1+i1]=B[i1][j1];
					if(cpt==8) BE7[MNE*j1+i1]=B[i1][j1];
}

//printf("\n");

}
cpt=cpt+1;

//printf("j1=%d\n",j1);

}
}
}


}


//Calculation of the constitutuve elastic matrix
void matC(double E,double nu,double *C){
	double den;
	den=(double)E/((1+nu)*(1-2*nu));

	C[0]=den*(1-nu);
	C[1]=den*nu;
	C[2]=den*nu;
	C[0+6*1]=den*nu;
	C[1+6*1]=den*(1-nu);
	C[2+6*1]=den*nu;
	C[0+6*2]=den*nu;
	C[1+6*2]=den*nu;
	C[2+6*2]=den*(1-nu);
	C[3+6*3]=den*0.5*(1-2*nu);
	C[4+6*4]=den*0.5*(1-2*nu);
	C[5+6*5]=den*0.5*(1-2*nu);


}

//Matrix transpose
void transMat(int i1, int j1, double *original, double *trans){
	//printf("BE \n");
	int i,j;
	for(j=0;j<i1;j++){
		for(i=0;i<j1;i++){
			trans[i+j1*j]=original[j+i*i1];
			//printf("%f ",original[i+i1*j]);
		}
		//printf("\n");
	}
}

//Matrix vector product on CPU
void prodMatVec(int i1, int j1, int j2, double *Mat, double *Vec, double *product){
int j,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[k+i1*j]*Vec[k];
		}
		product[j]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

void prodVecMat(int i1, int i2, int j2, double *Vec, double *Mat, double *product){
int i,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (i=0;i<i2;i++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[i+i2*k]*Vec[k];
      		}
		product[i]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

void prodVecMat1(int i1, int i2, int j2, double *Vec, double *Mat, double *product, int e, int q){
int i,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (i=0;i<i2;i++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[i+i2*k]*Vec[k];
      if(i==0 && e==0 && q==0) printf("Mat=%lg, Vec=%lg,somme=%lg\n",Mat[i+i1*k],Vec[k],somme);
      		}
		product[i]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}


void prodMatMat(int i1, int j1, int i2, int j2, double *Mat1, double *Mat2, double *product){
int j,k,l;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
for (l=0;l<i2;l++){
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat1[k+i1*j]*Mat2[i2*k+l];
		}
		product[j*i2+l]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}
}

//Matrix vector product on CPU
void prodMatVec_e(int i1, int j1, int j2, double *Mat, double rho, double penal,double *Vec, double *product){
int j,k;
double somme;
if(i1!=j2) printf("DIMENSIONS PROBLEM : use transposed matrix maybe\n");
	for (j=0;j<j1;j++){
		somme=0;
		for (k=0;k<i1;k++){
			somme=somme+Mat[k+i1*j]*pow(rho,penal)*Vec[k];
		}
		product[j]=somme;
	//	if(vrai==0) printf("%f\n",product[j]);
	}
}

//Relation between node number and degrees of freedom for a voxel mesh
void edofMat_calculation(int id, int ni, int nj, int *edofMat){
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//z1
  edofMat[3]=id+3;//x2
  edofMat[4]=id+4;//y2
  edofMat[5]=id+5;//z2
  edofMat[6]=id+3+3*(ni+1); //x3
  edofMat[7]=id+4+3*(ni+1); //y3
  edofMat[8]=id+5+3*(ni+1); //z3
  edofMat[9]=id+3*(ni+1); //x4
  edofMat[10]=id+1+3*(ni+1); //y4
  edofMat[11]=id+2+3*(ni+1); //z4
  edofMat[12]=id+3*(ni+1)*(nj+1);//x5
  edofMat[13]=id+1+3*(ni+1)*(nj+1);//y5
  edofMat[14]=id+2+3*(ni+1)*(nj+1);//z5
  edofMat[15]=id+3+3*(ni+1)*(nj+1);//x6
  edofMat[16]=id+4+3*(ni+1)*(nj+1);//y6
  edofMat[17]=id+5+3*(ni+1)*(nj+1);//z6
  edofMat[18]=id+3+3*(ni+1)+3*(ni+1)*(nj+1); //x7
  edofMat[19]=id+4+3*(ni+1)+3*(ni+1)*(nj+1); //y7
  edofMat[20]=id+5+3*(ni+1)+3*(ni+1)*(nj+1); //z7
  edofMat[21]=id+3*(ni+1)+3*(ni+1)*(nj+1); //x8
  edofMat[22]=id+1+3*(ni+1)+3*(ni+1)*(nj+1); //y8
  edofMat[23]=id+2+3*(ni+1)+3*(ni+1)*(nj+1); //z8

}

//Relation between node number and degrees of freedom for a voxel mesh
__device__ void edofMat_calculation_gpu(int id, int ni, int nj, int *edofMat){
  edofMat[0]=id;//x1
  edofMat[1]=id+1;//y1
  edofMat[2]=id+2;//z1
  edofMat[3]=id+3;//x2
  edofMat[4]=id+4;//y2
  edofMat[5]=id+5;//z2
  edofMat[6]=id+3+3*(ni+1); //x3
  edofMat[7]=id+4+3*(ni+1); //y3
  edofMat[8]=id+5+3*(ni+1); //z3
  edofMat[9]=id+3*(ni+1); //x4
  edofMat[10]=id+1+3*(ni+1); //y4
  edofMat[11]=id+2+3*(ni+1); //z4
  edofMat[12]=id+3*(ni+1)*(nj+1);//x5
  edofMat[13]=id+1+3*(ni+1)*(nj+1);//y5
  edofMat[14]=id+2+3*(ni+1)*(nj+1);//z5
  edofMat[15]=id+3+3*(ni+1)*(nj+1);//x6
  edofMat[16]=id+4+3*(ni+1)*(nj+1);//y6
  edofMat[17]=id+5+3*(ni+1)*(nj+1);//z6
  edofMat[18]=id+3+3*(ni+1)+3*(ni+1)*(nj+1); //x7
  edofMat[19]=id+4+3*(ni+1)+3*(ni+1)*(nj+1); //y7
  edofMat[20]=id+5+3*(ni+1)+3*(ni+1)*(nj+1); //z7
  edofMat[21]=id+3*(ni+1)+3*(ni+1)*(nj+1); //x8
  edofMat[22]=id+1+3*(ni+1)+3*(ni+1)*(nj+1); //y8
  edofMat[23]=id+2+3*(ni+1)+3*(ni+1)*(nj+1); //z8

}

int Min(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Max(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Mini(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Maxi(double d1,double d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filter(int ni, int nj, int nk, double rmin, double *x, double *df, double *df2, int *map_gradient)
{

	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;


	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
      for (k = 0; k < nk; k++){

			i02=I3D(ni,nj,i,j,k);

      if(map_gradient[i02]>-1){
				somme=0;
				somme2=0;
				for (i2 = Max(i-floor(rmin),0); i2 <= Min(i+floor(rmin),(ni-1)); i2++) {
					for (j2 = Max(j-floor(rmin),0); j2 <= Min(j+floor(rmin),(nj-1)); j2++) {
            for (k2 = Max(k-floor(rmin),0); k2 <= Min(k+floor(rmin),(nk-1)); k2++) {

								i0=I3D(ni,nj,i2,j2,k2);
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Maxi(0,fac);
								somme2=somme2+Maxi(0,fac)*x[map_gradient[i0]]*df[map_gradient[i0]];
            }
					}
				}


			if(x[map_gradient[i02]]*somme>0) {
				df2[map_gradient[i02]]=somme2/(x[map_gradient[i02]]*somme);
			}
			else {
				df2[map_gradient[i02]]=df[map_gradient[i02]];
			}

    }
		}
		}
  }

}

void FE_internal_forces2(int qpoints, double delta_x, double delta_y, double delta_z,
  double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8,
  int numElems_red,  double *stress, double *FE_int){

	int e,i1,l;
	double a,b,c;
	double somme_l1,somme_l2,somme_l3,somme_l4;
  double somme_l5,somme_l6,somme_l7,somme_l8;

a=0.5*delta_x;
b=0.5*delta_y;
c=0.5*delta_z;


for (e=0;e<numElems_red;e++){
//  if(e==0) printf("e=0,q=0, stress=[%lg %lg %lg %lg %lg %lg]\n",
//  stress[qpoints*6*e+0],stress[qpoints*6*e+1],stress[qpoints*6*e+2],stress[qpoints*6*e+3],stress[qpoints*6*e+4],stress[qpoints*6*e+5]);
  //if(active_elements[e]==1){

for (i1=0;i1<MNE;i1++){

          somme_l1=0;   somme_l2=0;   somme_l3=0;   somme_l4=0;
          somme_l5=0;   somme_l6=0;   somme_l7=0;   somme_l8=0;


            for (l=0;l<6;l++){
              somme_l1=somme_l1+BE1[6*i1+l]*stress[qpoints*6*e+6*0+l];
              somme_l2=somme_l2+BE2[6*i1+l]*stress[qpoints*6*e+6*1+l];
              somme_l3=somme_l3+BE3[6*i1+l]*stress[qpoints*6*e+6*2+l];
              somme_l4=somme_l4+BE4[6*i1+l]*stress[qpoints*6*e+6*3+l];
              somme_l5=somme_l5+BE5[6*i1+l]*stress[qpoints*6*e+6*4+l];
              somme_l6=somme_l6+BE6[6*i1+l]*stress[qpoints*6*e+6*5+l];
              somme_l7=somme_l7+BE7[6*i1+l]*stress[qpoints*6*e+6*6+l];
              somme_l8=somme_l8+BE8[6*i1+l]*stress[qpoints*6*e+6*7+l];
            }

        FE_int[MNE*e+i1]=(somme_l1+somme_l2+somme_l3+somme_l4+somme_l5+somme_l6+somme_l7+somme_l8)*a*b*c;
        //if(e==0) printf("FE_int=%lg\n",FE_int[MNE*e+i1]);

}

//}
}

}

void inversion(int m, double *original, double *inverse, int e){

int i,j,k,j1;
double max1,pivot,temp;
double *reference=(double *)calloc(m*m,sizeof(double));
double *original1=(double *)calloc(m*m,sizeof(double));

int r=0;

for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    inverse[j+m*i]=0;
    reference[j+m*i]=0;
    original1[j+m*i]=original[j+m*i];
    if(i==j) {
      inverse[j+m*i]=1;
      reference[j+m*i]=1;
    }
  }
}

for (j=0;j<m;j++){
  //recherche max
  max1=-1;
  for (i=r;i<m;i++){
    if(abs(original[j+m*i])>max1) { max1=abs(original[j+m*i]); k=i;}
  }

  if(original[j+m*k]!=0){
    pivot=original[j+m*k];

  for (j1=0;j1<m;j1++){
    original[j1+m*k]=(double)original[j1+m*k]/pivot;
    inverse[j1+m*k]=(double)inverse[j1+m*k]/pivot;
  }

  if(k!=r){
    for (j1=0;j1<m;j1++){
      temp=original[j1+m*k];  original[j1+m*k]=original[j1+m*r];  original[j1+m*r]=temp;
      temp=inverse[j1+m*k];   inverse[j1+m*k]=inverse[j1+m*r];  inverse[j1+m*r]=temp;
    }
  }


  for (i=0;i<m;i++){
    if(i!=r){
      pivot=original[j+m*i];
      //printf("pivot=%lg\n",pivot);
      for (j1=0;j1<m;j1++){
        original[j1+m*i]=original[j1+m*i]-original[j1+m*r]*pivot;
        inverse[j1+m*i]=inverse[j1+m*i]-inverse[j1+m*r]*pivot;
      }
    }
  }

  r++;

  }

}

//Verification
double difference=0;
for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    difference=difference+abs(original[m*i+j]-reference[m*i+j]);
  }
}


for (i=0;i<m;i++){
  for (j=0;j<m;j++){
    original[m*i+j]=original1[m*i+j];
  }
}

if(difference>1e-6) {
  printf("issue with inversion : element %d : diff=%lg\n",e,difference);
  printf("original\n");
  for (i=0;i<m;i++){
    for (j=0;j<m;j++){
      printf("%lg ",original[m*i+j]);
    }
    printf("\n");
  }
  printf("inverse\n");
  for (i=0;i<m;i++){
    for (j=0;j<m;j++){
      printf("%lg ",inverse[m*i+j]);
    }
    printf("\n");
  }
}

}

//Adjoint and gradient functions
void dH_due(int qpoints, int e, int numElems_red, int l, double *C1, double *yphys_red, double *BE1, double *rho_red, double penal, double *dH_due_q1)
{

  int i;
  double mat1[144]={0};
  prodMatMat(6,6,24,6,C1,BE1,mat1);

  for(i=0;i<6*3*qpoints;i++){
  dH_due_q1[i]=pow(rho_red[e],penal)*yphys_red[l*numElems_red+e]*mat1[i];
  }
}

void dR_dwe(int l, int numElems_red, int q, int qpoints, int e, double *ID_mat, double *dn_ds,
  double hard_constant_e, double Ge, double weight1, double delta_x, double delta_y, double delta_z, double *BE1_t, double *dR_dwe_q1,
bool *plasticity_nodes)
{
  int i,m1,m2;
  double *dR_dw1e_q1=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dw2e_q1=(double *)calloc(6*6,sizeof(double));

  double frac=2*Ge/(2*Ge+hard_constant_e);

  for(i=0;i<144;i++)   dR_dw1e_q1[i]=-BE1_t[i]*weight1*0.5*delta_x*0.5*delta_y*0.5*delta_z;

  for (m1=0;m1<6;m1++) {
    for (m2=0;m2<6;m2++) {

      dR_dw2e_q1[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*numElems_red*l+qpoints*e+q]*dn_ds[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*m1+m2]*frac;
    }
  }


  prodMatMat(6,24,6,6,dR_dw1e_q1,dR_dw2e_q1,dR_dwe_q1);


    free(dR_dw1e_q1);
    free(dR_dw2e_q1);


}

void dS_dY_f1(int l, int numElems_red, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
    double *dS_dYe_q1, bool *plasticity_nodes)
{
  int m1;

  double frac=2*Ge/(2*Ge+hard_constant_e);

  for (m1=0;m1<6;m1++) {
        dS_dYe_q1[m1]=plasticity_nodes[l*numElems_red*qpoints+qpoints*e+q]*n_trial_stress[l*numElems_red*qpoints*6+qpoints*6*e+6*q+m1]*frac;
    }
}

void dS_dG_f1(int l, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,
  double Ge, double hard_constant_e, double yield_stress_e, double *dS1_dG_q1,bool *plasticity_nodes)
{
  int m1;

  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac=-2*hard_constant_e/denom2;

  for (m1=0;m1<6;m1++) {
        dS1_dG_q1[m1]=plasticity_nodes[l*numElems_red*qpoints+qpoints*e+q]*
        (norm_dev_stress[l*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[l*numElems_red*qpoints*6+qpoints*6*e+6*q+m1]*frac;
    }

}

void dS_da_f1(int l, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,
  double Ge, double hard_constant_e, double yield_stress_e, double *dS_da_q1,bool *plasticity_nodes)
{
  int m1;

  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac=2*Ge/denom2;

  for (m1=0;m1<6;m1++) {
        dS_da_q1[m1]=plasticity_nodes[l*numElems_red*qpoints+qpoints*e+q]*
        (norm_dev_stress[l*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[l*numElems_red*qpoints*6+qpoints*6*e+6*q+m1]*frac;
    }
}

void assembly_lambda(double *dR_dwe_q1, double *dR_dwe_q2, double *dR_dwe_q3, double *dR_dwe_q4,
  double *dR_dwe_q5, double *dR_dwe_q6, double *dR_dwe_q7, double *dR_dwe_q8,
double *dH_due_q1, double *dH_due_q2, double *dH_due_q3, double *dH_due_q4,
double *dH_due_q5, double *dH_due_q6, double *dH_due_q7, double *dH_due_q8, double *Ke2)
{

int i;

//Tangent matrix 2
double Ke_q1[576]={0};
double Ke_q2[576]={0};
double Ke_q3[576]={0};
double Ke_q4[576]={0};
double Ke_q5[576]={0};
double Ke_q6[576]={0};
double Ke_q7[576]={0};
double Ke_q8[576]={0};

prodMatMat(6,24,24,6,dR_dwe_q1,dH_due_q1,Ke_q1);
prodMatMat(6,24,24,6,dR_dwe_q2,dH_due_q2,Ke_q2);
prodMatMat(6,24,24,6,dR_dwe_q3,dH_due_q3,Ke_q3);
prodMatMat(6,24,24,6,dR_dwe_q4,dH_due_q4,Ke_q4);
prodMatMat(6,24,24,6,dR_dwe_q5,dH_due_q5,Ke_q5);
prodMatMat(6,24,24,6,dR_dwe_q6,dH_due_q6,Ke_q6);
prodMatMat(6,24,24,6,dR_dwe_q7,dH_due_q7,Ke_q7);
prodMatMat(6,24,24,6,dR_dwe_q8,dH_due_q8,Ke_q8);

for(i=0;i<MNE*MNE;i++){
  Ke2[i]=Ke_q1[i]+Ke_q2[i]+Ke_q3[i]+Ke_q4[i]+Ke_q5[i]+Ke_q6[i]+Ke_q7[i]+Ke_q8[i];
}
}


void deta_du_f1(int l, int e, int numElems_red, double *rho_red, double penal, double *yphys_red, double *C,
  double *DEV, double *BE, double *deta_du)
{
int i;
double prod1[MNE*6];
double prod2[MNE*6];

//ProdMat 1
prodMatMat(6,6,MNE,6,C,BE,prod1);
for (i=0;i<MNE*6;i++) prod1[i]=pow(rho_red[e],penal)*yphys_red[l*numElems_red+e]*prod1[i];

//ProdMat 2
prodMatMat(6,6,MNE,6,DEV,prod1,prod2);
for (i=0;i<MNE*6;i++) deta_du[MNE*6*l+i]=prod2[i];
}

void dep_du_f(int q, int qpoints, int e, int numElems_red, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_du, double *dep_du)
{
  int i;
  double dep_deta[36];
  double prod[24*6];
  double mat1[24*6];

  double frac1=1.f/(2*Ge+hard_constant_e);

  for (i=0;i<6*6;i++) {
    if(i<18) dep_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
    if(i>=18) dep_deta[i]=2*plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
    //if(e==0) printf("dep_deta=%lg\n",dep_deta[i]);
  }

  for (i=0;i<24*6;i++) mat1[i]=deta_du[24*6*(l2-1)+i];

  prodMatMat(6,6,24,6,dep_deta,mat1,prod);

  for (i=0;i<24*6;i++) dep_du[24*6*l2+i]=prod[i]+dep_du[24*6*(l2-1)+i];
}

void dbeta_du_f(int q, int qpoints, int e, int numElems_red, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_du, double *dbeta_du)
{
  int i;
  double dbeta_deta[36];
  double prod[24*6];
  double mat1[24*6];

  double frac1=hard_constant_e/(2*Ge+hard_constant_e);

  for (i=0;i<6*6;i++) {
  dbeta_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
  }

  for (i=0;i<24*6;i++) mat1[i]=deta_du[24*6*(l2-1)+i];

  prodMatMat(6,6,24,6,dbeta_deta,mat1,prod);

  for (i=0;i<24*6;i++) dbeta_du[24*6*l2+i]=prod[i]+dbeta_du[24*6*(l2-1)+i];
}

void deta_du_f2(int l2, int e, int numElems_red, double *rho_red, double penal, double *yphys_red, double *C,
  double *DEV, double *dep_du, double *dbeta_du, double *deta_du)
{
int i;
double prod1[6*6];
double mat1[MNE*6];
double prod2[MNE*6];

//ProdMat 1
prodMatMat(6,6,6,6,DEV,C,prod1);
for (i=0;i<6*6;i++) prod1[i]=-pow(rho_red[e],penal)*yphys_red[l2*numElems_red+e]*prod1[i];

for (i=0;i<MNE*6;i++) {
  mat1[i]=dep_du[24*6*l2+i];
}

//ProdMat 2
prodMatMat(6,6,MNE,6,prod1,mat1,prod2);
for (i=0;i<MNE*6;i++) deta_du[MNE*6*l2+i]=prod2[i]-dbeta_du[24*6*l2+i];
}

void deta_du_f3(int l, int *initial_elements_red, int e, int numElems_red, double *rho_red, double penal, double *yphys_red, double *C,
  double *DEV,  double *BE, int l1, double *deta_du)
{
int i;
double prod1[6*6];
double mat1[MNE*6];
double prod2[MNE*6];

//ProdMat 1
prodMatMat(6,6,6,6,DEV,C,prod1);
for (i=0;i<6*6;i++) prod1[i]=-pow(rho_red[e],penal)*yphys_red[l1*numElems_red+e]*prod1[i];

for (i=0;i<MNE*6;i++) {
  mat1[i]=initial_elements_red[(l+1)*numElems_red+e]*BE[i];
}

//ProdMat 2
prodMatMat(6,6,MNE,6,prod1,mat1,prod2);
for (i=0;i<MNE*6;i++) deta_du[MNE*6*l1+i]=deta_du[MNE*6*l1+i]+prod2[i];
}

void deini_du_f(int l, int e, int numElems_red, double *BE, int *initial_elements_red, double *deini_du)
{
int i;
  for(i=0;i<144;i++) deini_du[i]=initial_elements_red[(l+1)*numElems_red+e]*BE[i];
}

void Ke_rhs_adjoint1(int l1, int loadsteps, int numElems_red, double delta_x, double delta_y, double delta_z, int q,int qpoints, int e,
   double *BE_t, double weight, double *dvm_dwe, double *rho_red, double penal, double *yphys_red, double *C,
   double *dep_du, double *Ke_rhs1, double *dc_rhs1)
{

  int i,m1;
  double dR_dw1e_q1[144];
  double dc_dH[6];
  double dH_dep[36];

  //First derivative R
  for(i=0;i<144;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y*0.5*delta_z;

  //First derivative C
  for(i=0;i<6;i++)   {
    dc_dH[i]=dvm_dwe[6*e+i];
  }

  //Second derivative
  for (m1=0;m1<6*6;m1++) dH_dep[m1]=-pow(rho_red[e],penal)*yphys_red[l1*numElems_red+e]*C[m1];

  //Third derivative
  double dep_du1[144]={0};
  for(i=0;i<24*6;i++)  dep_du1[i]=dep_du[24*6*l1+i];

  //Matproduct 1
  double mat1[24*6]={0};
  prodMatMat(6,6,24,6,dH_dep,dep_du1,mat1);

  //Matproduct 2 R
  prodMatMat(6,24,24,6,dR_dw1e_q1,mat1,Ke_rhs1);

  //Matproduct 2 c
  prodVecMat(6,24,6,dc_dH,mat1,dc_rhs1);
}


void Ke_rhs_adjoint2(int l1, int loadsteps, int numElems_red, double delta_x, double delta_y, double delta_z,
  int q,int qpoints, int e, double *BE_t, double weight, double Ge, double hard_constant_e, double *dvm_dwe,
  double *dH_deta, double *deta_du, double *Ke_rhs2,  double *dc_rhs2, bool *plasticity_nodes)
{

  int i,m1;
  double dR_dw1e_q1[144];
  double dH_deta1[36];
  double dc_dH[6];

  double frac1=2*Ge/(2*Ge+hard_constant_e);

  //First derivative R
  for(i=0;i<144;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y*0.5*delta_z;

  //First derivative C
  for(i=0;i<6;i++)   dc_dH[i]=dvm_dwe[6*e+i];

  //Second derivative
  for (m1=0;m1<6*6;m1++) {
      dH_deta1[m1]=-plasticity_nodes[qpoints*numElems_red*l1+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*l1+36*qpoints*e+36*q+m1]*frac1;
  }

  //Third derivative
  double deta_du1[144]={0};
  for(i=0;i<24*6;i++)  deta_du1[i]=deta_du[24*6*l1+i];

  //Matproduct 1
  double mat1[24*6]={0};
  prodMatMat(6,6,24,6,dH_deta1,deta_du1,mat1);

  //Matproduct2 R
  prodMatMat(6,24,24,6,dR_dw1e_q1,mat1,Ke_rhs2);

  //Matproduct2 c
  prodVecMat(6,24,6,dc_dH,mat1,dc_rhs2);
}

void Ke_rhs_adjoint3(int l1, int loadsteps, int numElems_red, double delta_x, double delta_y, double delta_z, int q,int qpoints, int e,
   double *BE_t, double weight, double *dvm_dwe, double *rho_red, double penal, double *yphys_red, double *C,
   double *deini_du, double *Ke_rhs1, double *dc_rhs1)
{

  int i,m1;
  double dR_dw1e_q1[144];
  double dH_deini[36];
  double dc_dH[6];

  //First derivative R
  for(i=0;i<144;i++)   dR_dw1e_q1[i]=-BE_t[i]*weight*0.5*delta_x*0.5*delta_y*0.5*delta_z;

  //First derivative c
  for(i=0;i<6;i++)   dc_dH[i]=dvm_dwe[6*e+i];

  //Second derivative
  for (m1=0;m1<6*6;m1++)  dH_deini[m1]=-pow(rho_red[e],penal)*yphys_red[l1*numElems_red+e]*C[m1];

  //Matproduct 1
  double mat1[24*6]={0};
  prodMatMat(6,6,24,6,dH_deini,deini_du,mat1);

  //Matproduct2 R
  prodMatMat(6,24,24,6,dR_dw1e_q1,mat1,Ke_rhs1);

  //Matproduct2 c
  prodVecMat(6,24,6,dc_dH,mat1,dc_rhs1);

}

void deta_dC_f1(int *edofMat_e_red, int ni, int nj, int l2, int numElems_red, int numNodes_red,int q, int qpoints, int e, double *rho_red, double penal, double *yphys_red, double *C,
  double *DEV_mat, double *BE, double *U_red_l, double *inh_strain2_l, double *old_plastic_strain_l, double *strain_ini, double *deta_dC)
{
  int i,m;

  double Ue[24]={0};
  double strain[6]={0};
  double deta_dC1[6]={0};
  double dC_dx[36]={0};
  double prod1[36]={0};
  double penal1=penal-1;


  for (i=0;i<6*6;i++) dC_dx[i]=penal*pow(rho_red[e],penal1)*yphys_red[l2*numElems_red+e]*C[i];

  prodMatMat(6,6,6,6,DEV_mat,dC_dx,prod1);

  for (m=0;m<24;m++) {
    Ue[m]=U_red_l[numNodes_red*l2+edofMat_e_red[MNE*e+m]];
        }

  prodMatVec(3*qpoints,6,3*qpoints,BE,Ue,strain);

  for (m=0;m<6;m++) {
    strain[m]=strain[m]-inh_strain2_l[6*l2*numElems_red+6*e+m]-
    old_plastic_strain_l[l2*numElems_red*6*qpoints+6*qpoints*e+6*q+m]-strain_ini[l2*numElems_red*6*qpoints+6*qpoints*e+6*q+m];
    //if(e==0 && q==0 && l2==0) printf("new code : strain1=%lg\n",strain[m]);
  }

  prodMatVec(6,6,6,prod1,strain,deta_dC1);

  for (m=0;m<6;m++) deta_dC[6*l2+m]=deta_dC1[m];
}

void dep_dC_f(int q, int qpoints, int e, int numElems_red, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_dC, double *dep_dC)
{
  int i;
  double dep_deta[36];
  double prod[6];
  double mat1[6];

  double frac1=1.f/(2*Ge+hard_constant_e);

  for (i=0;i<6*6;i++) {
    if(i<18) dep_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
    if(i>=18) dep_deta[i]=2*plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
      }

  for (i=0;i<6;i++) {
    mat1[i]=deta_dC[6*(l2-1)+i];
  }

  prodMatVec(6,6,6,dep_deta,mat1,prod);

  for (i=0;i<6;i++) dep_dC[6*l2+i]=prod[i]+dep_dC[6*(l2-1)+i];
}

void dbeta_dC_f(int q, int qpoints, int e, int numElems_red, int l2, double Ge, double hard_constant_e,
  bool *plasticity_nodes, double *dH_deta, double *deta_dC, double *dbeta_dC)
{
  int i;
  double dbeta_deta[36];
  double prod[6];
  double mat1[6];

  double frac1=hard_constant_e/(2*Ge+hard_constant_e);

  for (i=0;i<6*6;i++) {
  dbeta_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
  }

  for (i=0;i<6;i++) mat1[i]=deta_dC[6*(l2-1)+i];

  prodMatVec(6,6,6,dbeta_deta,mat1,prod);

  for (i=0;i<6;i++) dbeta_dC[6*l2+i]=prod[i]+dbeta_dC[6*(l2-1)+i];
}

void dep_dY_f(int l2, int numElems_red, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
                double *dH_deta, double *deta_dY, double *dep_dY, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double dep_dY1[6];
  double dep_dY2[6];
  double dep_dY3[6];
  double dep_deta[36];
  double deta_dY1[6];

  for (i=0;i<6;i++){
    if(i<3){
      dep_dY1[i]=-plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac1;
          }
          else{
      dep_dY1[i]=-2*plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac1;
          }

      dep_dY2[i]=dep_dY[6*(l2-1)+i];
                }

  for (i=0;i<6*6;i++) {
      if(i<18){
      dep_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
              }
      else{
      dep_deta[i]=2*plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
          }
                     }

  for (i=0;i<6;i++) deta_dY1[i]=deta_dY[6*(l2-1)+i];

  prodMatVec(6,6,6,dep_deta,deta_dY1,dep_dY3);

  for (i=0;i<6;i++) {
    dep_dY[6*l2+i]=dep_dY1[i]+dep_dY2[i]+dep_dY3[i];
    //if(e==0 && q==0) printf("l2=%d, dep_dY=%lg, [%lg %lg %lg]\n",l2,dep_dY[i],dep_dY1[i],dep_dY2[i],dep_dY3[i]);
  }

}

void dbeta_dY_f(int l2, int numElems_red, int q, int qpoints, int e, double *n_trial_stress, double Ge, double hard_constant_e,
                double *dH_deta, double *deta_dY, double *dbeta_dY, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double dbeta_dY1[6];
  double dbeta_dY2[6];
  double dbeta_dY3[6];
  double dbeta_deta[36];
  double deta_dY1[6];

  for (i=0;i<6;i++){
      dbeta_dY1[i]=-plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac1;
      dbeta_dY2[i]=dbeta_dY[6*(l2-1)+i];
                }

  for (i=0;i<6*6;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
                }

  for (i=0;i<6;i++) deta_dY1[i]=deta_dY[6*(l2-1)+i];

  prodMatVec(6,6,6,dbeta_deta,deta_dY1,dbeta_dY3);

  for (i=0;i<6;i++) dbeta_dY[6*l2+i]=dbeta_dY1[i]+dbeta_dY2[i]+dbeta_dY3[i];

}

void dep_dG_f(int l2, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress,  double Ge, double yield_stress_e,
                double hard_constant_e, double *dH_deta, double *deta_dG, double *dep_dG, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2.f/denom2;
  double dep_dG1[6];
  double dep_dG2[6];
  double dep_dG3[6];
  double dep_deta[36];
  double deta_dG1[6];

  for (i=0;i<6;i++){
    if(i<3){
      dep_dG1[i]=-plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
          }
          else{
      dep_dG1[i]=-2*plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
            (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
          }

      dep_dG2[i]=dep_dG[6*(l2-1)+i];
                }

  for (i=0;i<6*6;i++) {
      if(i<18){
      dep_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
              }
      else{
      dep_deta[i]=2*plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
          }
                     }

  for (i=0;i<6;i++) deta_dG1[i]=deta_dG[6*(l2-1)+i];

  prodMatVec(6,6,6,dep_deta,deta_dG1,dep_dG3);

  for (i=0;i<6;i++) {
    dep_dG[6*l2+i]=dep_dG1[i]+dep_dG2[i]+dep_dG3[i];
    //if(e==0 && q==0) printf("dep_dG=[%lg %lg %lg]\n",dep_dG1[i],dep_dG2[i],dep_dG3[i]);
  }

}

void dbeta_dG_f(int l2, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                 double hard_constant_e, double *dH_deta, double *deta_dG, double *dbeta_dG, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2*hard_constant_e/denom2;
  double dbeta_dG1[6];
  double dbeta_dG2[6];
  double dbeta_dG3[6];
  double dbeta_deta[36];
  double deta_dG1[6];

  for (i=0;i<6;i++){
      dbeta_dG1[i]=-plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
      dbeta_dG2[i]=dbeta_dG[6*(l2-1)+i];
                }

  for (i=0;i<6*6;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
                }

  for (i=0;i<6;i++) deta_dG1[i]=deta_dG[6*(l2-1)+i];

  prodMatVec(6,6,6,dbeta_deta,deta_dG1,dbeta_dG3);

  for (i=0;i<6;i++) dbeta_dG[6*l2+i]=dbeta_dG1[i]+dbeta_dG2[i]+dbeta_dG3[i];

}

void dep_da_f(int l2, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                double hard_constant_e, double *dH_deta, double *deta_da, double *dep_da, bool *plasticity_nodes)
{
  int i;
  double frac1=1.f/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=1.f/denom2;
  double dep_da1[6];
  double dep_da2[6];
  double dep_da3[6];
  double dep_deta[36];
  double deta_da1[6];

  for (i=0;i<6;i++){
    if(i<3){
      dep_da1[i]=-plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
        (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
      }
      else{
        dep_da1[i]=-2*plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
        (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
      }

      dep_da2[i]=dep_da[6*(l2-1)+i];

                }

  for (i=0;i<6*6;i++) {
      dep_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
                     }

  for (i=0;i<6;i++) deta_da1[i]=deta_da[6*(l2-1)+i];

  prodMatVec(6,6,6,dep_deta,deta_da1,dep_da3);

  for (i=0;i<6;i++) {
    dep_da[6*l2+i]=dep_da1[i]+dep_da2[i]+dep_da3[i];
      //if(e==0 && q==0)  printf("dep_da=[%lg %lg %lg]\n",dep_da1[i],dep_da2[i],dep_da3[i]);
    }

}

void dbeta_da_f(int l2, int numElems_red, int q, int qpoints, int e, double *norm_dev_stress, double *n_trial_stress, double Ge, double yield_stress_e,
                 double hard_constant_e, double *dH_deta, double *deta_da, double *dbeta_da, bool *plasticity_nodes)
{
  int i;
  double frac1=hard_constant_e/(2*Ge+hard_constant_e);
  double denom=2*Ge+hard_constant_e;
  double denom2=denom*denom;
  double frac2=2*Ge/denom2;
  double dbeta_da1[6];
  double dbeta_da2[6];
  double dbeta_da3[6];
  double dbeta_deta[36];
  double deta_da1[6];

  for (i=0;i<6;i++){
      dbeta_da1[i]=plasticity_nodes[(l2-1)*numElems_red*qpoints+qpoints*e+q]*
      (norm_dev_stress[(l2-1)*numElems_red*qpoints+qpoints*e+q]-yield_stress_e)*n_trial_stress[(l2-1)*numElems_red*qpoints*6+qpoints*6*e+6*q+i]*frac2;
      dbeta_da2[i]=dbeta_da[6*(l2-1)+i];
                }

  for (i=0;i<6*6;i++) {
      dbeta_deta[i]=plasticity_nodes[qpoints*numElems_red*(l2-1)+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*(l2-1)+36*qpoints*e+36*q+i]*frac1;
                }

  for (i=0;i<6;i++) deta_da1[i]=deta_da[6*(l2-1)+i];

  prodMatVec(6,6,6,dbeta_deta,deta_da1,dbeta_da3);

  for (i=0;i<6;i++) {
    dbeta_da[6*l2+i]=dbeta_da1[i]+dbeta_da2[i]+dbeta_da3[i];
    //if(e==0 && q==0)  printf("dbeta_da=[%lg %lg %lg]\n",dbeta_da1[i],dbeta_da2[i],dbeta_da3[i]);
  }

}

void deta_dCYGa_f2(int l2, int e, int q, int numElems_red, double *rho_red, double penal, double *yphys_red, double *C,
  double *DEV_mat, double *dep_dC, double *dbeta_dC, double *deta_dC)
{
int i;
double prod1[6*6];
double mat1[6];
double prod2[6];

//ProdMat 1
prodMatMat(6,6,6,6,DEV_mat,C,prod1);
for (i=0;i<6*6;i++) prod1[i]=-pow(rho_red[e],penal)*yphys_red[l2*numElems_red+e]*prod1[i];

for (i=0;i<6;i++) {
  mat1[i]=dep_dC[6*l2+i];
  //if(e==0 && q==0) printf("l2=%d, mat1=%lg\n",l2,mat1[i]);
}

//ProdMat 2
prodMatVec(6,6,6,prod1,mat1,prod2);
for (i=0;i<6;i++) {
  deta_dC[6*l2+i]=deta_dC[6*l2+i]+prod2[i]-dbeta_dC[6*l2+i];
  //if(e==0 && q==0) printf("l2=%d : deta_dC=%lg, prod2=%lg, dbeta_dC=%lg\n",l2,deta_dC[3*l2+i],prod2[i],dbeta_dC[3*l2+i]);
}
}

void dS_dC_f1_bis(int *edofMat_e_red, int l, int numElems_red, int numNodes_red, int q, int qpoints, int e, double *BE, double *U_red_l,
  double *rho_red, double penal, double *yphys_red, double *C,
double *inh_strain2_l, double *old_plastic_strain_l, double *strain_ini, double *ID_mat, double *dep_dC, double *dS1_dC)
{
  int i,m;
  double Ue[24]={0};
  double strain[6]={0}; double strain_C[6]={0};
  double mat1[36]={0};
  double mat2[6]={0};
  double prod1[6]={0};
  double ds_tr_dC[6]={0};
  double dC_dx[36]={0};
  double penal1=penal-1;

  for (m=0;m<24;m++) {
    Ue[m]=U_red_l[numNodes_red*l+edofMat_e_red[MNE*e+m]];
        }

  prodMatVec(3*qpoints,6,3*qpoints,BE,Ue,strain);

  for (m=0;m<6;m++) {
    strain[m]=strain[m]-inh_strain2_l[6*l*numElems_red+6*e+m]-
    old_plastic_strain_l[l*numElems_red*6*qpoints+6*qpoints*e+6*q+m]-strain_ini[l*numElems_red*6*qpoints+6*qpoints*e+6*q+m];
  }


  for (i=0;i<6*6;i++) {
    mat1[i]=pow(rho_red[e],penal)*yphys_red[l*numElems_red+e]*C[i];
  }

  for (i=0;i<6;i++) {
    mat2[i]=dep_dC[6*l+i];
  }

  prodMatVec(6,6,6,mat1,mat2,prod1);

  for (i=0;i<6*6;i++) dC_dx[i]=penal*pow(rho_red[e],penal1)*yphys_red[l*numElems_red+e]*C[i];
  prodMatVec(6,6,6,dC_dx,strain,strain_C);

  for (i=0;i<6;i++) {
    ds_tr_dC[i]=strain_C[i]-prod1[i];
  }
  prodMatVec(6,6,6,ID_mat,ds_tr_dC,dS1_dC);
}

void dS_dCYGa_f2_bis(int l, int loadsteps, int numElems_red, int q,int qpoints, int e, double Ge, double hard_constant_e,
  bool *plasticity_nodes,double *dH_deta, double *deta_dC, double *dS2_dC)
{

  int i,m1;
  double dH_deta1[36];

  double frac1=2*Ge/(2*Ge+hard_constant_e);


  //Second derivative
  for (m1=0;m1<6*6;m1++) {
      dH_deta1[m1]=-plasticity_nodes[qpoints*numElems_red*l+qpoints*e+q]*dH_deta[36*qpoints*numElems_red*l+36*qpoints*e+36*q+m1]*frac1;
  }

  //Third derivative
  double deta_dC1[6]={0};

  for(i=0;i<6;i++){
  deta_dC1[i]=deta_dC[6*l+i];
  //if(e==0 && q==0) printf("deta_dC/dY=%lg\n",deta_dC1[i]);
  }

  //Matproduct1
  prodMatVec(6,6,6,dH_deta1,deta_dC1,dS2_dC);


}

void dS_dYGa_f3_bis(int l, int numElems_red, int numNodes_red, int q, int qpoints, int e,
double *rho_red, double penal, double *yphys_red, double *C, double *ID_mat,
double *dep_dY, double *dS3_dY)
{
  int i;
  double dep_dY1[6]={0};
  double prod1[6]={0};

  for (i=0;i<6;i++) dep_dY1[i]=dep_dY[6*l+i];

  prodMatVec(6,6,6,C,dep_dY1,prod1);

  for (i=0;i<6;i++) prod1[i]=-pow(rho_red[e],penal)*yphys_red[l*numElems_red+e]*prod1[i];

  prodMatVec(6,6,6,ID_mat,prod1,dS3_dY);

}


void adjoint_K(int l, int loadsteps, int numElems, int numElems_red, int qpoints, double *rho_red, double penal, double G, double yield_stress, double hard_constant,
double *ID_mat, double *dn_ds_l, double *yphys_red_l, double *C1, double delta_x, double delta_y, double delta_z,
double weight1, double weight2, double weight3, double weight4, double weight5, double weight6, double weight7, double weight8,
double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8,
double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t, double *BE5_t, double *BE6_t, double *BE7_t, double *BE8_t,
bool *plasticity_nodes_l, double nodal_force, bool pnorm_stress,double *dc_dvm, double *dvm_dwe, double dc_dvm1, double *von_mises_stress,
double *stress_xx_e, double *stress_yy_e, double *stress_zz_e, double *stress_xy_e, double *stress_yz_e, double *stress_xz_e,
double pnorm, double *dc_due, bool *plasticity_nodes, double *Ke2_e, double *FE_load, int *map_elements_short, double *U_red, int *edofMat_e_red)
{
  int e,e1,q,m,m1,m2;


  double *Ke2=(double *)calloc(MNE*MNE,sizeof(double));
  double *dvm_dwe_short=(double *)calloc(6,sizeof(double));
  double *vm_q1=(double *)calloc(6,sizeof(double));
  double *vm_q2=(double *)calloc(6,sizeof(double));
  double *vm_q3=(double *)calloc(6,sizeof(double));
  double *vm_q4=(double *)calloc(6,sizeof(double));
  double *vm_q5=(double *)calloc(6,sizeof(double));
  double *vm_q6=(double *)calloc(6,sizeof(double));
  double *vm_q7=(double *)calloc(6,sizeof(double));
  double *vm_q8=(double *)calloc(6,sizeof(double));
  double *r2_q1=(double *)calloc(MNE,sizeof(double));
  double *r2_q2=(double *)calloc(MNE,sizeof(double));
  double *r2_q3=(double *)calloc(MNE,sizeof(double));
  double *r2_q4=(double *)calloc(MNE,sizeof(double));
  double *r2_q5=(double *)calloc(MNE,sizeof(double));
  double *r2_q6=(double *)calloc(MNE,sizeof(double));
  double *r2_q7=(double *)calloc(MNE,sizeof(double));
  double *r2_q8=(double *)calloc(MNE,sizeof(double));

  double *dn_ds_q1=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q2=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q3=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q4=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q5=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q6=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q7=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q8=(double *)calloc(6*6,sizeof(double));

  double *dn_ds_q1_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q2_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q3_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q4_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q5_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q6_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q7_t=(double *)calloc(6*6,sizeof(double));
  double *dn_ds_q8_t=(double *)calloc(6*6,sizeof(double));

  double *dR_dwe_q1=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q2=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q3=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q4=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q5=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q6=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q7=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dwe_q8=(double *)calloc(6*3*qpoints,sizeof(double));

  double *dH_due_q1=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q2=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q3=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q4=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q5=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q6=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q7=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q8=(double *)calloc(6*3*qpoints,sizeof(double));

  double *dH_due_q1_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q2_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q3_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q4_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q5_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q6_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q7_t=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dH_due_q8_t=(double *)calloc(6*3*qpoints,sizeof(double));

  double pnorm2=pnorm-1;

    for(e=0;e<numElems_red;e++){

    double Ge=pow(rho_red[e],penal)*G;
    double hard_constant_e=pow(rho_red[e],penal)*hard_constant;
    double yield_stress_e=pow(rho_red[e],penal)*yield_stress;

    double frac=(double)2*Ge/(2*Ge+hard_constant_e);

    q=0 ; dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight1,delta_x,delta_y,delta_z,BE1_t,dR_dwe_q1,plasticity_nodes_l);
    q=1 ; dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight2,delta_x,delta_y,delta_z,BE2_t,dR_dwe_q2,plasticity_nodes_l);
    q=2;  dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight3,delta_x,delta_y,delta_z,BE3_t,dR_dwe_q3,plasticity_nodes_l);
    q=3;  dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight4,delta_x,delta_y,delta_z,BE4_t,dR_dwe_q4,plasticity_nodes_l);
    q=4 ; dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight5,delta_x,delta_y,delta_z,BE5_t,dR_dwe_q5,plasticity_nodes_l);
    q=5 ; dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight6,delta_x,delta_y,delta_z,BE6_t,dR_dwe_q6,plasticity_nodes_l);
    q=6;  dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight7,delta_x,delta_y,delta_z,BE7_t,dR_dwe_q7,plasticity_nodes_l);
    q=7;  dR_dwe(l,numElems_red,q,qpoints,e,ID_mat,dn_ds_l, hard_constant_e,Ge, weight8,delta_x,delta_y,delta_z,BE8_t,dR_dwe_q8,plasticity_nodes_l);

      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE1,rho_red,penal,dH_due_q1);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE2,rho_red,penal,dH_due_q2);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE3,rho_red,penal,dH_due_q3);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE4,rho_red,penal,dH_due_q4);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE5,rho_red,penal,dH_due_q5);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE6,rho_red,penal,dH_due_q6);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE7,rho_red,penal,dH_due_q7);
      dH_due(qpoints,e,numElems_red,l,C1,yphys_red_l,BE8,rho_red,penal,dH_due_q8);

      transMat(24,6,dH_due_q1,dH_due_q1_t);
      transMat(24,6,dH_due_q2,dH_due_q2_t);
      transMat(24,6,dH_due_q3,dH_due_q3_t);
      transMat(24,6,dH_due_q4,dH_due_q4_t);
      transMat(24,6,dH_due_q5,dH_due_q5_t);
      transMat(24,6,dH_due_q6,dH_due_q6_t);
      transMat(24,6,dH_due_q7,dH_due_q7_t);
      transMat(24,6,dH_due_q8,dH_due_q8_t);


      assembly_lambda(dR_dwe_q1, dR_dwe_q2, dR_dwe_q3, dR_dwe_q4,
        dR_dwe_q5, dR_dwe_q6, dR_dwe_q7, dR_dwe_q8, dH_due_q1,
        dH_due_q2, dH_due_q3, dH_due_q4,dH_due_q5, dH_due_q6, dH_due_q7, dH_due_q8, Ke2);


      for(m=0;m<MNE*MNE;m++)   Ke2_e[MNE*MNE*e+m]=Ke2[m];

      //dc_due *********************************

      if(nodal_force==0 && pnorm_stress==1){
        e1=map_elements_short[e];
        if(von_mises_stress[l*numElems+e1]!=0){
        dc_dvm[e]=(double)pow(von_mises_stress[l*numElems+e1],pnorm2)*dc_dvm1/numElems_red;
        dvm_dwe[6*e+0]=(double)(2*stress_xx_e[l*numElems+e1]-stress_yy_e[l*numElems+e1]-stress_zz_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
        dvm_dwe[6*e+1]=(double)(2*stress_yy_e[l*numElems+e1]-stress_xx_e[l*numElems+e1]-stress_zz_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
        dvm_dwe[6*e+2]=(double)(2*stress_zz_e[l*numElems+e1]-stress_xx_e[l*numElems+e1]-stress_yy_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
        dvm_dwe[6*e+3]=(double)3*stress_xy_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
        dvm_dwe[6*e+4]=(double)3*stress_yz_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
        dvm_dwe[6*e+5]=(double)3*stress_xz_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
      }
      else{
        dc_dvm[e]=0;
        dvm_dwe[6*e+0]=0;
        dvm_dwe[6*e+1]=0;
        dvm_dwe[6*e+2]=0;
        dvm_dwe[6*e+3]=0;
        dvm_dwe[6*e+4]=0;
        dvm_dwe[6*e+5]=0;
      }

        for (m1=0;m1<6;m1++) {
          for (m2=0;m2<6;m2++) {
        dn_ds_q1[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+0]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*0+6*m1+m2]*frac;
        dn_ds_q2[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+1]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*1+6*m1+m2]*frac;
        dn_ds_q3[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+2]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*2+6*m1+m2]*frac;
        dn_ds_q4[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+3]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*3+6*m1+m2]*frac;
        dn_ds_q5[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+4]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*4+6*m1+m2]*frac;
        dn_ds_q6[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+5]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*5+6*m1+m2]*frac;
        dn_ds_q7[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+6]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*6+6*m1+m2]*frac;
        dn_ds_q8[6*m1+m2]=ID_mat[6*m1+m2]-plasticity_nodes[qpoints*e+7]*dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*7+6*m1+m2]*frac;

      }

        dvm_dwe_short[m1]=dvm_dwe[6*e+m1];

      }
        transMat(6,6,dn_ds_q1,dn_ds_q1_t);
        transMat(6,6,dn_ds_q2,dn_ds_q2_t);
        transMat(6,6,dn_ds_q3,dn_ds_q3_t);
        transMat(6,6,dn_ds_q4,dn_ds_q4_t);
        transMat(6,6,dn_ds_q5,dn_ds_q5_t);
        transMat(6,6,dn_ds_q6,dn_ds_q6_t);
        transMat(6,6,dn_ds_q7,dn_ds_q7_t);
        transMat(6,6,dn_ds_q8,dn_ds_q8_t);

        prodMatVec(6,6,6,dn_ds_q1_t,dvm_dwe_short,vm_q1);
        prodMatVec(6,6,6,dn_ds_q2_t,dvm_dwe_short,vm_q2);
        prodMatVec(6,6,6,dn_ds_q3_t,dvm_dwe_short,vm_q3);
        prodMatVec(6,6,6,dn_ds_q4_t,dvm_dwe_short,vm_q4);
        prodMatVec(6,6,6,dn_ds_q5_t,dvm_dwe_short,vm_q5);
        prodMatVec(6,6,6,dn_ds_q6_t,dvm_dwe_short,vm_q6);
        prodMatVec(6,6,6,dn_ds_q7_t,dvm_dwe_short,vm_q7);
        prodMatVec(6,6,6,dn_ds_q8_t,dvm_dwe_short,vm_q8);

        prodMatVec(6,24,6,dH_due_q1_t,vm_q1,r2_q1);
        prodMatVec(6,24,6,dH_due_q2_t,vm_q2,r2_q2);
        prodMatVec(6,24,6,dH_due_q3_t,vm_q3,r2_q3);
        prodMatVec(6,24,6,dH_due_q4_t,vm_q4,r2_q4);
        prodMatVec(6,24,6,dH_due_q5_t,vm_q5,r2_q5);
        prodMatVec(6,24,6,dH_due_q6_t,vm_q6,r2_q6);
        prodMatVec(6,24,6,dH_due_q7_t,vm_q7,r2_q7);
        prodMatVec(6,24,6,dH_due_q8_t,vm_q8,r2_q8);


        for(m=0;m<24;m++){
        dc_due[24*numElems_red*l+24*e+m]=dc_dvm[e]*0.125*(r2_q1[m]+r2_q2[m]+r2_q3[m]+r2_q4[m]+r2_q5[m]+r2_q6[m]+r2_q7[m]+r2_q8[m]);
        //if(e==0 || e==59) printf("e=%d : dc_due[%d]=%lg\n",e,m,dc_due[24*numElems*l+24*e+m]);
        }

      }
      else{
        if(l==loadsteps-1){

          for(m=0;m<24;m++){
        if(m==2 || m==5 || m==8 || m==11)dc_due[24*numElems_red*l+24*e+m]=2*U_red[edofMat_e_red[24*e+m]];//FE_load[m];
        if(m==14 || m==17 || m==20 || m==23)dc_due[24*numElems_red*l+24*e+m]=2*U_red[edofMat_e_red[24*e+m]];//FE_load[m];
      }

      }
      }

  }//end element loop

  free(dR_dwe_q1); free(dR_dwe_q2); free(dR_dwe_q3); free(dR_dwe_q4); free(dR_dwe_q5); free(dR_dwe_q6); free(dR_dwe_q7); free(dR_dwe_q8);
  free(dH_due_q1); free(dH_due_q2); free(dH_due_q3); free(dH_due_q4); free(dH_due_q5); free(dH_due_q6); free(dH_due_q7); free(dH_due_q8);
  free(dH_due_q1_t); free(dH_due_q2_t); free(dH_due_q3_t); free(dH_due_q4_t); free(dH_due_q5_t); free(dH_due_q6_t); free(dH_due_q7_t); free(dH_due_q8_t);
  free(dn_ds_q1); free(dn_ds_q2); free(dn_ds_q3); free(dn_ds_q4); free(dn_ds_q5); free(dn_ds_q6); free(dn_ds_q7); free(dn_ds_q8);
  free(dn_ds_q1_t); free(dn_ds_q2_t); free(dn_ds_q3_t); free(dn_ds_q4_t); free(dn_ds_q5_t); free(dn_ds_q6_t); free(dn_ds_q7_t); free(dn_ds_q8_t);
  free(vm_q1); free(vm_q2); free(vm_q3); free(vm_q4); free(vm_q5); free(vm_q6); free(vm_q7); free(vm_q8);
  free(r2_q1); free(r2_q2); free(r2_q3); free(r2_q4); free(r2_q5); free(r2_q6); free(r2_q7); free(r2_q8);
  free(Ke2); free(dvm_dwe_short);
}

void adjoint_rhs(int *edofMat_e_red,int qpoints, double delta_x, double delta_y, double delta_z, int l, int load1, int loadsteps1, int loadsteps, double nodal_force,
  int numElems, int numElems_red,int numNodes_red,
  double penal, double hard_constant, double yield_stress, double *yphys_red_l, double *DEV_mat, double *C1, double G, double *rho_red,
  double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8,
double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t, double *BE5_t, double *BE6_t, double *BE7_t, double *BE8_t,
double weight1, double weight2, double weight3, double weight4, double weight5, double weight6, double weight7, double weight8,
double *von_mises_stress, double *stress_xx_e, double *stress_yy_e, double *stress_zz_e, double *stress_xy_e, double *stress_yz_e, double *stress_xz_e,
double pnorm, double *dH_deta_l, bool *plasticity_nodes_l, int *initial_elements_red_l,
double *lambda_red_l,double *rhs, double *rhs_j, bool pnorm_stress, double *dc_dvm, double *dvm_dwe, double *dc_dvm2, bool *old_pstrain, int *map_elements_short)
{
  int e,e1,i,j,l1,l2,q,m;


  double Ke_rhs[MNE*MNE]; double Ke_rhs_t[MNE*MNE];
  double Ke_rhs1_q1[MNE*MNE],Ke_rhs1_q2[MNE*MNE],Ke_rhs1_q3[MNE*MNE],Ke_rhs1_q4[MNE*MNE],Ke_rhs1_q5[MNE*MNE],Ke_rhs1_q6[MNE*MNE],Ke_rhs1_q7[MNE*MNE],Ke_rhs1_q8[MNE*MNE];
  double Ke_rhs2_q1[MNE*MNE],Ke_rhs2_q2[MNE*MNE],Ke_rhs2_q3[MNE*MNE],Ke_rhs2_q4[MNE*MNE],Ke_rhs2_q5[MNE*MNE],Ke_rhs2_q6[MNE*MNE],Ke_rhs2_q7[MNE*MNE],Ke_rhs2_q8[MNE*MNE];
  double Ke_rhs3_q1[MNE*MNE],Ke_rhs3_q2[MNE*MNE],Ke_rhs3_q3[MNE*MNE],Ke_rhs3_q4[MNE*MNE],Ke_rhs3_q5[MNE*MNE],Ke_rhs3_q6[MNE*MNE],Ke_rhs3_q7[MNE*MNE],Ke_rhs3_q8[MNE*MNE];

  double dc_rhs[MNE];
  double dc_rhs1_q1[MNE],dc_rhs1_q2[MNE],dc_rhs1_q3[MNE],dc_rhs1_q4[MNE],dc_rhs1_q5[MNE],dc_rhs1_q6[MNE],dc_rhs1_q7[MNE],dc_rhs1_q8[MNE];
  double dc_rhs2_q1[MNE],dc_rhs2_q2[MNE],dc_rhs2_q3[MNE],dc_rhs2_q4[MNE],dc_rhs2_q5[MNE],dc_rhs2_q6[MNE],dc_rhs2_q7[MNE],dc_rhs2_q8[MNE];
  double dc_rhs3_q1[MNE],dc_rhs3_q2[MNE],dc_rhs3_q3[MNE],dc_rhs3_q4[MNE],dc_rhs3_q5[MNE],dc_rhs3_q6[MNE],dc_rhs3_q7[MNE],dc_rhs3_q8[MNE];

  double *deta_du_q1=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q2=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q3=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q4=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q5=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q6=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q7=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *deta_du_q8=(double *)calloc(24*6*loadsteps,sizeof(double));

  double *dep_du_q1=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q2=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q3=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q4=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q5=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q6=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q7=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dep_du_q8=(double *)calloc(24*6*loadsteps,sizeof(double));

  double *dbeta_du_q1=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q2=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q3=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q4=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q5=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q6=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q7=(double *)calloc(24*6*loadsteps,sizeof(double));
  double *dbeta_du_q8=(double *)calloc(24*6*loadsteps,sizeof(double));

  double *deini_du_q1=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q2=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q3=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q4=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q5=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q6=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q7=(double *)calloc(24*6,sizeof(double));
  double *deini_du_q8=(double *)calloc(24*6,sizeof(double));

  double lambda_e[24]; double r2[24];

  double pnorm2=pnorm-1;

  for (e=0;e<numElems_red;e++){

  double Ge=pow(rho_red[e],penal)*G;
  double hard_constant_e=pow(rho_red[e],penal)*hard_constant;
  double yield_stress_e=pow(rho_red[e],penal)*yield_stress;

  for (i=0;i<MNE*MNE;i++){
    Ke_rhs1_q1[i]=0;   Ke_rhs2_q1[i]=0; Ke_rhs3_q1[i]=0;
    Ke_rhs1_q2[i]=0;   Ke_rhs2_q2[i]=0; Ke_rhs3_q2[i]=0;
    Ke_rhs1_q3[i]=0;   Ke_rhs2_q3[i]=0; Ke_rhs3_q3[i]=0;
    Ke_rhs1_q4[i]=0;   Ke_rhs2_q4[i]=0; Ke_rhs3_q4[i]=0;
    Ke_rhs1_q5[i]=0;   Ke_rhs2_q5[i]=0; Ke_rhs3_q5[i]=0;
    Ke_rhs1_q6[i]=0;   Ke_rhs2_q6[i]=0; Ke_rhs3_q6[i]=0;
    Ke_rhs1_q7[i]=0;   Ke_rhs2_q7[i]=0; Ke_rhs3_q7[i]=0;
    Ke_rhs1_q8[i]=0;   Ke_rhs2_q8[i]=0; Ke_rhs3_q8[i]=0;
  }

  for (i=0;i<MNE;i++){
    dc_rhs1_q1[i]=0;   dc_rhs2_q1[i]=0; dc_rhs3_q1[i]=0;
    dc_rhs1_q2[i]=0;   dc_rhs2_q2[i]=0; dc_rhs3_q2[i]=0;
    dc_rhs1_q3[i]=0;   dc_rhs2_q3[i]=0; dc_rhs3_q3[i]=0;
    dc_rhs1_q4[i]=0;   dc_rhs2_q4[i]=0; dc_rhs3_q4[i]=0;
    dc_rhs1_q5[i]=0;   dc_rhs2_q5[i]=0; dc_rhs3_q5[i]=0;
    dc_rhs1_q6[i]=0;   dc_rhs2_q6[i]=0; dc_rhs3_q6[i]=0;
    dc_rhs1_q7[i]=0;   dc_rhs2_q7[i]=0; dc_rhs3_q7[i]=0;
    dc_rhs1_q8[i]=0;   dc_rhs2_q8[i]=0; dc_rhs3_q8[i]=0;
  }

  for (l1=l+1;l1<loadsteps;l1++){
    e1=map_elements_short[e];
    if(von_mises_stress[l1*numElems+e1]!=0){
    dc_dvm[e]=(double)pow(von_mises_stress[l1*numElems+e1],pnorm2)*dc_dvm2[l1]/numElems_red;
    dvm_dwe[6*e+0]=(double)(2*stress_xx_e[l1*numElems+e1]-stress_yy_e[l1*numElems+e1]-stress_zz_e[l1*numElems+e1])/(2*von_mises_stress[l1*numElems+e1]);
    dvm_dwe[6*e+1]=(double)(2*stress_yy_e[l1*numElems+e1]-stress_xx_e[l1*numElems+e1]-stress_zz_e[l1*numElems+e1])/(2*von_mises_stress[l1*numElems+e1]);
    dvm_dwe[6*e+2]=(double)(2*stress_zz_e[l1*numElems+e1]-stress_xx_e[l1*numElems+e1]-stress_yy_e[l1*numElems+e1])/(2*von_mises_stress[l1*numElems+e1]);
    dvm_dwe[6*e+3]=(double)3*stress_xy_e[l1*numElems+e1]/von_mises_stress[l1*numElems+e1];
    dvm_dwe[6*e+4]=(double)3*stress_yz_e[l1*numElems+e1]/von_mises_stress[l1*numElems+e1];
    dvm_dwe[6*e+5]=(double)3*stress_xz_e[l1*numElems+e1]/von_mises_stress[l1*numElems+e1];
  }
  else{
    dc_dvm[e]=0;
    dvm_dwe[6*e+0]=0;
    dvm_dwe[6*e+1]=0;
    dvm_dwe[6*e+2]=0;
    dvm_dwe[6*e+3]=0;
    dvm_dwe[6*e+4]=0;
    dvm_dwe[6*e+5]=0;
  }

    //Calculation of dep_du, dbeta_du and deta_du
    for (i=0;i<24*6*loadsteps;i++){
    deta_du_q1[i]=0; dep_du_q1[i]=0; dbeta_du_q1[i]=0;
    deta_du_q2[i]=0; dep_du_q2[i]=0; dbeta_du_q2[i]=0;
    deta_du_q3[i]=0; dep_du_q3[i]=0; dbeta_du_q3[i]=0;
    deta_du_q4[i]=0; dep_du_q4[i]=0; dbeta_du_q4[i]=0;
    deta_du_q5[i]=0; dep_du_q5[i]=0; dbeta_du_q5[i]=0;
    deta_du_q6[i]=0; dep_du_q6[i]=0; dbeta_du_q6[i]=0;
    deta_du_q7[i]=0; dep_du_q7[i]=0; dbeta_du_q7[i]=0;
    deta_du_q8[i]=0; dep_du_q8[i]=0; dbeta_du_q8[i]=0;
    }

    for (i=0;i<24*6;i++){
    deini_du_q1[i]=0;
    deini_du_q2[i]=0;
    deini_du_q3[i]=0;
    deini_du_q4[i]=0;
    deini_du_q5[i]=0;
    deini_du_q6[i]=0;
    deini_du_q7[i]=0;
    deini_du_q8[i]=0;
    }

    if(plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+0]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+1]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+2]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+3]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+4]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+5]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+6]==1 ||
       plasticity_nodes_l[qpoints*numElems_red*l1+qpoints*e+7]==1){
  //  if(old_pstrain[l1]==1 && old_pstrain[l+1]==1){

          for(l2=l;l2<=l1;l2++){
            if(l2==l){
              q=0; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE1,deta_du_q1);
              q=1; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE2,deta_du_q2);
              q=2; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE3,deta_du_q3);
              q=3; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE4,deta_du_q4);
              q=4; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE5,deta_du_q5);
              q=5; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE6,deta_du_q6);
              q=6; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE7,deta_du_q7);
              q=7; deta_du_f1(l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE8,deta_du_q8);
            }
            else{
              if(plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+0]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+1]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+2]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+3]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+4]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+5]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+6]==1 ||
                 plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+7]==1){

                   //time=get_time();
              q=0; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q1,dep_du_q1);
              q=1; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q2,dep_du_q2);
              q=2; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q3,dep_du_q3);
              q=3; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q4,dep_du_q4);
              q=4; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q5,dep_du_q5);
              q=5; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q6,dep_du_q6);
              q=6; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q7,dep_du_q7);
              q=7; dep_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q8,dep_du_q8);
                  //printf("time dep_du_f=%f s\n",get_time()-time);

                  //time=get_time();
              q=0; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q1,dbeta_du_q1);
              q=1; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q2,dbeta_du_q2);
              q=2; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q3,dbeta_du_q3);
              q=3; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q4,dbeta_du_q4);
              q=4; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q5,dbeta_du_q5);
              q=5; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q6,dbeta_du_q6);
              q=6; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q7,dbeta_du_q7);
              q=7; dbeta_du_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_du_q8,dbeta_du_q8);
                //printf("time dbeta_du_f=%f s\n",get_time()-time);

                //time=get_time();
              q=0; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q1,dbeta_du_q1,deta_du_q1);
              q=1; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q2,dbeta_du_q2,deta_du_q2);
              q=2; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q3,dbeta_du_q3,deta_du_q3);
              q=3; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q4,dbeta_du_q4,deta_du_q4);
              q=4; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q5,dbeta_du_q5,deta_du_q5);
              q=5; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q6,dbeta_du_q6,deta_du_q6);
              q=6; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q7,dbeta_du_q7,deta_du_q7);
              q=7; deta_du_f2(l2,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_du_q8,dbeta_du_q8,deta_du_q8);
              //printf("time deta_du_f=%f s\n",get_time()-time);
            }
            }
          }//end l2 loop

          //Plastic strain contribution
          q=0; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE1_t,weight1,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q1,Ke_rhs1_q1,dc_rhs1_q1);
          q=1; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE2_t,weight2,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q2,Ke_rhs1_q2,dc_rhs1_q2);
          q=2; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE3_t,weight3,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q3,Ke_rhs1_q3,dc_rhs1_q3);
          q=3; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE4_t,weight4,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q4,Ke_rhs1_q4,dc_rhs1_q4);
          q=4; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE5_t,weight5,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q5,Ke_rhs1_q5,dc_rhs1_q5);
          q=5; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE6_t,weight6,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q6,Ke_rhs1_q6,dc_rhs1_q6);
          q=6; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE7_t,weight7,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q7,Ke_rhs1_q7,dc_rhs1_q7);
          q=7; Ke_rhs_adjoint1(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE8_t,weight8,dvm_dwe,rho_red,penal,yphys_red_l,C1, dep_du_q8,Ke_rhs1_q8,dc_rhs1_q8);


             }//end plasticity loop


          //Shifted stress contribution
            //if(e==0) time=get_time();

          if(load1==loadsteps1-1 ){
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE1,l1,deta_du_q1);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE2,l1,deta_du_q2);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE3,l1,deta_du_q3);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE4,l1,deta_du_q4);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE5,l1,deta_du_q5);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE6,l1,deta_du_q6);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE7,l1,deta_du_q7);
            deta_du_f3(l,initial_elements_red_l,e,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,BE8,l1,deta_du_q8);

          }

          q=0; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE1_t,weight1,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q1,Ke_rhs2_q1,dc_rhs2_q1,plasticity_nodes_l);
          q=1; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE2_t,weight2,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q2,Ke_rhs2_q2,dc_rhs2_q2,plasticity_nodes_l);
          q=2; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE3_t,weight3,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q3,Ke_rhs2_q3,dc_rhs2_q3,plasticity_nodes_l);
          q=3; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE4_t,weight4,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q4,Ke_rhs2_q4,dc_rhs2_q4,plasticity_nodes_l);
          q=4; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE5_t,weight5,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q5,Ke_rhs2_q5,dc_rhs2_q5,plasticity_nodes_l);
          q=5; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE6_t,weight6,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q6,Ke_rhs2_q6,dc_rhs2_q6,plasticity_nodes_l);
          q=6; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE7_t,weight7,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q7,Ke_rhs2_q7,dc_rhs2_q7,plasticity_nodes_l);
          q=7; Ke_rhs_adjoint2(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE8_t,weight8,Ge,hard_constant_e,dvm_dwe,dH_deta_l,deta_du_q8,Ke_rhs2_q8,dc_rhs2_q8,plasticity_nodes_l);


       //Activation strain contribution
       if(load1==loadsteps1-1 ){
         //if(e==0) printf("activation strain : l=%d, l1=%d\n",l,l1);
         //if(e==0) time=get_time();
          deini_du_f(l,e,numElems_red,BE1,initial_elements_red_l,deini_du_q1);
          deini_du_f(l,e,numElems_red,BE2,initial_elements_red_l,deini_du_q2);
          deini_du_f(l,e,numElems_red,BE3,initial_elements_red_l,deini_du_q3);
          deini_du_f(l,e,numElems_red,BE4,initial_elements_red_l,deini_du_q4);
          deini_du_f(l,e,numElems_red,BE5,initial_elements_red_l,deini_du_q5);
          deini_du_f(l,e,numElems_red,BE6,initial_elements_red_l,deini_du_q6);
          deini_du_f(l,e,numElems_red,BE7,initial_elements_red_l,deini_du_q7);
          deini_du_f(l,e,numElems_red,BE8,initial_elements_red_l,deini_du_q8);
         //if(e==0) printf("time deini_du_f=%f s\n",get_time()-time);
       }

       if(deini_du_q1[0]!=0 || deini_du_q2[0]!=0 || deini_du_q3[0]!=0 || deini_du_q4[0]!=0
       || deini_du_q5[0]!=0 || deini_du_q6[0]!=0 || deini_du_q7[0]!=0 || deini_du_q8[0]!=0){
        q=0; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE1_t,weight1,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q1,Ke_rhs3_q1,dc_rhs3_q1);
        q=1; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE2_t,weight2,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q2,Ke_rhs3_q2,dc_rhs3_q2);
        q=2; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE3_t,weight3,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q3,Ke_rhs3_q3,dc_rhs3_q3);
        q=3; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE4_t,weight4,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q4,Ke_rhs3_q4,dc_rhs3_q4);
        q=4; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE5_t,weight5,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q5,Ke_rhs3_q5,dc_rhs3_q5);
        q=5; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE6_t,weight6,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q6,Ke_rhs3_q6,dc_rhs3_q6);
        q=6; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE7_t,weight7,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q7,Ke_rhs3_q7,dc_rhs3_q7);
        q=7; Ke_rhs_adjoint3(l1,loadsteps,numElems_red,delta_x,delta_y,delta_z,q,qpoints,e,BE8_t,weight8,dvm_dwe,rho_red,penal,yphys_red_l,C1,deini_du_q8,Ke_rhs3_q8,dc_rhs3_q8);
       }

       if(Ke_rhs1_q1[0]!=0 || Ke_rhs1_q2[0]!=0 || Ke_rhs1_q3[0]!=0 || Ke_rhs1_q4[0]!=0 || Ke_rhs1_q5[0]!=0 || Ke_rhs1_q6[0]!=0 || Ke_rhs1_q7[0]!=0 || Ke_rhs1_q8[0]!=0 ||
          Ke_rhs2_q1[0]!=0 || Ke_rhs2_q2[0]!=0 || Ke_rhs2_q3[0]!=0 || Ke_rhs2_q4[0]!=0 || Ke_rhs2_q5[0]!=0 || Ke_rhs2_q6[0]!=0 || Ke_rhs2_q7[0]!=0 || Ke_rhs2_q8[0]!=0 ||
          Ke_rhs3_q1[0]!=0 || Ke_rhs3_q2[0]!=0 || Ke_rhs3_q3[0]!=0 || Ke_rhs3_q4[0]!=0 || Ke_rhs3_q5[0]!=0 || Ke_rhs3_q6[0]!=0 || Ke_rhs3_q7[0]!=0 || Ke_rhs3_q8[0]!=0){

       for(i=0;i<MNE;i++){
         for(j=0;j<MNE;j++){
           Ke_rhs[MNE*i+j]=Ke_rhs1_q1[MNE*i+j]+Ke_rhs1_q2[MNE*i+j]+Ke_rhs1_q3[MNE*i+j]+Ke_rhs1_q4[MNE*i+j]+
                           Ke_rhs1_q5[MNE*i+j]+Ke_rhs1_q6[MNE*i+j]+Ke_rhs1_q7[MNE*i+j]+Ke_rhs1_q8[MNE*i+j]+
                           Ke_rhs2_q1[MNE*i+j]+Ke_rhs2_q2[MNE*i+j]+Ke_rhs2_q3[MNE*i+j]+Ke_rhs2_q4[MNE*i+j]+
                           Ke_rhs2_q5[MNE*i+j]+Ke_rhs2_q6[MNE*i+j]+Ke_rhs2_q7[MNE*i+j]+Ke_rhs2_q8[MNE*i+j]+
                           Ke_rhs3_q1[MNE*i+j]+Ke_rhs3_q2[MNE*i+j]+Ke_rhs3_q3[MNE*i+j]+Ke_rhs3_q4[MNE*i+j]+
                           Ke_rhs3_q5[MNE*i+j]+Ke_rhs3_q6[MNE*i+j]+Ke_rhs3_q7[MNE*i+j]+Ke_rhs3_q8[MNE*i+j];

       }
          dc_rhs[i]=dc_rhs1_q1[i]+dc_rhs1_q2[i]+dc_rhs1_q3[i]+dc_rhs1_q4[i]+dc_rhs1_q5[i]+dc_rhs1_q6[i]+dc_rhs1_q7[i]+dc_rhs1_q8[i]+
                    dc_rhs2_q1[i]+dc_rhs2_q2[i]+dc_rhs2_q3[i]+dc_rhs2_q4[i]+dc_rhs2_q5[i]+dc_rhs2_q6[i]+dc_rhs2_q7[i]+dc_rhs2_q8[i]+
                    dc_rhs3_q1[i]+dc_rhs3_q2[i]+dc_rhs3_q3[i]+dc_rhs3_q4[i]+dc_rhs3_q5[i]+dc_rhs3_q6[i]+dc_rhs3_q7[i]+dc_rhs3_q8[i];
     }


    for (m=0;m<24;m++) {
      lambda_e[m]=lambda_red_l[numNodes_red*l1+edofMat_e_red[MNE*e+m]];
      }

      transMat(24,24,Ke_rhs,Ke_rhs_t);
      prodMatVec(24,24,24,Ke_rhs_t,lambda_e,r2);

    for (m=0;m<24;m++)    {
      rhs[24*e+m]=rhs[24*e+m]+r2[m];
      if(nodal_force==0 && pnorm_stress==1) rhs_j[24*e+m]=rhs_j[24*e+m]+dc_dvm[e]*0.125*dc_rhs[m];
    }
   }

  }//end l1 loop
} //end element loop

free(deta_du_q1); free(deta_du_q2); free(deta_du_q3); free(deta_du_q4); free(deta_du_q5); free(deta_du_q6); free(deta_du_q7); free(deta_du_q8);
free(dep_du_q1); free(dep_du_q2); free(dep_du_q3); free(dep_du_q4); free(dep_du_q5); free(dep_du_q6); free(dep_du_q7); free(dep_du_q8);
free(dbeta_du_q1); free(dbeta_du_q2); free(dbeta_du_q3); free(dbeta_du_q4); free(dbeta_du_q5); free(dbeta_du_q6); free(dbeta_du_q7); free(dbeta_du_q8);
free(deini_du_q1); free(deini_du_q2); free(deini_du_q3); free(deini_du_q4); free(deini_du_q5); free(deini_du_q6); free(deini_du_q7); free(deini_du_q8);
}

void x_derivatives(int *edofMat_e_red, int l, int loadsteps, int numNodes_red, int numElems, int numElems_red, int qpoints, double *U_red_l, double *lambda_red_l, int ni, int nj, double *rho_red, double penal,
  double yield_stress, double G, double hard_constant, double *yphys_red_l, double *C1, double *ID_mat, double *DEV_mat, double *inh_strain2_l,
  double *old_plastic_strain_l, double *strain_ini, double delta_x, double delta_y, double delta_z, double weight1, double weight2, double weight3, double weight4,
  double weight5, double weight6, double weight7, double weight8,
 double *BE1, double *BE2, double *BE3, double *BE4, double *BE5, double *BE6, double *BE7, double *BE8,
 double *BE1_t, double *BE2_t, double *BE3_t, double *BE4_t,double *BE5_t, double *BE6_t, double *BE7_t, double *BE8_t, bool *old_pstrain,
bool *plasticity_nodes_l, double *dH_deta_l, double *norm_dev_stress_l, double *n_trial_stress_l,
double *von_mises_stress, double *stress_xx_e, double *stress_yy_e, double *stress_zz_e, double *stress_xy_e, double *stress_yz_e, double *stress_xz_e,
bool pnorm_stress, double dc_dvm1, double pnorm, double *dc_dvm, double *dvm_dwe, double nodal_force, double *dc_dxe,
double *gradient2_l, double *gradient2_map, int *map_elements_short)
{
  int e,e1,m,l2,i,q;
  double somme1, somme2, somme3, somme4, somme5, somme6, somme7, somme8;
  double somme1_dc, somme2_dc, somme3_dc, somme4_dc, somme5_dc, somme6_dc, somme7_dc, somme8_dc;

  double lambda_e[24];
  //double U_e[24];
  double penal1=penal-1;
  double pnorm2=pnorm-1;

  double *dR_dC_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dC_q8=(double *)calloc(3*qpoints,sizeof(double));

  double *dR_dxe_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dxe_q8=(double *)calloc(3*qpoints,sizeof(double));

  double *dR_dS_q1=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q2=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q3=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q4=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q5=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q6=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q7=(double *)calloc(6*3*qpoints,sizeof(double));
  double *dR_dS_q8=(double *)calloc(6*3*qpoints,sizeof(double));

  double *dR_dY_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dY_q8=(double *)calloc(3*qpoints,sizeof(double));

  double *dR_dG_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_dG_q8=(double *)calloc(3*qpoints,sizeof(double));

  double *dR_da_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dR_da_q8=(double *)calloc(3*qpoints,sizeof(double));



//////////////////////////////////////////


  double *dS_dC_q1=(double *)calloc(6,sizeof(double));
  double *dS_dC_q2=(double *)calloc(6,sizeof(double));
  double *dS_dC_q3=(double *)calloc(6,sizeof(double));
  double *dS_dC_q4=(double *)calloc(6,sizeof(double));
  double *dS_dC_q5=(double *)calloc(6,sizeof(double));
  double *dS_dC_q6=(double *)calloc(6,sizeof(double));
  double *dS_dC_q7=(double *)calloc(6,sizeof(double));
  double *dS_dC_q8=(double *)calloc(6,sizeof(double));

  double *dS1_dC_q1=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q2=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q3=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q4=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q5=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q6=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q7=(double *)calloc(6,sizeof(double));
  double *dS1_dC_q8=(double *)calloc(6,sizeof(double));

  double *dS2_dC_q1=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q2=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q3=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q4=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q5=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q6=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q7=(double *)calloc(6,sizeof(double));
  double *dS2_dC_q8=(double *)calloc(6,sizeof(double));

  double *dS_dY_q1=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q2=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q3=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q4=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q5=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q6=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q7=(double *)calloc(3*qpoints,sizeof(double));
  double *dS_dY_q8=(double *)calloc(3*qpoints,sizeof(double));

  double *dS1_dY_q1=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q2=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q3=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q4=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q5=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q6=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q7=(double *)calloc(6,sizeof(double));
  double *dS1_dY_q8=(double *)calloc(6,sizeof(double));

  double *dS2_dY_q1=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q2=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q3=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q4=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q5=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q6=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q7=(double *)calloc(6,sizeof(double));
  double *dS2_dY_q8=(double *)calloc(6,sizeof(double));

  double *dS3_dY_q1=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q2=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q3=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q4=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q5=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q6=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q7=(double *)calloc(6,sizeof(double));
  double *dS3_dY_q8=(double *)calloc(6,sizeof(double));

  double *dS_dG_q1=(double *)calloc(6,sizeof(double));
  double *dS_dG_q2=(double *)calloc(6,sizeof(double));
  double *dS_dG_q3=(double *)calloc(6,sizeof(double));
  double *dS_dG_q4=(double *)calloc(6,sizeof(double));
  double *dS_dG_q5=(double *)calloc(6,sizeof(double));
  double *dS_dG_q6=(double *)calloc(6,sizeof(double));
  double *dS_dG_q7=(double *)calloc(6,sizeof(double));
  double *dS_dG_q8=(double *)calloc(6,sizeof(double));

  double *dS1_dG_q1=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q2=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q3=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q4=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q5=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q6=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q7=(double *)calloc(6,sizeof(double));
  double *dS1_dG_q8=(double *)calloc(6,sizeof(double));

  double *dS2_dG_q1=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q2=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q3=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q4=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q5=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q6=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q7=(double *)calloc(6,sizeof(double));
  double *dS2_dG_q8=(double *)calloc(6,sizeof(double));

  double *dS3_dG_q1=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q2=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q3=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q4=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q5=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q6=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q7=(double *)calloc(6,sizeof(double));
  double *dS3_dG_q8=(double *)calloc(6,sizeof(double));

  double *dS_da_q1=(double *)calloc(6,sizeof(double));
  double *dS_da_q2=(double *)calloc(6,sizeof(double));
  double *dS_da_q3=(double *)calloc(6,sizeof(double));
  double *dS_da_q4=(double *)calloc(6,sizeof(double));
  double *dS_da_q5=(double *)calloc(6,sizeof(double));
  double *dS_da_q6=(double *)calloc(6,sizeof(double));
  double *dS_da_q7=(double *)calloc(6,sizeof(double));
  double *dS_da_q8=(double *)calloc(6,sizeof(double));

  double *dS1_da_q1=(double *)calloc(6,sizeof(double));
  double *dS1_da_q2=(double *)calloc(6,sizeof(double));
  double *dS1_da_q3=(double *)calloc(6,sizeof(double));
  double *dS1_da_q4=(double *)calloc(6,sizeof(double));
  double *dS1_da_q5=(double *)calloc(6,sizeof(double));
  double *dS1_da_q6=(double *)calloc(6,sizeof(double));
  double *dS1_da_q7=(double *)calloc(6,sizeof(double));
  double *dS1_da_q8=(double *)calloc(6,sizeof(double));

  double *dS2_da_q1=(double *)calloc(6,sizeof(double));
  double *dS2_da_q2=(double *)calloc(6,sizeof(double));
  double *dS2_da_q3=(double *)calloc(6,sizeof(double));
  double *dS2_da_q4=(double *)calloc(6,sizeof(double));
  double *dS2_da_q5=(double *)calloc(6,sizeof(double));
  double *dS2_da_q6=(double *)calloc(6,sizeof(double));
  double *dS2_da_q7=(double *)calloc(6,sizeof(double));
  double *dS2_da_q8=(double *)calloc(6,sizeof(double));

  double *dS3_da_q1=(double *)calloc(6,sizeof(double));
  double *dS3_da_q2=(double *)calloc(6,sizeof(double));
  double *dS3_da_q3=(double *)calloc(6,sizeof(double));
  double *dS3_da_q4=(double *)calloc(6,sizeof(double));
  double *dS3_da_q5=(double *)calloc(6,sizeof(double));
  double *dS3_da_q6=(double *)calloc(6,sizeof(double));
  double *dS3_da_q7=(double *)calloc(6,sizeof(double));
  double *dS3_da_q8=(double *)calloc(6,sizeof(double));


  ////////////////////////////////////////////////

  //C
  double *deta_dC_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dC_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dep_dC_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dC_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dbeta_dC_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dC_q8=(double *)calloc(6*loadsteps,sizeof(double));

  //Y
  double *deta_dY_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dY_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dep_dY_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dY_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dbeta_dY_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dY_q8=(double *)calloc(6*loadsteps,sizeof(double));

  //G
  double *deta_dG_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_dG_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dep_dG_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_dG_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dbeta_dG_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_dG_q8=(double *)calloc(6*loadsteps,sizeof(double));

  //a
  double *deta_da_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *deta_da_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dep_da_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dep_da_q8=(double *)calloc(6*loadsteps,sizeof(double));

  double *dbeta_da_q1=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q2=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q3=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q4=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q5=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q6=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q7=(double *)calloc(6*loadsteps,sizeof(double));
  double *dbeta_da_q8=(double *)calloc(6*loadsteps,sizeof(double));


for(e=0;e<numElems_red;e++){

  double Ge=pow(rho_red[e],penal)*G;
  double dGe=penal*pow(rho_red[e],penal1)*G;
  double yield_stress_e=pow(rho_red[e],penal)*yield_stress;
  double d_yield_stress_e=penal*pow(rho_red[e],penal1)*yield_stress;
  double hard_constant_e=pow(rho_red[e],penal)*hard_constant;
  double d_hard_constant_e=penal*pow(rho_red[e],penal1)*hard_constant;


  for (m=0;m<24;m++) {
    lambda_e[m]=lambda_red_l[numNodes_red*l+edofMat_e_red[MNE*e+m]];
    //U_e[m]=U_l[numNodes*l+edofMat[m]];
    //if(e==0 || e==200) printf("l=%d, e=%d lambda_e=%lg \n",l,e,lambda_e[m]);
    }

    for(i=0;i<144;i++){
      dR_dS_q1[i]=-BE1_t[i]*weight1*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q2[i]=-BE2_t[i]*weight2*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q3[i]=-BE3_t[i]*weight3*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q4[i]=-BE4_t[i]*weight4*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q5[i]=-BE5_t[i]*weight5*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q6[i]=-BE6_t[i]*weight6*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q7[i]=-BE7_t[i]*weight7*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      dR_dS_q8[i]=-BE8_t[i]*weight8*0.5*delta_x*0.5*delta_y*0.5*delta_z;
      //printf("dR_dS_q1=%lg\n",dR_dS_q1[i]);
    }

    for(i=0;i<6;i++) {
    dS2_dC_q1[i]=0;  dS2_dY_q1[i]=0; dS3_dY_q1[i]=0; dS2_dG_q1[i]=0; dS3_dG_q1[i]=0; dS2_da_q1[i]=0; dS3_da_q1[i]=0;
    dS2_dC_q2[i]=0;  dS2_dY_q2[i]=0; dS3_dY_q2[i]=0; dS2_dG_q2[i]=0; dS3_dG_q2[i]=0; dS2_da_q2[i]=0; dS3_da_q2[i]=0;
    dS2_dC_q3[i]=0;  dS2_dY_q3[i]=0; dS3_dY_q3[i]=0; dS2_dG_q3[i]=0; dS3_dG_q3[i]=0; dS2_da_q3[i]=0; dS3_da_q3[i]=0;
    dS2_dC_q4[i]=0;  dS2_dY_q4[i]=0; dS3_dY_q4[i]=0; dS2_dG_q4[i]=0; dS3_dG_q4[i]=0; dS2_da_q4[i]=0; dS3_da_q4[i]=0;
    dS2_dC_q5[i]=0;  dS2_dY_q5[i]=0; dS3_dY_q5[i]=0; dS2_dG_q5[i]=0; dS3_dG_q5[i]=0; dS2_da_q5[i]=0; dS3_da_q5[i]=0;
    dS2_dC_q6[i]=0;  dS2_dY_q6[i]=0; dS3_dY_q6[i]=0; dS2_dG_q6[i]=0; dS3_dG_q6[i]=0; dS2_da_q6[i]=0; dS3_da_q6[i]=0;
    dS2_dC_q7[i]=0;  dS2_dY_q7[i]=0; dS3_dY_q7[i]=0; dS2_dG_q7[i]=0; dS3_dG_q7[i]=0; dS2_da_q7[i]=0; dS3_da_q7[i]=0;
    dS2_dC_q8[i]=0;  dS2_dY_q8[i]=0; dS3_dY_q8[i]=0; dS2_dG_q8[i]=0; dS3_dG_q8[i]=0; dS2_da_q8[i]=0; dS3_da_q8[i]=0;
    }

    //Derivative with respect to C
    for (i=0;i<6*loadsteps;i++){
    deta_dC_q1[i]=0; dep_dC_q1[i]=0; dbeta_dC_q1[i]=0;
    deta_dC_q2[i]=0; dep_dC_q2[i]=0; dbeta_dC_q2[i]=0;
    deta_dC_q3[i]=0; dep_dC_q3[i]=0; dbeta_dC_q3[i]=0;
    deta_dC_q4[i]=0; dep_dC_q4[i]=0; dbeta_dC_q4[i]=0;
    deta_dC_q5[i]=0; dep_dC_q5[i]=0; dbeta_dC_q5[i]=0;
    deta_dC_q6[i]=0; dep_dC_q6[i]=0; dbeta_dC_q6[i]=0;
    deta_dC_q7[i]=0; dep_dC_q7[i]=0; dbeta_dC_q7[i]=0;
    deta_dC_q8[i]=0; dep_dC_q8[i]=0; dbeta_dC_q8[i]=0;
  }


  for(l2=0;l2<=l;l2++){
    q=0; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE1,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q1);
    q=1; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE2,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q2);
    q=2; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE3,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q3);
    q=3; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE4,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q4);
    q=4; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE5,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q5);
    q=5; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE6,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q6);
    q=6; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE7,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q7);
    q=7; deta_dC_f1(edofMat_e_red,ni,nj,l2,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,DEV_mat,BE8,U_red_l,inh_strain2_l,old_plastic_strain_l,strain_ini,deta_dC_q8);

      if(old_pstrain[l2]==1){
      if(plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+0]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+1]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+2]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+3]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+4]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+5]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+6]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+7]==1){


    q=0; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q1,dep_dC_q1);
    q=1; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q2,dep_dC_q2);
    q=2; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q3,dep_dC_q3);
    q=3; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q4,dep_dC_q4);
    q=4; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q5,dep_dC_q5);
    q=5; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q6,dep_dC_q6);
    q=6; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q7,dep_dC_q7);
    q=7; dep_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q8,dep_dC_q8);

    q=0; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q1,dbeta_dC_q1);
    q=1; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q2,dbeta_dC_q2);
    q=2; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q3,dbeta_dC_q3);
    q=3; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q4,dbeta_dC_q4);
    q=4; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q5,dbeta_dC_q5);
    q=5; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q6,dbeta_dC_q6);
    q=6; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q7,dbeta_dC_q7);
    q=7; dbeta_dC_f(q,qpoints,e,numElems_red,l2,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q8,dbeta_dC_q8);

    q=0; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q1,dbeta_dC_q1,deta_dC_q1);
    q=1; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q2,dbeta_dC_q2,deta_dC_q2);
    q=2; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q3,dbeta_dC_q3,deta_dC_q3);
    q=3; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q4,dbeta_dC_q4,deta_dC_q4);
    q=4; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q5,dbeta_dC_q5,deta_dC_q5);
    q=5; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q6,dbeta_dC_q6,deta_dC_q6);
    q=6; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q7,dbeta_dC_q7,deta_dC_q7);
    q=7; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dC_q8,dbeta_dC_q8,deta_dC_q8);

  }
}

  }//end l2 loop

  q=0; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE1,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q1,dS1_dC_q1);
  q=1; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE2,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q2,dS1_dC_q2);
  q=2; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE3,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q3,dS1_dC_q3);
  q=3; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE4,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q4,dS1_dC_q4);
  q=4; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE5,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q5,dS1_dC_q5);
  q=5; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE6,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q6,dS1_dC_q6);
  q=6; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE7,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q7,dS1_dC_q7);
  q=7; dS_dC_f1_bis(edofMat_e_red,l,numElems_red,numNodes_red,q,qpoints,e,BE8,U_red_l,rho_red,penal,yphys_red_l,C1,inh_strain2_l,old_plastic_strain_l,strain_ini,ID_mat,dep_dC_q8,dS1_dC_q8);

  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q1,dS2_dC_q1);
  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q2,dS2_dC_q2);
  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q3,dS2_dC_q3);
  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q4,dS2_dC_q4);
  q=4; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q5,dS2_dC_q5);
  q=5; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q6,dS2_dC_q6);
  q=6; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q7,dS2_dC_q7);
  q=7; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dC_q8,dS2_dC_q8);

    for (m=0;m<6;m++) {
        dS_dC_q1[m]=dS1_dC_q1[m]+dS2_dC_q1[m];
        dS_dC_q2[m]=dS1_dC_q2[m]+dS2_dC_q2[m];
        dS_dC_q3[m]=dS1_dC_q3[m]+dS2_dC_q3[m];
        dS_dC_q4[m]=dS1_dC_q4[m]+dS2_dC_q4[m];
        dS_dC_q5[m]=dS1_dC_q5[m]+dS2_dC_q5[m];
        dS_dC_q6[m]=dS1_dC_q6[m]+dS2_dC_q6[m];
        dS_dC_q7[m]=dS1_dC_q7[m]+dS2_dC_q7[m];
        dS_dC_q8[m]=dS1_dC_q8[m]+dS2_dC_q8[m];
                  }

  prodMatVec(6,24,6,dR_dS_q1,dS_dC_q1,dR_dC_q1);
  prodMatVec(6,24,6,dR_dS_q2,dS_dC_q2,dR_dC_q2);
  prodMatVec(6,24,6,dR_dS_q3,dS_dC_q3,dR_dC_q3);
  prodMatVec(6,24,6,dR_dS_q4,dS_dC_q4,dR_dC_q4);
  prodMatVec(6,24,6,dR_dS_q5,dS_dC_q5,dR_dC_q5);
  prodMatVec(6,24,6,dR_dS_q6,dS_dC_q6,dR_dC_q6);
  prodMatVec(6,24,6,dR_dS_q7,dS_dC_q7,dR_dC_q7);
  prodMatVec(6,24,6,dR_dS_q8,dS_dC_q8,dR_dC_q8);

    //Derivative with respect to Y,G,a

  for (i=0;i<6*loadsteps;i++){
      //yield stress
      deta_dY_q1[i]=0; dep_dY_q1[i]=0; dbeta_dY_q1[i]=0;
      deta_dY_q2[i]=0; dep_dY_q2[i]=0; dbeta_dY_q2[i]=0;
      deta_dY_q3[i]=0; dep_dY_q3[i]=0; dbeta_dY_q3[i]=0;
      deta_dY_q4[i]=0; dep_dY_q4[i]=0; dbeta_dY_q4[i]=0;
      deta_dY_q5[i]=0; dep_dY_q5[i]=0; dbeta_dY_q5[i]=0;
      deta_dY_q6[i]=0; dep_dY_q6[i]=0; dbeta_dY_q6[i]=0;
      deta_dY_q7[i]=0; dep_dY_q7[i]=0; dbeta_dY_q7[i]=0;
      deta_dY_q8[i]=0; dep_dY_q8[i]=0; dbeta_dY_q8[i]=0;

      //shear modulus
      deta_dG_q1[i]=0; dep_dG_q1[i]=0; dbeta_dG_q1[i]=0;
      deta_dG_q2[i]=0; dep_dG_q2[i]=0; dbeta_dG_q2[i]=0;
      deta_dG_q3[i]=0; dep_dG_q3[i]=0; dbeta_dG_q3[i]=0;
      deta_dG_q4[i]=0; dep_dG_q4[i]=0; dbeta_dG_q4[i]=0;
      deta_dG_q5[i]=0; dep_dG_q5[i]=0; dbeta_dG_q5[i]=0;
      deta_dG_q6[i]=0; dep_dG_q6[i]=0; dbeta_dG_q6[i]=0;
      deta_dG_q7[i]=0; dep_dG_q7[i]=0; dbeta_dG_q7[i]=0;
      deta_dG_q8[i]=0; dep_dG_q8[i]=0; dbeta_dG_q8[i]=0;

      //hardening constant
      deta_da_q1[i]=0; dep_da_q1[i]=0; dbeta_da_q1[i]=0;
      deta_da_q2[i]=0; dep_da_q2[i]=0; dbeta_da_q2[i]=0;
      deta_da_q3[i]=0; dep_da_q3[i]=0; dbeta_da_q3[i]=0;
      deta_da_q4[i]=0; dep_da_q4[i]=0; dbeta_da_q4[i]=0;
      deta_da_q5[i]=0; dep_da_q5[i]=0; dbeta_da_q5[i]=0;
      deta_da_q6[i]=0; dep_da_q6[i]=0; dbeta_da_q6[i]=0;
      deta_da_q7[i]=0; dep_da_q7[i]=0; dbeta_da_q7[i]=0;
      deta_da_q8[i]=0; dep_da_q8[i]=0; dbeta_da_q8[i]=0;
    }

    for(l2=0;l2<=l;l2++){

      if(old_pstrain[l2]==1){
      if(plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+0]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+1]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+2]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+3]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+4]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+5]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+6]==1 ||
         plasticity_nodes_l[qpoints*numElems_red*(l2-1)+qpoints*e+7]==1){

        //yield stress
      //  if(e==0) printf("yield stress\n");
        q=0; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q1,dep_dY_q1,plasticity_nodes_l);
        q=1; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q2,dep_dY_q2,plasticity_nodes_l);
        q=2; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q3,dep_dY_q3,plasticity_nodes_l);
        q=3; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q4,dep_dY_q4,plasticity_nodes_l);
        q=4; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q5,dep_dY_q5,plasticity_nodes_l);
        q=5; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q6,dep_dY_q6,plasticity_nodes_l);
        q=6; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q7,dep_dY_q7,plasticity_nodes_l);
        q=7; dep_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q8,dep_dY_q8,plasticity_nodes_l);

        q=0; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q1,dbeta_dY_q1,plasticity_nodes_l);
        q=1; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q2,dbeta_dY_q2,plasticity_nodes_l);
        q=2; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q3,dbeta_dY_q3,plasticity_nodes_l);
        q=3; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q4,dbeta_dY_q4,plasticity_nodes_l);
        q=4; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q5,dbeta_dY_q5,plasticity_nodes_l);
        q=5; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q6,dbeta_dY_q6,plasticity_nodes_l);
        q=6; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q7,dbeta_dY_q7,plasticity_nodes_l);
        q=7; dbeta_dY_f(l2,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dH_deta_l,deta_dY_q8,dbeta_dY_q8,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q1,dbeta_dY_q1,deta_dY_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q2,dbeta_dY_q2,deta_dY_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q3,dbeta_dY_q3,deta_dY_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q4,dbeta_dY_q4,deta_dY_q4);
        q=4; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q5,dbeta_dY_q5,deta_dY_q5);
        q=5; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q6,dbeta_dY_q6,deta_dY_q6);
        q=6; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q7,dbeta_dY_q7,deta_dY_q7);
        q=7; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dY_q8,dbeta_dY_q8,deta_dY_q8);

        //shear modulus
        //if(e==0) printf("shear modulus\n");
        q=0; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q1,dep_dG_q1,plasticity_nodes_l);
        q=1; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q2,dep_dG_q2,plasticity_nodes_l);
        q=2; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q3,dep_dG_q3,plasticity_nodes_l);
        q=3; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q4,dep_dG_q4,plasticity_nodes_l);
        q=4; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q5,dep_dG_q5,plasticity_nodes_l);
        q=5; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q6,dep_dG_q6,plasticity_nodes_l);
        q=6; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q7,dep_dG_q7,plasticity_nodes_l);
        q=7; dep_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q8,dep_dG_q8,plasticity_nodes_l);

        q=0; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q1,dbeta_dG_q1,plasticity_nodes_l);
        q=1; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q2,dbeta_dG_q2,plasticity_nodes_l);
        q=2; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q3,dbeta_dG_q3,plasticity_nodes_l);
        q=3; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q4,dbeta_dG_q4,plasticity_nodes_l);
        q=4; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q5,dbeta_dG_q5,plasticity_nodes_l);
        q=5; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q6,dbeta_dG_q6,plasticity_nodes_l);
        q=6; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q7,dbeta_dG_q7,plasticity_nodes_l);
        q=7; dbeta_dG_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_dG_q8,dbeta_dG_q8,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q1,dbeta_dG_q1,deta_dG_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q2,dbeta_dG_q2,deta_dG_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q3,dbeta_dG_q3,deta_dG_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q4,dbeta_dG_q4,deta_dG_q4);
        q=4; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q5,dbeta_dG_q5,deta_dG_q5);
        q=5; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q6,dbeta_dG_q6,deta_dG_q6);
        q=6; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q7,dbeta_dG_q7,deta_dG_q7);
        q=7; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_dG_q8,dbeta_dG_q8,deta_dG_q8);

        //hardening constant
        //if(e==0) printf("hardening constant\n");
        q=0; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q1,dep_da_q1,plasticity_nodes_l);
        q=1; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q2,dep_da_q2,plasticity_nodes_l);
        q=2; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q3,dep_da_q3,plasticity_nodes_l);
        q=3; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q4,dep_da_q4,plasticity_nodes_l);
        q=4; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q5,dep_da_q5,plasticity_nodes_l);
        q=5; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q6,dep_da_q6,plasticity_nodes_l);
        q=6; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q7,dep_da_q7,plasticity_nodes_l);
        q=7; dep_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q8,dep_da_q8,plasticity_nodes_l);

        q=0; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q1,dbeta_da_q1,plasticity_nodes_l);
        q=1; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q2,dbeta_da_q2,plasticity_nodes_l);
        q=2; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q3,dbeta_da_q3,plasticity_nodes_l);
        q=3; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q4,dbeta_da_q4,plasticity_nodes_l);
        q=4; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q5,dbeta_da_q5,plasticity_nodes_l);
        q=5; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q6,dbeta_da_q6,plasticity_nodes_l);
        q=6; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q7,dbeta_da_q7,plasticity_nodes_l);
        q=7; dbeta_da_f(l2,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,yield_stress_e,hard_constant_e,dH_deta_l,deta_da_q8,dbeta_da_q8,plasticity_nodes_l);

        q=0; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q1,dbeta_da_q1,deta_da_q1);
        q=1; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q2,dbeta_da_q2,deta_da_q2);
        q=2; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q3,dbeta_da_q3,deta_da_q3);
        q=3; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q4,dbeta_da_q4,deta_da_q4);
        q=4; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q5,dbeta_da_q5,deta_da_q5);
        q=5; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q6,dbeta_da_q6,deta_da_q6);
        q=6; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q7,dbeta_da_q7,deta_da_q7);
        q=7; deta_dCYGa_f2(l2,e,q,numElems_red,rho_red,penal,yphys_red_l,C1,DEV_mat,dep_da_q8,dbeta_da_q8,deta_da_q8);
      }
    }
  }//end l2 loop

  ///////////////////// Y /////////////////////////////
  //S
  q=0; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q1,plasticity_nodes_l);
  q=1; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q2,plasticity_nodes_l);
  q=2; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q3,plasticity_nodes_l);
  q=3; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q4,plasticity_nodes_l);
  q=4; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q5,plasticity_nodes_l);
  q=5; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q6,plasticity_nodes_l);
  q=6; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q7,plasticity_nodes_l);
  q=7; dS_dY_f1(l,numElems_red,q,qpoints,e,n_trial_stress_l,Ge,hard_constant_e,dS1_dY_q8,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q1,dS2_dY_q1);
  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q2,dS2_dY_q2);
  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q3,dS2_dY_q3);
  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q4,dS2_dY_q4);
  q=4; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q5,dS2_dY_q5);
  q=5; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q6,dS2_dY_q6);
  q=6; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q7,dS2_dY_q7);
  q=7; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dY_q8,dS2_dY_q8);

  //ds_tr
  q=0; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q1,dS3_dY_q1);
  q=1; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q2,dS3_dY_q2);
  q=2; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q3,dS3_dY_q3);
  q=3; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q4,dS3_dY_q4);
  q=4; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q5,dS3_dY_q5);
  q=5; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q6,dS3_dY_q6);
  q=6; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q7,dS3_dY_q7);
  q=7; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dY_q8,dS3_dY_q8);


  ///////////////////// G /////////////////////////////

  //S
  q=0; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q1,plasticity_nodes_l);
  q=1; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q2,plasticity_nodes_l);
  q=2; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q3,plasticity_nodes_l);
  q=3; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q4,plasticity_nodes_l);
  q=4; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q5,plasticity_nodes_l);
  q=5; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q6,plasticity_nodes_l);
  q=6; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q7,plasticity_nodes_l);
  q=7; dS_dG_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_dG_q8,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q1,dS2_dG_q1);
  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q2,dS2_dG_q2);
  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q3,dS2_dG_q3);
  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q4,dS2_dG_q4);
  q=4; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q5,dS2_dG_q5);
  q=5; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q6,dS2_dG_q6);
  q=6; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q7,dS2_dG_q7);
  q=7; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_dG_q8,dS2_dG_q8);

  //ds_tr
  q=0; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q1,dS3_dG_q1);
  q=1; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q2,dS3_dG_q2);
  q=2; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q3,dS3_dG_q3);
  q=3; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q4,dS3_dG_q4);
  q=4; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q5,dS3_dG_q5);
  q=5; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q6,dS3_dG_q6);
  q=6; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q7,dS3_dG_q7);
  q=7; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_dG_q8,dS3_dG_q8);

  ///////////////////// a /////////////////////////////

  //S
  q=0; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q1,plasticity_nodes_l);
  q=1; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q2,plasticity_nodes_l);
  q=2; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q3,plasticity_nodes_l);
  q=3; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q4,plasticity_nodes_l);
  q=4; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q5,plasticity_nodes_l);
  q=5; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q6,plasticity_nodes_l);
  q=6; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q7,plasticity_nodes_l);
  q=7; dS_da_f1(l,numElems_red,q,qpoints,e,norm_dev_stress_l,n_trial_stress_l,Ge,hard_constant_e,yield_stress_e,dS1_da_q8,plasticity_nodes_l);

  //eta
  q=0; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q1,dS2_da_q1);
  q=1; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q2,dS2_da_q2);
  q=2; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q3,dS2_da_q3);
  q=3; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q4,dS2_da_q4);
  q=4; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q5,dS2_da_q5);
  q=5; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q6,dS2_da_q6);
  q=6; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q7,dS2_da_q7);
  q=7; dS_dCYGa_f2_bis(l,loadsteps,numElems_red,q,qpoints,e,Ge,hard_constant_e,plasticity_nodes_l,dH_deta_l,deta_da_q8,dS2_da_q8);


  //ds_tr
  q=0; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q1,dS3_da_q1);
  q=1; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q2,dS3_da_q2);
  q=2; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q3,dS3_da_q3);
  q=3; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q4,dS3_da_q4);
  q=4; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q5,dS3_da_q5);
  q=5; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q6,dS3_da_q6);
  q=6; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q7,dS3_da_q7);
  q=7; dS_dYGa_f3_bis(l,numElems_red,numNodes_red,q,qpoints,e,rho_red,penal,yphys_red_l,C1,ID_mat,dep_da_q8,dS3_da_q8);

  //////////////////////////////////////////////////////////////////////

  for (m=0;m<6;m++) {
      dS_dY_q1[m]=dS1_dY_q1[m]+dS2_dY_q1[m]+dS3_dY_q1[m];
      dS_dY_q2[m]=dS1_dY_q2[m]+dS2_dY_q2[m]+dS3_dY_q2[m];
      dS_dY_q3[m]=dS1_dY_q3[m]+dS2_dY_q3[m]+dS3_dY_q3[m];
      dS_dY_q4[m]=dS1_dY_q4[m]+dS2_dY_q4[m]+dS3_dY_q4[m];
      dS_dY_q5[m]=dS1_dY_q5[m]+dS2_dY_q5[m]+dS3_dY_q5[m];
      dS_dY_q6[m]=dS1_dY_q6[m]+dS2_dY_q6[m]+dS3_dY_q6[m];
      dS_dY_q7[m]=dS1_dY_q7[m]+dS2_dY_q7[m]+dS3_dY_q7[m];
      dS_dY_q8[m]=dS1_dY_q8[m]+dS2_dY_q8[m]+dS3_dY_q8[m];

      dS_dG_q1[m]=dS1_dG_q1[m]+dS2_dG_q1[m]+dS3_dG_q1[m];
      dS_dG_q2[m]=dS1_dG_q2[m]+dS2_dG_q2[m]+dS3_dG_q2[m];
      dS_dG_q3[m]=dS1_dG_q3[m]+dS2_dG_q3[m]+dS3_dG_q3[m];
      dS_dG_q4[m]=dS1_dG_q4[m]+dS2_dG_q4[m]+dS3_dG_q4[m];
      dS_dG_q5[m]=dS1_dG_q5[m]+dS2_dG_q5[m]+dS3_dG_q5[m];
      dS_dG_q6[m]=dS1_dG_q6[m]+dS2_dG_q6[m]+dS3_dG_q6[m];
      dS_dG_q7[m]=dS1_dG_q7[m]+dS2_dG_q7[m]+dS3_dG_q7[m];
      dS_dG_q8[m]=dS1_dG_q8[m]+dS2_dG_q8[m]+dS3_dG_q8[m];

      dS_da_q1[m]=dS1_da_q1[m]+dS2_da_q1[m]+dS3_da_q1[m];
      dS_da_q2[m]=dS1_da_q2[m]+dS2_da_q2[m]+dS3_da_q2[m];
      dS_da_q3[m]=dS1_da_q3[m]+dS2_da_q3[m]+dS3_da_q3[m];
      dS_da_q4[m]=dS1_da_q4[m]+dS2_da_q4[m]+dS3_da_q4[m];
      dS_da_q5[m]=dS1_da_q5[m]+dS2_da_q5[m]+dS3_da_q5[m];
      dS_da_q6[m]=dS1_da_q6[m]+dS2_da_q6[m]+dS3_da_q6[m];
      dS_da_q7[m]=dS1_da_q7[m]+dS2_da_q7[m]+dS3_da_q7[m];
      dS_da_q8[m]=dS1_da_q8[m]+dS2_da_q8[m]+dS3_da_q8[m];

      }

prodMatVec(6,24,6,dR_dS_q1,dS_dY_q1,dR_dY_q1);
prodMatVec(6,24,6,dR_dS_q2,dS_dY_q2,dR_dY_q2);
prodMatVec(6,24,6,dR_dS_q3,dS_dY_q3,dR_dY_q3);
prodMatVec(6,24,6,dR_dS_q4,dS_dY_q4,dR_dY_q4);
prodMatVec(6,24,6,dR_dS_q5,dS_dY_q5,dR_dY_q5);
prodMatVec(6,24,6,dR_dS_q6,dS_dY_q6,dR_dY_q6);
prodMatVec(6,24,6,dR_dS_q7,dS_dY_q7,dR_dY_q7);
prodMatVec(6,24,6,dR_dS_q8,dS_dY_q8,dR_dY_q8);

prodMatVec(6,24,6,dR_dS_q1,dS_dG_q1,dR_dG_q1);
prodMatVec(6,24,6,dR_dS_q2,dS_dG_q2,dR_dG_q2);
prodMatVec(6,24,6,dR_dS_q3,dS_dG_q3,dR_dG_q3);
prodMatVec(6,24,6,dR_dS_q4,dS_dG_q4,dR_dG_q4);
prodMatVec(6,24,6,dR_dS_q5,dS_dG_q5,dR_dG_q5);
prodMatVec(6,24,6,dR_dS_q6,dS_dG_q6,dR_dG_q6);
prodMatVec(6,24,6,dR_dS_q7,dS_dG_q7,dR_dG_q7);
prodMatVec(6,24,6,dR_dS_q8,dS_dG_q8,dR_dG_q8);

prodMatVec(6,24,6,dR_dS_q1,dS_da_q1,dR_da_q1);
prodMatVec(6,24,6,dR_dS_q2,dS_da_q2,dR_da_q2);
prodMatVec(6,24,6,dR_dS_q3,dS_da_q3,dR_da_q3);
prodMatVec(6,24,6,dR_dS_q4,dS_da_q4,dR_da_q4);
prodMatVec(6,24,6,dR_dS_q5,dS_da_q5,dR_da_q5);
prodMatVec(6,24,6,dR_dS_q6,dS_da_q6,dR_da_q6);
prodMatVec(6,24,6,dR_dS_q7,dS_da_q7,dR_da_q7);
prodMatVec(6,24,6,dR_dS_q8,dS_da_q8,dR_da_q8);

//////////////////////////////////////////
for (m=0;m<24;m++) {

  dR_dY_q1[m]=dR_dY_q1[m]*d_yield_stress_e;
  dR_dY_q2[m]=dR_dY_q2[m]*d_yield_stress_e;
  dR_dY_q3[m]=dR_dY_q3[m]*d_yield_stress_e;
  dR_dY_q4[m]=dR_dY_q4[m]*d_yield_stress_e;
  dR_dY_q5[m]=dR_dY_q5[m]*d_yield_stress_e;
  dR_dY_q6[m]=dR_dY_q6[m]*d_yield_stress_e;
  dR_dY_q7[m]=dR_dY_q7[m]*d_yield_stress_e;
  dR_dY_q8[m]=dR_dY_q8[m]*d_yield_stress_e;

  dR_dG_q1[m]=dR_dG_q1[m]*dGe;
  dR_dG_q2[m]=dR_dG_q2[m]*dGe;
  dR_dG_q3[m]=dR_dG_q3[m]*dGe;
  dR_dG_q4[m]=dR_dG_q4[m]*dGe;
  dR_dG_q5[m]=dR_dG_q5[m]*dGe;
  dR_dG_q6[m]=dR_dG_q6[m]*dGe;
  dR_dG_q7[m]=dR_dG_q7[m]*dGe;
  dR_dG_q8[m]=dR_dG_q8[m]*dGe;

  dR_da_q1[m]=dR_da_q1[m]*d_hard_constant_e;
  dR_da_q2[m]=dR_da_q2[m]*d_hard_constant_e;
  dR_da_q3[m]=dR_da_q3[m]*d_hard_constant_e;
  dR_da_q4[m]=dR_da_q4[m]*d_hard_constant_e;
  dR_da_q5[m]=dR_da_q5[m]*d_hard_constant_e;
  dR_da_q6[m]=dR_da_q6[m]*d_hard_constant_e;
  dR_da_q7[m]=dR_da_q7[m]*d_hard_constant_e;
  dR_da_q8[m]=dR_da_q8[m]*d_hard_constant_e;

  dR_dxe_q1[m]=dR_dC_q1[m]+dR_dY_q1[m]+dR_dG_q1[m]+dR_da_q1[m];
  dR_dxe_q2[m]=dR_dC_q2[m]+dR_dY_q2[m]+dR_dG_q2[m]+dR_da_q2[m];
  dR_dxe_q3[m]=dR_dC_q3[m]+dR_dY_q3[m]+dR_dG_q3[m]+dR_da_q3[m];
  dR_dxe_q4[m]=dR_dC_q4[m]+dR_dY_q4[m]+dR_dG_q4[m]+dR_da_q4[m];
  dR_dxe_q5[m]=dR_dC_q5[m]+dR_dY_q5[m]+dR_dG_q5[m]+dR_da_q5[m];
  dR_dxe_q6[m]=dR_dC_q6[m]+dR_dY_q6[m]+dR_dG_q6[m]+dR_da_q6[m];
  dR_dxe_q7[m]=dR_dC_q7[m]+dR_dY_q7[m]+dR_dG_q7[m]+dR_da_q7[m];
  dR_dxe_q8[m]=dR_dC_q8[m]+dR_dY_q8[m]+dR_dG_q8[m]+dR_da_q8[m];

  //if(e==0 && m==0) printf("e=%d : dR_dxe_q[%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg]\n",
  //e,dR_dxe_q1[m],dR_dxe_q2[m],dR_dxe_q3[m],dR_dxe_q4[m],dR_dxe_q5[m],dR_dxe_q6[m],dR_dxe_q7[m],dR_dxe_q8[m]);

  //if(e==200 && m==9) printf("e=%d : dR_dxe_q[%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg]\n",
  //e,dR_dxe_q1[m],dR_dxe_q2[m],dR_dxe_q3[m],dR_dxe_q4[m],dR_dxe_q5[m],dR_dxe_q6[m],dR_dxe_q7[m],dR_dxe_q8[m]);

}

somme1=0;
somme2=0;
somme3=0;
somme4=0;
somme5=0;
somme6=0;
somme7=0;
somme8=0;

for (m=0;m<24;m++){
  somme1=somme1+lambda_e[m]*dR_dxe_q1[m];
  somme2=somme2+lambda_e[m]*dR_dxe_q2[m];
  somme3=somme3+lambda_e[m]*dR_dxe_q3[m];
  somme4=somme4+lambda_e[m]*dR_dxe_q4[m];
  somme5=somme5+lambda_e[m]*dR_dxe_q5[m];
  somme6=somme6+lambda_e[m]*dR_dxe_q6[m];
  somme7=somme7+lambda_e[m]*dR_dxe_q7[m];
  somme8=somme8+lambda_e[m]*dR_dxe_q8[m];

  //if(e==200 && l==0) printf("e=%d : lambda_e=%lg, dR_dxe_q[%lg,%lg,%lg,%lg,%lg,%lg,%lg,%lg]\n",e,lambda_e[m],
//dR_dxe_q1[m],dR_dxe_q2[m],dR_dxe_q3[m],dR_dxe_q4[m],dR_dxe_q5[m],dR_dxe_q6[m],dR_dxe_q7[m],dR_dxe_q8[m]);
  //if(e==200 && m==9) printf("e=%d : lambda_e=%lg\n",e,lambda_e[m]);
  }

    e1=map_elements_short[e];
  if(von_mises_stress[l*numElems+e1]!=0){
  dc_dvm[e]=(double)pow(von_mises_stress[l*numElems+e1],pnorm2)*dc_dvm1/numElems_red;
  dvm_dwe[6*e+0]=(double)(2*stress_xx_e[l*numElems+e1]-stress_yy_e[l*numElems+e1]-stress_zz_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
  dvm_dwe[6*e+1]=(double)(2*stress_yy_e[l*numElems+e1]-stress_xx_e[l*numElems+e1]-stress_zz_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
  dvm_dwe[6*e+2]=(double)(2*stress_zz_e[l*numElems+e1]-stress_xx_e[l*numElems+e1]-stress_yy_e[l*numElems+e1])/(2*von_mises_stress[l*numElems+e1]);
  dvm_dwe[6*e+3]=(double)3*stress_xy_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
  dvm_dwe[6*e+4]=(double)3*stress_yz_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
  dvm_dwe[6*e+5]=(double)3*stress_xz_e[l*numElems+e1]/von_mises_stress[l*numElems+e1];
}
else{
  dc_dvm[e]=0;
  dvm_dwe[6*e+0]=0;
  dvm_dwe[6*e+1]=0;
  dvm_dwe[6*e+2]=0;
  dvm_dwe[6*e+3]=0;
  dvm_dwe[6*e+4]=0;
  dvm_dwe[6*e+5]=0;
}


somme1_dc=0;
somme2_dc=0;
somme3_dc=0;
somme4_dc=0;
somme5_dc=0;
somme6_dc=0;
somme7_dc=0;
somme8_dc=0;


for (m=0;m<6;m++){
    somme1_dc=somme1_dc+dvm_dwe[6*e+m]*(dS_dC_q1[m]+dS_dY_q1[m]*d_yield_stress_e+dS_dG_q1[m]*dGe+dS_da_q1[m]*d_hard_constant_e);
    somme2_dc=somme2_dc+dvm_dwe[6*e+m]*(dS_dC_q2[m]+dS_dY_q2[m]*d_yield_stress_e+dS_dG_q2[m]*dGe+dS_da_q2[m]*d_hard_constant_e);
    somme3_dc=somme3_dc+dvm_dwe[6*e+m]*(dS_dC_q3[m]+dS_dY_q3[m]*d_yield_stress_e+dS_dG_q3[m]*dGe+dS_da_q3[m]*d_hard_constant_e);
    somme4_dc=somme4_dc+dvm_dwe[6*e+m]*(dS_dC_q4[m]+dS_dY_q4[m]*d_yield_stress_e+dS_dG_q4[m]*dGe+dS_da_q4[m]*d_hard_constant_e);
    somme5_dc=somme5_dc+dvm_dwe[6*e+m]*(dS_dC_q5[m]+dS_dY_q5[m]*d_yield_stress_e+dS_dG_q5[m]*dGe+dS_da_q5[m]*d_hard_constant_e);
    somme6_dc=somme6_dc+dvm_dwe[6*e+m]*(dS_dC_q6[m]+dS_dY_q6[m]*d_yield_stress_e+dS_dG_q6[m]*dGe+dS_da_q6[m]*d_hard_constant_e);
    somme7_dc=somme7_dc+dvm_dwe[6*e+m]*(dS_dC_q7[m]+dS_dY_q7[m]*d_yield_stress_e+dS_dG_q7[m]*dGe+dS_da_q7[m]*d_hard_constant_e);
    somme8_dc=somme8_dc+dvm_dwe[6*e+m]*(dS_dC_q8[m]+dS_dY_q8[m]*d_yield_stress_e+dS_dG_q8[m]*dGe+dS_da_q8[m]*d_hard_constant_e);
}

dc_dxe[e]=dc_dvm[e]*0.125*(somme1_dc+somme2_dc+somme3_dc+somme4_dc+somme5_dc+somme6_dc+somme7_dc+somme8_dc);

gradient2_l[l*numElems+map_elements_short[e]]=(nodal_force==0 && pnorm_stress==1)*dc_dxe[e]+somme1+somme2+somme3+somme4+somme5+somme6+somme7+somme8;

gradient2_map[map_elements_short[e]]=gradient2_map[map_elements_short[e]]+gradient2_l[l*numElems+map_elements_short[e]];
if(map_elements_short[e]==0 || map_elements_short[e]==50) if(l==loadsteps-1) printf("gradient[%d]=%.8lg\n",map_elements_short[e],gradient2_map[map_elements_short[e]]);
if(e==0 || e==50) printf("von_mises_stress=%lg\n",von_mises_stress[numElems*l+e]);
//if(e==0 || e==200) if(l==0) printf("e=%d :  somme[%lg %lg %lg %lg %lg %lg %lg %lg] \n",
//e,somme1,somme2,somme3,somme4,somme5,somme6,somme7,somme8);
//if(map_elements_short[e]==0 || map_elements_short[e]==200 )  if(l==loadsteps-1) printf("e=%d :  dc_dxe=%lg, gradient2=%.8lg \n", e,dc_dxe[e],gradient2_map[map_elements_short[e]]);
//if(map_elements_short[e]==0) printf("map0, e=%d\n",e);

}//end element loop

//printf("map_elements_short[0]=%d, map_elements_short[200]=%d\n",map_elements_short[0],map_elements_short[200]);

free(dR_dS_q1); free(dR_dS_q2); free(dR_dS_q3); free(dR_dS_q4); free(dR_dS_q5); free(dR_dS_q6); free(dR_dS_q7); free(dR_dS_q8);
free(dS_dC_q1); free(dS_dC_q2); free(dS_dC_q3); free(dS_dC_q4); free(dS_dC_q5); free(dS_dC_q6); free(dS_dC_q7); free(dS_dC_q8);
free(dS1_dC_q1); free(dS1_dC_q2); free(dS1_dC_q3); free(dS1_dC_q4); free(dS1_dC_q5); free(dS1_dC_q6); free(dS1_dC_q7); free(dS1_dC_q8);
free(dS2_dC_q1); free(dS2_dC_q2); free(dS2_dC_q3); free(dS2_dC_q4); free(dS2_dC_q5); free(dS2_dC_q6); free(dS2_dC_q7); free(dS2_dC_q8);
free(deta_dC_q1); free(deta_dC_q2); free(deta_dC_q3); free(deta_dC_q4); free(deta_dC_q5); free(deta_dC_q6); free(deta_dC_q7); free(deta_dC_q8);
free(dep_dC_q1); free(dep_dC_q2); free(dep_dC_q3); free(dep_dC_q4); free(dep_dC_q5); free(dep_dC_q6); free(dep_dC_q7); free(dep_dC_q8);
free(dbeta_dC_q1); free(dbeta_dC_q2); free(dbeta_dC_q3); free(dbeta_dC_q4); free(dbeta_dC_q5); free(dbeta_dC_q6); free(dbeta_dC_q7); free(dbeta_dC_q8);
free(deta_dY_q1); free(deta_dY_q2); free(deta_dY_q3); free(deta_dY_q4); free(deta_dY_q5); free(deta_dY_q6); free(deta_dY_q7); free(deta_dY_q8);
free(dep_dY_q1); free(dep_dY_q2); free(dep_dY_q3); free(dep_dY_q4); free(dep_dY_q5); free(dep_dY_q6); free(dep_dY_q7); free(dep_dY_q8);
free(dbeta_dY_q1); free(dbeta_dY_q2); free(dbeta_dY_q3); free(dbeta_dY_q4); free(dbeta_dY_q5); free(dbeta_dY_q6); free(dbeta_dY_q7); free(dbeta_dY_q8);
free(deta_dG_q1); free(deta_dG_q2); free(deta_dG_q3); free(deta_dG_q4); free(deta_dG_q5); free(deta_dG_q6); free(deta_dG_q7); free(deta_dG_q8);
free(dep_dG_q1); free(dep_dG_q2); free(dep_dG_q3); free(dep_dG_q4); free(dep_dG_q5); free(dep_dG_q6); free(dep_dG_q7); free(dep_dG_q8);
free(dbeta_dG_q1); free(dbeta_dG_q2); free(dbeta_dG_q3); free(dbeta_dG_q4); free(dbeta_dG_q5); free(dbeta_dG_q6); free(dbeta_dG_q7); free(dbeta_dG_q8);
free(deta_da_q1); free(deta_da_q2); free(deta_da_q3); free(deta_da_q4); free(deta_da_q5); free(deta_da_q6); free(deta_da_q7); free(deta_da_q8);
free(dep_da_q1); free(dep_da_q2); free(dep_da_q3); free(dep_da_q4); free(dep_da_q5); free(dep_da_q6); free(dep_da_q7); free(dep_da_q8);
free(dbeta_da_q1); free(dbeta_da_q2); free(dbeta_da_q3); free(dbeta_da_q4); free(dbeta_da_q5); free(dbeta_da_q6); free(dbeta_da_q7); free(dbeta_da_q8);
free(dR_dY_q1); free(dR_dY_q2);  free(dR_dY_q3); free(dR_dY_q4); free(dR_dY_q5); free(dR_dY_q6);  free(dR_dY_q7); free(dR_dY_q8);
free(dR_dG_q1); free(dR_dG_q2);  free(dR_dG_q3); free(dR_dG_q4); free(dR_dG_q5); free(dR_dG_q6);  free(dR_dG_q7); free(dR_dG_q8);
free(dS_dY_q1); free(dS_dY_q2); free(dS_dY_q3); free(dS_dY_q4);  free(dS_dY_q5); free(dS_dY_q6);  free(dS_dY_q7); free(dS_dY_q8);
free(dS1_dY_q1); free(dS1_dY_q2); free(dS1_dY_q3); free(dS1_dY_q4); free(dS1_dY_q5); free(dS1_dY_q6); free(dS1_dY_q7); free(dS1_dY_q8);
free(dS2_dY_q1); free(dS2_dY_q2); free(dS2_dY_q3); free(dS2_dY_q4); free(dS2_dY_q5); free(dS2_dY_q6); free(dS2_dY_q7); free(dS2_dY_q8);
free(dS3_dY_q1); free(dS3_dY_q2); free(dS3_dY_q3); free(dS3_dY_q4); free(dS3_dY_q5); free(dS3_dY_q6); free(dS3_dY_q7); free(dS3_dY_q8);
free(dR_da_q1); free(dR_da_q2); free(dR_da_q3); free(dR_da_q4); free(dR_da_q5); free(dR_da_q6); free(dR_da_q7); free(dR_da_q8);
free(dS_dG_q1);  free(dS_dG_q2); free(dS_dG_q3); free(dS_dG_q4); free(dS_dG_q5);  free(dS_dG_q6); free(dS_dG_q7); free(dS_dG_q8);
free(dS1_dG_q1);  free(dS1_dG_q2); free(dS1_dG_q3); free(dS1_dG_q4); free(dS1_dG_q5);  free(dS1_dG_q6); free(dS1_dG_q7); free(dS1_dG_q8);
free(dS2_dG_q1);  free(dS2_dG_q2); free(dS2_dG_q3); free(dS2_dG_q4); free(dS2_dG_q5);  free(dS2_dG_q6); free(dS2_dG_q7); free(dS2_dG_q8);
free(dS3_dG_q1);  free(dS3_dG_q2); free(dS3_dG_q3); free(dS3_dG_q4); free(dS3_dG_q5);  free(dS3_dG_q6); free(dS3_dG_q7); free(dS3_dG_q8);
free(dS_da_q1);  free(dS_da_q2); free(dS_da_q3); free(dS_da_q4); free(dS_da_q5);  free(dS_da_q6); free(dS_da_q7); free(dS_da_q8);
free(dS1_da_q1);  free(dS1_da_q2); free(dS1_da_q3); free(dS1_da_q4); free(dS1_da_q5);  free(dS1_da_q6); free(dS1_da_q7); free(dS1_da_q8);
free(dS2_da_q1);  free(dS2_da_q2); free(dS2_da_q3); free(dS2_da_q4); free(dS2_da_q5);  free(dS2_da_q6); free(dS2_da_q7); free(dS2_da_q8);
free(dS3_da_q1);  free(dS3_da_q2); free(dS3_da_q3); free(dS3_da_q4); free(dS3_da_q5);  free(dS3_da_q6); free(dS3_da_q7); free(dS3_da_q8);
free(dR_dC_q1); free(dR_dC_q2); free(dR_dC_q3); free(dR_dC_q4); free(dR_dC_q5); free(dR_dC_q6); free(dR_dC_q7); free(dR_dC_q8);
free(dR_dxe_q1); free(dR_dxe_q2); free(dR_dxe_q3); free(dR_dxe_q4); free(dR_dxe_q5); free(dR_dxe_q6); free(dR_dxe_q7); free(dR_dxe_q8);
}

////////////

void rho_definition(int numElems, double *domain, double *rho, double volfrac, int *map_gradient)
  {
    int e,cpt;

      cpt=0;
  for (e=0;e<numElems;e++) {

        if(domain[e]==0){
          rho[e]=0;
          map_gradient[e]=-1;
          //rho[e]=1;
          //map_gradient[e]=cpt;
          //cpt++;
        }
        if(domain[e]==1){
          rho[e]=1;
          map_gradient[e]=-1;
        }
        if(domain[e]!=0 && domain[e]!=1){
          rho[e]=volfrac;
          map_gradient[e]=cpt;
          cpt++;
        }

  }

  }

  int mapping(int ni,int nj, int numElems, int numNodes, int qpoints, double *rho, int *map_elements_long,
      int *map_elements_short, int *edofMat2, int *edofMat_e, bool *active_elements, bool *active_nodes)
      {
        int e,i,j,k,m,n,id,id2,cpt;
        int edofMat[MNE];

        //Active elements and nodes
        cpt=0;
        for(e=0;e<numElems;e++) {
          k=e/(ni*nj);
          j=(e%(nj*ni))/ni;
          i=(e%(nj*ni))%ni;
          id=3*((ni+1)*(nj+1)*k+(ni+1)*j+i);
          id2=(ni+1)*(nj+1)*k+(ni+1)*j+i;

          edofMat2[qpoints*e+0]=id2;
          edofMat2[qpoints*e+1]=id2+1;
          edofMat2[qpoints*e+2]=(ni+1)+id2+1;
          edofMat2[qpoints*e+3]=(ni+1)+id2;
          edofMat2[qpoints*e+4]=id2+(ni+1)*(nj+1);
          edofMat2[qpoints*e+5]=id2+1+(ni+1)*(nj+1);
          edofMat2[qpoints*e+6]=(ni+1)+id2+1+(ni+1)*(nj+1);
          edofMat2[qpoints*e+7]=(ni+1)+id2+(ni+1)*(nj+1);

          edofMat_calculation(id,ni,nj,edofMat);
          for (m=0;m<3*qpoints;m++) {
          edofMat_e[3*qpoints*e+m]=edofMat[m];
        }

          if(rho[e]>0){
            active_elements[e]=1;
            map_elements_long[e]=cpt;
            map_elements_short[cpt]=e;
            for (m=0;m<3*qpoints;m++)     active_nodes[edofMat[m]]=1;
            cpt++;
          }
          else{
            map_elements_long[e]=-1;
          }
        }

        //Nodes counter
        cpt=0;
        for(n=0;n<numNodes;n++) {
          if(active_nodes[n]==1) cpt++;
          }

          return cpt;
      }

//Matrix-free vector product : elastic contribution
__global__ void GPUMatVec_elastic(int *edofMat, size_t numElems_red, double *rho_red,  double penal,
  double *yphys_red, double *Ke,  double *U, double *r)
{
  //global gpu thread index
	//double Au;


  double Au[MNE],sum;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems_red){


	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= pow(rho_red[e],penal)*yphys_red[e]*Ke[i*MNE+j]*U[edofMat[MNE*e+j]];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+j]*U[edofMat[j]];
                                  }
        sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7]+
            Au[8]+Au[9]+Au[10]+Au[11]+Au[12]+Au[13]+Au[14]+Au[15]+
            Au[16]+Au[17]+Au[18]+Au[19]+Au[20]+Au[21]+Au[22]+Au[23];

      atomicAdd(&r[edofMat[MNE*e+i]], sum);
                         }
  }

}

__global__ void GPUMatVec_2(int *edofMat, size_t numElems_red,  double *Ke_total,  double *U, double *r)
{
  //global gpu thread index
	//double Au;

  int l;
  double Au[MNE],sum;
  double Ke[MNE*MNE];
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems_red){

    for(l=0;l<MNE*MNE;l++) Ke[l]=Ke_total[MNE*MNE*e+l];

	for (int i=0; i<MNE; i++){
        for (int j=0; j<MNE; j++){
      Au[j]= Ke[i*MNE+j]*U[edofMat[24*e+j]];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+j]*U[edofMat[j]];
                                  }
          sum=Au[0]+Au[1]+Au[2]+Au[3]+Au[4]+Au[5]+Au[6]+Au[7]+
              Au[8]+Au[9]+Au[10]+Au[11]+Au[12]+Au[13]+Au[14]+Au[15]+
              Au[16]+Au[17]+Au[18]+Au[19]+Au[20]+Au[21]+Au[22]+Au[23];

      atomicAdd(&r[edofMat[24*e+i]], sum);
                         }
  }

}

//Preconditionning for the conjugate gradient algorithm : Jacobi preconditionner = diagonal scaling
__global__ void GPUMatVec_precond(int *edofMat, size_t numElems, double *rho, double *yphys, double penal,
  double *Ke,  double *r)
{
  //global gpu thread index
	//double Au;

  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems){

	for (int i=0; i<MNE; i++){

      Au= pow(rho[e],penal)*yphys[e]*Ke[i*MNE+i];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+i];
      atomicAdd(&r[edofMat[24*e+i]], Au);
                         }
  }

}

__global__ void GPUMatVec_precond2(int *edofMat, size_t numElems_red,  double *Ke,  double *r)
{
  //global gpu thread index
	//double Au;

  double Au;
  size_t e = blockIdx.x * blockDim.x + threadIdx.x;


  if (e<numElems_red){


	for (int i=0; i<MNE; i++){

      Au= Ke[MNE*MNE*e+i*MNE+i];//(Emin+pow(rho[e],penal)*(E-Emin))*Ke[i*MNE+i];
      atomicAdd(&r[edofMat[MNE*e+i]], Au);
                         }
  }

}

//Conjugate gradient Procedures
__global__ void rzp_calculation(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
      //printf("M[%d]=%f\n",n,M_pre[n]);
      M_pre[n]= 1.f/M_pre[n];
      r[n] = f[n] - prod[n];
      z[n]=M_pre[n]*r[n];
      p[n] = z[n];

      //if(n==0) printf("M_pre[21056]=%lg, r[21056]=%lg, p[21056]=%lg, z[21056]=%lg\n",M_pre[21056],
    //r[21056],p[21056],z[21056]);
    }

  }

}

__global__ void rzp_calculation2(double *f, double *M_pre, double *r, double *prod, double *z, double *p,  int numNodes, int *fixednodes)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){


    if(fixednodes[n]>0 ){
      r[n]=0;
    }
    else{
      //printf("M[%d]=%f\n",n,M_pre[n]);
      M_pre[n]= 1.f/M_pre[n];
      r[n] = f[n] - prod[n];
      z[n]=M_pre[n]*r[n];
      p[n] = z[n];

      //if(n==0) printf("M_pre[21056]=%lg, r[21056]=%lg, p[21056]=%lg, z[21056]=%lg\n",M_pre[21056],
    //r[21056],p[21056],z[21056]);
    }

  }

}

__global__ void UpdateVec(double *p, double *r, int numNodes, int *fixednodes, double *rzold, double *rznew,
  double *Ap)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  double beta = rznew[0]/rzold[0];
  //if(n==0) printf("rzold=%lg, rznew=%lg and beta=%f\n",rzold[0],rznew[0],beta);
  if (n<numNodes){
    Ap[n]=0;
  if(fixednodes[n]==0 ){

      p[n] = r[n] + beta * p[n];
    }
    }
}

__global__ void switch_r(double *rzold, double *rznew, double *pAp){
  //size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  if(threadIdx.x==0){
    rzold[0]=rznew[0];
    rznew[0]=0;
    pAp[0]=0;
    //norme[0]=0;
  }
}


__global__ void Norm(double *r, int numNodes, double *result)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes ){
  result[n]=r[n]*r[n];
    }
}

__global__ void Prod2_rz(double *r, double*z, int numNodes, int *fixednodes, double *result)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0 ){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
   }

}

__global__ void Prod3_rz(double *r, double*z, int numNodes, int *fixednodes, double *result, double *norme)
{
  __shared__ double cache[BLOCKSIZE2];
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp=0.0;
  while (n<numNodes){

    if(fixednodes[n]==0 ){
      temp=temp+ r[n]*z[n];
    }
    n=n+stride;
  }
    cache[threadIdx.x]=temp;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache[threadIdx.x]+=cache[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }
    if (threadIdx.x ==0){
     atomicAdd(&result[0],cache[0]);
     norme[0]=0;
   }

}

__global__ void Update_UR(double *M_pre, double *U, double *r, double *z, double *p, double *Ap, int numNodes, int *fixednodes,
  double *rzold, double *pAp, double *result1, double *result2)
{

  /*size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = M_pre[n]*r[n];

    }

  }*/

  __shared__ double cache1[BLOCKSIZE2];
  __shared__ double cache2[BLOCKSIZE2];

  size_t n = blockIdx.x * blockDim.x + threadIdx.x;
  size_t stride=blockDim.x*gridDim.x;

  double temp1=0.0;
  double temp2=0.0;


  while (n<numNodes){
    if(fixednodes[n]==0 ){
        double alpha=rzold[0]/pAp[0];
        //if(n==numNodes-1) printf("rzold=%lg, pAp=%lg and alpha=%lg\n",rzold[0],pAp[0],alpha);
      U[n] = U[n] + alpha * p[n];
      r[n] = r[n] - alpha * Ap[n];
      z[n] = M_pre[n]*r[n];
      temp1=temp1+ r[n]*z[n];
      temp2=temp2+ r[n]*r[n];
    }
    n=n+stride;
    }

    cache1[threadIdx.x]=temp1;
    cache2[threadIdx.x]=temp2;
    __syncthreads();

    unsigned int i=blockDim.x/2;
    while(i!=0){
      if (threadIdx.x <i){
        cache1[threadIdx.x]+=cache1[threadIdx.x+i];
        cache2[threadIdx.x]+=cache2[threadIdx.x+i];
      }
      __syncthreads();
      i/=2;
    }

    if (threadIdx.x ==0){
     atomicAdd(&result1[0],cache1[0]);
     atomicAdd(&result2[0],cache2[0]);

   }

}

__global__ void Update_Newton(int numNodes, double *delta_U, double *U)
{
  size_t n = blockIdx.x * blockDim.x + threadIdx.x;

  if (n<numNodes){
      U[n] = U[n]+delta_U[n];
  }

}

__global__ void display(double *vector, int size){
  if(threadIdx.x==0){
    for (int i=0;i<size;i++){
      printf("vector[%d]=%lg\n",i,vector[i]);
    }
  }
}


int main(int argc, char **argv)
{
printf("Program beginning\n");
  //////////////////////////////////////// End Read Mesh ////////////////////////////////////////

FILE *file_density;
char filename_input[100];
int ni; int nj; int nk; int numElems;
double *domain;

  if(argc==2){
    strcpy(filename_input,argv[1]);
    file_density=fopen(filename_input,"r");
    if(file_density!=NULL)
    {
      if(fscanf(file_density,"%d\n",&ni));
      if(fscanf(file_density,"%d\n",&nj));
      if(fscanf(file_density,"%d\n",&nk));
      numElems=ni*nj*nk;
      domain= (double *)calloc(numElems,sizeof(double));
      for (int e=0;e<numElems;e++) if(fscanf(file_density,"%lf\n",&(domain[e])));
      fclose(file_density);
    }
    else{
      printf("error : input file not found \n");
      return 0;
    }
  }
  else{
    printf("error : input file needed\n");
    return 0;
  }

  cudaSetDevice(1);
  cudaDeviceReset();

  double time_program = get_time();

  int i,j,k,l,l1,q,m,m1,m2,n,e,i0,cpt,cpt1,cpt2,iter,nb_plastic,nb_plastic_nodes;
  int layer, load1;
  bool plasticity;

FILE *fichier;
double weight1,weight2,weight3,weight4,weight5,weight6,weight7,weight8,cost_function1, cost_function2,
 sommeV,n1,n2,n3,n4,n5,n6,pnorm1, somme,
dp1_ds1,dp1_ds2,dp1_ds3,dp1_ds4,dp1_ds5,dp1_ds6,dp2_ds1,dp2_ds2,dp2_ds3,
dp2_ds4,dp2_ds5,dp2_ds6,dp3_ds1,dp3_ds2,dp3_ds3,dp3_ds4,dp3_ds5,dp3_ds6,
dp4_ds1,dp4_ds2,dp4_ds3,dp4_ds4,dp4_ds5,dp4_ds6,dp5_ds1,dp5_ds2,dp5_ds3,
dp5_ds4,dp5_ds5,dp5_ds6,dp6_ds1,dp6_ds2,dp6_ds3,dp6_ds4,dp6_ds5,dp6_ds6,
ds1_dstr1,ds2_dstr1,ds3_dstr1,
ds1_dstr2,ds2_dstr2,ds3_dstr2,
ds1_dstr3,ds2_dstr3,ds3_dstr3,
ds4_dstr4,ds5_dstr5,ds6_dstr6;
double dc_dvm1,min_stress,ratio,ratio2;
weight1=1; weight2=1; weight3=1; weight4=1;
weight5=1; weight6=1; weight7=1; weight8=1;

//number of elements in each direction

int qpoints=8;
//Note that if you want to change this value (qpoints), you will have also to create
//new BE arrays
double volfrac=0.5;
double rmin=1.8;
double penal=3;
double penal1=penal-1;
double pnorm=4;
int opt_steps=150;
int Max_iters=50000;
double tol=1e-6;
int Max_iters_newton=200;
double tol_newton=1e-6;
double nodal_force=-0*10;
//number of nodes in x and y
int ni2=ni+1;
int nj2=nj+1;

double delta_x=(double)0.001;
double delta_y=(double)0.001;
double delta_z=(double)0.001;

int numNodes=3*(ni+1)*(nj+1)*(nk+1);

double *rho= (double *)calloc(numElems,sizeof(double));
int *map_gradient= (int *)calloc(numElems,sizeof(int));

rho_definition(numElems,domain,rho,volfrac,map_gradient);

//Mapping void/solid elements
cpt1=0;
cpt2=0;
for(e=0;e<numElems;e++) {
//  printf("map_gradient[%d]=%d\n",e,map_gradient[e]);
  if(rho[e]>0) cpt1++;
  if(rho[e]>0 && rho[e]<1) cpt2++;
  }

int numElems_red=cpt1;
int numElems_opt=cpt2;

FILE *cost_function_file;
cost_function_file=fopen("min_stress.txt","w");
fclose(cost_function_file);

int nb_layers=5;
int layer_height=nk/nb_layers;
printf("layer_height=%d, numElems_red=%d, numElems_opt=%d\n",layer_height,numElems_red,numElems_opt);
int loadsteps1=1;
int loadsteps=nb_layers*loadsteps1;
bool pnorm_stress=1;
//pnorm_stress=1 : pnorm stress objective function
//pnorm_stress=0 : displacement objective function
double E=180E9;
double Emin=1e-6;
double nu=0.30;
//double inh_strain[6]={0.0018*0.2,0.0018*0.2,-0.0036*0.2,0,0,0};
double inh_strain[6]={0.003*0.1,0.003*0.1,-0.006*0.1,0,0,0};
double inh_strain2[6];
double hard_constant=1e-2*E;//1e-4;//0.1*E;
double hard_constant_e;
double yield_stress=800e6*sqrt(2.f/3);
double yield_stress_e, Ge;
double G=(double)E/(2*(1+nu));

bool finite_difference=0;
bool forward_problem=1;
bool gradient_calculation=1;

double *ux= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *uy= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *uz= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *fx= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *fy= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *fz= (double *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(double));
double *lambdax= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
double *lambday= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
double *lambdaz= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
double *rhs_x= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
double *rhs_y= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
double *rhs_z= (double *)calloc(loadsteps*(ni+1)*(nj+1)*(nk+1),sizeof(double));
bool *old_pstrain= (bool *)calloc(loadsteps,sizeof(bool));
double *inh_strain2_l = (double *) malloc(6*loadsteps * numElems_red*sizeof(double));

char str1[10]=".vtk"; char str2[10];
char u2[10];

int *edofMat2= (int *)calloc(qpoints*numElems,sizeof(int));
int *edofMat_e=(int *)calloc(3*qpoints*numElems,sizeof(int));
int *edofMat_e_red=(int *)calloc(3*qpoints*numElems_red,sizeof(int));

int *map_elements_long=(int *)calloc(numElems,sizeof(int));
int *map_elements_short=(int *)calloc(numElems_red,sizeof(int));

//Element and node status (void or solid)
bool *active_elements= (bool *)calloc(numElems,sizeof(bool));

bool *active_nodes= (bool *)calloc(numNodes,sizeof(bool));
bool *active_nodes_x= (bool *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(bool));
bool *active_nodes_y= (bool *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(bool));
bool *active_nodes_z= (bool *)calloc((ni+1)*(nj+1)*(nk+1),sizeof(bool));

int numNodes_red=mapping(ni,nj,numElems,numNodes,qpoints,
rho,map_elements_long,map_elements_short,edofMat2,
edofMat_e,active_elements,active_nodes);

printf("numNodes_red=%d\n",numNodes_red);

double *rho_red= (double *)calloc(numElems_red,sizeof(double));

for(e=0;e<numElems_red;e++) {
  rho_red[e]=rho[map_elements_short[e]];
}

cpt=0;
for (k=0;k<nk+1;k++){
for (j=0;j<nj+1;j++){
for (i=0;i<ni+1;i++){
 i0=I3D(ni2,nj2,i,j,k);
 active_nodes_x[i0]=active_nodes[cpt]; cpt=cpt+1;
 active_nodes_y[i0]=active_nodes[cpt]; cpt=cpt+1;
 active_nodes_z[i0]=active_nodes[cpt]; cpt=cpt+1;
}
}
}

//Mapping void/solid Nodes
int *map_nodes_long=(int *)calloc(numNodes,sizeof(int));
int *map_nodes_short=(int *)calloc(numNodes_red,sizeof(int));

//Connectivity reduced domain Mechanical
cpt=0;
for(n=0;n<numNodes;n++) {
  if(active_nodes[n]==1){
    map_nodes_long[n]=cpt;
    map_nodes_short[cpt]=n;
    cpt++;
  }
}

for(e=0;e<numElems_red;e++) {
    for (m=0;m<3*qpoints;m++) {
      edofMat_e_red[3*qpoints*e+m]=map_nodes_long[edofMat_e[3*qpoints*map_elements_short[e]+m]];
    }
}

//Element/node connectivity
int *edofMat_e_red_d;            cudaMalloc((void **) &edofMat_e_red_d, 3*qpoints*numElems_red * sizeof(int));
cudaMemcpy(edofMat_e_red_d, edofMat_e_red, 3*qpoints*numElems_red * sizeof(int), cudaMemcpyHostToDevice);

//MMA initialization

double Xmin = 0.2;
double Xmax = 0.9;
double movlim = 0.01;

double *rho_opt= (double *)calloc(numElems_opt,sizeof(double));
double *rho_old_opt= (double *)calloc(numElems_opt,sizeof(double));
double *g= (double *)calloc(1,sizeof(double));
double *dg= (double *)calloc(numElems_opt,sizeof(double));
double *xmin= (double *)calloc(numElems_opt,sizeof(double));
double *xmax= (double *)calloc(numElems_opt,sizeof(double));
double *af= (double *)calloc(numElems_opt,sizeof(double));
double *bf= (double *)calloc(numElems_opt,sizeof(double));
MMASolver *mma = new MMASolver(numElems_opt,1,0,1e3,0);


double *gradient_map= (double *)calloc(numElems,sizeof(double));
double *gradient= (double *)calloc(numElems_opt,sizeof(double));
double *gradient1= (double *)calloc(numElems_opt,sizeof(double));
double *gradient2=(double *)calloc(numElems_opt,sizeof(double));
double *gradient_FD=(double *)calloc(numElems_opt,sizeof(double));
double *gradient2_map=(double *)calloc(numElems,sizeof(double));
double *gradient2_l=(double *)calloc(loadsteps*numElems,sizeof(double));
double *gradient_FD_map=(double *)calloc(numElems,sizeof(double));

double *cost_function=(double *)calloc(opt_steps,sizeof(double));
double *constraint=(double *)calloc(opt_steps,sizeof(double));


for (e=0;e<numElems;e++){
  if(map_gradient[e]>-1) rho_opt[map_gradient[e]]=rho[e];
}

double *yphys_red_d;
double *yphys= (double *)calloc(numElems,sizeof(double));
double *yphys_red= (double *)calloc(numElems_red,sizeof(double));
double *yphys_red_l= (double *)calloc(loadsteps*numElems_red,sizeof(double));
int *initial_elements= (int *)calloc(numElems,sizeof(int));
int *initial_elements_red= (int *)calloc(numElems_red,sizeof(int));
int *initial_elements_red_l= (int *)calloc(loadsteps*numElems_red,sizeof(int));
double *von_mises_stress=(double *)calloc(loadsteps*numElems,sizeof(double));
double *dev_von_mises_stress=(double *)calloc(numElems,sizeof(double));
double *stress_xx_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_yy_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_zz_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_xy_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_yz_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *stress_xz_e= (double *)calloc(loadsteps*numElems,sizeof(double));
double *dev_stress_xx_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_yy_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_zz_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_xy_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_yz_e= (double *)calloc(numElems,sizeof(double));
double *dev_stress_xz_e= (double *)calloc(numElems,sizeof(double));

double *n_trial_stress_xx_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_yy_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_zz_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_xy_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_yz_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_xz_e =(double *)calloc(numElems,sizeof(double));
double *n_trial_stress_eq =(double *)calloc(numElems,sizeof(double));

double *dR_due=(double *)calloc(loadsteps*6*3*qpoints*numElems_red,sizeof(double));
double *dc_dvm=(double *)calloc(numElems_red,sizeof(double));
double *dc_dvm2=(double *)calloc(loadsteps,sizeof(double));
double *dvm_dwe=(double *)calloc(6*numElems_red,sizeof(double));

double *dc_due=(double *)calloc(loadsteps*3*qpoints*numElems_red,sizeof(double));
double *rhs=(double *)calloc(3*qpoints*numElems_red,sizeof(double));
double *rhs_j=(double *)calloc(3*qpoints*numElems_red,sizeof(double));
double *rhs_l=(double *)calloc(loadsteps*numNodes_red,sizeof(double));
double *dc_dxe=(double *)calloc(numElems_red,sizeof(double));


double C[36]={0}; double C1[36]={0};
double n_trial_stress_matrix[36]={0};
double Ue[MNE]={0}; double Ue_ini[MNE]={0};

double *Ke_elastic = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2 = (double *) malloc(MNE*MNE * sizeof(double));

double *Ke2_t = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_plastic = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_plastic_t = (double *) malloc(MNE*MNE * sizeof(double));
double *Ke2_e = (double *) calloc(MNE*MNE * numElems_red,sizeof(double));
double *Ke_plastic = (double *) calloc(MNE*MNE * numElems_red,sizeof(double));
double *Ke_plastic_d;
checkCudaErrors(cudaMalloc((void **) &Ke_plastic_d, MNE*MNE * numElems_red* sizeof(double)));

//Gathering of all strain-displacement matrix for the different quadrature points for lighter coding

double BE[144],BE1[144],BE1_t[144],BE2[144],BE2_t[144], BE3[144],BE3_t[144], BE4[144],BE4_t[144];
double BE5[144],BE5_t[144],BE6[144],BE6_t[144], BE7[144],BE7_t[144], BE8[144],BE8_t[144];
double strain1[6]={0}; 	double strain1_ini[6]={0}; 	double diff_strain1[6]={0};
double trial_stress1[6]={0}; 	double n_trial_stress1[6]={0}; double dev_stress1[6]={0};
double norm_dev_stress1;

BE_matrix2(delta_x,delta_y,delta_z,BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,E,nu);
transMat(24,6,BE1,BE1_t); transMat(24,6,BE2,BE2_t); transMat(24,6,BE3,BE3_t); transMat(24,6,BE4,BE4_t);
transMat(24,6,BE5,BE5_t); transMat(24,6,BE6,BE6_t); transMat(24,6,BE7,BE7_t); transMat(24,6,BE8,BE8_t);

double *BE_total = (double *) malloc(qpoints*6*3*qpoints * sizeof(double));

  for (i=0;i<6*3*qpoints;i++){
    BE_total[0*6*3*qpoints+i]=BE1[i];
    BE_total[1*6*3*qpoints+i]=BE2[i];
    BE_total[2*6*3*qpoints+i]=BE3[i];
    BE_total[3*6*3*qpoints+i]=BE4[i];
    BE_total[4*6*3*qpoints+i]=BE5[i];
    BE_total[5*6*3*qpoints+i]=BE6[i];
    BE_total[6*6*3*qpoints+i]=BE7[i];
    BE_total[7*6*3*qpoints+i]=BE8[i];
  }

//Material properties
matC(E,nu,C);
matC(1,nu,C1);

//Stiffness matrix
KE_elasticity(delta_x,delta_y,delta_z,Ke_elastic,E,nu,C1);

//Deviatoric stress matrix
double DEV[36]={0};
DEV[0*6+0]=2.f/3;  DEV[0*6+1]=-1.f/3;  DEV[0*6+2]=-1.f/3;
DEV[1*6+0]=-1.f/3; DEV[1*6+1]=2.f/3;   DEV[1*6+2]=-1.f/3;
DEV[2*6+0]=-1.f/3; DEV[2*6+1]=-1.f/3;  DEV[2*6+2]=2.f/3;
DEV[3*6+3]=1.f/2;  DEV[4*6+4]=1.f/2;   DEV[5*6+5]=1.f/2;

double ID_mat[36]={0};
ID_mat[0*6+0]=1; ID_mat[1*6+1]=1; ID_mat[2*6+2]=1;
ID_mat[3*6+3]=1; ID_mat[4*6+4]=1; ID_mat[5*6+5]=1;

double DEV_mat[36]={0};
DEV_mat[0*6+0]=2.f/3;  DEV_mat[0*6+1]=-1.f/3;  DEV_mat[0*6+2]=-1.f/3;
DEV_mat[1*6+0]=-1.f/3; DEV_mat[1*6+1]=2.f/3;   DEV_mat[1*6+2]=-1.f/3;
DEV_mat[2*6+0]=-1.f/3; DEV_mat[2*6+1]=-1.f/3;  DEV_mat[2*6+2]=2.f/3;
DEV_mat[3*6+3]=1.f;    DEV_mat[4*6+4]=1.f;     DEV_mat[5*6+5]=1.f;

cudaStream_t *streams;
streams = (cudaStream_t *)malloc(2 * sizeof(cudaStream_t));
checkCudaErrors(cudaStreamCreate(&(streams[0])));
checkCudaErrors(cudaStreamCreate(&(streams[1])));

double *rho_red_d,*U_red_d,*delta_U_red_d,*delta_lambda_red_d,*lambda_red_d, *norm_delta_U_red_d,*norm_Uold_red_d,*norm_Unew_red_d,
*norm_delta_lambda_red_d,*norm_lambda_old_red_d,*norm_lambda_new_red_d,
*r_red_d,*M_red_d, *prod_red_d, *prod_elastic_red_d, *prod_plastic_red_d, *f_red_d,*p_red_d,*z_red_d,*Ap_red_d, *Ap_elastic_red_d, *Ap_plastic_red_d;
int *fixednodes_red_d;

  //int *pos_heat_d;
  //double *U_d, *r_d, *prod_d,*p_d, *M_d, *z_d, *b_d, *b1_d, *f_d;
  double  *Ke_elastic_d, *Ke2_d; //z_d
  double *pAp_d;
  double *rznew_d,*rzold_d;
  double *rNorm_d;
  double *pAp = (double *) calloc(1,sizeof(double));
  double *rznew = (double *) calloc(1,sizeof(double));
  double *rNorm = (double *) calloc(1,sizeof(double));
  double norme, norme1_forward,norme2_forward,norme3_forward,norme_newton_forward;
  char filename[500];

  double *norm_delta_U_red =(double *)calloc(numNodes_red,sizeof(double));
  double *norm_Unew_red =(double *)calloc(numNodes_red,sizeof(double));
  double *norm_Uold_red =(double *)calloc(numNodes_red,sizeof(double));

  double *norm_delta_lambda_red =(double *)calloc(numNodes_red,sizeof(double));
  double *norm_lambda_new_red =(double *)calloc(numNodes_red,sizeof(double));
  double *norm_lambda_old_red =(double *)calloc(numNodes_red,sizeof(double));

  //Fixed displacement
  int *fixednodes = (int *)calloc(numNodes, sizeof(int));
  int *fixednodes_red = (int *)calloc(numNodes_red, sizeof(int));
  int *fixednodes_x = (int *)calloc((ni+1)*(nj+1)*(nk+1), sizeof(int));
  int *fixednodes_y = (int *)calloc((ni+1)*(nj+1)*(nk+1), sizeof(int));
  int *fixednodes_z = (int *)calloc((ni+1)*(nj+1)*(nk+1), sizeof(int));

  if(nodal_force==0){
    k=0;//Fixed displacement at y=0;
    for (j=0;j<nj+1;j++){
      for (i=0;i<ni+1;i++){
        i0=I3D(ni2,nj2,i,j,k);
        fixednodes[3*i0]=1; // x-direction
        fixednodes[3*i0+1]=1; // y-direction
        fixednodes[3*i0+2]=1; // z-direction
      }
    }
  }
  else{
    i=0;//Fixed displacement at x=0;
    for (k=0;k<nk+1;k++){
      for (j=0;j<nj+1;j++){
        i0=I3D(ni2,nj2,i,j,k);
        fixednodes[3*i0]=1; // x-direction
        fixednodes[3*i0+1]=1; // y-direction
        fixednodes[3*i0+2]=1; // z-direction
      }
    }
  }

  for (n=0;n<numNodes_red;n++) {
   fixednodes_red[n]=fixednodes[map_nodes_short[n]];
  }

  //Nodal force
  double *fext =(double *)calloc(numNodes,sizeof(double));
  double *fext_red =(double *)calloc(numNodes_red,sizeof(double));
  double *fint =(double *)calloc(numNodes_red,sizeof(double));
  double *f_red =(double *)calloc(numNodes_red,sizeof(double));

  double *U = (double *) calloc(numNodes , sizeof(double));
  double *U_red = (double *) calloc(numNodes_red , sizeof(double));
  double *U_red_l = (double *) calloc(loadsteps*numNodes_red , sizeof(double));
  double *Uini_red = (double *) calloc(numNodes_red , sizeof(double));
  double *Uold_red = (double *) calloc(numNodes_red , sizeof(double));
  double *delta_U_red = (double *) calloc(numNodes_red , sizeof(double));
  double *lambda_red = (double *) calloc(numNodes_red , sizeof(double));
  double *lambda_red_l = (double *) calloc(loadsteps*numNodes_red , sizeof(double));
  double *delta_lambda_red = (double *) calloc(numNodes_red , sizeof(double));
  double *lambda_old_red = (double *) calloc(numNodes_red , sizeof(double));
  double *FE_load=(double *)calloc(MNE*numElems_red,sizeof(double));

  for (e=0;e<numElems_red;e++){
    double gravity=0*8e-3;
    if(rho_red[e]==1){
    FE_load[MNE*e+2]=-gravity;
    FE_load[MNE*e+5]=-gravity;
    FE_load[MNE*e+8]=-gravity;
    FE_load[MNE*e+11]=-gravity;
    FE_load[MNE*e+14]=-gravity;
    FE_load[MNE*e+17]=-gravity;
    FE_load[MNE*e+20]=-gravity;
    FE_load[MNE*e+23]=-gravity;
  }
  }

  // Allocate memory
  cudaMalloc((void **) &Ke_elastic_d, MNE*MNE * sizeof(double));

  //additional variable for reduction operations
  cudaMalloc((void **) &pAp_d, 1 * sizeof(double));
  cudaMalloc((void **) &rznew_d, 1 * sizeof(double));
  cudaMalloc((void **) &rzold_d, 1 * sizeof(double));
  cudaMalloc((void **) &rNorm_d, 1 * sizeof(double));

  //Calculation of the element matrix
  cudaMemcpy(Ke_elastic_d, Ke_elastic, MNE*MNE * sizeof(double),cudaMemcpyHostToDevice);

  //Elastic strain predictor
  double *dn_ds= (double *)calloc(numElems_red*36*qpoints,sizeof(double));
  double *dn_ds_l= (double *)calloc(loadsteps*numElems_red*36*qpoints,sizeof(double));
  double *dH_deta_l= (double *)calloc(loadsteps*numElems_red*36*qpoints,sizeof(double));
  double *n_trial_stress= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *n_trial_stress_l= (double *)calloc(loadsteps*numElems_red*6*qpoints,sizeof(double));
  double *dgamma= (double *)calloc(numElems_red*qpoints,sizeof(double));
  double *strain= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *strain_ini= (double *)calloc(loadsteps*numElems_red*6*qpoints,sizeof(double));
  double *plastic_strain= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *hardening= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *old_plastic_strain= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *old_plastic_strain_l= (double *)calloc(loadsteps*numElems_red*6*qpoints,sizeof(double));
  double *old_hardening= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *stress= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *deviatoric_stress= (double *)calloc(numElems_red*6*qpoints,sizeof(double));
  double *consistent_matrix= (double *)calloc(numElems_red*36*qpoints,sizeof(double));
  bool *plasticity_nodes=(bool *)calloc(numElems_red*qpoints,sizeof(bool));
  bool *plasticity_nodes_l=(bool *)calloc(loadsteps*numElems_red*qpoints,sizeof(bool));
  double *norm_dev_stress_l=(double *)calloc(loadsteps*numElems_red*qpoints,sizeof(double));
  double *criteria_vms=(double *)calloc(numElems_red*qpoints,sizeof(double));
  bool *plasticity_elements=(bool *)calloc(numElems_red,sizeof(bool));
  bool *plasticity_elements_d;  cudaMalloc((void **) &plasticity_elements_d, numElems_red * sizeof(bool));
  int *map_plastic=(int *)calloc(numElems_red,sizeof(int));
  int *map_plastic_d;  cudaMalloc((void **) &map_plastic_d, numElems_red * sizeof(int));
  double *FE_int=(double *)calloc(3*qpoints*numElems_red,sizeof(double));

  checkCudaErrors(cudaMalloc((void **) &yphys_red_d, numElems_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &rho_red_d, numElems_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &delta_lambda_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &lambda_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &delta_U_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_delta_U_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_Unew_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_Uold_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_delta_lambda_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_lambda_new_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &norm_lambda_old_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &U_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &f_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &M_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &r_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_elastic_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &prod_plastic_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &p_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &z_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_elastic_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &Ap_plastic_red_d, numNodes_red * sizeof(double)));
  checkCudaErrors(cudaMalloc((void **) &fixednodes_red_d, numNodes_red * sizeof(int)));
  checkCudaErrors(cudaMalloc((void **) &Ke2_d, numElems_red*MNE*MNE * sizeof(double)));

    int NBLOCKS   = (numElems_red+BLOCKSIZE1-1) / BLOCKSIZE1; //round up if n is not a multiple of blocksize
    int NBLOCKS2 = (numNodes_red+BLOCKSIZE2-1) / BLOCKSIZE2; //round up if n is not a multiple of blocksize


if (finite_difference==1){


    double eps=1e-6;
    int e2=0;
    while(e2<numElems_opt){

          if(1==1){
           //if(i0==0 || i0==1 || i0==58 || i0==59 || i0==600 || i0==601 ){
        cpt=1;
              while(cpt<=2){

                for (int e1=0;e1<numElems_opt;e1++){
                    rho_red[e1]=volfrac;
                  if(cpt==1 && e1==map_gradient[e2]) rho_red[e1]=volfrac+eps;
                  if(cpt==2 && e1==map_gradient[e2]) rho_red[e1]=volfrac-eps;
                }


                //CONJUGATE GRADIENT BEGINNING

                //Transfer to GPU

                checkCudaErrors(cudaMemcpy(rho_red_d, rho_red, numElems_red * sizeof(double),cudaMemcpyHostToDevice));
                checkCudaErrors(cudaMemset(U_red_d, 0.0, numNodes_red*sizeof(double)));

                for(m=0;m<6*qpoints*numElems_red;m++){
                old_hardening[m]=0;
                old_plastic_strain[m]=0;
                stress[m]=0;
                }


                for(m=0;m<6*qpoints*numElems_red*loadsteps;m++) strain_ini[m]=0;

                for(m=0;m<6*numElems_red*loadsteps;m++) inh_strain2_l[m]=0;

                for (n=0;n<numNodes_red;n++) {
                  U_red[n]=0;
                }

                /////////////////////////////////////////////////////////////////////
                min_stress=0;

                for(e=0;e<numElems;e++) yphys[e]=E*Emin;

                for(layer=0;layer<nb_layers;layer++){

                  for(e=0;e<numElems;e++) initial_elements[e]=0;
                  for(e=0;e<numElems_red;e++)    initial_elements_red[e]=0;

                  for(i=0;i<numNodes_red;i++) Uini_red[i]=U_red[i];

                  printf("active jmax=%d, cpt=%d\n",layer_height*(layer+1),cpt);

                  for(k=layer_height*layer;k<layer_height*(layer+1);k++){
                      for(j=0;j<nj;j++){
                        for(i=0;i<ni;i++){
                      i0=I3D(ni,nj,i,j,k);
                      yphys[i0]=E;
                      initial_elements[i0]=1;
                    }
                  }
                  }

                  for(e=0;e<numElems_red;e++) {
                    yphys_red[e]=yphys[map_elements_short[e]];
                    initial_elements_red[e]=initial_elements[map_elements_short[e]];
                  }

                //  printf("yphys_red[0]=%lg\n",yphys_red[0]);

                  checkCudaErrors(cudaMemcpy(yphys_red_d, yphys_red, numElems_red * sizeof(double), cudaMemcpyHostToDevice));

                for(load1=0;load1<loadsteps1;load1++){

                  l=loadsteps1*layer+load1;

                  printf("\n\n\n Simulation %d/%d : layer %d ;load step =%d \n\n\n",e2,numElems_opt,layer,load1);

                  double ratio=(double)(load1+1)/loadsteps1;
                  double ratio2=ratio;

                  for (m=0;m<6;m++) inh_strain2[m]=ratio2*inh_strain[m];

                  int iter_newton;

                  for(iter_newton=0; iter_newton<Max_iters_newton; iter_newton++)
                  {

                    for (n=0;n<numNodes;n++) fext[n]=0;

                    for(e=0;e<numElems_red;e++){

                      initial_elements_red_l[l*numElems_red+e]=initial_elements_red[e];

                        Ge=pow(rho_red[e],penal)*G;
                        hard_constant_e=pow(rho_red[e],penal)*hard_constant;
                        yield_stress_e=pow(rho_red[e],penal)*yield_stress;

                      //if(e==0) printf("k=%d, threshold=%d\n",k,layer_height*(layer+1));

                      if(yphys_red[e]==E){

                        for (m=0;m<MNE;m++) {
                          Ue[m]=U_red[edofMat_e_red[MNE*e+m]];
                          Ue_ini[m]=Uini_red[edofMat_e_red[MNE*e+m]];
                          fext[edofMat_e[MNE*map_nodes_short[e]+m]]+=FE_load[MNE*e+m];
                          }

                      for (q=0;q<qpoints;q++){

                        for (i=0;i<6*3*qpoints;i++){
                          BE[i]=BE_total[q*6*3*qpoints+i];
                        }

                        prodMatVec(3*qpoints,6,3*qpoints,BE,Ue,strain1);
                        prodMatVec(3*qpoints,6,3*qpoints,BE,Ue_ini,strain1_ini);


                    for (m=0;m<6;m++) {
                      if(layer>0){
                      if(initial_elements_red[e]==1 && load1==0)    {
                        strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m]=strain_ini[6*qpoints*numElems_red*(l-1)+qpoints*6*e+6*q+m]+strain1_ini[m];
                      }
                      else{
                        strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m]=strain_ini[6*qpoints*numElems_red*(l-1)+qpoints*6*e+6*q+m];
                      }
                      }
                      if(layer==0){
                        inh_strain2_l[6*numElems_red*l+6*e+m]=inh_strain2[m];
                      }
                      else{
                        inh_strain2_l[6*numElems_red*l+6*e+m]=(initial_elements_red[e]==0)*inh_strain[m]+(initial_elements_red[e]==1)*inh_strain2[m];
                      }


                      diff_strain1[m]=strain1[m]-old_plastic_strain[qpoints*6*e+6*q+m]-inh_strain2_l[6*numElems_red*l+6*e+m]
                      -strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m];

                    }


                    //Stress trial with previous strain
                    prodMatVec_e(6,6,6,C,rho_red[e],penal,diff_strain1,trial_stress1);


                      //Deviatoric stress : from trial stress
                    dev_stress1[0]=trial_stress1[0]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
                    dev_stress1[1]=trial_stress1[1]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
                    dev_stress1[2]=trial_stress1[2]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
                    dev_stress1[3]=trial_stress1[3];
                    dev_stress1[4]=trial_stress1[4];
                    dev_stress1[5]=trial_stress1[5];

                    //Deviatoric stress : Substract hardening Variable
                    for (m=0;m<6;m++) {
                      dev_stress1[m]=dev_stress1[m]-old_hardening[qpoints*6*e+6*q+m];
                    }

                    norm_dev_stress1=sqrt(dev_stress1[0]*dev_stress1[0]+dev_stress1[1]*dev_stress1[1]+dev_stress1[2]*dev_stress1[2]
                    +2*(dev_stress1[3]*dev_stress1[3]+dev_stress1[4]*dev_stress1[4]+dev_stress1[5]*dev_stress1[5]));


                    //Stress update
                    if(norm_dev_stress1-yield_stress_e<=0){
                        plasticity_nodes[qpoints*e+q]=0;
                      for (m=0;m<6;m++)   {
                        stress[qpoints*6*e+6*q+m]=trial_stress1[m];
                        plastic_strain[qpoints*6*e+6*q+m]=old_plastic_strain[qpoints*6*e+6*q+m];
                        hardening[qpoints*6*e+6*q+m]=old_hardening[qpoints*6*e+6*q+m];
                      }
                      for (m=0;m<36;m++)  consistent_matrix[qpoints*36*e+36*q+m]=pow(rho_red[e],penal)*C[m];
                      for (m=0;m<6;m++)  deviatoric_stress[qpoints*6*e+6*q+m]=dev_stress1[m];
                    }
                    else{
                        plasticity_nodes[qpoints*e+q]=1;
                    for (m=0;m<6;m++) {
                      n_trial_stress1[m]=dev_stress1[m]/norm_dev_stress1;
                      n_trial_stress[qpoints*6*e+6*q+m]=n_trial_stress1[m];

                      stress[qpoints*6*e+6*q+m]=trial_stress1[m]-(double)(2*Ge*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

                      hardening[qpoints*6*e+6*q+m]=old_hardening[qpoints*6*e+6*q+m]
                      +(double)(hard_constant_e*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
                    plastic_strain[qpoints*6*e+6*q+m]=old_plastic_strain[qpoints*6*e+6*q+m]
                      +(1*(m<=2)+2*(m>=3))*(double)((norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
                      deviatoric_stress[qpoints*6*e+6*q+m]=dev_stress1[m];
                    }
                    for (m1=0;m1<6;m1++) {
                      for (m2=0;m2<6;m2++) {
                        n_trial_stress_matrix[m2+6*m1]=n_trial_stress1[m1]*n_trial_stress1[m2];
                      }
                  }
                for (m=0;m<36;m++)   {
                  consistent_matrix[qpoints*36*e+36*q+m]=pow(rho_red[e],penal)*C[m]-(double)4*Ge*Ge*DEV[m]/(2*Ge+hard_constant_e)+
                (double)4*Ge*Ge*(DEV[m]-n_trial_stress_matrix[m])*(double)(yield_stress_e/norm_dev_stress1)/(2*Ge+hard_constant_e);
              }
                }
              }
            }
            else{
                for (q=0;q<qpoints;q++){
                  for (m=0;m<36;m++)  {
                consistent_matrix[qpoints*36*e+36*q+m]=yphys_red[e]*pow(rho_red[e],penal)*C1[m];
              }
            }

            }
              }


                    nb_plastic=0;
                    nb_plastic_nodes=0;
                    for(e=0;e<numElems_red;e++){
                      plasticity=0;
                      plasticity_elements[e]=0;
                      for(m=0;m<qpoints;m++){
                      if(plasticity_nodes[qpoints*e+m]==1){
                          //  printf("Element %d  : norm_dev_stress1=%lg, yield_stress=%lg\n",e,norm_dev_stress1,yield_stress);
                        plasticity=1;
                        nb_plastic_nodes++;
                      }
                      }
                      if(plasticity==1) {
                        plasticity_elements[e]=1;
                        nb_plastic++;
                      }

                    }

                  printf("Plasticity : %d elements and %d nodes\n",nb_plastic,nb_plastic_nodes);

                  KE_plasticity3(qpoints,delta_x,delta_y,delta_z,numElems_red,consistent_matrix,
                    BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,Ke_plastic);

                  checkCudaErrors(cudaMemcpy(Ke_plastic_d, Ke_plastic, MNE*MNE * numElems_red *sizeof(double),cudaMemcpyHostToDevice));
                  checkCudaErrors(cudaMemcpy(plasticity_elements_d, plasticity_elements, numElems_red *sizeof(bool),cudaMemcpyHostToDevice));



                  if(forward_problem==1){

                    //FORWARD PROBLEM
                  FE_internal_forces2(qpoints,delta_x,delta_y,delta_z, BE1_t,BE2_t,BE3_t,BE4_t,
                    BE5_t,BE6_t,BE7_t,BE8_t,numElems_red,stress,FE_int);

                    for(n=0;n<numNodes_red;n++) {
                      fint[n]=0;
                      fext_red[n]=0;
                    }

                  for (n=0;n<numNodes_red;n++) {
                     fext_red[n]=fext[map_nodes_short[n]];
                  }


                  for(e=0;e<numElems_red;e++){

                  for(m=0;m<MNE;m++){
                      fint[edofMat_e_red[MNE*e+m]]=fint[edofMat_e_red[MNE*e+m]]+FE_int[MNE*e+m];
                    }
                  }

                  //FILE *f_file=fopen("f.txt","w");
                  for (n=0;n<numNodes_red;n++) {
                    f_red[n]=fext_red[n]-fint[n];
                    //fprintf(f_file,"%lg\n",f[n]);
                          }
                    //fclose(f_file);

                checkCudaErrors(cudaMemcpy(f_red_d, f_red, numNodes_red * sizeof(double),cudaMemcpyHostToDevice));
                checkCudaErrors(cudaMemcpy(fixednodes_red_d, fixednodes_red, numNodes_red * sizeof(int),cudaMemcpyHostToDevice));

                checkCudaErrors(cudaMemset(delta_U_red_d, 0.0, numNodes_red*sizeof(double)));
                checkCudaErrors(cudaMemset(prod_red_d, 0.0, numNodes_red*sizeof(double)));
                checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes_red*sizeof(double)));
                GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1>>>(edofMat_e_red_d,numElems_red,Ke_plastic_d,delta_U_red_d,prod_red_d);

                cudaMemset(M_red_d, 0.0, numNodes_red*sizeof(double));
                GPUMatVec_precond<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red,rho_red_d,
                  yphys_red_d,penal,Ke_elastic_d, M_red_d);

                checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes_red*sizeof(double)));
                rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_red_d, M_red_d,r_red_d,prod_red_d, z_red_d, p_red_d,
                  numNodes_red, fixednodes_red_d);

                cudaMemset(rzold_d, 0.0, 1*sizeof(double));
                Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_red_d, r_red_d,numNodes_red,fixednodes_red_d,rzold_d);

                checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));
                checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
              //  printf("Init : rzold=%lg  \n",rznew[0]);

                rNorm[0]=10000;

                  //==============PCG Iterations Start Here=========================//
                for(iter=0; iter<Max_iters; iter++)
                {

                  checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));
                  GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, Ke_plastic_d,p_red_d, Ap_red_d);

                  Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, Ap_red_d,numNodes_red,fixednodes_red_d,pAp_d,rNorm_d);

                  Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_red_d,delta_U_red_d, r_red_d, z_red_d, p_red_d, Ap_red_d,
                    numNodes_red,fixednodes_red_d, rzold_d,pAp_d,rznew_d,rNorm_d);

                  UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, z_red_d, numNodes_red,fixednodes_red_d,rzold_d,rznew_d,Ap_red_d);

                  switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

                  checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

                  norme=sqrt(rNorm[0]);
                  //if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
                  checkCudaErrors(cudaStreamSynchronize(streams[0]));

                  if(norme <= tol) break;

              }

              if(iter==Max_iters){
                printf("Residual error in PCG: norme=%lg : end of program \n",norme);
                return(0);
              }

              checkCudaErrors(cudaMemset(norm_delta_U_red_d, 0.0, numNodes_red*sizeof(double)));
              checkCudaErrors(cudaMemset(norm_Uold_red_d, 0.0, numNodes_red*sizeof(double)));
              checkCudaErrors(cudaMemset(norm_Unew_red_d, 0.0, numNodes_red*sizeof(double)));

              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d,penal,
                yphys_red_d,Ke_elastic_d,delta_U_red_d, norm_delta_U_red_d);

              checkCudaErrors(cudaMemcpy(delta_U_red, delta_U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_delta_U_red, norm_delta_U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d, penal,
                yphys_red_d,Ke_elastic_d, U_red_d, norm_Uold_red_d);
              checkCudaErrors(cudaMemcpy(Uold_red, U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_Uold_red, norm_Uold_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

              Update_Newton<<<NBLOCKS2,BLOCKSIZE2>>>(numNodes_red,delta_U_red_d,U_red_d);

              GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d, penal,
                yphys_red_d,Ke_elastic_d,U_red_d, norm_Unew_red_d);
              checkCudaErrors(cudaMemcpy(U_red, U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
              checkCudaErrors(cudaMemcpy(norm_Unew_red, norm_Unew_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

              norme1_forward=0;
              norme2_forward=0;
              norme3_forward=0;
              for(m=0;m<numNodes_red;m++) {
                norme1_forward=norme1_forward+delta_U_red[m]*norm_delta_U_red[m];
                norme2_forward=norme2_forward+Uold_red[m]*norm_Uold_red[m];
                norme3_forward=norme3_forward+U_red[m]*norm_Unew_red[m];
              }

              norme1_forward=sqrt(norme1_forward);
              norme2_forward=sqrt(norme2_forward);
              norme3_forward=sqrt(norme3_forward);

              norme_newton_forward=(double) norme1_forward/(norme2_forward+norme3_forward);
              printf("FORWARD solution : Newton steps : %d , Newton residual=%lg, PCG steps : %d, PCG residual=%lg\n",iter_newton,norme_newton_forward, iter,norme);

              //END OF FORWARD PROBLEM
              }



              if(norme_newton_forward <= tol_newton  || norme3_forward<=tol_newton ) break;

              }//END NEWTON

              if(iter_newton==Max_iters_newton){
                printf("Residual error in Newton algorithm: end of program \n");
                return(0);
              }



            for (n=0;n<numNodes_red;n++) U_red_l[numNodes_red*l+n]=U_red[n];

            for (n=0;n<numNodes_red;n++) {
              U[map_nodes_short[n]]=U_red[n];
              //printf("u[%d]=%lg\n",map_nodes_short[n],U[map_nodes_short[n]]);
            }

            int cpt2=0;
          for (i=0;i<(ni+1)*(nj+1)*(nk+1);i++){
          ux[i]=U[cpt2];   cpt2++;
          uy[i]=U[cpt2];   cpt2++;
          uz[i]=U[cpt2];   cpt2++;
          }
          //printf("l=%d\n",l);


              for(m=0;m<6*qpoints*numElems_red;m++){
              old_hardening[m]=hardening[m];
              old_plastic_strain[m]=plastic_strain[m];
              }


              //Gradient Calculation

              somme=0;

              for(e=0;e<numElems;e++){
              stress_xx_e[numElems*l+e]=0;
              stress_yy_e[numElems*l+e]=0;
              stress_zz_e[numElems*l+e]=0;
              stress_xy_e[numElems*l+e]=0;
              stress_yz_e[numElems*l+e]=0;
              stress_xz_e[numElems*l+e]=0;

              for (m=0;m<qpoints;m++){
                  if(map_elements_long[e]>=0){
                stress_xx_e[numElems*l+e]=stress_xx_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+0];
                stress_yy_e[numElems*l+e]=stress_yy_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+1];
                stress_zz_e[numElems*l+e]=stress_zz_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+2];
                stress_xy_e[numElems*l+e]=stress_xy_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+3];
                stress_yz_e[numElems*l+e]=stress_yz_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+4];
                stress_xz_e[numElems*l+e]=stress_xz_e[numElems*l+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+5];
              }
              }
              von_mises_stress[numElems*l+e]=sqrt(stress_xx_e[numElems*l+e]*stress_xx_e[numElems*l+e]+
                stress_yy_e[numElems*l+e]*stress_yy_e[numElems*l+e]+stress_zz_e[numElems*l+e]*stress_zz_e[numElems*l+e]
              -stress_xx_e[numElems*l+e]*stress_yy_e[numElems*l+e]
              -stress_yy_e[numElems*l+e]*stress_zz_e[numElems*l+e]
              -stress_xx_e[numElems*l+e]*stress_zz_e[numElems*l+e]
              +3*stress_xy_e[numElems*l+e]*stress_xy_e[numElems*l+e]
              +3*stress_yz_e[numElems*l+e]*stress_yz_e[numElems*l+e]
              +3*stress_xz_e[numElems*l+e]*stress_xz_e[numElems*l+e]);

              if(e==0) printf("FD : von_mises_stress=%lg\n",von_mises_stress[numElems*l+e]);

                if(map_elements_long[e]>-1) somme=somme+pow(von_mises_stress[numElems*l+e],pnorm);
              }


                if(nodal_force!=0){

                  for (n=0;n<numNodes_red;n++){
                    min_stress=min_stress+(double)fext_red[n]*U_red[n]/loadsteps;
                  }
                }

                //if(i0==0 || i0==59) printf("i0=%d : somme=%.8lg, von_mises_stress[i0]=%.8lg\n",
              //i0,somme,von_mises_stress[i0]);

              }//END LOADSTEPS

            }//END LAYER

            if(nodal_force==0){
            pnorm1=1.f/pnorm;
            somme=(double)somme/numElems_red;
            min_stress=pow(somme,pnorm1);
            printf("min_stress=%lg\n",min_stress);
          }

    if (cpt==1) cost_function1=min_stress;
    if (cpt==2) cost_function2=min_stress;

    cpt++;
    }
        }

    gradient_FD_map[map_elements_short[e2]]=(double)(cost_function1-cost_function2)/(2*eps);
    e2++;

    }

    printf("gradient_FD[0]=%.8lg\n",gradient_FD_map[0]);
    //printf("gradient_FD[1]=%.8lg\n",gradient_FD[1]);
    //printf("gradient_FD[58]=%.8lg\n",gradient_FD[58]);
    //printf("gradient_FD[59]=%.8lg\n",gradient_FD[59]);
    //printf("gradient_FD[600]=%.8lg\n",gradient_FD[600]);
    //printf("gradient_FD[601]=%.8lg\n",gradient_FD[601]);

    checkCudaErrors(cudaMemset(U_red_d, 0.0, numNodes_red*sizeof(double)));

  rho_definition(numElems,domain,rho,volfrac,map_gradient);

  for(e=0;e<numElems_red;e++) {
    rho_red[e]=rho[map_elements_short[e]];
  }

}


for (int opt=0;opt<opt_steps;opt++){

  printf("\n********************************************\n");
  printf("\n          OPTIMIZATION STEP %d              \n",opt);
  printf("\n********************************************\n");

  //CONJUGATE GRADIENT BEGINNING

  //Transfer to GPU

  checkCudaErrors(cudaMemcpy(rho_red_d, rho_red, numElems_red * sizeof(double),cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemset(U_red_d, 0.0, numNodes_red*sizeof(double)));
  checkCudaErrors(cudaMemset(lambda_red_d, 0.0, numNodes_red*sizeof(double)));

  for(m=0;m<6*qpoints*numElems_red;m++){
  old_hardening[m]=0;
  old_plastic_strain[m]=0;
  hardening[m]=0;
  plastic_strain[m]=0;
  stress[m]=0;
  }

  for(m=0;m<qpoints*numElems_red;m++) plasticity_nodes[m]=0;
  for(m=0;m<36*qpoints*numElems_red;m++) consistent_matrix[m]=0;
  for(m=0;m<6*qpoints*numElems_red*loadsteps;m++) strain_ini[m]=0;
  for(m=0;m<6*qpoints*numElems_red*loadsteps;m++) old_plastic_strain_l[m]=0;
  for(m=0;m<6*numElems_red*loadsteps;m++) inh_strain2_l[m]=0;
  for(m=0;m<numElems_red*loadsteps;m++) initial_elements_red_l[m]=0;
  for(m=0;m<36*qpoints*numElems_red*loadsteps;m++) dn_ds_l[m]=0;
  for(m=0;m<36*qpoints*numElems_red*loadsteps;m++) dH_deta_l[m]=0;
  for(m=0;m<qpoints*numElems_red*loadsteps;m++) plasticity_nodes_l[m]=0;

  for (n=0;n<numNodes_red;n++) {
    U_red[n]=0;
  }

  /////////////////////////////////////////////////////////////////////

  for(e=0;e<numElems;e++) yphys[e]=E*Emin;

  for(layer=0;layer<nb_layers;layer++){

    //Layer activation
    for(e=0;e<numElems;e++)    initial_elements[e]=0;
    for(e=0;e<numElems_red;e++)    initial_elements_red[e]=0;

    for(k=layer_height*layer;k<layer_height*(layer+1);k++){
      for(j=0;j<nj;j++){
        for(i=0;i<ni;i++){
          i0=I3D(ni,nj,i,j,k);
          yphys[i0]=E;
          initial_elements[i0]=1;
      }
    }
  }

for(e=0;e<numElems_red;e++) {
  yphys_red[e]=yphys[map_elements_short[e]];
  initial_elements_red[e]=initial_elements[map_elements_short[e]];
}

checkCudaErrors(cudaMemcpy(yphys_red_d, yphys_red, numElems_red * sizeof(double), cudaMemcpyHostToDevice));

  //Displacement update
  for(i=0;i<numNodes_red;i++) Uini_red[i]=U_red[i];

  for(load1=0;load1<loadsteps1;load1++){

    l=loadsteps1*layer+load1;

    for(e=0;e<numElems_red;e++) yphys_red_l[l*numElems_red+e]=yphys_red[e];

    printf("\n\n\n layer %d, load step =%d \n\n\n",layer,load1);

     ratio=(double)(load1+1)/loadsteps1;
     ratio2=ratio;

    for (m=0;m<6;m++) {
      inh_strain2[m]=ratio2*inh_strain[m];

    }

    int iter_newton;

    for(iter_newton=0; iter_newton<Max_iters_newton; iter_newton++)
    {

      for (n=0;n<numNodes;n++) fext[n]=0;

      for(e=0;e<numElems_red;e++){

        initial_elements_red_l[l*numElems_red+e]=initial_elements_red[e];

          Ge=pow(rho_red[e],penal)*G;
          hard_constant_e=pow(rho_red[e],penal)*hard_constant;
          yield_stress_e=pow(rho_red[e],penal)*yield_stress;

      if(e==0) printf("k=%d, layer=%d, layer_height=%d\n",k,layer,layer_height);

        if(yphys_red[e]==E){

        for (m=0;m<MNE;m++) {
          Ue[m]=U_red[edofMat_e_red[MNE*e+m]];
          Ue_ini[m]=Uini_red[edofMat_e_red[MNE*e+m]];
          fext[edofMat_e[MNE*map_nodes_short[e]+m]]+=FE_load[MNE*e+m];
          //if(e==0 || e==49) printf("e=%d : U=%lg\n",e,Ue[m]);
          }

        for (q=0;q<qpoints;q++){

          for (i=0;i<6*3*qpoints;i++){
            BE[i]=BE_total[q*6*3*qpoints+i];
          }

          prodMatVec(3*qpoints,6,3*qpoints,BE,Ue,strain1);
          prodMatVec(3*qpoints,6,3*qpoints,BE,Ue_ini,strain1_ini);


      for (m=0;m<6;m++) {
        if(layer>0){
        if(initial_elements_red[e]==1 && load1==0)    {
          strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m]=strain_ini[6*qpoints*numElems_red*(l-1)+qpoints*6*e+6*q+m]+strain1_ini[m];
        }
        else{
          strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m]=strain_ini[6*qpoints*numElems_red*(l-1)+qpoints*6*e+6*q+m];
        }
        }

        if(layer==0){
          inh_strain2_l[6*numElems_red*l+6*e+m]=inh_strain2[m];
        }
        else{
          inh_strain2_l[6*numElems_red*l+6*e+m]=(initial_elements_red[e]==0)*inh_strain[m]+(initial_elements_red[e]==1)*inh_strain2[m];
        }

        diff_strain1[m]=strain1[m]-old_plastic_strain[qpoints*6*e+6*q+m]-inh_strain2_l[6*numElems_red*l+6*e+m]
        -strain_ini[6*qpoints*numElems_red*l+qpoints*6*e+6*q+m];
      }

      //if(e==0) printf("diff_strain=[%lg %lg %lg %lg %lg %lg]\n",
      //diff_strain1[0],diff_strain1[1],diff_strain1[2],diff_strain1[3],diff_strain1[4],diff_strain1[5]);


      //Stress trial with previous strain
      prodMatVec_e(6,6,6,C,rho_red[e],penal,diff_strain1,trial_stress1);

      //Deviatoric stress : from trial stress
      dev_stress1[0]=trial_stress1[0]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
      dev_stress1[1]=trial_stress1[1]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
      dev_stress1[2]=trial_stress1[2]-(1.f/3)*(trial_stress1[0]+trial_stress1[1]+trial_stress1[2]);
      dev_stress1[3]=trial_stress1[3];
      dev_stress1[4]=trial_stress1[4];
      dev_stress1[5]=trial_stress1[5];

      //Deviatoric stress : Substract hardening Variable
      for (m=0;m<6;m++) {
        dev_stress1[m]=dev_stress1[m]-old_hardening[qpoints*6*e+6*q+m];
      }

      norm_dev_stress1=sqrt(dev_stress1[0]*dev_stress1[0]+dev_stress1[1]*dev_stress1[1]+dev_stress1[2]*dev_stress1[2]
      +2*(dev_stress1[3]*dev_stress1[3]+dev_stress1[4]*dev_stress1[4]+dev_stress1[5]*dev_stress1[5]));

      norm_dev_stress_l[l*qpoints*numElems_red+qpoints*e+q]=norm_dev_stress1;

      //if(e==63 || e==64) printf("q=%d, norm_dev_stress1=%lg\n",q,norm_dev_stress1);

      //Stress update
      if(norm_dev_stress1-yield_stress_e<=0){
        dgamma[qpoints*e+q]=0;

          plasticity_nodes[qpoints*e+q]=0;
          plasticity_nodes_l[numElems_red*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
        for (m=0;m<6;m++)   {
          stress[qpoints*6*e+6*q+m]=trial_stress1[m];
          hardening[qpoints*6*e+6*q+m]=old_hardening[qpoints*6*e+6*q+m];
          plastic_strain[qpoints*6*e+6*q+m]=old_plastic_strain[qpoints*6*e+6*q+m];
        }
        for (m=0;m<36;m++)  consistent_matrix[qpoints*36*e+36*q+m]=pow(rho_red[e],penal)*C[m];
        for (m=0;m<6;m++)  deviatoric_stress[qpoints*6*e+6*q+m]=dev_stress1[m];
      }
      else{
          plasticity_nodes[qpoints*e+q]=1;
          plasticity_nodes_l[numElems_red*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
          //dgamma[qpoints*e+q]=(norm_dev_stress1-yield_stress_e)/(2*Ge+hard_constant_e);
          //if(e==1) printf("q=%d, dgamma=%lg\n",q,dgamma[qpoints*e+q]);
      for (m=0;m<6;m++) {
        n_trial_stress1[m]=dev_stress1[m]/norm_dev_stress1;
        n_trial_stress[qpoints*6*e+6*q+m]=n_trial_stress1[m];
        n_trial_stress_l[l*numElems_red*qpoints*6+qpoints*6*e+6*q+m]=n_trial_stress[qpoints*6*e+6*q+m];

        stress[qpoints*6*e+6*q+m]=trial_stress1[m]-(double)(2*Ge*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

        deviatoric_stress[qpoints*6*e+6*q+m]=dev_stress1[m];
        hardening[qpoints*6*e+6*q+m]=old_hardening[qpoints*6*e+6*q+m]
        +(double)(hard_constant_e*(norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);
        plastic_strain[qpoints*6*e+6*q+m]=old_plastic_strain[qpoints*6*e+6*q+m]
        +(1*(m<=2)+2*(m>=3))*(double)((norm_dev_stress1-yield_stress_e)*n_trial_stress1[m])/(2*Ge+hard_constant_e);

      }
      n1=n_trial_stress[qpoints*6*e+6*q+0];
      n2=n_trial_stress[qpoints*6*e+6*q+1];
      n3=n_trial_stress[qpoints*6*e+6*q+2];
      n4=n_trial_stress[qpoints*6*e+6*q+3];
      n5=n_trial_stress[qpoints*6*e+6*q+4];
      n6=n_trial_stress[qpoints*6*e+6*q+5];

      dp1_ds1=1-yield_stress_e*(1-n1*n1)/norm_dev_stress1;
      dp1_ds2=yield_stress_e*n1*n2/norm_dev_stress1;
      dp1_ds3=yield_stress_e*n1*n3/norm_dev_stress1;
      dp1_ds4=2*yield_stress_e*n1*n4/norm_dev_stress1;
      dp1_ds5=2*yield_stress_e*n1*n5/norm_dev_stress1;
      dp1_ds6=2*yield_stress_e*n1*n6/norm_dev_stress1;

      dp2_ds1=yield_stress_e*n2*n1/norm_dev_stress1;
      dp2_ds2=1-yield_stress_e*(1-n2*n2)/norm_dev_stress1;
      dp2_ds3=yield_stress_e*n2*n3/norm_dev_stress1;
      dp2_ds4=2*yield_stress_e*n2*n4/norm_dev_stress1;
      dp2_ds5=2*yield_stress_e*n2*n5/norm_dev_stress1;
      dp2_ds6=2*yield_stress_e*n2*n6/norm_dev_stress1;

      dp3_ds1=yield_stress_e*n3*n1/norm_dev_stress1;
      dp3_ds2=yield_stress_e*n3*n2/norm_dev_stress1;
      dp3_ds3=1-yield_stress_e*(1-n3*n3)/norm_dev_stress1;
      dp3_ds4=2*yield_stress_e*n3*n4/norm_dev_stress1;
      dp3_ds5=2*yield_stress_e*n3*n5/norm_dev_stress1;
      dp3_ds6=2*yield_stress_e*n3*n6/norm_dev_stress1;

      dp4_ds1=yield_stress_e*n4*n1/norm_dev_stress1;
      dp4_ds2=yield_stress_e*n4*n2/norm_dev_stress1;
      dp4_ds3=yield_stress_e*n4*n3/norm_dev_stress1;
      dp4_ds4=1-yield_stress_e*((1-2*n4*n4)/norm_dev_stress1);
      dp4_ds5=2*yield_stress_e*n4*n5/norm_dev_stress1;
      dp4_ds6=2*yield_stress_e*n4*n6/norm_dev_stress1;

      dp5_ds1=yield_stress_e*n5*n1/norm_dev_stress1;
      dp5_ds2=yield_stress_e*n5*n2/norm_dev_stress1;
      dp5_ds3=yield_stress_e*n5*n3/norm_dev_stress1;
      dp5_ds4=2*yield_stress_e*n5*n4/norm_dev_stress1;
      dp5_ds5=1-yield_stress_e*((1-2*n5*n5)/norm_dev_stress1);
      dp5_ds6=2*yield_stress_e*n5*n6/norm_dev_stress1;

      dp6_ds1=yield_stress_e*n6*n1/norm_dev_stress1;
      dp6_ds2=yield_stress_e*n6*n2/norm_dev_stress1;
      dp6_ds3=yield_stress_e*n6*n3/norm_dev_stress1;
      dp6_ds4=2*yield_stress_e*n6*n4/norm_dev_stress1;
      dp6_ds5=2*yield_stress_e*n6*n5/norm_dev_stress1;
      dp6_ds6=1-yield_stress_e*((1-2*n6*n6)/norm_dev_stress1);

      ds1_dstr1=2.f/3;  ds2_dstr1=-1.f/3; ds3_dstr1=-1.f/3;
      ds1_dstr2=-1.f/3; ds2_dstr2=2.f/3;  ds3_dstr2=-1.f/3;
      ds1_dstr3=-1.f/3; ds2_dstr3=-1.f/3; ds3_dstr3=2.f/3;
      ds4_dstr4=1; ds5_dstr5=1; ds6_dstr6=1;

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+0]=dp1_ds1*ds1_dstr1+dp1_ds2*ds2_dstr1+dp1_ds3*ds3_dstr1; //dp1_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+1]=dp1_ds1*ds1_dstr2+dp1_ds2*ds2_dstr2+dp1_ds3*ds3_dstr2; //dp1_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+2]=dp1_ds1*ds1_dstr3+dp1_ds2*ds2_dstr3+dp1_ds3*ds3_dstr3; //dp1_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+3]=dp1_ds4*ds4_dstr4; //dp1_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+4]=dp1_ds5*ds5_dstr5; //dp1_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+5]=dp1_ds6*ds6_dstr6; //dp1_ds6

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+0]=dp2_ds1*ds1_dstr1+dp2_ds2*ds2_dstr1+dp2_ds3*ds3_dstr1; //dp2_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+1]=dp2_ds1*ds1_dstr2+dp2_ds2*ds2_dstr2+dp2_ds3*ds3_dstr2; //dp2_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+2]=dp2_ds1*ds1_dstr3+dp2_ds2*ds2_dstr3+dp2_ds3*ds3_dstr3; //dp2_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+3]=dp2_ds4*ds4_dstr4; //dp2_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+4]=dp2_ds5*ds5_dstr5; //dp2_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+5]=dp2_ds6*ds6_dstr6; //dp2_ds6

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+0]=dp3_ds1*ds1_dstr1+dp3_ds2*ds2_dstr1+dp3_ds3*ds3_dstr1; //dp3_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+1]=dp3_ds1*ds1_dstr2+dp3_ds2*ds2_dstr2+dp3_ds3*ds3_dstr2; //dp3_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+2]=dp3_ds1*ds1_dstr3+dp3_ds2*ds2_dstr3+dp3_ds3*ds3_dstr3; //dp3_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+3]=dp3_ds4*ds4_dstr4; //dp3_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+4]=dp3_ds5*ds5_dstr5; //dp3_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+5]=dp3_ds6*ds6_dstr6; //dp3_ds6

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+0]=dp4_ds1*ds1_dstr1+dp4_ds2*ds2_dstr1+dp4_ds3*ds3_dstr1; //dp4_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+1]=dp4_ds1*ds1_dstr2+dp4_ds2*ds2_dstr2+dp4_ds3*ds3_dstr2; //dp4_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+2]=dp4_ds1*ds1_dstr3+dp4_ds2*ds2_dstr3+dp4_ds3*ds3_dstr3; //dp4_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+3]=dp4_ds4*ds4_dstr4; //dp4_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+4]=dp4_ds5*ds5_dstr5; //dp4_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+5]=dp4_ds6*ds6_dstr6; //dp4_ds6

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+0]=dp5_ds1*ds1_dstr1+dp5_ds2*ds2_dstr1+dp5_ds3*ds3_dstr1; //dp5_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+1]=dp5_ds1*ds1_dstr2+dp5_ds2*ds2_dstr2+dp5_ds3*ds3_dstr2; //dp5_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+2]=dp5_ds1*ds1_dstr3+dp5_ds2*ds2_dstr3+dp5_ds3*ds3_dstr3; //dp5_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+3]=dp5_ds4*ds4_dstr4; //dp5_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+4]=dp5_ds5*ds5_dstr5; //dp5_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+5]=dp5_ds6*ds6_dstr6; //dp5_ds6

      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+0]=dp6_ds1*ds1_dstr1+dp6_ds2*ds2_dstr1+dp6_ds3*ds3_dstr1; //dp6_ds1
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+1]=dp6_ds1*ds1_dstr2+dp6_ds2*ds2_dstr2+dp6_ds3*ds3_dstr2; //dp6_ds2
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+2]=dp6_ds1*ds1_dstr3+dp6_ds2*ds2_dstr3+dp6_ds3*ds3_dstr3; //dp6_ds3
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+3]=dp6_ds4*ds4_dstr4; //dp6_ds4
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+4]=dp6_ds5*ds5_dstr5; //dp6_ds5
      dn_ds_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+5]=dp6_ds6*ds6_dstr6; //dp6_ds6


      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+0]=dp1_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+1]=dp1_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+2]=dp1_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+3]=dp1_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+4]=dp1_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*0+5]=dp1_ds6;

      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+0]=dp2_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+1]=dp2_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+2]=dp2_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+3]=dp2_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+4]=dp2_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*1+5]=dp2_ds6;

      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+0]=dp3_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+1]=dp3_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+2]=dp3_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+3]=dp3_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+4]=dp3_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*2+5]=dp3_ds6;

      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+0]=dp4_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+1]=dp4_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+2]=dp4_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+3]=dp4_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+4]=dp4_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*3+5]=dp4_ds6;

      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+0]=dp5_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+1]=dp5_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+2]=dp5_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+3]=dp5_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+4]=dp5_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*4+5]=dp5_ds6;

      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+0]=dp6_ds1;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+1]=dp6_ds2;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+2]=dp6_ds3;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+3]=dp6_ds4;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+4]=dp6_ds5;
      dH_deta_l[36*qpoints*numElems_red*l+36*qpoints*e+36*q+6*5+5]=dp6_ds6;

    for (m1=0;m1<6;m1++) {
        for (m2=0;m2<6;m2++) {
          n_trial_stress_matrix[m2+6*m1]=n_trial_stress1[m1]*n_trial_stress1[m2];
      }
    }
  for (m=0;m<36;m++)   {
    consistent_matrix[qpoints*36*e+36*q+m]=pow(rho_red[e],penal)*C[m]-(double)4*Ge*Ge*DEV[m]/(2*Ge+hard_constant_e)+
  (double)4*Ge*Ge*(DEV[m]-n_trial_stress_matrix[m])*(double)(yield_stress_e/norm_dev_stress1)/(2*Ge+hard_constant_e);
}
  }
}
      }

        else{
          for (q=0;q<qpoints;q++){
            for (m=0;m<36;m++)    consistent_matrix[qpoints*36*e+36*q+m]=pow(rho_red[e],penal)*yphys_red[e]*C1[m];
            plasticity_nodes[qpoints*e+q]=0;
            plasticity_nodes_l[numElems_red*qpoints*l+qpoints*e+q]=plasticity_nodes[qpoints*e+q];
          }
      }
    }

      nb_plastic=0;
      nb_plastic_nodes=0;
      for(e=0;e<numElems_red;e++){

        //if(e==0 || e==49) printf("e=%d : stress=[%lg %lg %lg %lg %lg %lg %lg %lg]\n",e,stress[qpoints*6*e+6*0+0],stress[qpoints*6*e+6*1+0],stress[qpoints*6*e+6*2+0],
      //stress[qpoints*6*e+6*3+0],stress[qpoints*6*e+6*4+0],stress[qpoints*6*e+6*5+0],stress[qpoints*6*e+6*6+0],stress[qpoints*6*e+6*7+0]);
        plasticity=0;
        plasticity_elements[e]=0;
        for(m=0;m<qpoints;m++){
        if(plasticity_nodes[qpoints*e+m]==1){
            //  printf("Element %d  : norm_dev_stress1=%lg, yield_stress=%lg\n",e,norm_dev_stress1,yield_stress);
          plasticity=1;
          nb_plastic_nodes++;
        }
        }
        if(plasticity==1) {
          plasticity_elements[e]=1;
          nb_plastic++;
        }

      }

    printf("Plasticity : %d elements and %d nodes\n",nb_plastic,nb_plastic_nodes);


    KE_plasticity3(qpoints,delta_x,delta_y,delta_z,numElems_red,consistent_matrix,
      BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,Ke_plastic);

    checkCudaErrors(cudaMemcpy(Ke_plastic_d, Ke_plastic, MNE*MNE * numElems_red *sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(plasticity_elements_d, plasticity_elements, numElems_red *sizeof(bool),cudaMemcpyHostToDevice));


    if(forward_problem==1){

      //FORWARD PROBLEM
    FE_internal_forces2(qpoints,delta_x,delta_y,delta_z,BE1_t,BE2_t,BE3_t,BE4_t,
      BE5_t,BE6_t,BE7_t,BE8_t,numElems_red,stress,FE_int);

      for (n=0;n<numNodes_red;n++) {
       fext_red[n]=fext[map_nodes_short[n]];
      }

    for(n=0;n<numNodes_red;n++) fint[n]=0;

    for(e=0;e<numElems_red;e++){

      for(m=0;m<MNE;m++){
        fint[edofMat_e_red[MNE*e+m]]=fint[edofMat_e_red[MNE*e+m]]+FE_int[MNE*e+m];
        //if(e==0 || e==49) printf("e=%d : FE_int=%lg\n",e,FE_int[MNE*e+m]);
        }
    }


    //FILE *f_file=fopen("f.txt","w");
    for (n=0;n<numNodes_red;n++) {
      f_red[n]=fext_red[n]-fint[n];
      //fprintf(f_file,"%lg %d\n",f[n],fixednodes[n]);
            }
      //fclose(f_file);



    checkCudaErrors(cudaMemcpy(f_red_d, f_red, numNodes_red * sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(fixednodes_red_d, fixednodes_red, numNodes_red * sizeof(int),cudaMemcpyHostToDevice));

  checkCudaErrors(cudaMemset(delta_U_red_d, 0.0, numNodes_red*sizeof(double)));
  checkCudaErrors(cudaMemset(prod_red_d, 0.0, numNodes_red*sizeof(double)));
  checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes_red*sizeof(double)));
  GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1>>>(edofMat_e_red_d,numElems_red,Ke_plastic_d,delta_U_red_d,prod_red_d);

  cudaMemset(M_red_d, 0.0, numNodes_red*sizeof(double));
  GPUMatVec_precond<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red,rho_red_d,
    yphys_red_d,penal,Ke_elastic_d, M_red_d);

  checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes_red*sizeof(double)));
  rzp_calculation<<<NBLOCKS2,BLOCKSIZE2>>>(f_red_d, M_red_d,r_red_d,prod_red_d, z_red_d, p_red_d,
    numNodes_red, fixednodes_red_d);

  cudaMemset(rzold_d, 0.0, 1*sizeof(double));
  Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_red_d, r_red_d,numNodes_red,fixednodes_red_d,rzold_d);

  checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));
  checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
  printf("Init : rzold=%lg  \n",rznew[0]);

  rNorm[0]=10000;

    //==============PCG Iterations Start Here=========================//
  for(iter=0; iter<Max_iters; iter++)
  {

    checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));

    GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, Ke_plastic_d,p_red_d, Ap_red_d);

    Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, Ap_red_d,numNodes_red,fixednodes_red_d,pAp_d,rNorm_d);

    Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_red_d,delta_U_red_d, r_red_d, z_red_d, p_red_d, Ap_red_d,
      numNodes_red,fixednodes_red_d, rzold_d,pAp_d,rznew_d,rNorm_d);

    UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, z_red_d, numNodes_red,fixednodes_red_d,rzold_d,rznew_d,Ap_red_d);

    switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

    checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

    norme=sqrt(rNorm[0]);
    if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
    checkCudaErrors(cudaStreamSynchronize(streams[0]));

    if(norme <= tol) break;

}

if(iter==Max_iters){
  printf("Residual error in PCG: norme=%lg : end of program \n",norme);
  return(0);
}

checkCudaErrors(cudaMemset(norm_delta_U_red_d, 0.0, numNodes_red*sizeof(double)));
checkCudaErrors(cudaMemset(norm_Uold_red_d, 0.0, numNodes_red*sizeof(double)));
checkCudaErrors(cudaMemset(norm_Unew_red_d, 0.0, numNodes_red*sizeof(double)));

GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d,penal,
  yphys_red_d,Ke_elastic_d,delta_U_red_d, norm_delta_U_red_d);

checkCudaErrors(cudaMemcpy(delta_U_red, delta_U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_delta_U_red, norm_delta_U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d, penal,
  yphys_red_d,Ke_elastic_d, U_red_d, norm_Uold_red_d);
checkCudaErrors(cudaMemcpy(Uold_red, U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_Uold_red, norm_Uold_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

Update_Newton<<<NBLOCKS2,BLOCKSIZE2>>>(numNodes_red,delta_U_red_d,U_red_d);

GPUMatVec_elastic<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, rho_red_d, penal,
  yphys_red_d,Ke_elastic_d,U_red_d, norm_Unew_red_d);
checkCudaErrors(cudaMemcpy(U_red, U_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(norm_Unew_red, norm_Unew_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

norme1_forward=0;
norme2_forward=0;
norme3_forward=0;
for(m=0;m<numNodes_red;m++) {
  norme1_forward=norme1_forward+delta_U_red[m]*norm_delta_U_red[m];
  norme2_forward=norme2_forward+Uold_red[m]*norm_Uold_red[m];
  norme3_forward=norme3_forward+U_red[m]*norm_Unew_red[m];
}

norme1_forward=sqrt(norme1_forward);
norme2_forward=sqrt(norme2_forward);
norme3_forward=sqrt(norme3_forward);

norme_newton_forward=(double) norme1_forward/(norme2_forward+norme3_forward);
printf("norme_newton=%lg : norme du=%lg : norme Uold=%lg : norme Unew=%lg\n",norme_newton_forward,norme1_forward,norme2_forward,norme3_forward);
printf("FORWARD solution : Newton steps : %d , Newton residual=%lg, PCG steps : %d, PCG residual=%lg\n",iter_newton,norme_newton_forward, iter,norme);


//END OF FORWARD PROBLEM
}


if(norme_newton_forward <= tol_newton  || norme3_forward<=tol_newton ) break;

}//END NEWTON

if(iter_newton==Max_iters_newton){
  printf("Residual error in Newton algorithm: end of program \n");
  return(0);
}

for (n=0;n<numNodes_red;n++) U_red_l[numNodes_red*l+n]=U_red[n];

for (n=0;n<numNodes_red;n++) {
  U[map_nodes_short[n]]=U_red[n];
  //printf("u[%d]=%lg\n",map_nodes_short[n],U[map_nodes_short[n]]);
}

/*printf("e=0\n");
for (m=0;m<MNE;m++){
  printf("U=%lg\n",U_red[edofMat_e_red[0*MNE+m]]);
}
printf("e=49\n");
for (m=0;m<MNE;m++){
  printf("U=%lg\n",U_red[edofMat_e_red[49*MNE+m]]);
}*/

//printf("n=%d,U[2]=%lg, U[2442]=%lg, U[3]=%lg, U[2443]=%lg\n",numNodes,U[2],U[2442],U[3],U[2443]);

double min_vms=1e15;
double max_vms=-1e-15;

for(e=0;e<numElems;e++){
  stress_xx_e[numElems*l+e]=0;
  stress_yy_e[numElems*l+e]=0;
  stress_zz_e[numElems*l+e]=0;
  stress_xy_e[numElems*l+e]=0;
  stress_yz_e[numElems*l+e]=0;
  stress_xz_e[numElems*l+e]=0;
  dev_stress_xx_e[e]=0;
  dev_stress_yy_e[e]=0;
  dev_stress_zz_e[e]=0;
  dev_stress_xy_e[e]=0;
  dev_stress_yz_e[e]=0;
  dev_stress_xz_e[e]=0;

  //printf("map_elements_long[%d]=%d\n",e,map_elements_long[e]);

  for (m=0;m<qpoints;m++){
    if(map_elements_long[e]>=0){
  stress_xx_e[l*numElems+e]=stress_xx_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+0];
  stress_yy_e[l*numElems+e]=stress_yy_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+1];
  stress_zz_e[l*numElems+e]=stress_zz_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+2];
  stress_xy_e[l*numElems+e]=stress_xy_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+3];
  stress_yz_e[l*numElems+e]=stress_yz_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+4];
  stress_xz_e[l*numElems+e]=stress_xz_e[l*numElems+e]+0.125*stress[qpoints*6*map_elements_long[e]+6*m+5];
}
}

von_mises_stress[numElems*l+e]=sqrt(stress_xx_e[numElems*l+e]*stress_xx_e[numElems*l+e]+
  stress_yy_e[numElems*l+e]*stress_yy_e[numElems*l+e]+stress_zz_e[numElems*l+e]*stress_zz_e[numElems*l+e]
-stress_xx_e[numElems*l+e]*stress_yy_e[numElems*l+e]
-stress_yy_e[numElems*l+e]*stress_zz_e[numElems*l+e]
-stress_xx_e[numElems*l+e]*stress_zz_e[numElems*l+e]
+3*stress_xy_e[numElems*l+e]*stress_xy_e[numElems*l+e]
+3*stress_yz_e[numElems*l+e]*stress_yz_e[numElems*l+e]
+3*stress_xz_e[numElems*l+e]*stress_xz_e[numElems*l+e]);


if(von_mises_stress[l*numElems+e]<min_vms) min_vms=von_mises_stress[l*numElems+e];
if(von_mises_stress[l*numElems+e]>max_vms) max_vms=von_mises_stress[l*numElems+e];
}
  printf("min_vms=%lg and max_vms=%lg\n",min_vms,max_vms);

for(m=0;m<6*qpoints*numElems_red;m++){
  old_plastic_strain_l[numElems_red*6*qpoints*l+m]=old_plastic_strain[m];
}

  for(m=0;m<6*qpoints*numElems_red;m++){
  old_hardening[m]=hardening[m];
  old_plastic_strain[m]=plastic_strain[m];
  }

  }//END LOADSTEPS

}


double somme=0;
printf("l=%d\n",l);

for (e=0;e<numElems;e++)  gradient2_map[e]=0;

for (e=0;e<numElems;e++) {
  if(map_elements_long[e]>-1)  somme=somme+pow(von_mises_stress[l*numElems+e],pnorm);

}

double max_uz;

if(nodal_force==0 && pnorm_stress==1){
pnorm1=1.f/pnorm;
somme=(double)somme/numElems_red;
min_stress=pow(somme,pnorm1);
printf("min_stress=%lg\n",min_stress);
}
else{
min_stress=0;
max_uz=-8e4;
for (n=2;n<numNodes_red;n=n+3){
//  min_stress=min_stress+fext_red[n]*U_red[n];
min_stress=min_stress+U_red[n]*U_red[n];
if(Abs(U_red[n])>max_uz) max_uz=Abs(U_red[n]);
}
}
printf("max_uz=%lg\n",max_uz);
cost_function[opt]=min_stress;

  //Check old plastic strain value, no derivative if old plastic strain=0
  for(l=0;l<loadsteps;l++){
    old_pstrain[l]=0;
      for(m=0;m<6*qpoints*numElems_red;m++){
          if(old_plastic_strain_l[6*qpoints*numElems_red*l+m]!=0 ) old_pstrain[l]=1;
        }
      printf("old_pstrain[%d]=%d\n",l,old_pstrain[l]);
  }

  if(gradient_calculation==1){

  for(layer=nb_layers-1;layer>=0;layer--){

  for(load1=loadsteps1-1;load1>=0;load1--)
  {
    l=loadsteps1*layer+load1;

    ratio=(double)(load1+1)/loadsteps1;

    //Gradient calculation NEWTON

  //ADJOINT STIFFNESS Matrix

        somme=0;
        if(l==loadsteps-1){
        for(e=0;e<numElems;e++) {
          if(map_elements_long[e]>-1) somme=somme+pow(von_mises_stress[l*numElems+e],pnorm);
        }
        pnorm1=1.f/pnorm-1;
        somme=somme/numElems_red;
      }

        if(somme!=0) dc_dvm1=pow(somme,pnorm1);
        if(somme==0) dc_dvm1=0;


  for(e=0;e<MNE*numElems_red;e++) {
    rhs[e]=0;
    rhs_j[e]=0;
  }

double l1_loop_time=get_time();

double first_loop_time=get_time();

adjoint_K(l,loadsteps,numElems,numElems_red,qpoints,rho_red,penal,G,yield_stress,hard_constant,ID_mat,dn_ds_l,yphys_red_l,C1,delta_x,delta_y,delta_z,
  weight1,weight2,weight3,weight4,weight5,weight6,weight7,weight8,
BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,BE1_t,BE2_t,BE3_t,BE4_t,BE5_t,BE6_t,BE7_t,BE8_t,
plasticity_nodes_l,nodal_force,pnorm_stress,dc_dvm, dvm_dwe, dc_dvm1, von_mises_stress, stress_xx_e,stress_yy_e, stress_zz_e,
stress_xy_e,stress_yz_e,stress_xz_e,pnorm, dc_due,plasticity_nodes,Ke2_e,FE_load,map_elements_short,U_red,edofMat_e_red);


printf("first loop time =%f s \n",get_time()-first_loop_time);

//dc_dvm2 calculation
for (l1=l+1;l1<loadsteps;l1++){

  somme=0;
  if(l1==loadsteps-1){
  for(int e1=0;e1<numElems;e1++) {
      if(map_elements_long[e1]>-1) somme=somme+pow(von_mises_stress[l1*numElems+e1],pnorm);
  }
  pnorm1=1.f/pnorm-1;
  somme=somme/numElems_red;
  }

  if(somme!=0) dc_dvm2[l1]=pow(somme,pnorm1);
  if(somme==0) dc_dvm2[l1]=0;
}

double second_loop_time=get_time();

adjoint_rhs(edofMat_e_red,qpoints,delta_x,delta_y,delta_z,l,load1,loadsteps1,loadsteps,nodal_force,numElems,numElems_red,numNodes_red,penal,hard_constant,yield_stress,yphys_red_l,DEV_mat,
C1,G,rho_red,BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,BE1_t,BE2_t,BE3_t,BE4_t,BE5_t,BE6_t,BE7_t,BE8_t,
weight1,weight2,weight3,weight4,weight5,weight6,weight7,weight8,von_mises_stress,stress_xx_e,stress_yy_e,stress_zz_e,stress_xy_e,stress_yz_e,stress_xz_e, pnorm,
dH_deta_l,plasticity_nodes_l, initial_elements_red_l, lambda_red_l, rhs,rhs_j,pnorm_stress,dc_dvm,dvm_dwe,dc_dvm2,old_pstrain,map_elements_short);

printf("second loop time =%f s \n",get_time()-second_loop_time);

double adjoint_time=get_time();

checkCudaErrors(cudaMemcpy(Ke2_d, Ke2_e, numElems_red*MNE*MNE * sizeof(double),cudaMemcpyHostToDevice));

  //ADJOINT PROBLEM

    //lambda
    for(n=0;n<numNodes_red;n++) f_red[n]=0;


    for(e=0;e<numElems_red;e++){

      for(m=0;m<24;m++){
        f_red[edofMat_e_red[MNE*e+m]]=f_red[edofMat_e_red[MNE*e+m]]-dc_due[24*numElems_red*l+24*e+m]-rhs[24*e+m]-rhs_j[24*e+m];
        rhs_l[numNodes_red*l+edofMat_e_red[MNE*e+m]]=rhs_l[numNodes_red*l+edofMat_e_red[MNE*e+m]]+rhs[24*e+m]+rhs_j[24*e+m];
        }
    }

    checkCudaErrors(cudaMemcpy(f_red_d, f_red, numNodes_red * sizeof(double),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemset(delta_lambda_red_d, 0.0, numNodes_red*sizeof(double)));

    bool conjugate_gradient=0;
    for(n=0;n<numNodes_red;n++){
      if(f_red[n]!=0) conjugate_gradient=1;
    }

    if(conjugate_gradient==1){
    checkCudaErrors(cudaMemset(prod_red_d, 0.0, numNodes_red*sizeof(double)));
    checkCudaErrors(cudaMemset(r_red_d, 0.0, numNodes_red*sizeof(double)));
    checkCudaErrors(cudaMemset(p_red_d, 0.0, numNodes_red*sizeof(double)));
    checkCudaErrors(cudaMemset(z_red_d, 0.0, numNodes_red*sizeof(double)));
    checkCudaErrors(cudaMemset(rznew_d, 0.0, sizeof(double)));
    checkCudaErrors(cudaMemset(rNorm_d, 0.0, sizeof(double)));
    checkCudaErrors(cudaMemset(pAp_d, 0.0, sizeof(double)));

    GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d,numElems_red,Ke2_d,delta_lambda_red_d,prod_red_d);

    cudaMemset(M_red_d, 0.0, numNodes_red*sizeof(double));
    GPUMatVec_precond2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, Ke2_d, M_red_d);

    rzp_calculation<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(f_red_d, M_red_d, r_red_d,prod_red_d, z_red_d, p_red_d, numNodes_red, fixednodes_red_d);

    cudaMemset(rzold_d, 0.0, 1*sizeof(double));
    Prod2_rz<<<NBLOCKS2,BLOCKSIZE2>>>(p_red_d, r_red_d,numNodes_red,fixednodes_red_d,rzold_d);

    checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));
    checkCudaErrors(cudaMemcpyAsync(rznew, rzold_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));
    //printf("rzold=%lg\n",rznew[0]);
    //rNorm[0]=10000;

    for(iter=0; iter<Max_iters; iter++)
    {
      checkCudaErrors(cudaMemset(Ap_red_d, 0.0, numNodes_red*sizeof(double)));
      GPUMatVec_2<<<NBLOCKS,BLOCKSIZE1,0,streams[0]>>>(edofMat_e_red_d, numElems_red, Ke2_d,p_red_d, Ap_red_d);

      Prod3_rz<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, Ap_red_d,numNodes_red,fixednodes_red_d,pAp_d,rNorm_d);

      Update_UR<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(M_red_d,delta_lambda_red_d, r_red_d, z_red_d, p_red_d, Ap_red_d,numNodes_red,fixednodes_red_d, rzold_d,pAp_d,rznew_d,rNorm_d);

      UpdateVec<<<NBLOCKS2,BLOCKSIZE2,0,streams[0]>>>(p_red_d, z_red_d, numNodes_red,fixednodes_red_d,rzold_d,rznew_d,Ap_red_d);

      switch_r<<<1,1,0,streams[0]>>>(rzold_d,rznew_d,pAp_d);

      checkCudaErrors(cudaMemcpyAsync(rNorm, rNorm_d, 1 * sizeof(double),cudaMemcpyDeviceToHost,streams[0]));

      norme=sqrt(rNorm[0]);
      if(iter%10000==0 && iter>10000) printf("iter=%d : norme=%lg\n",iter,norme);
      checkCudaErrors(cudaStreamSynchronize(streams[0]));

      if(norme <= tol) break;

    }

    if(iter==Max_iters){
    printf("Residual error in PCG adjoint: norme=%lg : end of program \n",norme);
    return(0);
    }

    printf("ADJOINT solution : loadstep=%d, PCG steps : %d, PCG residual=%lg\n", l,iter,norme);

    }
    else{
      printf("WARNING : Null right hand side : the adjoint conjugate gradient loop has been skipped : normal behavior when : objective function not depending on this step, 1st layer, no plastic strain\n");
    }
  //END OF ADJOINT PROBLEM

checkCudaErrors(cudaMemcpy(lambda_red, delta_lambda_red_d, numNodes_red * sizeof(double), cudaMemcpyDeviceToHost));

for (n=0;n<numNodes_red;n++) lambda_red_l[numNodes_red*l+n]=lambda_red[n];

printf("adjoint time =%f s \n",get_time()-adjoint_time);

  }//END LOADSTEPS

}

  for(l=0;l<loadsteps;l++)
  {

    printf("gradient calculation : l=%d/%d\n",l+1,loadsteps);

    somme=0;
    if(l==loadsteps-1){
    for(e=0;e<numElems;e++) {
      if(map_elements_long[e]>-1) somme=somme+pow(von_mises_stress[l*numElems+e],pnorm);
    }


    pnorm1=1.f/pnorm-1;
    somme=somme/numElems_red;
  }

    if(somme!=0) dc_dvm1=pow(somme,pnorm1);
    if(somme==0) dc_dvm1=0;


x_derivatives(edofMat_e_red,l,loadsteps,numNodes_red,numElems,numElems_red,qpoints,U_red_l,lambda_red_l,ni,nj,rho_red,penal,yield_stress,G,hard_constant,yphys_red_l,C1,ID_mat,DEV_mat,
inh_strain2_l,old_plastic_strain_l,strain_ini,delta_x,delta_y,delta_z, weight1,weight2,weight3,weight4,weight5,weight6,weight7,weight8,
BE1,BE2,BE3,BE4,BE5,BE6,BE7,BE8,BE1_t,BE2_t,BE3_t,BE4_t,BE5_t,BE6_t,BE7_t,BE8_t,old_pstrain,plasticity_nodes_l,dH_deta_l, norm_dev_stress_l, n_trial_stress_l,
von_mises_stress,stress_xx_e,stress_yy_e,stress_zz_e,stress_xy_e,stress_yz_e,stress_xz_e,pnorm_stress,dc_dvm1,pnorm,dc_dvm,dvm_dwe,nodal_force,dc_dxe,gradient2_l, gradient2_map,map_elements_short);


  }//END LOADSTEPS

}

//Filtering

for (e=0;e<numElems;e++) {
  if(map_gradient[e]>-1){
    gradient2[map_gradient[e]]=1e5*gradient2_map[e];

  }
}

filter(ni, nj, nk, rmin, rho_opt, gradient2, gradient,map_gradient);

//Density Update
sommeV=0;

double max_gradient=0;
for (e=0;e<numElems_opt;e++) {
  if(abs(gradient[e])>max_gradient) max_gradient=abs(gradient[e]);
}


// Set outer move limits
for (e=0;e<numElems_opt;e++) {
	xmax[e] = Mini(Xmax, rho_opt[e] + movlim);
	xmin[e] = Maxi(Xmin, rho_opt[e] - movlim);
  sommeV=sommeV+rho_opt[e];
  dg[e]=1.f/numElems_opt;
}

//for (e=0;e<numElems_opt;e++) gradient[e]=(double)gradient[e]/Abs(max_gradient);

//printf("max gradient=[%lg]\n",max_gradient);

g[0]=(double) sommeV/numElems_opt-volfrac;
printf("g[0]=%lg\n",g[0]);
constraint[opt]=(double) sommeV/numElems_opt;

for (e=0;e<=opt;e++) printf("step %d : cost_function=%lg, vol.fraction=%lg\n",e,cost_function[e],constraint[e]);


cost_function_file=fopen("min_stress.txt","a");

//for (i=0;i<opt_steps;i++){
fprintf(cost_function_file,"%d %lg %lg\n",opt,cost_function[opt],constraint[opt]);
//}

fclose(cost_function_file);

mma->Update(rho_opt,gradient,g,dg,xmin,xmax);

sommeV=0;
for (e=0;e<numElems_opt;e++) sommeV=sommeV+rho_opt[e];
printf("vol_frac after=%f\n",(double) sommeV/numElems_opt);


for (e=0;e<numElems_opt;e++) {
  rho_old_opt[e]=rho_opt[e];
}

for (e=0;e<numElems;e++) {
  if(map_gradient[e]>-1){
    rho[e]=rho_opt[map_gradient[e]];
    //gradient_FD_map[e]=gradient[map_gradient[e]];
    gradient2_map[e]=gradient2[map_gradient[e]];
  }
  else{
    //gradient_FD_map[e]=0;
    gradient2_map[e]=0;
  }
}

for(e=0;e<numElems_red;e++) {
  rho_red[e]=rho[map_elements_short[e]];
}

printf("Optimization step %d : PCG steps : %d, PCG residual=%lg, cost function=%lg, volume fraction=%lg\n",
opt,iter,norme,cost_function[opt],constraint[opt]);


//POST PROCESSING

for (n=0;n<numNodes_red;n++) {
  U[map_nodes_short[n]]=U_red[n];
  //printf("u[%d]=%lg\n",map_nodes_short[n],U[map_nodes_short[n]]);
}

for (e=0;e<numElems_red;e++) {
  yphys[map_elements_short[e]]=yphys_red[e];
}

//for (l=0;l<loadsteps;l++){
  int cpt=0;
for (i=0;i<(ni+1)*(nj+1)*(nk+1);i++){
 ux[i]=U[cpt];   fixednodes_x[i]=fixednodes[cpt]; cpt++;
 uy[i]=U[cpt];   fixednodes_y[i]=fixednodes[cpt];cpt++;
 uz[i]=U[cpt];   fixednodes_z[i]=fixednodes[cpt];cpt++;
}
//}

l=loadsteps-1;

strcpy(filename,"optim_3d_elastic_");  sprintf(u2,"%d",opt);	strcat(filename,u2);
 strcpy(str2,str1);   strcat(filename,str2);
fichier=fopen(filename,"w"); memset(filename, 0, sizeof(filename));
fprintf(fichier, "# vtk DataFile Version 3.0\n");
fprintf(fichier, "VTK from C\n");
fprintf(fichier,"ASCII\n");
fprintf(fichier,"DATASET UNSTRUCTURED_GRID\n");
fprintf(fichier,"POINTS %d float\n",(nj+1)*(ni+1)*(nk+1));
for (k=0;k<nk+1;k++){
for (j=0;j<nj+1;j++){
    for (i=0;i<ni+1;i++){
    fprintf(fichier,"%f %f %f\n",i*0.001,j*0.001,k*0.001);
  }
}
}
fprintf(fichier,"CELLS %d %d\n",ni*nj*nk,9*ni*nj*nk); //5 for 4 (node index) +1 (number of nodes per cell)
for (i=0;i<ni*nj*nk;i++){
  fprintf(fichier,"8 %d %d %d %d %d %d %d %d\n",
  edofMat2[qpoints*i+0],edofMat2[qpoints*i+1],edofMat2[qpoints*i+3],edofMat2[qpoints*i+2],
  edofMat2[qpoints*i+4],edofMat2[qpoints*i+5],edofMat2[qpoints*i+7],edofMat2[qpoints*i+6]);
}
fprintf(fichier,"CELL_TYPES %d\n",ni*nj*nk);
for (i=0;i<numElems;i++){
fprintf(fichier,"11\n"); // 8 for pixel (2D) https://www.kitware.com/products/books/VTKUsersGuide.pdf
}
fprintf(fichier,"CELL_DATA %d\n",numElems);
fprintf(fichier,"SCALARS vms float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
    fprintf(fichier,"%lg ",von_mises_stress[l*numElems+i0]);
}
}
}
fprintf(fichier,"\nSCALARS density float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
    fprintf(fichier,"%lg ",rho[i0]);
}
}
}
fprintf(fichier,"\nSCALARS map_elements float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
    fprintf(fichier,"%d ",map_elements_long[i0]);
}
}
}
fprintf(fichier,"\nSCALARS gradient float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
    fprintf(fichier,"%lg ",gradient2_map[i0]);
}
}
}
if(finite_difference==1){
fprintf(fichier,"\nSCALARS diff_FD float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      if(gradient_FD_map[i0]!=0){
    fprintf(fichier,"%lg ",abs(gradient_FD_map[i0]-gradient2_map[i0])/abs(gradient_FD_map[i0]));
  }
  else{
    fprintf(fichier,"%lg ",0.0);
  }
}
}
}
fprintf(fichier,"\nSCALARS gradient_FD float\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<nk;k++){
for (j=0;j<nj;j++){
  for (i=0;i<ni;i++){
      i0=I3D(ni,nj,i,j,k);
      fprintf(fichier,"%lg ",gradient_FD_map[i0]);
    }
  }
}
}

fprintf(fichier,"\nPOINT_DATA %d\n",(ni+1)*(nj+1)*(nk+1));
fprintf(fichier,"SCALARS u double\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<(nk+1);k++){
  for (j=0;j<(nj+1);j++){
    for (i=0;i<(ni+1);i++){
        i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%lg ",uz[i0]);
    }
  }
}
fprintf(fichier,"SCALARS fixednodes_x double\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<(nk+1);k++){
  for (j=0;j<(nj+1);j++){
    for (i=0;i<(ni+1);i++){
        i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%d ",fixednodes_x[i0]);
    }
  }
}
fprintf(fichier,"SCALARS fixednodes_y double\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<(nk+1);k++){
  for (j=0;j<(nj+1);j++){
    for (i=0;i<(ni+1);i++){
        i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%d ",fixednodes_y[i0]);
    }
  }
}
fprintf(fichier,"SCALARS fixednodes_z double\n");
fprintf(fichier,"LOOKUP_TABLE default\n");
for (k=0;k<(nk+1);k++){
  for (j=0;j<(nj+1);j++){
    for (i=0;i<(ni+1);i++){
        i0=I3D(ni2,nj2,i,j,k);
      fprintf(fichier,"%d ",fixednodes_z[i0]);
    }
  }
}

fclose(fichier);

}


size_t free_memory, total_memory;
checkCudaErrors(cudaMemGetInfo(&free_memory,&total_memory));
printf("GPU memory usage : %.3f GB\n",(double) (total_memory-free_memory)/1e9);


  //free gpu memory

  checkCudaErrors(cudaFree(Ke_elastic_d));
  checkCudaErrors(cudaFree(pAp_d));
  checkCudaErrors(cudaFree(rznew_d));
  checkCudaErrors(cudaFree(rzold_d));
  checkCudaErrors(cudaFree(rNorm_d));
  checkCudaErrors(cudaFree(fixednodes_red_d));
  checkCudaErrors(cudaFree(U_red_d));
  checkCudaErrors(cudaFree(M_red_d));
  checkCudaErrors(cudaFree(r_red_d));
  checkCudaErrors(cudaFree(p_red_d));
  checkCudaErrors(cudaFree(z_red_d));
  checkCudaErrors(cudaFree(f_red_d));
  checkCudaErrors(cudaFree(prod_red_d));
  checkCudaErrors(cudaFree(Ap_red_d));
  cudaFree(Ke_plastic_d);

  //free(BE_total);
  //free(inh_strain2_l);
  //free(U);
  //free(ux);
  //free(uy);
  //free(uz);
  //free(lambda_red);
  //free(lambdax);
  //free(lambday);
  //free(lambdaz);
  //free(pAp);
  //free(rznew);
  //free(rNorm);
  //free(fixednodes);

  free(rhs_x);
  free(rhs_y);
  free(rhs_z);
  free(old_pstrain);
  free(edofMat2);
  free(rho);
  free(rho_opt);
  free(rho_old_opt);
  free(g);
  free(dg);
  free(xmin);
  free(xmax);
  free(af);
  free(bf);
  free(map_gradient);
  free(gradient_map);
  free(gradient);
  free(gradient1);
  free(gradient2);
  free(gradient_FD);
  free(gradient2_map);
  free(gradient2_l);
  free(gradient_FD_map);
  free(cost_function);
  free(constraint);

  free(von_mises_stress);
  free(dev_von_mises_stress);
  free(stress_xx_e);
  free(stress_yy_e);
  free(stress_zz_e);
  free(stress_xy_e);
  free(stress_yz_e);
  free(stress_xz_e);
  free(dev_stress_xx_e);
  free(dev_stress_yy_e);
  free(dev_stress_zz_e);
  free(dev_stress_xy_e);
  free(dev_stress_yz_e);
  free(dev_stress_xz_e);
  free(n_trial_stress_xx_e);
  free(n_trial_stress_yy_e);
  free(n_trial_stress_zz_e);
  free(n_trial_stress_xy_e);
  free(n_trial_stress_yz_e);
  free(n_trial_stress_xz_e);
  free(n_trial_stress_eq);

  free(initial_elements);
  free(initial_elements_red_l);

  free(dR_due);
  free(dc_dvm);
  free(dvm_dwe);

  free(dc_due);
  free(rhs);
  free(rhs_j);
  free(rhs_l);
  free(dc_dxe);

  free(Ke_elastic);
  free(Ke2);

  free(Ke2_t);
  free(Ke2_plastic);
  free(Ke2_plastic_t);
  free(Ke2_e);
  free(Ke_plastic);


  free(norm_delta_U_red);
  free(norm_Unew_red);
  free(norm_Uold_red);
  free(norm_delta_lambda_red);
  free(norm_lambda_new_red);
  free(norm_lambda_old_red);
  free(fixednodes_x);
  free(fixednodes_y);
  free(fixednodes_z);

  free(fext);
  free(fint);
  free(U_red_l);
  free(Uini_red);

  free(Uold_red);
  free(delta_U_red);
  free(lambda_red_l);
  free(delta_lambda_red);
  free(lambda_old_red);
  free(FE_load);

  free(dn_ds);
  free(dn_ds_l);
  free(n_trial_stress);
  free(n_trial_stress_l);
  free(dgamma);
  free(strain);
  free(strain_ini);
  free(plastic_strain);
  free(hardening);
  free(old_plastic_strain);
  free(old_plastic_strain_l);
  free(old_hardening);
  free(stress);
  free(deviatoric_stress);
  free(consistent_matrix);
  free(plasticity_nodes);
  free(plasticity_nodes_l);
  free(norm_dev_stress_l);
  free(criteria_vms);
  free(plasticity_elements);
  free(map_plastic);
  free(FE_int);

  double final_time=get_time()-time_program;
  int hours=final_time/3600;
  int minutes1=final_time-hours*3600;
  int minutes=minutes1/60;
  int seconds=minutes1%60;
  printf("Program finished in %d hours %d minutes and %d seconds \n",hours,minutes,seconds);

  //Reset GPU Device
  cudaDeviceReset();

  return EXIT_SUCCESS;
}
