//this is the used for comparing SIMP and LS methods.......
//this is the level set version of the distributor.........
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>
// includes, cuda
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include<device_launch_parameters.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h>

#define TILE_I 10
#define TILE_J 10
#define TILE_K 2

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)

//Poids pour les fonctions feq et geq LBM
//D2Q9
#define faceq1 4.f / 9.f
#define faceq2 1.f / 9.f
#define faceq3 1.f / 36.f

//Tableaux CPU 3D
double *h1_vx, *h1_vy, *h1_R, *tampon1, *tampon2, *h1_gamma, *h1_gamma2, *h1_gradient, *h1_gradient2, *h1_adjoint;

double *d1_vx, *d1_vy, *d1_gamma, *d1_rho, *d1_gradient, *d1_adjoint;
cudaError_t err;

//Tableaux GPU 3D
double *d1_f0,*d1_f1,*d1_f2,*d1_f3,*d1_f4,*d1_f5,*d1_f6,*d1_f7,*d1_f8;
double *d1_f0_1,*d1_f1_1,*d1_f2_1,*d1_f3_1,*d1_f4_1,*d1_f5_1,*d1_f6_1,*d1_f7_1,*d1_f8_1;

double *d1_fa0,*d1_fa1,*d1_fa2,*d1_fa3,*d1_fa4,*d1_fa5,*d1_fa6,*d1_fa7,*d1_fa8;
double *d1_fa0_1,*d1_fa1_1,*d1_fa2_1,*d1_fa3_1,*d1_fa4_1,*d1_fa5_1,*d1_fa6_1,*d1_fa7_1,*d1_fa8_1;

double *uin_data, *uin, *Tprim, *Tprim_data;

size_t pitch, pitch1, pitch2;
size_t slicepitch;

typedef struct Sf{
	double *d0;
	double *d1;
	double *d2;
	double *d3;
	double *d4;
	double *d5;
	double *d6;
	double *d7;
	double *d8;
}Sf;

Sf *h1_f,*h1_fa, *h1_g, *h1_ga;


double *debit_host,*debit_data,debit;
double cpu_time_used;
clock_t start_cpu, end_cpu;

double diff_fluide,diff_solide,tau,alpha_max,q,rtau, rtau1,vxin,kvisc, roout,epsilon1,epsilon2,eps,volfrac, cost_function,somme,beta;
int i, j,k,k1,k2,cpt,display,i0, ni,nj, nk1, dimx2, dimy2, dimz2, lbm_it_max,opt_it_max,*domain_optim;

FILE *fichier_vx1, *fichier_vy1, *fichier_vz1, *fichier_T1, *fichier_p1, *fichier_gamma1, *fichier_gradient1, *fichier_adjoint,
*fichier_fa0,*fichier_fa1,*fichier_fa2,*fichier_fa3,*fichier_fa4,*fichier_fa5,*fichier_fa6,*fichier_fa7,*fichier_fa8,
*fichier_ga0,*fichier_ga1,*fichier_ga2,*fichier_ga3,*fichier_ga4,*fichier_ga5,*fichier_ga6,*fichier_ga7,*fichier_ga8;
char racine[200],u2[200],filename[200];

int block_x,block_y, block_z;
cudaExtent volumesize1;
dim3 grid2,block2;
int nmax,i02;
double omega1, omega2, T0, Tmax, beta_max,deltap_max,cost_function1,cost_function2;

//LS
double *TD,*phi_n,*phi_e,*td2,*TDN,*TDN_1,*TDN_2,*TDN_3,*TDN_4, *phi_1, *phi_2, *phi_3, *phi_4;


void malloc_f_gpu1(int ni, int nj, int nelx, int nely)
{
	h1_f = (Sf*)malloc(sizeof(Sf));
	h1_fa = (Sf*)malloc(sizeof(Sf));


	h1_f->d0 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d1 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d2 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d3 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d4 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d5 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d6 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d7 = (double*)malloc(ni*nj*sizeof(double));
	h1_f->d8 = (double*)malloc(ni*nj*sizeof(double));

	h1_fa->d0 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d1 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d2 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d3 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d4 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d5 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d6 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d7 = (double*)malloc(ni*nj*sizeof(double));
	h1_fa->d8 = (double*)malloc(ni*nj*sizeof(double));

	h1_R = (double*)malloc(ni*nj*sizeof(double));
	h1_vx = (double*)malloc(ni*nj*sizeof(double));
	h1_vy = (double*)malloc(ni*nj*sizeof(double));
	h1_gradient = (double*)malloc(ni*nj*sizeof(double));
	h1_gradient2 = (double*)malloc(ni*nj*sizeof(double));
	h1_adjoint = (double*)malloc(ni*nj*sizeof(double));
	h1_gamma = (double*)malloc(ni*nj*sizeof(double));
	h1_gamma2 = (double*)malloc(ni*nj*sizeof(double));
	domain_optim = (int*)malloc(ni*nj*sizeof(int));
	debit_host = (double*)malloc(ni*nj*sizeof(double));

	tampon1 = (double*)malloc(ni*nj*sizeof(double));
	tampon2 = (double*)malloc(ni*nj*sizeof(double));
	uin = (double*)malloc(ni*nj*sizeof(double));

	//LS


	// Tableau de d�riv�e topologique
	TD = (double *)calloc(nelx*nely, sizeof(double));

	//Tableau phi noeuds
	phi_n = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));

	//Tableau phi elements
	phi_e = (double *)calloc(nelx*nely, sizeof(double));

	// Tableau intermediaire pour l'interpolation
	td2 = (double *)calloc((nelx + 2)*(nely + 2), sizeof(double));

	//Tableau TD interpolation
	TDN = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));

	TDN_1 = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));
	TDN_2 = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));
	TDN_3 = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));
	TDN_4 = (double *)calloc((nelx + 1)*(nely + 1), sizeof(double));

	phi_1 = (double *)calloc(nelx*nely, sizeof(double));
	phi_2 = (double *)calloc(nelx*nely, sizeof(double));
	phi_3 = (double *)calloc(nelx*nely, sizeof(double));
	phi_4 = (double *)calloc(nelx*nely, sizeof(double));
}

void Init_CPU_gpu1(int ni, int nj, int nelx, int nely, double roout, double vxin, double kvisc, double volfrac, int *domain_optim)
{
	int i, j, i0;


	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){

				i0 = I2D(ni, i, j);

				h1_f->d0[i0] = faceq1 * roout;
				h1_f->d1[i0] = faceq2 * roout;
				h1_f->d2[i0] = faceq2 * roout;
				h1_f->d3[i0] = faceq2 * roout;
				h1_f->d4[i0] = faceq2 * roout;
				h1_f->d5[i0] = faceq3* roout;
				h1_f->d6[i0] = faceq3 * roout;
				h1_f->d7[i0] = faceq3 * roout;
				h1_f->d8[i0] = faceq3 * roout;

				h1_fa->d0[i0] = 0;
				h1_fa->d1[i0] = 0;
				h1_fa->d2[i0] = 0;
				h1_fa->d3[i0] = 0;
				h1_fa->d4[i0] = 0;
				h1_fa->d5[i0] = 0;
				h1_fa->d6[i0] = 0;
				h1_fa->d7[i0] = 0;
				h1_fa->d8[i0] = 0;

				h1_vx[i0] = 0;
				h1_vy[i0] = 0;
				h1_R[i0] = roout;
				h1_gradient[i0]=0;
				h1_gradient2[i0]=0;
				h1_adjoint[i0]=0;

		}
	}


int bordure=10;

for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
				domain_optim[i0]=1;
				debit_host[i0]=0;
					h1_gamma[i0]=1; //instead of volfrac
					if(i<bordure) h1_gamma[i0]=1;
					if(i>(ni-bordure-1)) h1_gamma[i0]=1;

					//if ((((j > (nj-bordure-1)) || (j < bordure)) && (i < bordure)) || (((j > 135) || (j < 65)) && (i >(ni-bordure-1)))) { h1_gamma[i0] = 0.001; }
					if (j < bordure || j > (nj - bordure -1)) {h1_gamma[i0] = 0; domain_optim[i0]=0;} //0 instead of 0.001;
					if (i < bordure || i > (ni - bordure -1)) {h1_gamma[i0] = 0; domain_optim[i0]=0;}

					if ((i < bordure) && ((j > 20) && (j < 60))) h1_gamma[i0] = 1;
					if ((i >(ni-bordure-1)) && ((j > 120) && (j < 160))) h1_gamma[i0] = 1;
					if ((i >(ni-bordure-1)) && ((j > 40) && (j < 80))) h1_gamma[i0] = 1;
					if ((j >(nj-bordure-1)) && ((i > 140) && (i < 180))) h1_gamma[i0] = 1;
					if ((j < bordure)  && ((i > 140) && (i < 180))) h1_gamma[i0] = 1;
					if ((i ==(ni-1)) && ((j > 120) && (j < 160)))  debit_host[i0]=2;
					if ((i ==(ni-1)) && ((j > 40) && (j < 80))) debit_host[i0]=3;
					if ((j ==(nj-1)) && ((i > 140) && (i < 180))) debit_host[i0]=1;
					if ((j == 0)  && ((i > 140) && (i < 180))) debit_host[i0]=4;
					if ((i ==0) && ((j > 20) && (j < 60))) debit_host[i0]=5;

					h1_gamma2[i0]=h1_gamma[i0];


			//	int ic,jc;
				//ic=50;	jc=50;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<10*10) h1_gamma[i0]=0.5;
				/*ic=25;	jc=50;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.7;
				ic=75;	jc=50;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.2;

				ic=50;	jc=25;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.8;
				ic=25;	jc=25;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.5;
				ic=75;	jc=25;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.3;

				ic=50;	jc=75;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.6;
				ic=25;	jc=75;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.4;
				ic=75;	jc=75;	test=(ic-i)*(ic-i)+(jc-j)*(jc-j);		if(test<5*5) h1_gamma[i0]=0.1;*/


}
}

		double njf=40;//nj-2*bordure;
		int indice; double forcex;
		for (i = 0; i <= njf; i++) {
			indice = i + 21;
			forcex = (double)8 * vxin*kvisc / (njf*njf);
			uin[indice] = (double)(-1 / (2 * kvisc))*forcex*(i)*(i - njf);

		}

double somme=0;
		for (i = 0; i < ni; i++) {
			for (j = 0; j < nj; j++){
					i0 = I2D(ni, i, j);
					if(domain_optim[i0]==1) somme=somme+h1_gamma[i0];
				}
			}
printf("volfrac=%f\n",somme/((ni-2*bordure)*(nj-2*bordure)));


//LS

//Initialisation de phi_n

for (i = 0; i < nelx + 1; i++){
	for (j = 0; j < nely + 1; j++){
		i0 = I2D((nelx + 1), i, j);
		if (i == 0 || j == 0 || i == nelx || j == nely){
			phi_n[i0] = 0;
		}
		else{
			phi_n[i0] = 1;
		}

	}
}

}

void malloc_fhost_gpu1(int ni, int nj, int nelx, int nely)
{

	cudaMallocPitch((void **)&d1_vx, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_vy, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_rho, &pitch,ni*sizeof(double),nj);

	cudaMallocPitch((void **)&d1_f0, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f2, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f3, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f4, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f5, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f6, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f7, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f8, &pitch,ni*sizeof(double),nj);


	cudaMallocPitch((void **)&d1_f0_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f1_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f2_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f3_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f4_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f5_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f6_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f7_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_f8_1, &pitch,ni*sizeof(double),nj);

	cudaMallocPitch((void **)&d1_fa0, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa2, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa3, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa4, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa5, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa6, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa7, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa8, &pitch,ni*sizeof(double),nj);


	cudaMallocPitch((void **)&d1_fa0_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa1_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa2_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa3_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa4_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa5_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa6_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa7_1, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_fa8_1, &pitch,ni*sizeof(double),nj);

	cudaMallocPitch((void **)&d1_gamma, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_gradient, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&d1_adjoint, &pitch,ni*sizeof(double),nj);
	cudaMallocPitch((void **)&debit_data, &pitch,ni*sizeof(double),nj);
	cudaMalloc((void **)&uin_data, sizeof(double)*nj);
}

void Copy_CPU_to_GPU1( int ni, int nj , int nelx, int nely)
{
cudaMemcpy2D((void *)debit_data, pitch, (void *)debit_host, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_vx, pitch, (void *)h1_vx, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_vy, pitch, (void *)h1_vy, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_rho, pitch, (void *)h1_R, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_gamma, pitch, (void *)h1_gamma, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

cudaMemcpy2D((void *)d1_f0, pitch, (void *)h1_f->d0, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f1, pitch, (void *)h1_f->d1, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f2, pitch, (void *)h1_f->d2, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f3, pitch, (void *)h1_f->d3, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f4, pitch, (void *)h1_f->d4, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f5, pitch, (void *)h1_f->d5, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f6, pitch, (void *)h1_f->d6, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f7, pitch, (void *)h1_f->d7, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f8, pitch, (void *)h1_f->d8, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

cudaMemcpy2D((void *)d1_f0_1, pitch, (void *)h1_f->d0, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f1_1, pitch, (void *)h1_f->d1, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f2_1, pitch, (void *)h1_f->d2, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f3_1, pitch, (void *)h1_f->d3, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f4_1, pitch, (void *)h1_f->d4, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f5_1, pitch, (void *)h1_f->d5, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f6_1, pitch, (void *)h1_f->d6, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f7_1, pitch, (void *)h1_f->d7, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_f8_1, pitch, (void *)h1_f->d8, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

cudaMemcpy2D((void *)d1_fa0, pitch, (void *)h1_fa->d0, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa1, pitch, (void *)h1_fa->d1, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa2, pitch, (void *)h1_fa->d2, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa3, pitch, (void *)h1_fa->d3, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa4, pitch, (void *)h1_fa->d4, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa5, pitch, (void *)h1_fa->d5, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa6, pitch, (void *)h1_fa->d6, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa7, pitch, (void *)h1_fa->d7, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa8, pitch, (void *)h1_fa->d8, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

cudaMemcpy2D((void *)d1_fa0_1, pitch, (void *)h1_fa->d0, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa1_1, pitch, (void *)h1_fa->d1, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa2_1, pitch, (void *)h1_fa->d2, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa3_1, pitch, (void *)h1_fa->d3, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa4_1, pitch, (void *)h1_fa->d4, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa5_1, pitch, (void *)h1_fa->d5, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa6_1, pitch, (void *)h1_fa->d6, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa7_1, pitch, (void *)h1_fa->d7, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);
cudaMemcpy2D((void *)d1_fa8_1, pitch, (void *)h1_fa->d8, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

cudaMemcpy((void *)uin_data, (void *)uin, sizeof(double)*nj, cudaMemcpyHostToDevice);

}

double convergence(int ni, int nj, double *plotvx, double *tampon)
{
	int i, j,  i0;
	double somme, mean,epsilon;
	somme = 0;
 	mean=0;


	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){

				i0 = I2D(ni, i, j);

			mean= mean + sqrt(plotvx[i0]*plotvx[i0]);

	}
	}

		mean=(double) mean/(ni*nj);
		//printf("mean=%f \n",mean);

		if(mean==0) {
			printf("Convergence : champ nul \n");
			mean=1;
		}

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){

				i0 = I2D(ni, i, j);

			somme = somme + abs((double)(plotvx[i0] - tampon[i0])/mean);

	}
	}
	epsilon = (double)somme / (ni*nj);
	return epsilon;

}



__global__ void initialisation_kernel(int ni, int nj, size_t pitch,
	double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,	double *f8_data,
	double *f0_data1,	double *f1_data1,	double *f2_data1,	double *f3_data1,	double *f4_data1,	double *f5_data1,	double *f6_data1,	double *f7_data1,	double *f8_data1,
	double *plotvx_data, double *plotvy_data, double *plotR_data)
	{

	int i, j;
	i = blockIdx.x*blockDim.x + threadIdx.x;
	j = blockIdx.y*blockDim.y + threadIdx.y;
	int i3d = i + j*pitch/sizeof(double);
	size_t pitch1;
	pitch1 = pitch / sizeof(double);

	if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){

	f0_data[i3d]=faceq1;
	f1_data[i3d]=faceq2;
	f2_data[i3d]=faceq2;
	f3_data[i3d]=faceq2;
	f4_data[i3d]=faceq2;
	f5_data[i3d]=faceq3;
	f6_data[i3d]=faceq3;
	f7_data[i3d]=faceq3;
	f8_data[i3d]=faceq3;


	f0_data1[i3d]=faceq1;
	f1_data1[i3d]=faceq2;
	f2_data1[i3d]=faceq2;
	f3_data1[i3d]=faceq2;
	f4_data1[i3d]=faceq2;
	f5_data1[i3d]=faceq3;
	f6_data1[i3d]=faceq3;
	f7_data1[i3d]=faceq3;
	f8_data1[i3d]=faceq3;

	plotvx_data[i3d]=0;
	plotvy_data[i3d]=0;
	plotR_data[i3d]=1;
}

}

__global__ void initialisation_adjoint_kernel(int ni, int nj,  size_t pitch,
	double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,	double *f8_data,
	double *f0_data1,	double *f1_data1,	double *f2_data1,	double *f3_data1,	double *f4_data1,	double *f5_data1,	double *f6_data1,	double *f7_data1,	double *f8_data1)
	{

	int i, j;
	i = blockIdx.x*blockDim.x + threadIdx.x;
	j = blockIdx.y*blockDim.y + threadIdx.y;
	int i3d = i + j*pitch/sizeof(double);
	size_t pitch1;
	pitch1 = pitch / sizeof(double);


	if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){

	f0_data[i3d]=0;
	f1_data[i3d]=0;
	f2_data[i3d]=0;
	f3_data[i3d]=0;
	f4_data[i3d]=0;
	f5_data[i3d]=0;
	f6_data[i3d]=0;
	f7_data[i3d]=0;
	f8_data[i3d]=0;

	f0_data1[i3d]=0;
	f1_data1[i3d]=0;
	f2_data1[i3d]=0;
	f3_data1[i3d]=0;
	f4_data1[i3d]=0;
	f5_data1[i3d]=0;
	f6_data1[i3d]=0;
	f7_data1[i3d]=0;
	f8_data1[i3d]=0;

}

}

__global__ void apply_collide_kernel(int ni, int nj, size_t pitch,double alpha_max, double q,
	double rtau, double rtau1, double *gamma_data,
	double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,
	double *f8_data,
	double *f0_data1,	double *f1_data1,	double *f2_data1,	double *f3_data1,	double *f4_data1,	double *f5_data1,	double *f6_data1,	double *f7_data1,
	double *f8_data1,	double *uin_data)
	{

	int i, j, l;
	double   v_sq_term, uz;
	double ro, vx, vy, vx2,vy2,f0, f1, f2, f3, f4, f5, f6, f7, f8, ro2, f0_2, f1_2, f2_2, f3_2, f4_2, f5_2, f6_2, f7_2, f8_2;

	i = blockIdx.x*blockDim.x + threadIdx.x;
	j = blockIdx.y*blockDim.y + threadIdx.y;
	int i3d = i + j*pitch/sizeof(double);
	size_t pitch1;
	pitch1 = pitch / sizeof(double);

	if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){
	int newi_f, newj_f;
	int indice_f[9] = {};
	int cx_f[9] = { 0,1,0,-1,0,1,-1,-1,1 };
	int cy_f[9] = { 0, 0,1,0,-1,1,1,-1,-1};

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Streaming  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	if(gamma_data[i3d]==1.0){ //for fluid nodes only....applying halfway bounceback & streaming....
		for (l = 0; l < 9; l++)	{
		/*	newi_f = (i - cx_f[l] + ni) % ni;
			newj_f = (j - cy_f[l] + nj) % nj;
			newk_f = (k - cz_f[l] + nk) % nk;*/
			newi_f = i ;
			newj_f = j ;
			if((i-cx_f[l])>=0 && (i-cx_f[l])<ni)	newi_f = i - cx_f[l] ;
			if((j-cy_f[l])>=0 && (j-cy_f[l])<nj)	newj_f = j - cy_f[l] ;
			indice_f[l] = newi_f + pitch1*newj_f;
		}
      f0 = f0_data[i3d];
			if (gamma_data[indice_f[1]] == 1.0) { f1 = f1_data[indice_f[1]];}
			else { f1 = f3_data[i3d];}
			if (gamma_data[indice_f[2]] == 1.0) { f2 = f2_data[indice_f[2]];}
			else { f2 = f4_data[i3d];}
			if (gamma_data[indice_f[3]] == 1.0) { f3 = f3_data[indice_f[3]];}
			else { f3 = f1_data[i3d];}
			if (gamma_data[indice_f[4]] == 1.0) { f4 = f4_data[indice_f[4]];}
			else { f4 = f2_data[i3d];}
			if (gamma_data[indice_f[5]] == 1.0) { f5 = f5_data[indice_f[5]];}
			else { f5 = f7_data[i3d];}
			if (gamma_data[indice_f[6]] == 1.0) { f6 = f6_data[indice_f[6]];}
			else { f6 = f8_data[i3d];}
			if (gamma_data[indice_f[7]] == 1.0) { f7 = f7_data[indice_f[7]];}
			else { f7 = f5_data[i3d];}
			if (gamma_data[indice_f[8]] == 1.0) { f8 = f8_data[indice_f[8]];}
			else { f8 = f6_data[i3d];}
	}

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Boundary Conditions <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	if (j == 0 ) {
		if(gamma_data[i3d]==1){
		uz=-1+(f0+f1+f3+2*(f4+f7+f8));

		f2 = f4- (double)2*uz/3;
		f5 = f7-(double)uz/6 + 0.5*(f3-f1);
		f6 = f8- (double)uz/6 -0.5*(f3-f1);
	}
		if(gamma_data[i3d]<0.5){
		f2=f4;
		f5=f7;
		f6=f8;
	}
}

	if (j == (nj-1) ) {
		if(gamma_data[i3d]==1){
		uz=-1+(f0+f1+f3+2*(f2+f5+f6));

		f4 = f2- (double)2*uz/3;
		f7 = f5-(double)uz/6 - 0.5*(f3-f1);
		f8 = f6- (double)uz/6 +0.5*(f3-f1);
	}
if(gamma_data[i3d]<0.5){
		f4=f2;
		f7=f5;
		f8=f6;
	}
	}

	// Inlet BC - very crude
	if (i == 0 ) {

		if(gamma_data[i3d]==1){
		double rho;
		rho=(double)(f0+f2+f4+2*(f3+f6+f7))/(1-uin_data[j]);
		f1 = f3+(double)2*rho*uin_data[j]/3;
		f5 = f7 +(double)rho*uin_data[j]/6 + 0.5*(f4-f2);
		f8 = f6 +(double)rho*uin_data[j]/6 - 0.5*(f4-f2);
	}

		if(gamma_data[i3d]<0.5){
				f1=f3;
				f5=f7;
				f8=f6;
			}

	}

	// Exit BC - very crude

	if (i == (ni - 1) ) {

		if(gamma_data[i3d]==1){
		uz=-1+(f0+f2+f4+2*(f1+f5+f8));

		f3 = f1- (double)2*uz/3;
		f6 = f8-(double)uz/6 + 0.5*(f4-f2);
		f7 = f5- (double)uz/6 -0.5*(f4-f2);
	}

if(gamma_data[i3d]<0.5){
		f3=f1;
		f6=f8;
		f7=f5;
	}

}

	// Macroscopic flow props:

	double alpha,gamma;
	gamma=gamma_data[i3d];
	//alpha=(alpha_max-(alpha_max*((double)(1+q)*gamma/(gamma+q))));
	alpha=alpha_max-(alpha_max*gamma);

	ro = f0 + f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8;
	vx = (double)(f1 - f3 + f5 - f6 - f7 + f8)/ro;
	vy = (double)(f2 - f4 +f5 + f6 -f7 -f8)/ro;
	v_sq_term = 1.5f*(vx*vx + vy*vy);

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Collisions <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	if(gamma_data[i3d]==1){   //for the fluid nodes only.......
		f0_data1[i3d] = rtau1 * f0 + rtau * faceq1*ro*(1- v_sq_term);
		f1_data1[i3d] = rtau1 * f1 + rtau * faceq2*ro*(1+ 3.f*vx + 4.5f*vx*vx - v_sq_term);
		f2_data1[i3d] = rtau1 * f2 + rtau * faceq2*ro*(1+ 3.f*vy + 4.5f*vy*vy - v_sq_term);
		f3_data1[i3d] = rtau1 * f3 + rtau * faceq2*ro*(1- 3.f*vx + 4.5f*vx*vx - v_sq_term);
		f4_data1[i3d] = rtau1 * f4 + rtau * faceq2*ro*(1- 3.f*vy + 4.5f*vy*vy - v_sq_term);
		f5_data1[i3d] = rtau1 * f5 + rtau * faceq3*ro*(1+ 3.f*(vx+vy) + 4.5f*(vx + vy)*(vx + vy) - v_sq_term);
		f6_data1[i3d] = rtau1 * f6 + rtau * faceq3*ro*(1+ 3.f*(-vx+vy) + 4.5f*(-vx + vy)*(-vx + vy) - v_sq_term);
		f7_data1[i3d] = rtau1 * f7 + rtau * faceq3*ro*(1+ 3.f*(-vx-vy) + 4.5f*(-vx - vy)*(-vx - vy) - v_sq_term);
		f8_data1[i3d] = rtau1 * f8 + rtau * faceq3*ro*(1+ 3.f*(vx-vy) + 4.5f*(vx - vy)*(vx - vy) - v_sq_term);
		}

}
}

__global__ void apply_collide_kernel_adjoint(int ni, int nj, size_t pitch,double omega1, double alpha_max, double q,
	double deltap_max,double ratio, double rtau, double rtau1, double *gamma_data,
	double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,	double *f8_data,
	double *f0_data1,	double *f1_data1,	double *f2_data1,	double *f3_data1,	double *f4_data1,	double *f5_data1,	double *f6_data1,	double *f7_data1,	double *f8_data1,
	double *plotR_data, double *plotvx_data, double *plotvy_data,  double *uin_data, double lambda1, double lambda2,double lambda3,double lambda4,
	double debit1, double debit2, double debit3, double debit4,double debit1_c, double debit2_c, double debit3_c, double debit4_c,double *debit_data)
	{

	int i, j, l, ki, kj;
	double vx, vy,f[9], faeq[9],force_f[9],n1,n2,n3,n4;

	i = blockIdx.x*blockDim.x + threadIdx.x;
	j = blockIdx.y*blockDim.y + threadIdx.y;
	int i3d = i + j*pitch/sizeof(double);
	size_t pitch1;
	pitch1 = pitch / sizeof(double);

	if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){

	int newi_f, newj_f;

	int indice_f[9] = {};

	int cx_f[9] = { 0,1,0,-1,0,1,-1,-1,1 };
	int cy_f[9] = { 0, 0,1,0,-1,1,1,-1,-1};

	double poids[9];
	double somme,somme2;
	poids[0] = faceq1;
	poids[1] = faceq2;
	poids[2] = faceq2;
	poids[3] = faceq2;
	poids[4] = faceq2;
	poids[5] = faceq3;
	poids[6] = faceq3;
	poids[7] = faceq3;
	poids[8] = faceq3;

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Adj Streaming  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	for (l = 0; l < 9; l++)	{
	/*	newi_f = (i - cx_f[l] + ni) % ni;
		newj_f = (j - cy_f[l] + nj) % nj;
		newk_f = (k - cz_f[l] + nk) % nk;*/
		newi_f = i ;
		newj_f = j ;


		if((i+cx_f[l])>=0 && (i+cx_f[l])<ni)	newi_f = i + cx_f[l] ;
		if((j+cy_f[l])>=0 && (j+cy_f[l])<nj)	newj_f = j + cy_f[l] ;


		indice_f[l] = newi_f + pitch1*newj_f;

	}

	f[0] = f0_data[i3d];
	f[1] = f1_data[indice_f[1]];
	f[2] = f2_data[indice_f[2]];
	f[3] = f3_data[indice_f[3]];
	f[4] = f4_data[indice_f[4]];
	f[5] = f5_data[indice_f[5]];
	f[6] = f6_data[indice_f[6]];
	f[7] = f7_data[indice_f[7]];
	f[8] = f8_data[indice_f[8]];

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Adj Boundary Conditions <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  //ro=plotR_data[i3d];
	vx=plotvx_data[i3d];
	vy=plotvy_data[i3d];

	if (j == 0 ) {
		if(gamma_data[i3d]==1){
			n4=(lambda4/debit4_c)*((double)(debit4/debit4_c)-1);
		f[4]  = f[2] - (1.f/3)*(double)(4*f[2]+f[6]+f[5])-cy_f[4]*n4;
		f[7]  = f[5] - (1.f/3)*(double)(4*f[2]+f[6]+f[5])-cy_f[7]*n4;
		f[8]  = f[6] - (1.f/3)*(double)(4*f[2]+f[6]+f[5])-cy_f[8]*n4;
		}
			if(gamma_data[i3d]<0.5){
		f[4]=f[2];
		f[7]=f[5];
		f[8]=f[6];
	}
	}

	if (j == (nj-1) ) {
	if(gamma_data[i3d]==1){
		n1=(lambda1/debit1_c)*((double)(debit1/debit1_c)-1);
		f[2]=f[4]- (1.f/3)*(double)(4*f[4]+f[7]+f[8])+cy_f[2]*n1;
		f[5]=f[7]- (1.f/3)*(double)(4*f[4]+f[7]+f[8])+cy_f[5]*n1;
		f[6]=f[8]- (1.f/3)*(double)(4*f[4]+f[7]+f[8])+cy_f[6]*n1;
	}
	if(gamma_data[i3d]<0.5){
		f[2]=f[4];
		f[5]=f[7];
		f[6]=f[8];
	}
	}

	// Inlet BC - very crude
	if (i == 0) {

if(gamma_data[i3d]==1){
		f[3] = f[1] -(double)omega1*1;//+ (double)(uin_data[j]/(3*(1-uin_data[j])))*(4*f[1]+f[5]+f[8]);// - (double)omega1/(3*N);
		f[6] = f[8] -(double)omega1*1;//+ (double)(uin_data[j]/(3*(1-uin_data[j])))*(4*f[1]+f[5]+f[8]);//- (double)omega1/(3*N);
		f[7] = f[5] -(double)omega1*1;//+ (double)(uin_data[j]/(3*(1-uin_data[j])))*(4*f[1]+f[5]+f[8]);//- (double)omega1/(3*N);
	}

		if(gamma_data[i3d]<0.5){
			f[3]=f[1];
			f[7]=f[5];
			f[6]=f[8];
		}

	}

	// Exit BC - very crude

	if (i == (ni - 1) ) {

		if(gamma_data[i3d]==1){

			if(debit_data[i3d]==2){
					n2=(lambda2/debit2_c)*((double)(debit2/debit2_c)-1);
		f[1]  = f[3] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[1]*n2;
		f[5]  = f[7] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[5]*n2;
		f[8]  = f[6] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[8]*n2;
	}

	if(debit_data[i3d]==3){
			n3=(lambda3/debit3_c)*((double)(debit3/debit3_c)-1);
f[1]  = f[3] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[1]*n3;
f[5]  = f[7] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[5]*n3;
f[8]  = f[6] - (1.f/3)*(double)(4*f[3]+f[6]+f[7])+cx_f[8]*n3;
}
		}

		if(gamma_data[i3d]<0.5){
			f[1]=f[3];
			f[5]=f[7];
			f[8]=f[6];
		}
	}


	// Macroscopic flow props:

	double alpha,gamma;
	gamma=gamma_data[i3d];
	alpha=alpha_max-(alpha_max*((double)(1+q)*gamma/(gamma+q)));
	//alpha=alpha_max-(alpha_max*gamma);

 //ADJOINT EQUILIBIRUM DISTRIBUTION FUNCTION AND ADJOINT BRINKMANN
	ki = 0;
	while (ki < 9){
	kj = 0;
	somme = 0;
	somme2=0;
	double rho_adjoint=0;
	double jx_adjoint=0;
	double jy_adjoint=0;

	while (kj < 9)
	{

		somme = somme + f[kj] * (poids[kj] * (1 + 3 * (cx_f[kj] * cx_f[ki] + cy_f[kj] * cy_f[ki])
																	+	4.5*2*(cx_f[kj] * vx +  cy_f[kj] * vy)*(cx_f[kj] * cx_f[ki] + cy_f[kj] * cy_f[ki])
																									- 3*(cx_f[ki]*vx + cy_f[ki]*vy)));

		rho_adjoint=rho_adjoint+poids[kj]*f[kj]*(1+3*(cx_f[kj]*vx+cy_f[kj]*vy)+4.5*(cx_f[kj]*cx_f[kj]*vx*vx+cy_f[kj]*cy_f[kj]*vy*vy-vx*vx/3-vy*vy/3+2*cx_f[kj]*cy_f[kj]*vx*vy));
		jx_adjoint= jx_adjoint+poids[kj]*f[kj]*(cx_f[kj]+3*(cx_f[kj]*cx_f[kj]*vx-vx/3+cx_f[kj]*cy_f[kj]*vy));
		jy_adjoint=	jy_adjoint+poids[kj]*f[kj]*(cy_f[kj]+3*(cy_f[kj]*cy_f[kj]*vy-vy/3+cy_f[kj]*cx_f[kj]*vx));

		/*somme = somme + f[kj] * (poids[kj] * (1 + 3 * (cx_f[kj] * cx_f[ki] + cy_f[kj] * cy_f[ki]) +
			4.5 * (2 * cx_f[kj] * cx_f[kj] * cx_f[ki] * vx + 2 * cy_f[kj] * cy_f[kj] * cy_f[ki] * vy	+ 2 * cx_f[kj] * cy_f[kj] * (cx_f[ki] * vy + cy_f[ki] * vx - vx*vy)		- cx_f[kj] * cx_f[kj] * vx*vx - cy_f[kj] * cy_f[kj] * vy*vy
				- (2.f / 3)*(cx_f[ki] * vx + cy_f[ki] * vy) + (1.f / 3)*(vx*vx + vy*vy))));*/

		kj = kj + 1;
	}

	faeq[ki] = rho_adjoint+3*(cx_f[ki]-vx)*jx_adjoint+3*(cy_f[ki]-vy)*jy_adjoint;//somme;
	ki = ki + 1;
	}

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Adj Collisions <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		f0_data1[i3d] = rtau1 * f[0] + rtau * faeq[0];
		f1_data1[i3d] = rtau1 * f[1] + rtau * faeq[1];
		f2_data1[i3d] = rtau1 * f[2] + rtau * faeq[2];
		f3_data1[i3d] = rtau1 * f[3] + rtau * faeq[3];
		f4_data1[i3d] = rtau1 * f[4] + rtau * faeq[4];
		f5_data1[i3d] = rtau1 * f[5] + rtau * faeq[5];
		f6_data1[i3d] = rtau1 * f[6] + rtau * faeq[6];
		f7_data1[i3d] = rtau1 * f[7] + rtau * faeq[7];
		f8_data1[i3d] = rtau1 * f[8] + rtau * faeq[8];

		}
	}

__global__ void gradient_kernel(int ni, int nj, size_t pitch,  double omega1 ,double alpha_max, double q,
			double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,
			double *f8_data,
			double *vx_data, double *vy_data, double *gamma_data, double *gradient_data)
		{
			int i, j;
			double vx,vy,f[9], somme,alpha2,gamma;

			i = blockIdx.x*blockDim.x + threadIdx.x;
			j = blockIdx.y*blockDim.y + threadIdx.y;
			int i3d = i + j*pitch/sizeof(double);
			size_t pitch1;
			pitch1 = pitch / sizeof(double);


			if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){

			f[0] = f0_data[i3d];
			f[1] = f1_data[i3d];
			f[2] = f2_data[i3d];
			f[3] = f3_data[i3d];
			f[4] = f4_data[i3d];
			f[5] = f5_data[i3d];
			f[6] = f6_data[i3d];
			f[7] = f7_data[i3d];
			f[8] = f8_data[i3d];

			vx=vx_data[i3d];
			vy=vy_data[i3d];
			gamma=gamma_data[i3d];

			int cx_f[9] = { 0,1,0,-1,0,1,-1,-1,1 };
			int cy_f[9] = { 0, 0,1,0,-1,1,1,-1,-1};


			double poids[9];
			poids[0] = faceq1;
			poids[1] = faceq2;
			poids[2] = faceq2;
			poids[3] = faceq2;
			poids[4] = faceq2;
			poids[5] = faceq3;
			poids[6] = faceq3;
			poids[7] = faceq3;
			poids[8] = faceq3;


			alpha2= - alpha_max*((double)(1+q)/(gamma+q))*(1-(gamma/(gamma+q)));

			//alpha2=1;

			int ki=0;
			somme=0;

			while (ki<9){
				somme=somme-3*alpha2*f[ki]*poids[ki]*(cx_f[ki]*vx+cy_f[ki]*vy); // Graham : the expression of the gradient is different for you as you don't have alpha, check in previous JCP codes
				ki=ki+1;
			}

			gradient_data[i3d]=somme;//-(double)omega2*beta2*(Tmax-T)/(ni*nj*beta_max*Tmax);
		}
	}

__global__ void macro_kernel(int ni, int nj, 	size_t pitch, double *f0_data,	double *f1_data,	double *f2_data,	double *f3_data,	double *f4_data,	double *f5_data,	double *f6_data,	double *f7_data,
	double *f8_data,	double *vx_data, double *vy_data,double *rho_data)
{
	int i, j;
	i = blockIdx.x*blockDim.x + threadIdx.x;
	j = blockIdx.y*blockDim.y + threadIdx.y;
	int i3d = i + j*pitch/sizeof(double);
	size_t pitch1;
	pitch1 = pitch / sizeof(double);


	if (i <= (ni - 1) && i3d <= (ni - 1) + (nj - 1)*pitch1){

	// Macroscopic flow props:
	rho_data[i3d]= f0_data[i3d] + f1_data[i3d] + f2_data[i3d] + f3_data[i3d] + f4_data[i3d] + f5_data[i3d] + f6_data[i3d] + f7_data[i3d] + f8_data[i3d];
	vx_data[i3d] = (double)(f1_data[i3d] - f3_data[i3d] + f5_data[i3d] - f6_data[i3d] - f7_data[i3d] + f8_data[i3d]) /rho_data[i3d];
	vy_data[i3d] = (double)(f2_data[i3d] - f4_data[i3d] + f5_data[i3d] + f6_data[i3d] - f7_data[i3d] - f8_data[i3d])/ rho_data[i3d];
}
}

int Min(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Max(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Mini(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Maxi(double d1,double d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, double rmin, double *x, double *df, double *df2, int *domain_optim, int bordure)
{
	int ni2,nj2,nk2;
	int i,j,i0,i02,i03,i1,j1,i2,j2;
	double somme,somme2,fac;

	ni2=ni-2*bordure;
	nj2=nj-2*bordure;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			i1=i+bordure;
			j1=j+bordure;
			i03=I2D(ni2,i,j);
				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {

								i02 = I2D(ni2,i2,j2);
								i1=i2+bordure;
								j1=j2+bordure;
								i0=I2D(ni,i1,j1);
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i0]*df[i02];

					}
				}


			if(x[i0]*somme>0) {
				df2[i03]=somme2/(x[i0]*somme);
			}
			else {
				df2[i03]=df[i03];
			}

		}
		}

}

void filtre2(int ni, int nj, double rmin, double *df, double *df2, int bordure)
{
	int ni2,nj2,nk2;
	int i,j,i0,i02,i03,i1,j1,i2,j2;
	double somme,somme2,fac;

	ni2=ni-2*bordure;
	nj2=nj-2*bordure;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){

			i03=I2D(ni2,i,j);
				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {

								i02 = I2D(ni2,i2,j2);

								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];

					}
				}


			if(somme>0) {
				df2[i03]=somme2/somme;
			}
			else {
				df2[i03]=df[i03];
			}

		}
		}

}

void OC (int ni, int nj, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				i0 = I2D(ni, i, j);
				gamma_old[i0]=gamma0[i0];
			}
		}


	while ((l2-l1)> 1e-14){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				i0 = I2D(ni, i, j);
				if(domain_optim[i0]==1){
				if(gradient[i0]>0) {

					if(gradient[i0]>1e-6) printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= Max(xmin,move1)){
				gamma0[i0]=Max(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > Max(xmin,move1) && xe_Ben < Min(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= Min(1,move2)){
				gamma0[i0]=Min(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==96 && j==76) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);

		}
	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);

					if(domain_optim[i0]==1){
					somme=somme+gamma0[i0];
					cpt=cpt+1;
				}
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);


		}
	}


volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void Level_Set(double Vmax, int ni, int nj, int nelx, int nely, int bordure, double K, double lambda, double dt, double *gradient, double *phi_n, double *phi_e, double *solid, double *gradient3){

	//Remplissage de TD
	double nombre;
	double *solid2;
	double volfrac;
	double sommeV=0;
	double somme2=0;
	double lambda1=0;

	int i2,j2,i1,ia,ic,ib,id,ig,ie,ii,ih,il;

	for (i = 0; i < nelx; i++){
		for (j = 0; j < nely; j++){
			i2 = i + bordure;
			j2 = j + bordure;
			i0 = I2D(nelx, i, j);
			i1 = I2D(ni, i2, j2);
			TD[i0] = -gradient[i1];
			sommeV=sommeV+solid[i1];
		}
	}
	volfrac=sommeV/(nelx*nely);

	if(volfrac>Vmax) lambda1=lambda;

	printf("volfrac=%f\n",volfrac);

		// Construction de td2
	for (i = 0; i < nelx + 2; i++){
		for (j = 0; j < nely + 2; j++){

			i0 = I2D(nelx + 2, i, j);

			if (i == 0 && j == 0) {
				ia = I2D(nelx, i, j);
				td2[i0] = TD[ia];
			}

			if (i == 0 && j == nely + 1){
				ic = I2D(nelx, i, (j - 2));
				td2[i0] = TD[ic];
			}

			if (i == nelx + 1 && j == 0){
				ib = I2D(nelx, (i - 2), j);
				td2[i0] = TD[ib];
			}

			if (i == nelx + 1 && j == nely + 1){
				id = I2D(nelx, (i - 2), (j - 2));
				td2[i0] = TD[id];
			}

			if (i == nelx + 1 && j != 0 && j != nely + 1){
				ig = I2D(nelx, (i - 2), (j - 1));
				td2[i0] = TD[ig];
			}

			if (i == 0 && j != 0 && j != nely + 1){
				ie = I2D(nelx, i, (j - 1));
				td2[i0] = TD[ie];
			}

			if (j == 0 && i != 0 && i != nelx + 1){
				ii = I2D(nelx, (i - 1), j);
				td2[i0] = TD[ii];
			}

			if (j == nely + 1 && i != 0 && i != nelx + 1){
				ih = I2D(nelx, (i - 1), (j - 2));
				td2[i0] = TD[ih];
			}

			if (i > 0 && j > 0 && i < nelx + 1 && j < nely + 1){
				i1 = I2D(nelx, (i - 1), (j - 1));
				td2[i0] = TD[i1];
			}

		}
	}


	//Construction de TDN

	for (i = 0; i < nelx + 1; i++){
		for (j = 0; j < nely + 1; j++){
			i0 = I2D((nelx + 1), i, j);
			ia = I2D((nelx + 2), i, j);
			ib = I2D((nelx + 2), (i + 1), j);
			ic = I2D((nelx + 2), (i + 1), (j + 1));
			id = I2D((nelx + 2), i, (j + 1));
			TDN_1[i0] = td2[ia];
			TDN_2[i0] = td2[ib];
			TDN_3[i0] = td2[ic];
			TDN_4[i0] = td2[id];
			TDN[i0] = 0.25*(TDN_1[i0] + TDN_2[i0] + TDN_3[i0] + TDN_4[i0]);
		}
	}

	double moy_TDN;
	somme = 0;
	moy_TDN = 0;

	for (i = 0; i < nelx + 1; i++){
		for (j = 0; j < nely + 1; j++){
			i0 = I2D((nelx + 1), i, j);
			somme = somme + abs(TDN[i0]);
		}
	}

	moy_TDN = (double)somme / ((nelx + 1)*(nely + 1));
	printf("moy_TDN=%lg\n", moy_TDN);


	//Calcul de C : echelle de modification de LSF par rapport au gradient

	somme = 0;
	somme2=0;
	for (i = 0; i < (nelx + 1)*(nely + 1); i++){
		somme = somme + abs(TDN[i]);
		somme2= somme2+ TDN[i];
	}

	double C = (double)(nelx + 1)*(nely + 1) / somme;
	//printf("c=%f\n", C);
	//double ex1=Vmax+(1-Vmax)*Maxi(0,1-(double)k2/50);
	//lamnda=(somme2/((nelx+1)*(nely+1)))*exp(1*(volfrac-ex1)/(ex1+0));
	//printf("ex1=%f and lambda=%f \n",ex1,lambda);


	// Mise a jour de phi_n
	for (i = 0; i < nelx + 1; i++){
		for (j = 0; j < nely + 1; j++){
			i0 = I2D(nelx + 1, i, j);
			gradient3[i0] = TDN[i0] + lambda1;

			if (i == 0 || j == 0 || i == nelx || j == nely){
				phi_n[i0] = 0;
			}
			else{

				phi_n[i0] = phi_n[i0] - K*C*dt*(TDN[i0] + lambda1);
				if (phi_n[i0]>1) phi_n[i0] = 1;
				if (phi_n[i0]<-1) phi_n[i0] = -1;

			}
		}
	}

	//Construction de phi_e

	for (i = 0; i < nelx; i++){
		for (j = 0; j < nely; j++){
			i0 = I2D(nelx, i, j);
			ia = I2D(nelx + 1, i, j);
			ib = I2D(nelx + 1, (i + 1), j);
			ic = I2D(nelx + 1, (i + 1), (j + 1));
			id = I2D(nelx + 1, i, (j + 1));
			phi_1[i0] = phi_n[ia];
			phi_2[i0] = phi_n[ib];
			phi_3[i0] = phi_n[ic];
			phi_4[i0] = phi_n[id];
			//printf("i0=%d ia=%d ib=%d ic=%d id=%d \n", i0, ia, ib, ic, id);

			phi_e[i0] = 0.25*(phi_1[i0] + phi_2[i0] + phi_3[i0] + phi_4[i0]);
		}
	}

}

int MAJ_solid(int ni, int nj, int nelx, int nely, int bordure, double *phi_e, double *solid, double *solid1){

	int i,j,i2,j2,i0,i02;

	int changement = 0;

	for (i = 0; i < nelx; i++){
		for (j = 0; j < nely; j++){
			i0 = I2D(nelx, i, j);
			i2 = i + bordure;
			j2 = j + bordure;
			i02 = I2D(ni, i2, j2);
			if (phi_e[i0]>0){
				solid[i02] = 1;
			}
			else {
				solid[i02] = 0;
			}

			if (solid[i02] != solid1[i02]) changement = 1;
			solid1[i02] = solid[i02];
		}
	}

	return changement;
}


int main(int argc, char **argv)
{

	printf("Debut programme \n");

double ratio;
double lambda1,lambda2,lambda3,lambda4,debit1,debit2,debit3,debit4,debit1_c,debit2_c,debit3_c,debit4_c,somme_debit1,somme_debit2,somme_debit3,somme_debit4,somme2;
double *debit1_it,*debit2_it,*debit3_it,*debit4_it,*volfrac_it;

int cpt1,cpt2,cpt3,cpt4;

	ni = 200;
	nj = 200;
	int bordure=10;
	int n,m;
	m=1; //Number of constraints
	n=(ni-2*bordure)*(nj-2*bordure); //Number of design variables
	int nelx=ni-2*bordure;
	int nely=nj-2*bordure;

// Graham : variables related to MMA, useless for LS
	/*double Xmin = 0.0001;
	double Xmax = 1;
	double movlim = 0.1;*/
	double lambdaV=5e-4;
	int changement;
	double K=0.1;
	double dt=0.1;


	/*double *xmma = new double[n];
	double *xmma2 = new double[n];
	double *xold = new double[n];
	double *f = new double[1];
	double *df = new double[n];
	double *df2 = new double[n];
	double *g = new double[m];
	double *dg = new double[n];
	double *xmin = new double[n];
	double *xmax = new double[n];

	double *af = new double[n];
	double *bf = new double[n];
	// Initialize MMA
	MMASolver *mma = new MMASolver(n,m);*/

	cudaSetDevice(1);
	checkCudaErrors(cudaDeviceReset());


	lbm_it_max=1e6;
	opt_it_max=200;
	display=1000;
	eps=1e-6;
	vxin = 0.01;
	roout = 1;
	//m=0.2;
	volfrac=0.5;
	alpha_max=1; // Graham : force term parameter : useless for you
	q=1;// Graham : force term parameter : useless for you
	nmax=10;
	omega1=1;
  double rmin=2; //Graham : density filter radius : useless for you

	kvisc = 0.1;//(double)vxin*ni / Re;
	tau = 3 * kvisc + 0.5;

	rtau = 1.f / tau;
	rtau1 = 1.f - rtau;

	block_x=10;
	block_y=10;


	///////////////////////////////////////////
		dimx2 = (ni + block_x - 1) / block_x ;
		dimy2 = (nj + block_y - 1) / block_y;

		grid2 = dim3(dimx2, dimy2, 1);
		block2 = dim3(block_x , block_y, 1);

		debit1_it=(double *)calloc(opt_it_max,sizeof(double));
		debit2_it=(double *)calloc(opt_it_max,sizeof(double));
		debit3_it=(double *)calloc(opt_it_max,sizeof(double));
		debit4_it=(double *)calloc(opt_it_max,sizeof(double));
		volfrac_it=(double *)calloc(opt_it_max,sizeof(double));

	//checkCudaErrors(cudaSetDevice(0));
	//checkCudaErrors(cudaDeviceReset());

	//(cudaHostAlloc(&h1_f0, ni*nj*sizeof(double), cudaHostAllocPortable)_
	//cudaHostAlloc(&h1_f1, ni*nj*sizeof(double), cudaHostAllocPortable);

	malloc_f_gpu1(ni, nj, nelx,nely);			printf("malloc ok \n");
	Init_CPU_gpu1(ni, nj, nelx,nely, roout, vxin, kvisc,volfrac,domain_optim);	printf("init ok \n");
	malloc_fhost_gpu1(ni, nj, nelx,nely);		printf("alloc gpu ok \n");
	Copy_CPU_to_GPU1(ni, nj, nelx,nely);	printf("copy gpu ok \n");

	int ni2=ni-2*bordure;
	int i1,j1;

// Graham : MMA Stuff : useless for you

/*for (i = 0; i < ni-2*bordure; i++) {
	for (j = 0; j < nj-2*bordure; j++){

			i1=i+bordure;
				j1=j+bordure;

					i0 = I2D(ni, i1, j1);
					i02 = I2D(ni2, i, j);

	xmma[i02] = h1_gamma[i0];
	xold[i02] = h1_gamma[i0];


}
}

for (i=0;i<n;i++) {
	af[i] = 0;
	bf[i] = 0;
}*/



	printf("GPU1 ok \n");

	changement = 1;

//	start_cpu = clock();
	printf("OPTIMIZATION PROBLEM - BEGINNING \n ");

	for (k2=0;k2<opt_it_max;k2++){

		 printf("Optimization iteration = %d \n", k2);

		 if (changement == 1){

			for (j = 0; j < nj; j++){
				for (i = 0; i < ni; i++) {

				i0 = I2D(ni, i, j);
					tampon1[i0] = 0;
					tampon2[i0]=0;

			}
		}

	printf("FORWARD PROBLEM - BEGINNING \n ");

	initialisation_kernel << <grid2, block2 >> >(ni,nj,pitch,
	d1_f0,d1_f1,d1_f2,d1_f3,d1_f4,d1_f5,d1_f6,d1_f7,d1_f8,d1_f0_1,d1_f1_1,d1_f2_1,d1_f3_1,d1_f4_1,d1_f5_1,d1_f6_1,d1_f7_1,d1_f8_1,d1_vx,d1_vy,d1_rho);
	err = cudaGetLastError();if (err != cudaSuccess)	printf("Error initialize direct: %s\n", cudaGetErrorString(err));

	printf("Initialize Kernel ok \n");

	k1=1;
	epsilon1=1;
	epsilon2=1;
	cpt=1;


	while ( epsilon2>eps && k1<lbm_it_max){

		apply_collide_kernel << <grid2, block2 >> >(ni, nj, pitch, alpha_max,q,rtau,rtau1,d1_gamma,
		d1_f0,d1_f1,	d1_f2,	d1_f3,	d1_f4,	d1_f5,	d1_f6,	d1_f7,	d1_f8,		d1_f0_1,	d1_f1_1,	d1_f2_1,	d1_f3_1,	d1_f4_1,	d1_f5_1,	d1_f6_1,	d1_f7_1,	d1_f8_1,uin_data);

		err = cudaGetLastError();if (err != cudaSuccess)	printf("Error collide direct: %s\n", cudaGetErrorString(err));
	//	printf("Collide Kernel 1 ok \n");


		apply_collide_kernel << <grid2, block2 >> >(ni, nj, pitch, alpha_max,q,rtau,rtau1,d1_gamma,
		d1_f0_1,	d1_f1_1,	d1_f2_1,	d1_f3_1,	d1_f4_1,	d1_f5_1,	d1_f6_1,		d1_f7_1,	d1_f8_1,		d1_f0,	d1_f1,	d1_f2,	d1_f3,	d1_f4,	d1_f5,	d1_f6,	d1_f7,	d1_f8, uin_data);

	//	printf("Collide Kernel 2 ok \n");

		if (k1 == cpt * display){
			//printf("Calcul convergence \n");
			//Calcul de la convergence et affichage
			macro_kernel << <grid2, block2 >> >(ni, nj, pitch, 	d1_f0,	d1_f1,	d1_f2,	d1_f3,	d1_f4,	d1_f5,	d1_f6,	d1_f7,	d1_f8,		d1_vx,d1_vy,d1_rho);

	cudaMemcpy2D((void *)h1_vx, sizeof(double)*ni, (void *)d1_vx, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
	cudaMemcpy2D((void *)h1_vy, sizeof(double)*ni, (void *)d1_vy, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);


	epsilon2 = convergence(ni, nj, h1_vx, tampon2);

					for (j = 0; j < nj; j++){
						for (i = 0; i < ni; i++) {

							i0 = I2D(ni, i, j);
							tampon2[i0] = h1_vx[i0];
							}
				}
		//printf("k=%d, et erreur Vitesse =%lg \n ", k1, epsilon2);
				cpt = cpt + 1;
		}
		k1=k1+1;
	}

			macro_kernel << <grid2, block2 >> >(ni, nj, pitch, 	d1_f0,	d1_f1,	d1_f2,	d1_f3,	d1_f4,	d1_f5,	d1_f6,	d1_f7,	d1_f8,	d1_vx,d1_vy,d1_rho);

cudaMemcpy2D((void *)h1_R, sizeof(double)*ni, (void *)d1_rho, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);

	if(k2==0){
	somme=0;
	i=0;
	//for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){


			i0 = I2D(ni, i, j);

			if(h1_gamma[i0]==1) 	{
				somme=somme+h1_R[i0];

			}
					//printf("h1_R[i0]=%lg \n",h1_R[i0]);

		//	}


	//}
}
}
cpt=0;
cpt1=0;
cpt2=0;
cpt3=0;
cpt4=0;
somme_debit1=0;
somme_debit2=0;
somme_debit3=0;
somme_debit4=0;
somme2=0;

for (i = 0; i < ni; i++){
for (j = 0; j < nj; j++){
	i0 = I2D(ni, i, j);

if(debit_host[i0]==5){
	somme2=somme2+h1_vx[i0];
	cpt=cpt+1;
}

if(debit_host[i0]==1) {
	somme_debit1=somme_debit1+h1_vy[i0];
	cpt1++;
}
if(debit_host[i0]==2) {
	somme_debit2=somme_debit2+h1_vx[i0];
		cpt2++;
}
if(debit_host[i0]==3) {
	somme_debit3=somme_debit3+h1_vx[i0];
		cpt3++;
}
if(debit_host[i0]==4) {
	somme_debit4=somme_debit4-h1_vy[i0];
		cpt4++;
}
}
}

deltap_max=nmax*somme;
debit=(double)somme2/cpt;

lambda1=0*-4e-3*exp(-0.001*k2);
lambda2=0*-4e-3*exp(-0.001*k2);
lambda3=0*-4e-3*exp(-0.001*k2);
lambda4=0*-4e-3*exp(-0.001*k2);
debit1=(double)somme_debit1/cpt1;
debit2=(double)somme_debit2/cpt2;
debit3=(double)somme_debit3/cpt3;
debit4=(double)somme_debit4/cpt4;
debit1_c=0.25*debit;
debit2_c=0.25*debit;//(debit2+debit3);
debit3_c=0.25*debit;//(debit2+debit3);
debit4_c=0.25*debit;
printf("debit=%f, debit1=%f , debit2=%f ,debit3=%f, debit4=%f \n",debit,debit1,debit2,debit3,debit4);
printf("cpt=%d, cpt1=%d , cpt2=%d ,cpt3=%d, cpt4=%d \n",cpt,cpt1,cpt2,cpt3,cpt4);
printf("pression=%lg \n",somme);
debit1_it[k2]=debit1;
debit2_it[k2]=debit2;
debit3_it[k2]=debit3;
debit4_it[k2]=debit4;


somme=0;
i=0;
//for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){


			i0 = I2D(ni, i, j);

		if(h1_gamma[i0]==1)	somme=somme+h1_R[i0];


//}
}
cost_function1=omega1*somme;
ratio=(double)somme/deltap_max;

printf("cost function1 = %lg \n", cost_function1);



	cost_function=omega1*cost_function1;
	printf("cost function = %lg \n", cost_function);


	printf("ADJOINT PROBLEM - BEGINNING \n ");

	for (j = 0; j < nj; j++){
		for (i = 0; i < ni; i++) {

		i0 = I2D(ni, i, j);
					tampon2[i0]=0;

	}
}

	initialisation_adjoint_kernel << <grid2, block2 >> >(ni,nj,pitch, d1_fa0,	d1_fa1,	d1_fa2,	d1_fa3,	d1_fa4,	d1_fa5,		d1_fa6,	d1_fa7,	d1_fa8,	d1_fa0_1,	d1_fa1_1,	d1_fa2_1,	d1_fa3_1,	d1_fa4_1,	d1_fa5_1,		d1_fa6_1,	d1_fa7_1,	d1_fa8_1);

	k1=1;
	epsilon2=1;
	epsilon1=1;
	cpt=1;



while (epsilon2>eps && k1<lbm_it_max){

		apply_collide_kernel_adjoint << <grid2, block2 >> >(ni, nj, pitch, omega1, alpha_max,q, deltap_max,ratio,rtau,rtau1,d1_gamma,
		d1_fa0,	d1_fa1,	d1_fa2,	d1_fa3,	d1_fa4,	d1_fa5,	d1_fa6,	d1_fa7,	d1_fa8,	d1_fa0_1,	d1_fa1_1,	d1_fa2_1,	d1_fa3_1,	d1_fa4_1,
		d1_fa5_1,	d1_fa6_1,	d1_fa7_1,	d1_fa8_1,	d1_rho,d1_vx,d1_vy,uin_data,lambda1,lambda2,lambda3,lambda4,debit1,debit2,debit3,debit4,
	debit1_c,debit2_c,debit3_c,debit4_c,debit_data);

		err = cudaGetLastError();if (err != cudaSuccess)	printf("Error collide adjoint: %s\n", cudaGetErrorString(err));


		apply_collide_kernel_adjoint << <grid2, block2 >> >(ni, nj, pitch, omega1,alpha_max,q,deltap_max,ratio,rtau,rtau1,d1_gamma,
		d1_fa0_1,	d1_fa1_1,	d1_fa2_1,	d1_fa3_1,	d1_fa4_1,	d1_fa5_1,	d1_fa6_1,	d1_fa7_1,	d1_fa8_1,		d1_fa0,	d1_fa1,	d1_fa2,	d1_fa3,	d1_fa4,	d1_fa5,
		d1_fa6,	d1_fa7,	d1_fa8,	d1_rho,d1_vx,d1_vy,uin_data,lambda1,lambda2,lambda3,lambda4,debit1,debit2,debit3,debit4,
	debit1_c,debit2_c,debit3_c,debit4_c,debit_data);

		if (k1 == cpt *display){
			//Calcul de la convergence et affichage


cudaMemcpy2D((void *)h1_adjoint, sizeof(double)*ni, (void *)d1_fa1, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);

	epsilon2 = convergence(ni, nj,h1_adjoint, tampon2);

					for (j = 0; j < nj; j++){
						for (i = 0; i < ni; i++) {

							i0 = I2D(ni, i, j);
							tampon2[i0] = h1_adjoint[i0];

					}
				}
				cpt=cpt+1;
				//printf("k=%d, et erreur adjoint =%lg \n ", k1, epsilon2);
			}
		k1=k1+1;
	}

}


printf("GRADIENT CALCULATION \n ");
	//////////////// GRADIENT CALCULATION //////////////////////////

	gradient_kernel << <grid2, block2 >> >(ni, nj, pitch, omega1, alpha_max, q,	d1_fa0,	d1_fa1,	d1_fa2,	d1_fa3,	d1_fa4,	d1_fa5,	d1_fa6,	d1_fa7,	d1_fa8,
	 d1_vx,d1_vy,d1_gamma,d1_gradient);

			err = cudaGetLastError();if (err != cudaSuccess)	printf("Error gradient kernel: %s\n", cudaGetErrorString(err));

	/////////////// GEOMETRY UPDATE VIA OPTIMALITY CRITERION ALGORITHM /////////////////////////////////
printf("GEOMETRY UPDATE \n ");

cudaMemcpy2D((void *)h1_gradient, sizeof(double)*ni, (void *)d1_gradient, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);

Level_Set(volfrac,ni,nj,nelx,nely,bordure,K,lambdaV,dt,h1_gradient,phi_n,phi_e,h1_gamma,h1_gradient2);

changement = MAJ_solid(ni, nj, nelx, nely, bordure, phi_e, h1_gamma, h1_gamma2);


// Set outer move limits
//MMA part
/*printf("MMA limits \n");

for (i=0;i<(ni-2*bordure)*(nj-2*bordure);i++) {
	xmax[i] = Mini(Xmax, xmma[i] + movlim); //printf("xmax=%f\n", xmax[i]);
	xmin[i] = Maxi(Xmin, xmma[i] - movlim); //printf("xmin=%f\n", xmin[i]);
}


somme=0;

for (i = 0; i < (ni-2*bordure); i++) {
	for (j = 0; j < (nj-2*bordure); j++){
		i1=i+bordure;
		j1=j+bordure;

			i0 = I2D(ni, i1, j1);
			i02 = I2D(ni2, i, j);

	df[i02] = -h1_gradient[i0];
	dg[i02]=1;
	somme=somme+h1_gamma[i0];
//if(i02<100) printf("df[%d]=%f\n",i02,df[i02]);

}
}
g[0]=-volfrac+(double)somme/((ni-2*bordure)*(nj-2*bordure)); //Graham : this is to put the volume constraint in MMA but in LS you have to do exp(V/Vmax), cf JCP, I will maybe change it too to have a more similar code.

printf("volfrac=%f and g0=%f\n",somme/((ni-2*bordure)*(nj-2*bordure)),g[0]);
volfrac_it[k2]=somme/((ni-2*bordure)*(nj-2*bordure));

printf("Spatial filter \n");
filtre(ni,nj,rmin,h1_gamma,df,df2,domain_optim,bordure); //Graham : useless for you


//OC(ni, nj, df2, h1_gamma, movlim, volfrac,domain_optim);

mma->Update(xmma,df2,g,dg,xmin,xmax); // Graham : to be removed and replaced by LS update

//q=Max(0.1*exp(-0.04*k2),0.001);
filtre2(ni,nj,rmin,xmma,xmma2,bordure); //Graham : useless for you

i02=0;
somme=0;
for (i = 0; i < (ni-2*bordure); i++) {
	for (j = 0; j < (nj-2*bordure); j++){

		i1=i+bordure;
		j1=j+bordure;

			i0 = I2D(ni, i1, j1);
			i02 = I2D(ni2, i, j);

	h1_gamma[i0] = xmma2[i02];
	somme=somme+h1_gamma[i0];


}
}

for (i=0;i<n;i++) {
xold[i] = xmma2[i];
}


printf("volfrac after mma=%f\n",somme/((ni-2*bordure)*(nj-2*bordure)));*/


cudaMemcpy2D((void *)d1_gamma, pitch, (void *)h1_gamma, sizeof(double)*ni, sizeof(double)*ni, nj, cudaMemcpyHostToDevice);

	/////////////// OUTPUT ///////////////////////////////////////////////////////////////////////////
	printf("OUTPUT \n ");
FILE *fichier_gradient2,*fichier_debit2,*fichier_debit3,*fichier_debit1,*fichier_debit4,*fichier_volfrac,*fichier_phi;

			macro_kernel << <grid2, block2 >> >(ni, nj, pitch, 	d1_f0,	d1_f1,	d1_f2,	d1_f3,	d1_f4,	d1_f5,	d1_f6,	d1_f7,	d1_f8,	d1_vx,d1_vy,d1_rho);

	cudaMemcpy2D((void *)h1_vx, sizeof(double)*ni, (void *)d1_vx, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
	cudaMemcpy2D((void *)h1_vy, sizeof(double)*ni, (void *)d1_vy, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
	cudaMemcpy2D((void *)h1_R, sizeof(double)*ni, (void *)d1_rho, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
		strcpy(filename,"LS_phi_it_"); 				sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_phi =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_vx_it_"); 				sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_vx1 =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_vy_it_"); 				sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_vy1 =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_p_it_"); 				sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_p1 =        fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_gamma_it_"); 		sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_gamma1 =    fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_gradient_it_"); 	sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_gradient1 = fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_df_it_"); 	sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_gradient2 = fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	strcpy(filename,"LS_adjoint_it_"); 	sprintf(u2,"%d",k2); strcat(filename,u2);		fichier_adjoint= fopen(filename, "w"); memset(filename, 0, sizeof(filename));
	fichier_debit1=fopen("LS_debit1.txt","w");
	fichier_debit2=fopen("LS_debit2.txt","w");
	fichier_debit3=fopen("LS_debit3.txt","w");
	fichier_debit4=fopen("LS_debit4.txt","w");
	fichier_volfrac=fopen("LS_volfrac.txt","w");

		for (i = 0; i < ni*nj; i++){
		fprintf(fichier_vx1, "%lg\n", sqrt(h1_vx[i]*h1_vx[i]+h1_vy[i]*h1_vy[i]));
		fprintf(fichier_vy1, "%lg\n", h1_vy[i]);
		fprintf(fichier_p1, "%lg\n", (double)(h1_R[i]-1)/3);
		fprintf(fichier_gamma1, "%lg\n", h1_gamma[i]);
		fprintf(fichier_gradient1, "%lg\n", h1_gradient[i]);
		fprintf(fichier_adjoint,"%lg\n",h1_adjoint[i]);
			fprintf(fichier_gradient2, "%f\n", h1_gradient2[i]);
	}
		for (i = 0; i < nelx*nely; i++){
			fprintf(fichier_phi, "%f\n", phi_e[i]);
			//if(i<100) printf("df[%d]=%f\n",i,df[i]);
		}

	for (i = 0; i < opt_it_max; i++){
		fprintf(fichier_debit2, "%f\n", debit2_it[i]);
		fprintf(fichier_debit3, "%f\n", debit3_it[i]);
		fprintf(fichier_debit1, "%f\n", debit1_it[i]);
		fprintf(fichier_debit4, "%f\n", debit4_it[i]);
		fprintf(fichier_volfrac, "%f\n", volfrac_it[i]);
	}
	fclose(fichier_debit1);
	fclose(fichier_debit2);
	fclose(fichier_debit3);
	fclose(fichier_debit4);
	fclose(fichier_volfrac);
	fclose(fichier_phi);

/*FILE *adjoint;

cudaMemcpy2D((void *)h1_f->d0, sizeof(double)*ni, (void *)d1_f0, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d1, sizeof(double)*ni, (void *)d1_f1, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d2, sizeof(double)*ni, (void *)d1_f2, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d3, sizeof(double)*ni, (void *)d1_f3, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d4, sizeof(double)*ni, (void *)d1_f4, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d5, sizeof(double)*ni, (void *)d1_f5, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d6, sizeof(double)*ni, (void *)d1_f6, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d7, sizeof(double)*ni, (void *)d1_f7, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);
cudaMemcpy2D((void *)h1_f->d8, sizeof(double)*ni, (void *)d1_f8, pitch, sizeof(double)*ni, nj, cudaMemcpyDeviceToHost);


adjoint=fopen("fa_0.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d0[i]);fclose(adjoint);
adjoint=fopen("fa_1.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d1[i]);fclose(adjoint);
adjoint=fopen("fa_2.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d2[i]);fclose(adjoint);
adjoint=fopen("fa_3.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d3[i]);fclose(adjoint);
adjoint=fopen("fa_4.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d4[i]);fclose(adjoint);
adjoint=fopen("fa_5.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d5[i]);fclose(adjoint);
adjoint=fopen("fa_6.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d6[i]);fclose(adjoint);
adjoint=fopen("fa_7.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d7[i]);fclose(adjoint);
adjoint=fopen("fa_8.txt","w"); for(i=0;i<ni*nj;i++) fprintf(adjoint,"%f\n",h1_f->d8[i]);fclose(adjoint);*/


	fclose(fichier_vx1);
	fclose(fichier_vy1);
  fclose(fichier_p1);
	fclose(fichier_gamma1);
	fclose(fichier_gradient1);
	fclose(fichier_gradient2);
	fclose(fichier_adjoint);


}
	return 0;
}
