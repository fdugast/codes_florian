
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <sys/sysinfo.h>
#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)



double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, double rmin, double *x, double *df, double *df2)
{
	int ni2,nj2;
	int i,j,i0,i02,i2,j2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;

		for (j = 0; j < nj2; j++){
			for (i = 0; i < ni2; i++) {

			i0 = I2D(ni2,i, j);

				somme=0;
				somme2=0;
							for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {

									i02 = I2D(ni2,i2,j2);

								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];


				}
			}


			if(x[i0]*somme!=0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}


}

void filtre2(int ni, int nj, int nk, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			for (k = 0; k < nk2; k++){

		i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
								for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {
								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
						fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}
				}

						if(somme!=0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void OC (int ni, int nj, double *gradient, double *gamma0, double m, double volfrac){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*sizeof(double));

		for (j = 0; j < nj; j++){
	for (i = 0; i < ni; i++) {

				i0 = I2D(ni,i, j);
				gamma_old[i0]=gamma0[i0];
			}
		}



	while ((l2-l1)> 1e-9){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
				for (j = 0; j < nj; j++){
	for (i = 0; i < ni; i++) {

			i0 = I2D(ni, i, j);

	//printf("ok boucle \n");
				if(gradient[i0]>0) {

					printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				//if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);


	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
	for (j = 0; j < nj; j++){
for (i = 0; i < ni; i++) {

		i0 = I2D(ni,i, j);

					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

	}
}

volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}

}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/

printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

void KM_matrix(double delta_x, double delta_y, double E, double nu,double *KE){
	double N[4];
	double DN[2][4];
	double gxyz[3][3];
	double C[3][3]={0};
	double wg[3][3],wxyz;
	int i,j,k,i1,j1,i3,j3,k3;
	double a,b,c,a1,b1,c1,den;
	double x,y,z,sommeK,somme;

	//den=1.f/((1+nu)*(1-2*nu));
	den=1.f/(1-nu*nu);
	printf("den=%f\n",den);
	C[0][0]=den*(1);
	C[0][1]=den*nu;
	C[1][0]=den*nu;
	C[1][1]=den*(1);
	C[2][2]=0.5*den*(1-nu);


	//gxyz[0][0]=0;
	//gxyz[0][1]=-0.577350269189626;
	gxyz[0][2]=-0.774596669241483;
	//gxyz[1][0]=0;
	//gxyz[1][1]=0.577350269189626;
	gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
	gxyz[2][2]=0.774596669241483;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;

a=0.5;
b=0.5;

				printf("boucle\n");
				a1=a*delta_x;
				b1=b*delta_y;

	for (i1=0;i1<8;i1++){
			for (j1=0;j1<8;j1++){

sommeK=0;

					for (i=0;i<3;i++){
						for (j=0;j<3;j++){

								x=a1*gxyz[i][2];
								y=b1*gxyz[j][2];
								wxyz=wg[i][2]*wg[j][2];


								N[0]=(a1-x)*(b1-y)/(2*a1*2*b1);
								N[1]=(a1+x)*(b1-y)/(2*a1*2*b1);
								N[2]=(a1+x)*(b1+y)/(2*a1*2*b1);
								N[3]=(a1-x)*(b1+y)/(2*a1*2*b1);

								DN[0][0]=-(b1-y)/(2*a1*2*b1); DN[1][0]=-(a1-x)/(2*a1*2*b1);
								DN[0][1]=(b1-y)/(2*a1*2*b1);  DN[1][1]=-(a1+x)/(2*a1*2*b1);
								DN[0][2]=(b1+y)/(2*a1*2*b1);  DN[1][2]=(a1+x)/(2*a1*2*b1);
								DN[0][3]=-(b1+y)/(2*a1*2*b1); DN[1][3]=(a1-x)/(2*a1*2*b1);


								double prod1[3][8]={0};
								double prod2[8][8]={0};
								double B[3][8]={0};

								for(i3=0;i3<4;i3++){

											B[0][0+2*i3]=DN[0][i3]; 				B[0][1+2*i3]=0;
											B[1][0+2*i3]=0; 								B[1][1+2*i3]=DN[1][i3];
											B[2][0+2*i3]=DN[1][i3]; 				B[2][1+2*i3]=DN[0][i3];

									}



								for(i3=0;i3<8;i3++){
									for(j3=0;j3<3;j3++){
										somme=0;
										for(k3=0;k3<3;k3++){
											somme=somme+B[k3][i3]*C[j3][k3];
										}
									prod1[j3][i3]=somme;
								}
							}

							for(i3=0;i3<8;i3++){
								for(j3=0;j3<8;j3++){
									somme=0;
									for(k3=0;k3<3;k3++){
										somme=somme+prod1[k3][i3]*B[k3][j3];
									}
								prod2[j3][i3]=somme;
							}
						}

								sommeK=sommeK+prod2[i1][j1]*wxyz*a1*b1;

								//printf("sommeK=%f\n",sommeK);


						}
					}
					KE[i1+8*j1]=sommeK;
					printf("%f ",KE[i1+8*j1]);
}

printf("\n");

}

printf("j1=%d\n",j1);
}

int main(int argc,char **args)
{

	printf("Debut programme \n");




printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
    PetscInt      n,its,nz,nz2,start,end,start2,end2,Id,Istart,Iend,nlocal;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew,rnorm,KE1[64],ke[64],*array;
  PetscScalar y;
	char filename[200],u2[200];
	PetscInt indice_d;


	struct sysinfo info;

	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
  int nele,ndof,nb_freedof,boucle=0;

	double volfrac;

  PetscInt start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int icol,jcol,kcol,id,ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim,freq;
	int cpt1,cpt2,cpt3,cpt4,k3_it;
	FILE *fichier;


	ni = 32;
	nj = 20;

	ni2=ni+1;

	freq=1;

	ndof=(ni+1)*(nj+1);
	nele=ni*nj;

	MMASolver *mma = new MMASolver(nele,1,1,10,0);
	opt_it_max=600;
	alpha=1; // coeff pow compliance
	rmin=1.2;
	volfrac=0.4;
	double hconv=10;
	double delta,somme,somme1,somme2,delta_x1,delta_y1,delta_x2,delta_y2,delta_x3,delta_y3,delta_x4,delta_y4,delta_t,total_time,time;
	delta_x1=(double)1; //domaine de 10 mm par 10 mm ...
	delta_y1=(double)1;
	double E=1;
	double E0=1;
	double Emin=1e-6;
	double nu=0.3;


	KM_matrix(delta_x1,delta_y1,E,nu,KE1);

//getchar();

	double *F;
	F=(double *)calloc(1,sizeof(double));
	int *edofMat, *fixeddof,*fixednid, *loaddof, *loadnid;
	edofMat=(int *)calloc(8,sizeof(int));


	penal=3;

		double *x1;

		double  *gradient, *lu;
		int *q1, *num_elem, *num_elem1, *num_elem2;
		double *gamma_x, *gamma_h;
		double *fonction_cout, *porosity;
		double *xmma,*xmin,*xmax,*x1_a,*ux_1,*uy_1,*df,*dv,*dg,*g,*xold1,*xold2,Xmin,Xmax,movlim,*fx;
		fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
		porosity=(double *)calloc(opt_it_max,sizeof(double));
		xmma=(double *)calloc(nele,sizeof(double));
		xold1=(double *)calloc(nele,sizeof(double));
		xold2=(double *)calloc(nele,sizeof(double));
		xmin=(double *)calloc(nele,sizeof(double));
		xmax=(double *)calloc(nele,sizeof(double));
		df=(double *)calloc(nele,sizeof(double));
		dv=(double *)calloc(nele,sizeof(double));
		dg=(double *)calloc(nele,sizeof(double));
		g=(double *)calloc(1,sizeof(double));
		fx=(double *)calloc(1,sizeof(double));
		gradient=(double *)calloc(nele,sizeof(double));

		x1_a=(double *)calloc(2*ndof,sizeof(double));
		ux_1=(double *)calloc(ndof,sizeof(double));
		uy_1=(double *)calloc(ndof,sizeof(double));

		Xmin=0.1;
		Xmax=0.95;
		movlim=0.2;

		// Set outer move limits
		/*for (i=0;i<nele;i++) {
		xmax[i] = Xmax;
		xmin[i] = Xmin;
		//printf("i=%d, x[i]=%f, xmin[i]=%f et xmax[i]=%f  \n",i,x[i],xmin[i],xmax[i]);
	}*/


		AmgXSolver solver;
	//ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/AMG_CLASSICAL_AGGRESSIVE_L1.json" ); CHKERRQ(ierr);
	//ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/JACOBI.json" ); CHKERRQ(ierr);
	ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);

	 // ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,2*ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);

	/*ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
	printf("nlocal=%d\n",nlocal);*/


	ierr = VecSet(b,0);CHKERRQ(ierr);

	//Load
	double ratio;

	int n1=(nj+1);

 fixednid=(int *)calloc(n1,sizeof(int));
 fixeddof=(int *)calloc(2*n1,sizeof(int)); //indice pour vecteur f

 cpt=0;
 int il=0;
 for (j=0;j<nj+1;j++){

			 fixednid[cpt]=il+1+(ni+1)*j;
			 //printf("fixednid=%d\n",fixednid[cpt]);
			 cpt++;
 }

//Fixeddofs
cpt=0;
for (j=0;j<2*n1;j=j+2){
	fixeddof[j]=2*fixednid[cpt]-2;
	fixeddof[j+1]=2*fixednid[cpt]-1;
	cpt++;
}

/*for (j=0;j<2*n1;j++){
printf("fixeddof=%d\n",fixeddof[j]);
}*/

							 for(i=0;i<nele;i++){
							 xmma[i]=volfrac;
							 xold1[i]=xmma[i];
							 xold2[i]=xmma[i];
							 }


							 	for (k3_it=0;k3_it<opt_it_max;k3_it++){

					 if(k3_it>0) ierr = MatDestroy(&K1);CHKERRQ(ierr);
					 ierr = MatCreate(PETSC_COMM_WORLD,&K1);CHKERRQ(ierr);
					 ierr = MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,2*ndof,2*ndof);CHKERRQ(ierr);
					 ierr = MatSetFromOptions(K1);CHKERRQ(ierr);
					 ierr = MatSetUp(K1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(K1,50,NULL,50,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(K1,50,NULL);CHKERRQ(ierr);

					 // Loop over elements
					 for (i0=0; i0<nele; i0++) {

						 jcol=i0/ni;
					 //printf("jcol=%d\n",jcol);
					 icol=	i0%ni;
					 id=(ni+1)*jcol*2+2*icol;

					 edofMat[0]=id;//x1
					 edofMat[1]=id+1;//y1
					 edofMat[2]=id+2;//x2
					 edofMat[3]=id+3;//y2
					 edofMat[4]=id+2+2*(ni+1); //x3
					 edofMat[5]=id+3+2*(ni+1); //y3
					 edofMat[6]=id+2*(ni+1); //x4
					 edofMat[7]=id+1+2*(ni+1); //y4


					 PetscScalar dens = Emin + PetscPowScalar(xmma[i0],penal)*(E0-Emin);
							for (l=0;l<8*8;l++){
							 ke[l]=KE1[l]*dens;
							 //me[l]=ME1[l]*c[i0]/delta_t;
									 }
						 // Add values to the sparse matrix
						 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						 //ierr = MatSetValues(M1,4,edofMat,4,edofMat,me,ADD_VALUES);
					 //	}
				 		}

					 ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

					 ierr = MatZeroRows(K1,2*n1,fixeddof,1,0,0);CHKERRQ(ierr);


	printf("Load \n");

	loadnid=(int *)calloc(1,sizeof(int));
	loaddof=(int *)calloc(1,sizeof(int)); //indice pour vecteur f
	loadnid[0]=ni+1;
	loaddof[0]=2*loadnid[0]-1;
	//printf("loaddof=%d\n",loaddof[0]);
	F[0]=-1;

	ierr = VecSetValues(b,1,loaddof,F,INSERT_VALUES);CHKERRQ(ierr);

	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);



printf("Optimization iteration = %d \n", k3_it);
printf("Solver beginning \n");


ierr = solver.setA(K1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
//VecView(b,PETSC_VIEWER_STDOUT_SELF);
//MatView(K1,PETSC_VIEWER_STDOUT_SELF);
VecSet(b,0);

//getchar();
//VecView(x,PETSC_VIEWER_STDOUT_SELF);



for (i=0;i<2*(ni+1)*(nj+1);i++){
VecGetValues(x,1,&i,&y);
x1_a[i]=y;
}

cpt=0;
 for (j=0;j<nj+1;j++){
for (i=0;i<ni+1;i++){

	 ux_1[(ni+1)*j+i]=x1_a[cpt];   cpt=cpt+1;
	 uy_1[(ni+1)*j+i]=x1_a[cpt];   cpt=cpt+1;
}
}

//Sensitivity
fx[0] = 0.0;

for (i0=0; i0<nele; i0++) {

	jcol=i0/ni;
//printf("jcol=%d\n",jcol);
icol=	i0%ni;
id=(ni+1)*jcol*2+2*icol;

edofMat[0]=id;//x1
edofMat[1]=id+1;//y1
edofMat[2]=id+2;//x2
edofMat[3]=id+3;//y2
edofMat[4]=id+2+2*(ni+1); //x3
edofMat[5]=id+3+2*(ni+1); //y3
edofMat[6]=id+2*(ni+1); //x4
edofMat[7]=id+1+2*(ni+1); //y4

double uKu1=0.0;

for (PetscInt k=0;k<8;k++){
for (PetscInt h=0;h<8;h++){
uKu1 += x1_a[edofMat[k]]*KE1[k*8+h]*x1_a[edofMat[h]];
}
}

// Add to objective
fx[0] += (Emin + PetscPowScalar(xmma[i0],penal)*(E0 - Emin))*uKu1;
// Set the Senstivity
gradient[i0]= -1.0 * penal*PetscPowScalar(xmma[i0],penal-1)*(E0 - Emin)*uKu1;
dv[i0]=1;
}

filtre(ni,nj,rmin,xmma,gradient,df);

printf("MMA beginning \n ");
/////////////////////////////////////////////////////////////



//Cost function and constraint values


somme=0;
for (i=0;i<nele;i++) {
dg[i]=1;
somme=somme+xmma[i];
}
g[0]=-volfrac+(double)somme/(ni*nj);


// Call the update method
//OC(ni,nj, df, xmma,movlim, volfrac);
for (i=0;i<nele;i++) {
	xmax[i] = Xmax;//Min(Xmax, xmma[i] + movlim);
	xmin[i] = Xmin;//Max(Xmin, xmma[i] - movlim);
//printf("i=%d, x[i]=%f, xmin[i]=%f et xmax[i]=%f  \n",i,x[i],xmin[i],xmax[i]);
}

mma->Update(xmma,df,g,dg,xmin,xmax,xold1,xold2);
for (i=0;i<nele;i++) 	{
	xold2[i]=xold1[i];
	xold1[i] = xmma[i];
}

fonction_cout[k3_it]=fx[0];
porosity[k3_it]=(double)somme/(ni*nj);
printf("fonction cout =%lg, volfrac=%f\n",fonction_cout[k3_it],(double)somme/(ni*nj));
//k3_it++;
if(k3_it%100==0){

 strcpy(filename,"rho_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<nele;i++) 	fprintf(fichier,"%f\n",xmma[i]);
fclose(fichier);

strcpy(filename,"gradient_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<nele;i++) 	fprintf(fichier,"%f\n",gradient[i]);
fclose(fichier);

strcpy(filename,"ux_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) 	fprintf(fichier,"%f\n",ux_1[i]);
fclose(fichier);

strcpy(filename,"uy_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<ndof;i++) 	fprintf(fichier,"%f\n",uy_1[i]);
fclose(fichier);

strcpy(filename,"J_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<k3_it;i++) 	fprintf(fichier,"%f\n",fonction_cout[i]);
fclose(fichier);

strcpy(filename,"porosity_it_"); 				sprintf(u2,"%d",k3_it); strcat(filename,u2);		fichier =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));
for (i=0;i<k3_it;i++) 	fprintf(fichier,"%f\n",porosity[i]);
fclose(fichier);
}

}

MPI_Barrier(PETSC_COMM_WORLD);

solver.finalize();


printf("Fin boucle optim \n");

ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);

//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);

return ierr;
}
