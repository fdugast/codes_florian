
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <sys/sysinfo.h>
#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)




double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}

void filtre(int ni, int nj, int nk, double rmin, double *x, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
					for (k = 0; k < nk2; k++){

				i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
							for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {

								i02 = I3D(ni2,nj2, i, j,k);
					if(domain_optim[i02]==1){
								fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*x[i02]*df[i02];
						}
					}
				}
			}


			if(x[i0]*somme!=0) {
				df2[i0]=somme2/(x[i0]*somme);
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void filtre2(int ni, int nj, int nk, double rmin, double *df, double *df2, int *domain_optim)
{
	int ni2,nj2,nk2;
	int i,j,k,i0,i02,i2,j2,k2;
	double somme,somme2,fac;

	ni2=ni;
	nj2=nj;
	nk2=nk;

	for (i = 0; i < ni2; i++) {
		for (j = 0; j < nj2; j++){
			for (k = 0; k < nk2; k++){

		i0 = I3D(ni2,nj2, i, j,k);

				somme=0;
				somme2=0;
				for (i2 = Maxi(i-floor(rmin),0); i2 <= Mini(i+floor(rmin),(ni2-1)); i2++) {
					for (j2 = Maxi(j-floor(rmin),0); j2 <= Mini(j+floor(rmin),(nj2-1)); j2++) {
								for (k2 = Maxi(k-floor(rmin),0); k2 <= Mini(k+floor(rmin),(nk2-1)); k2++) {
								i02 = I2D(ni2,i2,j2);
									if(domain_optim[i02]==1){
						fac=rmin-sqrt((i-i2)*(i-i2)+(j-j2)*(j-j2)+(k-k2)*(k-k2));
								somme=somme+Max(0,fac);
								somme2=somme2+Max(0,fac)*df[i02];
							}
						}
					}
				}

						if(somme!=0) {
				df2[i0]=somme2/somme;
			}
			else {
				df2[i0]=df[i0];
			}

		}
		}
	}

}

void OC (int ni, int nj, int nk, double *gradient, double *gamma0, double m, double volfrac, int *domain_optim){

	double xmin,lambda,l1,l2,Be,somme,move1,move2,xe_Ben,volfrac2,test;
	int i,j,k,i0,cpt;
	l1=0;
	l2=1e8;
	xmin=0.0001;
	double *gamma_old;
	gamma_old = (double*)malloc(ni*nj*nk*sizeof(double));

	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
				for (k = 0; k < nk; k++){
				i0 = I3D(ni,nj, i, j,k);
				gamma_old[i0]=gamma0[i0];
			}
		}
	}


	while ((l2-l1)> 1e-4){
		lambda=0.5*(l1+l2);



		///// Modification of gamma0 /////////
	for (i = 0; i < ni; i++) {
		for (j = 0; j < nj; j++){
			for (k = 0; k < nk; k++){
			i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
	//printf("ok boucle \n");
				if(gradient[i0]>0) {

					printf("Warning : gradient positif : i=%d, j=%d , k=%d et gradient =%lg \n",i,j,k,gradient[i0]);
						gradient[i0]=0;
				}
				Be=-gradient[i0]/lambda;
				xe_Ben=gamma_old[i0]*sqrt(Be);

				move1=gamma_old[i0]-m;
				move2=gamma_old[i0]+m;

			/*	if(i==50 && j==50 && k==50) {
					printf("move1=%f \n",move1);
					printf("move2=%f \n",move2);
					printf("xe_Ben=%f \n",xe_Ben);
				}*/



				if(xe_Ben <= MAX(xmin,move1)){
				gamma0[i0]=MAX(xmin,move1);
			//	if(vrai==0) printf("cas 1 : gamma0[i0]=%f \n",gamma0[i0]);
					}

				if(xe_Ben > MAX(xmin,move1) && xe_Ben < MIN(1,move2)){
				gamma0[i0]=xe_Ben;
		//	printf("cas 2 : gamma0[i0]=%f \n",gamma0[i0]);
				}

				if(xe_Ben >= MIN(1,move2)){
				gamma0[i0]=MIN(1,move2);
				//printf("cas 3 : gamma0[i0]=%f \n",gamma0[i0]);
				}
				if(i==25 && j==50) printf("gradient[100,90]=%f, gamma[100,90]=%f\n",gradient[i0],gamma0[i0]);
			}
		}
	}
}

///// Volume constraint verification /////////
somme=0;
cpt=0;
for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
		for (k = 0; k < nk; k++){
		i0 = I3D(ni,nj, i, j,k);
if(domain_optim[i0]==1){
					//if(domain_optim[i0]==1)
					somme=somme+gamma0[i0];
					cpt=cpt+1;
					//if(gamma0[i0]!=0.35) printf("i=%d, j=%d , k=%d et gamma0[i0]=%f \n",i,j,k,gamma0[i0]);

}
		}
	}
}

volfrac2=somme/cpt;


if(volfrac2>volfrac){
	l1=lambda;
}
else {
	l2=lambda;
}
printf("diff=%lg, l1=%lg, l2=%lg, lambda=%lg et volfrac2=%f \n",l2-l1,l1,l2,lambda,volfrac2);
}

/*for (i = 0; i < ni; i++) {
	for (j = 0; j < nj; j++){
			i0 = I2D(ni, i, j);
					gamma0[i0]=1-gamma0[i0];
		}
	}*/


}

void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

void KM_matrix(double delta_x, double delta_y,double delta_z, double *KE, double *ME){
	double N[8];
	double DN[3][8];
	double gxyz[3][3];
	double wg[3][3],wxyz;
	int i,j,k,i1,j1;
	double a,b,c,a1,b1,c1;
	double x,y,z,sommeK,sommeM;

	//gxyz[0][0]=0;
	//gxyz[0][1]=-0.577350269189626;
	gxyz[0][2]=-0.774596669241483;
	//gxyz[1][0]=0;
	//gxyz[1][1]=0.577350269189626;
	gxyz[1][2]=0;
//	gxyz[2][0]=0;
//	gxyz[2][1]=0;
	gxyz[2][2]=0.774596669241483;

	wg[0][0]=2;
	wg[0][1]=1;
	wg[0][2]=0.555555555555556;
	wg[1][0]=0;
	wg[1][1]=1;
	wg[1][2]=0.888888888888889;
	wg[2][0]=0;
	wg[2][1]=0;
	wg[2][2]=0.555555555555556;

a=0.5;
b=0.5;
c=0.5;
				printf("boucle\n");
				a1=a*delta_x;
				b1=b*delta_y;
				c1=c*delta_z;

	for (i1=0;i1<8;i1++){
			for (j1=0;j1<8;j1++){
sommeK=0;
sommeM=0;
					for (i=0;i<3;i++){
						for (j=0;j<3;j++){
							for (k=0;k<3;k++){

								x=a1*gxyz[i][2];
								y=b1*gxyz[j][2];
								z=c1*gxyz[k][2];
								wxyz=wg[i][2]*wg[j][2]*wg[k][2];


								N[0]=(a1-x)*(b1-y)*(c1-z)/(2*a1*2*b1*2*c1);
								N[1]=(a1+x)*(b1-y)*(c1-z)/(2*a1*2*b1*2*c1);
								N[2]=(a1+x)*(b1+y)*(c1-z)/(2*a1*2*b1*2*c1);
								N[3]=(a1-x)*(b1+y)*(c1-z)/(2*a1*2*b1*2*c1);

								N[4]=(a1-x)*(b1-y)*(c1+z)/(2*a1*2*b1*2*c1);
								N[5]=(a1+x)*(b1-y)*(c1+z)/(2*a1*2*b1*2*c1);
								N[6]=(a1+x)*(b1+y)*(c1+z)/(2*a1*2*b1*2*c1);
								N[7]=(a1-x)*(b1+y)*(c1+z)/(2*a1*2*b1*2*c1);

								DN[0][0]=-(b1-y)*(c1-z)/(2*a1*2*b1*2*c1); DN[1][0]=-(a1-x)*(c1-z)/(2*a1*2*b1*2*c1);  DN[2][0]=-(a1-x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][1]=(b1-y)*(c1-z)/(2*a1*2*b1*2*c1);  DN[1][1]=-(a1+x)*(c1-z)/(2*a1*2*b1*2*c1);  DN[2][1]=-(a1+x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][2]=(b1+y)*(c1-z)/(2*a1*2*b1*2*c1);  DN[1][2]=(a1+x)*(c1-z)/(2*a1*2*b1*2*c1);   DN[2][2]=-(a1+x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][3]=-(b1+y)*(c1-z)/(2*a1*2*b1*2*c1); DN[1][3]=(a1-x)*(c1-z)/(2*a1*2*b1*2*c1);   DN[2][3]=-(a1-x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][4]=-(b1-y)*(c1+z)/(2*a1*2*b1*2*c1); DN[1][4]=-(a1-x)*(c1+z)/(2*a1*2*b1*2*c1);   DN[2][4]=(a1-x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][5]=(b1-y)*(c1+z)/(2*a1*2*b1*2*c1);  DN[1][5]=-(a1+x)*(c1+z)/(2*a1*2*b1*2*c1);   DN[2][5]=(a1+x)*(b1-y)/(2*a1*2*b1*2*c1);
								DN[0][6]=(b1+y)*(c1+z)/(2*a1*2*b1*2*c1);  DN[1][6]=(a1+x)*(c1+z)/(2*a1*2*b1*2*c1);  DN[2][6]=(a1+x)*(b1+y)/(2*a1*2*b1*2*c1);
								DN[0][7]=-(b1+y)*(c1+z)/(2*a1*2*b1*2*c1); DN[1][7]=(a1-x)*(c1+z)/(2*a1*2*b1*2*c1);  DN[2][7]=(a1-x)*(b1+y)/(2*a1*2*b1*2*c1);

								sommeK=sommeK+(DN[0][i1]*DN[0][j1]+DN[1][i1]*DN[1][j1]+DN[2][i1]*DN[2][j1])*wxyz*a1*b1*c1;
								sommeM=sommeM+(N[i1]*N[j1])*wxyz*a1*b1*c1;
								//printf("sommeK=%f\n",sommeK);

							}
						}
					}
					KE[i1+8*j1]=sommeK;
					ME[i1+8*j1]=sommeM;
					printf("%f ",KE[i1+8*j1]);
		}
			printf("\n");
	}


}

int main(int argc,char **args)
{

	printf("Debut programme \n");


clock_t clock_start,clock_end;

clock_start=clock();

printf("Init Petsc/Amgx\n");

	Vec            x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,M1,U1,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end,start2,end2,Id,Istart,Iend,nlocal;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew,rnorm,ke[64],me[64],*array;
	PetscScalar   KE1[64],ME1[64],KE2[64],ME2[64],KE3[64],ME3[64],KE4[64],ME4[64];
	PetscScalar   KE5[64],ME5[64],KE6[64],ME6[64],KE7[64],ME7[64],KE8[64],ME8[64];
  PetscScalar y,conv,Tp1;
	char filename[200],u2[200];
	PetscInt indice_d;

	struct sysinfo info;

	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  PetscInt start1,end1;
  double difference;
  double rmin,penal;

  PetscInt *nnz, *nnz2;
	int icol,jcol,kcol,id,ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim,freq;
	int cpt1,cpt2,cpt3,cpt4,cpt5,cpt6,cpt7,cpt8,cpt9,cpt10,cpt11,cpt12,cpt13,cpt14,cpt15,cpt16,cpt17,cpt18,cpt19,cpt20,cpt21,cpt22,cpt23,cpt24,cpt25,cpt26;
	long unsigned int memoire_1,memoire_2,memoire_3,memoire_4,memoire_5,memoire_6,memoire_7;

	double domain_x=0.2;
	double domain_y=0.2;
	double domain_z=0.2;
	int div;

	ni = 25;
	nj = 25;
	nk=25;
	ni2=ni+1;
	div=8;

	freq=1;

	ndof=(ni+1)*(nj+1)*(nk+1);
	nele=ni*nj*nk;
	opt_it_max=1;
	alpha=1; // coeff pow compliance
	rmin=1.4;
	volfrac=1;
	double hconv=10;
	double delta,somme,somme1,somme2,delta_t,total_time,time;
	double delta_x1,delta_y1,delta_z1,delta_x2,delta_y2,delta_z2,delta_x3,delta_y3,delta_z3,delta_x4,delta_y4,delta_z4;
	double delta_x5,delta_y5,delta_z5,delta_x6,delta_y6,delta_z6,delta_x7,delta_y7,delta_z7,delta_x8,delta_y8,delta_z8;

	//delta_x1=(double)domain_x/ni; 	delta_y1=(double)domain_y/nj;	  delta_z1=(double)domain_z/nk;
	delta_x1=0.001; delta_y1=0.001; delta_z1=0.001;
	delta_x2=div*delta_x1;	delta_y2=1*delta_y1;  delta_z2=1*delta_z1;
	delta_x3=1*delta_x1;	delta_y3=div*delta_y1;  delta_z3=1*delta_z1;
	delta_x4=div*delta_x1;	delta_y4=div*delta_y1;  delta_z4=1*delta_z1;
	delta_x5=1*delta_x1;	delta_y5=1*delta_y1;  delta_z5=div*delta_z1;
	delta_x6=div*delta_x1;	delta_y6=1*delta_y1;  delta_z6=div*delta_z1;
	delta_x7=1*delta_x1;	delta_y7=div*delta_y1;  delta_z7=div*delta_z1;
	delta_x8=div*delta_x1;	delta_y8=div*delta_y1;  delta_z8=div*delta_z1;

	KM_matrix(delta_x1,delta_y1,delta_z1, KE1, ME1);
	KM_matrix(delta_x2,delta_y2,delta_z2, KE2, ME2);
	KM_matrix(delta_x3,delta_y3,delta_z3, KE3, ME3);
	KM_matrix(delta_x4,delta_y4,delta_z4, KE4, ME4);
	KM_matrix(delta_x5,delta_y5,delta_z5, KE5, ME5);
	KM_matrix(delta_x6,delta_y6,delta_z6, KE6, ME6);
	KM_matrix(delta_x7,delta_y7,delta_z7, KE7, ME7);
	KM_matrix(delta_x8,delta_y8,delta_z8, KE8, ME8);

	//Calcul de KE et Me

	delta_t=1; // si delta t trop fort oscillations sur compliance
	total_time=1;
	int nbsteps;
	nbsteps=total_time/delta_t;
	double memory_cleared=1;


	//Medium properties
	int *face, *corner, *edge;
	face=(int *)calloc(nele,sizeof(int));
	edge=(int *)calloc(nele,sizeof(int));
	corner=(int *)calloc(nele,sizeof(int));

	double *F;
	F=(double *)calloc(8,sizeof(double));
	int *edofMat;
	edofMat=(int *)calloc(8,sizeof(int));
	domain_optim=(int *)calloc(nele,sizeof(int));
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp2,Tp3,Tp4,Tp5,Tp6,Tair;
	cp=(double *)calloc(nele,sizeof(double));
	lambda=(double *)calloc(nele,sizeof(double));
	derivee_c=(double *)calloc(nele,sizeof(double));
	derivee_lambda=(double *)calloc(nele,sizeof(double));
	rho=(double *)calloc(nele,sizeof(double));
	diff=(double *)calloc(nele,sizeof(double));
	c=(double *)calloc(nele,sizeof(double));
	rho_solide=1;
	lambda_solide=1;
	cp_solide=1;
	/*rho_solide=1;
	lambda_solide=1;
	cp_solide=1;*/


	rho_vide=1;
	lambda_vide=1;
	cp_vide=1000;
	//rho_vide=1000;
	//lambda_vide=100;
	//cp_vide=500;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	Qvol=1e4;//10;
	T0=0; //Initial temperature
	Tp1=0;
	Tp2=0;
	Tp3=0;
	Tp4=0;
	Tp5=0;
	Tp6=0;

	Qsurf=0*1e6;
	Tair=30;
	penal=3;

		double *x1;
		double *Told;
		double *T_transient, *pos_q1_transient, *T_transient2, *derivee_T_transient,*adjoint_transient,*prod_KU_transient,*second_membre_transient,*derivee_T;
		double *q2,*q3,*q4,*pos_q1,*pos_q2,*pos_q3,*pos_q4, *compliance, *gradient, *lu;
		int *q1, *num_elem, *num_elem1, *num_elem2;
		double *gamma_x, *gamma_h;
		double *fonction_cout;
		fonction_cout=(double *)calloc(opt_it_max,sizeof(double));
	//	solide=(double *)calloc(nele,sizeof(double));
		gamma_h=(double *)calloc(nele,sizeof(double));
		gamma_x=(double *)calloc(nele,sizeof(double));
		x1=(double *)calloc(ndof,sizeof(double));
		Told=(double *)calloc(ndof,sizeof(double));
		T_transient=(double *)calloc(nbsteps,sizeof(double));

	/*FILE *fichier_K1=fopen("Ke_3d_a1_b1_c1.txt","r");
	FILE *fichier_K2=fopen("Ke_3d_a2_b1_c1.txt","r");
	FILE *fichier_K3=fopen("Ke_3d_a1_b2_c1.txt","r");
	FILE *fichier_K4=fopen("Ke_3d_a2_b2_c1.txt","r");
	FILE *fichier_K5=fopen("Ke_3d_a1_b1_c2.txt","r");
	FILE *fichier_K6=fopen("Ke_3d_a2_b1_c2.txt","r");
	FILE *fichier_K7=fopen("Ke_3d_a1_b2_c2.txt","r");
	FILE *fichier_K8=fopen("Ke_3d_a2_b2_c2.txt","r");

	FILE *fichier_M1=fopen("Me_3d_a1_b1_c1.txt","r");
	FILE *fichier_M2=fopen("Me_3d_a2_b1_c1.txt","r");
	FILE *fichier_M3=fopen("Me_3d_a1_b2_c1.txt","r");
	FILE *fichier_M4=fopen("Me_3d_a2_b2_c1.txt","r");
	FILE *fichier_M5=fopen("Me_3d_a1_b1_c2.txt","r");
	FILE *fichier_M6=fopen("Me_3d_a2_b1_c2.txt","r");
	FILE *fichier_M7=fopen("Me_3d_a1_b2_c2.txt","r");
	FILE *fichier_M8=fopen("Me_3d_a2_b2_c2.txt","r");

	float test;
		for (i=0;i<64;i++){
			fscanf(fichier_K1,"%f\n",&test);		KE1[i]=test;
			fscanf(fichier_K2,"%f\n",&test);		KE2[i]=test;
			fscanf(fichier_K3,"%f\n",&test);		KE3[i]=test;
			fscanf(fichier_K4,"%f\n",&test);		KE4[i]=test;
			fscanf(fichier_K5,"%f\n",&test);		KE5[i]=test;
			fscanf(fichier_K6,"%f\n",&test);		KE6[i]=test;
			fscanf(fichier_K7,"%f\n",&test);		KE7[i]=test;
			fscanf(fichier_K8,"%f\n",&test);		KE8[i]=test;

			fscanf(fichier_M1,"%f\n",&test);		ME1[i]=test;
			fscanf(fichier_M2,"%f\n",&test);		ME2[i]=test;
			fscanf(fichier_M3,"%f\n",&test);		ME3[i]=test;
			fscanf(fichier_M4,"%f\n",&test);		ME4[i]=test;
			fscanf(fichier_M5,"%f\n",&test);		ME5[i]=test;
			fscanf(fichier_M6,"%f\n",&test);		ME6[i]=test;
			fscanf(fichier_M7,"%f\n",&test);		ME7[i]=test;
			fscanf(fichier_M8,"%f\n",&test);		ME8[i]=test;

		}

fclose(fichier_K1);
fclose(fichier_K2);
fclose(fichier_K3);
fclose(fichier_K4);
fclose(fichier_K5);
fclose(fichier_K6);
fclose(fichier_K7);
fclose(fichier_K8);

fclose(fichier_M1);
fclose(fichier_M2);
fclose(fichier_M3);
fclose(fichier_M4);
fclose(fichier_M5);
fclose(fichier_M6);
fclose(fichier_M7);
fclose(fichier_M8);*/



	AmgXSolver solver;
	//ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/FGMRES_CLASSICAL_AGGRESSIVE_HMIS.json" ); CHKERRQ(ierr);

	  //ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	int test1;
	FILE *domain;
		domain=fopen("domain3d_div32.txt","r");
	double somme_domain;

	double volfrac2;
	cpt=0;
	for (k=0; k<nk; k++) {
	for (j=0; j<nj; j++) {
	for (i=0; i<ni; i++) {

					i0=I3D(ni,nj,i,j,k);
				fscanf(domain,"%d\n",&test1);
				domain_optim[i0]=8;//test1;
				if(domain_optim[i0]==0) printf("WARNING ZERO\n");
				if(domain_optim[i0]>8) printf("WARNING UP TO 8\n");
		//					if(test==0) cpt=cpt+1;
	//			somme_domain=somme_domain+(1-test1);

			}
	}
}
fclose(domain);

printf("FICHIER OK \n");

	//Definition of boundary conditions

		for (k=0; k<nk; k++) {
				for (j=0; j<nj; j++) {
	for (i=0; i<ni; i++) {
				i0=I3D(ni,nj,i,j,k);
		if(i==0 || j==0 || i==(ni-1) || j==(nj-1) || k==0 || k==(nk-1)){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;
				if(i==0 && j==0 && k==0) corner[i0]=1;
				if(i==(ni-1) && j==0 && k==0) corner[i0]=2;
				if(i==(ni-1) && j==(nj-1) && k==0) corner[i0]=3;
				if(i==0 && j==(nj-1) && k==0) corner[i0]=4;
				if(i==0 && j==0 && k==(nk-1)) corner[i0]=5;
				if(i==(ni-1) && j==0 && k==(nk-1)) corner[i0]=6;
				if(i==(ni-1) && j==(nj-1) && k==(nk-1)) corner[i0]=7;
				if(i==0 && j==(nj-1) && k==(nk-1)) corner[i0]=8;

				//j=0
				if(k==0 && j==0 && corner[i0]==0) edge[i0]=1;
				if(i==(ni-1) && j==0 && corner[i0]==0) edge[i0]=2;
				if(k==(nk-1) && j==0 && corner[i0]==0) edge[i0]=3;
				if(i==0 && j==0 && corner[i0]==0) edge[i0]=4;

				//j=nj-1
				if(k==0 && j==(nj-1) && corner[i0]==0) edge[i0]=5;
				if(i==(ni-1) && j==(nj-1) && corner[i0]==0) edge[i0]=6;
				if(k==(nk-1) && j==(nj-1) && corner[i0]==0) edge[i0]=7;
				if(i==0 && j==(nj-1) && corner[i0]==0) edge[i0]=8;

				if(i==0 && k==0 && corner[i0]==0) edge[i0]=9;
				if(i==(ni-1) && k==0 && corner[i0]==0) edge[i0]=10;
				if(i==(ni-1) && k==(nk-1) && corner[i0]==0) edge[i0]=11;
				if(i==0 && k==(nk-1) && corner[i0]==0) edge[i0]=12;

				if(j==0 && corner[i0]==0 && edge[i0]==0) face[i0]=1;
				if(i==0 && corner[i0]==0 && edge[i0]==0) face[i0]=2;
				if(j==(nj-1) && corner[i0]==0 && edge[i0]==0) face[i0]=3;
				if(i==(ni-1) && corner[i0]==0 && edge[i0]==0) face[i0]=4;
				if(k==0 && corner[i0]==0 && edge[i0]==0) face[i0]=5;
				if(k==(nk-1) && corner[i0]==0 && edge[i0]==0) face[i0]=6;

			}

		}
	}
}

	for (i=0;i<ndof;i++){
		Told[i]=T0;
	}

  printf("Creation objects \n");

  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&D);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&Tn);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F2);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&T);CHKERRQ(ierr);
	/*ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
	printf("nlocal=%d\n",nlocal);*/

	ierr = VecSet(D,0);CHKERRQ(ierr);
	ierr = VecSet(Tn,0);CHKERRQ(ierr);
	ierr = VecSet(F1,0);CHKERRQ(ierr);
	ierr = VecSet(F2,0);CHKERRQ(ierr);
	ierr = VecSet(T,0);CHKERRQ(ierr);
		ierr = VecSet(b,0);CHKERRQ(ierr);

	double ratio;
			for (k=0; k<nk; k++) {
				for (j=0; j<nj; j++) {
					for (i=0; i<ni; i++) {
							i0=I3D(ni,nj,i,j,k);
											lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;c[i0]=c_solide;
									}
								}
							}

					start = rank*(nele/size) + ((nele%size) < rank ? (nele%size) : rank);
				  end   = start + nele/size + ((nele%size) > rank);

					start1 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
				  end1   = start1 + ndof/size + ((ndof%size) > rank);

					const PetscInt *nindices1,*nindices2,*nindices3,*nindices4,*nindices5,*nindices6,*nindices7,*nindices8,*nindices9,*nindices10,*nindices11,*nindices12,*nindices13,
					*nindices14,*nindices15,*nindices16,*nindices17,*nindices18,*nindices19,*nindices20,*nindices21,*nindices22,*nindices23,*nindices24,*nindices25,*nindices26;

					//DIRICHLET BOUNDARY CONDITION
						cpt1=0;	cpt2=0;	cpt3=0;	cpt4=0;	cpt5=0;	cpt6=0; cpt7=0;	cpt8=0;	cpt9=0;	cpt10=0;	cpt11=0;	cpt12=0; cpt13=0;	cpt14=0;	cpt15=0;	cpt16=0;
						cpt17=0;	cpt18=0;	cpt19=0;	cpt20=0;	cpt21=0;	cpt22=0; cpt23=0;	cpt24=0;	cpt25=0;	cpt26=0;

						for (k=0; k<nk; k++) {
						 for (j=0; j<nj; j++) {
				for (i=0; i<ni; i++) {
				i0=I3D(ni,nj,i,j,k);

				if(face[i0]==1) cpt1++;  if(face[i0]==2) cpt2++;	 if(face[i0]==3) cpt3++;	if(face[i0]==4) cpt4++;	 if(face[i0]==5) cpt5++; if(face[i0]==6) cpt6++;

				if(edge[i0]==1) cpt7++;	 if(edge[i0]==2) cpt8++;		if(edge[i0]==3) cpt9++;	  if(edge[i0]==4) cpt10++;	if(edge[i0]==5) cpt11++;
				if(edge[i0]==6) cpt12++; if(edge[i0]==7) cpt13++;   if(edge[i0]==8) cpt14++;	if(edge[i0]==9) cpt15++;	if(edge[i0]==10) cpt16++;
				if(edge[i0]==11) cpt17++;		if(edge[i0]==12) cpt18++;

				if(corner[i0]==1) cpt19++;
				if(corner[i0]==2) cpt20++;
				if(corner[i0]==3) cpt21++;
				if(corner[i0]==4) cpt22++;
				if(corner[i0]==5) cpt23++;
				if(corner[i0]==6) cpt24++;
				if(corner[i0]==7) cpt25++;
				if(corner[i0]==8) cpt26++;


						 }
					 }
				 }
					 PetscInt *edofMat1,*edofMat2,*edofMat3,*edofMat4,*edofMat5,*edofMat6, *edofMat7, *edofMat8, *edofMat9, *edofMat10, *edofMat11, *edofMat12,
					 *edofMat13, *edofMat14, *edofMat15, *edofMat16, *edofMat17, *edofMat18, *edofMat19, *edofMat20, *edofMat21, *edofMat22, *edofMat23,
					 *edofMat24, *edofMat25, *edofMat26;

					 int nb_BC1,nb_BC2,nb_BC3,nb_BC4,nb_BC5,nb_BC6,nb_BC7,nb_BC8,nb_BC9,nb_BC10,nb_BC11,nb_BC12,nb_BC13,nb_BC14,nb_BC15,nb_BC16,nb_BC17,nb_BC18,
					 nb_BC19,nb_BC20,nb_BC21,nb_BC22,nb_BC23,nb_BC24,nb_BC25,nb_BC26;

					 nb_BC1=cpt1;	 nb_BC2=cpt2;	 nb_BC3=cpt3;	 nb_BC4=cpt4;	 nb_BC5=cpt5;	 nb_BC6=cpt6;	 nb_BC7=cpt7;		 nb_BC8=cpt8;	 nb_BC9=cpt9;	 nb_BC10=cpt10;
					 nb_BC11=cpt11;	 nb_BC12=cpt12;	 nb_BC13=cpt13;	 nb_BC14=cpt14;	 nb_BC15=cpt15;	 nb_BC16=cpt16;	 nb_BC17=cpt17;	 nb_BC18=cpt18;	 nb_BC19=cpt19;
					 nb_BC20=cpt20;	 nb_BC21=cpt21;	 nb_BC22=cpt22;	 nb_BC23=cpt23;	 nb_BC24=cpt24;	 nb_BC25=cpt25;	 nb_BC26=cpt26;

					PetscMalloc1(4*nb_BC1,&edofMat1);		PetscMalloc1(4*nb_BC2,&edofMat2);			PetscMalloc1(4*nb_BC3,&edofMat3);
					PetscMalloc1(4*nb_BC4,&edofMat4);		PetscMalloc1(4*nb_BC5,&edofMat5); 	  PetscMalloc1(4*nb_BC6,&edofMat6);

					PetscMalloc1(6*nb_BC7,&edofMat7);		PetscMalloc1(6*nb_BC8,&edofMat8);		PetscMalloc1(6*nb_BC9,&edofMat9);			PetscMalloc1(6*nb_BC10,&edofMat10);
					PetscMalloc1(6*nb_BC11,&edofMat11); PetscMalloc1(6*nb_BC12,&edofMat12);	PetscMalloc1(6*nb_BC13,&edofMat13);		PetscMalloc1(6*nb_BC14,&edofMat14);
					PetscMalloc1(6*nb_BC15,&edofMat15);	PetscMalloc1(6*nb_BC16,&edofMat16);	PetscMalloc1(6*nb_BC17,&edofMat17); 	PetscMalloc1(6*nb_BC18,&edofMat18);

					PetscMalloc1(7*nb_BC19,&edofMat19);	PetscMalloc1(7*nb_BC20,&edofMat20);	PetscMalloc1(7*nb_BC21,&edofMat21);		PetscMalloc1(7*nb_BC22,&edofMat22);
					PetscMalloc1(7*nb_BC23,&edofMat23);	PetscMalloc1(7*nb_BC24,&edofMat24);	PetscMalloc1(7*nb_BC25,&edofMat25);		PetscMalloc1(7*nb_BC26,&edofMat26);

					IS is1,is2,is3,is4,is5,is6,is7,is8,is9,is10,is11,is12,is13,is14,is15,is16,is17,is18,is19,is20,is21,is22,is23,is24,is25,is26;
					PetscInt n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20,n21,n22,n23,n24,n25,n26;

					cpt1=0;	cpt2=0;	cpt3=0;	cpt4=0;	cpt5=0;	cpt6=0; cpt7=0;	cpt8=0;	cpt9=0;	cpt10=0;	cpt11=0;	cpt12=0; cpt13=0;	cpt14=0;	cpt15=0;	cpt16=0;
					cpt17=0;	cpt18=0;	cpt19=0;	cpt20=0;	cpt21=0;	cpt22=0; cpt23=0;	cpt24=0;	cpt25=0;	cpt26=0;

					for (i0=start; i0<end; i0++) {

						kcol=i0/(ni*nj);
						jcol=(i0%(nj*ni))/ni;
						icol=	(i0%(nj*ni))%ni;
						id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

					edofMat[0]=id;
					edofMat[1]=id+1;
					edofMat[2]=id+1+(ni+1);
					edofMat[3]=id+(ni+1);
					edofMat[4]=id + (ni+1)*(nj+1);
					edofMat[5]=id+1 + (ni+1)*(nj+1);
					edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
					edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

							 if(face[i0]==1){
								 edofMat1[4*cpt1]=edofMat[0];
								 edofMat1[1+4*cpt1]=edofMat[1];
								 edofMat1[2+4*cpt1]=edofMat[4];
								 edofMat1[3+4*cpt1]=edofMat[5];
									cpt1=cpt1+1;
							 }

							 if(face[i0]==2){
								 edofMat2[4*cpt2]=edofMat[0];
								 edofMat2[1+4*cpt2]=edofMat[4];
								 edofMat2[2+4*cpt2]=edofMat[3];
								 edofMat2[3+4*cpt2]=edofMat[7];
								 cpt2=cpt2+1;
							 }

							 if(face[i0]==3){
								 edofMat3[4*cpt3]=edofMat[2];
								 edofMat3[1+4*cpt3]=edofMat[3];
								 edofMat3[2+4*cpt3]=edofMat[6];
								 edofMat3[3+4*cpt3]=edofMat[7];
									cpt3=cpt3+1;
							 }

							 if(face[i0]==4){
								 edofMat4[4*cpt4]=edofMat[1];
								 edofMat4[1+4*cpt4]=edofMat[2];
								 edofMat4[2+4*cpt4]=edofMat[5];
								 edofMat4[3+4*cpt4]=edofMat[6];
								 cpt4=cpt4+1;
							 }

							 if(face[i0]==5){
								 edofMat5[4*cpt5]=edofMat[0];
								 edofMat5[1+4*cpt5]=edofMat[1];
								 edofMat5[2+4*cpt5]=edofMat[2];
								 edofMat5[3+4*cpt5]=edofMat[3];
									cpt5=cpt5+1;
							 }

							 if(face[i0]==6){
								 edofMat6[4*cpt6]=edofMat[4];
								 edofMat6[1+4*cpt6]=edofMat[5];
								 edofMat6[2+4*cpt6]=edofMat[6];
								 edofMat6[3+4*cpt6]=edofMat[7];
									cpt6=cpt6+1;
							 }
							 /////////////////////////

							 if(edge[i0]==1){ //67
								 edofMat7[6*cpt7]=edofMat[0];
								 edofMat7[1+6*cpt7]=edofMat[1];
								 edofMat7[2+6*cpt7]=edofMat[2];
								 edofMat7[3+6*cpt7]=edofMat[3];
								 edofMat7[4+6*cpt7]=edofMat[4];
								 edofMat7[5+6*cpt7]=edofMat[5];
								 cpt7++;
							 }

							 if(edge[i0]==2){ //37
								 edofMat8[6*cpt8]=edofMat[0];
								 edofMat8[1+6*cpt8]=edofMat[1];
								 edofMat8[2+6*cpt8]=edofMat[2];
								 edofMat8[3+6*cpt8]=edofMat[6];
								 edofMat8[4+6*cpt8]=edofMat[4];
								 edofMat8[5+6*cpt8]=edofMat[5];
								 cpt8++;
							 }

							 if(edge[i0]==3){ //23
								 edofMat9[6*cpt9]=edofMat[0];
								 edofMat9[1+6*cpt9]=edofMat[1];
								 edofMat9[2+6*cpt9]=edofMat[6];
								 edofMat9[3+6*cpt9]=edofMat[7];
								 edofMat9[4+6*cpt9]=edofMat[4];
								 edofMat9[5+6*cpt9]=edofMat[5];
								 cpt9++;
							 }

							 if(edge[i0]==4){ //26
								 edofMat10[6*cpt10]=edofMat[0];
								 edofMat10[1+6*cpt10]=edofMat[1];
								 edofMat10[2+6*cpt10]=edofMat[7];
								 edofMat10[3+6*cpt10]=edofMat[3];
								 edofMat10[4+6*cpt10]=edofMat[4];
								 edofMat10[5+6*cpt10]=edofMat[5];
								 cpt10++;
							 }

							 if(edge[i0]==5){ //45
								 edofMat11[6*cpt11]=edofMat[0];
								 edofMat11[1+6*cpt11]=edofMat[1];
								 edofMat11[2+6*cpt11]=edofMat[2];
								 edofMat11[3+6*cpt11]=edofMat[3];
								 edofMat11[4+6*cpt11]=edofMat[6];
								 edofMat11[5+6*cpt11]=edofMat[7];
								 cpt11++;
							 }

							 if(edge[i0]==6){ //04
								 edofMat12[6*cpt12]=edofMat[6];
								 edofMat12[1+6*cpt12]=edofMat[1];
								 edofMat12[2+6*cpt12]=edofMat[2];
								 edofMat12[3+6*cpt12]=edofMat[3];
								 edofMat12[4+6*cpt12]=edofMat[7];
								 edofMat12[5+6*cpt12]=edofMat[5];
								 cpt12++;
							 }

							 if(edge[i0]==7){ //01
								 edofMat13[6*cpt13]=edofMat[6];
								 edofMat13[1+6*cpt13]=edofMat[7];
								 edofMat13[2+6*cpt13]=edofMat[2];
								 edofMat13[3+6*cpt13]=edofMat[3];
								 edofMat13[4+6*cpt13]=edofMat[4];
								 edofMat13[5+6*cpt13]=edofMat[5];
								 cpt13++;
							 }

							 if(edge[i0]==8){ //15
								 edofMat14[6*cpt14]=edofMat[0];
								 edofMat14[1+6*cpt14]=edofMat[6];
								 edofMat14[2+6*cpt14]=edofMat[2];
								 edofMat14[3+6*cpt14]=edofMat[3];
								 edofMat14[4+6*cpt14]=edofMat[4];
								 edofMat14[5+6*cpt14]=edofMat[7];
								 cpt14++;
							 }

							 if(edge[i0]==9){ //56
								 edofMat15[6*cpt15]=edofMat[0];
								 edofMat15[1+6*cpt15]=edofMat[1];
								 edofMat15[2+6*cpt15]=edofMat[2];
								 edofMat15[3+6*cpt15]=edofMat[3];
								 edofMat15[4+6*cpt15]=edofMat[4];
								 edofMat15[5+6*cpt15]=edofMat[7];
								 cpt15++;
							 }

							 if(edge[i0]==10){ //47
								 edofMat16[6*cpt16]=edofMat[0];
								 edofMat16[1+6*cpt16]=edofMat[1];
								 edofMat16[2+6*cpt16]=edofMat[2];
								 edofMat16[3+6*cpt16]=edofMat[3];
								 edofMat16[4+6*cpt16]=edofMat[6];
								 edofMat16[5+6*cpt16]=edofMat[5];
								 cpt16++;
							 }

							 if(edge[i0]==11){ //03
								 edofMat17[6*cpt17]=edofMat[6];
								 edofMat17[1+6*cpt17]=edofMat[1];
								 edofMat17[2+6*cpt17]=edofMat[2];
								 edofMat17[3+6*cpt17]=edofMat[7];
								 edofMat17[4+6*cpt17]=edofMat[4];
								 edofMat17[5+6*cpt17]=edofMat[5];
								 cpt17++;
							 }

							 if(edge[i0]==12){//12
								 edofMat18[6*cpt18]=edofMat[0];
								 edofMat18[1+6*cpt18]=edofMat[6];
								 edofMat18[2+6*cpt18]=edofMat[7];
								 edofMat18[3+6*cpt18]=edofMat[3];
								 edofMat18[4+6*cpt18]=edofMat[4];
								 edofMat18[5+6*cpt18]=edofMat[5];
								 cpt18++;
							 }

							 ///////////////////////////////////

							 if(corner[i0]==1){ //6
								 edofMat19[7*cpt19]=edofMat[0];
								 edofMat19[1+7*cpt19]=edofMat[1];
								 edofMat19[2+7*cpt19]=edofMat[2];
								 edofMat19[3+7*cpt19]=edofMat[3];
								 edofMat19[4+7*cpt19]=edofMat[4];
								 edofMat19[5+7*cpt19]=edofMat[5];
								 edofMat19[6+7*cpt19]=edofMat[7];
								 cpt19++;
							 }

							 if(corner[i0]==2){ //7
								 edofMat20[7*cpt20]=edofMat[0];
								 edofMat20[1+7*cpt20]=edofMat[1];
								 edofMat20[2+7*cpt20]=edofMat[2];
								 edofMat20[3+7*cpt20]=edofMat[3];
								 edofMat20[4+7*cpt20]=edofMat[4];
								 edofMat20[5+7*cpt20]=edofMat[5];
								 edofMat20[6+7*cpt20]=edofMat[6];
								 cpt20++;
							 }

							 if(corner[i0]==3){ //4
								 edofMat21[7*cpt21]=edofMat[0];
								 edofMat21[1+7*cpt21]=edofMat[1];
								 edofMat21[2+7*cpt21]=edofMat[2];
								 edofMat21[3+7*cpt21]=edofMat[3];
								 edofMat21[4+7*cpt21]=edofMat[6];
								 edofMat21[5+7*cpt21]=edofMat[5];
								 edofMat21[6+7*cpt21]=edofMat[7];
								 cpt21++;
							 }

							 if(corner[i0]==4){ //5
								 edofMat22[7*cpt22]=edofMat[0];
								 edofMat22[1+7*cpt22]=edofMat[1];
								 edofMat22[2+7*cpt22]=edofMat[2];
								 edofMat22[3+7*cpt22]=edofMat[3];
								 edofMat22[4+7*cpt22]=edofMat[4];
								 edofMat22[5+7*cpt22]=edofMat[6];
								 edofMat22[6+7*cpt22]=edofMat[7];
								 cpt22++;
							 }

							 if(corner[i0]==5){ //2
								 edofMat23[7*cpt23]=edofMat[0];
								 edofMat23[1+7*cpt23]=edofMat[1];
								 edofMat23[2+7*cpt23]=edofMat[6];
								 edofMat23[3+7*cpt23]=edofMat[3];
								 edofMat23[4+7*cpt23]=edofMat[4];
								 edofMat23[5+7*cpt23]=edofMat[5];
								 edofMat23[6+7*cpt23]=edofMat[7];
								 cpt23++;
							 }

							 if(corner[i0]==6){ //3
								 edofMat24[7*cpt24]=edofMat[0];
								 edofMat24[1+7*cpt24]=edofMat[1];
								 edofMat24[2+7*cpt24]=edofMat[2];
								 edofMat24[3+7*cpt24]=edofMat[6];
								 edofMat24[4+7*cpt24]=edofMat[4];
								 edofMat24[5+7*cpt24]=edofMat[5];
								 edofMat24[6+7*cpt24]=edofMat[7];
								 cpt24++;
							 }

							 if(corner[i0]==7){ //0
								 edofMat25[7*cpt25]=edofMat[6];
								 edofMat25[1+7*cpt25]=edofMat[1];
								 edofMat25[2+7*cpt25]=edofMat[2];
								 edofMat25[3+7*cpt25]=edofMat[3];
								 edofMat25[4+7*cpt25]=edofMat[4];
								 edofMat25[5+7*cpt25]=edofMat[5];
								 edofMat25[6+7*cpt25]=edofMat[7];
								 cpt25++;
							 }

							 if(corner[i0]==8){ //1
								 edofMat26[7*cpt26]=edofMat[0];
								 edofMat26[1+7*cpt26]=edofMat[6];
								 edofMat26[2+7*cpt26]=edofMat[2];
								 edofMat26[3+7*cpt26]=edofMat[3];
								 edofMat26[4+7*cpt26]=edofMat[4];
								 edofMat26[5+7*cpt26]=edofMat[5];
								 edofMat26[6+7*cpt26]=edofMat[7];
								 cpt26++;
							 }

						 }

					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC1,edofMat1,PETSC_COPY_VALUES,&is1);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC2,edofMat2,PETSC_COPY_VALUES,&is2);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC3,edofMat3,PETSC_COPY_VALUES,&is3);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC4,edofMat4,PETSC_COPY_VALUES,&is4);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC5,edofMat5,PETSC_COPY_VALUES,&is5);
					ISCreateGeneral(PETSC_COMM_SELF,4*nb_BC6,edofMat6,PETSC_COPY_VALUES,&is6);

					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC7,edofMat7,PETSC_COPY_VALUES,&is7);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC8,edofMat8,PETSC_COPY_VALUES,&is8);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC9,edofMat9,PETSC_COPY_VALUES,&is9);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC10,edofMat10,PETSC_COPY_VALUES,&is10);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC11,edofMat11,PETSC_COPY_VALUES,&is11);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC12,edofMat12,PETSC_COPY_VALUES,&is12);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC13,edofMat13,PETSC_COPY_VALUES,&is13);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC14,edofMat14,PETSC_COPY_VALUES,&is14);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC15,edofMat15,PETSC_COPY_VALUES,&is15);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC16,edofMat16,PETSC_COPY_VALUES,&is16);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC17,edofMat17,PETSC_COPY_VALUES,&is17);
					ISCreateGeneral(PETSC_COMM_SELF,6*nb_BC18,edofMat18,PETSC_COPY_VALUES,&is18);

					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC19,edofMat19,PETSC_COPY_VALUES,&is19);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC20,edofMat20,PETSC_COPY_VALUES,&is20);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC21,edofMat21,PETSC_COPY_VALUES,&is21);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC22,edofMat22,PETSC_COPY_VALUES,&is22);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC23,edofMat23,PETSC_COPY_VALUES,&is23);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC24,edofMat24,PETSC_COPY_VALUES,&is24);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC25,edofMat25,PETSC_COPY_VALUES,&is25);
					ISCreateGeneral(PETSC_COMM_SELF,7*nb_BC26,edofMat26,PETSC_COPY_VALUES,&is26);


					ISSortRemoveDups(is1);
					ISSortRemoveDups(is2);
					ISSortRemoveDups(is3);
					ISSortRemoveDups(is4);
					ISSortRemoveDups(is5);
					ISSortRemoveDups(is6);
					ISSortRemoveDups(is7);
					ISSortRemoveDups(is8);
					ISSortRemoveDups(is9);
					ISSortRemoveDups(is10);
					ISSortRemoveDups(is11);
					ISSortRemoveDups(is12);
					ISSortRemoveDups(is13);
					ISSortRemoveDups(is14);
					ISSortRemoveDups(is15);
					ISSortRemoveDups(is16);
					ISSortRemoveDups(is17);
					ISSortRemoveDups(is18);
					ISSortRemoveDups(is19);
					ISSortRemoveDups(is20);
					ISSortRemoveDups(is21);
					ISSortRemoveDups(is22);
					ISSortRemoveDups(is23);
					ISSortRemoveDups(is24);
					ISSortRemoveDups(is25);
					ISSortRemoveDups(is26);


			  	ISGetLocalSize(is1,&n1);
					ISGetLocalSize(is2,&n2);
					ISGetLocalSize(is3,&n3);
					ISGetLocalSize(is4,&n4);
					ISGetLocalSize(is5,&n5);
					ISGetLocalSize(is6,&n6);
					ISGetLocalSize(is7,&n7);
					ISGetLocalSize(is8,&n8);
					ISGetLocalSize(is9,&n9);
					ISGetLocalSize(is10,&n10);
					ISGetLocalSize(is11,&n11);
					ISGetLocalSize(is12,&n12);
					ISGetLocalSize(is13,&n13);
					ISGetLocalSize(is14,&n14);
					ISGetLocalSize(is15,&n15);
					ISGetLocalSize(is16,&n16);
					ISGetLocalSize(is17,&n17);
					ISGetLocalSize(is18,&n18);
					ISGetLocalSize(is19,&n19);
					ISGetLocalSize(is20,&n20);
					ISGetLocalSize(is21,&n21);
					ISGetLocalSize(is22,&n22);
					ISGetLocalSize(is23,&n23);
					ISGetLocalSize(is24,&n24);
					ISGetLocalSize(is25,&n25);
					ISGetLocalSize(is26,&n26);


					ISGetIndices(is1,&nindices1);
					ISGetIndices(is2,&nindices2);
					ISGetIndices(is3,&nindices3);
					ISGetIndices(is4,&nindices4);
					ISGetIndices(is5,&nindices5);
					ISGetIndices(is6,&nindices6);
					ISGetIndices(is7,&nindices7);
					ISGetIndices(is8,&nindices8);
					ISGetIndices(is9,&nindices9);
					ISGetIndices(is10,&nindices10);
					ISGetIndices(is11,&nindices11);
					ISGetIndices(is12,&nindices12);
					ISGetIndices(is13,&nindices13);
					ISGetIndices(is14,&nindices14);
					ISGetIndices(is15,&nindices15);
					ISGetIndices(is16,&nindices16);
					ISGetIndices(is17,&nindices17);
					ISGetIndices(is18,&nindices18);
					ISGetIndices(is19,&nindices19);
					ISGetIndices(is20,&nindices20);
					ISGetIndices(is21,&nindices21);
					ISGetIndices(is22,&nindices22);
					ISGetIndices(is23,&nindices23);
					ISGetIndices(is24,&nindices24);
					ISGetIndices(is25,&nindices25);
					ISGetIndices(is26,&nindices26);


											 //sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared1=%ld\n",memoire_1);

							 ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/AMG_CLASSICAL_AGGRESSIVE_L1.json" ); CHKERRQ(ierr);
							// ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/AMGX/core/configs/JACOBI.json" ); CHKERRQ(ierr);
					 ierr = MatCreate(PETSC_COMM_WORLD,&K1);CHKERRQ(ierr);
					 ierr = MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);

					 ierr = MatSetFromOptions(K1);CHKERRQ(ierr);
					 ierr = MatSetUp(K1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(K1,100,NULL,100,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(K1,100,NULL);CHKERRQ(ierr);
					  //MatSetOption(K1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);

					 //printf("apres malloc K1\n"); getchar();

					 ierr = MatCreate(PETSC_COMM_WORLD,&M1);CHKERRQ(ierr);
					 ierr = MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);

					 ierr = MatSetFromOptions(M1);CHKERRQ(ierr);
					 ierr = MatSetUp(M1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(M1,100,NULL,100,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(M1,100,NULL);CHKERRQ(ierr);
					 	 //MatSetOption(M1,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);
					 //printf("apres malloc M1\n"); getchar();

				// sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared2=%ld\n",memoire_1);

					 //printf("Istart=%d, Iend=%d \n",Istart,Iend);

					 // Loop over elements
					 for (i0=start; i0<end; i0++) {

					 kcol=i0/(ni*nj);
					 jcol=(i0%(nj*ni))/ni;
					 //printf("jcol=%d\n",jcol);
					 icol=	(i0%(nj*ni))%ni;
					 id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

				 edofMat[0]=id;
				 edofMat[1]=id+1;
				 edofMat[2]=id+1+(ni+1);
				 edofMat[3]=id+(ni+1);
				 edofMat[4]=id + (ni+1)*(nj+1);
				 edofMat[5]=id+1 + (ni+1)*(nj+1);
				 edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
				 edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

					 if(domain_optim[i0]==1){
							 for (l=0;l<8*8;l++){
							 ke[l]=KE1[l]*lambda[i0];
							 me[l]=ME1[l]*c[i0]/delta_t;
									 }
						 // Add values to the sparse matrix
						 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
					 //	}
				 		}

						if(domain_optim[i0]==2){
						 for (l=0;l<8*8;l++){
								ke[l]=KE2[l]*lambda[i0];
								me[l]=ME2[l]*c[i0]/delta_t;
										}
							// Add values to the sparse matrix
							ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
	 					 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
						//	}
						 }

						 if(domain_optim[i0]==3){
							 for (l=0;l<8*8;l++){
								 ke[l]=KE3[l]*lambda[i0];
								 me[l]=ME3[l]*c[i0]/delta_t;
										 }
							 // Add values to the sparse matrix
							 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
						 //	}
							}

							if(domain_optim[i0]==4){
								 for (l=0;l<8*8;l++){
									ke[l]=KE4[l]*lambda[i0];
									me[l]=ME4[l]*c[i0]/delta_t;
											}
								// Add values to the sparse matrix
								ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
		 					 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
							//	}
							 }

							 if(domain_optim[i0]==5){
									for (l=0;l<8*8;l++){
									 ke[l]=KE5[l]*lambda[i0];
									 me[l]=ME5[l]*c[i0]/delta_t;
											 }
								 // Add values to the sparse matrix
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
								ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
							 //	}
								}

								if(domain_optim[i0]==6){
									 for (l=0;l<8*8;l++){
										ke[l]=KE6[l]*lambda[i0];
										me[l]=ME6[l]*c[i0]/delta_t;
												}
									// Add values to the sparse matrix
									ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
								 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
								//	}
								 }

								 if(domain_optim[i0]==7){
										for (l=0;l<8*8;l++){
										 ke[l]=KE7[l]*lambda[i0];
										 me[l]=ME7[l]*c[i0]/delta_t;
												 }
									 // Add values to the sparse matrix
									 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
									ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
								 //	}
									}

									if(domain_optim[i0]==8){
										 for (l=0;l<8*8;l++){
											ke[l]=KE8[l]*lambda[i0];
											me[l]=ME8[l]*c[i0]/delta_t;
													}
										// Add values to the sparse matrix
										ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
									 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
									//	}
								}

					 }

					 ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

					 ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


					 ierr = MatZeroRows(K1,n1,nindices1,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n2,nindices2,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n3,nindices3,1,0,0);CHKERRQ(ierr);
			  	 ierr = MatZeroRows(K1,n4,nindices4,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n5,nindices5,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n6,nindices6,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n7,nindices7,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n8,nindices8,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n9,nindices9,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n10,nindices10,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n11,nindices11,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n12,nindices12,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n13,nindices13,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n14,nindices14,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n15,nindices15,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n16,nindices16,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n17,nindices17,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n18,nindices18,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n19,nindices19,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n20,nindices20,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n21,nindices21,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n22,nindices22,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n23,nindices23,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n24,nindices24,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n25,nindices25,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(K1,n26,nindices26,1,0,0);CHKERRQ(ierr);

					 ierr = MatZeroRows(M1,n1,nindices1,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n2,nindices2,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n3,nindices3,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n4,nindices4,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n5,nindices5,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n6,nindices6,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n7,nindices7,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n8,nindices8,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n9,nindices9,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n10,nindices10,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n11,nindices11,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n12,nindices12,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n13,nindices13,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n14,nindices14,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n15,nindices15,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n16,nindices16,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n17,nindices17,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n18,nindices18,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n19,nindices19,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n20,nindices20,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n21,nindices21,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n22,nindices22,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n23,nindices23,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n24,nindices24,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n25,nindices25,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n26,nindices26,1,0,0);CHKERRQ(ierr);


					 ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
					 ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);

					 //ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
					 //ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);


					 int compteur=0;
					 while(compteur<nbsteps){


//	while(compteur<nbsteps ){

	printf("Load \n");

/*ratio=delta_x1*delta_y1*delta_z1;
i=62;
ierr = VecSetValues(b,1,&i,&ratio,INSERT_VALUES);CHKERRQ(ierr);*/

	for (i0=start; i0<end; i0++) {
			kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;
	edofMat[1]=id+1;
	edofMat[2]=id+1+(ni+1);
	edofMat[3]=id+(ni+1);
	edofMat[4]=id + (ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);

 if(domain_optim[i0]==1){
	 ratio=delta_x1*delta_y1*delta_z1;
		  F[0]=ratio*Qvol;//
			F[1]=ratio*Qvol;//
			F[2]=ratio*Qvol;//
			F[3]=ratio*Qvol;//
			F[4]=ratio*Qvol;//
			F[5]=ratio*Qvol;//
			F[6]=ratio*Qvol;//
			F[7]=ratio*Qvol;//

	ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	}

	if(domain_optim[i0]==2){
		ratio=delta_x2*delta_y2*delta_z2;
		F[0]=ratio*Qvol;//
		F[1]=ratio*Qvol;//
		F[2]=ratio*Qvol;//
		F[3]=ratio*Qvol;//
		F[4]=ratio*Qvol;//
		F[5]=ratio*Qvol;//
		F[6]=ratio*Qvol;//
		F[7]=ratio*Qvol;//
		ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	 }

	 if(domain_optim[i0]==3){
		 ratio=delta_x3*delta_y3*delta_z3;
		 F[0]=ratio*Qvol;//
		 F[1]=ratio*Qvol;//
		 F[2]=ratio*Qvol;//
		 F[3]=ratio*Qvol;//
		 F[4]=ratio*Qvol;//
		 F[5]=ratio*Qvol;//
		 F[6]=ratio*Qvol;//
		 F[7]=ratio*Qvol;//
		ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
		}

		if(domain_optim[i0]==4){
			ratio=delta_x4*delta_y4*delta_z4;
			F[0]=ratio*Qvol;//
			F[1]=ratio*Qvol;//
			F[2]=ratio*Qvol;//
			F[3]=ratio*Qvol;//
			F[4]=ratio*Qvol;//
			F[5]=ratio*Qvol;//
			F[6]=ratio*Qvol;//
			F[7]=ratio*Qvol;//
			ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
	 }

	 if(domain_optim[i0]==5){
		 ratio=delta_x5*delta_y5*delta_z5;
		 F[0]=ratio*Qvol;//
		 F[1]=ratio*Qvol;//
		 F[2]=ratio*Qvol;//
		 F[3]=ratio*Qvol;//
		 F[4]=ratio*Qvol;//
		 F[5]=ratio*Qvol;//
		 F[6]=ratio*Qvol;//
		 F[7]=ratio*Qvol;//
		ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
		}

		if(domain_optim[i0]==6){
			ratio=delta_x6*delta_y6*delta_z6;
			F[0]=ratio*Qvol;//
			F[1]=ratio*Qvol;//
			F[2]=ratio*Qvol;//
			F[3]=ratio*Qvol;//
			F[4]=ratio*Qvol;//
			F[5]=ratio*Qvol;//
			F[6]=ratio*Qvol;//
			F[7]=ratio*Qvol;//
		 ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
		 }

		 if(domain_optim[i0]==7){
			 ratio=delta_x7*delta_y7*delta_z7;
			 F[0]=ratio*Qvol;//
			 F[1]=ratio*Qvol;//
			 F[2]=ratio*Qvol;//
			 F[3]=ratio*Qvol;//
			 F[4]=ratio*Qvol;//
			 F[5]=ratio*Qvol;//
			 F[6]=ratio*Qvol;//
			 F[7]=ratio*Qvol;//
			ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
			}

			if(domain_optim[i0]==8){
				ratio=delta_x8*delta_y8*delta_z8;
				F[0]=ratio*Qvol;//
				F[1]=ratio*Qvol;//
				F[2]=ratio*Qvol;//
				F[3]=ratio*Qvol;//
				F[4]=ratio*Qvol;//
				F[5]=ratio*Qvol;//
				F[6]=ratio*Qvol;//
				F[7]=ratio*Qvol;//
			 ierr = VecSetValues(b,8,edofMat,F,INSERT_VALUES);CHKERRQ(ierr);
		 }
//}
	//}
}
	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);


	//Dirichlet condition

	for (i0=start; i0<end; i0++) {
		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;
	edofMat[1]=id+1;
	edofMat[2]=id+1+(ni+1);
	edofMat[3]=id+(ni+1);
	edofMat[4]=id + (ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);


				if(face[i0]==1){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(face[i0]==2){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp2,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp2,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(face[i0]==3){

					ierr = VecSetValues(b,1,&edofMat[2],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp3,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp3,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(face[i0]==4){

					ierr = VecSetValues(b,1,&edofMat[1],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp4,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp4,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(face[i0]==5){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp5,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp5,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp5,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp5,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(face[i0]==6){

					ierr = VecSetValues(b,1,&edofMat[4],&Tp6,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp6,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp6,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp6,INSERT_VALUES);CHKERRQ(ierr);

				}

				////////////

				if(edge[i0]==1){ //67

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==2){ //37

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==3){ //23

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==4){ //26

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}
				if(edge[i0]==5){ //45

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==6){ //04

					//ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==7){ //01

					//ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==8){ //15

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==9){ //56

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==10){ //47

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==11){ //03

					//ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(edge[i0]==12){ //12

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);

				}

				if(corner[i0]==1){ //6

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==2){ //7

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==3){ //4

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==4){ //5

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==5){ //2

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==6){ //3

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==7){ //0

					//ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}

				if(corner[i0]==8){ //1

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					//ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}


	}

	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);


	start2 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
	end2   = start2 + ndof/size + ((ndof%size) > rank);

	/*for(i=start2;i<end2;i++) {
		VecGetValues(b,1,&i,&y);
		if(y>0) printf("i=%d, b=%f\n",i,y);
		//if(y>minT) minT=y;
	}*/

	//MatView(K1,PETSC_VIEWER_STDOUT_SELF);


//	getchar();

	//Prod mat vecteur
	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);

	VecAXPY(b,1,F1);

 //Set exact solution; then compute right-hand-side vector.
printf("Solver beginning \n");

/*ierr = KSPSetOperators(ksp,K1,K1);CHKERRQ(ierr);
ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
ierr = PCSetType(pc,PCJACOBI);CHKERRQ(ierr);
ierr = KSPSetTolerances(ksp,1.e-5,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
CHKERRQ(ierr);
ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);*/


ierr = solver.setA(U1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
VecSet(b,0);


printf("size=%d et rank=%d\n",size,rank);
printf("start2=%d et end2=%d\n",start2,end2);
FILE *test0_f,*test1_f,*test2_f,*test3_f;

	test0_f =       fopen("temp_3d_test", "w"); memset(filename, 0, sizeof(filename));
/*	test1_f =       fopen("b", "w"); memset(filename, 0, sizeof(filename));
	test2_f =       fopen("K", "w"); memset(filename, 0, sizeof(filename));
	test3_f =       fopen("edge", "w"); memset(filename, 0, sizeof(filename));*/

double minT=-1;
for(i=start2;i<end2;i++) {
	VecGetValues(x,1,&i,&y); fprintf(test0_f,"%lg\n",y);
	if(y>minT) minT=y;
	//VecGetValues(b,1,&i,&y);	fprintf(test1_f,"%f\n",y);

}
printf("Tmax=%f\n",minT);
fclose(test0_f);
compteur++;
}
//fclose(test1_f);

/*for(i=0;i<ndof;i++) {
	for(j=0;j<ndof;j++) {
		MatGetValues(K1,1,&i,1,&j,&y); fprintf(test2_f,"%lg\n",y);
	}
}
fclose(test2_f);*/

MPI_Barrier(PETSC_COMM_WORLD);

solver.finalize();


printf("Fin boucle optim \n");

ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);
//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);

//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);
clock_end=clock();
printf("time=%ld\n",(clock_end-clock_start)*1000/CLOCKS_PER_SEC);
return ierr;
}
