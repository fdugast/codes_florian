
static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <sys/sysinfo.h>
#include <MMASolver.h>

#include<iostream>
#include<amgx_c.h>
#include <petscksp.h>
#include <petscis.h>
#include <AmgXSolver.hpp>

//#include "multi_inlet_contrainte_gamma_compliance_50_mpi.h"

#define TILE_I 2
#define TILE_J 2
#define TILE_K 2
#define PI 3.14159

#define I2D(ni,i,j) (((ni)*(j)) + i)
#define I3D(ni,nj,i,j,k) (((nj*ni)*(k))+((ni)*(j)) + i)




double Min(double d1, double d2) {
	return d1<d2 ? d1 : d2;
}

double Max(double d1, double d2) {
	return d1>d2 ? d1 : d2;
}


int Mini(int d1, int d2) {
	return d1<d2 ? d1 : d2;
}

int Maxi(int d1, int d2) {
	return d1>d2 ? d1 : d2;
}

double Abs(double d1) {
	return d1>0 ? d1 : -1.0*d1;
}


void edofMat_f(int i, int j ,int k, int ni, int nj, int nk, int *edofMat){
	edofMat[0]=i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
}

int main(int argc,char **args)
{

	printf("Debut programme \n");




printf("Init Petsc/Amgx\n");

	Vec            xtot,x,b,D,T,Tn,F1,F2;      /* approx solution, RHS, exact solution */
  Mat            K1,M1,U1,H;            /* linear system matrix */
  KSP            ksp;          /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscReal      norm;         /* norm of solution error */
  PetscErrorCode ierr;
  PetscInt        nelx,nely,nelz;
  PetscInt      n,its,nz,nz2,start,end,start2,end2,Id,Istart,Iend,nlocal;
  PetscMPIInt    rank,size;
  PetscScalar    value,value2,value3,xnew,rnorm,KE[64],ME[64],HE1[64],HE2[64],HE3[64],HE4[64],HE5[64],HE6[64],ke[64],me[64],*array;
  PetscScalar y,conv,Tp1;
	char filename[200],u2[200];

	double Xmin = 0.0001;
	double Xmax = 1;
	double movlim = 0.05;


	int display=0;
	struct sysinfo info;

	ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

	PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d, rank = %d\n", size, rank);
  int nele,ndof,nb_freedof,nb_dirichlet,*dirichlet_i, *dirichlet_j, nb_convection,*convection_i,*convection_j,nb_heatflux,*heatflux,boucle=0;
	double *convection_delta, *heatflux_delta;
	double volfrac;

  PetscInt start1,end1;
  double difference;
  double rmin,penal;
	penal=3;

  PetscInt *nnz, *nnz2;
	int ndof_total,icol,jcol,kcol,id,ni,nj,nk,ni2,nj2,i,j,k,l,i0,i0_left,i0_right,i0_bottom,i0_top,i0_side,k2_it,opt_it_max,alpha,cpt, *domain_optim;
	long unsigned int memoire_1,memoire_2,memoire_3,memoire_4,memoire_5,memoire_6,memoire_7;
	int nbsteps;

	FILE *fichier_K,*fichier_M, *fichier_H1,*fichier_H2, *fichier_H3,*fichier_H4,*fichier_H5,*fichier_H6;
	fichier_K=fopen("Ke_3d.txt","r");
	fichier_M=fopen("Me_3d.txt","r");
	fichier_H1=fopen("HE_3D_1.txt","r");
	fichier_H2=fopen("HE_3D_2.txt","r");
	fichier_H3=fopen("HE_3D_3.txt","r");
	fichier_H4=fopen("HE_3D_4.txt","r");
	fichier_H5=fopen("HE_3D_5.txt","r");
	fichier_H6=fopen("HE_3D_6.txt","r");
float test;
	for (i=0;i<64;i++){
		fscanf(fichier_K,"%f\n",&test);		KE[i]=test;
		fscanf(fichier_M,"%f\n",&test);		ME[i]=test;
		fscanf(fichier_H1,"%f\n",&test);		HE1[i]=test;
		fscanf(fichier_H2,"%f\n",&test);		HE2[i]=test;
		fscanf(fichier_H3,"%f\n",&test);		HE3[i]=test;
		fscanf(fichier_H4,"%f\n",&test);		HE4[i]=test;
		fscanf(fichier_H5,"%f\n",&test);		HE5[i]=test;
		fscanf(fichier_H6,"%f\n",&test);		HE6[i]=test;
	}

	fclose(fichier_K);
	fclose(fichier_M);
	fclose(fichier_H1);
	fclose(fichier_H2);
	fclose(fichier_H3);
	fclose(fichier_H4);
	fclose(fichier_H5);
	fclose(fichier_H6);

	AmgXSolver solver;
	ierr = solver.initialize(PETSC_COMM_WORLD, "dDDI", "/home/florian/Documents/test_AMGX/AMGX/core/configs/AMG_CLASSICAL_AGGRESSIVE_L1.json" ); CHKERRQ(ierr);
	int test1;
	int k2_max;
	FILE *domain;

	ni = 250;
	nj = ni;
	ndof_total=251*251*251;

	//int nk_tab[]={250};
	//int nbsteps_tab[]={1};
	int affichage[]={41,80,125};
	//int affichage[]={35};


	int nk_tab[]={70,160,200,250};
	//int nk_tab[]={70};
	//int nbsteps_tab[]={35};
	int nbsteps_tab[]={40,130,170,220};
	k2_max=sizeof(nk_tab)/sizeof(nk_tab[0]);
	printf("k2_max=%d\n",k2_max);

	int ndof_old=(ni+1)*(nj+1)*(nk_tab[0]+1);

	double hconv=10;
	double delta,somme,somme1,somme2,delta_x, delta_y,delta_z,delta_t,total_time,time;
	double rho_solide,lambda_solide,cp_solide,c_solide,rho_vide,lambda_vide,cp_vide,c_vide,Qvol,Qsurf,*cp,*c,*lambda,*rho,*diff,*derivee_c,*derivee_lambda,T0,Tp2,Tair;

	int *boundary;
	double *x1;
	int *pos_q1,*q1, *num_elem;
	double *gamma_x, *gamma_h;
	int compteur=0;
	PetscInt *edofMat2;
	int nb_BC;
	IS is;
	PetscInt n1,n2;
	const PetscInt *nindices;

	double *F;	F=(double *)calloc(8,sizeof(double));
	int *edofMat;	edofMat=(int *)calloc(8,sizeof(int));

	delta_x=0.001; //domaine de 10 mm par 10 mm ...
	delta_y=0.001;
	delta_z=0.001;
	delta_t=1; // si delta t trop fort oscillations sur compliance
	total_time=1;
	rho_solide=1000;
	lambda_solide=100;
	cp_solide=500;
	rho_vide=1;
	lambda_vide=0.01;
	cp_vide=1000;
	//rho_vide=1000;
	//lambda_vide=100;
	//cp_vide=500;
	c_vide=rho_vide*cp_vide;
	c_solide=rho_solide*cp_solide;

	T0=0; //Initial temperature
	Tp1=0; //Dirichlet temperature
	Tair=30;

	ierr = VecCreate(PETSC_COMM_WORLD,&xtot);CHKERRQ(ierr);
	ierr = PetscObjectSetName((PetscObject) xtot, "Solution");CHKERRQ(ierr);
	ierr = VecSetSizes(xtot,PETSC_DECIDE,ndof_total);CHKERRQ(ierr);
	ierr = VecSetFromOptions(xtot);CHKERRQ(ierr);
	ierr = VecSet(xtot,0);CHKERRQ(ierr);


	//nbsteps=total_time/delta_t;
	int changement=1;
	MPI_Barrier(PETSC_COMM_WORLD);


for (int k2=0;k2<k2_max;k2++){

	if(rank==0) printf("k2=%d, changement=%d\n",k2,changement);
	if(rank==1) printf("k2=%d, changement=%d\n",k2,changement);
	if(rank==2) printf("k2=%d, changement=%d\n",k2,changement);
	if(rank==3) printf("k2=%d, changement=%d\n",k2,changement);

nk=nk_tab[k2];
nbsteps=nbsteps_tab[k2];

if(changement==1){
	ndof=(ni+1)*(nj+1)*(nk+1);
	nele=ni*nj*nk;

	start = rank*(nele/size) + ((nele%size) < rank ? (nele%size) : rank);
	end   = start + nele/size + ((nele%size) > rank);



	printf("Creation objects \n");

	ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
	ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
	ierr = VecSetSizes(x,PETSC_DECIDE,ndof);CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&F1);CHKERRQ(ierr);

	/*ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);
	printf("nlocal=%d\n",nlocal);*/

	ierr = VecSet(F1,0);CHKERRQ(ierr);

	start2 = rank*(ndof_old/size) + ((ndof_old%size) < rank ? (ndof_old%size) : rank);
	end2   = start2 + ndof_old/size + ((ndof_old%size) > rank);

/*	for(i=start2;i<end2;i++) {
		VecGetValues(xtot,1,&i,&y);
		VecSetValues(x,1,&i,&y,INSERT_VALUES);
	}*/


	start1 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
	end1   = start1 + ndof/size + ((ndof%size) > rank);

	printf("size=%d et rank=%d\n",size,rank);
	printf("start=%d et end=%d\n",start,end);

	boundary=(int *)calloc(nele,sizeof(int));
	domain_optim=(int *)calloc(nele,sizeof(int));
	cp=(double *)calloc(nele,sizeof(double));
	lambda=(double *)calloc(nele,sizeof(double));
	rho=(double *)calloc(nele,sizeof(double));
	diff=(double *)calloc(nele,sizeof(double));
	c=(double *)calloc(nele,sizeof(double));

	//	solide=(double *)calloc(nele,sizeof(double));
		gamma_h=(double *)calloc(nele,sizeof(double));
		gamma_x=(double *)calloc(nele,sizeof(double));


		q1=(int *)calloc(nbsteps,sizeof(int));
		pos_q1=(int *)calloc(ndof,sizeof(int));
		num_elem=(int *)calloc(nbsteps,sizeof(int));



		for (i=0;i<nbsteps;i++) q1[i]=21+i;

		domain=fopen("domain_3d_250_z","r");

		cpt=0;
		for (k=0; k<nk; k++) {
		 for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {
				i0=I3D(ni,nj,i,j,k);
				fscanf(domain,"%d\n",&test1);
				domain_optim[i0]=1-test1;
			}
		}
	}

fclose(domain);
//getchar();


for (k=0; k<nk; k++) {
 for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {
				i0=I3D(ni,nj,i,j,k);
				//fscanf(init,"%f",&test);
				if(domain_optim[i0]==1){
				gamma_h[i0]=0;

			}
			if(domain_optim[i0]==0){
				gamma_h[i0]=1;
				gamma_x[i0]=1;//volfrac;
			}
			}
		}
	}

	//Definition of boundary conditions

	for (k=0; k<nk; k++) {
	 for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {
				i0=I3D(ni,nj,i,j,k);

				if(i==0 || j==0 || i==(ni-1) || j==(nj-1) || k==0 || k==(nk-1)){

				//if(i==0 && j>0) {boundary[i0]=2; cpt2=cpt2+1;}
				//boundary[i0]=1;
				if(j==0 ) boundary[i0]=1;
				if(i==0 ) boundary[i0]=2;//2;
				if(j==(nj-1)) boundary[i0]=3;//3;
				if(i==(ni-1)) boundary[i0]=4;//4; }
				if(k==0) boundary[i0]=5;
				if(k==(nk-1)) boundary[i0]=6;//2;

			}

			}
		}
	}


	for (k=0; k<nk; k++) {
	 for (j=0; j<nj; j++) {
for (i=0; i<ni; i++) {
									i0=I3D(ni,nj,i,j,k);

										if(domain_optim[i0]==0 && k>=q1[compteur]){
											lambda[i0]=lambda_vide; rho[i0]=rho_vide;cp[i0]=cp_vide; c[i0]=c_vide;
										}
										if(domain_optim[i0]==0 && k<q1[compteur]){
											lambda[i0]=lambda_solide; rho[i0]=rho_solide;cp[i0]=cp_solide;c[i0]=c_solide;
										}
										if(domain_optim[i0]==1){
										lambda[i0]=lambda_vide+PetscPowScalar(gamma_h[i0],penal)*(lambda_solide-lambda_vide);
										rho[i0]=rho_vide+PetscPowScalar(gamma_h[i0],penal)*(rho_solide-rho_vide);
										cp[i0]=cp_vide+PetscPowScalar(gamma_h[i0],penal)*(cp_solide-cp_vide);
										diff[i0]=lambda[i0]/(rho[i0]*cp[i0]);
										c[i0]=c_vide+PetscPowScalar(gamma_h[i0],penal)*(c_solide-c_vide);
									}
									}
								}
							}

					//DIRICHLET BOUNDARY CONDITION
						cpt=0;
						for (k=0; k<nk; k++) {
						 for (j=0; j<nj; j++) {
				for (i=0; i<ni; i++) {
						 i0=I3D(ni,nj,i,j,k);

								 if(boundary[i0]==5){
									 cpt=cpt+1;
								 }
							 }
						 }
					 }
					 nb_BC=cpt;

					PetscMalloc1(8*nb_BC,&edofMat2);


					cpt=0;
					for (k=0; k<nk; k++) {
					 for (j=0; j<nj; j++) {
			for (i=0; i<ni; i++) {
					 i0=I3D(ni,nj,i,j,k);

							 if(boundary[i0]==5){
								 value=1;
								 edofMat2[8*cpt]=i+(ni+1)*j+k*(ni+1)*(nj+1);
								 edofMat2[1+8*cpt]=i+1+(ni+1)*j+k*(ni+1)*(nj+1);
								 edofMat2[2+8*cpt]=i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
								 edofMat2[3+8*cpt]=i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
								 edofMat2[4+8*cpt]=i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
								 edofMat2[5+8*cpt]=i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
								 edofMat2[6+8*cpt]=i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
								 edofMat2[7+8*cpt]=i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
									cpt=cpt+1;
							 }
						 }
						}
					}

					ISCreateGeneral(PETSC_COMM_SELF,8*nb_BC,edofMat2,PETSC_COPY_VALUES,&is);
					 ISGetLocalSize(is,&n1);
					ISSortRemoveDups(is);
					 ISGetLocalSize(is,&n2);

						ISGetIndices(is,&nindices);

					 printf("n1=%d et n2=%d \n",n1,n2);

											 //sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared1=%ld\n",memoire_1);


					 ierr = MatCreate(PETSC_COMM_WORLD,&K1);CHKERRQ(ierr);
					 ierr = MatSetSizes(K1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
					 ierr = MatSetFromOptions(K1);CHKERRQ(ierr);
					 ierr = MatSetUp(K1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(K1,50,NULL,50,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(K1,50,NULL);CHKERRQ(ierr);
					 //printf("apres malloc K1\n"); getchar();

					 ierr = MatCreate(PETSC_COMM_WORLD,&M1);CHKERRQ(ierr);
					 ierr = MatSetSizes(M1,PETSC_DECIDE,PETSC_DECIDE,ndof,ndof);CHKERRQ(ierr);
					 ierr = MatSetFromOptions(M1);CHKERRQ(ierr);
					 ierr = MatSetUp(M1);CHKERRQ(ierr);
					 if(size>1) ierr=MatMPIAIJSetPreallocation(M1,50,NULL,50,NULL);CHKERRQ(ierr);
					 if(size==1) ierr=MatSeqAIJSetPreallocation(M1,50,NULL);CHKERRQ(ierr);
					 //printf("apres malloc M1\n"); getchar();

				// sysinfo(&info);		memoire_1=info.freeram/(1024*1024); printf("memory_cleared2=%ld\n",memoire_1);

					 //printf("Istart=%d, Iend=%d \n",Istart,Iend);

					 // Loop over elements
					 for (i0=start; i0<end; i0++) {

					 kcol=i0/(ni*nj);
					 jcol=(i0%(nj*ni))/ni;
					 //printf("jcol=%d\n",jcol);
					 icol=	(i0%(nj*ni))%ni;
					 id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

					 edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
					 edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

							 for (l=0;l<8*8;l++){
							 ke[l]=KE[l]*lambda[i0]/(delta_y*delta_x*delta_z);
							 me[l]=ME[l]*c[i0]/delta_t;
									 }
						 // Add values to the sparse matrix
						 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
						 ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);
					 //	}
					 }

					 ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

					 // Loop over elements
					 for (i0=start; i0<end; i0++) {

						 kcol=i0/(ni*nj);
						 jcol=(i0%(nj*ni))/ni;
						 icol=	(i0%(nj*ni))%ni;
						 id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

					 edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
					 edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
					 edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
					 edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
					 edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

								 /*if(boundary[i0]==1){
								 edofMat_f(i,j,k,ni,nj,nk,edofMat);
								 for (l=0;l<8*8;l++)	ke[l]=HE1[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }*/

							 if(boundary[i0]==1){
							 for (l=0;l<8*8;l++)	ke[l]=HE1[l]*hconv/(delta_z*delta_x);
							 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }

								 if(boundary[i0]==2){
								 for (l=0;l<8*8;l++)	ke[l]=HE2[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
								 }

								 if(boundary[i0]==3){
								 for (l=0;l<8*8;l++)	ke[l]=HE3[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }

								 if(boundary[i0]==4){
								 for (l=0;l<8*8;l++)	ke[l]=HE4[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }

								/* if(boundary[i0]==5){
								 for (l=0;l<8*8;l++) ke[l]=HE5[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }*/

							//if(k2==(k2_max-1)){
									 if(boundary[i0]==6){
								 for (l=0;l<8*8;l++)	ke[l]=HE6[l]*hconv/(delta_z*delta_x);
								 ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
							 }
						 //}
					 }

					 ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
					 ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

					 ierr = MatZeroRows(K1,n2,nindices,1,0,0);CHKERRQ(ierr);
					 ierr = MatZeroRows(M1,n2,nindices,1,0,0);CHKERRQ(ierr);

					 //ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
					 //ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
					 changement=0;
				 }
				 MPI_Barrier(PETSC_COMM_WORLD);
				 if(rank==0) printf("changement=%d\n",changement);
				 if(rank==1) printf("changement=%d\n",changement);
				 if(rank==2) printf("changement=%d\n",changement);
				 if(rank==3) printf("changement=%d\n",changement);

	while(compteur<nbsteps ){


/////////// Load //////////////

cpt=0;

for (i=start1; i<end1; i++) {
		pos_q1[i]=0;
}

			for (i0=start; i0<end; i0++) {

				kcol=i0/(ni*nj);
				jcol=(i0%(nj*ni))/ni;
				icol=	(i0%(nj*ni))%ni;
				id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;
				if((kcol>=0+q1[compteur])&& (kcol<1+q1[compteur])){

				if(domain_optim[i0]==0){

				edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
				edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
				edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
				edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
				edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
				edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
				edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
				edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

			pos_q1[edofMat[0]]=1;
			pos_q1[edofMat[1]]=1;
			pos_q1[edofMat[2]]=1;
			pos_q1[edofMat[3]]=1;
			pos_q1[edofMat[4]]=1;
			pos_q1[edofMat[5]]=1;
			pos_q1[edofMat[6]]=1;
			pos_q1[edofMat[7]]=1;
			cpt=cpt+1;

			for (l=0;l<8*8;l++){
			ke[l]=KE[l]*(lambda_solide-lambda_vide)/(delta_x*delta_y*delta_z);
			me[l]=ME[l]*(c_solide-c_vide)/delta_t;
							}
		// Add values to the sparse matrix
		ierr = MatSetValues(K1,8,edofMat,8,edofMat,ke,ADD_VALUES);
		ierr = MatSetValues(M1,8,edofMat,8,edofMat,me,ADD_VALUES);

		}

	}
}
	num_elem[compteur]=cpt;
	printf("num_elem=%d\n",num_elem[compteur]);


	ierr=MatAssemblyBegin(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(K1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	ierr=MatAssemblyBegin(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr=MatAssemblyEnd(M1, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	//sysinfo(&info);		memoire_3=info.freeram/(1024*1024);
//printf("debut boucle : freeram=%ld\n",info.freeram/(1024*1024));

	//ierr=MatAXPY(M1,1,K1,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

	//Addition de M et K
	ierr=MatDuplicate(M1,MAT_COPY_VALUES,&U1);CHKERRQ(ierr);
	ierr=MatAXPY(U1,1,K1,SAME_NONZERO_PATTERN);CHKERRQ(ierr);

//	sysinfo(&info);		memoire_4=info.freeram/(1024*1024);
//printf("debut boucle : freeram=%ld\n",info.freeram/(1024*1024));

	printf("Load \n");

	for (i0=start; i0<end; i0++) {

		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

	if(num_elem[compteur]>0){

		F[0]=pos_q1[edofMat[0]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[1]=pos_q1[edofMat[1]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[2]=pos_q1[edofMat[2]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[3]=pos_q1[edofMat[3]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[4]=pos_q1[edofMat[4]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[5]=pos_q1[edofMat[5]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[6]=pos_q1[edofMat[6]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
		F[7]=pos_q1[edofMat[7]]*6e5/(num_elem[compteur]*delta_x*delta_y*delta_z);//
	}
	else{
		F[0]=0;//
		F[1]=0;//
		F[2]=0;//
		F[3]=0;//
		F[4]=0;//
		F[5]=0;//
		F[6]=0;//
		F[7]=0;//
	}


/*if(domain_optim[i0]==0){
			F[0]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[1]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[2]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[3]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[4]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[5]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[6]=(double)0.01/(delta_x*delta_y*delta_z);//
			F[7]=(double)0.01/(delta_x*delta_y*delta_z);//


}*/
ierr = VecSetValues(b,8,edofMat,F,ADD_VALUES);CHKERRQ(ierr);
	}

	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);


	//Dirichlet condition

	for (i0=start; i0<end; i0++) {

		kcol=i0/(ni*nj);
		jcol=(i0%(nj*ni))/ni;
		icol=	(i0%(nj*ni))%ni;
		id=(ni+1)*(nj+1)*kcol+(ni+1)*jcol+icol;

	edofMat[0]=id;//i+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[1]=id+1;//i+1+(ni+1)*j+k*(ni+1)*(nj+1);
	edofMat[2]=id+1+(ni+1);//i+1+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[3]=id+(ni+1);//i+(ni+1)*(j+1)+k*(ni+1)*(nj+1);
	edofMat[4]=id + (ni+1)*(nj+1);//i+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[5]=id+1 + (ni+1)*(nj+1);//i+1+(ni+1)*j+(k+1)*(ni+1)*(nj+1);
	edofMat[6]=id+1+(ni+1) + (ni+1)*(nj+1);//i+1+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);
	edofMat[7]=id+(ni+1) + (ni+1)*(nj+1);//i+(ni+1)*(j+1)+(k+1)*(ni+1)*(nj+1);

		if(boundary[i0]==1){
		//edofMat_f(i,j,k,ni,nj,nk,edofMat);

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==2){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==3){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_x*delta_z);

			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		if(boundary[i0]==4){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_z);

			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}

		/*if(boundary[i0]==5){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

			ierr = VecSetValues(b,1,&edofMat[0],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[1],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[2],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[3],&conv,INSERT_VALUES);CHKERRQ(ierr);

		}*/
		//	if(k2==(k2_max-1)){
		if(boundary[i0]==6){

			conv=hconv*(delta_x*delta_y*delta_z)*Tair/(delta_y*delta_x);

			ierr = VecSetValues(b,1,&edofMat[4],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[5],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[6],&conv,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValues(b,1,&edofMat[7],&conv,INSERT_VALUES);CHKERRQ(ierr);

	//	}
		}

				if(boundary[i0]==5){

					ierr = VecSetValues(b,1,&edofMat[0],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[1],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[2],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[3],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[4],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[5],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[6],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
					ierr = VecSetValues(b,1,&edofMat[7],&Tp1,INSERT_VALUES);CHKERRQ(ierr);
				}
	}

	ierr=VecAssemblyBegin(b);CHKERRQ(ierr);
	ierr=VecAssemblyEnd(b);CHKERRQ(ierr);

	//Prod mat vecteur
	ierr=MatMult(M1,x,F1);CHKERRQ(ierr);

	VecAXPY(b,1,F1);
printf("Solver beginning \n");

ierr = solver.setA(U1); CHKERRQ(ierr);
ierr = solver.solve(x,b); CHKERRQ(ierr);
VecSet(b,0);


compteur=compteur+1;
printf("compteur=%d\n",compteur);

ierr = MatDestroy(&U1);CHKERRQ(ierr);

MPI_Barrier(PETSC_COMM_WORLD);
if(compteur==affichage[display]){
start2 = rank*(ndof/size) + ((ndof%size) < rank ? (ndof%size) : rank);
end2   = start2 + ndof/size + ((ndof%size) > rank);

printf("size=%d et rank=%d\n",size,rank);
printf("start2=%d et end2=%d\n",start2,end2);
FILE *test0_f,*test1_f,*test2_f,*test3_f;

if(rank==0) {strcpy(filename,"test0_250_cut4_bc2_"); sprintf(u2,"%d",affichage[display]); strcat(filename,u2);	test0_f =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));}
if(rank==1) {strcpy(filename,"test1_250_cut4_bc2_"); sprintf(u2,"%d",affichage[display]); strcat(filename,u2);	test1_f =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));}
if(rank==2) {strcpy(filename,"test2_250_cut4_bc2_"); sprintf(u2,"%d",affichage[display]); strcat(filename,u2);	test2_f =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));}
if(rank==3) {strcpy(filename,"test3_250_cut4_bc2_"); sprintf(u2,"%d",affichage[display]); strcat(filename,u2);	test3_f =       fopen(filename, "w"); memset(filename, 0, sizeof(filename));}

for(i=start2;i<end2;i++) {
if(rank==0){	VecGetValues(x,1,&i,&y); fprintf(test0_f,"%f\n",y);}
if(rank==1) {	VecGetValues(x,1,&i,&y); fprintf(test1_f,"%f\n",y);}
if(rank==2) {	VecGetValues(x,1,&i,&y); fprintf(test2_f,"%f\n",y);}
if(rank==3) {	VecGetValues(x,1,&i,&y); fprintf(test3_f,"%f\n",y);}

}

MPI_Barrier(PETSC_COMM_WORLD);
if(rank==0) fclose(test0_f);
if(rank==1) fclose(test1_f);
if(rank==2) fclose(test2_f);
if(rank==3) fclose(test3_f);
display=display+1;
printf("display=%d\n",display);
MPI_Barrier(PETSC_COMM_WORLD);
}


} //Fin boucle transient

if(compteur==nbsteps){
	printf("CHANGEMENT FIN BOUCLE \n");
	changement=1;
	for(i=start2;i<end2;i++) {
		VecGetValues(x,1,&i,&y);
		VecSetValues(xtot,1,&i,&y,INSERT_VALUES);
	}
	MPI_Barrier(PETSC_COMM_WORLD);
	ndof_old=ndof;
}
MPI_Barrier(PETSC_COMM_WORLD);


ierr = MatDestroy(&K1);CHKERRQ(ierr);
ierr = MatDestroy(&M1);CHKERRQ(ierr);

ierr = VecDestroy(&x);CHKERRQ(ierr);
ierr = VecDestroy(&b);CHKERRQ(ierr);


}

solver.finalize();



//ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
ierr = PetscFinalize();CHKERRQ(ierr);

return ierr;
}
